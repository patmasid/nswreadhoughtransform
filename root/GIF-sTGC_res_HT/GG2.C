void GG2(TH1F* h){
  char line[80];
  int i;
  float center, edge, nGood;

  TF1* gg = new TF1("gg", "[0]*exp(-0.5*((x-[1])/[2])^2)+[3]*exp(-0.5*(x/[4])^2)", -800, 800);
  gg->SetParNames ("Constant", "Mean", "Sigma", "Background", "Sigma Bkgnd");
  printf ("11111111111\n");
  center = 0;
  int nbin = h->GetNbinsX();
  for (i=-5; i<=5; i++) center += h->GetBinContent (nbin/2+1+i);
  center /= 10;
  edge=0;
  for (i=nbin/10; i<2*nbin/10; i++)    edge += h->GetBinContent(i+1);
  for (i=8*nbin/10; i<9*nbin/10; i++)  edge += h->GetBinContent(i+1);

  edge /= 2.0*(nbin/10);
  //if (edge<center/10) edge=center/10;
  printf ("center = %f, edge = %f\n", center, edge);
  gg->SetParameters(center-edge, 0, 0.4, edge, 2.4);

  h->Fit("gg", "LQ");
  float error = 1000*gg->GetParameter(2)*sqrt(0.6666666666666);
  //float error = 1000*gg->GetParameter(2)/sqrt(14.0/9.0);
  sprintf (line, "%6.1f micron", error);
  printf ("Resolution %6.1f microns, ", error);
  TText *text2 = new TText(0.12, 0.75, line);
  text2->SetNDC();
  text2->Draw();

  nGood = sqrt(2*3.141592)*gg->GetParameter(0)*gg->GetParameter(2)/h->GetBinWidth(2);
  printf ("%5.1f %%\n", 100.0*nGood/h->GetEntries());
  //nGood = 0;
  //for (i=75; i<325; i++) nGood+=h->GetBinContent(i+1);
  //printf ("%5.1f \n", 100.0*nGood/h->GetEntries());

  sprintf (line, "%5.1f %% inner", 100.0*nGood/h->GetEntries());
  TText *text2b = new TText(0.12, 0.68, line);
  text2b->SetNDC();
  text2b->Draw();
  //delete gg;
  //delete text2;
  //delete text2b;
}
