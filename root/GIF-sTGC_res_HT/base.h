//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Nov  7 14:00:00 2021 by ROOT version 6.24/02
// from TTree tree/Clusters
// found on file: data_test.1635662999._SRCoff-GIFCoin-sngON-sfrstOFF-20deg.daq.RAW._lb0000._GIF_sTGC-A13-sTGC-swROD._0001.seg.root
//////////////////////////////////////////////////////////

#ifndef base_h
#define base_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class base {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           branch_event;
   Int_t           branch_l1id;
   Int_t           branch_nprec;
   Int_t           branch_nclu;
   Int_t           branch_wid[8];
   Float_t         branch_cog[8];
   Float_t         branch_cog2[8];
   Float_t         branch_charge[8];
   Float_t         branch_z[8];
   Float_t         branch_inter;
   Float_t         branch_slope;
   Float_t         branch_chi2;
   Float_t         branch_angle;
   Float_t         branch_pred[8];
   Float_t         branch_res[8];
   Float_t         branch_y;
   Float_t         branch_amp[8][10];
   Float_t         branch_time[8][10];
   Int_t           branch_gap[8];
   Int_t           branch_start[8];
   Int_t           branch_peak[8];
   Int_t           branch_nflags[8];
   Float_t         branch_raw[8];
   Float_t         branch_cog3[8];

   // List of branches
   TBranch        *b_branch;   //!

   base(TTree *tree=0);
   virtual ~base();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef base_cxx
base::base(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data_test.1635662999._SRCoff-GIFCoin-sngON-sfrstOFF-20deg.daq.RAW._lb0000._GIF_sTGC-A13-sTGC-swROD._0001.seg.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data_test.1635662999._SRCoff-GIFCoin-sngON-sfrstOFF-20deg.daq.RAW._lb0000._GIF_sTGC-A13-sTGC-swROD._0001.seg.root");
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

base::~base()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t base::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t base::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void base::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("branch", &branch_event, &b_branch);
   Notify();
}

Bool_t base::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void base::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t base::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef base_cxx
