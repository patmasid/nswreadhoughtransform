#include "TFile.h"
#include "TH1.h"
#include "TString.h"
#include "TTree.h"

/** RooFit */
#include "RooRealVar.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooSimultaneous.h"
#include "RooCategory.h"
#include "RooConstVar.h"
#include "RooGenericPdf.h"

using namespace RooFit;

void FitResolution(TString filename){

  TFile *inFile = TFile::Open(filename, "READ");

  TH1F *hAli12 = (TH1F *)inFile->Get("hAli12");
  TH1F *hAli13 = (TH1F *)inFile->Get("hAli13");
  TH1F *hAli14 = (TH1F *)inFile->Get("hAli14");
  TH1F *hAli23 = (TH1F *)inFile->Get("hAli23");
  TH1F *hAli24 = (TH1F *)inFile->Get("hAli24");
  TH1F *hAli34 = (TH1F *)inFile->Get("hAli34");

  /** Simultaneous fit of sigma1, sigma2, sigma3, sigma4 */
  RooRealVar x("x", "x", -2, 2);

  std::cout<<"Start importing hist data..."<<std::endl;

  RooDataHist dh12("dh12", "dh12", x, Import(*hAli12)); 
  RooDataHist dh13("dh13", "dh13", x, Import(*hAli13)); 
  RooDataHist dh14("dh14", "dh14", x, Import(*hAli14)); 
  RooDataHist dh23("dh23", "dh23", x, Import(*hAli23)); 
  RooDataHist dh24("dh24", "dh24", x, Import(*hAli24)); 
  RooDataHist dh34("dh34", "dh34", x, Import(*hAli34)); 

  RooRealVar sigma1_s("sigma1_s", "sigma1_s", 0.1, 0, 2);
  RooRealVar sigma2_s("sigma2_s", "sigma2_s", 0.1, 0, 2);
  RooRealVar sigma3_s("sigma3_s", "sigma3_s", 0.1, 0, 2);
  RooRealVar sigma4_s("sigma4_s", "sigma4_s", 0.1, 0, 2);
  RooRealVar sigma_b("sigma_b", "sigma_b", 0.1, 0, 1);

  int nbin12 = hAli12->GetNbinsX();
  int nbin13 = hAli13->GetNbinsX();
  int nbin14 = hAli14->GetNbinsX();
  int nbin23 = hAli23->GetNbinsX();
  int nbin24 = hAli24->GetNbinsX();
  int nbin34 = hAli34->GetNbinsX();
  int center12=0, center13=0, center14=0, center23=0, center24=0, center34=0;
  int edge12=0, edge13=0, edge14=0, edge23=0, edge24=0, edge34=0;
  int peak12=0, peak13=0, peak14=0, peak23=0, peak24=0, peak34=0;

  for (int i=1; i<nbin12+1; i++) {
    if(center12 < hAli12->GetBinContent(i)){
      center12 = hAli12->GetBinContent(i);
      peak12 = hAli12->GetBinCenter(i);
    }
  }
  for (int i=3*nbin12/10; i<4*nbin12/10; i++)    edge12 += hAli12->GetBinContent(i+1);
  for (int i=6*nbin12/10; i<7*nbin12/10; i++)  edge12 += hAli12->GetBinContent(i+1);
  std::cout<<"edge12 = "<<edge12<<std::endl;
  edge12 /= 2.0*(nbin12/10);
  std::cout<<"edge12 = "<<edge12<<std::endl;

  for (int i=1; i<nbin13+1; i++) {
    if(center13 < hAli13->GetBinContent(i)){
      center13 = hAli13->GetBinContent(i);
      peak13 = hAli13->GetBinCenter(i);
    }
  }
  for (int i=3*nbin13/10; i<4*nbin13/10; i++)    edge13 += hAli13->GetBinContent(i+1);
  for (int i=6*nbin13/10; i<7*nbin13/10; i++)  edge13 += hAli13->GetBinContent(i+1);
  edge13 /= 2.0*(nbin13/10);

  for (int i=1; i<nbin14+1; i++) {
    if(center14 < hAli14->GetBinContent(i)){
      center14 = hAli14->GetBinContent(i);
      peak14 = hAli14->GetBinCenter(i);
    }
  }
  for (int i=3*nbin14/10; i<4*nbin14/10; i++)    edge14 += hAli14->GetBinContent(i+1);
  for (int i=6*nbin14/10; i<7*nbin14/10; i++)  edge14 += hAli14->GetBinContent(i+1);
  edge14 /= 2.0*(nbin14/10);

  for (int i=1; i<nbin23+1; i++) {
    if(center23 < hAli23->GetBinContent(i)){
      center23 = hAli23->GetBinContent(i);
      peak23 = hAli23->GetBinCenter(i);
    }
  }
  for (int i=3*nbin23/10; i<4*nbin23/10; i++)    edge23 += hAli23->GetBinContent(i+1);
  for (int i=6*nbin23/10; i<7*nbin23/10; i++)  edge23 += hAli23->GetBinContent(i+1);
  edge23 /= 2.0*(nbin23/10);

  for (int i=1; i<nbin24+1; i++) {
    if(center24 < hAli24->GetBinContent(i)){
      center24 = hAli24->GetBinContent(i);
      peak24 = hAli24->GetBinCenter(i);
    }
  }
  for (int i=3*nbin24/10; i<4*nbin24/10; i++)    edge24 += hAli24->GetBinContent(i+1);
  for (int i=6*nbin24/10; i<7*nbin24/10; i++)  edge24 += hAli24->GetBinContent(i+1);
  edge24 /= 2.0*(nbin24/10);

  for (int i=1; i<nbin34+1; i++) {
    if(center34 < hAli34->GetBinContent(i)){
      center34 = hAli34->GetBinContent(i);
      peak34 = hAli34->GetBinCenter(i);
    }
  }
  for (int i=3*nbin34/10; i<4*nbin34/10; i++)    edge34 += hAli34->GetBinContent(i+1);
  for (int i=6*nbin34/10; i<7*nbin34/10; i++)  edge34 += hAli34->GetBinContent(i+1);
  edge34 /= 2.0*(nbin34/10);

  RooRealVar mean("mean", "mean", peak12, -1, 1);
  // RooRealVar mean12("mean12", "mean12", peak12, -1, 1);
  // RooRealVar mean13("mean13", "mean13", peak13, -1, 1);
  // RooRealVar mean14("mean14", "mean14", peak14, -1, 1);
  // RooRealVar mean23("mean23", "mean23", peak23, -1, 1);
  // RooRealVar mean24("mean24", "mean24", peak24, -1, 1);
  // RooRealVar mean34("mean34", "mean34", peak34, -1, 1);

  RooRealVar amp12_s("amp12_s", "amp12_s", center12-edge12, 0, center12*1.5);
  RooRealVar amp13_s("amp13_s", "amp13_s", center13-edge13, 0, center13*1.5);
  RooRealVar amp14_s("amp14_s", "amp14_s", center24-edge14, 0, center14*1.5);
  RooRealVar amp23_s("amp23_s", "amp23_s", center23-edge23, 0, center23*1.5);
  RooRealVar amp24_s("amp24_s", "amp24_s", center24-edge24, 0, center24*1.5);
  RooRealVar amp34_s("amp34_s", "amp34_s", center34-edge34, 0, center34*1.5);

  RooRealVar amp12_b("amp12_b", "amp12_b", center12*0.1, 0, center12*0.5);
  RooRealVar amp13_b("amp13_b", "amp13_b", center13*0.1, 0, center13*0.5);
  RooRealVar amp14_b("amp14_b", "amp14_b", center14*0.1, 0, center14*0.5);
  RooRealVar amp23_b("amp23_b", "amp23_b", center23*0.1, 0, center23*0.5);
  RooRealVar amp24_b("amp24_b", "amp24_b", center24*0.1, 0, center24*0.5);
  RooRealVar amp34_b("amp34_b", "amp34_b", center34*0.1, 0, center34*0.5);

  RooGenericPdf gaus12("gaus12", "gaus fit for layer 1 and 2 residual", "(amp12_s*exp(-0.5*(x-1*mean)^2/(sigma1_s^2+sigma2_s^2))+amp12_b*exp(-0.5*(x/sigma_b)^2))*0.01", RooArgList(x, amp12_s, amp12_b, mean, sigma1_s, sigma2_s, sigma_b));
  RooGenericPdf gaus13("gaus13", "gaus fit for layer 1 and 3 residual", "(amp13_s*exp(-0.5*(x-2*mean)^2/(sigma1_s^2+sigma3_s^2))+amp13_b*exp(-0.5*(x/sigma_b)^2))*0.01", RooArgList(x, amp13_s, amp13_b, mean, sigma1_s, sigma3_s, sigma_b));
  RooGenericPdf gaus14("gaus14", "gaus fit for layer 1 and 4 residual", "(amp14_s*exp(-0.5*(x-3*mean)^2/(sigma1_s^2+sigma4_s^2))+amp14_b*exp(-0.5*(x/sigma_b)^2))*0.01", RooArgList(x, amp14_s, amp14_b, mean, sigma1_s, sigma4_s, sigma_b));
  RooGenericPdf gaus23("gaus23", "gaus fit for layer 2 and 3 residual", "(amp23_s*exp(-0.5*(x-1*mean)^2/(sigma2_s^2+sigma3_s^2))+amp23_b*exp(-0.5*(x/sigma_b)^2))*0.01", RooArgList(x, amp23_s, amp23_b, mean, sigma2_s, sigma3_s, sigma_b));
  RooGenericPdf gaus24("gaus24", "gaus fit for layer 2 and 4 residual", "(amp24_s*exp(-0.5*(x-2*mean)^2/(sigma2_s^2+sigma4_s^2))+amp24_b*exp(-0.5*(x/sigma_b)^2))*0.01", RooArgList(x, amp24_s, amp24_b, mean, sigma2_s, sigma4_s, sigma_b));
  RooGenericPdf gaus34("gaus34", "gaus fit for layer 3 and 4 residual", "(amp34_s*exp(-0.5*(x-1*mean)^2/(sigma3_s^2+sigma4_s^2))+amp34_b*exp(-0.5*(x/sigma_b)^2))*0.01", RooArgList(x, amp34_s, amp34_b, mean, sigma3_s, sigma4_s, sigma_b));

  RooCategory res("res", "res");
  res.defineType("res12");
  res.defineType("res13");
  res.defineType("res14");
  res.defineType("res23");
  res.defineType("res24");
  res.defineType("res34");

  std::cout<<"Start combining hist dataset..."<<std::endl;
  RooDataHist combRes("combRes", "combined residuals", x, Index(res), Import("res12", dh12), Import("res13", dh13), Import("res14", dh14), Import("res23", dh23), Import("res24", dh24), Import("res34", dh34));
  std::cout<<"Finish combining hist dataset..."<<std::endl;

  RooSimultaneous combPdf("combPdf", "combined pdf", res);
  combPdf.addPdf(gaus12, "res12");
  combPdf.addPdf(gaus13, "res13");
  combPdf.addPdf(gaus14, "res14");
  combPdf.addPdf(gaus23, "res23");
  combPdf.addPdf(gaus24, "res24");
  combPdf.addPdf(gaus34, "res34");
  std::cout<<"Finish combining pdf set..."<<std::endl;

  combPdf.fitTo(combRes);

  // end Simultaneous fit
}
