#include "TFile.h"
#include "TString.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TCanvas.h"

void GG2(TH1F* h){
  char line[80];
  int i;
  float center, edge, nGood;

  TF1* gg = new TF1("gg", "[0]*exp(-0.5*((x-[1])/[2])^2)+[3]*exp(-0.5*(x/[4])^2)", -800, 800);
  gg->SetParNames ("Constant", "Mean", "Sigma", "Background", "Sigma Bkgnd");
  printf ("11111111111\n");
  center = 0;
  int nbin = h->GetNbinsX();
  for (i=-5; i<=5; i++) center += h->GetBinContent (nbin/2+1+i);
  center /= 10;
  edge=0;
  for (i=nbin/10; i<2*nbin/10; i++)    edge += h->GetBinContent(i+1);
  for (i=8*nbin/10; i<9*nbin/10; i++)  edge += h->GetBinContent(i+1);

  edge /= 2.0*(nbin/10);
  //if (edge<center/10) edge=center/10;
  printf ("center = %f, edge = %f\n", center, edge);
  gg->SetParameters(center-edge, 0, 0.4, edge, 2.4);

  h->Fit("gg", "LQ");
  float error = 1000*gg->GetParameter(2)*sqrt(0.6666666666666);
  //float error = 1000*gg->GetParameter(2)/sqrt(14.0/9.0);
  std::cout<<error<<std::endl;
  sprintf (line, "%6.1f micron", error);
  printf ("Resolution %6.1f microns, ", error);
  TText *text2 = new TText(0.12, 0.75, line);
  text2->SetNDC();
  text2->Draw();

  nGood = sqrt(2*3.141592)*gg->GetParameter(0)*gg->GetParameter(2)/h->GetBinWidth(2);
  printf ("%5.1f %%\n", 100.0*nGood/h->GetEntries());
  //nGood = 0;
  //for (i=75; i<325; i++) nGood+=h->GetBinContent(i+1);
  //printf ("%5.1f \n", 100.0*nGood/h->GetEntries());

  sprintf (line, "%5.1f %% inner", 100.0*nGood/h->GetEntries());
  TText *text2b = new TText(0.12, 0.68, line);
  text2b->SetNDC();
  text2b->Draw();
  //delete gg;
  //delete text2;
  //delete text2b;
}


void ResEval(std::string filename, std::string outdir){
  TFile* file = TFile::Open(filename.c_str(), "READ");

  TH1F *hResIn[4];
  TH1F *hResEx[4];
  TCanvas *cResIn[4];
  TCanvas *cResEx[4];

  char name[15];
  for(int i=0; i<4; i++){
    sprintf(name, "hResIn%d", i+1);
    hResIn[i] = (TH1F *)file->Get(name);
    sprintf(name, "hResEx%d", i+1);
    hResEx[i] = (TH1F *)file->Get(name);
    sprintf(name, "cResIn%d", i+1);
    cResIn[i] = new TCanvas(name, name, 800, 600);
    sprintf(name, "cResEx%d", i+1);
    cResEx[i] = new TCanvas(name, name, 800, 600);
  }

  char line1[80], line2[80], line3[80];
  int nbin;
  float center, edge, mean;
  float error[2][4];
  for(int i=0; i<4; i++){

    /** Inclusive */
    // define double gaussian
    cResIn[i]->cd();
    TF1* ggIn = new TF1("ggIn", "[0]*exp(-0.5*((x-[1])/[2])^2)+[3]*exp(-0.5*(x/[4])^2)", -800, 800);
    ggIn->SetParNames("Constant", "Mean", "Sigma", "Background", "Sigma Bkgnd");
    printf ("================\n");
    // initializing parameters
    center = 0;
    nbin = hResIn[i]->GetNbinsX();
    int  max_bin = 0;
    long max_entry = 0;
    for (int j=0; j<nbin; j++){
      if(hResIn[i]->GetBinContent(j+1) > max_entry){
        max_entry = hResIn[i]->GetBinContent(j+1);
        max_bin   = j+1;
      }
    }
    mean = hResIn[i]->GetBinCenter(max_bin);
    for (int j=-5; j<=5; j++) center += hResIn[i]->GetBinContent(nbin/2+1+j);
    center /= 10;
    edge=0;
    for (int j=3.5*nbin/10; j<4.5*nbin/10; j++)    edge += hResIn[i]->GetBinContent(j+1);
    for (int j=5.5*nbin/10; j<6.5*nbin/10; j++)  edge += hResIn[i]->GetBinContent(j+1);
    edge /= 2.0*(nbin/10);
    //if (edge<center/10) edge=center/10;
    printf ("center = %f, edge = %f, mean = %f\n", center, edge, mean);
    ggIn->SetParameters(center-edge, mean, 0.4, 40, 2.4);
    //ggIn->SetParLimits(3, 0, 0.2*center);
    //ggIn->SetParLimits(0, 0.6*center, 1.2*center);

    // perform fit
    hResIn[i]->Fit("ggIn", "LQ");
    //sprintf (doubleGaus, "%6.1f*exp(-0.5*((x-%6.1f)/%6.1f)^2)+%6.1f*exp(-0.5*(x/%6.1f)^2)", ggIn->GetParameter(0), ggIn->GetParameter(1), ggIn->GetParameter(2), ggIn->GetParameter(3), ggIn->GetParameter(4)); 
    //std::cout<< doubleGaus<<std::endl;
    if(ggIn->GetParameter(0) < ggIn->GetParameter(3)) {
      float signal = ggIn->GetParameter(3);
      float bkg = ggIn->GetParameter(0);
      float signal_sigma = ggIn->GetParameter(4);
      float bkg_sigma = ggIn->GetParameter(2);
      ggIn->SetParameter(0, signal);
      ggIn->SetParameter(3, bkg);
      ggIn->SetParameter(2, signal_sigma);
      ggIn->SetParameter(4, bkg_sigma);
      hResIn[i]->Fit("ggIn", "LQ");
    }

    error[0][i] = 1000*fabs(ggIn->GetParameter(2));
    sprintf (line1, "#sigma_{s}: %6.1f micron", error[0][i]);
    sprintf (line2, "#sigma_{b}: %6.1f micron", 1000*fabs(ggIn->GetParameter(4)));
    sprintf (line3, "#mu_{s}: %6.1f micron", 1000*(ggIn->GetParameter(1)));
    //sprintf (doubleGaus, "%6.1f*exp(-0.5*((x-%6.1f)/%6.1f)^2)+%6.1f*exp(-0.5*(x/%6.1f)^2)", ggIn->GetParameter(0), ggIn->GetParameter(1), ggIn->GetParameter(2), ggIn->GetParameter(3), ggIn->GetParameter(4)); 
    printf ("Resolution %6.1f microns\n", error[0][i]);
    hResIn[i]->Draw();
    TLatex text1;
    text1.SetNDC();
    text1.SetTextSize(0.05);
    text1.DrawLatex(0.15, 0.85, line1);
    text1.DrawLatex(0.15, 0.75, line2);
    text1.DrawLatex(0.15, 0.65, line3);
    std::string plotname = outdir + "/hResIn" + std::to_string(i) + ".png";
    cResIn[i]->SaveAs(plotname.c_str());

    /** Exclusive */
    // define double gaussian
    cResEx[i]->cd();
    TF1* ggEx = new TF1("ggEx", "[0]*exp(-0.5*((x-[1])/[2])^2)+[3]*exp(-0.5*(x/[4])^2)", -800, 800);
    ggEx->SetParNames("Constant", "Mean", "Sigma", "Background", "Sigma Bkgnd");
    printf ("================\n");
    // initializing parameters
    center = 0;
    nbin = hResEx[i]->GetNbinsX();
    for (int j=-5; j<=5; j++) center += hResEx[i]->GetBinContent(nbin/2+1+j);
    center /= 10;
    edge=0;
    for (int j=nbin/10; j<2*nbin/10; j++)    edge += hResEx[i]->GetBinContent(j+1);
    for (int j=8*nbin/10; j<9*nbin/10; j++)  edge += hResEx[i]->GetBinContent(j+1);
    edge /= 2.0*(nbin/10);
    //if (edge<center/10) edge=center/10;
    printf ("center = %f, edge = %f\n", center, edge);
    ggEx->SetParameters(center-edge, 0, 0.4, edge, 2.4);

    // perform fit
    hResEx[i]->Fit("ggEx", "LQ");
    error[1][i] = 1000*fabs(ggEx->GetParameter(2));
    sprintf (line1, "#sigma_{s}: %6.1f micron", error[1][i]);
    sprintf (line2, "#sigma_{b}: %6.1f micron", 1000*fabs(ggEx->GetParameter(4)));
    sprintf (line3, "#mu_{s}: %6.1f micron", 1000*fabs(ggEx->GetParameter(1)));
    //sprintf (doubleGaus, "%6.1f*exp(-0.5*((x-%6.1f)/%6.1f)^2)+%6.1f*exp(-0.5*(x/%6.1f)^2)", ggEx->GetParameter(0), ggEx->GetParameter(1), ggEx->GetParameter(2), ggEx->GetParameter(3), ggEx->GetParameter(4)); 
    printf ("Resolution %6.1f microns.\n", error[1][i]);
    hResEx[i]->Draw();
    TLatex text2;
    text2.SetNDC();
    text2.SetTextSize(0.05);
    text2.DrawLatex(0.15, 0.85, line1);
    text2.DrawLatex(0.15, 0.75, line2);
    text2.DrawLatex(0.15, 0.65, line3);
    plotname = outdir + "/hResEx" + std::to_string(i) + ".png";
    cResEx[i]->SaveAs(plotname.c_str());
  }

  for(int i=0; i<4; i++){
    printf ("Layer %d resolution: %6.1f microns.\n", i, sqrt(error[0][i]*error[1][i]));
  }

  //TCanvas *c_align6 = new TCanvas("c_align6", "c_align6", 800, 600);
  //c_align6->cd();
  //gStyle->SetOptFit(111);
  //gStyle->SetOptStat(false);
  //hAli6->Draw();
  //c_align6->Update();
  //c_align6->Write();


}
