This Repo is derived from an older version of the repo: https://gitlab.cern.ch/atlas-muon-nsw-daq/software/utilities/NSWRead

Hough Transform: NSWHoughTx class defines the necessary tools for performing Hough Transform to reconstruct tracks. GIF-sTGC_HT application in src/apps can be used to execute it.

Usage: GIF-sTGC_HT_ana_New $pdoCalibFile $badChnlFile $Nstrip $sng $calib_meth $min_chan_perclu $clusterType $inputfile

pdoCalibFile=/eos/home-p/patmasid/GIF_TestBeam/Analysis/pdocalib_thres/pdocalib_thres.txt if calib_meth is th

pdoCalibFile=/eos/home-p/patmasid/GIF_TestBeam/Analysis/pdocalib/pdocal50ns_sfrstOFF.txt if calib_meth is tp

badChnlFile=/eos/atlas/atlascerngroupdisk/det-nsw-stgc/testbeam_2021/Analysis/GIFCoinOnly/50ns/sngON_sfrstOFF/0degtilt/Plots/Output_badChannels_srcOFF_sng1_sfrst0_peaktime50.txt

Nstrip=2 # 1 or 2,5,10

sng=1 #or 0

calib_meth=th #or tp or th

min_chan_perclu=3 # 2 or 3

clusterType=1 #0-cog,1-gaus,2-caruana,3-parabola
