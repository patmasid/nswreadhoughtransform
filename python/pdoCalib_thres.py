#!/usr/bin/python
import sys,string,random,os,fileinput,re,argparse
#import numpy as np
import collections
import ROOT
ROOT.gROOT.SetBatch()
import array as arr


class DBIDdetIDMap():
    
    def __init__(self, dbid_detid_txt):
        self.map_dbid_to_detid = dict()
        self.map_detid_to_dbid = dict()
        self.f_dbid_detid_txt = open(dbid_detid_txt,"r")
        
        self.dbid_detid_lines = self.f_dbid_detid_txt.readlines()
        
        self.setDBIDdetIDMap(self.dbid_detid_lines)

    def setDBIDdetIDMap(self, maplines):
    
        for line in maplines:
            
            line.strip("\n")
            contents = line.split()
            tech = int(contents[0])
            sect = int(contents[1])
            eta = int(contents[2])
            rad = int(contents[3])
            layer = int(contents[4])
            end_point = int(contents[5])
            phys_chn = int(contents[6])
            vmm = int(contents[7])
            chn = int(contents[8])
            dbid = int(contents[9])
            
            self.addMapElement(tech, sect, eta, rad, layer, end_point, phys_chn, vmm, chn, dbid)

    def addMapElement(self, tech, sect, eta, rad, layer, end_point, phys_chn, vmm, chn, dbid):
        
        list_channel_info = (rad, layer, end_point, phys_chn)
        self.map_detid_to_dbid[list_channel_info] = dbid
        self.map_dbid_to_detid[dbid] = list_channel_info
        
    def Getdbid(self, radius, layer, end_point, phys_chn):
        
        list_chn_info = (radius, layer, end_point, phys_chn)
        if(list_chn_info in self.map_detid_to_dbid):
            return self.map_detid_to_dbid[list_chn_info]
        else:
            dbid_ret = -1
            return dbid_ret

def arguments():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-input_root_dir",         help="input root file")
    parser.add_argument("-channel_dbid_map",   help="channel spec to dbid mapping txt file")
    parser.add_argument("-output_dir",         help="output directory")
    parser.add_argument("-output_root_name",   help="output root file name")
    parser.add_argument("-output_txt_name",   help="output root txt name")
    return parser.parse_args()

def fatal_error(msg):
    sys.exit("Fatal error: %s" % msg)

def main():
    
    ops = arguments()
    if (not ops.input_root_dir):           fatal("Please give --input_root_dir")
    if (not ops.channel_dbid_map):         fatal("Please give --channel_dbid_map")
    if (not ops.output_dir):               fatal("Please give --output_dir")
    if (not ops.output_root_name):         fatal("Please give --output_root_name")
    if (not ops.output_txt_name):          fatal("Please give --output_txt_name")

    calib_thres(ops.input_root_dir,ops.channel_dbid_map,ops.output_dir,ops.output_root_name,ops.output_txt_name)

def calib_thres(root_file_str,channel_dbid_map,output_dir,output_root_name,output_txt_name):
    root_file = [ROOT.TFile]*6
    thres_vec = ["m10","m05","p00","p05","p10","p15"]
    
    for ithr, thres in enumerate(thres_vec):
        root_file[ithr] = ROOT.TFile(root_file_str+thres+"/output-att10-GIFCoin-sngON-s-"+thres+".allchn_neigh01.root","READ")

    out_txt_file = open(os.path.join(output_dir,output_txt_name),"w")
    out_root_file = ROOT.TFile(os.path.join(output_dir,output_root_name),"RECREATE")

    h_neigh0_list = collections.OrderedDict()
    h_neigh1_list = collections.OrderedDict()
    
    g_Meas_Vs_Applied_thres_list = collections.OrderedDict()
    c_Meas_Vs_Applied_thres_list = collections.OrderedDict()
    
    c_overlay_neigh01_pdo = collections.OrderedDict()

    map_obj = DBIDdetIDMap(channel_dbid_map)
    
    applied_thres = [22.3077, 26.1538, 30, 33.8462, 37.6923, 41.5385]
    #arr_applied = arr.array("d", applied_thres)
    
    for layer in range(4,8):
        print("Layer "+str(layer))
        h_neigh0_list[layer] = collections.OrderedDict() #[ROOT.TH1F]*408
        h_neigh1_list[layer] = collections.OrderedDict() #[ROOT.TH1F]*408
        
        g_Meas_Vs_Applied_thres_list[layer] = collections.OrderedDict()
        c_Meas_Vs_Applied_thres_list[layer] = collections.OrderedDict()
        
        c_overlay_neigh01_pdo[layer] = collections.OrderedDict()

        out_root_file.mkdir("Layer_%d" %(layer))
        
        for ichn in range(408):
            print("Detector Channel: "+str(ichn))
            app_thres = []
            meas_thres = []

            h_neigh0_list[layer][ichn] = collections.OrderedDict()
            h_neigh1_list[layer][ichn] = collections.OrderedDict()
            
            dbid = map_obj.Getdbid(0, layer, 1, ichn)
            print("DatabaseID: "+str(dbid))
            if(dbid == -1):
                print("dbid is -1")
                continue
                
            for ithr, thres in enumerate(thres_vec):
                print("Threshold "+thres)
                h_neigh0_list[layer][ichn][thres] = ROOT.TH1F()
                h_neigh1_list[layer][ichn][thres] = ROOT.TH1F()

                h_neigh0_list[layer][ichn][thres] = root_file[ithr].Get("thresCalib") \
                                                                   .Get("Layer_%d" %(layer)) \
                                                                   .Get("neigh0_pdo_Layer"+str(layer)+"_StripChn"+str(ichn))
                h_neigh1_list[layer][ichn][thres] = root_file[ithr].Get("thresCalib") \
                                                                   .Get("Layer_%d" %(layer)) \
                                                                   .Get("neigh1_pdo_Layer"+str(layer)+"_StripChn"+str(ichn))

                print("h_neigh0_list[layer][ichn][thres].GetEntries(): ", h_neigh0_list[layer][ichn][thres].GetEntries())
                print("h_neigh1_list[layer][ichn][thres].GetEntries(): ", h_neigh1_list[layer][ichn][thres].GetEntries())

                neigh0_up = True
                ibin_trans = -1
                if(h_neigh0_list[layer][ichn][thres].GetEntries()==0 or h_neigh1_list[layer][ichn][thres].GetEntries()==0):
                    print("histograms are empty")
                    continue
                for ibin in range(59,121):
                    if(h_neigh0_list[layer][ichn][thres].GetBinContent(ibin)==0 and h_neigh1_list[layer][ichn][thres].GetBinContent(ibin)==0):
                        continue
                    elif(h_neigh0_list[layer][ichn][thres].GetBinContent(ibin) < h_neigh1_list[layer][ichn][thres].GetBinContent(ibin)):
                        neigh0_up = False
                        ibin_trans = ibin
                        break
                
                if(not neigh0_up):
                    if(meas_thres!=0):
                        app_thres.append(applied_thres[ithr])
                        meas_thres.append(float(ibin_trans-1))
                        if(float(ibin_trans-1) >= 120):
                            print("NOOOOOOO ... Measured threshold is high")
                
            if(len(meas_thres) == 0):
                print(len(meas_thres) == 0)
                continue
            if(len(app_thres) != len(meas_thres)):
                print("Error: len(app_thres) != len(meas_thres)")
                continue
            if(len(app_thres) == 1):
                print("Error: len(app_thres) == 1")
                continue

            print("meas thres: ", meas_thres)
            
            out_root_file.mkdir("Layer_%d/Layer%d_strip%d_dbid%d" %(layer, layer, ichn, dbid))
            
            for ithr, thres in enumerate(thres_vec):
                c_overlay_neigh01_pdo[layer][ichn] = ROOT.TCanvas("c_overlay_neigh01_pdo_thres-%s_Layer%d_strip%d_dbid%d" %(thres, layer, ichn, dbid), "c_overlay_neigh01_pdo_thres-%s_Layer%d_strip%d_dbid%d" %(thres, layer, ichn, dbid), 800, 600)
                c_overlay_neigh01_pdo[layer][ichn].cd()
                h_neigh0_list[layer][ichn][thres].SetLineColor(ROOT.kRed)
                h_neigh0_list[layer][ichn][thres].SetLineWidth(3)
                h_neigh0_list[layer][ichn][thres].Draw("hist")
                h_neigh1_list[layer][ichn][thres].SetLineColor(ROOT.kBlue)
                h_neigh1_list[layer][ichn][thres].SetLineWidth(3)
                h_neigh1_list[layer][ichn][thres].Draw("histsame")

                out_root_file.cd("Layer_%d/Layer%d_strip%d_dbid%d" %(layer, layer, ichn, dbid))
                c_overlay_neigh01_pdo[layer][ichn].Write()

            arr_app = arr.array('d', app_thres)
            arr_meas = arr.array('d', meas_thres)
            g_Meas_Vs_Applied_thres_list[layer][ichn] = ROOT.TGraph(len(app_thres), arr_app, arr_meas)
            g_Meas_Vs_Applied_thres_list[layer][ichn].SetTitle("Measured Pdo Vs Applied mV Threshold")
            g_Meas_Vs_Applied_thres_list[layer][ichn].GetXaxis().SetTitle("Applied Threshold (mV)")
            g_Meas_Vs_Applied_thres_list[layer][ichn].GetYaxis().SetTitle("Measured Threshold (PDO)")
            g_Meas_Vs_Applied_thres_list[layer][ichn].SetMarkerColor(4)
            g_Meas_Vs_Applied_thres_list[layer][ichn].SetMarkerSize(1)
            g_Meas_Vs_Applied_thres_list[layer][ichn].SetMarkerStyle(21)
            
            c_Meas_Vs_Applied_thres_list[layer][ichn] = ROOT.TCanvas("c_calib_curve_Layer%d_strip%d_dbid%d" %(layer, ichn, dbid), "c_calib_curve_Layer%d_strip%d_dbid%d" %(layer, ichn, dbid), 800, 600)
            c_Meas_Vs_Applied_thres_list[layer][ichn].cd()
            g_Meas_Vs_Applied_thres_list[layer][ichn].Draw()
            g_Meas_Vs_Applied_thres_list[layer][ichn].Fit("pol1", "Q");
            fit = g_Meas_Vs_Applied_thres_list[layer][ichn].GetFunction("pol1");
            inter = fit.GetParameter(0);
            slope = fit.GetParameter(1);
            interErr = fit.GetParError(0);
            slopeErr = fit.GetParError(1);
            chi2 = fit.GetChisquare();

            fit.Draw("SAME")
            
            tx = 37.6923
            ty = 42.0

            t = ROOT.TLatex()
            t.SetTextAlign(22)
            t.SetTextColor(ROOT.kBlue)
            t.SetTextFont(43)
            t.SetTextSize(40)
            eq = "y = "+str(slope)+"x + "+str(inter)
            t.DrawLatex(tx,ty,eq)
            
            out_root_file.cd("Layer_%d/Layer%d_strip%d_dbid%d" %(layer, layer, ichn, dbid))
            c_Meas_Vs_Applied_thres_list[layer][ichn].Write()
            
            out_txt_file.write(str(dbid)+"   "+str(slope)+"   "+str(inter)+"\n")
            
    out_root_file.Close()
    out_txt_file.close()
            
        
if (__name__ == "__main__"):
    main()
