# elinksxml.py reads an XML file with elink definitions,
#              prints the information one elink per line
#              uses nswelink from NSWRead to calculate IDs from strings
#              writes an output with detector updated elink numbers 

import xml.etree.ElementTree as ET
import subprocess
from subprocess import Popen, PIPE

tree = ET.parse('mm-elinks-A01.data.xml')
root = tree.getroot()
print "Label    \tFelix \tDet.ID\tDetector String"
for child in root:
    #print(child.attrib)
#    if child.at['class'] == 'SwRodInputLink':
    if child.tag == 'obj':
        #print child.getchildren()[0].attrib
        felix = child.getchildren()[0].attrib['val']
        label = child.attrib['id']
        string = child.getchildren()[2].attrib['val']
        s=subprocess.Popen(['nswelink', string], stdout=PIPE)
        numstring = s.stdout.read().split(":")[0]
        child.getchildren()[1].attrib['val'] = numstring
        print label, "\t", felix, "\t", numstring, "\t", string


    

    
tree.write('output.xml')
