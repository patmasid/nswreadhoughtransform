#!/usr/bin/python
import sys,string,random,os,fileinput,re,argparse
import numpy as np
import collections
#import ROOT
#ROOT.gROOT.SetBatch()
import array as arr

def arguments():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-input_csv_file",         help="input csv file")
    parser.add_argument("-output_dir",             help="outputt directory")
    parser.add_argument("-output_file_name",       help="name of the output file")
    return parser.parse_args()

def fatal_error(msg):
    sys.exit("Fatal error: %s" % msg)
    
def main():
    ops = arguments()
    if(not ops.input_csv_file):        fatal("Please give -input_csv_file")
    if(not ops.output_dir):            fatal("Please give -output_dir")
    if(not ops.output_file_name):      fatal("Please give -output_file_name")
    
    Write_ChanToPadTower(ops.input_csv_file,ops.output_dir,ops.output_file_name)

def Write_ChanToPadTower(input_csv_file,output_dir,output_file_name):
    f_PadTower_Channel_info = open(input_csv_file,"r")
    PadTower_Channel_lines = f_PadTower_Channel_info.readlines()
    
    chn_to_pt_info = collections.OrderedDict()
    
    chn_to_pt_info[0] = collections.OrderedDict()
    for layer in range(4,8):
        chn_to_pt_info[0][layer] = collections.OrderedDict()
        for endpt in range(0,2):
            chn_to_pt_info[0][layer][endpt] = collections.OrderedDict()
            if(endpt == 0):
                chn_to_pt_info[0][layer][endpt][0] = collections.OrderedDict() #pad
                for pad in range(1,97):
                    chn_to_pt_info[0][layer][endpt][0][pad] = []
                chn_to_pt_info[0][layer][endpt][2] = collections.OrderedDict() #wire
                for wire in range(1,33):
                    chn_to_pt_info[0][layer][endpt][2][wire] = collections.OrderedDict()
                    chn_to_pt_info[0][layer][endpt][2][wire]["pt"] = []
                    chn_to_pt_info[0][layer][endpt][2][wire]["wt"] = []
            if(endpt == 1):
                chn_to_pt_info[0][layer][endpt][1] = collections.OrderedDict() #strip 
                for strip in range(0,408):
                    chn_to_pt_info[0][layer][endpt][1][strip] = []
        
    num_pt = len(PadTower_Channel_lines)
    
    pt_to_chn_info = collections.OrderedDict()
    for pt in range(num_pt):
        pt_to_chn_info[pt] = collections.OrderedDict()
        pt_to_chn_info[pt]["pad_chan"] = collections.OrderedDict()
        #pt_to_chn_info[pt]["pt_pos"] = []
        pt_to_chn_info[pt]["strip_min_max"] = collections.OrderedDict()
        pt_to_chn_info[pt]["wire_min_max"] = collections.OrderedDict()
        pt_to_chn_info[pt]["wire_towers"] = []

    for il,line in enumerate(PadTower_Channel_lines):

        line = line.rstrip("\n")
        contents = line.split(",")
        pt_num = int(contents[0])

        pt_to_chn_info[il]["pad_chan"][4] = int(contents[1])
        pt_to_chn_info[il]["pad_chan"][5] = int(contents[2])
        pt_to_chn_info[il]["pad_chan"][6] = int(contents[3])
        pt_to_chn_info[il]["pad_chan"][7] = int(contents[4])
        pt_to_chn_info[il]["pt_pos"] = list(np.float_(contents[5].split(" "))) #float(contents[5].split(" "))
        pt_to_chn_info[il]["strip_min_max"][4] = list(np.int_(contents[6].split("-")))
        pt_to_chn_info[il]["strip_min_max"][5] = list(np.int_(contents[7].split("-")))
        pt_to_chn_info[il]["strip_min_max"][6] = list(np.int_(contents[8].split("-")))
        pt_to_chn_info[il]["strip_min_max"][7] = list(np.int_(contents[9].split("-")))
        pt_to_chn_info[il]["wire_min_max"][4] = list(np.int_(contents[10].split("-")))
        pt_to_chn_info[il]["wire_min_max"][5] = list(np.int_(contents[11].split("-")))
        pt_to_chn_info[il]["wire_min_max"][6] = list(np.int_(contents[12].split("-")))
        pt_to_chn_info[il]["wire_min_max"][7] = list(np.int_(contents[13].split("-")))
        wire_towers_str_raw1 = contents[14][2:]
        print("wire_towers_str_raw1: ", wire_towers_str_raw1)
        wire_towers_str_raw2 = wire_towers_str_raw1[:-1]
        print("wire_towers_str_raw2: ", wire_towers_str_raw2)
        wire_towers_str = wire_towers_str_raw2.split("][")
        print("wire_towers_str: ", wire_towers_str )
        for wt in wire_towers_str:
            l_wt = list(np.int_(wt.split(" ")))
            pt_to_chn_info[il]["wire_towers"].append(l_wt)            
            
    for layer in range(4,8):
        for pt in range(num_pt):
            for pad in range(1,97):
                if(pt_to_chn_info[pt]["pad_chan"][layer] == pad):
                    chn_to_pt_info[0][layer][0][0][pad].append(pt)
            
            for wire in range(1,33):
                if(pt_to_chn_info[pt]["wire_min_max"][layer][0] <= wire and wire <= pt_to_chn_info[pt]["wire_min_max"][layer][1]):
                    chn_to_pt_info[0][layer][0][2][wire]["pt"].append(pt)
                    l_pt_wt = []
                    for wt,wire_tower in enumerate(pt_to_chn_info[pt]["wire_towers"]):
                        if(wire_tower[layer-4]==wire):
                            l_pt_wt.append(wt)
                    chn_to_pt_info[0][layer][0][2][wire]["wt"].append(l_pt_wt)
            
            for strip in range(408):
                if(pt_to_chn_info[pt]["strip_min_max"][layer][0] <= strip and strip <= pt_to_chn_info[pt]["strip_min_max"][layer][1]):
                    chn_to_pt_info[0][layer][1][1][strip].append(pt)

    output_file = open( os.path.join(output_dir,output_file_name),"w" )
    for pad in range(1,97):
        for layer in range(4,8):
            for endpt in range(0,1):
                output_file.write("0,13,0,0,"+str(layer)+","+str(endpt)+","+str(0)+","+str(pad-1)+",")

                for ipt,ptnum in enumerate(chn_to_pt_info[0][layer][endpt][0][pad]):
                    output_file.write(str(ptnum)+" ")
                output_file.write(",\n")

    for wire in range(1,33):
        for layer in range(4,8):
            for endpt in range(0,1):
                output_file.write("0,13,0,0,"+str(layer)+","+str(endpt)+","+str(2)+","+str(wire-1)+",")
                
                for ipt,ptnum in enumerate(chn_to_pt_info[0][layer][endpt][2][wire]["pt"]):
                    output_file.write(str(ptnum)+" ")
                output_file.write(",")
                for ipt,l_wtnum in enumerate(chn_to_pt_info[0][layer][endpt][2][wire]["wt"]):
                    for wtnum in l_wtnum:
                        output_file.write(str(wtnum)+" ")
                    output_file.write("|")
                output_file.write(",\n")

    for strip in range(0,408):
        for layer in range(4,8):
            for endpt in range(1,2):
                output_file.write("0,13,0,0,"+str(layer)+","+str(endpt)+","+str(1)+","+str(strip)+",")

                for ipt,ptnum in enumerate(chn_to_pt_info[0][layer][endpt][1][strip]):
                    output_file.write(str(ptnum)+" ")
                output_file.write(",\n")

    output_file.close()

if (__name__ == "__main__"):
    main()
