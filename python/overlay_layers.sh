#### Calibration using att 1 data
#python3 overlay_layers.py \
#-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt \
#-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt/plots \
#-histname hClusCharge_Seg

#python3 overlay_layers.py \
#-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt \
#-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt/plots \
#-histname hClusCharge_Seg_4of4_

#python3 overlay_layers.py \
#-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt \
#-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt/plots \
#-histname hClusCharge_Seg_1stCalib_

#python3 overlay_layers.py \
#-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt \
#-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt/plots \
#-histname hClusCharge_Seg_4of4_1stCalib_

#### Calibration using att 10 data
#python3 overlay_layers.py \
#-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt \
#-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt/plots \
#-histname hClusCharge_Seg

#python3 overlay_layers.py \
#-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt \
#-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/4ScintCoin/50ns/sngON_sfrstOFF/0degtilt/plots \
#-histname hClusCharge_Seg_4of4_

#### Calibration using att 1 data but using old prachi code not sun's HT
python3 overlay_layers.py \
-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/GIFCoinOnly/50ns/sngON_sfrstOFF/0degtilt \
-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/GIFCoinOnly/50ns/sngON_sfrstOFF/0degtilt/Plots/ClusterChargeDist \
-histname hClusCharge_Seg

python3 overlay_layers.py \
-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/GIFCoinOnly/50ns/sngON_sfrstOFF/0degtilt \
-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/GIFCoinOnly/50ns/sngON_sfrstOFF/0degtilt/Plots/ClusterChargeDist \
-histname hClusCharge_Seg_4of4_
