#!/usr/bin/python
import sys,string,random,os,fileinput,re,argparse
#import numpy as np
import collections
import ROOT
ROOT.gROOT.SetBatch()
import array as arr
ROOT.gStyle.SetOptStat(0)

def arguments():
    
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-input_root_dir",         help="input root file")
    parser.add_argument("-output_dir",             help="output dir")
    parser.add_argument("-histname",               help="name of the histogram to overlay")
    return parser.parse_args()

def fatal_error(msg):
    sys.exit("Fatal error: %s" % msg)
    
def main():
    
    ops = arguments()
    if (not ops.input_root_dir):            fatal("Please give -input_root_dir")
    if (not ops.output_dir):            fatal("Please give -output_dir")
    if (not ops.histname):              fatal("Please give -histname")
    
    overlay(ops.input_root_dir,ops.output_dir,ops.histname)

def overlay(input_root_dir,output_dir,histname):
    
    #att_name = ["att1","att2p2","att3p3","att6p9","srcOFF"]
    #att_dir_name = ["att_1","att_2p2","att_3p3","att_6p9","srcOFF"]
    
    att_name = ["srcOFF","att460","att100","att22","att10","att6p9","att4p6","att3p3","att2p2","att1p5","att1"]
    att_dir_name = ["srcOFF","att_460","att_100","att_22","att_10","att_6p9","att_4p6","att_3p3","att_2p2","att_1p5","att_1"]
    
    bkg_rate = [0,0.028,0.130,0.591,1.3,1.884,2.826,3.939,5.909,8.667,13]
    #bkg_rate = [13,5.909,3.939,1.884,0]
    
    colors = [ROOT.kOrange-3, ROOT.kMagenta, ROOT.kCyan, ROOT.kGray+1, ROOT.kRed, ROOT.kBlack, ROOT.kGreen+2, ROOT.kBlue]
    
    c_hist = collections.OrderedDict()
    h_hist = collections.OrderedDict()

    input_file = collections.OrderedDict()
    
    first_layer = 4
    
    for iatt,att in enumerate(att_name):
        
        print("att: ", att)

        c_hist[att] = ROOT.TCanvas("c_%s_%s" %(att,histname), "c_%s_%s" %(att,histname), 800, 600)
        h_hist[att] = collections.OrderedDict()

        input_file[att] = ROOT.TFile( os.path.join(input_root_dir,att_dir_name[iatt],"condor/output-"+att+"-GIFCoin-sngON.HT_Nstrips5_sng1_res_eff_update1_corrpdo.root"), "READ")

        leg = ROOT.TLegend(0.5,0.7,0.9,0.9)
        
        h_max = 0

        for layer in range(4,8):
            
            h_hist[att][layer] = input_file[att].Get("%s%d" %(histname,layer))
            
            if(h_hist[att][layer].GetMaximum() > h_max):
                h_max = h_hist[att][layer].GetMaximum()

        for layer in range(4,8):
            h_hist[att][layer].GetYaxis().SetRangeUser(0,h_max)
            h_hist[att][layer].GetXaxis().SetRangeUser(0,800)
            h_hist[att][layer].SetLineColor(colors[layer])
            h_hist[att][layer].SetLineWidth(2)
            c_hist[att].cd()
            if(layer == first_layer):
                h_hist[att][layer].Draw("hist")
            else:
                h_hist[att][layer].Draw("histsame")

            leg.AddEntry(h_hist[att][layer], "Layer %d" %(layer), "l")
            
            

        c_hist[att].cd()
        leg.Draw("same")
        
        c_hist[att].Print( os.path.join(output_dir,"c_%s_%s" %(att,histname)+"_GIFCoin.png"))
        
if (__name__ == "__main__"):
    main()
