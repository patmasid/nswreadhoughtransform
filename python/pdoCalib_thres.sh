#python3 pdoCalib_thres.py \
#-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/GIFCoinOnly/50ns/sngON_sfrstOFF/0degtilt/testbeam_2/att_1/att_1_s- \
#-channel_dbid_map /eos/home-p/patmasid/GIF_TestBeam/Analysis/pdocalib/dbid_detid_map.txt \
#-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/pdocalib_thres \
#-output_root_name pdocalib_thres.root \
#-output_txt_name pdocalib_thres.txt

python3 pdoCalib_thres.py \
-input_root_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/GIFCoinOnly/50ns/sngON_sfrstOFF/0degtilt/testbeam_2/att_10/ \
-channel_dbid_map /eos/home-p/patmasid/GIF_TestBeam/Analysis/pdocalib/dbid_detid_map.txt \
-output_dir /eos/user/p/patmasid/GIF_TestBeam/Analysis/pdocalib_thres \
-output_root_name pdocalib_thres_att10.root \
-output_txt_name pdocalib_thres_att10.txt
