#reads a json file and prints out the trimmer and threshold for each channel
import json

import subprocess
from subprocess import Popen, PIPE

with open('full_sector_A13_191_noise_LS_x12_trimmers_2021_01_25_12h00m37s_offset926.json') as f:
  data = json.load(f)


##print(data)
#print (json.dumps(data, indent = 2, sort_keys=True))

for i in data:
    if "MMFE8" in i: 
        sname = "A07_M" + i[5:]
        s=subprocess.Popen(['nswelink', sname], stdout=PIPE)
        numstring = s.stdout.read().split(":")[0]
        #print (i + " " + sname + " " + numstring)
        for v in range(8):
            vname = "vmm" + str(v)
            try:
                thr = data[i][vname]["sdt_dac"]
            except KeyError:
                thr = -1
            #print (vname + " " + str(thr))
            for c in range(64):
                try:
                    trim = data[i][vname]["channel_sd"][c]
                except KeyError:
                    trim = -1
                vmmchn = (v<<29) | (c<<23)
                #print (i + " " + vname + " " + str(c) + " " + str(trim))
                print ("%10d %14s %d %2d %3d %2d" % (vmmchn+int(numstring), sname, v, c, thr, trim))
                
