#!/usr/bin/env tdaq_python

import os
import sys
import eformat
import libpyevent_storage as EventStorage

if __name__=='__main__':

    if len(sys.argv) < 3:
        print ('Usage: %s <infile> <outfile>' % os.path.basename(sys.argv[0]))
        sys.exit(1)


    ins = eformat.istream(sys.argv[1])

    out = eformat.ostream(directory = os.path.abspath(os.path.dirname(sys.argv[2])), 
                          core_name = os.path.basename(sys.argv[2]))



    for e in ins:
        #print ("New Event\n")
        new_event=eformat.write.FullEventFragment()
        new_event.copy_header(e)
        new_event.compression_type(0)

        for rob in e:
            cnt = 0
            if (str(rob.source_id()).find("MUON_STGC")>-1) or (str(rob.source_id()).find("MUON_MMEGA")>-1):
                #print ("SourceID: ", rob.source_id())
                new_event.append_unchecked(rob)
                
        out.write(new_event)
