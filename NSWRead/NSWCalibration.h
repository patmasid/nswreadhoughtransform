/** \file NSWCalibration.h
stores all calibration constants, from gifcalib.cpp etc.
Reads the output of gifcalib.cpp: intercept and slope
*/


#ifndef NSWCALIBRATION_H
#define NSWCALIBRATION_H

#include "NSWSector.h"
 
/** The internal structure for storing calibration data. */
typedef struct CalRecord { // stores calibration results per channel:
  unsigned short inter;         // intercept (to be used as baseline)
  unsigned short slope;         // slope of the line fit
  unsigned char tmin, tmax;     // TDO calibration
  unsigned char status;         // channel status
} TCalRecord;

/** \brief The NSWCalibration class reads, stores, and applies calibration constants.

    Intercept (baseline)  and slope (gain) values are read and stored.
    The data is read from text files written by the gifcalib.cpp program.

    Lookup of constants uses the databaseID of a strip, which makes it
    independent of the FEB output number.

    intercept is stored as float * 100, range 0 to 640.
    slope is stored as float *1000, range 0 to 64.
    tmin and tmax are stored directly as unsigned char.
*/
class NSWCalibration{ 

  private:
  TCalRecord *cal; // first 2M for MM, last 1M for sTGC, 21MB in total.

 public:
  /** Constructor for intialization.
      The intercepts are set to 49 and the slopes are set to 1.09
      The calibration is done using the threshold scan of test beam data.
      The means of slope and intercept came out to be the above ones
   */
  NSWCalibration(); 
  NSWCalibration(char *calmeth);

  /** Destructor */
  ~NSWCalibration(); 
  
  /** Reads all ped run constants from a ped.txt file.
  This reads the standard output of the gifcalib.cpp program
  @param filename the name of the text file.
  @return 1 if there was no error, otherwise 0.
  */
  int ReadPDO (char* filename);
  

  /** Prints all pedestals and noise values for debugging. */
  void Print();


 
  /** This method stores the intercept and slope values in all
      channels of the sector object.
      @param sec a pointer to the sector.
  */
  void Calibrate (NSWSector* sec); 



  TCalRecord * GetRecord (unsigned int databaseID) {
    return &(cal[databaseID]);
  }

  void Print (int sector, int layer, int direction, int channel);

  double GetIntercept (unsigned int databaseID){
    return 0.01*cal[databaseID].inter;
  };

  void SetIntercept (unsigned int databaseID, double b){
    cal[databaseID].inter = 100*b;
  };

  double GetSlope  (unsigned int databaseID){
    return 0.001*cal[databaseID].slope;
  };

  void SetSlope (unsigned int databaseID, double s){
    cal[databaseID].slope = 1000*s;
  };

  unsigned int GetStatus  (unsigned int databaseID){
    return cal[databaseID].status;
  };

  double GetTMin (unsigned int databaseID){
    return  cal[databaseID].tmin;
  };
  double GetTMax (unsigned int databaseID){
    return  cal[databaseID].tmax;
  };
  std::string calib_meth;
  void SetCalibMethod(std::string calmeth){
    calib_meth = calmeth;
  };
}; 


#endif
