/** \file NSWChannelID.h
This is the 23 bit online link ID for NSW data, MM and sTGC
https://espace.cern.ch/ATLAS-NSW-ELX/_layouts/15/WopiFrame.aspx?sourcedoc=/ATLAS-NSW-ELX/Shared%20Documents/DAQ/NSW_GeographicalNames.docx&action=default
It adds the VMM number and VMM channel in the most significant bits to
create a unique 32bit ID for every channel.



   0DDD Teee eESS SSLL LRRR RGGG

D: data type: 0:L1A, 1: config, 2: monitor
T: technology (sTGC = 0, MM = 1) 
e: Endpoint type 0: pad, 1: strip, 2: Trig. Proc, 3: Pad trig, 
E: Eta (endcap) 0: eta=+1: endcap A, 1: eta=-1, endcap C
S: Sector number - 1, so it counts 0 - 15.
L: Layer (0 - 7)
R: Radius: PCB (0 - 15): FFME8 number
G: Channel Group: configured VMM number for output

new format:
https://espace.cern.ch/ATLAS-NSW-ELX/_layouts/WopiFrame.aspx?sourcedoc=/ATLAS-NSW-ELX/Shared%20Documents/DAQ/NSW_Detector_Resource_Names.docx&action=default&DefaultItemOpen=1

dddd dddd VVVr eeeT TTSS SSLL LRRR REEE

d: detector ID:   MM-A=6b, MM-C=6c, sTGC-A=6d, sTGC-C=6e
V: version        0
r: reserved     
e: data type:     L1A=0, Mon=1, toSCA=2, fromSCA=3, TTC=4, L1Ainfo=5, Aux=6
T: resource type: pFEB=0, sFEB=1, TP=2, PadTr=3, L1DDC=4, ADDC=5 Router=6
S: sector - 1:    0..15
R: radius:        sTGC 0..2, MM 0..15
E: elink:         sROC 0..3, Rim-L1DDC: 0,1
*/


#ifndef NSWCHANNELID_H
#define NSWCHANNELID_H
#include "sTGCInput.h"

// numbers from MM parameter book
#define DZ_GAS    5.04
#define DZ_DRIFT 11.284
#define DZ_RO    11.69
#define DZ_SPA   50.0


class NSWChannelID{ 

 private:
  unsigned int channelID; // 32 bit elink ID, old or new format
  unsigned char vmm;      // vmm number 0..7
  unsigned char chn;      // vmm channel 0..63

 public:
  static constexpr double PitchLarge =  0.45;   // in mm
  static constexpr double PitchSmall =  0.425;  // in mm
  static constexpr double PitchsTGC  =  3.2;    // in mm
  static constexpr double WirePitch  =  1.8;    // in mm
  static constexpr double GlobalZsTGCsmall[] = {6993.5, 7004.5, 7015.5, 7026.5,
						 7327.5, 7338.5, 7349.5, 7360.5};
  static constexpr double AveZsTGCsmall = 7177; // in mm
  static constexpr double GlobalZsTGClarge[] = {7457.5, 7468.5, 7479.5, 7490.5,
						 7791.5, 7802.5, 7813.5, 7824.5};
  static constexpr double AveZsTGClarge = 7641; // in mm
  std::vector<float> pad_pos;
  static sTGCInput stgc; 
  /** Constructor for intialization */
  NSWChannelID(unsigned int ID = 0, unsigned char v= 0, unsigned char c = 0); 
  enum ChannelType  {PAD=0, STRIP=1, WIRE=2}; // sTGC


  /** Destructor */
  ~NSWChannelID(); 
  
  /** Get the sector number 1..16 for A, -1..-16 for C
      @return the signed sector number */
  int GetSector();

  /** Set the channelID parts elink, vmm and channel
      @param ID the elink ID
      @param v the VMM number 0..7
      @param c the channel number 0..63 */
  void SetChannelID (unsigned int ID = 0, unsigned char v= 0, unsigned char c = 0);

  /** set the elink parts of the old elink format*/
  void SetChannelID (unsigned dataType, unsigned technology, unsigned endpointType, unsigned eta, unsigned sector0, unsigned layer, unsigned radius, unsigned group);
  
  /** set the elink (old or new format)
      @param ID the elink ID */
  void SetElink(unsigned int ID) {channelID = ID;};

  /** get the elink (old or new format)
      @Return the elink ID */
  unsigned int GetElink() {return channelID;};

  /** @return 1 if it is the new elink format, otherwise 0 */
  bool IsNew() {return (channelID>>24) != 0;}; 

  /** @return the version number of the new ID, or -1 for the old ID */
  int GetVersion() { if(IsNew()) return (channelID>>21)&7; else return -1;};


 /** @return 1 if it is a large sector, otherwise 0 */
  bool IsLarge() {return (GetRawSector() + 1) % 2;};
  

  void SetDataTypeOld(unsigned int t) {channelID = (channelID & 0xFF8FFFFF) | ((t & 7) << 20);};
  /** @return the data type:  0:L1A, 1: config, 2: monitor */
  int GetDataType () {
    if (IsNew()) {
      return (channelID>>17) & 7; 
    } else { 
      return (channelID >> 20) & 7;
    }
  };

  /** print a string with the data type label */
  void GetDataTypeString (char* string);

  void SetTechnologyOld( unsigned int t) {channelID = (channelID & 0xFFF7FFFF) | ((t & 1) << 19);};
  /** @return the detector type: (MM = 1, sTGC = 0) */
  int GetTechnology() {
    if (IsNew()){
      return ((channelID>>24)<0x6d) ? 1:0;
    } else {
      return (channelID >> 19) & 1;
    }
  };

  void SetEndPointOld (unsigned int e) {channelID = (channelID & 0xFFF87FFF) | ((e& 0xf) << 15);};
  /** @return the endpoint type: 0: pad, 1: strip, 2: Trig. Proc, 3: Pad trig */
  int GetEndPoint() {
    if (IsNew()){
      return (channelID >> 14) & 0x7;
    } else {
      return (channelID >> 15) & 0xf;
    }
  };
  
  void SetEtaOld (unsigned int e) {channelID = (channelID & 0xFFFFBFFF) | ((e & 1) << 14);};
  /** @return the Endcap: 0: eta=+1: endcap A, 1: eta=-1, endcap C*/
  int GetEta() {
    if (IsNew()) {
      return (((channelID >> 24) & 1) == 1) ? 0 : 1;
    } else {
      return (channelID >> 14) & 1;
    }
  };

  /** write a string with the detector name and endcap */
  void GetDetectorString (char* string);

  /** @return the detector ID 0x6b .. 0x6e */
  int GetDetectorID(){
    if (IsNew()) {
      return (channelID >> 24) & 0xff;
    } else {
      return 0;
    }
  };


  /** @return the Endcap: 0: eta=+1: endcap A, 1: eta=-1, endcap C*/
  char GetEndcapName() {return (GetEta()==0)?'A':'C';};

  void SetRawSector (unsigned int s) {channelID = (channelID & 0xFFFFC3FF) | ((s & 0xf) << 10);};
  /** @return the 0-based raw sector number*/ 
  int GetRawSector() {
    return (channelID >> 10) & 0xf; // same for old and new format
  };

  void SetLayer (unsigned int l) {channelID = (channelID & 0xFFFFFC7F) | ((l & 7) << 7);};
  /** @return the 0-based layer number */ 
  int GetLayer() {
    return (channelID >> 7) & 7; // same for old and new format
  };

  void SetRadius (unsigned int r) {channelID = (channelID & 0xFFFFFF87) | ((r & 15) << 3);};

  /** @return the board number 0-2 for sTGC, 0-15 for MM */
  int GetRadius() {
    return (channelID >> 3) & 15; // same for old and new format
  };

  /** @return the PCB number 1 - 8 for MM */
  int GetPCB() {return 1 + (GetRadius() / 2);};

  /** @return the z position of the center of the gas gap */
  double GetZPosition();


  /**@return the z position of the center of the gas gap
  @param layer the layer number
  @param isBig true for large chambers
  @param tech  (MM = 1, sTGC = 0) */
  static double GetZPosition(int layer, bool isBig, int tech);


  
  /** @return the config vmm id based on the sROC capture vmm id */
  int GetConfigID(){
    switch (vmm){
    case 2: return 0; break;
    case 3: return 1; break;
    case 0: return 2; break;
    case 1: return 3; break;
    case 5: return 4; break;
    case 4: return 5; break;
    case 6: return 6; break;
    case 7: return 7; break;
    default:  printf ("INVALID VMM ID %d\n", vmm);
    }
    return 0;
  }
   
    
  
  void SetChannelGroup (unsigned int g) {channelID = (channelID & 0xFFFFFFF8) | (g & 7);};
  /** @return the ChannelGroup 0 - 7 */
  int GetChannelGroup() {
    return channelID & 7; // same for old and new format
  };
 

  void SetVMMChannel (unsigned int c) {chn = c;};
  /** @return the 0-based channel number of the VMM*/ 
  int GetVMMChannel() {return chn;};

  void SetVMM (unsigned int v) {vmm = v;};
  /** @return the chip number of the VMM*/ 
  int GetVMM() {return vmm;};


  /** @return the string representation of the Board_ID */
  void GetBoardID(char* string);

  /** @return the string representation of the link ID */
  void GetString(char* string);

  /** @return the string representation of the end point */
  void GetEndPointString(char* string);

  /** parse the linkID string representation
      @par string the string representation of th elinkID
      @return 0 if the strign was successfully parsed */
  int ParseString(char* string);

  /** parse the BoardID string without the sector name.
      @par string the BoardID
      @par sector the signed sector number, negative for endcap C
      @return 0 if the string was successfully parsed 
  */
  int ParseBoardID(char* string, int sector);

  /** parse the BoardID string, including the sector name.
      @par string the BoardID
      @return 0 if the string was successfully parsed 
  */
  int ParseBoardID(char* string);

  /** @returns the 32 bit online channel ID. 
      Use GetSector(), GetLayer(), etc. 
      for easier access to the ID information.
  */
  int GetChannelID() {return channelID;};

  /** @returns the strip (wire, or pad) number on the MM or sTGC detector 
      This number starts at 0 at the inner radius.
  */
  int GetDetectorStrip();
  
  /** @returns the strip position on the MM or sTGC detector 
      The unit is mm.
  */
  double GetStripPosition();
  double GetStripPosition(int chn);

  double GetWireCenterPosition();

  double GetWireGroupHalfWidth();

  double GetWireUpperEdge();

  double GetWireLowerEdge();


  std::vector<float>& GetPadPosition();
  

  /* @return an index for sorting linkIDs accroding to layer and radius
   */
  int GetIndex();

  
  /** provides the upper channel ID without chan number and vmm number
   */
  int GetLinkID() {return channelID & 0x7FFFFF;};

  /** @return the stereoangle of the layer, or 0 for eta layers */
  float GetStereoAngle();

  /** @return the MM strip length in mm (for eta strips) */
  float GetStripLength();
  
  /** Provides the pitch value of the strips in mm for MM chambers.
      @return the strip pitch in mm.
  */
  double GetPitch ();

  /** @return the type of the chamber, either 'P' for pivot or 'C' for confirm
   */
  char GetPivotConfirm();

  /** @return the type of the sTGC channel, PAD=0, STRIP=1, WIRE=2 */
  ChannelType GetChannelType() {
    if (GetEndPoint() == 1) return STRIP;
    if (GetConfigID() == 0) {
      return WIRE;
    } else {
      return PAD;
    }
  }


  /** @return a key for the calibration database */
  unsigned int GetDatabaseID();

  
  /** print the channel ID in readable format */
  void Print();

};

#endif
