/**  \file NSWEvent.h
Reads and stores one NSW event.
*/

#ifndef NSWEVENT_H
#define NSWEVENT_H

/**\mainpage
    The NSWRead package reads NSW rawdata from ATLAS events.

This package consists of the core reading and data storage classes:
NSWEvent, NSWChamber, NSWPacket and NSWChannel,
as well as some analysis-related classes: NSWHit, NSWHitList, and 
CalServer.

Also inluded are some application programs that use these classes
to fill root trees and histograms. Of general interest is ped.cpp for
the analysis of pedestal and noise values and moni.cpp for monitoring
hit information. Long tracks through all 4 chambers of the cosmic stand 
are fitted in long.cpp. Amplitudes are fitted in the amp.cpp application,
with the goal to correct the parabolic interpolation.

There is a Makefile to build the applications. For instance, 
<pre>
   make all
</pre>
compiles and links all  applications.

For questions and suggestions, contact schernau at uci dot edu.
*/
#include <vector>

#include "NSWSector.h"
#include "NSWReadException.h"


//#define TRUE 1
//#define FALSE 0;

#define MAX_STATUS 100 // max number of status elements 



/**
   \brief
This class desribes one event, consisting of an array of readout buffers 
(ATLAS event ROD fragments) called NSWSector. 

Reading is done in 2 steps: First the event is read into a buffer, then
the buffer is processed and the internal data structures are filled.

Access to the data is through the NSWSector objects. 
Starting form 0, here are GetNumSector() sectors to loop over.


Reconstruction results, however, are not part of the Event class.
@see NSWHit, NSWHitList.


*/
class NSWEvent{ 

  private:
  std::vector<NSWSector*> sector; // fragments with EE1234EE header

  int globalID;                  // ATLAS event number
  int dateTime;                  // time and date of run
  int nano;                      // nanoseconds of time stamp
  unsigned int runNumber;        // run number
  unsigned int lumiBlock;        // Lumi block 
  int BCID;                      // from ATLAS header
  int L1ID;                      // from ATLAS header
  unsigned int * pAA1234AA;      // pointer to full atlas event header
  int numStatus;
  
 public:
  NSWEvent();  // constructor for intialization
  ~NSWEvent(); // destructor

  /** delete any previous data */
  void Clear();  

  int ReadATLAS (FILE* infile, unsigned int * buf, int maxSize);
  int ReadNSW(unsigned int * source, int size);   // read one event from buffer
  int ReadGnam (std::vector<const uint32_t *> rods, std::vector<unsigned long int> sizes);


  /** @return  the number of sectors */
  int GetNumSector () const {return sector.size();};

  /** Provides access to the event data:
      @param i is the sector number, ranging from 0 to GetNumSector()-1.
      @return a pointer to the chamber object, or NULL if i is out of range.
      The user should test the pointer to make sure that the pointer 
      is not NULL before using the it.
  */
  NSWSector* GetSector(int i) const {return sector[i];};



  /** Store the event in a memory buffer.
      This is used for writing simulated events out to a file.
      Only the first 2 chambers are written.
      This function is analogous to  rod->ReceiveBuffer(...)
      @param buf The buffer location to store to.
      @return The number of words stored.
      @note The L1ID can later be overwritten with SetLevel1ID().
  */
  int Store( unsigned int * buf);

  /** Simulate an event.
      @param channelID The channelID of the first channel of layer 0 X.
   */
  void Simulate(TAtlasHeader h, unsigned int channelID[2]);

 

  /** retrieve the date and time of the event as an integer.
      @note cast to time_t and use char* ctime(const time_t* timer) for strings.
   *@return the date and time as integer number of seconds since 1970
   */
  int GetDateTime() const {
    return dateTime;
  };

  /** retrieve the nanoseconds of the date and time of the event as an integer.
   *@return the nanoseconds of date and time as integer 
   */
  int GetNano() const {
    return nano;
  };

  /** retrieve the global event number of the event in this run
   *@return the event number from the top-level atlas header
   */
  int GetGlobalID() const  {
    return globalID;
  };


  /** retrieve the BCID of the event from the ATLAS header
   *@return the  BCID
   */
  unsigned int GetBCID() const {
    return BCID;
  };

  /** retrieve the L1ID of the event from the ATLAS header
   *@return the  L1ID
   */
  unsigned int GetL1ID() const { 
    return L1ID;
  };

  /** retrieve the run number of the event from the ATLAS header
   *@return the  run number
   */
  unsigned int GetRunNumber() const {
    return runNumber;
  };

  /** retrieve the LumiBlock of the event from the ATLAS header
   *@return the  LumiBlock
   */
  unsigned int GetLumiBlock() const {
    return lumiBlock;
  };


  void FixGlobalID (int id);
  
  /**  prints information about the event */
  void Print() const; 
}; 

#endif // NSWEVENT_H
