/** @file NSWPacket.h
 reads and stores channels from one elink.
 There should be 8 VMM per e-link for MM
*/


#ifndef NSWPACKET_H
#define NSWPACKET_H

#include "NSWChannel.h"
#include <vector>

/**
   The NSWPacket class represents data of a continuous group of
   channels from the elink. Chip boundaries can split physical clusters. 

   The group has a start channel and a width.

   Internally, the data is stored as an array of NSWChannel() objects, 
   beginning with an index of 0 for the leftmost strip. To get
   the physical channel number, add GetStartChannel() to the index.

   To access the data of one strip, first find out how many stips
   are stored in this cluster by calling GetWidth() and then
   obtain a pointer to a channel object with GetChannel(). 

   The cluster stores the channelID of the first strip.
   This ID is used for the layer, endcap and PCB information.

*/
class NSWPacket : public NSWChannelID { 
  
 private:
  int linkID;                 // ID from Felix
  int linkBytes;              // number of bytes in this packet from FELIX
  int width;                  // number of channels in this packet
  //NSWChannel ** channel;      // array of channel pointers
  std::vector<NSWChannel*> channel;
  unsigned int headerType;    // first 2 bits
  unsigned char ROCID;
  unsigned short L1ID;
  unsigned short BCID;  // BCID in lower 12 bit, orbit in bit 12 and 13
  bool extended;
  bool hasTDO;          // true for MM, flase for sTGC
  unsigned short missing; // missing VMM in trailer
  unsigned char L0ID;
  unsigned short length; // number of channels in trailer
  unsigned char checksum;
  unsigned char timeout;
  unsigned int runningSum; // checksum
  
 public:
  /** Constructor */
  // NSWPacket(unsigned int upperID=0, unsigned char wid=0);
 
  NSWPacket(unsigned int* source); 
  /** Destructor */
  ~NSWPacket(); //destructor
  void DeleteChannels();
  int GetLinkID(){ return linkID;};
  int GetLinkBytes() { return linkBytes;};
  void SetLinkID(int id){ linkID=id;};
  void SetLinkBytes(int bytes) { linkBytes=bytes;};
  int GetWordLength() {return (linkBytes+3)/4;};

  /** @return the 0-based channel number of the first channel in
      this cluster.
  */
  //int GetStartChannel() {return channel[0]->GetChannel();};

  bool GetHasTDO() {return hasTDO;};
  void SetHasTDO(bool b) {hasTDO = b;};
  void SetExtended(bool b) {extended = b;};
  bool GetExtended() {return extended;};
  
  unsigned char GetChecksum() {return checksum;};
  void SetChecksum(unsigned char c) {checksum = c;};
  
  /** @return the number of channels in this cluster. 
      Use this instead of GetLength() because the length can be corrupted.*/
  int GetWidth() {return width;};

  /** Provides acces to the data of this cluster.
      @param i is the strip number, ranging from 0 to GetWidth()-1.
      @return the NSWChannel() object containing the data for this
      channel, or NULL if i is out of range.
  */
  NSWChannel* GetChannel(int i);

  
  /** @return the calculated the checksum. It should be zero.
   */
  unsigned int TestChecksum() { return runningSum;};
  
  /** Provides acces to the data of this cluster.
      @param i is the channel number, ranging from 0 to GetWidth()-1.
      @param ch is the NSWChannel() object
  */
  void SetChannel(int i, NSWChannel* ch);

  /** @return the ROC ID for Null Events */
  unsigned char GetRocID() {return ROCID;};

  /** @return the L1 ID */
  int GetL1ID() {return L1ID;};

  /** @return the BCID and orbit number 
      BCID in lower 12 bit, orbit in bit 12 and 13 */
  unsigned short GetBCID () {return BCID;};

  /** @return the orbit number */
  unsigned short GetOrbit() {return (BCID>>12)&3;};

  /** Gets missing VMM bitfield from the trailer
      @param mask true to ignore the VMM chips that are not installed on sFEB6
      @return missing VMMs bitfield, 0..255 */
  int GetMissing(bool mask = false);

  /** Gets the header type 0-3: MM, Null, sTGC, Illegal
      @return the headerType */
  int GetHeaderType() {return headerType;};

  /** Gets the header type 0-3: MM, Null, sTGC, Illegal
      @return the headerType */
  bool IsNull() {return headerType==1;};

  /** Gets the L0ID from the trailer
      @return the L0ID */
  int GetL0ID(){ return L0ID;};

  /** Gets the packet length from the trailer
  @return the number of channels in trailer */
  int GetLength() {return length;};
 
  /** Gets the checksum from the trailer
      @return the checksum from the trailer */
  int  Gethecksum() {return checksum;};

  /** Gets the timeout flag from the trailer
      @return the timeout flag */
  int GetTimeout() {return timeout;};


  /** Sets the ROC ID in the trailer (only for NULL events)
      @param id is the ROC ID for Null Events */
  void SetRocID(unsigned char id) {ROCID = id;};

  /** Sets the L1ID for the header
      @param id is the the L1 ID */
  void SetL1ID(unsigned short id) {L1ID = id;};

  /** Set the BCID and orbit number 
      @param id the BCID
      @param orbit the orbit number (2 bits) */
  void SetBCID (unsigned short id, unsigned short orbit=0) {BCID = ((orbit & 3)<<12) | (id & 0xfff);};

  /** Sets the orbit number 
      @param o the orbit number (2 bits) */
  void SetOrbit(unsigned char o) {BCID = (BCID & 0xfff) | (o & 3)<<12;};


  /** Sets the trailer length 
      @param len the number of channels, should be equal to width */
  void SetLength(unsigned char len) {length = len;};

  /** calculate the number bytes
      @return the number of 8 bit bytes in the packet */
  int GetNumBytes();

  /** check the packet BCID against a reference BCID
      @param refBCID the reference BCID
      @return true if it matches, otherwise false */
  bool CheckBCID(int refBCID){
    if (IsNull()) return true; // null packets have no BCID
    return (BCID == refBCID);
  };

  /** check the packet L1ID against a reference L1ID
      @param refL1ID the reference L1ID
      @return true if it matches, otherwise false */
  bool CheckL1ID(int refL1ID){
    if (IsNull()) {
      //printf ("Null: %6d %6d   0x%2X   0x%2X\n", L1ID, (refL1ID & 0xFF), L1ID, (refL1ID & 0xFF));
      return (L1ID == (refL1ID & 0xFF));
    } else {
      //printf ("Full: %6d %6d 0x%04X 0x%08X\n", L1ID, (refL1ID & 0xFFFF), L1ID, refL1ID);
      return (L1ID == (refL1ID & 0xFFFF));
    }
  };
      
  
  /** Checks the trailer length against the packet length
      @return true if it matches, otherwise false */
  bool CheckLength() {return (length == width);};

  /** Checks the packet sice to be a multiple of 4 bytes and reasonable
      @return true if the size is valid, otherwise false */
  bool CheckSize() {
    if (linkBytes == 10) return true;        // null packet
    if (linkBytes < 10) return false;        // too short
    if (linkBytes > 4*(512+4)) return false; // too long
    if (linkBytes%4 != 0) return false;      // corrupted
    return true;                             // passed
  };

  /** Print some data to the screen. Used for debugging. */
  void Print();

  /** count the number of channels with parity errors 
      @return the  number of channels with parity errors 
  */
  int GetParityErrors();

  
  /** Perform some tests for consistency */
  int Test(int verbose = 0);

}; 

#endif
