#include <vector>
#include <algorithm>
#include "NSWSector.h"
#include "NSWCluster.h"
#include "NSWClusterList.h"

#ifndef NSWsTGCGeometry
#define NSWsTGCGeometry

//-------------------------------------------------------------//
//  Class which stores all hits and geometry of a sTGC Quad
//-------------------------------------------------------------//

class NSWsTGCGeometry{

  private:

  std::vector<std::pair<float, float>> pad_tower_position;

  std::vector<NSWsTGCWireTower*> overlapping_wire_towers; // wire tower numbers
  
  std::vector<std::vector<NSWsTGCStrip*>> overlapping_strips; // strips overlapping per layer
  std::vector<std::vector<NSWsTGCWire*>>  overlapping_wires;  // wires overlapping per layer

  std::vector<NSWsTGCPads*>  overlapping_pads; // [layer] pads per layer in tower
  std::vector<int> start_overlap_strip_number; // [layer] start strip number in each layer
  std::vector<int> last_overlap_strip_number;  // [layer] end strip number in each layer

  std::vector<std::vector<NSWCluster*>> overlapping_strip_clusters; //[layer][cluster]

  std::vector<std::vector<int>> Filled_Pad_Tower_Index; //
  std::vector<std::vector<int>> ROI_Pad_Tower_Index; // pad towers with significant activity

  //--------------------------------------------------------//
  //    Stored Geometry of rows of pad towers
  //--------------------------------------------------------//
  
  // Map_PadTowerRow_Position.at(ilayer).at(iPadTowerRow) =  std::pair< start pos R, end pos R > of row of pad tower
  std::vector< std::vector< std::pair<float, float> >> Map_PadTowerRow_Position; 
  // Map_PadTowerRow_TowerIndex.at(ilayer).at(iPadTowerRow) =  vector of pad towers index in the pad tower row
  std::vector< std::vector< std::vector<int> >>        Map_PadTowerRow_TowerIndex;
  
  //--------------------------------------------------------//
  //    Maps of which pad/wire associate with which tower
  //--------------------------------------------------------//

  // Map_PadNum_AssociatedPadTowerIndex.at(ilayer).at(ipad) = vector of associated pad tower indexes
  std::vector< std::vector< std::vector<int> >> Map_PadNum_AssociatedPadTowerIndex; // [layer][pad][pad_tower#]
  // Map_PadNum_AssociatedPadTowerIndex.at(ilayer).at(ipad) = vector of associated wire tower indexes
  std::vector< std::vector< std::vector<int> >> Map_PadNum_AssociatedWireTowerIndex; // [layer][pad][wire_tower#]

  // Map_PadNum_AssociatedWireTowerIndex.at(ilayer).at(iwire) = vector of associated pad tower indexes
  std::vector< std::vector< std::vector<int> >> Map_WireNum_AssociatedPadTowerIndex; // [layer][wire][pad_tower#]
  // Map_PadNum_AssociatedWireTowerIndex.at(ilayer).at(iwire) = vector of associated wire tower indexes
  std::vector< std::vector< std::vector<int> >> Map_WireNum_AssociatedWireTowerIndex; // [layer][wire][wire_tower#]

  //--------------------------------------------------------//
  //     Stored list of hits and clusters [layer][channel]
  //--------------------------------------------------------//

  std::vector< std::vector< NSWChannel* >> pad_hits;  // vector of pad hits
  std::vector< std::vector< NSWChannel* >> wire_hits; // vector of wire hits
  
  static const int maxStripNum = 408*3; // I forget max number of strips;

  NSWChannel * strip_channels[8][maxStripNum]; // store map of strips

  //--------------------------------------------------------//
  //                Stored List of pad/wire towers
  //--------------------------------------------------------//

  std::vector< NSWsTGCPadTower*  > pad_towers;  // towers of pads 
  std::vector< NSWsTGCWireTower* > wire_towers; // towers of wires

  //--------------------------------------------------------//

  static const int TypePad   = 0;
  static const int TypeStrip = 1;
  static const int TypeWire  = 2;

  static const int TypePFEB = 0;
  static const int TypeSFEB = 1;

  static const int TypeClCoG = 0;
  static const int TypeClGauss = 1;
  static const int TypeClCaruana = 2;
  static const int TypeClParabola = 3;

  int nMaxPad;


  public:

    NSWsTGCGeometry();
    ~NSWsTGCGeometry();

    // read in data, fill pad, wire, strip hits, fill pad tower, wire tower, strip clusters
    void NSWsTGCGeometry::ReadData(NSWSector* sec);

    // put pad and wire hits into pad towers
    void NSWsTGCGeometry::Fill_PadTower( NSWChannel* chn );

    void FillPadTowerGeometry(TString filename);
    void FillWireTowerGeometry(TString filename);
    
    void FillPadWireStripHits(NSWSector* sec);

    void FindStripClusters();

    void MatchPadTower(NSWSector* sec);

    void Fill(NSWSector* sec);

    void Print();
    
    NSWChannel* GetChannel(int layer, int channel){ return wires[layer][channel]; };

    int GetNumClusterList() {return clusterList.size();};

    NSWClusterList* GetClusterList(int i){
      return clusterList.at(i);
    };

    //int GetNumClusters (int layer) {return cluster[layer].size();};

    std::vector<NSWChannel*> GetPadTower(int i){ return padTower_list.at(i); };

    int GetNumPadTower(){ return padTower_list.size(); };

    NSWChannel* GetRawPad(int layer, int channel){ return pads[layer][channel]; };

    NSWChannel* GetRawWire(int layer, int channel){ return wires[layer][channel]; };
};




#endif
