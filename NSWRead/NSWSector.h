/** \file NSWSector.h
 represents one chamber: one sector of one technology
 TODO: 4 or 8 layers?
*/
#include <vector>
#include "NSWPacket.h"
//#include "NSWCluster.h"


#ifndef NSWSECTOR
#define NSWSECTOR


#define MAX_STATUS 100 // max number of status elements 

/** This is the standard ATLAS header structure for ROD fragments.
This typedef is used as an internal variable in the NSWSector class.
@see NSWEvent().
 */
typedef struct AtlasHeader{
  /** This is 0xee1234ee for ROD fragments. */
  unsigned int marker;

  /** The size of this structure is 9 words. */
  unsigned int size;

  /** The major version is in the most significant half word, 
      and the minor version is in the least significant half word. */
  unsigned int version;

  /* The source ID is 0x6B0000 .. 0x6EFFFF for the NSW.
     The 2least significant bytes are the sector number, or TP data if >16. */
  unsigned int sourceID;

  /** Run number */
  unsigned int runNumber;

  /** Event number */
  unsigned int level1ID;

  /** Beam crossing ID */
  unsigned int BCID;


  /** Type information from the TIM */
   unsigned int triggerType;

  /** The event type summarizes NSW configuartion parameters. 
      TBD*/
  unsigned int eventType;
} TAtlasHeader;

/** This is the standard ATLAS trailer for ROD fragments.
This typedef is used as an internal variable in the NSWSector class.
@see NSWEvent().
 */
typedef struct AtlasTrailer {
  /** number of status words */
  unsigned int numStatus;

  /** number of data words in the fragment */
  unsigned int numData;

  /** Status before or after the data */
  unsigned int StatusPosition;
}TAtlasTrailer;


/** This is the standard ATLAS header structure for ROB fragments.
This typedef is used as an internal variable in the NSWSector class.
@see NSWEvent().
 */
typedef struct ROBHeader{
  /** This is 0xDD1234DD for ROB fragments. */
  unsigned int marker;

  /** The size of the ROB fragment in words. */
  unsigned int fragmentSize;

  /** The size of the ROB header in words. */
  unsigned int headerSize;

  /** The major version is in the most significant half word, 
      and the minor version is in the least significant half word. */
  unsigned int version;

  /* The source ID is 0x6B0000 .. 0x6EFFFF for the NSW.
     The 2least significant bytes are the sector number, or TP data if >16. */
  unsigned int sourceID;

  /** Number of status elements */
  unsigned int numStatus;

  /** Status elements, normally there is only one */
  unsigned int status[10];

} TROBHeader;

/**
   The NSWSector class holds the event data for one sector of one technology.
   this is a readout buffer ROD fragment, so it includes the ATLAS header
   and trailer and status words.
   The VMM data is stored in NSWPacket objects and the Sector 
   holds arrays of Packets for each layer. 

   To access the data of a specific layer, 
   1. Find out how many clusters exist with GetNumPackets()
   2. Get a pointer to a specific cluster with GetPacket()
   @see NSWPacket().
*/  
class NSWSector{ 

 private:
  TAtlasHeader atlasHeader;  // ROD header ee1234ee
  TROBHeader   robHeader;    // ROB header dd1234dd
  TAtlasTrailer atlasTrailer;
  unsigned int atlasStatus[10];
  unsigned int robSize; // fragment size from ROBin header
  unsigned int rosStatus;

  /** packets per sector/layer */
  unsigned int numPacket[8]; 
  /** vector of packets */
  std::vector<NSWPacket*> packet;
  /** number of data words in all packets of this sector*/
  unsigned int numData;        
  /** empty dummy event from event builder */
  int isDummy;
  /** sector number, negative for endcap C, where eta=-1. */                 
  int sector;
  /** technology, 0 = MM, 1=sTGC */
  int technology;
  
  /** number of channels in this sector */
  int numChannels;

  /** vector of clusters */
  //  std::vector<NSWCluster> cluster;
  
 public:
   
  /** Constructor for intialization: this produces an empty chamber. */
  NSWSector(); 
  

  /** Destructor */
  ~NSWSector(); 
  
  /** delete any previous data */
  void Clear();

  /** link lock status */
  //  NSWLinkStatus linkStatus;
  
  
  /** Reads a sector fragment from a buffer.
      This is only used in the NSWEvent class.
      @param source the address from which to read
      @return number of words read if reading went ok, -1 otherwise.
  */
  int Read(unsigned int * source);    
  
  
  /** Reads a sector fragment from a buffer.
      This is only used in the NSWEvent class.
      @param source the address from which to read
      @param size the number of words in source
      @return number of words read if reading went ok, -1 otherwise.
  */
  int ReadGnam(const unsigned int * source, unsigned long int size);    
  
  
 
  /** Provides access to the packets.
     @param i the packet number: 0 to GetNumPacket().
     @return The packet pointer, or NULL if i is out of range.
     @see GetNumPacket()
  */
  NSWPacket* GetPacket(int i);



  /** Provides the number of packets 
      @return the number of packets in this sector.
  */
  int GetNumPacket () {return packet.size();};




  /** @return TRUE if the ROD fragment was a dummy from the event builder
   */
  bool IsDummy() { return isDummy;};


  /** set the sector number
      this is negative for side C, where eta=-1.
      @param sourceID the sourceID for the ROD header
  */
  void SetSector(unsigned int sourceID);

  /** get the sector number
      this is negative for side C, where eta=-1.
      @return the sector number of this chamber
  */
  int GetSector();

  /** Store the chamber in a memory buffer.
      This is used for writing simulated events out to a file.
      This function is called by NSWEvent.Store().
      @param buf The buffer location to store to.
      @return The number of words stored.
  */
  int Store( unsigned int * buf);

  /** Create 4 X and 4 Y clusters with all channels.
      @param channelID The channelID of the first channel of layer 0 X.
      @param moduleID The ID of the RPU, wither 5 or 11 */
  void Simulate(unsigned channelID, int moduleID=0);

  /** @return the total number of strips in this chamber
   */
  int GetNumStrips();

  /** @return the  number of active strips in this chamber with max-min>amp
   */
  int GetActiveStrips(int amp);

  /** @return returns the source ID number from the atlas header.*/
  unsigned int GetSourceID() {return atlasHeader.sourceID;};

  /** @param i the number of the status word, 
      @return the StatusElement of the rod
  */
  unsigned int GetStatusElement (int i=0) {
     return atlasStatus[i];
  };

  /** get the triggerCalibrationKey
      @return the triggerCalibrationKey or -1 if it does not exist
  */
  int GetTriggerCalibrationKey () {
    if ((atlasHeader.version & 0xffff) < 0x0102) return -1;
    if ((atlasHeader.eventType & 1) == 0) return -1;
    return atlasStatus[4];
  };


  /** @return the number of StatusElements of the rod
  */
  unsigned int GetNumStatus() {
    return atlasTrailer.numStatus; 
  };



  /**  @return the number of StatusElements */
  unsigned int GetRosStatus() {
    return rosStatus;
  };

  /** @return the trigger type from the atlas header. */
  unsigned int GetTriggerType() {return atlasHeader.triggerType;};

  /** @return the detector event type from the atlas header. */
  unsigned int GetEventType() {return atlasHeader.eventType;};

  /** @return the Beam Crossing ID number from the atlas header. */
  unsigned int GetBCID() {return atlasHeader.BCID;};

  /** @return returns the version from the atlas header */
  unsigned int GetVersion() {return atlasHeader.version;};

  /** @return returns the run number from the atlas header */
  unsigned int GetRunNumber() {return atlasHeader.runNumber;};

  /** @return returns the Level 1 ID number from the atlas header.
   */
  unsigned int GetLevel1ID() {return atlasHeader.level1ID;};


  /** @return the technology, 1 = MM, 0=sTGC */
  int GetTechnology(){ return technology;};
  
  /** @return  the position of the status word. */
  unsigned int GetStatusPosition() {return atlasTrailer.StatusPosition;};

  /** @return  the number of data words from the ATLAS trailer. */
  unsigned int GetNumData() {return atlasTrailer.numData;};

  TAtlasHeader GetAtlasHeader() { return atlasHeader;};
  TROBHeader GetRobHeader() { return robHeader;};
  unsigned int GetRobStatus() { return robHeader.status[0];};
  TAtlasTrailer GetAtlasTrailer() { return atlasTrailer;};

  /** @return the total number of channels in this sector.
   */
  unsigned int GetNumChannels() {return numChannels;};


  /** Prints some information about the event.
      Used for debugging.
  */
  void Print(); // prints information
}; 

#endif
