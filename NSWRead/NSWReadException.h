/** file NSWReadException.h
    defines exceptions for the NSW data reading code
*/

#ifndef NSWREADEXCEPTION_H
#define NSWREADEXCEPTION_H

#include <iostream>  // IWYU pragma: keep
#include <exception>

#define SEV_INFO 0
#define SEV_WARN 1
#define SEV_ERROR 2
#define SEV_FATAL 4

/*
class NSWReadException : public std::exception{
public:
  NSWReadException(const std::string &file, unsigned int line, const std::string &message = ""):file(file), line(line), message(message){}
virtual
  ~NSWReadException() throw() {printf("destru\n");}

 public:
  std::string file, message;
  unsigned int line;
};

*/


class NSWReadException : public std::exception
{
private:
    std::string file, message;
    unsigned int line;
    char txt[200];
    int severity;

public:
    NSWReadException(const std::string &file, unsigned int line, const std::string &message = "", int severity = 0);
    NSWReadException(const std::string &message = "");
    virtual ~NSWReadException() throw();
    virtual const char * what() const throw();
    int IsInfo()    {return (severity==SEV_INFO);};
    int isWarning() {return (severity==SEV_WARN);};
    int isError() {return (severity==SEV_ERROR);};
    int isFatal() {return (severity==SEV_FATAL);};
    int GetSeverity() {return severity;};

};
// Macro for throwing the exception with current line number and file: 
#define THROW_EXCEPTION(ErrorString, severity) throw NSWReadException(__FILE__, __LINE__, ErrorString, severity)

//#define THROW_EXCEPTION(ErrorString) throw NSWReadException(ErrorString)
#endif //  NSWREADEXCEPTION_H
