/** \file NSWChannel.h
Represents a channel in the NSW raw data format 
*/

#ifndef NSWCHANNEL_H
#define NSWCHANNEL_H
#include <stdio.h>
#include <iostream>
#include "NSWRead/NSWChannelID.h"
class NSWPacket;

/** \brief This class represents the VMM data for a single channel.

    It consists of a online channel ID that contains the channel number,
    the layer and the chamber, and the rest of the VMM data.

    Pedestal and noise are stored in the class and automatically subtracted.
    The constructor initializes the pedestal to 0, so no subtraction
    takes place until SetPedestal() is called. This is typically done
    with the CalServer class.
*/
class NSWChannel : public NSWChannelID { 

 private:
  unsigned char vmm;          // vmm number
  unsigned char channel;      // vmm channel
  unsigned short int pdo;     // adc amplitude
  unsigned char tdo;          // peaking time 
  unsigned char rel;          // relative BCID
  unsigned char neighbor;     // flag
  unsigned char parity;       // parity
  // --------- calibration const ------------
  unsigned char tmin, tmax;   // minimum & maximum TDO from calibration
  float base=0.0;                 // baseline in PDO counts
  float gain;                 // gain
  float noise;                // noise in PDO counts
  unsigned char status;       // channel status: calibrated, dead, etc.
  std::string calib_meth = "th";
  // ---------------------------------------
  NSWPacket* packet;

 public:
  /** Constructor of an empty strip object */  
  NSWChannel(unsigned int channelID = 0);
  NSWChannel(unsigned int channelID, unsigned int data);

  /** Destructor */
  ~NSWChannel(); 

  /** destroy the channels. Call this before the destructor if necessary
   */
  void DeleteChannels();

  /** Set Calibration method
   */
  void SetCalibMethod(std::string calmeth){
    calib_meth = calmeth;
  };

  /** Provides access to the amplitude.
      @return the PDO value witout pedestal subtaction.
  */
  short int GetPDO (){return pdo;};

  /** Provides access to the TDO.
      @return the TDO value 
  */
  short int GetTDO (){return tdo;};

  /** Provides access to the relative BCID.
      @return the relative BCID value 
  */
  short int GetRelBCID (){return rel;};

 
  /** Set the amplitude.
  */
  void SetPDO (unsigned short p){pdo=p;};

  /** Set the TDO.
  */
  void SetTDO (unsigned char t){tdo=t;};

  /** Set the relative BCID.
  */
  void SetRelBCID (unsigned char r){rel=r;};

  /** set the time consisting of relBCID and TDO with calibration
      @par t the time in ns;
   */
  void SetTime (double t);

  
  /** @return the peaking time of this strip in ns. This is based on
      relative BCID and TDO value.
  */
  double GetPeakingTime() {return 25*(rel-(tdo-tmin)/(double)(tmax-tmin));};

  /** Set the peaking time of this strip in ns. This sets the
      relative BCID and TDO value accroding to tmin and tmax.
  */
  void SetPeakingTime(double t);

  /** @return the peaking amplitude of this strip. This is based on
      PDO and pedestal
      The unit is ADC counts.
  */
  double GetAmplitude() {
    double ampl = pdo;//-base;
    //std::cout << "BASELINE IS: " << base << std::endl;
    //if(ampl < 0){
    //  ampl = 0;
      //printf("AMPLITUDE WAS NEGATIVE!!");
    //}

    return ampl;
  };
  
  /** Using another calibration studies: @return Amplitude in mV.*/
  double GetAmplitude_mV(){
    
    double calib_ampl = 0.0;
    double ampl = pdo-base;

    // only one calib
    calib_ampl = ampl/gain;
    //
    // two calibs
    /*if(GetLayer()==4){
      calib_ampl = (ampl - 10.5752)/0.958285;
    }
    else if(GetLayer()==5){
      calib_ampl = (ampl - 12.6972)/0.980571;
    }
    else if(GetLayer()==6){
      calib_ampl = (ampl - 16.3638)/0.980571;
    }
    else if(GetLayer()==7){
      calib_ampl = (ampl - 35.2495)/1.20343;
      //calib_ampl = (ampl - 38.421)/0.869143;
    }*/

    //return
    /*if(calib_ampl < 0){
      calib_ampl = 0;
    }*/
    //std::cout << "amplitude_mv method: " << calib_meth << std::endl;
    if(calib_meth=="tp")
      return ampl;
    else if(calib_meth=="th")
      return calib_ampl;

  };

  /** @return the chanrge in fC*/
  double GetCharge() {return (pdo-base)/10.0;};
  

  /** @return the pedestal in ADC counts.
      @see SetPedestal().
  */
  double GetBaseline() {return base;};


  /** Sets the baseline value for this channel.
      @param pedestal the baseline value in ADC counts.
      This value is used in the GetAmplitude() method.
      This method is used by the CalServer() class.
  */
  void SetGain (float g){gain = g;};

  /** @return the pedestal in ADC counts.
      @see SetPedestal().
  */
  double GetGain() {return gain;};


  /** Sets the gain value for this channel.
      @param pedestal the pedestal value in ADC counts.
      This value is used in the GetAmplitude() method.
      This method is used by the CalServer() class.
  */
  void SetBaseline (float baseline){base = baseline;};

  /** @return the status bits
  */
  int GetStatus() {return status;};


  /** Sets the status bits
      @param s the status bits
  */
  void SetStatus (unsigned char s){status = s;};

  /** @return the Tmin value for the TDO calibration
  */
  int GetTmin() {return tmin;};


  /** Sets the  Tmin value for the TDO calibration
      @param  t the Tmin value for the TDO calibration
  */
  void SetTmin (unsigned char t){tmin = t;};

  /** @return the Tmax value for the TDO calibration
  */
  int GetTmax() {return tmax;};


  /** Sets the  Tmax value for the TDO calibration
      @param  t the Tmax value for the TDO calibration
  */
  void SetTmax (unsigned char t){tmax = t;};

  /** @return the noise value in ADC counts.
   */
  double GetNoise() {return noise;};

  /** Sets the noise value for this channel.
      @param n the noise value in ADC counts.
      This method is used by the CalServer() class.
  */
  void SetNoise (double n){noise=n;};


  /** Sets the neighbor flag for this channel.
      @param n the flag
  */
  void SetNeighbor (unsigned char  n){neighbor=n;};


  /** Get the neighbor flag for this channel.
      @return the flag
  */
  unsigned char GetNeighbor (){ return neighbor;};

  /** Sets the parity flag for this channel.
      @param n the flag
  */
  void SetParity (unsigned char  p){parity=p;};


  /** Get the parity flag for this channel.
      @return the flag
  */
  unsigned char GetParity (){ return parity;};

  /** calculate the parity of this channel
      bcid the bcid from the header of this packet
      @return the parity value, 0 or 1
  */
  int CalculateParity(int bcid);
 

  
  /** This method is only used by the NSWCluster() class to read in 
      raw data.
      @return the number of bytes read  */
  int Read (unsigned char *buf, bool hasTDO, unsigned int upperID);
  

  /** Store the strip in a memory buffer.
      This is used for writing  events out to a file.
      This function is called by CSCCluster.Store().
      @param buf The buffer location to store to.
      @return The number of words stored.
  */
  int Store (unsigned char * buf, bool hasTDO);


  /** Set the pointer to the packet */
  void SetPacket(NSWPacket* p){ packet = p;};

  /** Get a pointer to the packet 
      @return the packet pointer
  */
  NSWPacket* GetPacket() { return packet; }

  


  /** Prints the VMM values to stdout. Used for debugging. */
  void Print();
}; 

#endif // NSWCHANNEL_H


