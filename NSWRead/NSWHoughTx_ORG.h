/** \file NSWHoughTx.h
 represents a clustering and track fitting algorithm
 it is different from the other methods as
 it finds the tracks first using Hough Transforms 
 and then finds the clusters around the tracks to do the final fitting. This class combines the classes NSWClusterList, NSWSegment, NSWSegmentList.
*/

#include <vector>
#include "NSWSegment.h"
#include "NSWSegmentList.h"
#include "NSWCluster.h"
#include "NSWSector.h"
#include <algorithm>
#include <TH2F.h>
#include <cmath>
#include <iomanip>
#include <cstdint>
#ifndef NSWHOUGHTX
#define NSWHOUGHTX

class NSWHoughTx : public NSWChannelID {
 private:

  //std::vector<NSWSegment*> segments;
  std::vector<std::vector< std::vector <NSWCluster*>>> multi_clusters;
  std::vector< std::vector <NSWCluster*>> single_clusters;

 public:

  NSWSegmentList* segmentList;

  NSWHoughTx(int num_strips_percell);
  ~NSWHoughTx();

  void HoughTransform();
  NSWChannel* ch_det[8][8192];
  TH2F *h_HThits_xL4VsxL1, *h_HThitsPot_xL4VsxL1, *h_HThitsMask_xL4VsxL1, *h_HTTotCharge_xL4VsxL1;

  int num_FilledHT = 0, num_found1perlayer = 0, num_SmallAngleHT = 0, num_isClusterHT = 0, num_isGoodClusterHT = 0;
  bool reject = false, reject_noFilledHT=false, reject_noHit1perlayer=false, reject_noSmallAngle=false, reject_noClusterHT=false, reject_noGoodClusterHT=false;

  void define_HT_var();
  int Ncells;

  NSWSegmentList* segList;

  std::vector<Float_t> xbins_arr, ybins_arr;

  void select_hits(NSWSector* sec);

  std::vector<std::vector<std::vector<std::vector<int>>>> HT_cells_hits, HT_cells_hitsPot, HT_cells_hitsMask;
  /** Perform the Hough-Transform and return a set of strips from layer 1 and 4
      which can be considered for the track
  */
  std::vector<double> Fill_HT(double minAmp = 40, int minWid = 1);
  bool isGoodCluster(NSWCluster* cl);

  std::vector< std::tuple<double,double,double,double> >  valid_HTcells;
  std::vector< std::pair<int,int> > valid_HTbins;

  int Nstrips_percell;
  void Clear();

  NSWSegmentList* GetSegmentList();

  /** vector of bad channels
   */
  std::vector< std::vector <std::vector<int> >> v_bad_chnls;

  /** set vector of bad channels
   */
  void Set_vecBadChanls(std::vector< std::vector <std::vector<int> >> vec){
    v_bad_chnls = vec;
  }
  /** Check if the channel is contained in the bad channel list
   */
  bool isChnBad(int layer, int chnl_num);

  /** is sng was ON or OFF during the data
   */
  bool sng;
  
  /** set sng
   */
  void Set_sng(bool is_sng){sng=is_sng;}

  /** Get the multi-clusters vector
   */
  std::vector<std::vector< std::vector <NSWCluster*>>> GetMultiClusters(){return multi_clusters;}

};

#endif
