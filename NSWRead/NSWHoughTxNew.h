/** \file NSWHoughTx.h
 represents a clustering and track fitting algorithm
 it is different from the other methods as
 it finds the tracks first using Hough Transforms 
 and then finds the clusters around the tracks to do the final fitting. This class combines the classes NSWClusterList, NSWSegment, NSWSegmentList.
*/

#include <vector>
#include "NSWSegment.h"
#include "NSWCluster.h"
#include "NSWSector.h"
#include <TH2F.h>
#include <cmath>
#include <iomanip>
#include <cstdint>
#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <exception>
#include "TH2.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TFile.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "TLatex.h"
#include "TStyle.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWSegmentList.h"

#ifndef NSWHOUGHTXNew
#define NSWHOUGHTXNew

class NSWHoughTxNew : public NSWChannelID {
 private:

  bool verbose;

  int firstLayerNum, lastLayerNum, NLayers;
  int firstStripNum, lastStripNum, NChannelsInLayer;

  // Map of HT cells with cell edges xL4_bins_arr and xL7_bins_arr
  int Ncells;
  double Nstrips_percell;

  std::vector<Double_t> xL4_bins_arr, xL7_bins_arr;

  NSWClusterList clusterList;
  NSWClusterList clusterList_badclusters;

  std::vector< std::vector< std::vector < std::vector< NSWCluster* > > > > HTCell_Clusters; // [ixL4cell][ixL7cell][ilayer][icluster]
  std::vector< std::vector< int > >                                        HTCell_NLayer_WClusters;

  std::vector< std::pair<int,int> >                      acceptedHTCell_index;
  std::vector< std::vector< std::vector <NSWCluster*> >> acceptedHTCell_clusters;            // [AcceptedHTCell][ilayer][icluster]
  
  double firstStripPos, lastStripPos, stripWidth;

  double target_seg_angle, seg_angle_range;
  double low_target_ang,   high_target_ang;
  double target_slope, low_target_slope, high_target_slope;

  int AcceptSegment_NCluster;

  int min_chan_perclu = 2;

 public:

  bool SetVerbose( bool v ) { verbose = v; }

  //  Map of hits and clusters in the event
  std::vector< std::vector <NSWChannel*>> ch_det; //[ilayer][istrip]
  std::vector< std::vector <NSWCluster*>> Clusters_PerLayer; //[ilayer][icluster]

  std::vector< std::vector <std::vector<int> >> v_bad_chnls;

  bool sng;

  NSWHoughTxNew(  double num_strips_percell, int startStripN, int lastStripN, int firstLayerN, int lastLayerN );
  ~NSWHoughTxNew();
  
  // Clear saved HTCell_Clusters and acceptedHTCell_clusters but not map of hits or Clusters_PerLayer
  void Clear();
  void ClearStripHitMap();

  // set the starting/ending strip and layer numbers.  Must be run right after constructor before all other functions
  bool SetStripRange( double num_strips_percell, int startStripN, int lastStripN, int firstLayerN, int lastLayerN );
  // set the position of the first strip
  void SetFirstStripPosition( double pos );
  // set min,max,target angles of the segment roads
  void SetTargetAngle ( double target_angle, double angle_range );
  // set N clusters needed for segment
  void SetAcceptNClusterSegment ( int ncluster );
  // set the strip hit map directly instead of decoding it
  void SetStripHitMap( std::vector< std::vector <NSWChannel*>> stripHitMap );
  // set the list of clusters per layer good/bad externally instead of decoding it
  void SetClusters(NSWClusterList clusterList_tmp, NSWClusterList clusterList_badclusters_tmp);

  //Set the minimum number of channels in the cluster
  void SetMinChanInCluster(int min_chan){min_chan_perclu = min_chan;}

  // set the HT cell sizes xL4_bins_arr and xL7_bins_arr
  void define_HT_var();
  // inverse of equation in define_HT_var() find the cell corresponding to a position
  int OverlappingCell( int layer, double pos, bool lowest ); // only defined for layer 4 and 7
  // find the cell edge equivalent at an intermediate edge
  double GetPredictedEdge(int iFirstL, int iLastL, double clusterZ, bool low);
  // find the slope closest to the target slope allowed in the road 
  double GetMinimumSlopeDiffTarget(int iFirstL, int iLastL, double clusterPos, double clusterZ, int clusterLayer);

  // load map of hits on strips for the event
  void select_hits(NSWSector* sec);

  // Find all good clusters in the event
  void FindClusters(int clusterType=2);


  // fill the cells of the Hough Transform with clusters
  void Fill_HT(int clusterType=2, int BeamStartStrip=0, int BeamEndStrip=408);
  // fill the HT Cells corresponding to a single cluster on a specific layer
  void FillHTCell(NSWCluster * cl, int cl_layer, int clusterType);

  // Check if the cluster is good
  bool isGoodCluster(NSWCluster* cl, std::string &reason, int clusterType, double pos_min_beam, double pos_max_beam);
  bool isChnBad(int layer, int chnl_num);

  // set or get functions
  void Set_vecBadChanls(std::vector< std::vector <std::vector<int> >> vec){
    v_bad_chnls = vec;
  }
  void Set_sng(bool is_sng){sng=is_sng;}

  std::vector<std::vector< std::vector <NSWCluster*>>> GetAcceptedHTCell_Clusters(){
    return acceptedHTCell_clusters;
  }

  int GetNChannelsInLayer() { return NChannelsInLayer; }

  NSWClusterList GetClusterList()    { return clusterList; }
  NSWClusterList GetBadClusterList() { return clusterList_badclusters; }
  

};

#endif
