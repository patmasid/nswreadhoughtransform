/** \file NSWSegmentList.h
 represents a list of clusters of channels.
*/
#include <vector>
#include "NSWSegment.h"
#include "NSWClusterList.h"

#ifndef NSWSEGMENTLIST
#define NSWSEGMENTLIST


/**
   The NSWSegmentList class holds the NSWSegments for a NSWSector.
   @see NSWSegment.
*/  
class NSWSegmentList{ 
 private:
  std::vector<NSWSegment*> segment; // one vector per NSWSector
  
  
 public:
   
  /** Constructor for intialization: this produces an empty chamber. */
  NSWSegmentList(); 
  

  /** Destructor */
  ~NSWSegmentList(); 
  
  /** delete any previous data */
  void Clear();
  
 
  /** Provides access to the segments.
     @param i the segment number: 0 to GetNumSegments-1.
     @return The segment pointer.
     @see GetNumChannel()
  */
  NSWSegment* GetSegment (int i){
    return segment.at(i);
  };

  /** adds a segment to the segment vector
   */
  void AddSegment(NSWSegment* seg);
  
  /** removes a segment from segment vector
   */
  void RemoveSegment(int index);

  /** Provides the number of segments 
      @return the number of segments in this sector.
  */
  int GetNumSegments () {return segment.size();};


  /** Fill a NSWSegmentList with the segments from NSWClusters of a NSWSector.
      @param clusterList the NSWClusterList that holds the NSWClusters
      @param chicut the chi2 cut of the line fit
  */
  void Fill(NSWClusterList* clusterList, double chiCut = 20);

  /** Fill a NSWSegmentList with the segments from NSWClusters of a NSWSector.
      This function fills 4 layers of sTGC.
      @param clusterList the NSWClusterList that holds the NSWClusters
      @param start the fist of the 4 layers under consideration
      @param chicut the chi2 cut of the line fit
  */
  void Fill4(NSWClusterList* clusterList, int start = 0, double chiCut = 20, int clusterType = 1, double sig=-1.);

  /** Multi-cluster track list filling algorithm
  */ 
  void multiCluster_Fill4(NSWClusterList* clusterList, int start = 0, double chiCut = 20, int clusterType =1, double sig=-1.);  

  /** Prints some information about the segment list.
      Used for debugging.
  */
  void Print(); // prints information
}; 

#endif
