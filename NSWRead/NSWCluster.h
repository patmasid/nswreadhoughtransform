/** \file NSWCluster.h
 represents one cluster of channels.
*/
#include <vector>
#include <TGraph.h>
#include <TF1.h>
#include <TMatrixD.h>
#include <TVectorD.h>

#include "NSWPacket.h"


#ifndef NSWCLUSTER
#define NSWCLUSTER


/**
   The NSWCluster class holds the NSWChannels for one cluster.
   @see NSWPacket().
*/  
class NSWCluster : public NSWChannelID { 
 private:
  /** vector of channels */
  std::vector<NSWChannel*> channel;
  
  /** number of missing channels in this cluster */
  int missing;

  /** channel number with peak amplitude for sTGC*/
  int imax;

  /** total PDO amplitude of all strips */
  double totAmp;
  
  /** total PDO amplitude of all strips after 1dst calib */
  double totAmp_1stCalib;

  /** total charge of all strips */
  double totCharge;

  /** total center of gravity of all strips */
  double totCOG;

  /** total strip number of better COG */
  double totStrip;

  /** minimum time of all strips */
  double minTime;

  /** maximum time of all strips */
  double maxTime;

  /** minimum amplitude of all strips */
  double minPDO;

  /** maximum amplitude of all strips */
  double maxPDO;

  /** pointer to the first packet */
  NSWPacket* packet;
 
  /** Gaussian used to fit centroid */ 
  //TF1* gaus = new TF1("fgaus", "[0]*exp(-0.5*((x-[1])/[2])^2)");  
  TF1* gaus = new TF1("fgaus", "gaus");

  /** TGraph to store cluster */
  //TGraph* gCentroid;

  double centroid = -1; 

  double sigma = -1;
  
  double chisq;

  int num_pot, num_mask, num_bkg;
 
  /** 0:no split; 1: split from the left; 2: split from the right; 3: split from both side */
  int typeSplit = 0;
  double peak = -1;
  int fitstatus = -1;

  /** caruana mean  */
  double meanCaruana = -1.0;
  

  /** parabola mean */
  double meanParabola = -1.0;

 public:
   
  /** Constructor for intialization: this produces an empty cluster. */
  NSWCluster(); 
  

  /** Constructor for intialization: this produces a cluster with one channel. */
  NSWCluster(NSWChannel* chn); 
  

  /** Destructor */
  ~NSWCluster(); 
  
 
  /** Provides access to the channels.
     @param i the channel number: 0 to GetNumChannels()-1.
     @return The channel pointer, or NULL if i is out of range.
     @see GetNumChannel()
  */
  NSWChannel* GetChannel(int i);


  
  /** adds one channel to the cluster.
      @param chn The channel to add.
  */
  void AddChannel (NSWChannel* chn);

  

  /** Provides the number of channels including gaps
      @return the number of channels in this cluster.
  */
  int GetNumChannel () {return channel.size();};

  /** Provides the number of channels including gaps
      @return the number of channels in this cluster.
  */
  int GetWidth () {return channel.size();};

  /** provides length of cluster in x axis*/
  double GetXWidth();


  /** Provides the number of missing channels 
      @return the number of missing channels in this cluster.
  */
  int GetNumMissing () {return missing;};


  /** @return the total PDO amplitude of all strips */
  double GetTotalAmplitude(){return totAmp;};

  /** @return the total PDO amplitude of all strips after 1st Calib */
  double GetTotalAmplitude_1stCalib(){return totAmp_1stCalib;};

  /** @return the average PDO amplitude of all strips */
  double GetAverageAmplitude(){return totAmp/(channel.size()-missing);};

  /** @return cluster centroid obtained from Gaussian fit */
  void FitGaussian(); 

  double GetGaussianCentroid(){return centroid;};

  //TGraph* GetClusterGraph(){return gCentroid;};

  //TF1* GetGaussianFunction(){return gaus;};
  TF1* GetGaussianFunction(){
    gaus->SetParameters(peak, centroid, sigma);
    return gaus;
  };

  double GetGaussianMaximum(){return peak;};

  double GetGaussianSigma(){return sigma;};

  double GetGaussianChisq(){return chisq;};
  
  void GetNumPotMaskBkg(int ichn);
  int GetNumPotential(){return num_pot;};
  int GetNumMasked(){return num_mask;};
  int GetNumBkg(){return num_bkg;};

  /** return the cluster mean using caruana method
   */

  void FitCaruana();// almost same as GetMeanClusterPosition() from the original implementation of caruana in NSWRead
  double GetMeanCaruana(){return meanCaruana;};
  
  /** @return center of gravity position in mm of this cluster */
  double GetCOG();
  /*{
    if (totAmp>0) {
      return totCOG/totAmp;
    } else {
      return -100;
    }
  };*/


  /** @return avergage strip position */
  double GetCOG2() {
    if (channel.size()>0) {
      return totStrip/channel.size();
    } else {
      return -1;
    }
  };

  /** @return the number of the channel with the peak amplitude for sTGC */
  int GetPeakChannel() {
    if (imax<0) return -1;
    if (imax>channel.size()-1) return -1;
    return channel[imax]->GetDetectorStrip();
  };

  /** @return the channel with peak amplitude */
  NSWChannel* GetPeakNSWChannel() {return channel[imax];};

  /** @return the time of the channel with the peak amplitude for sTGC */
  int GetPeakingTime() {
    if (imax<0) return -1;
    if (imax>channel.size()-1) return -1;
    return channel[imax]->GetPeakingTime();
  };


  /** @return the number of the first channel */
  int GetStartChannel() {
    if (channel[0] == NULL) return -1;
    return channel[0]->GetDetectorStrip();
  };

  /** @return the minimum Time of all strips */
  double GetMinimumTime() {return minTime;};

  /** @return the  maximum time of all strips */
  double GetMaximumTime() {return maxTime;};

  /** @return the minimum Amplitude of all strips */
  double GetMinPDO() {return minPDO;};

  /** @return the  maximum Amplitude of all strips */
  double GetMaxPDO() {return maxPDO;};

  /** @return the channel number in the cluster which has peak amplitude */
  int Getimax() {return imax;};

  /** @return the total charge of all strips */
  double GetCharge() {return totCharge;};

  /** @return the raw value of the parabola interpolation */
  double GetParabolaRaw();

  /** @return the corrected position of the parabola interpolation */
  double GetParabola();

  /** @return save parabola interpolated mean */
  void SaveParabolaMean();
  double GetParabolaMean();

  /** @return the 3 strip center of gravity interpolation */
  double GetCOG3();

  /** @return the number of strips where the neighbor flag is zero */
  int CountNeighbors();

  /** set the split for cluster */
  void SetSplit(int type) {typeSplit += type;};
  int GetSplit(){return typeSplit;};
  /** Prints some information about the cluster.
      Used for debugging.
  */

  /** compare clusters*/
  bool isSameCluster(NSWCluster* &clu);

  void Print(); // prints information
}; 

#endif
