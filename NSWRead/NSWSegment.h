/** \file NSWSegment.h
 represents a segment as an array of clusters.
*/
#include "NSWCluster.h"
#include <math.h>
#include "TMath.h"
#include "Math/ProbFunc.h"
#include "TSystem.h"

#ifndef NSWSEGMENT
#define NSWSEGMENT


/**
   The NSWSegment class holds the NSWClusters for a segment in a NSWSector.
   @see NSWCluster().
*/  
class NSWSegment{ 
 private:
  /** array of clusters [layer] */
  NSWCluster* cluster[8];
  int numClusters = 0;
  double inter, slope, chi2; // line fit precision readout
  double ypos;
  int isLarge, tech; // for getting the z position for each layer
  std::vector<float> sigma_4layer;

 public:
   
  /** Constructor for intialization: this produces an empty chamber. */
  NSWSegment(); 
  

  /** Destructor */
  ~NSWSegment(); 
  
  /** delete any previous data */
  void Clear();
  
 
  /** Provides access to the channels.
     @param layer The layer of the cluster
     @param i the channel number: 0 to GetNumChannels(layer)-1.
     @return The channel pointer, or NULL if i is out of range.
     @see GetNumChannel()
  */
  NSWCluster* GetCluster(int layer){
    return cluster[layer];
  }

  /** PutCluster puts a cluster into the segment
      @param cl the cluster pointer
      @param layer the layer number
  */
  void PutCluster (int layer, NSWCluster* cl);

  /** Remove a cluster from the segment
   */
  void RemoveCluster(int layer);
  
  /** Provides the number of channels 
      @return the number of channels in this sector.
  */
  int GetNumClusters () {return numClusters;};

  /** Fit the clusters in the precision layers to a line */
  void FitPrecision(double sigma=1.0);
  
  /** Fit the clusters in the precision layers to a line using 4 layers 
  @param start the first layer number
  @param sigma the position error for the hits */
  void FitPrecisionGaus(int start, double sigma=-1.0);  

  void FitPrecisionCOG(int start, double sigma=1.0);

  /** Fit the clusters in precision layers to a line using caruana*/
  void FitPrecisionCaruana(int start, double sigma=-1.0);
  
  /** Fit the clusters in precision layers to a line using parabola*/
  void FitPrecisionParabola(int start, double sigma=-1.0);

  /** @return the intercept of the line fit */
  double GetIntercept() {return inter;};
  
  
  /** @return the slope of the line fit */
  double GetSlope() {return slope;};
  
  
  /** @return the angle of the line fit */
  double GetAngle() {return atan(slope)/3.141592*180;};
  
  /** @return the strip pitch */
  double GetPitch();
  
  /** @return the predicted position in a layer
      @param layer the layer for the predicted position */
  double GetPredicted (int layer);

  /** @return the degree of freedom for the segment fitting with chi2 */
  double GetDOF(){return GetNumCluster()-2.0;};

  /** @return the chi2 of the line fit */
  double GetChi2() {return chi2;};

  /**@return the uncert on strip positions */
  std::vector<float> GetSigma() {return sigma_4layer;};

  /** @return the cdf of chi2 of the line fit */
  double GetCDF();

  /** @return the inv cdf of chi2 of the line fit*/
  double GetInvCDF();

  /** @return the number of precision clusters */
  int GetNumPrecision();
  
  /** @return the number of all clusters */
  int GetNumCluster();

  /** @return the y position */
  double GetYPosition() { return ypos;};

  /** set the y poisition 
      @param y the y position */
  void SetYPosition (double y) { ypos = y;};

  /** compare with another segment to find common clusters
   */
  void Compare(NSWSegment* seg, std::vector<int> &common_clusters);
  
  /** Prints some information about the segment.
      Used for debugging.
  */
  void Print(); // prints information
}; 

#endif
