/** \file NSWClusterList.h
 represents a list of clusters of channels.
*/
#include <vector>
#include "NSWCluster.h"
#include "NSWSector.h"
#include <algorithm>
#ifndef NSWCLUSTERLIST
#define NSWCLUSTERLIST


/**
   The NSWClusterList class holds the NSWClusters for a NSWChamber.
   @see NSWCluster().
*/  
class NSWClusterList{ 
 private:
  std::vector<NSWCluster*> cluster[8]; // one vector per layer
  

 public:
   
  /** Constructor for intialization: this produces an empty chamber. */
  NSWClusterList(); 
  

  /** Destructor */
  ~NSWClusterList(); 
  
  /** delete any previous data */
  void Clear();
  
 
  /** Provides access to the channels.
     @param layer The layer of the cluster
     @param i the channel number: 0 to GetNumChannels(layer)-1.
     @return The channel pointer, or NULL if i is out of range.
     @see GetNumChannel()
  */
  NSWCluster* GetCluster(int layer, int i){
    return cluster[layer].at(i);
  }

  /** Add cluster */
  void AddCluster(int layer, NSWCluster* cl);

  /** Remove unwanted cluster */
  void RemoveBadCluster(int layer, int i);
  
  /** Split the cluster using double maxima */
  void SplitCluster(int layer, int i, int chnl, bool isNULL);

  /** Set clusterList
   */
  void SetClusterList(std::vector<std::vector<NSWCluster*>> list);
  /** Provides the number of channels 
      @return the number of channels in this sector.
  */
  int GetNumClusters (int layer) {return cluster[layer].size();};


  /** Fill a clusterList with the channels in a NSWSector.
      @param sector the NSWSector that holds the NSWChannels
      @param minAmp the minimal amplitude for at least one strip
      @param minWid the minimum number of channels in the cluster
  */
  void Fill(NSWSector* sector, double minAmp = 40, int minWid = 1);

  /** vector of bad channels
   */
  std::vector< std::vector <std::vector<int> >> v_bad_chnls;

  /** set vector of bad channels
   */
  void Set_vecBadChanls(std::vector< std::vector <std::vector<int> >> vec){
    v_bad_chnls = vec;
  }
  /** Check of the channel is contained in the bad channel list
   */
  bool isChnBad(int layer, int chnl_num);

  /** Prints some information about the cluster.
      Used for debugging.
  */
  void GetVectorClusters(std::vector< std::vector< NSWCluster* > > &multi_cluster_vec);
  bool isCommonCluster(NSWClusterList &list);
  void CombineClustersLists(NSWClusterList &list, NSWClusterList &list_comb);
  void CombineClustersLists(NSWClusterList &list);
  void Print(); // prints information
};

#endif
