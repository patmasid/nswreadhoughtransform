#include <vector>
#include <algorithm>
#include "NSWSector.h"
#include "NSWCluster.h"
#include "NSWClusterList.h"

#ifndef NSWsTGCPADTOWER
#define NSWsTGCPADTOWER

class NSWsTGCPadTower{

  private:

  std::vector<std::pair<float, float>> position;

  std::vector<NSWsTGCWireTower*> overlapping_wire_towers; // wire tower numbers
  
  std::vector<std::vector<NSWsTGCStrip*>> overlapping_strips; // strips overlapping per layer
  std::vector<std::vector<NSWsTGCWire*>>  overlapping_wires;  // wires overlapping per layer

  std::vector<NSWsTGCPads*>  overlapping_pads; // [layer] pads per layer in tower
  std::vector<int> start_overlap_strip_number; // [layer] start strip number in each layer
  std::vector<int> last_overlap_strip_number;  // [layer] end strip number in each layer

  std::vector<std::vector<NSWCluster*>> overlapping_strip_clusters; //[layer][cluster]

  public:
    NSWsTGCPadTower();

    ~NSWsTGCPadTower();

    void FindPadTower(NSWSector* sec);

    void MatchPadTower(NSWSector* sec);

    void Fill(NSWSector* sec);

    void Print();
    
    NSWChannel* GetChannel(int layer, int channel){ return wires[layer][channel]; };

    int GetNumClusterList() {return clusterList.size();};

    NSWClusterList* GetClusterList(int i){
      return clusterList.at(i);
    };

    //int GetNumClusters (int layer) {return cluster[layer].size();};

    std::vector<NSWChannel*> GetPadTower(int i){ return padTower_list.at(i); };

    int GetNumPadTower(){ return padTower_list.size(); };

    NSWChannel* GetRawPad(int layer, int channel){ return pads[layer][channel]; };

    NSWChannel* GetRawWire(int layer, int channel){ return wires[layer][channel]; };
};




#endif
