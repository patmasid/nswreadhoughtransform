//General reading class for atlas nsw data files 
#include<stdio.h>

#include"NSWRead/NSWChannel.h"
#include"NSWRead/NSWPacket.h"


NSWChannel::NSWChannel(unsigned int channelID){
  SetChannelID(channelID);
  vmm = 0;
  channel = 0; // get from channelID?
  pdo = 0;
  tdo = 0;
  rel = 0;
  neighbor = 0;
  parity = 0;
  base = 32;
  //base = 0.0; // @patmasid
  gain = 0.3;
  noise = 1;
  tmin = 72; // average tdo calibration
  tmax = 160;
  status = 0;
  packet = NULL;
} 


NSWChannel::NSWChannel(unsigned int channelID, unsigned int data){
  vmm = (data >> 24) & 7;
  channel = (data >>18) & 0x3f;
  SetChannelID(channelID, vmm, channel);
  pdo = (data >> 8) & 0x3ff;

  tdo = data & 0xff;
  rel = (data >> 27) & 7;
  neighbor = (data >> 30) & 1;
  //std::cout << "NSWChannel: neighbor " << neighbor << std::endl;
  parity = data >> 31;
  // calibration constants:
  base = 32;
  //base = 0.0;
  gain = 0.3;
  noise = 1;
  tmin = 72; // average tdo calibration
  tmax = 160;
  status = 0;
  packet = NULL;
} 

NSWChannel::~NSWChannel(){
}



int NSWChannel::Read (unsigned char *buf, bool hasTDO, unsigned int upperID){
  unsigned char * start = buf;
  if (hasTDO) {
    tdo = *buf;
    buf++;
  }

  pdo = *buf; // lower 8 bit
  buf++;
  
  unsigned int vmmChannel = *buf>>2;
  pdo |= ((*buf & 3)<<8); //upper 2 bit
  buf++;

  parity = *buf>>7;
  neighbor = (*buf>>6)&1;
  rel = (*buf>>3) & 7;
  unsigned int vmm = (*buf) &7;
  SetChannelID (upperID, vmm, vmmChannel);
  buf++;
  return buf-start;
}

int NSWChannel::CalculateParity (int bcid){
  unsigned int v = tdo | (pdo<<8) | (parity<<31) | (neighbor<<30); // P, N, tdc, adc
  v |= (bcid+rel)<<18;   // word value to compute the parity of
  //  printf ("v = %08X\n", v);
  v ^= v >> 16;
  v ^= v >> 8;
  v ^= v >> 4;
  v &= 0xf;
  return (0x6996 >> v) & 1;
}


void NSWChannel::SetTime (double t){
  // invert t = 25*(rel-(tdo-tmin)/(tmax-tmin))
  rel = ((int)t) / 25; // int division
  if (rel<0) rel = 0;
  if (rel>7) rel = 7;
  double range = tmax-tmin;
  tdo = -((int)(t+25) % 25)/25.0 * range +tmin;
}

int NSWChannel::Store (unsigned char * buf, bool hasTDO){
  unsigned char * start = buf;
  if (hasTDO) *buf++ = tdo;
  *buf++ = pdo & 255;
  *buf++ = ((pdo>>8)  | (GetVMMChannel()<<2));
  *buf++ = (GetVMM() | (rel<<3) | (neighbor<<6) | (parity<<7));
  return buf-start;
}

void NSWChannel::Print () {
  // print some information
 
  //printf ("%c%02dM%dL%d ch%4d", GetSector()<0?'C':'A', GetSector()<0?-GetSector():GetSector(), GetMulti(), GetLayer(), GetChannel());

  printf ("VMM %d chn = %2d", vmm, channel);
  printf (" pdo=%4d tdo=%3d rel=%d time=%5.1f ns", pdo, tdo, rel, GetPeakingTime());
  if (packet != NULL){
    if (CalculateParity(packet->GetBCID())!=0) {
      printf (" Parity Error!\n");
    } else {
      printf ("\n");
    }
  }
}
