//General reading class for atlas nsw data files 
#include <stdio.h>

#include "NSWRead/NSWCluster.h"
#include <math.h>

NSWCluster::NSWCluster(){ 
  SetChannelID(0);
  channel.clear();
  missing = 0;
  totAmp = 0;
  totAmp_1stCalib = 0;
  totCharge = 0;
  totCOG = 0;
  totStrip = 0;
  minTime = 0;
  maxTime = 0;
  minPDO = 0;
  maxPDO = 0;
  imax = -1;
  //gCentroid = NULL;
  centroid = -1.0;
  sigma = -1.0;
  chisq = -1.0;
  num_pot = 0;
  num_mask = 0;
  num_bkg = 0;
} 

NSWCluster::NSWCluster(NSWChannel* chn){ 
  SetChannelID(chn->GetChannelID());
  channel.clear();
  missing = 0;
  channel.push_back(chn);
  totAmp = chn->GetAmplitude_mV();
  totAmp_1stCalib = chn->GetAmplitude();
  totCharge = chn->GetCharge();
  totCOG = chn->GetAmplitude_mV() * chn->GetStripPosition();
  totStrip = chn->GetDetectorStrip();
  minTime = chn->GetPeakingTime();
  maxTime = chn->GetPeakingTime();
  minPDO = chn->GetPDO();
  maxPDO = chn->GetPDO();
  imax = 0;
  //gCentroid = NULL;
  centroid = -1.0;
  sigma = -1.0;
  chisq = -1.0;
  num_pot = 0;
  num_mask = 0;
  num_bkg = 0;
} 


NSWCluster::~NSWCluster(){ 
}

NSWChannel* NSWCluster::GetChannel(int i){
  return channel[i];
}


void NSWCluster::AddChannel (NSWChannel* chn){
  channel.push_back (chn);
  if (chn == NULL) {
    missing++;
  } else { // got a channel
    totAmp += chn->GetAmplitude_mV();
    totAmp_1stCalib += chn->GetAmplitude();
    totCharge += chn->GetCharge();
    totCOG += chn->GetAmplitude_mV() * chn->GetStripPosition();
    totStrip += chn->GetDetectorStrip();
    if (minTime > chn->GetPeakingTime())  minTime = chn->GetPeakingTime();
    if (maxTime < chn->GetPeakingTime())  maxTime = chn->GetPeakingTime();
    if (minPDO > chn->GetPDO())  minPDO = chn->GetPDO();
    if (maxPDO < chn->GetPDO()) {
      maxPDO = chn->GetPDO();
      imax = channel.size()-1;
    }
  }
}

bool NSWCluster::isSameCluster(NSWCluster* &cl){

  int max = GetNumChannel()-1;
  int max_cl = cl->GetNumChannel()-1;

  bool isSame = false;
  
  if(GetGaussianCentroid() == cl->GetGaussianCentroid() && GetWidth()==cl->GetWidth() && GetChannel(0)->GetStripPosition()==cl->GetChannel(0)->GetStripPosition() && GetChannel(max)->GetStripPosition()==cl->GetChannel(max_cl)->GetStripPosition()){
    isSame = true;
  }
  else
    isSame = false;
  
  return isSame;

}

/*double NSWCluster::GetParabolaRaw(){
  if (channel.size() < 3) return -1; // not enough strips
  double A, B, C;
  NSWChannel* c;
  if (imax < 1) return -1;
  if (imax > channel.size() - 2) return -1;
  c = GetChannel(imax - 1);
  if (c == NULL) return -1;
  A = c->GetAmplitude_mV(); // left strip
  c = GetChannel(imax);
  if (c == NULL) return -1;
  B = c->GetAmplitude_mV(); // center strip
  c = GetChannel(imax + 1);
  if (c == NULL) return -1;
  C= c->GetAmplitude_mV(); // right strip

  return 0.5*(A-C)/(A+C-2*B); // parabola
}

double NSWCluster::GetParabola(){
  double raw = GetParabolaRaw();
  if ((raw < -0.7) || (raw > 0.7)) return -10; // no good interpolation
  //double par[3] = {3.39620e-01, 5.23407e+00, 1.22661e-01}; // from fit
  double par[3] = {3.73007e-01, 4.85297e+00, 1.23585e-01}; // from fit
  return par[0] * atan(par[1] * raw) + par[2] * raw;
}*/

double NSWCluster::GetParabolaRaw(){
  double A, B, C;
  double raw;

  A = -1;
  B = -1;
  C = -1;
  raw = -9;
  //pos = -9;
  //err = -9;
  unsigned int flags = 255;
  if (this == NULL) return 255;

  NSWChannel* chn;
  //int imax = GetImax(); // GetPeakChannel() - GetStartChannel();
  flags = 0;
  if (imax < 1) { // no left
    flags |= 1;
    //return flags;
    return -10;
  }
  if (imax > GetWidth() - 2) { // no right
    flags |= 2;
    //return flags;
    return -10;
  }
  chn = GetChannel(imax - 1);
  if (chn == NULL) { // no left
    flags |= 1;
    //return flags;
    return -10;
  }

  A = chn->GetAmplitude(); // left strip
  chn = GetChannel(imax);
  if (chn == NULL) { // no center
    flags |= 4;
    //return flags;
    return -10;
  }

  B = chn->GetAmplitude(); // center strip
  chn = GetChannel(imax + 1);
  if (chn == NULL) { // no right
    flags |= 2;
    //return flags;
    return -10;
  }
  C = chn->GetAmplitude(); // right strip
  
  raw = 0.5*(A-C)/(A+C-2*B); // raw parabola interpolation
  if ((raw < -0.75) || (raw > 0.75)) { // no good raw interpolation
    flags |= 8;
    //return flags;
    return -10;
  }

  return raw;

}

double NSWCluster::GetParabola(){
  double raw = GetParabolaRaw();
  if(raw == -10) return GetCOG();
  //else return raw;
  double par [4]= {0.583068, 4.01645, -0.397182, 0.420514};
  return par[0] * atan(par[1] * raw) + par[2] * raw + par[3]*raw*raw*raw;
}

void NSWCluster::SaveParabolaMean(){
  //std::cout << "GetStripPosition(imax): " << GetStripPosition(imax) << " GetParabola(): " << GetParabola() << std::endl;
  NSWChannel *c = GetChannel(imax);
  meanParabola = c->GetStripPosition() - 3.2*GetParabola();
  //meanParabola = (GetPeakChannel()+GetParabolaRaw())*3.2;
}

double NSWCluster::GetParabolaMean(){
  return meanParabola;
}

void NSWCluster::FitGaussian(){
  
  num_pot=0; 
  num_mask=0;
  num_bkg=0;
  
  if (channel.size() < 3){
    centroid = GetCOG();
    return; // not enough strips
  }

  NSWChannel* c;
  if (imax < 1) return;
  if (imax > channel.size() - 2) return;

  std::vector<float> xpos, ypos;
  xpos.clear();
  ypos.clear();
  for(unsigned int chn_i=0; chn_i < channel.size(); chn_i++){
    GetNumPotMaskBkg(chn_i);
    c = GetChannel(chn_i);
    if(c == NULL) continue;

    xpos.push_back(c->GetStripPosition());
    ypos.push_back(c->GetAmplitude_mV());
  }

  // gaus->SetParLimits(0,0,500);

  TGraph *gCentroid = new TGraph(xpos.size(), &xpos[0], &ypos[0]);
  
  /*TFitResultPtr fit_result = gCentroid->Fit("fgaus", "Q");
  // int fit_status = fit_result;
  // if (fit_status!=0) std::cout<<"Gaussian Fit fails!"<<std::endl;
  
  fitstatus = int(fit_result); 
  
  if ( fitstatus == 0 ){ 
    centroid = gCentroid->GetFunction("fgaus")->GetParameter(1);
    sigma = gCentroid->GetFunction("fgaus")->GetParameter(2);
    chisq = gCentroid->GetFunction("fgaus")->GetChisquare();
  }*/
  
  gCentroid->Fit("fgaus", "Q"); 
  centroid = gCentroid->GetFunction("fgaus")->GetParameter(1); 
  sigma = gCentroid->GetFunction("fgaus")->GetParameter(2); 
  chisq = gCentroid->GetFunction("fgaus")->GetChisquare();

  delete gCentroid;
  return;

}

double NSWCluster::GetCOG3(){
  if (channel.size() < 3) return -1; // not enough strips
  double A, B, C;
  NSWChannel* c;
  if (imax < 1) return -1;
  if (imax > channel.size() - 2) return -1;
  c = GetChannel(imax - 1);
  if (c == NULL) return -1;
  A = c->GetAmplitude_mV(); // left strip
  c = GetChannel(imax);
  if (c == NULL) return -1;
  B = c->GetAmplitude_mV(); // center strip
  c = GetChannel(imax + 1);
  if (c == NULL) return -1;
  C= c->GetAmplitude_mV(); // right strip

  return (C-A)/(A+B+C); // center of gravity relative to peak strip
}

int NSWCluster::CountNeighbors() {
  int n = 0;
  for (int i=0; i<channel.size(); i++){
    if (channel[i] != NULL){
      if (channel[i]->GetNeighbor() == 0) n++;
    }
  }
  return n;
}

void NSWCluster::GetNumPotMaskBkg(int ichn){
  
  if (channel[ichn] == NULL) return;
  if( channel[ichn]->GetRelBCID() == 3 || channel[ichn]->GetRelBCID() == 4 || channel[ichn]->GetRelBCID() == 5 ){
    num_pot++;
  }
  else if( channel[ichn]->GetRelBCID() == 2 || channel[ichn]->GetRelBCID() == 6 ){
    num_mask++;
  }
  else{
    num_bkg++;
  }
  return;
  
}

void NSWCluster::Print () {
  // print some information
 
  printf ("Cluster with %li channels and %d missing: %c%02dL%d ch%4d", channel.size(), missing, GetSector()<0?'C':'A', GetSector()<0?-GetSector():GetSector(), GetLayer(), GetVMMChannel());
  printf ("COG = %5.1f, aveAmp = %5.1f, minPDO = %5.1f, maxPDO = %5.1f\n", GetCOG(), GetAverageAmplitude(), minPDO, maxPDO); 
  for (unsigned int i=0; i<channel.size(); i++) {
    if (channel[i] != NULL) {
      channel[i]->Print();
    } else {
      printf ("Missing\n");
    }
  }
}

double NSWCluster::GetCOG(){
  double totalAmpl=0;
  double totalWeight=0;

  double minAmpl=9999;

  for ( int i=0; i<channel.size(); i++ ) {
    NSWChannel* c = channel.at(i);
    if ( c!= NULL ) {
      if ( minAmpl > c->GetAmplitude_mV() ) minAmpl = c->GetAmplitude_mV();
    }
  }

  for ( int i=0; i<channel.size(); i++ ) {
    NSWChannel* c = channel.at(i);
    if ( c!= NULL ) {
      //std::cout << "ch pos " << c->GetStripPosition()
      //      << " ch amp " << c->GetAmplitude_mV() << std::endl;
      if ( minAmpl > 0 ) {
        totalAmpl  +=c->GetAmplitude_mV();
	totalWeight+=c->GetStripPosition()*c->GetAmplitude_mV();
      }
      else {
        totalAmpl  +=c->GetAmplitude_mV()+fabs(minAmpl);
        totalWeight+=c->GetStripPosition()*(c->GetAmplitude_mV()+fabs(minAmpl));
      }
    }
  }
  double mean = totalWeight/totalAmpl;
  return mean;
}

// same calculation as GetMeanClusterPosition() from the original implementation of caruana in NSWRead
void NSWCluster::FitCaruana() {
  /*
  double totalAmpl=0;
  double totalWeight=0;

  double minAmpl=9999;

  for ( int i=0; i<channel.size(); i++ ) {
    NSWChannel* c = channel.at(i);
    if ( c!= NULL ) {
      if ( minAmpl > c->GetAmplitude_mV() ) minAmpl = c->GetAmplitude_mV();
    }
  }

  for ( int i=0; i<channel.size(); i++ ) {
    NSWChannel* c = channel.at(i);
    if ( c!= NULL ) {
      //std::cout << "ch pos " << c->GetStripPosition()                                                                                                    
      //      << " ch amp " << c->GetAmplitude_mV() << std::endl;                                                                                          

      if ( minAmpl > 0 ) {
	totalAmpl  +=c->GetAmplitude_mV();
	totalWeight+=c->GetStripPosition()*c->GetAmplitude_mV();
      }
      else {
	totalAmpl  +=c->GetAmplitude_mV()+fabs(minAmpl);
	totalWeight+=c->GetStripPosition()*(c->GetAmplitude_mV()+fabs(minAmpl));
      }
    }
  }
  //std::cout << "totalWeight: " << totalWeight << std::endl;                                                                                              
  //std::cout << "totalAmpl: " << totalAmpl << std::endl;                                                                                                  
  meanCaruana = totalWeight/totalAmpl;//GetCOG(); 
  return;  
  */
  //  std::cout << "Fitting Caruana" << std::endl;
  if (channel.size() < 3){
    meanCaruana = GetCOG();
    
    //  std::cout << "Caruana Mean " << meanCaruana << std::endl;
    return;
  }
  // Use COG for cluster with two strips or less
  //if (channel.size() < 3){
  //  meanCaruana = GetCOG();
  //  return;
  //}

  // A struct to store channel position and pdo
  struct SimpleChannel {
    double m_pos;
    int m_pdo;
    SimpleChannel(double pos, int pdo) : m_pos(pos), m_pdo(pdo) {}
  };

  // Vector of channel positions and pdos
  std::vector<SimpleChannel> cl_channels;

  // Calculation is simpler if the center of the cluster is at 0 mm.
  // So create a vector of good channels and compute the position of the 
  // center of this cluster.
  double center_cluster = 0.;
  for (unsigned int i = 0; i < channel.size(); ++i) {
    NSWChannel* c = channel.at(i);
    if (c != NULL) { 
      // Regular channel, so add its position to the sum
      center_cluster += c->GetStripPosition();
      cl_channels.push_back(SimpleChannel(c->GetStripPosition(), c->GetAmplitude_mV())); //c->GetPDO())); //new 2nd calib
    } else {
      // Null channel are kept if the both adjacent channels registered charge
      if (center_cluster > 0.) {
	// Consider up to 2 null channels between non-null channels.
	// PDO of null channel is set to 1, since pdo=0 breaks the Caruana alg.
	if (i == 0) {
	  continue;
	} else if (((i+1) < channel.size()) && (channel.at(i+1) != NULL)) {
	  // Case with 1 null channel
	  double pos_average = ( (channel.at(i-1))->GetStripPosition() 
				 + (channel.at(i+1))->GetStripPosition() ) / 2.;
	  center_cluster += pos_average;
	  cl_channels.push_back(SimpleChannel(pos_average, 1)); 
	  ++i;
	} else if (((i+2) < channel.size()) && (channel.at(i+2) != NULL)) { 
	  // Case with 2 null channels
	  double ch_pitch = std::abs((channel.at(i+2))->GetStripPosition() 
				     - (channel.at(i-1))->GetStripPosition()) / 3.;

	  double temp_pos = (channel.at(i-1))->GetStripPosition() + ch_pitch;
	  center_cluster += temp_pos;
	  cl_channels.push_back(SimpleChannel(temp_pos, 1));

	  temp_pos = (channel.at(i-1))->GetStripPosition() + 2 * ch_pitch;
	  center_cluster += temp_pos;
	  cl_channels.push_back(SimpleChannel(temp_pos, 1)); 
	  i += 2;
	}
      } else {
        // Null channel at the begining of the list is discarded
        continue;
      }
    }
  }
  // Size of the cluster
  int cluster_size = static_cast<int>(cl_channels.size());
  // Position of the cluster center
  center_cluster = center_cluster / cluster_size;

  // Caruana algorithm begins by fitting a Gaussian function to the charge 
  // distribution y(x). Taking a natural logarithm of a Gaussian function 
  // results in a quadratic function, so ln( y(x) ) = a + bx + cx^2.
  // Solving for parameters a, b and c is a minimization that can be done
  // using a matrix equation.

  // Position matrix which is symmetric
  double zeros[9] = {0.};
  TMatrixD pos_matrix(3,3,zeros);
  // Charge vector
  TVectorD charge_vec(3,zeros);

  // Build the matrix equation
  for (int i = 0; i < cluster_size; ++i) {
    // Fill the upper triangle of the matrix
    double strip_position = (cl_channels.at(i)).m_pos;
    pos_matrix(0,0) = pos_matrix(0,0) + 1.;
    pos_matrix(0,1) = pos_matrix(0,1) + strip_position - center_cluster;
    pos_matrix(0,2) = pos_matrix(0,2) + std::pow(strip_position - center_cluster, 2);
    pos_matrix(1,2) = pos_matrix(1,2) + std::pow(strip_position - center_cluster, 3);
    pos_matrix(2,2) = pos_matrix(2,2) + std::pow(strip_position - center_cluster, 4);
    // Fill the charge vector
    double log_charge = std::log( (cl_channels.at(i)).m_pdo );
    charge_vec(0) += log_charge;
    charge_vec(1) += log_charge * (strip_position - center_cluster);
    charge_vec(2) += log_charge * std::pow(strip_position - center_cluster, 2);
  }
  // Fill the lower triangle of the matrix
  pos_matrix(1,0) = pos_matrix(0,1);
  pos_matrix(1,1) = pos_matrix(0,2);
  pos_matrix(2,0) = pos_matrix(0,2);
  pos_matrix(2,1) = pos_matrix(1,2);

  // The Caruana algorithm fails if the matrix determinant is zero. 
  if (std::abs(pos_matrix.Determinant()) < 0.00001) {
    meanCaruana = GetCOG();
    return;
  }

  // Parameters a, b and c
  TVectorD caruana_param = (pos_matrix.Invert()) * charge_vec;
  // Note that pos_matrix is already inverted, i.e Invert() modifies the matrix

  // If element caruana_param(2), i.e c, is equal to zero, then the mean 
  // cluster position goes to infinity. So it is better to use COG
  if (std::abs(caruana_param(2)) < 0.00001) {
    meanCaruana = GetCOG();
    return;
  }

  // Center of the cluster in mm 
  meanCaruana = center_cluster - caruana_param(1)/(2 * caruana_param(2));

  if(isnan(meanCaruana)) meanCaruana = GetCOG();

  //always just use COG for now
  meanCaruana = GetCOG();
  return;
}
