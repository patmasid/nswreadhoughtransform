#include "NSWRead/NSWHoughTx.h"

NSWHoughTx::NSWHoughTx(int num_strips_percell){
  Nstrips_percell = num_strips_percell;
  Ncells = 408/Nstrips_percell;
  if(408 % Nstrips_percell != 0)
    Ncells += 1;
  
  xbins_arr.resize(Ncells+1);
  ybins_arr.resize(Ncells+1);
  
  //segmentList = new NSWSegmentList();
  //segmentList->Clear();

}

NSWHoughTx::~NSWHoughTx(){
  //for (int i=0; i<GetNumSegments(); i++) delete segments[i];
  //segments.clear();
  delete h_HThits_xL4VsxL1;
  delete h_HThitsPot_xL4VsxL1;
  delete h_HThitsMask_xL4VsxL1;
  delete h_HTTotCharge_xL4VsxL1;
  //delete segmentList;
}

void NSWHoughTx::Clear(){

}

NSWSegmentList* NSWHoughTx::GetSegmentList(){
  //std::cout << "number of segments: " << segmentList->GetNumSegments() << std::endl;
  //return segmentList;
}

bool NSWHoughTx::isChnBad(int layer, int chnl_num){
  if(v_bad_chnls[0][layer].size() != 0){
    if(std::find(v_bad_chnls[0][layer].begin(), v_bad_chnls[0][layer].end(), chnl_num) != v_bad_chnls[0][layer].end())
      return true;
    else
      return false;
  }
  else
    return false;
}

void NSWHoughTx::define_HT_var(){

  int rem = (408 % Nstrips_percell);
  for(int iedge=0; iedge<Ncells+1; iedge++){
    if(iedge == 0){
      xbins_arr[iedge] = iedge*3.2;
      ybins_arr[iedge] = iedge*3.2;
    }
    else if(iedge == 1){  
      xbins_arr[iedge] = iedge*3.2*Nstrips_percell;
      ybins_arr[iedge] = iedge*3.2*Nstrips_percell-1.6;
    }
    else if(iedge == Ncells){
      xbins_arr[iedge] = xbins_arr[iedge-1]+(3.2*rem);
      ybins_arr[iedge] = ybins_arr[iedge-1]+(3.2*rem);
    }
    else{
      xbins_arr[iedge] = xbins_arr[iedge-1]+(3.2*Nstrips_percell);
      ybins_arr[iedge] = ybins_arr[iedge-1]+(3.2*Nstrips_percell);
    }
    

  }
  const Float_t *xbins = {&xbins_arr[0]};
  const Float_t *ybins = {&ybins_arr[0]};
  h_HThits_xL4VsxL1 = new TH2F("HT_xL4VsxL1","HT_xL4VsxL1",Ncells,xbins,Ncells,ybins);
  h_HThitsPot_xL4VsxL1 = new TH2F("HThitsPot_xL4VsxL1","HThitsPot_xL4VsxL1",Ncells,xbins,Ncells,ybins);
  h_HThitsMask_xL4VsxL1 = new TH2F("HThitsMask_xL4VsxL1","HThitsMask_xL4VsxL1",Ncells,xbins,Ncells,ybins);
  h_HTTotCharge_xL4VsxL1 = new TH2F("HTTotCharge_xL4VsxL1","HTTotCharge_xL4VsxL1",Ncells,xbins,Ncells,ybins);
}

void NSWHoughTx::select_hits(NSWSector* sec){
  if (sec == NULL) return;
  for (int layer=0; layer<8; layer++){
    for (int i=0; i<8192; i++) ch_det[layer][i] = NULL;
  }

  std::vector<bool> bad_layers;
  bad_layers.resize(8);
  for(int iL=0; iL<8; iL++){
    bad_layers[iL] = false;
  }

  // store channels:
  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(j);
    if (pac->GetEndPoint() != 1) continue; // only strips
    for (int k=0; k<pac->GetWidth(); k++){
      NSWChannel* chn = pac->GetChannel(k);
      // if (chn->GetAmplitude_mV() < thr) continue; 
      int layer = chn->GetLayer();
      int detchn = chn->GetDetectorStrip();
      if ((detchn>=0) && (detchn<8192)) {
        ch_det[layer][detchn]=chn; // store it here 
      }
    } // next k
  } // next j
}

void NSWHoughTx::HoughTransform(){
  HT_cells_hits.resize(Ncells);
  HT_cells_hitsPot.resize(Ncells);
  HT_cells_hitsMask.resize(Ncells);

  for(int ix=0; ix<Ncells; ix++){
    HT_cells_hits[ix].resize(Ncells);
    HT_cells_hitsPot[ix].resize(Ncells);
    HT_cells_hitsMask[ix].resize(Ncells);
    
    for(int iy=0; iy<Ncells; iy++){
      HT_cells_hits[ix][iy].resize(8);
      HT_cells_hitsPot[ix][iy].resize(8);
      HT_cells_hitsMask[ix][iy].resize(8);
    }
  }

  for(int iL=0; iL<8; iL++){
    for(int chn = 0; chn<408; chn++){
      if(ch_det[iL][chn] != NULL){

	double x = ch_det[iL][chn]->GetStripPosition();
	double z = ch_det[iL][chn]->GetZPosition();
	double z1 = ch_det[iL][chn]->GetZPosition(4,true,0);
	double z2 = ch_det[iL][chn]->GetZPosition(7,true,0);
	double xL4 = -1.0, xL1 = -1.0, xL1_low = -1.0, xL1_high = -1.0, xL4_low = -1.0, xL4_high = -1.0;
	
	for(int iedge=0; iedge<Ncells; iedge++){

	  if(z == z1){
	    xL1 =((z1 - z2)/(z - z2))*x;
	    if(iedge != Ncells){
	      int bin = h_HThits_xL4VsxL1->FindBin(xL1,ybins_arr[iedge]);
	      h_HThits_xL4VsxL1->SetBinContent(bin,h_HThits_xL4VsxL1->GetBinContent(bin)+1);
	      //std::cout << "bin: " << bin << " iL: " << iL << std::endl;
	      int binx=-1, biny=-1, binz=-1;
	      h_HThits_xL4VsxL1->GetBinXYZ(bin,binx,biny,binz);
	      if( (binx>0 && binx<Ncells+1) && (biny>0 && biny<Ncells+1) ){
		//std::cout << "ibin ibinx ibiny ibinz " << bin << " " << binx << " " << biny << " " << binz << std::endl; 
		HT_cells_hits[binx-1][biny-1][iL].push_back(chn);

		h_HTTotCharge_xL4VsxL1->SetBinContent(bin,h_HTTotCharge_xL4VsxL1->GetBinContent(bin)+ch_det[iL][chn]->GetAmplitude());
		
		if(ch_det[iL][chn]->GetRelBCID() == 3 || ch_det[iL][chn]->GetRelBCID() == 4 || ch_det[iL][chn]->GetRelBCID() == 5){
		  h_HThitsPot_xL4VsxL1->SetBinContent(bin,h_HThitsPot_xL4VsxL1->GetBinContent(bin)+1);
		  HT_cells_hitsPot[binx-1][biny-1][iL].push_back(chn);
		}
		else if(ch_det[iL][chn]->GetRelBCID() == 2 || ch_det[iL][chn]->GetRelBCID() == 6){
		  h_HThitsMask_xL4VsxL1->SetBinContent(bin,h_HThitsMask_xL4VsxL1->GetBinContent(bin)+1);
		  HT_cells_hitsMask[binx-1][biny-1][iL].push_back(chn);
		}
	      }
	    }
	  }
	  else{
	    xL1_low = xbins_arr[iedge];
	    xL4_low = ((z-z2)/(z-z1))*xL1_low + ((z2-z1)/(z-z1))*x;
	    xL1_high = xbins_arr[iedge+1];
	    xL4_high = ((z-z2)/(z-z1))*xL1_high + ((z2-z1)/(z-z1))*x;
	    
	    int bin_low = h_HThits_xL4VsxL1->FindBin(xL1_low,xL4_low);
	    int bin_high = h_HThits_xL4VsxL1->FindBin(xL1_high-1.6,xL4_high);
	    
	    int binx_low=-1, biny_low=-1, binz_low=-1;
	    h_HThits_xL4VsxL1->GetBinXYZ(bin_low,binx_low,biny_low,binz_low);

	    int binx_high=-1, biny_high=-1, binz_high=-1;
            h_HThits_xL4VsxL1->GetBinXYZ(bin_high,binx_high,biny_high,binz_high);

	    if(biny_low != biny_high){
	      int low = -1, high = -1;
	      if(biny_low < biny_high){
		low = biny_low;
		high = biny_high;
	      }
	      else if(biny_low > biny_high){
		low = biny_high;
		high = biny_low;
	      }
	      for(int ibiny=low; ibiny <= high; ibiny++){
		
		if( (binx_low>0 && binx_low<Ncells+1) && (ibiny>0 && ibiny<Ncells+1) ){
		  int ibin = h_HThits_xL4VsxL1->GetBin(binx_low,ibiny,binz_low);
		  h_HThits_xL4VsxL1->SetBinContent(ibin,h_HThits_xL4VsxL1->GetBinContent(ibin)+1);
		  //std::cout << "ibin binx_low ibiny ibinz " << ibin << " " << binx_low << " " << ibiny << " " << binz_low << std::endl;
		  HT_cells_hits[binx_low-1][ibiny-1][iL].push_back(chn);
		  h_HTTotCharge_xL4VsxL1->SetBinContent(ibin,h_HTTotCharge_xL4VsxL1->GetBinContent(ibin)+ch_det[iL][chn]->GetAmplitude());
		  
		  if(ch_det[iL][chn]->GetRelBCID() == 3 || ch_det[iL][chn]->GetRelBCID() == 4 || ch_det[iL][chn]->GetRelBCID() == 5){
		    h_HThitsPot_xL4VsxL1->SetBinContent(ibin,h_HThitsPot_xL4VsxL1->GetBinContent(ibin)+1);
		    HT_cells_hitsPot[binx_low-1][ibiny-1][iL].push_back(chn);
		  }
		  else if(ch_det[iL][chn]->GetRelBCID() == 2 || ch_det[iL][chn]->GetRelBCID() == 6){
		    h_HThitsMask_xL4VsxL1->SetBinContent(ibin,h_HThitsMask_xL4VsxL1->GetBinContent(ibin)+1);
		    HT_cells_hitsMask[binx_low-1][ibiny-1][iL].push_back(chn);
		  }
		}
	      }
	    }
	    else{
	      if( (binx_low>0 && binx_low<Ncells+1) && (biny_low>0 && biny_low<Ncells+1) ){
		h_HThits_xL4VsxL1->SetBinContent(bin_low,h_HThits_xL4VsxL1->GetBinContent(bin_low)+1);
		HT_cells_hits[binx_low-1][biny_low-1][iL].push_back(chn);
		h_HTTotCharge_xL4VsxL1->SetBinContent(bin_low,h_HTTotCharge_xL4VsxL1->GetBinContent(bin_low)+ch_det[iL][chn]->GetAmplitude());
		
		if(ch_det[iL][chn]->GetRelBCID() == 3 || ch_det[iL][chn]->GetRelBCID() == 4 || ch_det[iL][chn]->GetRelBCID() == 5){
		  h_HThitsPot_xL4VsxL1->SetBinContent(bin_low,h_HThitsPot_xL4VsxL1->GetBinContent(bin_low)+1);
		  HT_cells_hitsPot[binx_low-1][biny_low-1][iL].push_back(chn);
		}
		else if(ch_det[iL][chn]->GetRelBCID() == 2 || ch_det[iL][chn]->GetRelBCID() == 6){
		  h_HThitsMask_xL4VsxL1->SetBinContent(bin_low,h_HThitsMask_xL4VsxL1->GetBinContent(bin_low)+1);
		  HT_cells_hitsMask[binx_low-1][biny_low-1][iL].push_back(chn);
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

bool NSWHoughTx::isGoodCluster(NSWCluster* cl, std::string &reason, int clusterType){

  bool isValid = true;
  int num_chns = cl->GetNumChannel();

  int layer = -1;

  double pos_min_beam = 160*3.2 - 1.6;
  double pos_max_beam = 279*3.2;
  double mean_cluster = -1.0;

  if(clusterType==1)
    mean_cluster = cl->GetGaussianCentroid();
  else if(clusterType==2)
    mean_cluster = cl->GetMeanCaruana();
  
  /*if(mean_cluster<pos_min_beam || mean_cluster>pos_max_beam){
    isValid=(isValid && false);
    reason = "no_beam_area";
    return false;
  }*/

  if(cl->GetWidth()<3){
    isValid = (isValid && false);
    reason = "cluster_wid_lt3";
    return false;
  }

  double pos_min = -10000, pos_max = 10000;

  pos_min = cl->GetChannel(0)->GetStripPosition();
  pos_max = cl->GetChannel(num_chns-1)->GetStripPosition();

  if(clusterType==1)
    mean_cluster = cl->GetGaussianCentroid();
  else if(clusterType==2)
    mean_cluster = cl->GetMeanCaruana();

  /*if(mean_cluster<=pos_min || mean_cluster>=pos_max){
    isValid=(isValid && false);
    reason = "recopos_not_middle";
    //std::cout << "centroid not in beam area- layer: " << layer << " centroid: " << mean_cluster << std::endl;
    return false;
  }*/

  for(int ichn=0; ichn<num_chns; ichn++){
    NSWChannel* channel = cl->GetChannel(ichn);
    int chn_num = -1;
    bool is_chn_bad = false;
    
    if(fabs(ichn-cl->Getimax()) > 1) continue;
    
    if(channel==NULL){
      if(ichn==0){
	layer = cl->GetChannel(ichn+1)->GetLayer();
	chn_num = cl->GetChannel(ichn+1)->GetDetectorStrip()-1;
      }
      else if(ichn==num_chns-1){
	layer = cl->GetChannel(ichn-1)->GetLayer();
	chn_num= cl->GetChannel(ichn-1)->GetDetectorStrip()+1;
      }
      else{
	layer = cl->GetChannel(ichn+1)->GetLayer();
	chn_num= cl->GetChannel(ichn+1)->GetDetectorStrip()-1;
      }
    }
    else{
      layer = channel->GetLayer();
      chn_num = channel->GetDetectorStrip();
    }

    is_chn_bad = isChnBad(layer, chn_num);

    if(is_chn_bad){
      isValid = (isValid && false);
      reason = "chn_bad";
      return false;
    }

    if(channel==NULL) continue;
    
    if(sng){

      if( ichn==0 || ichn==(num_chns-1) ){ //edge channels are not neighbours
	
      }
      else if(channel->GetNeighbor()==0){ //middle ones are neighbours 
	isValid=(isValid && false);
	reason = "middle_no_neighbour";
	return false;
      }
    }
    
    //========== relbcid 0 1 7 in central strips events are 0 ===========//
    //std::cout << "MaxPdo Value: " << cl->GetMaxPDO() << " Pdo of imax: " << cl->GetChannel(cl->Getimax())->GetPDO() << std::endl;
    if( channel->GetRelBCID() == 0 || channel->GetRelBCID() == 1 || channel->GetRelBCID() == 7 ){
      //std::cout<< "fabs(ichn-cl->Getimax()): "<< fabs(ichn-cl->Getimax()) << " Cluster width: " << cl->GetWidth() << std::endl;
      isValid=(isValid && false);
      reason = "bkg_relbcid_chn";
      return false;
    }
    //else if(fabs(ichn-cl->Getimax()) > 1 && (channel->GetRelBCID() == 0 || channel->GetRelBCID() == 1 || channel->GetRelBCID() == 7)){
      //std::cout << "fabs(ichn-cl->Getimax()): " << fabs(ichn-cl->Getimax()) << " Cluster width: " << cl->GetWidth() << std::endl;
    //}

    //========== relbcid 0 1 7 events are 0 ===========// 
    /*if(channel->GetRelBCID() == 0 || channel->GetRelBCID() == 1 || channel->GetRelBCID() == 7){
      isValid=(isValid && false);
      reason = "bkg_relbcid_chn";
      return false;       
    }*/

  }

  return isValid;

}

std::vector< std::pair< std::pair<int,int>,double > > NSWHoughTx::Fill_HT(double minAmp, int minWid, int clusterType){
  
  Clear();
  
  int num_tracks = 0;
  std::vector< std::pair< std::pair<int,int>,double > > SumAmp_tracks;
  // Select the valid cells of the Hough Transform:
  for(int ix=1; ix<=Ncells; ix++){
    for(int iy=1; iy<=Ncells; iy++){
      
      std::vector<int> Validhits_perlayer(8,0), ValidPot_perlayer(8,0), ValidMask_perlayer(8,0);

      std::vector<bool> isLayerPresent(8), isLayer_Pot(8), isLayer_Mask(8);
      int num_Pot = -1, num_Mask = -1;
      for(int iL=4; iL<8; iL++){

	if(HT_cells_hitsPot[ix-1][iy-1][iL].size()!=0 || HT_cells_hitsMask[ix-1][iy-1][iL].size()!=0){
	  num_FilledHT++;
	}

	isLayerPresent[iL] = false;
	isLayer_Pot[iL] = false;
	isLayer_Mask[iL] = false;

	for(int ihit=0; ihit<HT_cells_hitsPot[ix-1][iy-1][iL].size(); ihit++){
	  Validhits_perlayer[iL] += 1;
	  ValidPot_perlayer[iL] += 1;
	}
	
	for(int ihit=0; ihit<HT_cells_hitsMask[ix-1][iy-1][iL].size(); ihit++){
	  Validhits_perlayer[iL] += 1;
	  ValidMask_perlayer[iL] += 1;
	}
      }
      
      bool found_1perlayer = true;
      bool small_angle = false;
      
      for(int iL=0; iL<8; iL++){
	if(Validhits_perlayer[iL] >= 1) isLayerPresent[iL] = true;
	if(ValidPot_perlayer[iL] >= 1) isLayer_Pot[iL] = true;
	else if(ValidMask_perlayer[iL] >= 1) isLayer_Mask[iL] = true;
	
      }
      
      found_1perlayer = (found_1perlayer 
			 && count(isLayerPresent.begin(), isLayerPresent.end(), true)>=3
			 && count(isLayer_Pot.begin(), isLayer_Pot.end(), true)>=3
                         && count(isLayer_Mask.begin(), isLayer_Mask.end(), true)<=1);
      
      num_Pot = count(isLayer_Pot.begin(), isLayer_Pot.end(), true);
      num_Mask = count(isLayer_Mask.begin(), isLayer_Mask.end(), true);

      double slope = (h_HThits_xL4VsxL1->GetYaxis()->GetBinCenter(iy) - h_HThits_xL4VsxL1->GetXaxis()->GetBinCenter(ix))/(ch_det[0][0]->GetZPosition(7,true,0) - ch_det[0][0]->GetZPosition(4,true,0));
      double angle = atan(slope)*180/M_PI;
      if(std::abs(angle) < 40){
	small_angle = true;
	//if(angle > 10)
	//  std::cout << "Angle: " << angle << std::endl;
      }

      if(!found_1perlayer) continue;
      num_found1perlayer++;
      
      if(!small_angle) continue;
      num_SmallAngleHT++;
      
      //=============================== Some prints ===================================//
      /*
	std::cout<< "angle: " << atan(slope) << std::endl; 
	
	std::cout << "Coordinates: " << h_HThits_xL4VsxL1->GetXaxis()->GetBinLowEdge(ix) << " " << h_HThits_xL4VsxL1->GetXaxis()->GetBinUpEdge(ix) << " " << h_HThits_xL4VsxL1->GetYaxis()->GetBinLowEdge(iy) << " " << h_HThits_xL4VsxL1->GetYaxis()->GetBinUpEdge(iy) << std::endl;
	std::cout << "Total Charge: " << h_HTTotCharge_xL4VsxL1->GetBinContent(ix,iy) << std::endl;
	
	std::cout << "num_Pot: " << count(isLayer_Pot.begin(), isLayer_Pot.end(), true) << " num_Mask: " << count(isLayer_Mask.begin(), isLayer_Mask.end(), true) << std::endl;
	
	for(int iL=4; iL<8; iL++){
	std::cout << "Potential strips for layer: " << iL << std::endl;
	for(int ihit=0; ihit<HT_cells_hitsPot[ix-1][iy-1][iL].size(); ihit++){
	std::cout << "ix iy layer channel: " << ix << " " << iy << " " << iL << " " << HT_cells_hitsPot[ix-1][iy-1][iL][ihit] << " charge: " << ch_det[iL][HT_cells_hitsPot[ix-1][iy-1][iL][ihit]]->GetAmplitude_mV() << std::endl;
	}
	std::cout << "Masked strips for layer: " << iL << std::endl;
	for(int ihit=0; ihit<HT_cells_hitsMask[ix-1][iy-1][iL].size(); ihit++){
	std::cout << "ix iy layer channel: " << ix << " " << iy << " " << iL << " " << HT_cells_hitsMask[ix-1][iy-1][iL][ihit] << " charge: " << ch_det[iL][HT_cells_hitsMask[ix-1][iy-1][iL][ihit]]->GetAmplitude_mV() << std::endl;
	}
	}
      */
      //==============================================================================//
      
      //std::cout << "ix iy " << ix << " " << iy << std::endl;
      
      std::tuple<double,double,double,double> tuple = std::make_tuple( h_HThits_xL4VsxL1->GetXaxis()->GetBinLowEdge(ix), h_HThits_xL4VsxL1->GetXaxis()->GetBinUpEdge(ix), h_HThits_xL4VsxL1->GetYaxis()->GetBinLowEdge(iy), h_HThits_xL4VsxL1->GetYaxis()->GetBinUpEdge(iy) );
      valid_HTcells.push_back(tuple);
      valid_HTbins.push_back( std::make_pair(ix,iy) );
      
      //===============================================================================================================//
      //============================================= Actual clustering ===============================================//
      //===============================================================================================================//
      
      std::vector<std::vector<int>> multicl_channels;
      std::vector<std::vector<double>> multicl_amps;
      multicl_channels.resize(8);
      multicl_amps.resize(8);
      
      for(int iL=4; iL<8; iL++){
	
	if(HT_cells_hits[ix-1][iy-1][iL].size() == 0){
	  continue;
	}
	
	int phys_min = *min_element(HT_cells_hits[ix-1][iy-1][iL].begin(), HT_cells_hits[ix-1][iy-1][iL].end());
	int phys_max = *max_element(HT_cells_hits[ix-1][iy-1][iL].begin(), HT_cells_hits[ix-1][iy-1][iL].end());
	
	for(int phys_chn=phys_min; phys_chn<=phys_max; phys_chn++){
	  multicl_channels[iL].push_back(phys_chn);
	  if(ch_det[iL][phys_chn]!=NULL){
	    multicl_amps[iL].push_back(ch_det[iL][phys_chn]->GetAmplitude());
	  }
	  else{
	    multicl_amps[iL].push_back(0);
	  }
	}
	
	int chn_low = phys_min-1;
	int num_miss_low = 0;
	while(num_miss_low<2){	    
	  if(num_miss_low==0){
	    if(ch_det[iL][chn_low]!=NULL && ch_det[iL][chn_low]->GetAmplitude()>0){
	      multicl_channels[iL].insert(multicl_channels[iL].begin(), chn_low);
	      multicl_amps[iL].insert(multicl_amps[iL].begin(), ch_det[iL][chn_low]->GetAmplitude());
	    }
	    else{
	      if(ch_det[iL][chn_low-1]!=NULL && ch_det[iL][chn_low-1]->GetAmplitude()>0){
		num_miss_low++;
	      }
	      else{
		break;
	      }
	    }
	  }
	  else if(num_miss_low==1){
	    if(ch_det[iL][chn_low]!=NULL && ch_det[iL][chn_low]->GetAmplitude()>0){
	      multicl_channels[iL].insert(multicl_channels[iL].begin(), chn_low);
	      multicl_amps[iL].insert(multicl_amps[iL].begin(), ch_det[iL][chn_low]->GetAmplitude());
	      num_miss_low=0;
	    }
	  }
	  chn_low--;
	}
	
	int chn_high = phys_max+1;
	int num_miss_high = 0;
	while(num_miss_high<2){
	  if(num_miss_high==0){
	    if(ch_det[iL][chn_high]!=NULL && ch_det[iL][chn_high]->GetAmplitude()>0){
	      multicl_channels[iL].push_back(chn_high);
	      multicl_amps[iL].push_back(ch_det[iL][chn_high]->GetAmplitude());
	    }
	    else{
	      if(ch_det[iL][chn_high+1]!=NULL && ch_det[iL][chn_high+1]->GetAmplitude()>0){
		num_miss_high++;
	      }
	      else{
		break;
	      }
	    }
	  }
	  else if(num_miss_high==1){
	    if(ch_det[iL][chn_high]!=NULL && ch_det[iL][chn_high]->GetAmplitude()>0){
	      multicl_channels[iL].push_back(chn_high);
	      multicl_amps[iL].push_back(ch_det[iL][chn_high]->GetAmplitude());
	      num_miss_high=0;
	    }
	  }
	  chn_high++;
	}
	
      }
      
      //===========================================================================================================//
      //===========================================================================================================//
      //===========================================================================================================//
      
      //===========================================================================================================//
      //========================Getting set of clusters per layer===================================================//
      //===========================================================================================================//  
      
      std::vector<std::vector<NSWCluster*>> HTcell_clusters;
      HTcell_clusters.resize(8);

      NSWCluster* cl = NULL;
      for (int layer=4; layer<8; layer++){
	if(multicl_channels[layer].size()==0) continue;
	int min_chn = multicl_channels[layer][0];
	int max_chn = multicl_channels[layer][multicl_channels[layer].size()-1];
	
	int chn=min_chn; // count channels
	
	//std::cout << "layer: " << layer << " min_chn: " << min_chn << std::endl;
	//std::cout << "layer: " << layer << " max_chn: " << max_chn << std::endl;
	
	do{
	  //std::cout << "channel in clusterization: " << chn << std::endl;
	  if (ch_det[layer][chn] != NULL && ch_det[layer][chn]->GetAmplitude()>0) { // there is a channel
	    if(ch_det[layer][chn+1] != NULL && ch_det[layer][chn]->GetAmplitude()>0){
	      if(ch_det[layer][chn]->GetNeighbor()==0 && ch_det[layer][chn+1]->GetNeighbor()==1){ // divide the cluster
		if (cl == NULL) { // start of new cluster
		  cl = new NSWCluster(ch_det[layer][chn]); // first channel 
		  cl->AddChannel(ch_det[layer][chn+1]);
		  chn+=2;
		} 
		else { // continuation cluster ended
		  cl->AddChannel(ch_det[layer][chn]);
		  
		  //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
		  cl->FitGaussian();
		  cl->FitCaruana();
		  //std::cout << "1: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
		  HTcell_clusters[layer].push_back(cl);
		  //}
		  //else{
		  //  delete cl;
		  //}
		  cl = NULL;
		  
		  cl = new NSWCluster(ch_det[layer][chn]);
		  cl->AddChannel(ch_det[layer][chn+1]);
		  chn+=2;
		}
	      }
	      
	      else if(ch_det[layer][chn]->GetNeighbor()==0 && ch_det[layer][chn+1]->GetNeighbor()==0){ // divide the cluster 
		if (cl == NULL) { // start of new cluster 
		  cl = new NSWCluster(ch_det[layer][chn+1]); // first channel
		  chn+=2;
		}
		else{
		  cl->AddChannel(ch_det[layer][chn]);
		  //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
		  cl->FitGaussian();
		  cl->FitCaruana();
		  //std::cout << "2: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
		  HTcell_clusters[layer].push_back(cl);
		  //}
		  //else{
		  //  delete cl;
		  //}
		  cl = NULL;
		  
		  cl = new NSWCluster(ch_det[layer][chn+1]);
		  chn+=2;
		}
	      }
	      else{
		if (cl == NULL) { // start of new cluster
		  cl = new NSWCluster(ch_det[layer][chn]); // first channel 
		  chn+=1;
		}
		else{
		  cl->AddChannel(ch_det[layer][chn]);
		  chn+=1;
		}
	      }
	    }
	    
	    else{
	      if(ch_det[layer][chn]->GetNeighbor()==0){
		if (cl == NULL) { 
		  chn+=1;
		}
		else{
		  cl->AddChannel(ch_det[layer][chn]);
		  //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
		  cl->FitGaussian();
		  cl->FitCaruana();
		  //std::cout << "3: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
		  HTcell_clusters[layer].push_back(cl);
		  //}
		  //else{
		  //  delete cl;
		  //}
		  cl = NULL;
		  chn+=1;
		}
	      }
	      else{
		if (cl == NULL) { // start of new cluster
		  cl = new NSWCluster(ch_det[layer][chn]); // first channel
		  chn+=1;
		}
		else{
		  cl->AddChannel(ch_det[layer][chn]);
		  chn+=1;
		}
	      }
	    }
	    
	  }
	  else{  // no channel 
	    if (cl != NULL) { // in a cluster
	      if ((chn+1<=max_chn) && (ch_det[layer][chn+1] != NULL && ch_det[layer][chn+1]->GetAmplitude()>0)){ // only one missing
		if(ch_det[layer][chn-1]->GetNeighbor()==1 && ch_det[layer][chn+1]->GetNeighbor()==1){
		  cl->AddChannel(NULL);
		  cl->AddChannel(ch_det[layer][chn+1]);
		  chn +=2;
		}
		else{ // cluster ended
		  
		  //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
		  cl->FitGaussian();
		  cl->FitCaruana();
		  //std::cout << "4: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
		  HTcell_clusters[layer].push_back(cl);
		  //}
		  //else{
		  //  delete cl;
		  //}
		  cl = NULL;
		  chn +=1;
		  
		}
	      }
	      else { // cluster ended
		//if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
		cl->FitGaussian();
		cl->FitCaruana();
		//std::cout << "5: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
		HTcell_clusters[layer].push_back(cl);
		//} 
		//else {
		//  delete cl;
		//}
		cl = NULL;
		chn +=2;
	      } // if 2 missing
	    } // in a cluster
	    else{
	      chn++;
	    }
	  } // no channel
	} while (chn<=max_chn);
	if (cl != NULL) { // last cluster in this layer
	  //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
	  cl->FitGaussian();
	  cl->FitCaruana();
	  //std::cout << "6: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
	  HTcell_clusters[layer].push_back(cl);
	  //} 
	  //else {
	  //  delete cl;
	  //}
	  cl = NULL;
	}
      }
      //================================================================================//
      //=============== End of Clustering taken from Michael's code in NSWClusterList ====================//
      //================================================================================//
      
      std::vector<bool> isCluster(8,false);
      
      for(int iL=4; iL<8; iL++){
	if(HTcell_clusters[iL].size()!=0){
	  isCluster[iL] = true;
	}
      }
      
      if(count(isCluster.begin(), isCluster.end(), true)>=3){
	num_isClusterHT++;
      }
      
      std::vector<std::vector<NSWCluster*>> final_HTcell_clusters;
      final_HTcell_clusters.resize(8);
      
      std::vector<bool> v_isGoodCluster(8,false);
      
      for (int layer=4; layer<8; layer++){
	int num_clusters_perlayer = 0;
	if(HTcell_clusters[layer].size()==0) continue;
	//std::cout << "num of clusters: " << HTcell_clusters[layer].size() << std::endl;
	for(int icl=0; icl<HTcell_clusters[layer].size(); icl++){
	  //std::cout << "//============ cluster number: " << icl << std::endl;
	  //if(isGoodCluster(HTcell_clusters[layer][icl])){
	  NSWCluster* cl = HTcell_clusters[layer][icl];
	    //std::cout << "isGood: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
	  num_clusters_perlayer++;	      
	  //v_isGoodCluster[layer] = true;
	  final_HTcell_clusters[layer].push_back(HTcell_clusters[layer][icl]);
	  //}
	}
	//std::cout << "layer: " << layer << " num_clusters_perlayer: " << num_clusters_perlayer << std::endl;
      }
      
      //if( !(count(v_isGoodCluster.begin(), v_isGoodCluster.end(), true)>=3) ) continue;
      //num_isGoodClusterHT++;*/
      std::pair< std::pair<int,int>,double > pair_HT = std::make_pair( std::make_pair(ix,iy), h_HTTotCharge_xL4VsxL1->GetBinContent(ix,iy) );
      SumAmp_tracks.push_back(pair_HT);
      multi_clusters.push_back(HTcell_clusters);
	
      //====================some printouts ======================//
      
      /*std::cout << "ix: " << ix << " iy: " << iy << std::endl;
	
	for (int layer=4; layer<8; layer++){
	std::cout << "Layer: " << layer << std::endl;
	for(int ichn=0; ichn<HT_cells_hits[ix-1][iy-1][layer].size(); ichn++){
	std::cout << "channel "<< ichn << ": " << HT_cells_hits[ix-1][iy-1][layer][ichn] << std::endl;
	}
	for(int icl=0; icl<final_HTcell_clusters[layer].size(); icl++){
	std::cout<< "Cluster number: " << icl << std::endl;
	
	int num_chns = final_HTcell_clusters[layer][icl]->GetNumChannel();
	for(int ichn=0; ichn<num_chns; ichn++){
	NSWChannel* channel = final_HTcell_clusters[layer][icl]->GetChannel(ichn);
	if(channel!=NULL){
	std::cout << "Channel num: " << channel->GetDetectorStrip() << ", RelBCID: " << channel->GetRelBCID() << std::endl;
	}
	}
	}
	}
      */
      //===============================================================================================//
      //===============================================================================================//  
      //===============================================================================================//  

    }
  }

  if(num_FilledHT==0){
    reject = true;
    reject_noFilledHT = true;
  }
  else if(num_found1perlayer==0){
    reject = true;
    reject_noHit1perlayer = true;
  }
  else if(num_SmallAngleHT==0){
    reject = true;
    reject_noSmallAngle =true;
  }
  else if(num_isClusterHT==0){
    reject = true;
    reject_noClusterHT =true;
  }
  /*else if(num_isGoodClusterHT==0){
    reject = true;
    reject_noGoodClusterHT =true;
    }*/
  else{
  }  

  return SumAmp_tracks;

}
