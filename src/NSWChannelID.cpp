// Stores the online channel ID
#include <stdio.h>
#include <string.h>
#include "NSWRead/NSWChannelID.h"
/*

   0DDD Teee eESS SSLL LRRR RGGG

D: data type: 0:L1A, 1: config, 2: monitor
T: technology (sTGC = 0, MM = 1) 
e: Endpoint type 0: pad, 1: strip, 2: Trig. Proc, 3: Pad trig, 
E: Eta (endcap) 0: eta=+1: endcap A, 1: eta=-1, endcap C
S: Sector number - 1, so it counts 0 - 15.
L: Layer (0 - 7)
R: Radius: PCB (0 - 15): FFME8 number
G: Channel Group: configured VMM number for output

*/

sTGCInput NSWChannelID::stgc; // static member
constexpr double NSWChannelID::GlobalZsTGClarge[8];
constexpr double NSWChannelID::GlobalZsTGCsmall[8];

NSWChannelID::NSWChannelID(unsigned int ID, unsigned char v, unsigned char c){
  channelID = ID;
  vmm = v;
  chn = c;
}


NSWChannelID::~NSWChannelID(){
  // nothing to do
}


void NSWChannelID::SetChannelID (unsigned dataType, unsigned technology, unsigned endpointType, unsigned eta, unsigned sector0, unsigned layer, unsigned radius, unsigned group){
  channelID = (group & 7) |
    ((radius & 15) << 3) |
    ((layer & 15) << 7) |
    ((sector0 &15) << 10 ) |
    ((eta & 1) << 14 ) |
    ((endpointType & 15) << 15) |
    ((technology & 1) << 19) |
    ((dataType & 7) << 20);
}

int NSWChannelID::GetSector(){ // signed 1-based sector number
  int sector = GetRawSector()+1;
  int eta = GetEta();   // 0: eta=+1: endcap A, 1: eta=-1, endcap C
  if (eta == 1) sector = -sector;
  return sector;
}

void NSWChannelID::SetChannelID(unsigned int ID, unsigned char v, unsigned char c){
  channelID = ID;
  vmm = v;
  chn = c;
}

double NSWChannelID::GetPitch (){
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0) 
    if (IsLarge()) {
      return PitchLarge; // 0.45mm
    } else {
      return PitchSmall; // 0.425 mm
    }
  } else { // sTGC
    return PitchsTGC;
  }
}
 
  
  /** @return the stereoangle of the layer, or 0 for eta layers */
float  NSWChannelID::GetStereoAngle(){
  int layer = GetLayer();
  if ((layer==0) | (layer==4)) return -1.5;
  if ((layer==1) | (layer==5)) return  1.5;
  return 0;
}

/** @return the strip length in mm */
float NSWChannelID::GetStripLength(){
  int ch0 = 5*2*8*64; // last channel of lower module
  int size = (GetRawSector() + 1) % 2;
  int strip = GetDetectorStrip();
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0) 
    if (size == 0) { // small
      if (strip<ch0){
	return 500 + (1319.2-500)*(strip/(float)ch0);
      } else {
	return 1321.1 + (1821.5 - 1321.1)*(strip-ch0)/(float)(3*2*8*64);
      }
    } else { // large
      if (strip<ch0){
	return 640 + (2008.5-640)*(strip/(float)ch0);
      } else {
	return 2022.8 + (2220 - 2022.8)*(strip-ch0)/(float)(3*2*8*64);
      }
    }
  } else { // sTGC
    return 1000; // TODO  fix it
  }
}

int NSWChannelID::GetDetectorStrip() {
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0)
    //if ((vmm == 0) && (chn < 2)) return -1; // 1st two chn are not connected
    if ((GetRadius()%2)==1) { // odd boards are not inverted
      ///return chn -2 + 64*vmm +512*GetRadius() - 2*(GetPCB()-1);
      return chn+64*vmm+512*GetRadius();
    } else {
      ///return 511 -(chn -2 + 64*vmm) +512*GetRadius() - 2*(GetPCB()-1);
      return 511-(chn+64*vmm)+512*GetRadius();
    }
  } else { // sTGC
    // QS1 strips: 1 .. 406, QS2: 1 .. 365, QS3: 1 .. 307, 1078 in total
    // QL1 strips: 1 .. 408, QL2: 1 .. 366, QL3: 1 .. 353, 1127 in total
    int stripOffsetSmall[3] = {0, 406, 406+365};
    int stripOffsetLarge[3] = {0, 408, 408+366};
    // QS1 wires L0: 1 .. 19, QS2: 1 .. 29, 37
    // QS1 wires L1: 1 .. 20, QS2: 1 .. 30, 37
    // QS1 wires L3: 1 .. 19, QS2: 1 .. 29, 38
    // QS1 wires L4: 1 .. 19, QS2: 1 .. 29, 38
    // QS1 pads: 1 .. 68 68 51 51

    int mappedchannel; mappedchannel=-1; // Sonia Kabana
    // mappedchannel == -1 means it is not existing in mapping.txt
    // it has no connection to detector but can be connected to electronics
    // there should be no other negative values than -1 in the mapping method 3
    
    int channelType = GetChannelType();
    int conf = GetConfigID();
    // printf ("ConfigID = %d, vmm = %d\n", conf, vmm);
    switch (channelType) {
    case STRIP :
      if (GetSector()%2==0) { // small sectors

	// please don't delete this commented out line: (it serves as reference)
	//return NSWChannelID::stgc.map_strip (GetEndcapName(), 'S', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn) -1 + stripOffsetSmall[GetRadius()];
	  
	mappedchannel = NSWChannelID::stgc.map_strip (GetEndcapName(), 'S', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn);
	
	//check negative values  (it should not become < -1 :)
	//if ( mappedchannel < -1 ) printf ("NSWChannelID mappedchannel is less than -1 mappedchannel = %d\n", mappedchannel);
        //if ( mappedchannel == 0  ) printf ("NSWChannelID mappedchannel in data is 0 mappedchannel = %d\n", mappedchannel);  
        
        if ( mappedchannel > 0 ) return (mappedchannel -1 + stripOffsetSmall[GetRadius()]);
	if ( mappedchannel < 0 ) return (mappedchannel);  
	if ( mappedchannel == 0 ) return (mappedchannel-1);  
    
      } else { // large sectors

	// please don't delete this (commented out) line: (it serves as reference)
	//return NSWChannelID::stgc.map_strip (GetEndcapName(), 'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn) -1 + stripOffsetLarge[GetRadius()];
	  
	mappedchannel = NSWChannelID::stgc.map_strip (GetEndcapName(), 'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn);

	//if ( mappedchannel < -1 ) printf ("NSWChannelID mappedchannel is less than -1 mappedchannel = %d\n", mappedchannel);
	//if ( mappedchannel == 0  ) printf ("NSWChannelID mappedchannel in data is 0 mappedchannel = %d\n", mappedchannel);
	if ( mappedchannel > 0 ) return (mappedchannel -1 + stripOffsetLarge[GetRadius()] );
	if ( mappedchannel < 0 ) return (mappedchannel);  
	if ( mappedchannel == 0 ) return (mappedchannel-1);  

      }
      break;
    case PAD :  

      // please don't delete this (commented out) line:  (it serves as reference)
      //return stgc.map_pad (GetEndcapName(), (GetSector()%2==0)?'S':'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn);
      
      mappedchannel = stgc.map_pad (GetEndcapName(), (GetSector()%2==0)?'S':'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn);

      // if ( mappedchannel < -1  ) printf ("NSWChannelID mappedchannel pad is less than -1 mappedchannel = %d\n", mappedchannel);
      //if ( mappedchannel == 0  ) printf ("NSWChannelID mappedchannel pad is 0 mappedchannel = %d\n", mappedchannel);

      if ( mappedchannel >= 0 ) return (mappedchannel -1);
      if ( mappedchannel < 0 ) return (mappedchannel);  

      break;
    case WIRE :  

      // please don't delete this (commented out) line: (it serves as reference)
      //return stgc.map_wire (GetEndcapName(), (GetSector()%2==0)?'S':'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn);
      
      mappedchannel = stgc.map_wire (GetEndcapName(), (GetSector()%2==0)?'S':'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn);

      //if ( mappedchannel < -1  ) printf ("NSWChannelID mappedchannel wire is less than -1 mappedchannel = %d\n", mappedchannel);
      //if ( mappedchannel == 0  ) printf ("NSWChannelID mappedchannel wire is 0 mappedchannel = %d\n", mappedchannel);

      if ( mappedchannel >= 0 ) return (mappedchannel -1);
      if ( mappedchannel < 0 ) return (mappedchannel);

      break;
    default:
      printf ("Invalid ChannelType %d for %d", GetChannelType(), channelID);
    }
    return -1; // if not a wire, pad, or strip
  }
}


/*int NSWChannelID::GetDetectorStrip() {
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0)
    ///if ((vmm == 0) && (chn < 2)) return -1; // 1st two chn are not connected
    if ((GetRadius()%2)==1) { // odd boards are not inverted
      ///return chn -2 + 64*vmm +512*GetRadius() - 2*(GetPCB()-1);
      return chn+64*vmm+512*GetRadius();
    } else {
      ///return 511 -(chn -2 + 64*vmm) +512*GetRadius() - 2*(GetPCB()-1);
      return 511-(chn+64*vmm)+512*GetRadius();
    }
  } else { // sTGC
    // QS1 strips: 1 .. 406, QS2: 1 .. 365, QS3: 1 .. 307, 1078 in total
    // QL1 strips: 1 .. 408, QL2: 1 .. 366, QL3: 1 .. 353, 1127 in total
    int stripOffsetSmall[3] = {0, 406, 406+365};
    int stripOffsetLarge[3] = {0, 408, 408+366};
    // QS1 wires L0: 1 .. 19, QS2: 1 .. 29, 37
    // QS1 wires L1: 1 .. 20, QS2: 1 .. 30, 37
    // QS1 wires L3: 1 .. 19, QS2: 1 .. 29, 38
    // QS1 wires L4: 1 .. 19, QS2: 1 .. 29, 38
    // QS1 pads: 1 .. 68 68 51 51
    int channelType = GetChannelType();
    int conf = GetConfigID();
    switch (channelType) {
      case STRIP :
	if (GetSector()%2==0) { // small sectors
	  return NSWChannelID::stgc.map_strip (GetEndcapName(), 'S', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn) -1 + stripOffsetSmall[GetRadius()];
	} else { // large sectors
	  return NSWChannelID::stgc.map_strip (GetEndcapName(), 'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn) -1 + stripOffsetLarge[GetRadius()];
	}
      break;
      case PAD :  
      return stgc.map_pad (GetEndcapName(), (GetSector()%2==0)?'S':'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn);
      break;
      case WIRE :  
      return stgc.map_wire (GetEndcapName(), (GetSector()%2==0)?'S':'L', GetPivotConfirm(), GetRadius(), GetLayer(), conf, chn);
      break;
    default:
      printf ("Invalid ChannelType %d for %d", GetChannelType(), channelID);
    }
    return -1; // if not a wire, pad, or strip
  }
}*/

/*double NSWChannelID::GetStripPosition(int chn){
  double pos = chn * GetPitch(); // position in mm
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0) 
    if (GetDetectorStrip() > 5*1024) pos += 5; // 5mm gap between panels
    //TODO: channel 0 and 1 in each pcb are not connected 
  } else { // sTGC
    // TODO: add gaps between panels from /afs/cern.ch/work/s/stgcic/public/swROD/muons_analysis/cosmic_ray_hitsmap/chnl_mapping/Large_Wedge
    if ((GetLayer() % 2) == 1){ // stagger by -0.5*pitch
      pos -= 0.5*GetPitch();
    }
  }
  return pos;
}

double NSWChannelID::GetStripPosition(){
  double pos = GetDetectorStrip() * GetPitch(); // position in mm
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0)
    if (GetDetectorStrip() > 5*1024) pos += 5; // 5mm gap between panels
    //TODO: channel 0 and 1 in each pcb are not connected
  } else { // sTGC
    // TODO: add gaps between panels from /afs/cern.ch/work/s/stgcic/public/swROD/muons_analysis/cosmic_ray_hitsmap/chnl_mapping/Large_Wedge
    if ((GetLayer() % 2) == 1){ // stagger by -0.5*pitch
      pos -= 0.5*GetPitch();
    }
  }
  return pos;
}*/

double NSWChannelID::GetStripPosition(int chn){
  double pos = chn * GetPitch(); // position in mm
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0)
    if (GetDetectorStrip() > 5*1024) pos += 5; // 5mm gap between panels
    //TODO: channel 0 and 1 in each pcb are not connected
  } else { // sTGC
    // TODO: add gaps between panels from /afs/cern.ch/work/s/stgcic/public/swROD/muons_analysis/cosmic_ray_hitsmap/chnl_mapping/Large_Wedge
    pos += 946.2;
    if ((GetLayer() % 2) == 1){ // stagger by -0.5*pitch
      pos -= 0.5*GetPitch();
    }
  }
  return 4432.9 - pos;
}

double NSWChannelID::GetStripPosition(){
  double pos = GetDetectorStrip() * GetPitch(); // position in mm
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0)
    if (GetDetectorStrip() > 5*1024) pos += 5; // 5mm gap between panels
    //TODO: channel 0 and 1 in each pcb are not connected
  } else { // sTGC
    // TODO: add gaps between panels from /afs/cern.ch/work/s/stgcic/public/swROD/muons_analysis/cosmic_ray_hitsmap/chnl_mapping/Large_Wedge
    pos += 946.2;
    if ((GetLayer() % 2) == 1){ // stagger by -0.5*pitch
      pos -= 0.5*GetPitch();
    }
  }
  return 4432.9 - pos;
}

double NSWChannelID::GetWireCenterPosition(){
  int quad = GetRadius();
  int layer = GetLayer();
  int phys_chn = GetDetectorStrip();

  if (quad != 0) {
    printf("Invalid quad and layer number %d and %d for channel %d", quad, layer);
    return -1;
  }

  double pos_ref = 570.54;
  double pos_diff;
  phys_chn = 33 - phys_chn;
  switch(layer){
    case 7:
      if (phys_chn == 1) pos_diff = 12.547;
      else if (phys_chn == 2) pos_diff = 31.447;
      else pos_diff = 31.447 + 20*1.8*(phys_chn-2);
      break;
    case 6:
      if (phys_chn == 1) pos_diff = 25.597;
      else if (phys_chn == 2) pos_diff = 57.097;
      else if (phys_chn == 32) pos_diff = 1123.597;
      else pos_diff = 57.097 + 20*1.8*(phys_chn-2);
      break;
    case 5:
      if (phys_chn == 1) pos_diff = 21.997;
      else if (phys_chn == 2) pos_diff = 48.997;
      else if (phys_chn == 32) pos_diff = 1119.997;
      else pos_diff = 48.997 + 20*1.8*(phys_chn-2);
      break;
    case 4:
      if (phys_chn == 1) pos_diff = 17.047;
      else if (phys_chn == 2) pos_diff = 39.547;
      else if (phys_chn == 32) pos_diff = 1115.047;
      else pos_diff = 39.547 + 20*1.8*(phys_chn-2);
      break;
    default:
      printf("Invalid quad and layer number %d and %d for channel %d\n", quad, layer);
      return -1;
  }
  // if(layer==1 || layer==4)
    return pos_ref-pos_diff;
  // else
  //   return -pos_ref+pos_diff;
}

double NSWChannelID::GetWireGroupHalfWidth(){

  int quad = GetRadius();
  int layer = GetLayer();
  int phys_chn = GetDetectorStrip();

  if (quad != 0) {
    printf("Invalid quad and layer number %d and %d for channel %d", quad, layer);
    return -1;
  }

  phys_chn = 33 - phys_chn;
  double half_width;
  switch(layer){
    case 7:
      if (phys_chn == 1) half_width = 1.8*0.5;
      else half_width = 1.8*10;
      break;
    case 6:
      if (phys_chn == 1) half_width = 1.8*7.5;
      else if (phys_chn == 32) half_width = 1.8*2.5;
      else half_width = 1.8*10;
      break;
    case 5:
      if (phys_chn == 1) half_width = 1.8*5;
      else if (phys_chn == 32) half_width = 1.8*5;
      else half_width = 1.8*10;
      break;
    case 4:
      if (phys_chn == 1) half_width = 1.8*2.5;
      else if (phys_chn == 32) half_width = 1.8*7.5;
      else half_width = 1.8*10;
      break;
    default:
      printf("Invalid quad and layer number %d and %d for channel %d\n", quad, layer);
      return -1;
  }
}

double NSWChannelID::GetWireUpperEdge(){
  return GetWireCenterPosition()+GetWireGroupHalfWidth();
}

double NSWChannelID::GetWireLowerEdge(){
  return GetWireCenterPosition()-GetWireGroupHalfWidth();
}

/*std::vector<float>& NSWChannelID::GetPadPosition() {
  int quad = GetRadius();
  int layer = GetLayer();
  int phys_chn = GetDetectorStrip();

  pad_pos.clear();

  if ( GetSector()%2==0 || GetPivotConfirm() != 'C' || quad != 0 ){
    printf("Quad type Q%c%d%c not supported yet.\n", GetSector()%2==0?'S':'L', GetPivotConfirm(), quad);
    return pad_pos;
  }

  pad_pos = stgc.mapPadPos(GetSector()%2==0?'S':'L', GetPivotConfirm(), quad, layer, GetDetectorStrip()).at(phys_chn);
  for(int i=0; i<4; i++){
    pad_pos[i] = 4432.9 - pad_pos[i];
    pad_pos[i+4] = - pad_pos[i+4];
  }

  //printf("layer %d phys_chn %d\n", GetLayer(), GetDetectorStrip());
  //for(int i=0; i<pad_pos.size(); i++){
  //  printf("  %4.2f", pad_pos.at(i));
  //}
  //std::cout<<std::endl;

  return pad_pos;
}*/


double NSWChannelID::GetZPosition() {
  return GetZPosition(GetLayer(), IsLarge(), GetTechnology());
}


double NSWChannelID::GetZPosition(int layer, bool isBig, int tech) {
  static constexpr double z[] = { 0.5*DZ_SPA+2*DZ_DRIFT+2*DZ_RO+3.5*DZ_GAS,
				  0.5*DZ_SPA+2*DZ_DRIFT+  DZ_RO+2.5*DZ_GAS,
				  0.5*DZ_SPA+  DZ_DRIFT+  DZ_RO+1.5*DZ_GAS,
				  0.5*DZ_SPA+  DZ_DRIFT+        0.5*DZ_GAS,
				  -0.5*DZ_SPA-  DZ_DRIFT-        0.5*DZ_GAS,
				  -0.5*DZ_SPA-  DZ_DRIFT-  DZ_RO-1.5*DZ_GAS,
				  -0.5*DZ_SPA-2*DZ_DRIFT-  DZ_RO-2.5*DZ_GAS,
				  -0.5*DZ_SPA-2*DZ_DRIFT-2*DZ_RO-3.5*DZ_GAS};
  if (tech == 1) { // (MM = 1, sTGC = 0)
    return z[layer];
  } else { // sTGC
    if (isBig){
      return GlobalZsTGClarge[layer] - AveZsTGClarge;
    } else { // small
      return GlobalZsTGCsmall[layer] - AveZsTGCsmall;
    }
  }
}


char NSWChannelID::GetPivotConfirm(){
  if (GetTechnology() == 1) { // (MM = 1, sTGC = 0)
    return '?'; // does not apply to MM
  } else { // sTGC
    if (((GetRawSector()+1)%2)==1) { // large chambers
      return (GetLayer()<4)?'P':'C';
    } else { // small chambers
      return (GetLayer()<4)?'C':'P';
    }
  }
}

int NSWChannelID::ParseString(char* string){
  int  sector, layer, radius, group;
  int itype, itech, iendpoint, ieta;
  //  sscanf (string, "NSW/%s/%s/%s/%c/S%d/L%d/R%d/G%d", "L1A", tech, endpoint, eta, &sector, &layer, &radius, &group);
  //int n = sscanf (string, "NSW/%s/%s/%s/%c02%d/L%d/R%d/G%d", dataType, tech, endpoint, eta, &sector, &layer, &radius, &group);
  
  // printf ("ParseString          '%s'\n", string);
  char label[40];
  char* ap = &label[0];
  char** a = &ap;
  strncpy (label, string, 39);
  char * p = strsep (a, "/");
  if (strcmp(p, "NSW")) {
    printf ("Cannot parse label: starts with '%s'\n", p);
    return 1; // parse error
  }
  p = strsep (a, "/");
  itype = -1;
  if (p[0] == 'L') itype = 0; // L1A
  if (p[0] == 'C') itype = 1; // Config
  if (p[0] == 'M') itype = 2; // Monitoring
  if (itype == -1) {
    printf ("Cannot parse label: dataType '%s'\n", p);
    return 1; // parse error
  }
  
  p = strsep (a, "/");
  itech = -1; // invalid
  if (p[0]=='M') itech = 1; // MM
  if (p[0]=='s') itech = 0; // sTGC
  if (p[0]=='S') itech = 0; // sTGC
  if (itech == -1) {
    printf ("Cannot parse label: technology '%s'\n", p);
    return 1; // parse error  
  }
  p = strsep (a, "/");
  iendpoint = -1; // invalid
  if ((p[0]=='p')||(p[0]=='P')){
    if ((p[1]=='a')||(p[1]=='A')) iendpoint = 0; // pad FEB
    if ((p[1]=='t')||(p[1]=='T')) iendpoint = 3; // pad Trigger
  }
  if ((p[0]=='s')||(p[0]=='S')) iendpoint = 1; // strip FEB
  if ((p[0]=='t')||(p[0]=='T')) iendpoint = 2; // trigger proc
  if ((p[0]=='L')||(p[0]=='l')) iendpoint = 4; // L1DDC
  if ((p[0]=='A')||(p[0]=='a')) iendpoint = 5; // ADDC
  if (iendpoint==-1) {
    printf ("Cannot parse label: endPoint '%s'\n", p);
    return 1; // error
  }
  p = strsep (a, "/");
  ieta = -1; // invalid
  if (p[0] == 'A') ieta = 0;
  if (p[0] == 'C') ieta = 1;
  if (ieta==-1) {
    printf ("Cannot parse label: eta '%c'\n", p[0]);
    return 1; // error
  }
  int n = sscanf (&p[1], "%02d", &sector);
  if (n != 1) {
    printf ("Cannot parse label: sector '%s'\n", p);
    return 1; // error
  }
  p = strsep (a, "/");
  n = sscanf (p, "L%d", &layer);
  if (n != 1) {
    printf ("Cannot parse label: layer '%s'\n", p);
    return 1; // error
  }
  p = strsep (a, "/");
  n = sscanf (p, "R%d", &radius);
  if (n != 1) {
    printf ("Cannot parse label: radius '%s'\n", p);
    return 1; // error
  }
  p = strsep (a, "/");
  n = sscanf (p, "G%d", &group);
  if (n != 1) {
    printf ("Cannot parse label: group '%s'\n", p);
    return 1; // error
  }
  SetChannelID (itype, itech, iendpoint, ieta, sector-1, layer, radius, group);
  return 0; // OK
}


int NSWChannelID::ParseBoardID(char* string){
  int sector;
  char endcap, rest[40];
  int n = sscanf(string, "%c%02d_%s", &endcap, &sector, rest);
  if (n != 3) {
    printf ("got only %d of 5 fields.\n", n);
    return -1;
  }
  if ((endcap=='C') || (endcap=='c')) {
    sector = -sector;
  } else {
    if ((endcap!='A') && (endcap!='a')) {
      printf ("Endcap is %c.\n", endcap);
      return -1;
    }
  }
  return ParseBoardID (rest, sector);
}


int NSWChannelID::ParseBoardID(char* string, int sector){ // MM only?
  int layer, pcb, radius;
  char chamber[20], output='#', side;
  int start = 1; // skipping the 'M'
  if (string[1] == '_') start = 2; // skipping the "M_"
  int n = sscanf(string+start, "L%1dP%1d_%2s%c_%c", &layer, &pcb, chamber, &side, &output);
  pcb = string[4] - '0'; // sscanf always returns 0. Why???
  //printf("'%s', n = %d: layer = %d, pcb = %d, chamber = %s, side = %c, output = %c\n", string, n, layer, pcb, chamber, side, output);
  if (n < 4) { // accept 4 or 5
    printf ("0: got only %d of 5 fields.\n", n);
    return -1;
  }
  if ((layer>4)||(layer<1)){
    printf ("Layer %d is outside of the range 1 - 4\n", layer);
    return -1;
  }
  if ((side!='L')&&(side!='R')) {
    printf ("Side is %c.\n", side);
    return -1;
  }
  if ((chamber[0]!='I')&&(chamber[0]!='H')) { // IP or HO
    printf ("Chamber is '%s'.\n", chamber);
    return -1;
  }

  if (chamber[0] == 'I') { // IP side
    layer = layer - 1; // start at 0
  } else { // HO side
    layer = 8 - layer; // 1 -> 7, 2 -> 6, 3 -> 5, 4 -> 4
  }

  if ((layer%2)==0) { // layer 0, 2, 4, 6
    if (side == 'L'){ // left side
      radius = 2*(pcb-1);
    } else { // right side
      radius = 2*(pcb-1)+1;
    }
  } else { // layer 1, 3, 5, 7
    if (side == 'L'){ // left side
      radius = 2*(pcb-1)+1;
    } else { // right side
      radius = 2*(pcb-1);
    }
  }

  SetDataTypeOld (0); // L1A readout
  SetTechnologyOld (1); // MM
  SetEndPointOld (1); // strip
  SetEtaOld ((sector>0)?0:1); //0: eta=+1: endcap A, 1: eta=-1, endcap C
  SetRawSector(sector>0?sector-1:-sector-1); // sector starts at 0 here
  SetLayer (layer);
  SetRadius(radius);
  if ((output=='A') || (output == '#')){ // first output
    SetChannelGroup(0);
  } else {
    if (output=='B') { // second output
      SetChannelGroup(1);
    } else {
      SetChannelGroup(2); // third output
    } 
  }
  return 0;
}

void NSWChannelID::GetEndPointString(char* string){
  int i = GetEndPoint();
  switch (i) {
  case 0: sprintf (string, "pad");
    break;
  case 1: sprintf (string, "strip");
    break;
  case 2: sprintf (string, "TProc");
    break;
  case 3: sprintf (string, "pTrig");
    break;
  case 4: sprintf (string, "L1DDC");
    break;
  case 5: sprintf (string, "ADDC");
    break;
  case 6: sprintf (string, "Router");
    break;
  default: sprintf (string, "???"); printf ("Unknown Endpoint = %d\n", i);
  }
}

void NSWChannelID::GetDetectorString (char* string){
  if (IsNew()){
    int det = GetDetectorID();
  switch (det) {
  case 0x6b: sprintf (string, "MM-A");
    break;
  case 0x6c: sprintf (string, "MM-C");
    break;
  case 0x6d: sprintf (string, "sTGC-A");
    break;
  case 0x6e: sprintf (string, "sTGC-C");
    break;
  default: sprintf (string, "???");
  }
  } else {
    sprintf (string, "???");
  }
}


void NSWChannelID::GetDataTypeString (char* string){
  if (IsNew()){
    int data =  GetDataType ();
  switch (data) {
  case 0: sprintf (string, "L1A");
    break;
  case 1: sprintf (string, "Monitor");
    break;
  case 2: sprintf (string, "to-SCA");
    break;
  case 3: sprintf (string, "from-SCA");
    break;
  case 4: sprintf (string, "TTC");
    break;
  case 5: sprintf (string, "L1Ainfo");
    break;
  case 6: sprintf (string, "Aux");
    break;
  case 7: sprintf (string, "Exc");
    break;
  default: sprintf (string, "???");
  }
  } else {
    sprintf (string, "???");
  }
}


void NSWChannelID::GetString(char* string){
  //  printf ("NSW/L1A/%s/%s/%c/S%d/L%d/R%d/G%d", GetTechnology()==0?"sTGC":"MM", "strip", GetEta()==0?'A':'C', GetRawSector()+1, GetLayer(), GetRadius(), GetChannelGroup());
  //sprintf (string, "NSW/L1A/%s/%s/%c/S%d/L%d/R%d/G%d", GetTechnology()==0?"sTGC":"MM", "strip", GetEta()==0?'A':'C', GetRawSector()+1, GetLayer(), GetRadius(), GetChannelGroup());
  char epString[10];
  GetEndPointString (epString);
  char typeString[10];
  GetDataTypeString (typeString);
  if (IsNew()){
    char detString[10];
    GetDetectorString (detString);
    sprintf (string, "%s/V%1d/%s/%s/S%d/L%d/R%d/E%d", detString, GetVersion(), typeString, epString, GetRawSector(), GetLayer(), GetRadius(), GetChannelGroup());
  } else {
    sprintf (string, "NSW/%3s/%s/%s/%c%02d/L%d/R%d/G%d", GetDataType()==0?"L1A":"MON", GetTechnology()==0?"sTGC":"MM", epString, GetEta()==0?'A':'C', GetRawSector()+1, GetLayer(), GetRadius(), GetChannelGroup());
  }
}

void NSWChannelID::GetBoardID(char* string){
  int layer = GetLayer();
  if (layer<4) { // IP
    layer = layer + 1; // count from 1
  } else { // OH
    layer = 8 - layer; // count from 1, but reverse
  }
  
  if (GetTechnology()==0) { // sTGC
    sprintf (string, "%c%02d_%2s_L%dQ%d%cFEB_%c",GetEta()==0?'A':'C', GetRawSector()+1, GetLayer()<4?"IP":"HO", GetLayer()%4+1, GetRadius()+1, GetEndPoint()==1?'s':'p', 'A'+GetChannelGroup());
  } else { // MM
    if (GetEndPoint() == 1){ // strip
      char side; // L or R
      if ((layer%2)==0) { // layer 0, 2, 4, 6
	side =  (GetRadius()%2)==0?'R':'L';
      } else  { // layer 1, 3, 5, 7
	side = (GetRadius()%2)==0?'L':'R';
      }
      sprintf (string, "%c%02d_ML%dP%d_%s%c_%c",GetEta()==0?'A':'C', GetRawSector()+1, layer, (GetRadius()/2)+1, GetLayer()<4?"IP":"HO", side, 'A'+GetChannelGroup());
    } else { // not strip
      if (GetEndPoint() == 2) {
	if (GetDataType()==2){ // monitoring
	  sprintf (string, "MM_TPMon"); // monitoring
	} else {
	  sprintf (string, "MM_TP");   // trigger segments
	}
      } else {
	sprintf (string, "Endpoint%d", GetEndPoint());
      }
    }
  }
}
 
int NSWChannelID::GetIndex() {
  int radius = GetRadius();
  int group = GetChannelGroup();
  int index = -1;
  if (GetTechnology() == 1){ // MM
    index = (radius<12) ? radius : 12+2*(radius-12)+group;
    return 20 * GetLayer() + index;
  } else { // sTGC
    int endpoint = GetEndPoint(); //  0: pad, 1: strip
    index = (radius<1) ? 4*radius + 2*endpoint + group : 4+2*(radius-1)+endpoint;
    return 8 * GetLayer() + index;
  }
}

unsigned int NSWChannelID::GetDatabaseID(){
  unsigned int lower = (GetRadius() << 9) | (vmm << 6) | chn;
  unsigned int upper = (GetEta() << 7) | (GetRawSector() << 3) | GetLayer();
  if (GetTechnology() ==  1) { //sTGC = 0, MM = 1
    return (upper << 13) | lower;
  } else { // sTGC
    lower |= (GetEndPoint() << 11);
    return (1 << 21) | (upper << 12) | lower;
  }
}

void NSWChannelID::Print(){
  char string[40];
  char boardID[20];
  GetString  (string); // official string representation
  GetBoardID (boardID);
  printf ("%8u: %s %s VMM %d, chn %d strip %d\n", channelID, boardID, string, vmm, chn, GetDetectorStrip());
}

