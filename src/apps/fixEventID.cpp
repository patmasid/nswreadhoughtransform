// reading NSWEvents and fix the missing global ID
// 
#include <stdio.h>


#include "NSWRead/NSWEvent.h"

#define maxSize 21000000/4 // 4000*32   // for event buffer

int main(int argc, char *argv[]){

  FILE* infile, *outfile;
  NSWEvent event;
  int eventCount = 0;
  int errorCount = 0;
  int size ; // event size
  //unsigned int buffer[maxSize];
  unsigned int * buffer;
  
  if (argc==2) {
    printf ("usage: fixEventID infile outfile\n");
    printf ("reads NSWEvents and fixes the missing global ID.\n");
    return 1;
  }

  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  infile = fopen (argv[1], "rb");
  if (!infile) {
    printf ("Cannot open '%s'\n", argv[1]);
    return 1;
  }

  outfile = fopen (argv[2], "wb");
  if (!outfile) {
    printf ("Cannot open '%s'\n", argv[2]);
    return 1;
  }

  int n = 0;

  do{ //event loop
    try{
      size = event.ReadATLAS(infile, buffer, maxSize);
    }
    catch (NSWReadException& ex){
      printf ("NSWRead: %s\n", ex.what());
      break;
    }

    try {
      event.ReadNSW(buffer, size);
      //event.Print();
      
      NSWSector* sec = event.GetSector(0);
      if (sec->GetActiveStrips(80) > 15) {
	event.FixGlobalID(n);
	fwrite (buffer, 4, size, outfile); // write event to file
	n++;
      }
    } 
    catch (NSWReadException& ex){
      printf ("ReadNSWException: ");
      printf("%s\n", ex.what());
      errorCount++;
    }
    eventCount++;
  } while (size>0); // next event

  printf ("Read %i events\n", eventCount);
  printf ("Encountered %i exceptions\n", errorCount);
  printf ("Wrote %d events.\n", n);
  fclose (infile);
  delete [] buffer;
}
