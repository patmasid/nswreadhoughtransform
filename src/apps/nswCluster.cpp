/** @file nswCluster.cpp
This application fills a root tree with hit reconstruction results.
It can be used for monitoring, or for looking at hit reconstruction
results. The tree variables are stored in moni_t.

Reads nsw data file and produces monitoring histograms based on clusters.
The tree is filled after cluster finding and there is one entry per cluster.

@see moni.cpp for monitoring packet data
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "TH2.h"
//#include" TGraph.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TProfile.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWCalibration.h"


/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


/** read  data file, produce root file for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer

  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;
  int numCluster = 0;
  FILE *infile;

  if(argc <2){
    printf("Usage: nswCluster file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWCalibration cal;
  if (cal.ReadPDO(argv[1]) != 1){
    printf ("Cannot read calibration file.\n");
    return 1;
  }

  NSWEvent event;
  NSWSector* sec;
  NSWClusterList clusterList;
  
  strcpy(fils,argv[2]);
  fils[(strlen(argv[2])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".clust.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }

  /** \struct moni_t
      defines variables in root tree.
      One entry per ROD cluster.
  */
  struct data{
    /** event number, increments over all files. */
    int event;
    /** Level one ID */
    int l1id;
    /** bcid */
    int bcid;
    /** MMFE8 board radius position */
    int radius;
    /** layer */
    int layer;
    /** number of channels of this cluster */
    int wid;
    /** starting channel of this cluster */
    int start;
    /** peak channel of this cluster */
    int peak;
    /** number of missing channels of this cluster */
    int gap;
    /** electrode type: strips, wires, pads */
    int type;
    /** cluster parameters:*/
    float cog, aveAmp, minTime, maxTime;
    /** amplitudes */
    float minPDO, maxPDO, totAmp;
  };
  data branch;

  TH1F * dist[3][8];
  TH1F * occ[3][8];
  TH2F * time2d[3][8], *amp2d[3][8];
  TH1F * occs[3];
  char name[80], title[80];
  const char* tech[3] = {"MM_strips", "sTGC_strips", "sTGC_wires"};
  const int imax[3]   = {8192,           1200,            50};
  double omax[3]= {0.001, 0.001, 0.03};
  for (int t=0; t<3; t++){ // loop over "MM", "sTGC strip", "sTGC wire"
    for (int i=0; i<8; i++) {
      sprintf (name, "%s_occ%d", tech[t], i);
      sprintf (title, "%s occupancy of layer %d;strip;Occupancy", tech[t], i);
      occ[t][i] = new TH1F (name, title, imax[t], 0, imax[t]);
      sprintf (name, "%s_dist%d", tech[t], i);
      sprintf (title, "%s distance between clusters of layer %d;dist in strips", tech[t], i);
      dist[t][i] = new TH1F (name, title, 100, 0, 100);
      
      sprintf (name, "%s_time2d%d", tech[t], i);
      sprintf (title, "%s drift time of layer %d;strip;time in ns", tech[t], i);
      time2d[t][i] = new TH2F (name, title, imax[t], 0, imax[t], 512, -25, 7*25);
      sprintf (name, "%s_amp2d%d", tech[t], i);
      sprintf (title, "%s PDO of layer %d;strip;PDO", tech[t], i);
      amp2d[t][i] = new TH2F (name, title, imax[t], 0, imax[t], 512, 0, 1024);
    }  
    sprintf (name, "occs%d", t);
    sprintf (title, "%s log occupancy;log(hits/evt)", tech[t]);
    occs[t] = new TH1F(name, title, 400, -20, 0);
  }
  

  TTree *tree = new TTree("tree","Clusters");
  tree->Branch("branch",&branch,"event/I:l1id:bcid:radius:layer:wid:start:peak:gap:type:cog/F:aveAmp:minTime:maxTime:minPDO:maxPDO:totAmp");

  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data

	//fill hitlist with cluster data
	branch.event = eventNumber;
	for (int k=0; k<event.GetNumSector(); k++){
	  NSWSector* sec = event.GetSector(k);
	  branch.l1id = sec->GetLevel1ID();
	  branch.bcid = sec->GetBCID();
	  cal.Calibrate(sec);
	  clusterList.Clear();
	  clusterList.Fill(sec, 20, 1); // 20 adc min, 2 channels min

	  
	  for (int layer =0; layer<8; layer++) {
	    for (int i=0; i<clusterList.GetNumClusters(layer); i++){
	      NSWCluster*  clu = clusterList.GetCluster(layer, i);
	      int t = -1; // "MM_strips", "sTGC_strips", "sTGC_wires"
	      if (clu->GetTechnology() == 1) { // MM
		t = 0;
	      } else { // "sTGC_strips", "sTGC_wires"
		if (clu->GetChannelType() == NSWChannelID::STRIP) t = 1;
		if (clu->GetChannelType() == NSWChannelID::WIRE) t = 2;
	      }
	      
	      branch.gap = clu->GetNumMissing();
	      branch.wid = clu->GetNumChannel();
	      branch.peak = clu->GetPeakChannel();
	      branch.start = clu->GetChannel(0)->GetDetectorStrip();
	      branch.cog = clu->GetCOG();
	      branch.aveAmp = clu->GetAverageAmplitude();
	      branch.minTime = clu->GetMinimumTime();
	      branch.maxTime = clu->GetMaximumTime();
	      branch.minPDO = clu->GetMinPDO();
	      branch.maxPDO = clu->GetMaxPDO();
	      branch.totAmp = clu->GetTotalAmplitude();
	      branch.layer = layer;
	      branch.radius = clu->GetRadius();
	      branch.type = clu->GetChannelType();
	      tree->Fill();
	      for (int j=0; j<clu->GetNumChannel(); j++){
		NSWChannel* chn = clu->GetChannel(j);
		if (chn == NULL) continue;
		occ[t][layer]->Fill(chn->GetDetectorStrip());
		amp2d[t][layer]->Fill(chn->GetDetectorStrip(), chn->GetAmplitude());
		time2d[t][layer]->Fill(chn->GetDetectorStrip(), chn->GetPeakingTime());
		if (i>0) {
		  NSWCluster*  c0 = clusterList.GetCluster(layer, i-1);
		  dist[t][layer]->Fill(abs(clu->GetDetectorStrip()-c0->GetDetectorStrip()-c0->GetWidth()));
		}
	      } // next channel
	    } // next cluster
	    numCluster += clusterList.GetNumClusters(layer);
	  } // next layer
	} // next sector

	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	eventNumber++;
      } // try     
      catch (NSWReadException& ex){
	printf ("ReadNSW() encountered an Exception for event %d\n", eventNumber);
	printf("%s\n", ex.what());
	errorCount++;
      }
    } while (size>0); // next event
    fclose(infile);
  } // next file
  tree->Write();

  for (int t=0; t<3; t++) {
    f->mkdir(tech[t]);

    f->cd(tech[t]);
    for (int layer=0; layer<8; layer++) {
      occ[t][layer]->Scale(1.0/eventNumber);
      for (int j=0; j<512; j++){ //convert occ to log
	double o=occ[t][layer]->GetBinContent(j+1);
	if (o>0)  occs[t]->Fill(log(o));
      }
      //occ[t][layer]->GetYaxis()->SetRangeUser(0, omax[t]);
      occ[t][layer]->Write();
      amp2d[t][layer]->Write();
      time2d[t][layer]->Write();
      dist[t][layer]->Write();
    }
    occs[t]->Write();
  }
  f->cd("/");
  f->Close();
  delete [] buffer;
  double ratio = 0;
  if (numCluster>0) ratio = (double)numCluster/eventNumber;
  printf("\nTotal number of events = %d with %d clusters, %6.1f clusters per event\n", eventNumber, numCluster, ratio);
  printf ("Encountered %i exceptions\n", errorCount);
}

