/** @file nswclean.cpp
This application removes events with missingVMM errors and those with 
exceptions. It can be used to combine several runs into one file.
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"NSWRead/NSWEvent.h"



/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32



int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer

  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;
  int exceptionCount = 0;
  int outCount = 0;
  FILE *infile, *outfile;

  if (argc < 3){
    printf("Copies events without missingVMM errors and exceptions.\n");
    printf("Usage: nswclean output.data input1.data [input2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWEvent event;

  outfile = fopen(argv[1],"wb");
  if (outfile==NULL) {
    printf("Can't open output file '%s'.\n", argv[1]);
    exit(-1);	
  }
    
  for (int iFile=2; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }

    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	 if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data
	int miss = 0;
	for (int i=0; i<event.GetNumSector(); i++){ // loop over sectors
	  NSWSector* sec = event.GetSector(i);
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac =sec->GetPacket(j);
	    if (pac->GetMissing(true) > 0) miss++; // count missingVMM
	  } // next packet
	} // next sector
	if (miss == 0) { // write event out:
	  fwrite (buffer, 4, size, outfile); // write event to file
	  outCount++;
	} else { // count bad events
	  errorCount ++;
	}

	eventNumber++;
	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
      } // try     
      catch (NSWReadException& ex){
	printf ("Exception for event %d\n", eventNumber);
	printf("%s\n", ex.what());
	exceptionCount++;
      }
    } while (size>0); // next event
    fclose(infile);
  } // next file

  fclose(outfile);
  delete [] buffer;  

  printf("\nTotal number of events = %d, events with missingVMM errors = %d, %f percent.\n", eventNumber, errorCount, 100*errorCount/(float)eventNumber);
  printf ("Encountered %i exceptions, wrote %d clean events\n", exceptionCount, outCount);
}

