/** @file mmcog.cpp
This application fills a root tree with segment reconstruction results.
It fills residual histograms for center of gravity position reconstruction.


@see nswseg.cpp for general segment data
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include "TH2.h"
//#include" TGraph.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWSegmentList.h"

/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32



void GG2(TH1F* h);
double mmCOG(NSWCluster* c, double sat, double base);
double mmCOGfill(NSWCluster* c, double sat, double base);

/** read  data file, produce root file for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer


  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;

  FILE *infile;

  if(argc <2){
    printf("Usage: mmcog file1.data [file2.data]...\n");
    printf("       fills histograms with residuals from center of gravity\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }


  NSWEvent event;
  NSWSector* sec;
  NSWClusterList clusterList;
  
  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".mmcog.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }

  /** \struct moni_t
      defines variables in root tree.
      One entry per ROD cluster.
  */
  struct data_t{
    /** event number, increments over all files. */
    int event;
    /** Level one ID */
    int l1id;
    /** number of precision layer clusters */
    int nprec;
    /** number of clusters */
    int nclu;
    /** first layer of the pair */
    int layer;
    /** number of channels of this cluster */
    int wid[2];
    /** missing channels in a cluster */
    int gap[2];
    /** cluster parameters:*/
    float cog[2];
    /** better cog*/
    float cog2[2];
    /** cluster charge*/
    float charge[2];
    /** line fit */
    float inter, slope, chi2, angle;
    /** median y positon */
    float y;
    /** PDO and TDO per channel*/
    //float adc[2][10], time[2][10];
  };
  data_t data;


  TTree *tree = new TTree("tree","mmCOG");
  tree->Branch("data",&data,"event/I:l1id:nprec:nclu:layer:wid[2]:gap[2]:cog[2]/F:cog2[2]:charge[2]:inter:slope:chi2:angle:y");

  TH1F * cogres[70];
  char name[80], title[80];
  for (int i=-35; i<35; i++) {
    sprintf (name, "cogres%02d", i+35);
    sprintf (title, "COG Residual %d degrees;Residual in mm", i);
    cogres[i+35] = new TH1F(name, title, 1000, -2, 2);
  }

  TH1F * satres[1024][70];
  for (int s=1; s<1024; s++){
    for (int i=-35; i<35; i++) {
      sprintf (name, "sat%04dres%02d", s, i+35);
      sprintf (title, "mmCOG (s=%d) Residual %d degrees;Residual in mm", s, i);
      satres[s][i+35] = new TH1F(name, title, 1000, -2, 2);
    }
  }
  TH1F* hpdo = new TH1F("hpdo", "PDO;PDO", 1024, 0, 1024);
	  
  int totSeg = 0;
  int numSeg = 0; // per event
  int gifNumber = 0;
  
  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data

	//fill hitlist with cluster data
	data.event = eventNumber;
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  data.l1id = sec->GetLevel1ID();
	  clusterList.Clear();
	  clusterList.Fill(sec, 20, 2);
	  int nClu = 0;
	  for (int layer=0; layer<8; layer++){
	    nClu += clusterList.GetNumClusters(layer);
	    for (int i=0; i<clusterList.GetNumClusters(layer); i++){
	      NSWCluster *c = clusterList.GetCluster(layer, i);
	    }
	  }

	  NSWSegmentList segmentList;
	  segmentList.Clear();
	  segmentList.Fill (&clusterList, 10);
	  for (int i=0; i<segmentList.GetNumSegments(); i++){
	    NSWSegment* s = segmentList.GetSegment(i);
	    if (s->GetNumPrecision() != 4) continue;
	    data.y =  s->GetYPosition();
	    data.layer = 0; // for layer 0 and 1
	    data.inter = s->GetIntercept();
	    data.slope = s->GetSlope();
	    data.chi2 = s->GetChi2();
	    data.angle = s->GetAngle();
	    data.nprec = s->GetNumPrecision();
	    data.nclu = s->GetNumCluster();
	    
	    for (int layer=0; layer<2; layer++){
	      NSWCluster* c = s->GetCluster(layer);
	      if (c == NULL) continue;
	      data.gap[layer] = c->GetNumMissing();
	      data.cog[layer] = c->GetCOG();
	      data.cog2[layer] = c->GetCOG2();
	      data.charge[layer] = c->GetCharge();
	      data.wid[layer] = c->GetNumChannel();
	      for (int ch=0; ch < c->GetNumChannel(); ch++){
		NSWChannel *chn = c->GetChannel(ch);
		if (chn != NULL) hpdo->Fill(chn->GetPDO());
	      }
	    } // next layer
	    int ang = s->GetAngle() + 35;
	    if ((ang>=0)&&(ang<70)){
	      cogres[ang]->Fill((data.cog[0]-data.cog[1]-16.6691*data.slope-0.2363)/sqrt(2.0)*0.45);
	      for (int sat=1; sat<1024; sat++){
		satres[sat][ang]->Fill((mmCOGfill(s->GetCluster(0), sat, 32)-mmCOGfill(s->GetCluster(1), sat, 32)-16.6691*data.slope-0.2363)/sqrt(2.0)*0.45);
	      }
	    }
	    tree->Fill(); 
	    numSeg++;
	    totSeg++;
	  } // next segment
	} // next sector

	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	eventNumber++;
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0); // next event
    fclose(infile);
  } // next file
  tree->Write();
  TH1F* hres = new TH1F("hres", "COG resolution vs angle;angle in degrees;resolution in mm", 70, -35, 36);
  TH1F* hspoil = new TH1F("hspoil", "COG unspoiled vs angle;angle in degrees;unspoiled clusters in percent", 70, -35, 36);
  TH1F* hrms = new TH1F("hrms", "COG residual RMS vs angle;angle in degrees;RMS in mm", 70, -35, 36);
  
  for (int i=0; i<70; i++){
    GG2(cogres[i]);
    TF1* gg = cogres[i]->GetFunction("gg");
    hres->SetBinContent(1+i, gg->GetParameter(2));
    hres->SetBinError(1+i, gg->GetParError(2));
    double nGood = sqrt(2*3.141592)*gg->GetParameter(0)*gg->GetParameter(2)/cogres[i]->GetBinWidth(2);
    hspoil->SetBinContent(1+i, 100.0*nGood/cogres[i]->GetEntries());
    hrms->SetBinContent(1+i, cogres[i]->GetRMS());
    cogres[i]->Write();
  }
  hres->Fit("pol2", "Q");
  hres->Write();
  hspoil->Write();
  hrms->Write();

  TH1F* sres[1024];
  TH1F* sspoil[1024];
  TH1F* srms[1024];
  TH1F* s0 = new TH1F("s0", "Resolution curve minimum;saturation", 1024,0,1024); 
  TH1F* s2 = new TH1F("s2", "Resolution curve quadratic term;saturation", 1024,0,1024); 
  TH1F* a0 = new TH1F("a0", "Resolution for 0 degrees;saturation", 1024,0,1024); 
  TH1F* a10 = new TH1F("a10", "Resolution for 10 degrees;saturation", 1024,0,1024); 
  TH1F* a20 = new TH1F("a20", "Resolution for 20 degrees;saturation", 1024,0,1024); 
  TH1F* a30 = new TH1F("a30", "Resolution for 30 degrees;saturation", 1024,0,1024); 
  for (int sat=1; sat<1024; sat++){
    sprintf (name, "sres%04d", sat);
    sprintf (title, "mmCOG residual sat = %d;angle in degrees;Residual in mm", sat);
    sres[sat] = new TH1F(name, title, 70, -35, 36);
    sprintf (name, "sspoil%04d", sat);
    sprintf (title, "mmCOG unspoiled  sat = %d;angle in degrees;unspoiled clusters in percent", sat);
    sspoil[sat] = new TH1F(name, title, 70, -35, 36);
    sprintf (name, "srms%04d", sat);
    sprintf (title, "mmCOG residual RMS sat = %d;angle in degrees;RMS in mm", sat);
    srms[sat] = new TH1F(name, title, 70, -35, 36);
    for (int i=0; i<70; i++){
      GG2(satres[sat][i]);
      TF1* gg = satres[sat][i]->GetFunction("gg");
      sres[sat]->SetBinContent(1+i, gg->GetParameter(2));
      sres[sat]->SetBinError(1+i, gg->GetParError(2));
      double nGood = sqrt(2*3.141592)*gg->GetParameter(0)*gg->GetParameter(2)/cogres[i]->GetBinWidth(2);
      sspoil[sat]->SetBinContent(1+i, 100.0*nGood/satres[sat][i]->GetEntries());
      srms[sat]->SetBinContent(1+i, satres[sat][i]->GetRMS());
    }
    sres[sat]->Fit("pol2", "Q");
    TF1* pol2 = sres[sat]->GetFunction("pol2");
    s0->SetBinContent(1+sat, pol2->GetParameter(0));
    s0->SetBinError(1+sat, pol2->GetParError(0));
    s2->SetBinContent(1+sat, pol2->GetParameter(2));
    s2->SetBinError(1+sat, pol2->GetParError(2));
    
    sres[sat]->Write();
    sspoil[sat]->Write();
    srms[sat]->Write();
    a0->SetBinContent(1+sat, pol2->GetParameter(0));
    a10->SetBinContent(1+sat, pol2->GetParameter(0)+100*pol2->GetParameter(2));
    a20->SetBinContent(1+sat, pol2->GetParameter(0)+400*pol2->GetParameter(2));
    a30->SetBinContent(1+sat, pol2->GetParameter(0)+900*pol2->GetParameter(2));
  } // next sat
    
  s0->Write();
  s2->Write();
  a0->Write();
  a10->Write();
  a20->Write();
  a30->Write();
  hpdo->Write();
  f->Close();
  delete [] buffer;  
  printf("number of events   = %d\n", eventNumber);
  printf("number of segments = %d, %5.3f segments/event.", totSeg, totSeg/(double)eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}



void GG2(TH1F* h){
  char line[80];
  int i;
  float center, edge, nGood;

  TF1* gg = new TF1("gg", "[0]*exp(-0.5*((x-[1])/[2])^2)+[3]*exp(-0.5*(x/[4])^2)", -2, 2);
  gg->SetParNames ("Constant", "Mean", "Sigma", "Background", "Sigma Bkgnd");

  center = 0;
  int nbin = h->GetNbinsX();
  for (i=-5; i<=5; i++) center += h->GetBinContent (nbin/2+1+i);
  center /= 10;
  edge=0;
  for (i=0; i<nbin/10; i++) edge += h->GetBinContent(i+1);
  edge /= (nbin/10);
  //if (edge<center/10) edge=center/10;
  //printf ("center = %f, edge = %f\n", center, edge);
  gg->SetParameters(center-edge, 0, 0.25, edge, 1.2);

  h->Fit("gg", "LQ");
}

 double mmCOG(NSWCluster* c, double sat, double base){
  double cog = 0;
  double wsum = 0;
  if (c == NULL) return -99;
  for (int i=0; i<c->GetNumChannel(); i++){
    NSWChannel *chn = c->GetChannel(i);
    if (chn != NULL) {
      double pdo = chn->GetPDO();
      if (pdo > sat) pdo = sat; // saturation
      pdo -= base;              // baseline subtraction
      int strip = chn->GetDetectorStrip();
      cog += strip * pdo;
      wsum += pdo;
    }
  }
  return cog / wsum;
}
      

 double mmCOGfill(NSWCluster* c, double sat, double base){
  double cog = 0;
  double wsum = 0;
  double pdo;
  int strip;
  if (c == NULL) return -99;
  int start =  c->GetChannel(0)->GetDetectorStrip();
  for (int i=0; i<c->GetNumChannel(); i++){
    NSWChannel *chn = c->GetChannel(i);
    if (chn != NULL) {
      pdo = chn->GetPDO();
      strip = chn->GetDetectorStrip();
      /*if (strip!=start+i) {
	printf ("\nstrip = %d, n = %d\n", strip, start+i);
	// c->Print();
	}*/
    } else {
      pdo = sat*0.8;
      strip = start + i;
    }
    if (pdo > sat) pdo = sat; // saturation
    pdo -= base;              // baseline subtraction
    cog += strip * pdo;
    wsum += pdo;
  }
  return cog / wsum;
}
      
