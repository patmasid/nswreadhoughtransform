/** @file nswseg4.cpp
This application fills a root tree with segment reconstruction results.
It can be used for monitoring, or for looking at hit reconstruction
results. The tree variables are stored in moni_t.

This version uses only 4 layers, and can run on GIF data from sTGC.

Reads nsw data file and produces monitoring histograms based on segments.
The tree is filled after cluster finding and there is one entry per segment.

@see moni.cpp for monitoring packet data
 
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <exception>
#include "TH2.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "TLatex.h"
#include "TLine.h"
#include "TStyle.h"
#include "TPostScript.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWSegmentList.h"
#include "NSWRead/NSWCalibration.h"
#include "NSWRead/NSWHoughTxNew.h"

/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


void linfit(double x[], double y[], int ndata, double sig[], int mwt, double *a, double *b, double *siga, double *sigb, double *chi2);

/** Draw a simple event display */
void DrawDisplay (NSWSector* sec, int gifNumber){
  TH1F *hamp[8], *htime[8];
  TH1F *hwire[3][8];
  TH1F *hpad[3][8];
  char name[40], title[80];
  char gifName[80];
  int l1id;
  int imax;
  if (sec->GetTechnology()==1){ // 1 = MM, 0=sTGC */
    imax = 8196;
  } else {
    imax = 400; //1200;
  }
  for (int layer=0; layer<8; layer++){
    sprintf (name, "hamp%d", layer);
    sprintf (title, "PDO layer %d;channel", layer);
    hamp[layer] = new TH1F(name, title, imax, 0, imax);  
    sprintf (name, "htime%d", layer);
    sprintf (title, "Time layer %d;channel", layer);
    htime[layer] = new TH1F(name, title, imax, 0, imax);
  }
  for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(i);
    l1id = pac->GetL1ID();
    for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
      NSWChannel* chn = pac->GetChannel(j);
      if (chn->GetChannelType() == NSWChannel::STRIP){
	int strip = chn->GetDetectorStrip();
	int layer = chn->GetLayer();
	hamp[layer]->Fill(strip, chn->GetPDO());
	htime[layer]->Fill(strip, chn->GetPeakingTime());
      }
    }
  }
  TCanvas* can = new TCanvas ("can", "Event display", 1600, 1200);  
  can->Divide(1, 8);
  //gStyle->SetOptStat(0); 
  for (int layer=0; layer<8; layer++){
    can->cd(layer+1);
    hamp[layer]->GetXaxis()->SetLabelSize(0.1);
    hamp[layer]->GetYaxis()->SetLabelSize(0.1);
    hamp[layer]->GetYaxis()->SetRangeUser(0, 1000);
    hamp[layer]->SetFillColor(2);
    hamp[layer]->Draw("HIST");
    htime[layer]->SetMarkerColor(2);
    htime[layer]->SetMarkerStyle(7);
    htime[layer]->Draw("psame");
  }
  can->cd(0);
  TPaveText t(0.4,0.94,0.6,0.99,"NDC");
  char line[40];
  sprintf (line, "Strips L1ID = %d", l1id);
  t.SetTextSizePixels(8);
  t.AddText(line);
  t.SetFillColor(0);
  t.Draw();

  sprintf (gifName, "ed%04ds.gif", gifNumber);
  can->SaveAs(gifName);

  // wires:
  if (sec->GetTechnology()==0){ //  1 = MM, 0=sTGC
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++){
	sprintf (name, "hwire%d%d", q, layer);
	sprintf (title, "Wire PDO Q%d layer %d;wire", q, layer);
	int range = 20+10*q;
	hwire[q][layer] = new TH1F(name, title, range, 0, range);  
      }
    }
    for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
      NSWPacket *pac =sec->GetPacket(i);
      l1id = pac->GetL1ID();
      for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
	NSWChannel* chn = pac->GetChannel(j);
	if (chn->GetChannelType() == NSWChannel::WIRE){
	  int wire = chn->GetDetectorStrip();
	  int layer = chn->GetLayer();
	  int q = chn->GetRadius();
	  hwire[q][layer]->Fill(wire, chn->GetPDO());
	}
      }
    }
    can->Clear();
    can->Divide(3, 8);
      for (int layer=0; layer<8; layer++){
	for (int q=0; q<3; q++){
	  can->cd(q+3*layer+1);
	  hwire[q][layer]->GetXaxis()->SetLabelSize(0.1);
	  hwire[q][layer]->GetYaxis()->SetLabelSize(0.1);
	  hwire[q][layer]->GetYaxis()->SetRangeUser(0, 1000);
	  hwire[q][layer]->SetFillColor(2);
	  hwire[q][layer]->Draw("HIST");
	}
      }
    can->cd(0);
    TPaveText t(0.4,0.94,0.6,0.99,"NDC");
    char line[40];
    sprintf (line, "Wires L1ID = %d", l1id);
    t.AddText(line);
    t.SetFillColor(0);
    t.Draw();
    sprintf (gifName, "ed%04dw.gif", gifNumber);
    can->SaveAs(gifName);

    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++) delete hwire[q][layer];
    }
    
    // pads:
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++){
	sprintf (name, "hpad%d%d", q, layer);
	sprintf (title, "Pads PDO Q%d layer %d;pad", q, layer);
	int range = 120;
	hpad[q][layer] = new TH1F(name, title, range, 0, range);  
      }
    }
    for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
      NSWPacket *pac =sec->GetPacket(i);
      l1id = pac->GetL1ID();
      for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
	NSWChannel* chn = pac->GetChannel(j);
	if (chn->GetChannelType() == NSWChannel::PAD){
	  int pad = chn->GetDetectorStrip();
	  int layer = chn->GetLayer();
	  int q = chn->GetRadius();
	  hpad[q][layer]->Fill(pad, chn->GetPDO());
	}
      }
    }
    can->Clear();
    can->Divide(3, 8);
      for (int layer=0; layer<8; layer++){
	for (int q=0; q<3; q++){
	  can->cd(q+3*layer+1);
	  hpad[q][layer]->GetXaxis()->SetLabelSize(0.1);
	  hpad[q][layer]->GetYaxis()->SetLabelSize(0.1);
	  hpad[q][layer]->GetYaxis()->SetRangeUser(0, 1000);
	  hpad[q][layer]->SetFillColor(2);
	  hpad[q][layer]->Draw("HIST");
	}
      }
    can->cd(0);
    TPaveText t2(0.4,0.94,0.6,0.99,"NDC");
    //char line[40];
    sprintf (line, "Pads L1ID = %d", l1id);
    t2.AddText(line);
    t2.SetFillColor(0);
    t2.Draw();
    sprintf (gifName, "ed%04dp.gif", gifNumber);
    can->SaveAs(gifName);
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++) delete hpad[q][layer];
    }
  } //if sTGC
    
  delete can;
  for (int layer=0; layer<8; layer++){
    delete  htime[layer];
    delete  hamp[layer];
  }
}

/** calculates the efficiency and the error on the efficiency. 
  @param numer the numerator of the ratio
  @param denom the denominator of the ratio
  @param effic the calculated efficiency
  @param error the calculated error */
void CalculateEffi(std::vector<int> n_totalHits_perLayer, std::vector<int> n_totalPred_perLayer, std::vector<double> &effi, std::vector<double> &error) {

  // maybe add sanity checks on n_totalHits_perlayer and n_totalPred_perLayer first??

  int max_total_seg = 0;
  
  // Assume the totalPred contains all segments 3/4 and 4/4 for all layers
  for ( int ilayer=0; ilayer<n_totalPred_perLayer.size(); ilayer++ ) {
    if (n_totalPred_perLayer.at(ilayer)>max_total_seg) max_total_seg = n_totalPred_perLayer.at(ilayer);
  }

  // since each 4/4 segment makes a hit in all segment, 3/4 segment that missed the layer
  // should be total seg - all hits in layer = 3/4 segment that missed the layer

  std::vector<int> num_3o4_seg_missedLayer;
  num_3o4_seg_missedLayer.resize(0);
  for ( int ilayer=0; ilayer<n_totalHits_perLayer.size(); ilayer++ ) {
    if (  n_totalPred_perLayer.at(ilayer)==0 ) num_3o4_seg_missedLayer.push_back(0);
    else {
      num_3o4_seg_missedLayer.push_back( max_total_seg-n_totalHits_perLayer.at(ilayer) );
      if (num_3o4_seg_missedLayer.at(ilayer) < 0) std::cout << "WTF: fewer predictions than hits in layer " << ilayer << std::endl;
    }
  }

  // number of 4/4 segments is all segments minus each of the 3/4 segments that missed each layer
  int num_4o4_seg=max_total_seg;
  for ( int ilayer=0; ilayer<num_3o4_seg_missedLayer.size(); ilayer++ ) {
    if (  n_totalPred_perLayer.at(ilayer)==0 ) continue;

    num_4o4_seg -= num_3o4_seg_missedLayer.at(ilayer);
    if ( num_4o4_seg < 0 ) std::cout << "WTF: number of 4/4 seg " << num_4o4_seg << " is less than 0" << std::endl;
  }

  // efficiency is #4/4 segments / (#4/4 segments + #3/4 segments that missed the layer in question)
  // efficiency is if I have cluster in 3 other layers do I also get a cluster in the 4th layer?
  effi.resize(0);
  error.resize(0);
  for ( int ilayer=0; ilayer<num_3o4_seg_missedLayer.size(); ilayer++ ) {
    if ( num_4o4_seg+num_3o4_seg_missedLayer.at(ilayer) == 0 || n_totalPred_perLayer.at(ilayer)==0) {
      effi.push_back(0);
      error.push_back(0);
    }
    else {

      double dnum = (double)num_4o4_seg;
      double dden = (double)(num_4o4_seg+num_3o4_seg_missedLayer.at(ilayer));

      effi.push_back(dnum/dden);

      double err = 0.0;
      double disc = ((dnum+1)*(dnum+2))/((dden+2)*(dden+3))
	- pow(dnum+1,2)/pow(dden+2,2);
      if (disc >=0) err = sqrt(disc);

      error.push_back(err);
    }
  }
  
  return;
  
  /**********************
  // Error calculation from P. Avery.
  double disc = (1.0/denom * (effic + 1/denom)* (1.0-effic + 1/denom)
		 / (pow(1+2.0/denom,2.0)*(1.0+3.0/denom)));
  if (disc >=0) {
    error = sqrt(disc);

    // For effic = 0 or 1, add to the error the value of the unbiased
    // estimate of Y1/Y2. This will give a better estimate of the conf.
    // interval.
    if (effic==1.0 || effic==0.0) {
      error = (1.0/denom) / (1.0+2.0/denom) + error;
    }
  }
  ************************/
}

void CalculateEffiNew(std::vector<int> n_3o4Pred_perLayer, std::vector<int> n_4o4Pred_perLayer, std::vector<double> &effi, std::vector<double> &error) {

  // maybe add sanity checks on n_totalHits_perlayer and n_totalPred_perLayer first??

  if ( n_4o4Pred_perLayer.size() != n_4o4Pred_perLayer.size() ) {
    std::cout << "WTF: 4/4 and 3/4 segment predictions don't match in layer numbers" << std::endl;
    return;
  }

  // efficiency is #4/4 segments / (#4/4 segments + #3/4 segments that missed the layer in question)
  // efficiency is if I have cluster in 3 other layers do I also get a cluster in the 4th layer?
  effi.resize(0);
  error.resize(0);
  for ( int ilayer=0; ilayer<n_3o4Pred_perLayer.size(); ilayer++ ) {
    if ( n_3o4Pred_perLayer.at(ilayer) < 0 || n_4o4Pred_perLayer.at(ilayer)<0) {
      std::cout << "WTF: number of predictions less than 0?? " << std::endl;
      effi.push_back(0);
      error.push_back(0);
    }
    if ( n_3o4Pred_perLayer.at(ilayer) == 0 && n_4o4Pred_perLayer.at(ilayer)==0) {
      effi.push_back(0);
      error.push_back(0);
    }
    else {

      double dnum = (double)n_4o4Pred_perLayer.at(ilayer);
      double dden = (double)(n_4o4Pred_perLayer.at(ilayer)+n_3o4Pred_perLayer.at(ilayer));

      effi.push_back(dnum/dden);

      double err = 0.0;
      double disc = ((dnum+1)*(dnum+2))/((dden+2)*(dden+3))
        - pow(dnum+1,2)/pow(dden+2,2);
      if (disc >=0) err = sqrt(disc);

      error.push_back(err);
    }
  }

  return;

}

/** read  data file, produce root file for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer
  char fil[300], fils[300];
  int eventNumber = 0;
  int NrejEvnts = 0;
  int NrejEvnts_noFilledHT = 0;
  int NrejEvnts_noHit1perlayer = 0;
  int NrejEvnts_noSmallAngle = 0;
  int NrejEvnts_noClusterHT = 0;
  int NrejEvnts_noGoodClusterHT = 0;

  int NrejEvnts_noHitsLayers_ge2 = 0;
  int NselEvnts_HitsLayers_ge3 = 0;

  int NrejEvnts_noClusLayers_ge2 = 0;
  int NselEvnts_ClusLayers_ge3 = 0;

  int NrejEvnts_noSeg = 0;
  int NselEvnts_Seg = 0;

  int errorCount = 0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  FILE *infile;

  if(argc <6){
    printf("Usage: GIF-sTGC_HT pdoCalib.txt bad_channels.txt num_strips_percell sng(0/1) file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWCalibration cal(argv[5]);
  if (cal.ReadPDO(argv[1]) != 1){
    printf ("Cannot read calibration file.\n");
    return 1;
  }
  //cal.Print();

  std::vector< std::vector <std::vector<int> >> v_bad_channels_strip;
  v_bad_channels_strip.resize(3);
  for(int iQ=0; iQ<3; iQ++){
    v_bad_channels_strip[iQ].resize(8);
  }

  //Converting the bad channels to vector
  std::string line;
  std::ifstream myfile(argv[2]);
  if ( myfile.is_open() ) {
    while ( std::getline ( myfile,line ) ) {
      std::string data_str =  line;
      std::istringstream iss(line);

      int radius = -1;
      int layer = -1;
      std::string det_type = "";
      int det_chnl = -1;

      if ( !(iss
             >> radius
             >> layer
             >> det_type
             >> det_chnl) ){
	std::cout << "Error in reading bad channels file!" << std::endl;
        return 1;
      }

      if(det_type == "strip"){
        v_bad_channels_strip[radius][layer].push_back(det_chnl);
      }

    }
  }

  bool verbose = false;

  std::istringstream iss_Nstrips(argv[3]);
  float Nstrips_perHTcell;
  iss_Nstrips >> Nstrips_perHTcell;
  std::cout << "Nstrips_perHTcell: " << Nstrips_perHTcell << std::endl;

  std::istringstream iss_sng(argv[4]);
  bool sng;
  iss_sng >> sng;
  std::cout << "sng: " << sng << std::endl;

  NSWEvent event;
  NSWSector* sec;
  NSWClusterList clusterList;
  //NSWSegment* seg, * sg;

  //clusterList.Set_vecBadChanls(v_bad_channels_strip);  

  std::istringstream iss_min_chan(argv[6]);
  int min_chan_perclu;
  iss_min_chan >> min_chan_perclu;
  std::cout << "minimum number of channels in a cluster: " << min_chan_perclu << std::endl;

  std::istringstream iss_clus_type(argv[7]);
  int clus_type;
  iss_clus_type >> clus_type;
  std::cout << "clusterType " << clus_type << std::endl;

  strcpy(fils,argv[8]);
  fils[(strlen(argv[8])-5)] = 0;
  strcpy(fil,fils);
  std::string root_name = ".HT_Nstrips"+std::to_string(Nstrips_perHTcell)+"_sng"+std::to_string(sng)+"_clus"+std::to_string(clus_type)+"_calib"+argv[5]+"_minchn"+std::to_string(min_chan_perclu)+"_res_eff.root";//"_caruana.root";
  TFile *f = new TFile(strcat(fils,root_name.c_str()), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }
  // char mainDir[15] = "PerEventPlots";
  f->cd();
  f->mkdir("segs1_3LayerClus");
  f->mkdir("segs1_4LayerClus_3LayerSeg");
  f->mkdir("segs0_3or4LayerHits");

  /** \struct moni_t
      defines variables in root tree.
      One entry per ROD cluster.
  */
  struct moni_t{
    /** event number, increments over all files. */
    int event;
    /** Level one ID */
    int l1id;
    /** number of precision layer clusters */
    int nprec;
    /** number of clusters */
    int nclu;
    /** number of channels of this cluster */
    int wid[8];
    /** center of gravity*/
    float cog[8];
    /** better cog*/
    float cog2[8];
    /** cluster charge*/
    float charge[8];
    /** z position */
    float z[8];
    /** line fit */
    float inter, slope, chi2, angle, invcdf, cdf;
    /** predicted positon and residual */
    float pred[8], res[8];
    /** median y positon residual */
    float y;
    /** amplitude and time per channel*/
    float amp[8][10], time[8][10];
    /** number of missing strips in the cluster */
    int gap[8];
    /** strip number of the first strip */
    int start[8];
    /** strip number of the peak */
    int peak[8];
    /** number of channels with neighbor flag = 0 */
    int nflags[8];
    /** raw parabola interpolation */
    float raw[8];
    /** corrected parabola interpolation */
    float para[8];
    /** three strip center of gravity */
    float cog3[8];
    /** all strip gaussian centroid */
    float gaus[8];
    /** all strip caruana mean */
    float meanCaruana[8];
  };
  moni_t branch;

  TH1F *hResIn[8];
  TH1F *hResEx[8];

  TTree *tree = new TTree("tree","Clusters");
  tree->Branch("branch",&branch,"event/I:l1id:nprec:nclu:wid[8]:cog[8]/F:cog2[8]:charge[8]:z[8]:inter:slope:chi2:angle:pred[8]:res[8]:y:amp[8][10]:time[8][10]:gap[8]/I:start[8]:peak[8]:nflags[8]:raw[8]/F:para[8]:cog3[8]:gaus[8]:meanCaruana[8]");
  TH1F* hSeg = new TH1F ("hSeg", "Segments per event;segments", 30, 0, 30);
  
  TH1F* hDist = new TH1F ("hDist", "intercept distance;mm", 200, -400, 400);
  TH1F* hAli5 = new TH1F ("hAli5", "Alignment for layer 5;mm", 1000, -5, 5);
  TH1F* hAli6 = new TH1F ("hAli6", "Alignment for layer 6;mm", 1000, -5, 5);
  TH1F* hResCog = new TH1F ("hResCog", "Full cluster cog residual;Residual in mm", 1000, -3, 3);
  TH1F* hResCog3 = new TH1F ("hResCog3", "Three strip cog residual;Residual in mm", 1000, -3, 3);
  TH1F* hResPara = new TH1F ("hResPara", "Three strip parabola residual;Residual in mm", 1000, -3, 3);
  TH1F* hChi2 = new TH1F ("hChi2", "Chi2 per segment", 500, 0, 2000);
  TH1F* hChi2divDOF = new TH1F ("hChi2divDOF", "Chi2 divided by DOF per segment", 500, 0, 2000);
  TH1F* hAng = new TH1F("hAng", "Angle per segment", 90, -90, 90);
  TH1F* hInvCDF = new TH1F("hInvCDF","Inv-CDF for the obtained Chi2",50,0,1);
  TH1F* hCDF = new TH1F("hCDF","CDF for the obtained chi2",50,0,1);
  TH1F* hLayerEff = new TH1F("hLayerEff", "Layer efficiency", 10, 0, 10);

  std::string hNum_HTcells_name, hSumAmp_HTcells_name;
  hNum_HTcells_name = "hNum_HTcells_"+std::to_string(Nstrips_perHTcell);
  hSumAmp_HTcells_name = "hSumAmp_HTcells"+std::to_string(Nstrips_perHTcell);

  TH1F* hNum_HTcells = new TH1F(hNum_HTcells_name.c_str(),"number of valid HT cells per event",11,-0.5,10.5);
  TH1F* hSumAmp_HTcells = new TH1F(hSumAmp_HTcells_name.c_str(),"total amplitude of HT cells selected more than one per event",40,0,4000);
  TH1F* hClusterWidth = new TH1F("ClusterWidth","ClusterWidth",20,0,20);
  TH1F* hMaxPdo = new TH1F("MaxPdo","MaxPdo",1024,-0.5,1023.5);
  TH1F* hPeakPdo = new TH1F("PeakPdo","PeakPdo",1024,-0.5,1023.5);
  TH1F* hNumPot = new TH1F("NumPot","Number of potential signal channels per cluster", 20,0,20);
  TH1F* hNumMask = new TH1F("NumMask","Number of masked signal channels per cluster", 20,0,20);
  TH1F* hNumBkg = new TH1F("NumBkg","Number of background channels per cluster", 20,0,20);

  TH1F* hClusterWidth_Seg = new TH1F("ClusterWidth_Seg","ClusterWidth_Seg",20,0,20);
  TH1F* hMaxPdo_Seg = new TH1F("MaxPdo_Seg","MaxPdo_Seg",1024,-0.5,1023.5);
  TH1F* hPeakPdo_Seg = new TH1F("PeakPdo_Seg","PeakPdo_Seg",1024,-0.5,1023.5);
  TH1F* hNumPot_Seg = new TH1F("NumPot_Seg","Number of potential signal channels per cluster in a reco segment",20,0,20);
  TH1F* hNumMask_Seg = new TH1F("NumMask_Seg","Number of masked signal channels per cluster in a reco segment", 20,0,20);
  TH1F* hNumBkg_Seg = new TH1F("NumBkg_Seg","Number of background channels per cluster in a reco segment", 20,0,20);

  TH1F* h_fracNumPot_Seg = new TH1F("fracNumPot_Seg","Number of potential signal channels per cluster", 50,0,1);
  TH1F* h_fracNumMask_Seg = new TH1F("fracNumMask_Seg","Number of masked signal channels per cluster", 50,0,1);
  TH1F* h_fracNumBkg_Seg = new TH1F("fracNumBkg_Seg","Number of background channels per cluster", 50,0,1);
  TH2F* h_fracNumPot_vs_Ang = new TH2F("fracNumPot_vs_Ang","# of potential signal chans per clu in a reco segment vs angle", 90,-90,90,50,0,1);
  TH2F* h_fracNumMask_vs_Ang = new TH2F("fracNumMask_vs_Ang","# of masked signal chans per clu in a reco segment vs angle", 90,-90,90,50,0,1);
  TH2F* h_fracNumBkg_vs_Ang = new TH2F("fracNumBkg_vs_Ang","# of background chans per clu in a reco segment vs angle", 90,-90,90,50,0,1);
  TH2F* h_fracNumPot_vs_Chi2 = new TH2F("fracNumPot_vs_Chi2","# of potential signal chans per clu in a reco segment vs chi2", 500,0,2000,50,0,1);
  TH2F* h_fracNumMask_vs_Chi2 = new TH2F("fracNumMask_vs_Chi2","# of masked signal chans per clu in a reco segment vs chi2", 500,0,2000,50,0,1);
  TH2F* h_fracNumBkg_vs_Chi2 = new TH2F("fracNumBkg_vs_Chi2","# of background chans per clu in a reco segment vs chi2", 500,0,2000,50,0,1);

  TH1F* h_numEvwNoHitsLayers_Vs_numLayerswNoHits = new TH1F("numEvwNoHitsLayers_Vs_numLayerswNoHits","numEvwNoHitsLayers_Vs_numLayerswNoHits",5,-0.5,4.5);
  TH1F* h_numEvwNoClusLayers_Vs_numLayerswNoClus = new TH1F("numEvwNoClusLayers_Vs_numLayerswNoClus","numEvwNoClusLayers_Vs_numLayerswNoClus",5,-0.5,4.5);

  TH2F* h_ClusWidth_Vs_bkgChns = new TH2F("ClusWidth_Vs_bkgChns","ClusWidth_Vs_bkgChns",20,0,20,20,0,20);
  
  TH2F* h_Relbcid_Vs_DistMaxPdo_1seg3layer = new TH2F("Relbcid_Vs_DistMaxPdo_1seg2layer","Relbcid_Vs_DistMaxPdo_1seg3layer",8,0,8,8,0,8);
  TH2F* h_Relbcid_Vs_DistMaxPdo = new TH2F("Relbcid_Vs_DistMaxPdo","Relbcid_Vs_DistMaxPdo",8,0,8,8,0,8);

  //================= per event plots after segment reconstruction =================//
  
  std::vector< std::vector<TCanvas*> > c_segments;
  
  std::vector<TCanvas*> c_evtDisplay;
  
  //std::vector< std::vector<TH2F*> > h_Pdo_Vs_PosVsLayer;
  //std::vector< std::vector<TGraph*> > g_Pos_Vs_Layer_allHits;
  
  //=============================================================================//

  TH1F* hNumClu[8];
  TH1F* hNumClu_BeforeRej[8];
  TH1F* hNumClu_Seg = new TH1F("hNumClu_Seg", "Number of layers with cluster", 20, 0, 20);
  TH1F * hOcc[8];
  TH2F * hTime[8];
  TH1F * hn[8], *hN[8], *hEffi[8]; // efficiency old
  TH1F * h3o4SegPred_missingLayer[8], *h4o4SegPred_allLayers[8]; //efficiency new 
  TH1F* hClusCharge[8]; 
  TH1F* hClusCharge_Seg_4of4[8];
  TH1F* hClusCharge_Seg[8];
  TH1F* hClusCentroid_Seg[8];
  TH1F* hClusCentroid_Seg_4of4[8];
  TH1F* hMaxPdoStripCharge_Seg[8];
  TH1F* hMaxPdoStripCharge_Seg_4of4[8];
  TH1F* hNeighFlag0_Clus[8];
  TH1F* hNeighFlag1_Clus[8];
  
  TH1F* hClusCharge_1stCalib[8];
  TH1F* hClusCharge_Seg_4of4_1stCalib[8];
  TH1F* hClusCharge_Seg_1stCalib[8];
  TH1F* hMaxPdoStripCharge_Seg_4of4_1stCalib[8];
  TH1F* hMaxPdoStripCharge_Seg_1stCalib[8];
  TH1F* hNeighFlag0_Clus_1stCalib[8];
  TH1F* hNeighFlag1_Clus_1stCalib[8];

  TH2F* hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[8];
  TH2F* hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[8];

  TH1F* hRelBcid01_Strips[8];
  TH1F* hRelBcid01_Wires[8];
  TH1F* hRelBcid01_Pads[8];
  TH2F* hRelBcid_Vs_VMM_SFEB[8];
  TH2F* hRelBcid_Vs_VMM_PFEB[8];

  TH2F* hAng_Chi2  = new TH2F("hAng_Chi2", "Chi2 vs Track angle;Angle;chi2",65, -65, 65, 100, 0, 100);
  TH2F* hChi2_nSeg = new TH2F("hChi2_nSeg","segment multi vs Track chi2;chi2;#Segment", 50,0,50, 10,0,10);
  TH2F* hInvCDFvschi2 = new TH2F("hInvCDFvschi2","inv-cdf vs chi2",500,0,2000,50,0,1);
  TH2F* hCDFvschi2 = new TH2F("hCDFvschi2","cdf vs chi2",500,0,2000,50,0,1);

  char resname[10], restitle[80];
  for (int i=0; i<8; i++){
    sprintf(resname, "hResIn%d", i);
    sprintf(restitle, "Layer %d inclusive residual;Residual in mm", i);
    hResIn[i] = new TH1F(resname, restitle, 1000, -3, 3);

    sprintf(resname, "hResEx%d", i);
    sprintf(restitle, "Layer %d exclusive residual;Residual in mm", i);
    hResEx[i] = new TH1F(resname, restitle, 1000, -3, 3);
    
  }
 
  char name[80], title[80];
  for (int i=0; i<8; i++) {
    sprintf (name, "hOcc%d", i);
    sprintf (title, "Cluster position of layer %d;Position in mm", i);
    hOcc[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hTime%d", i);
    sprintf (title, "Cluster peaking time of layer %d;Position in mm;time in ns", i);
    hTime[i] = new TH2F (name, title, 1000, 0, 4000, 100, -25, 175);

    sprintf (name, "hn%d", i);
    sprintf (title, "Predicted position of layer %d;Position in mm", i);
    hn[i] = new TH1F (name, title, 1000, 0, 4000);
    
    sprintf (name, "hN%d", i);
    sprintf (title, "Predicted positon of layer %d;Position in mm", i);
    hN[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "h3o4SegPred_missingLayer%d", i);
    sprintf (title, "Predicted positon of 3/4 segment in layer %d where hit is missing;Position in mm", i);
    h3o4SegPred_missingLayer[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "h4o4SegPred_allLayers%d", i);
    sprintf (title, "Predicted positon of 4/4 segment in layer %d;Position in mm", i);
    h4o4SegPred_allLayers[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hEffi%d", i);
    sprintf (title, "Strip cluster efficiency of layer %d;Position in mm;Efficiency", i);
    hEffi[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hNumClu%d", i);
    sprintf (title, "Number of clusters in layer %d", i);
    hNumClu[i] = new TH1F(name, title, 20, 0, 20);
    
    sprintf (name, "hNumClu_BeforeRej%d", i);
    sprintf (title, "Number of clusters before rejecting in layer %d", i);
    hNumClu_BeforeRej[i] = new TH1F(name, title, 20, 0, 20);

    sprintf (name, "hClusCharge%d", i);
    sprintf (title, "sum of all channel charges (pdo - baseline) in layer %d", i);
    hClusCharge[i]= new TH1F(name, title,200,0,2000);

    sprintf (name, "hClusCharge_Seg%d", i);
    sprintf (title, "sum of all channel charges (pdo - baseline) for segmemnts in layer %d", i);
    hClusCharge_Seg[i]= new TH1F(name, title,200,0,2000);

    sprintf (name, "hClusCharge_Seg_4of4_%d", i);
    sprintf (title, "sum of all channel charges (pdo - baseline) for segmemnts 4 out of 4 in layer %d", i);
    hClusCharge_Seg_4of4[i]= new TH1F(name, title,200,0,2000);

    sprintf (name, "hClusCentroid_Seg%d", i);
    sprintf (title, "cluster centroid position for layer %d", i);
    hClusCentroid_Seg[i]=new TH1F(name, title, 135 ,0,1350);

    sprintf (name, "hClusCentroid_Seg_4of4_%d", i);
    sprintf (title, "cluster centroid position for 4 out of 4 in layer %d", i);
    hClusCentroid_Seg_4of4[i]=new TH1F(name, title, 135 ,0,1350);

    sprintf (name, "hMaxPdoStripCharge_Seg%d", i);
    sprintf (title, "max pdo strip charge on layer %d", i);
    hMaxPdoStripCharge_Seg[i] = new TH1F(name, title, 1000, 0, 1000);

    sprintf (name, "hMaxPdoStripCharge_Seg_4of4_%d", i);
    sprintf (title, "max pdo strip charge 4 out of 4 on layer %d", i);
    hMaxPdoStripCharge_Seg_4of4[i] =new TH1F(name, title, 1000, 0, 1000);
    
    sprintf (name, "hNeighFlag0_Clus%d", i);
    sprintf (title, "cluster channels with nigh flag 0 on layer %d", i);
    hNeighFlag0_Clus[i] = new TH1F(name, title, 1000, 0, 1000);

    sprintf (name, "hNeighFlag1_Clus%d", i);
    sprintf (title, "cluster channels with nigh flag 0 on layer %d", i);
    hNeighFlag1_Clus[i] = new TH1F(name, title, 1000, 0, 1000);

    sprintf (name, "hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib%d", i);
    sprintf (title, "Max pdo vs 1st neighbour pdo on layer %d", i);
    hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[i] = new TH2F(name, title, 100, 0, 1000, 100, 0, 1000);

    sprintf (name, "hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib%d", i);
    sprintf (title, "Max pdo vs 2nd neighbour pdo on layer %d",i);
    hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[i] =new TH2F(name, title, 100, 0, 1000, 100, 0, 1000);
    
    sprintf (name, "hRelBcid01_Strips_Layer%d", i);
    sprintf (title, "No. of events versus strip no. Layer %d", i);
    hRelBcid01_Strips[i] = new TH1F(name, title, 408, -0.5, 407.5);

    sprintf (name, "hRelBcid01_Wires_Layer%d", i);
    sprintf (title, "No. of events versus wire no. Layer %d", i);
    hRelBcid01_Wires[i] = new TH1F(name, title, 32, -0.5, 31.5);

    sprintf (name, "hRelBcid01_Pads_Layer%d", i);
    sprintf (title, "No. of events versus pad no. Layer %d", i);
    hRelBcid01_Pads[i] = new TH1F(name, title, 96, -0.5, 95.5);

    sprintf (name, "hRelBcid_Vs_VMM_SFEB_Layer%d", i);
    sprintf (title, "Relbcid Vs SFEB VMM number Layer %d", i);
    hRelBcid_Vs_VMM_SFEB[i] =  new TH2F(name, title, 8, -0.5, 7.5, 8, -0.5, 7.5);

    sprintf (name, "hRelBcid_Vs_VMM_PFEB_Layer%d", i);
    sprintf (title, "Relbcid Vs PFEB VMM number Layer %d", i);
    hRelBcid_Vs_VMM_PFEB[i] =  new TH2F(name, title, 8, -0.5, 7.5, 8, -0.5, 7.5);

    sprintf (name, "hClusCharge_1stCalib_%d", i);
    sprintf (title, "sum of all channel charges 1st Calib in layer %d", i);
    hClusCharge_1stCalib[i]= new TH1F(name, title,1000,0,1000);

    sprintf (name, "hClusCharge_Seg_1stCalib_%d", i);
    sprintf (title, "sum of all channel charges 1st Calib for segments in layer %d", i);
    hClusCharge_Seg_1stCalib[i]= new TH1F(name, title,1000,0,1000);

    sprintf (name, "hClusCharge_Seg_4of4_1stCalib_%d", i);
    sprintf (title, "sum of all channel charges 1st Calib for segments 4 out of 4 in layer %d", i);
    hClusCharge_Seg_4of4_1stCalib[i]= new TH1F(name, title,1000,0,1000);
    
    sprintf (name, "hMaxPdoStripCharge_Seg_1stCalib_%d", i);
    sprintf (title, "max pdo strip charge 1st Calib on layer %d", i);
    hMaxPdoStripCharge_Seg_1stCalib[i] = new TH1F(name, title, 1000, 0, 1000);

    sprintf (name, "hMaxPdoStripCharge_Seg_4of4_1stCalib_%d", i);
    sprintf (title, "max pdo strip charge 4 out of 4 1st Calib on layer %d", i);
    hMaxPdoStripCharge_Seg_4of4_1stCalib[i] =new TH1F(name, title, 1000, 0, 1000);
    
    sprintf (name, "hNeighFlag0_Clus_1stCalib_%d", i);
    sprintf (title, "cluster channels with neigh flag 0 1st Calib on layer %d", i);
    hNeighFlag0_Clus_1stCalib[i] = new TH1F(name, title, 1000, 0, 1000);

    sprintf (name, "hNeighFlag1_Clus_1stCalib_%d", i);
    sprintf (title, "cluster channels with neigh flag 1 1st Calib on layer %d", i);
    hNeighFlag1_Clus_1stCalib[i] = new TH1F(name, title, 1000, 0, 1000);
    
    

  }
	  
  int totSeg = 0;
  int numSeg = 0; // per event
  int gifNumber = 0;
  
  int beamStartStrip = 159;
  int beamEndStrip   = 190;

  NSWChannelID chID;

  float startingStripPos = chID.GetStripPosition(0)+1.6;
  float endStripPos = chID.GetStripPosition(407)-1.6;

  float pos_min_beam = chID.GetStripPosition(beamStartStrip) + 1.6;
  float pos_max_beam = chID.GetStripPosition(beamEndStrip);//   - 1.6;

  if ( pos_min_beam > pos_max_beam ) {
    float tmp = pos_max_beam;
    pos_max_beam = pos_min_beam;
    pos_min_beam = tmp;
  }
  
  std::string relbcid_plots_name = ".BkgDistribution.root";
  TFile *f_relbcid_plots = new TFile(strcat(fil,relbcid_plots_name.c_str()),"recreate");
  if (!f_relbcid_plots) {
    printf ("Cannot open root file 2!\n");
    return -1;
  }

  int min_layer = 4;
  int max_layer = 7;

  double z_minlayer = NSWCluster::GetZPosition(min_layer,true,0) - 10.5;
  double z_maxlayer = NSWCluster::GetZPosition(max_layer,true,0) + 10.5;

  TPostScript *ps = new TPostScript("event_displays.ps",112);
  TCanvas* c_ev_display = new TCanvas("c_ev_display");
  TH1F* g_display_null = new TH1F("g_display_null","",1,z_minlayer,z_maxlayer);

  TLine * lmax = new TLine( z_minlayer, pos_max_beam, z_maxlayer, pos_max_beam);
  TLine * lmin = new TLine( z_minlayer, pos_min_beam, z_maxlayer, pos_min_beam);
  
  NSWHoughTxNew* o_HT = new NSWHoughTxNew(Nstrips_perHTcell, 0, 407, min_layer, max_layer);
  //  bool stripRangeSet = o_HT->SetStripRange( Nstrips_perHTcell, 0, 407, 4, 7 );
  //if ( !stripRangeSet ) return 0;

  o_HT->SetVerbose( verbose );
  o_HT->Set_vecBadChanls(v_bad_channels_strip);
  o_HT->Set_sng(sng);
  o_HT->define_HT_var();
  o_HT->SetTargetAngle ( 0, 20 );
  o_HT->SetAcceptNClusterSegment ( 3 );
  o_HT->SetMinChanInCluster(min_chan_perclu);

  TLatex * t = new TLatex();

  for (int iFile=8; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    //------------------------------------------------------//    
    //         start processing events:
    //------------------------------------------------------//

    int size; // actual size of the ATLAS event
    int num_events_found = 0;
    do{ //event loop

      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
	break;
      }

      try{ 

 	event.ReadNSW(buffer, size);           // read the data

        bool isSingleCluster = true;
  
	//fill hitlist with cluster data
	branch.event = eventNumber;
	if ( verbose ) std::cout << "################### Event Number : " << eventNumber << " ##########################" << std::endl;

	//std::string c_display_name = "c_display_Ev"+std::to_string(eventNumber);
	//TCanvas* c_evtDisplay_perev = new TCanvas(c_display_name.c_str(),c_display_name.c_str(),800,600);
	bool isEvtRej_noHitsLayers = false, isEvtRej_noClusLayers = false;
	
	//std::cout << "Number of sectors: " << event.GetNumSector() << std::endl;

	for (int i=0; i<event.GetNumSector(); i++){
	//for (int i=28; i<29; i++){
	  
	  NSWSector* sec = event.GetSector(i);
	  cal.Calibrate(sec); 

	  /*std::vector<float> z_chn, x_chn; 
	  z_chn.clear();
	  x_chn.clear();

	  std::vector<bool> isLayerWithHits(4,false);
	  */
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac =sec->GetPacket(j);
	    if (pac->GetEndPoint() != 1) continue; // only strips 
	    for (int k=0; k<pac->GetWidth(); k++){
	      NSWChannel* chn = pac->GetChannel(k);
	      //if(chn->GetAmplitude_mV()==0) continue;
	      int layer= chn->GetLayer();
	      if(chn->GetDetectorStrip()==-1) continue;
	      //isLayerWithHits[layer-4] = true;
	      //z_chn.push_back(chn->GetZPosition());
	      //x_chn.push_back(chn->GetStripPosition());
	      if(verbose) std::cout << "before filling hRelBcid plots" << std::endl;
	      if(chn->GetEndPoint()==1 && chn->GetChannelType()==1){
                hRelBcid_Vs_VMM_SFEB[layer]->Fill(chn->GetRelBCID(), chn->GetConfigID());
                if(chn->GetRelBCID()==0 || chn->GetRelBCID()==1)
                  hRelBcid01_Strips[layer]->Fill(chn->GetDetectorStrip());
              }
              else if(chn->GetEndPoint()==0){
                hRelBcid_Vs_VMM_PFEB[layer]->Fill(chn->GetRelBCID(), chn->GetConfigID());

		if(chn->GetChannelType()==0){
                  if(chn->GetRelBCID()==0 || chn->GetRelBCID()==1)
                    hRelBcid01_Pads[layer]->Fill(chn->GetDetectorStrip());
                }
                if(chn->GetChannelType()==2){
                  if(chn->GetRelBCID()==0 || chn->GetRelBCID()==1)
                    hRelBcid01_Wires[layer]->Fill(chn->GetDetectorStrip());
                }
              }
	      
	      if(verbose) std::cout << "after filling hRelBcid plots" << std::endl;

	    }// next k  
	  }// next j
		    
	  /*int num_noHitsLayers = count(isLayerWithHits.begin(), isLayerWithHits.end(), false);
	  h_numEvwNoHitsLayers_Vs_numLayerswNoHits->Fill(num_noHitsLayers);
	  if(num_noHitsLayers >= 2){
	    NrejEvnts_noHitsLayers_ge2++;
	    isEvtRej_noHitsLayers = true;
	  }
	  else
	    NselEvnts_HitsLayers_ge3++;

	  if(z_chn.size()!=0){
	    c_evtDisplay_perev->cd();
	    TGraph* g_display_chn = new TGraph(z_chn.size(), &z_chn[0], &x_chn[0]);
	    std::string g_display_chn_name = "g_display_chn_Ev"+std::to_string(eventNumber);
	    g_display_chn->SetName(g_display_chn_name.c_str());
	    g_display_chn->GetXaxis()->SetTitle("z [mm]");
	    g_display_chn->GetYaxis()->SetTitle("x [mm]");
	    g_display_chn->SetMarkerColor(kGreen+1);  
	    g_display_chn->SetMarkerSize(1);
	    g_display_chn->SetMarkerStyle(21);
	    g_display_chn->Draw("AP");
	  }*/

	  int clusterType = clus_type;
	  
	  //------------------------------------------------//
	  //              Fill Pad Towers
	  //------------------------------------------------//

	  //	  NSWPadWires* PadWireList = new NSWPadWires();

	  //	  PadWireList->FindPadTower(sec);

	  //	  PadWireList->MatchPadTower(sec);

	  //	  PadWireList->GetNumPadTower();

	  //------------------------------------------------//
	  //            Get Pad, Wire Position
	  //------------------------------------------------//

	  /*
	  std::vector<std::vector<float>> pad_pos_2D[4];
	  std::vector<float> wire_pos_all[4], wire_err_all[4];
	  std::vector<float> pad_pos_all[4], pad_err_all[4];
	  std::vector<float> strip_pos_all[4];

	  for(int layer=4; layer<8; layer++){
	    // wires
	    for(int i_chn=0; i_chn<50; i_chn++){
	      NSWChannel* chn = PadWireList->GetRawWire(layer, i_chn);
	      if ( chn != NULL ){
		wire_pos_all[layer-4].push_back(chn->GetWireCenterPosition());
		wire_err_all[layer-4].push_back(chn->GetWireGroupHalfWidth());
		//wire_zpos_all[layer].push_back(chn->GetZPosition());
	      }
	    }

	    // pads
	    for(int i_chn=0; i_chn<97; i_chn++){
	      NSWChannel* chn = PadWireList->GetRawPad(layer, i_chn);
	      if ( chn != NULL  ){
		std::vector<float> pad_pos = chn->GetPadPosition();
		pad_pos_2D[layer-4].push_back(pad_pos);

		double y_max = *max_element(pad_pos.begin()+4, pad_pos.end());
		double y_min = *min_element(pad_pos.begin()+4, pad_pos.end());

		pad_pos_all[layer-4].push_back( (y_max+y_min)/2 );
		pad_err_all[layer-4].push_back( (y_max-y_min)/2 );
		//if( chn->GetLayer() == 4 || chn->GetLayer() == 6 )
		//  pad_zpos_all.push_back(chn->GetZPosition()-1.63);
		//else
		//  pad_zpos_all.push_back(chn->GetZPosition()+1.63);
	      }
	    }
	  
	  } // end of loop over layers
	  */

	  //------------------------------------------------//
	  //      Set up Hough Transform on Strips
	  //------------------------------------------------//

	  /*
	  NSWHoughTxNew* o_HT = new NSWHoughTxNew(Nstrips_perHTcell);

	  o_HT->Set_vecBadChanls(v_bad_channels_strip);
	  o_HT->Set_sng(sng);
	  o_HT->define_HT_var();
	  o_HT->SetTargetAngle ( 0, 20 );
	  o_HT->SetAcceptNClusterSegment ( 3 );
	  */

	  // decoded the hit from data and find clusters
	  // can be replaced with just 
	  // void SetStripHitMap( std::vector< std::vector <NSWChannel*>> stripHitMap );
	  // void SetClusters(NSWClusterList clusterList_tmp, NSWClusterList clusterList_badclusters_tmp);
	  // if a list of hits and good clusters are already determined externally
	  o_HT->select_hits(sec);
	  o_HT->FindClusters(clusterType);

	  if ( verbose ) std::cout << "fill HT" << std::endl;

	  o_HT->Fill_HT(clusterType, beamStartStrip, beamEndStrip);
	  
	  if ( verbose ) std::cout << "filled HT" << std::endl;

	  //=============================================================================//
	  //                      All Plot Hits and Clusters
	  //-----------------------------------------------------------------------------//

	  float min_y_hit = chID.GetStripPosition(0);//-1.6;
          float max_y_hit = chID.GetStripPosition(407);//+1.6;
	  
	  if( verbose ) std::cout << "min_y_hit: " << min_y_hit << " max_y_hit: " << max_y_hit << std::endl;

	  if ( min_y_hit > max_y_hit ) {
	    float tmp = max_y_hit;
	    max_y_hit = min_y_hit;
	    min_y_hit = tmp;
	  }
	  
          g_display_null->SetMinimum(min_y_hit-1.6);
          g_display_null->SetMaximum(max_y_hit+1.6);

	  c_ev_display->cd();
          g_display_null->Draw();
          c_ev_display->Update();

	  lmax->Draw();
	  lmin->Draw();

	  double y = (min_y_hit + max_y_hit)/2;//ymax - ymax/5;
	  double x = z_maxlayer-20; //xmin + xmin/5;

	  std::string latex_name = "Sec"+std::to_string(i)+"_Ev"+std::to_string(eventNumber);
	  c_ev_display->cd();
	  t->DrawLatex(x,y, latex_name.c_str());

	  //-------------------------------------------//

	  std::vector<float> z_chn_neigh1, x_chn_neigh1, z_chn_neigh0, x_chn_neigh0,
            z_chn_relbcid_early, x_chn_relbcid_early, z_chn_relbcid_late, x_chn_relbcid_late;
          z_chn_neigh1.clear();
          x_chn_neigh1.clear();
          z_chn_neigh0.clear();
          x_chn_neigh0.clear();

          z_chn_relbcid_early.clear();
          x_chn_relbcid_early.clear();
          z_chn_relbcid_late.clear();
          x_chn_relbcid_late.clear();

	  for ( uint il=min_layer; il<max_layer+1; il++ ) {
	    //if( verbose ) std::cout << "all channel loop: " << " Layer: " << il << " o_HT->GetNChannelsInLayer(): " << o_HT->GetNChannelsInLayer() << std::endl;
	    for ( uint ich=0; ich<o_HT->GetNChannelsInLayer(); ich++ ) {
	      //if( verbose ) std::cout << "ich: " << ich << std::endl;
	      NSWChannel* chn = o_HT->ch_det[il][ich];

	      if ( chn == NULL ) continue;

	      if( verbose ) std::cout << "CH LAYER: " << chn->GetLayer() << " CH VMM: " << chn->GetVMM() 
				      << " VMM-CH: " << chn->GetVMMChannel() << std::endl;
	      if( verbose ) std::cout << " CH NUM: " << chn->GetDetectorStrip() << " CH POS " << chn->GetStripPosition() 
				      << " CH rel_bc " << chn->GetRelBCID() << std::endl;
	      //<< " CH DATABASEID: " << chn->GetDatabaseID()
	      if( verbose ) std::cout << " CH PDO: " << chn->GetPDO() << " CH AMPLITUDE: " << chn->GetAmplitude() 
				      << " CH AMPLITUDE mV: " << chn->GetAmplitude_mV() << " CH BASELINE: " << chn->GetBaseline()
				      << std::endl;

	      if(chn->GetNeighbor()==1){
                z_chn_neigh1.push_back(chn->GetZPosition());
                x_chn_neigh1.push_back(chn->GetStripPosition());
                if ( chn->GetStripPosition() > max_y_hit ) max_y_hit = chn->GetStripPosition();
                if ( chn->GetStripPosition() < min_y_hit ) min_y_hit = chn->GetStripPosition();
              }
	      else if(chn->GetNeighbor()==0){
                z_chn_neigh0.push_back(chn->GetZPosition());
                x_chn_neigh0.push_back(chn->GetStripPosition());
                if ( chn->GetStripPosition() > max_y_hit ) max_y_hit = chn->GetStripPosition();
                if ( chn->GetStripPosition() < min_y_hit ) min_y_hit = chn->GetStripPosition();
              }

	      if( chn->GetRelBCID() == 0 || chn->GetRelBCID() == 1 ) {
                z_chn_relbcid_early.push_back(chn->GetZPosition()+1.0);
                x_chn_relbcid_early.push_back(chn->GetStripPosition());
              }
              else if( chn->GetRelBCID() == 7 ) {
                z_chn_relbcid_late.push_back(chn->GetZPosition()+1.0);
                x_chn_relbcid_late.push_back(chn->GetStripPosition());
              }

	    } // ichannel
	  } // ilayer

	  //---------------------------------------------------//

          if(z_chn_neigh1.size()!=0){
            //c_evtDisplay_perev->cd();
            TGraph g_display_chn_neigh1(z_chn_neigh1.size(), &z_chn_neigh1[0], &x_chn_neigh1[0]);
	    std::string g_display_chn_neigh1_name = "g_display_chn_neigh1_Ev"+std::to_string(eventNumber);
            g_display_chn_neigh1.SetName(g_display_chn_neigh1_name.c_str());
            g_display_chn_neigh1.GetXaxis()->SetTitle("z [mm]");
            g_display_chn_neigh1.GetYaxis()->SetTitle("x [mm]");
            g_display_chn_neigh1.SetMarkerColor(kGreen+1);
            g_display_chn_neigh1.SetMarkerSize(1);
            g_display_chn_neigh1.SetMarkerStyle(21);
            //g_display_chn_neigh1->Draw("AP");
            c_ev_display->cd();
            g_display_chn_neigh1.Draw("P same");

          }
          if(z_chn_neigh0.size()!=0){
            //c_evtDisplay_perev->cd();
            TGraph g_display_chn_neigh0(z_chn_neigh0.size(), &z_chn_neigh0[0], &x_chn_neigh0[0]);
	    std::string g_display_chn_neigh0_name = "g_display_chn_neigh0_Ev"+std::to_string(eventNumber);
            g_display_chn_neigh0.SetName(g_display_chn_neigh0_name.c_str());
            g_display_chn_neigh0.GetXaxis()->SetTitle("z [mm]");
            g_display_chn_neigh0.GetYaxis()->SetTitle("x [mm]");
            g_display_chn_neigh0.SetMarkerColor(kMagenta);
            g_display_chn_neigh0.SetMarkerSize(1);
            g_display_chn_neigh0.SetMarkerStyle(21);
            //g_display_chn_neigh0->Draw("AP");
            c_ev_display->cd();
            g_display_chn_neigh0.Draw("P SAME");
          }

          if(z_chn_relbcid_early.size()!=0){
            TGraph g_display_chn_relbcid_early(z_chn_relbcid_early.size(), &z_chn_relbcid_early[0], &x_chn_relbcid_early[0]);
	    std::string g_display_chn_relbcid_early_name = "g_display_chn_relbcid_early_Ev"+std::to_string(eventNumber);
            g_display_chn_relbcid_early.SetName(g_display_chn_relbcid_early_name.c_str());
            g_display_chn_relbcid_early.GetXaxis()->SetTitle("z [mm]");
            g_display_chn_relbcid_early.GetYaxis()->SetTitle("x [mm]");
            g_display_chn_relbcid_early.SetMarkerColor(kOrange);
            g_display_chn_relbcid_early.SetMarkerSize(1);
            g_display_chn_relbcid_early.SetMarkerStyle(21);
            //g_display_chn_relbcid_early->Draw("AP");
            c_ev_display->cd();
            g_display_chn_relbcid_early.Draw("P SAME");
          }

          if(z_chn_relbcid_late.size()!=0){
            TGraph g_display_chn_relbcid_late(z_chn_relbcid_late.size(), &z_chn_relbcid_late[0], &x_chn_relbcid_late[0]);
	    std::string g_display_chn_relbcid_late_name = "g_display_chn_relbcid_late_Ev"+std::to_string(eventNumber);
            g_display_chn_relbcid_late.SetName(g_display_chn_relbcid_late_name.c_str());
            g_display_chn_relbcid_late.GetXaxis()->SetTitle("z [mm]");
            g_display_chn_relbcid_late.GetYaxis()->SetTitle("x [mm]");
            g_display_chn_relbcid_late.SetMarkerColor(kBlack);
            g_display_chn_relbcid_late.SetMarkerSize(1);
            g_display_chn_relbcid_late.SetMarkerStyle(21);
            //g_display_chn_relbcid_late->Draw("AP");
            c_ev_display->cd();
            g_display_chn_relbcid_late.Draw("P SAME");
          }
	
	  if ( verbose ) std::cout << "drew hits" << std::endl;

	  //---------------------------------------------------------------------//
	  //                   Reconstructed Good Clusters
	  //---------------------------------------------------------------------//

	  std::vector<float> z_clu, x_clu;
          z_clu.clear();
          x_clu.clear();

	  for ( uint il=0; il<o_HT->Clusters_PerLayer.size(); il++ ) {
	    if( verbose ) std::cout << "Before Number of clusters @patmasid: Layer " << il << " number of clusters: " << o_HT->Clusters_PerLayer.at(il).size() << std::endl;
	    for ( uint icl=0; icl<o_HT->Clusters_PerLayer.at(il).size(); icl++ ) {
	      if( verbose ) std::cout << "@patmasid cluster number: " << icl << std::endl;
	      NSWCluster* cl = o_HT->Clusters_PerLayer.at(il).at(icl);
	      
	      if( verbose ){
		for(int ich=0; ich< cl->GetNumChannel(); ich++){
		  NSWChannel* chn = cl->GetChannel(ich);
		  if(chn == NULL) continue;
		  std::cout << "@patmasid cluster channel number: " << ich << " chn->GetPDO(): " << chn->GetPDO() << std::endl;
		}
	      }

	      if ( verbose ) std::cout << "good cluster layer " << il << " mean " << cl->GetMeanCaruana() << std::endl;
    
	      double mean= 0.0;
	      if(clusterType==0){
		mean = cl->GetCOG();
	      }
	      else if(clusterType==1){
		mean = cl->GetGaussianCentroid();
	      }
	      else if(clusterType==2){
		mean = cl->GetMeanCaruana();
	      }
	      else if(clusterType==3){
		mean = cl->GetParabolaMean();
	      }

	      z_clu.push_back(cl->GetZPosition());
	      if(clusterType==0){
		x_clu.push_back(cl->GetCOG());
	      }
	      else if(clusterType==1){
		x_clu.push_back(cl->GetGaussianCentroid());
	      }
	      else if(clusterType==2){
		x_clu.push_back(cl->GetMeanCaruana());
	      }
	      else if(clusterType==3){
		x_clu.push_back(cl->GetParabolaMean());
	      }
	    }
	  }

	  TGraph g_display_clu(z_clu.size(), &z_clu[0], &x_clu[0]);
	  std::string g_display_clu_name = "g_display_clu_Ev"+std::to_string(eventNumber);
	  g_display_clu.SetName(g_display_clu_name.c_str());
	  g_display_clu.GetXaxis()->SetTitle("z [mm]");
	  g_display_clu.GetYaxis()->SetTitle("x [mm]");
	  g_display_clu.SetMarkerColor(kBlue);
	  g_display_clu.SetMarkerSize(1.5);
	  g_display_clu.SetMarkerStyle(22);
	  //g_display_clu->Draw("P SAME");
	  c_ev_display->cd();
	  g_display_clu.Draw("P SAME");

	  if ( verbose ) std::cout << "drew clusters" << std::endl;

	  //-------------------------------------------------------------------//
	  //                  Reconstructed Bad Clusters
	  //-------------------------------------------------------------------//

	  NSWClusterList badClusterList = o_HT->GetBadClusterList();

	  for ( int layer=min_layer; layer<max_layer+1; layer++) {
	    for (int icl=0; icl<badClusterList.GetNumClusters(layer); icl++){
	      
	      std::string reason;
	      NSWCluster *cl = badClusterList.GetCluster(layer, icl);
	      
	      if ( verbose ) std::cout << "bad cluster layer " << layer << " mean " << cl->GetMeanCaruana() << std::endl;

	      if(!(o_HT->isGoodCluster(cl,reason,clusterType, startingStripPos, endStripPos))){
		
		if(reason == "cluster_wid_lt2"){
		  std::vector<float> z_bad_clu, x_bad_clu;
		  z_bad_clu.clear();
		  x_bad_clu.clear();
		  
		  double mean = 0.0;
		  if(clusterType==0){
		    mean = cl->GetCOG();
		  }
		  else if(clusterType==1){
		    mean = cl->GetGaussianCentroid();
		  }
		  else if(clusterType==2){
		    mean = cl->GetMeanCaruana();
		  }
		  else if(clusterType==3){
		    mean = cl->GetParabolaMean();
		  }
		    
		  z_bad_clu.push_back(cl->GetZPosition());
		  if(clusterType==0){
		    x_bad_clu.push_back(cl->GetCOG());
		  }
		  else if(clusterType==1){
		    x_bad_clu.push_back(cl->GetGaussianCentroid());
		  }
		  else if(clusterType==2){
		    x_bad_clu.push_back(cl->GetMeanCaruana());
		  }
		  else if(clusterType==3){
		    x_bad_clu.push_back(cl->GetParabolaMean());
		  }

		  //c_evtDisplay_perev->cd();
		  if(z_bad_clu.size()!=0){
		    TGraph g_display_bad_clu_wid(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_wid_Ev"+std::to_string(eventNumber);
		    g_display_bad_clu_wid.SetName(g_display_bad_clu_name.c_str());
		    g_display_bad_clu_wid.GetXaxis()->SetTitle("z [mm]");
		    g_display_bad_clu_wid.GetYaxis()->SetTitle("x [mm]");
		    g_display_bad_clu_wid.SetMarkerColor(kBlack);
		    g_display_bad_clu_wid.SetMarkerSize(1.5);
		    g_display_bad_clu_wid.SetMarkerStyle(29);
		    //g_display_bad_clu_wid->Draw("P SAME");
		    c_ev_display->cd();
		    g_display_bad_clu_wid.Draw("P SAME");
		  }
		}
		else if(reason == "recopos_not_middle"){
		  std::vector<float> z_bad_clu, x_bad_clu;
		  z_bad_clu.clear();
		  x_bad_clu.clear();
		  
		  double mean= 0.0;
		  if(clusterType==0){
		    mean = cl->GetCOG();
		  }
		  else if(clusterType==1){
		    mean = cl->GetGaussianCentroid();
		  }
		  else if(clusterType==2){
		    mean = cl->GetMeanCaruana();
		  }
		  else if(clusterType==3){
		    mean = cl->GetParabolaMean();
		  }

		  z_bad_clu.push_back(cl->GetZPosition());
		  if(clusterType==0){
		    x_bad_clu.push_back(cl->GetCOG());
		  }
		  else if(clusterType==1){
		    x_bad_clu.push_back(cl->GetGaussianCentroid());
		  }
		  else if(clusterType==2){
		    x_bad_clu.push_back(cl->GetMeanCaruana());
		  }
		  else if(clusterType==3){
		    x_bad_clu.push_back(cl->GetParabolaMean());
		  }

		  //c_evtDisplay_perev->cd();
		  if(z_bad_clu.size()!=0){
		    TGraph g_display_bad_clu_pos(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_pos_Ev"+std::to_string(eventNumber);
		    g_display_bad_clu_pos.SetName(g_display_bad_clu_name.c_str());
		    g_display_bad_clu_pos.GetXaxis()->SetTitle("z [mm]");
		    g_display_bad_clu_pos.GetYaxis()->SetTitle("x [mm]");
		    g_display_bad_clu_pos.SetMarkerColor(kBlack);
		    g_display_bad_clu_pos.SetMarkerSize(1.5);
		    g_display_bad_clu_pos.SetMarkerStyle(49);
		    //g_display_bad_clu_pos->Draw("P SAME");
		    c_ev_display->cd();
		    g_display_bad_clu_pos.Draw("P SAME");
		  }
		}
		else if(reason == "chn_bad"){
		  std::vector<float> z_bad_clu, x_bad_clu;
		  z_bad_clu.clear();
		  x_bad_clu.clear();
		  
		  double mean= 0.0;
		  if(clusterType==0){
		    mean = cl->GetCOG();
		  }
		  else if(clusterType==1){
		    mean = cl->GetGaussianCentroid();
		  }
		  else if(clusterType==2){
		    mean = cl->GetMeanCaruana();
		  }
		  else if(clusterType==3){
		    mean = cl->GetParabolaMean();
		  }

		  z_bad_clu.push_back(cl->GetZPosition());
		  if(clusterType==0){
		    x_bad_clu.push_back(cl->GetCOG());
		  }
		  else if(clusterType==1){
		    x_bad_clu.push_back(cl->GetGaussianCentroid());
		  }
		  else if(clusterType==2){
		    x_bad_clu.push_back(cl->GetMeanCaruana());
		  }
		  else if(clusterType==3){
		    x_bad_clu.push_back(cl->GetParabolaMean());
		  }
		  
		  //c_evtDisplay_perev->cd();
		  if(z_bad_clu.size()!=0){
		    TGraph g_display_bad_clu_chn(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_chn_Ev"+std::to_string(eventNumber);
		    g_display_bad_clu_chn.SetName(g_display_bad_clu_name.c_str());
		    g_display_bad_clu_chn.GetXaxis()->SetTitle("z [mm]");
		    g_display_bad_clu_chn.GetYaxis()->SetTitle("x [mm]");
		    g_display_bad_clu_chn.SetMarkerColor(kBlack);
		    g_display_bad_clu_chn.SetMarkerSize(1.5);
		    g_display_bad_clu_chn.SetMarkerStyle(34);
		    //g_display_bad_clu_chn->Draw("P SAME");
		    c_ev_display->cd();
		    g_display_bad_clu_chn.Draw("P SAME");
		  }
		}
		else if(reason == "middle_no_neighbour"){
		  std::vector<float> z_bad_clu, x_bad_clu;
		  z_bad_clu.clear();
		  x_bad_clu.clear();
		  
		  double mean= 0.0;
		  if(clusterType==0){
		    mean = cl->GetCOG();
		  }
		  else if(clusterType==1){
		    mean = cl->GetGaussianCentroid();
		  }
		  else if(clusterType==2){
		    mean = cl->GetMeanCaruana();
		  }
		  else if(clusterType==3){
		    mean = cl->GetParabolaMean();
		  }
		  
		  z_bad_clu.push_back(cl->GetZPosition());
		  if(clusterType==0){
		    x_bad_clu.push_back(cl->GetCOG());
		  }
		  else if(clusterType==1){
		    x_bad_clu.push_back(cl->GetGaussianCentroid());
		  }
		  else if(clusterType==2){
		    x_bad_clu.push_back(cl->GetMeanCaruana());
		  }
		  else if(clusterType==3){
		    x_bad_clu.push_back(cl->GetParabolaMean());
		  }
		  
		  //c_evtDisplay_perev->cd();
		  if(z_bad_clu.size()!=0){
		    TGraph g_display_bad_clu_neigh(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_neigh_Ev"+std::to_string(eventNumber);
		    g_display_bad_clu_neigh.SetName(g_display_bad_clu_name.c_str());
		    g_display_bad_clu_neigh.GetXaxis()->SetTitle("z [mm]");
		    g_display_bad_clu_neigh.GetYaxis()->SetTitle("x [mm]");
		    g_display_bad_clu_neigh.SetMarkerColor(kBlack);
		    g_display_bad_clu_neigh.SetMarkerSize(1.5);
		    g_display_bad_clu_neigh.SetMarkerStyle(47);
		    //g_display_bad_clu_neigh->Draw("P SAME");
		    c_ev_display->cd();
		    g_display_bad_clu_neigh.Draw("P SAME");
		  }
		}
		else if(reason == "bkg_relbcid_chn"){
		  std::vector<float> z_bad_clu, x_bad_clu;
		  z_bad_clu.clear();
		  x_bad_clu.clear();
		  
		  double mean= 0.0;
		  if(clusterType==0){
		    mean = cl->GetCOG();
		  }
		  else if(clusterType==1){
		    mean = cl->GetGaussianCentroid();
		  }
		  else if(clusterType==2){
		    mean = cl->GetMeanCaruana();
		  }
		  else if(clusterType==3){
		    mean = cl->GetParabolaMean();
		  }
		  
		  z_bad_clu.push_back(cl->GetZPosition());
		  if(clusterType==0){
		    x_bad_clu.push_back(cl->GetCOG());
		  }
		  else if(clusterType==1){
		    x_bad_clu.push_back(cl->GetGaussianCentroid());
		  }
		  else if(clusterType==2){
		    x_bad_clu.push_back(cl->GetMeanCaruana());
		  }
		  else if(clusterType==3){
		    x_bad_clu.push_back(cl->GetParabolaMean());
		  }
		  
		  //c_evtDisplay_perev->cd();
		  if(z_bad_clu.size()!=0){
		    TGraph g_display_bad_clu_bkg(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_bkg_Ev"+std::to_string(eventNumber);
		    g_display_bad_clu_bkg.SetName(g_display_bad_clu_name.c_str());
		    g_display_bad_clu_bkg.GetXaxis()->SetTitle("z [mm]");
		    g_display_bad_clu_bkg.GetYaxis()->SetTitle("x [mm]");
		    g_display_bad_clu_bkg.SetMarkerColor(kBlack);
		    g_display_bad_clu_bkg.SetMarkerSize(1.5);
		    g_display_bad_clu_bkg.SetMarkerStyle(39);
		    //g_display_bad_clu_bkg->Draw("P SAME");
		    c_ev_display->cd();
		    g_display_bad_clu_bkg.Draw("P SAME");
		  }
		}
	      }
	    }
	  } // end of loop over bad clusters

	  if ( verbose ) std::cout << "drew bad clusters" << std::endl;
	  
	  //-------------------------------------------------------------//
	  //                   Reconstructed Segments
	  //-------------------------------------------------------------//

	  std::vector<std::vector< std::vector <NSWCluster*>>> acceptedCell_clusters = o_HT->GetAcceptedHTCell_Clusters();
	  int num_HTcells = acceptedCell_clusters.size();
	  
	  if ( verbose ) std::cout << "Number of tracks from HT in event " << eventNumber << ": " << num_HTcells << std::endl;
	  
	  hNum_HTcells->Fill(num_HTcells);

	  if ( verbose ) std::cout << "filled num HT cells" << std::endl;

	  std::vector<NSWClusterList> acceptedHTCell_ClusterList(num_HTcells);
	  //	  acceptedHTCell_ClusterList.resize(0);

	  for ( int icell=0; icell<num_HTcells; icell++ ) {
	    NSWClusterList iCell_ClusterList;
	    iCell_ClusterList.Clear();
	    if ( verbose ) std::cout << "setting icell cluster list" << std::endl;
	    
	    for ( int il=0; il<acceptedCell_clusters.at(icell).size(); il++ ) {
	      for ( int ic=0; ic<acceptedCell_clusters.at(icell).at(il).size(); ic++ ) {
		if ( verbose ) std::cout << " cluster HT " << il << " " << acceptedCell_clusters.at(icell).at(il).at(ic)->GetMeanCaruana() << std::endl;
	      }
	    }

	    //iCell_ClusterList.SetClusterList(acceptedCell_clusters.at(icell));
	    if ( verbose ) std::cout << "set icell cluster list" << std::endl;
	    //acceptedHTCell_ClusterList.push_back(iCell_ClusterList);
	    acceptedHTCell_ClusterList.at(icell).SetClusterList(acceptedCell_clusters.at(icell));
	  }

	  if ( verbose ) std::cout << "got clusters" << std::endl;

	  //===========================================================================================//
	  //============================= Resolution code taken from GIF-sTGC_res =====================//
	  //===========================================================================================// 

	  NSWClusterList clusterList             = o_HT->GetClusterList();
	  NSWClusterList clusterList_badclusters = o_HT->GetBadClusterList();

	  NSWSegmentList segmentList;
	  segmentList.Clear();

	  //--------------------------------------------------------------------------------//  
	  //                            Reconstruct Segments
	  //--------------------------------------------------------------------------------//

	  for ( int icell =0; icell<acceptedHTCell_ClusterList.size(); icell++ ) {
	    // Here change the second argument according to which 4 layers you want. e.g. 0 means Layers 0 1 2 3 and 4 means layers 4 5 6 7.
	    segmentList.multiCluster_Fill4(&acceptedHTCell_ClusterList.at(icell), 4, 1000, clusterType, 0.3);	    
	  }

	  if ( verbose ) std::cout << "reco segments" << std::endl;

	  //--------------------------------------------------------------------------------//
	  //                        Plots on Global Cluster Distributions
	  //--------------------------------------------------------------------------------//

	  for (int layer=min_layer; layer<max_layer+1; layer++){
	    if( verbose ) std::cout << "Number of clusters @patmasid: Layer " << layer << " number of clusters: " << clusterList.GetNumClusters(layer) << std::endl;
	    for ( int icl=0; icl<clusterList.GetNumClusters(layer); icl++ ) {

	      NSWCluster *cl = clusterList.GetCluster(layer, icl);
	      
	      for(int ichn=0; ichn<cl->GetNumChannel(); ichn++){
		NSWChannel* chn = cl->GetChannel(ichn);
		if(chn == NULL) continue;
		if( verbose ) std::cout << "Cluster channel info: CH LAYER: " << chn->GetLayer() << " CH DatabaseID: " << chn->GetDatabaseID() << " CH NUM: " << chn->GetDetectorStrip() << " CH PDO: " << chn->GetPDO() << " CH AMPLITUDE mV: " << chn->GetAmplitude_mV() << " CH Baseline: " << chn->GetBaseline() << " CH Gain: " << chn->GetGain() <<  " CH POS " <<  chn->GetStripPosition() << std::endl;
	      }
	      int num_chns = cl->GetNumChannel();

	      //if ( verbose ) std::cout << "Cluster number: " << icl << " num_chns: " << cl->GetWidth() 
	      //<< " gaussian centroid: " << cl->GetGaussianCentroid() << " caruana mean: " << cl->GetMeanCaruana() << std::endl;
		  
	      hClusterWidth->Fill(cl->GetWidth());
		  
	      hMaxPdo->Fill(cl->GetMaxPDO());
	      TF1* f_clu = cl->GetGaussianFunction();
	      hPeakPdo->Fill(f_clu->GetMaximum( cl->GetChannel(0)->GetStripPosition(), cl->GetChannel(num_chns-1)->GetStripPosition() ));
	      hNumPot->Fill(cl->GetNumPotential());
	      hNumMask->Fill(cl->GetNumMasked());
	      hNumBkg->Fill(cl->GetNumBkg());
		  
	      hClusCharge[layer]->Fill(cl->GetTotalAmplitude());
	      hClusCharge_1stCalib[layer]->Fill(cl->GetTotalAmplitude_1stCalib());

	      for(int ichn=0; ichn<num_chns; ichn++){
		NSWChannel* channel = cl->GetChannel(ichn);
		if(channel == NULL) continue;
		if(channel->GetNeighbor()==0){
		  hNeighFlag0_Clus[layer]->Fill(channel->GetAmplitude_mV());
		  hNeighFlag0_Clus_1stCalib[layer]->Fill(channel->GetAmplitude());
		}
		else if(channel->GetNeighbor()==1){
		  hNeighFlag1_Clus[layer]->Fill(channel->GetAmplitude_mV());
		  hNeighFlag1_Clus_1stCalib[layer]->Fill(channel->GetAmplitude());
		}
	      }
	      
	      /*z_clu.push_back(cl->GetZPosition());
		if(clusterType==0){
		x_clu.push_back(cl->GetCOG());
		}
		else if(clusterType==1){
		x_clu.push_back(cl->GetGaussianCentroid()); 
		} 
		else if(clusterType==2){ 
		x_clu.push_back(cl->GetMeanCaruana()); 
		}
		else if(clusterType==3){
		x_clu.push_back(cl->GetParabolaMean());
		}
	      */
		  
	    } // end of loop over all clusters in event
		
	    hNumClu[layer]->Fill( clusterList.GetNumClusters(layer) );	  

	  }// end of loop over layers
       
	  if ( verbose ) std::cout << "plotted clusters" << std::endl;

	  //---------------------------------------------------//
	  //       if no segment reconstructed. end event
	  //---------------------------------------------------//
	  
	  if(segmentList.GetNumSegments()==0){
	    /*if(!isEvtRej_noHitsLayers && !isEvtRej_noClusLayers){
		NrejEvnts_noSeg++;
		//if ( verbose ) std::cout << "num of layers with no clusters: " << num_noClusLayers << std::endl;
	    }*/
	    hSeg->Fill(segmentList.GetNumSegments());
	    c_ev_display->Update();
            ps->NewPage();
	    //delete o_HT;
	    continue;
	  }
	  
	  //if ( verbose ) std::cout << "num of segments: " << segmentList.GetNumSegments() << std::endl;
	  //hSeg->Fill(segmentList.GetNumSegments());
	  //if ( verbose ) std::cout << "Number of tracks from HT in event after segment reconstruction: " << eventNumber << ": " << segmentList.GetNumSegments() << std::endl;
	  
	  //-----------------------------------------------------------------------//
	  //            remove out of beam region segments
	  //-----------------------------------------------------------------------//

	  //if ( verbose ) std::cout << "//================= Event ==================//" << std::endl;
		  
	  for(int i=0; i<segmentList.GetNumSegments(); i++){
	    //if ( verbose ) std::cout << "//================= Segment number: " << i << " ==================//" << std::endl;
	    NSWSegment* seg = segmentList.GetSegment(i);
	    //if ( verbose ) std::cout << "//========== chi square: " << seg->GetChi2() << std::endl;

	    //---------------------------------------------------------------------------//
	    //  Check that the segment's predicted loc. of hits are within beam region
	    //---------------------------------------------------------------------------//

	    std::vector<double> predicted_values(8,0.0);
	    int num_layers = max_layer-min_layer+1;
	    std::vector<bool> isLayerInBeam(num_layers,false);
            for (int j=min_layer; j<max_layer+1; j++) {
              double pred = seg->GetPredicted(j);
              predicted_values[j-min_layer] = pred;
	      if(pred>=pos_min_beam && pred<=pos_max_beam){
                isLayerInBeam[j-min_layer] = true;
              }
            }

	    //------------------------------------------------------------------------------------//
	    //   If all 4 predicted locations are within the beam region plus angle/chi2 cut
	    //               accept the segment
	    //------------------------------------------------------------------------------------//

	    if(count(isLayerInBeam.begin(), isLayerInBeam.end(), true)<(num_layers) || (seg->GetChi2()/seg->GetDOF()) > 40){ 
		//fabs(seg->GetAngle()) >= 15 ) { //|| 
		//(seg->GetChi2()/seg->GetDOF()) > 40){
	      segmentList.RemoveSegment(i);
	      i--;
	    }
	    
	    //if ( verbose ) std::cout << "//===============================================//" << std::endl;
	    
	    //if ( verbose ) std::cout << "Number of tracks from HT in event after segment reconstruction: " << eventNumber << ": " << segmentList.GetNumSegments() << std::endl;
	    
	  } // end of segment loop
	  
	  hSeg->Fill(segmentList.GetNumSegments());
	  
  	  if ( verbose ) std::cout << "Number of segments: " << segmentList.GetNumSegments() 
				   << " in " << pos_min_beam << "-" << pos_max_beam << std::endl;

	  //--------------------------------------------------------//
	  //        if no segments in beam region finish event
	  //--------------------------------------------------------//
	  
	  if(segmentList.GetNumSegments()==0){
	    c_ev_display->Update();
            ps->NewPage();
	    //delete o_HT;
	    continue;
	  }
	  
	  //------------------------------------------------//
	  //          Plot in-beam segment distributions
	  //------------------------------------------------//

	  num_events_found++;

	  for (int i=0; i<segmentList.GetNumSegments(); i++){

            NSWSegment* s = segmentList.GetSegment(i);

	    //----------------------------------------------------//
	    //
	    //----------------------------------------------------//

	    std::vector<float> z, x;
            x.clear();
            z.clear();
            double zmin, zmax;
	    std::vector<float> sigmax, sigmaz;
            sigmax.clear();
            sigmaz.clear();

	    zmin = NSWCluster::GetZPosition(min_layer,true,0) - 10;
            zmax = NSWCluster::GetZPosition(max_layer,true,0) + 10;

	    //-------------------------------------------------//

	    std::vector<float> sig = s->GetSigma();

	    for ( int ii=0; ii<sig.size(); ii++ ) {
	      sigmax.push_back(sig[ii]);
              sigmaz.push_back(0.0);
	    }

	    //------------------------------------------------//

            for (int layer=min_layer; layer<max_layer+1; layer++){

	      std::string gName = "SegClu_Ev"+std::to_string(eventNumber)+"_"+"Seg"+std::to_string(i)+"_"+"Layer"+std::to_string(layer);
              //std::cout << "layer: " << layer << std::endl;
              NSWCluster* c = s->GetCluster(layer);

              if(c == NULL) continue;

              z.push_back(c->GetZPosition());
	      if(clusterType==0){
		x.push_back(c->GetCOG());
	      }
              else if(clusterType==1){
                x.push_back(c->GetGaussianCentroid());
              }
              else if(clusterType==2){
                x.push_back(c->GetMeanCaruana());
              }
	      else if(clusterType==3){
		x.push_back(c->GetParabolaMean());
	      }

	      if ( verbose ) std::cout << "Selected Segment cluster " << layer << " " << x.back() << std::endl;

	    }

	    //-----------------------------------------------------//
	    //          Draw the segment in the plot
	    //-----------------------------------------------------//

	    TF1 f_seg("f_seg","[0]+[1]*x",zmin,zmax);
            f_seg.SetParameter(0,s->GetIntercept());
            f_seg.SetParameter(1,s->GetSlope());	    

	    std::string seg_name = "Ev"+std::to_string(eventNumber)+"_"+"Seg"+std::to_string(i);
	    TGraphErrors gSegment(z.size(), &z[0], &x[0], &sigmaz[0], &sigmax[0]);
            gSegment.SetName(seg_name.c_str());
            gSegment.SetTitle();
            gSegment.GetXaxis()->SetTitle("z [mm]");
            gSegment.GetYaxis()->SetTitle("x [mm]");
            gSegment.SetMarkerColor(kRed);
            gSegment.SetMarkerSize(1.5);
            gSegment.SetMarkerStyle(20);

	    c_ev_display->cd();
	    gSegment.Draw("P SAME");
	    f_seg.DrawF1(zmin, zmax, "SAME");

	    c_ev_display->Update();
	    ps->NewPage();

	  } // Loop over segments
	

	  //----------------------------------------------------//
	  //          Plot 2D Evt. Display
	  //----------------------------------------------------//

	  for (int i=0; i<segmentList.GetNumSegments(); i++){

            NSWSegment* s = segmentList.GetSegment(i);

            //----------------------------------------------------//

	    std::vector<float> z, x;
            x.clear();
            z.clear();
            double zmin, zmax;
	    std::vector<float> sigmax, sigmaz;
            sigmax.clear();
            sigmaz.clear();

            zmin = NSWCluster::GetZPosition(min_layer,true,0) - 10;
            zmax = NSWCluster::GetZPosition(max_layer,true,0) + 10;

	    /*
	    //-------------------------------------------------------//
	    //                  2D Event Display
	    //-------------------------------------------------------//
	    
	    //char canvasName[40], wire_graphName[40], pad_graphName[40];
	    char canvasName[40], histName[40];
	    sprintf(canvasName, "c_Evt_%li_display", eventNumber);
	    sprintf(histName, "EvtNo_%li_display", eventNumber);
	    //sprintf(wire_graphName, "Evt_%li_wire_display", eventNumber);
	    //sprintf(pad_graphName, "Evt_%li_pad_display", eventNumber);
	    TCanvas * c_display_per_event = new TCanvas(canvasName, canvasName, 800, 600);
	    c_display_per_event->cd();
	    hframe->SetTitle(histName);
	    hframe->Draw("");

	    pad_frame->Draw();
	    */

	    //------------------------------------------------------//
	    //            Draw pads 
	    //------------------------------------------------------//

	    /*
	    for(int layer=0; layer<4; layer++){
	      for(int i_pad=0; i_pad<pad_pos_2D[layer].size(); i_pad++){

		for(int i_wire=0; i_wire<wire_pos_all[layer].size(); i_wire++){
		  wire_box = new TBox(2120+layer*20, wire_pos_all[layer][i_wire]-wire_err_all[layer][i_wire], 
				      3520-layer*20, wire_pos_all[layer][i_wire]+wire_err_all[layer][i_wire]);
		  //wire_box->SetLineColor(colors[layer]);
		  //wire_box->SetFillColorAlpha(colors[layer],0.10);
		  //wire_box->SetFillStyle(0);//styles[layer]);//4050);
		  //wire_box->SetLineColor(0);
		  //wire_box->SetLineWidth(0);
		  //std::cout << "draw wire" << wire_pos_all[layer][i_wire] << std::endl;
		  //wire_box->Draw("fsame");
		}

		for(int i_strip=0; i_strip<strip_pos_all[layer].size(); i_strip++){
		  strip_box = new TBox(strip_pos_all[layer][i_strip]-1.6, -640+layer*20, 
				       strip_pos_all[layer][i_strip]+1.6, 620-layer*10);
		  //strip_box->SetLineColor(colors[layer]);
		  //strip_box->SetFillColorAlpha(colors[layer],0.10);
		  //strip_box->SetFillStyle(styles[layer]);//4050);
		  //std::cout << "draw strip" << strip_pos_all[layer][i_strip] << std::endl;
		  //strip_box->Draw("fsame");
		}

		double x_corr[5], y_corr[5];
		for(int i_cvx=3; i_cvx>=0; i_cvx--){
		  x_corr[3-i_cvx] = pad_pos_2D[layer][i_pad][i_cvx];
		  y_corr[3-i_cvx] = pad_pos_2D[layer][i_pad][i_cvx+4];
		  //printf("%3.2f %3.2f\n", pad_pos_2D[layer][i_pad][i_cvx], pad_pos_2D[layer][i_pad][i_cvx+4]);
		}
		x_corr[4] = x_corr[0];
		y_corr[4] = y_corr[0];

		pad_poly = new TPolyLine(5, x_corr, y_corr);
		//pad_poly->SetFillColorAlpha(colors[layer],0.5);
		pad_poly->SetFillColor(colors[layer]);
		pad_poly->SetFillStyle(styles[layer]);
		pad_poly->SetLineColor(colors[layer]);
		pad_poly->SetLineWidth(2);
		pad_poly->Draw("flsame");
	      }

	    }

	    for (int layer=4; layer<8; layer++){
	      
	      NSWCluster* c = s->GetCluster(layer);
	      
	      if(c == NULL) continue;
	      
	      float mean = 0;
	      if(clusterType==0){
	        mean = c->GetCOG();
	      }
	      else if(clusterType==1){
		mean = c->GetGaussianCentroid();
	      }
	      else if(clusterType==2){
		mean = c->GetMeanCaruana();
	      }
	      else if(clusterType==3){
	        mean = c->GetParabolaMean();
	      }

	      if ( verbose ) std::cout << "Selected Segment cluster " << layer << " " << mean << std::endl;

	      cluster_box = new TBox(mean-1.6, -640+(layer-4)*20,
				     mean+1.6,  620-(layer-4)*10);
	      
	      cluster_box->SetLineColor(colors[layer]);
	      //cluster_box->SetFillColorAlpha(colors[layer],0.10);
	      cluster_box->SetFillStyle(styles[4]);//4050);
	      //std::cout << "draw segment cluster" << mean << std::endl;
	      cluster_box->Draw("fsame");
	    }
	    */
	  } // Loop over segments

	  /*
	  c_display_per_event->Update();
	  ps->NewPage();

	  //char drawName[40];
	  //sprintf(drawName, "img/c_Evt_%li_display.png", eventNumber);
	  //c_display_per_event->SaveAs(drawName);
	  */

	  //------------------------------------------------------//
	  //           Plot Segment Related Histograms
	  //------------------------------------------------------//

          for (int i=0; i<segmentList.GetNumSegments(); i++){

            NSWSegment* s = segmentList.GetSegment(i);

            //----------------------------------------------------//
            //
            //----------------------------------------------------//

	    //============== for debugging ===============//
	    //if(s->GetNumCluster()<=3) continue;
	    //============================================//
	    
            branch.y =  s->GetYPosition();
	    hNumClu_Seg->Fill(s->GetNumCluster());

            for (int layer=min_layer; layer<max_layer+1; layer++){
	      //if ( verbose ) std::cout << "GIF-sTGC_HT: layer - " << layer << std::endl;

              branch.pred[layer] = s->GetPredicted (layer);
              NSWCluster* c = s->GetCluster(layer);
	      
              if (c == NULL){
                branch.cog[layer] = -100;
                branch.cog2[layer] = -100;
                branch.charge[layer] = -100;
                branch.wid[layer] = -1;
                branch.z[layer] = -1;
                branch.res[layer] = -100;
                for (int i=0; i<10; i++){
                  branch.amp[layer][i] = -1;
                  branch.time[layer][i] = -100;
                }
                branch.gap[layer] = -1;
                branch.start[layer] = -1;
                branch.peak[layer] = -1;
                branch.nflags[layer] = -1;
                branch.raw[layer] = -1;
                branch.para[layer] = -1;
                branch.cog3[layer] = -1;
                branch.gaus[layer] = -1;
		branch.meanCaruana[layer] = -1;
              } else {
		int num_chns = c->GetNumChannel();

		hClusterWidth_Seg->Fill(c->GetWidth());
		hMaxPdo_Seg->Fill(c->GetMaxPDO());
		TF1* f_clu = c->GetGaussianFunction();
		hPeakPdo_Seg->Fill(f_clu->GetMaximum( c->GetChannel(0)->GetStripPosition(), c->GetChannel(num_chns-1)->GetStripPosition() ));
		hNumPot_Seg->Fill(c->GetNumPotential());
		hNumMask_Seg->Fill(c->GetNumMasked());
		hNumBkg_Seg->Fill(c->GetNumBkg());

		hClusCharge_Seg[layer]->Fill(c->GetTotalAmplitude());
		hClusCharge_Seg_1stCalib[layer]->Fill(c->GetTotalAmplitude_1stCalib());

		hClusCentroid_Seg[layer]->Fill(c->GetGaussianCentroid());
		hMaxPdoStripCharge_Seg[layer]->Fill(c->GetChannel(c->Getimax())->GetAmplitude_mV());
		hMaxPdoStripCharge_Seg_1stCalib[layer]->Fill(c->GetChannel(c->Getimax())->GetAmplitude());

		int neigh_1 = c->Getimax()-1;
		int neigh_2 = c->Getimax()+1;
		
		if ( neigh_1 > 0 && neigh_2 < num_chns ) {
		  
		  bool isNeigh1Null = (c->GetChannel(neigh_1)==NULL);
		  bool isNeigh2Null = (c->GetChannel(neigh_2)==NULL);
		
		  if(!isNeigh1Null && isNeigh2Null){
		    hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_1)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		  }
		  else if(isNeigh1Null && !isNeigh2Null){
		    hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_2)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		  }
		  else if(isNeigh1Null && isNeigh2Null){
		    
		  }
		  else{
		    //if ( verbose ) std::cout << "neigh 1 " << neigh_1 << " neigh 2  " << neigh_2 << std::endl;
		    
		    if(c->GetChannel(neigh_1)->GetAmplitude_mV() >= c->GetChannel(neigh_2)->GetAmplitude_mV()){
		      hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_1)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		      hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_2)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		    }
		    else{
		      hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_2)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		      hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_1)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		    }
		  }
		}

		if(s->GetNumCluster()==4){
		  hClusCharge_Seg_4of4[layer]->Fill(c->GetTotalAmplitude());
		  hClusCharge_Seg_4of4_1stCalib[layer]->Fill(c->GetTotalAmplitude_1stCalib());
		  hClusCentroid_Seg_4of4[layer]->Fill(c->GetGaussianCentroid());
		  hMaxPdoStripCharge_Seg_4of4[layer]->Fill(c->GetChannel(c->Getimax())->GetAmplitude_mV());
		  hMaxPdoStripCharge_Seg_4of4_1stCalib[layer]->Fill(c->GetChannel(c->Getimax())->GetAmplitude());
		}

		int sum_AllRelBC = c->GetNumPotential()+c->GetNumMasked()+c->GetNumBkg();
		double frac_Pot = c->GetNumPotential()*1.0/sum_AllRelBC;
		double frac_Mask = c->GetNumMasked()*1.0/sum_AllRelBC;
		double frac_Bkg = c->GetNumBkg()*1.0/sum_AllRelBC;
		
		h_fracNumPot_Seg->Fill(frac_Pot);
		h_fracNumMask_Seg->Fill(frac_Mask);
		h_fracNumBkg_Seg->Fill(frac_Bkg);
		h_fracNumPot_vs_Ang->Fill(s->GetAngle(), frac_Pot);
		h_fracNumMask_vs_Ang->Fill(s->GetAngle(), frac_Mask);
		h_fracNumBkg_vs_Ang->Fill(s->GetAngle(), frac_Bkg);
		h_fracNumPot_vs_Chi2->Fill(s->GetChi2(), frac_Pot);
		h_fracNumMask_vs_Chi2->Fill(s->GetChi2(), frac_Mask);
		h_fracNumBkg_vs_Chi2->Fill(s->GetChi2(), frac_Bkg);

		//if ( verbose ) std::cout << "cluster width: " << c->GetWidth() << std::endl;
                branch.cog[layer] = c->GetMeanCaruana();//c->GetCOG();
                branch.cog2[layer] = c->GetCOG2();
                branch.charge[layer] = c->GetCharge();
                branch.wid[layer] = c->GetNumChannel();
                branch.z[layer] = c->GetZPosition();
                branch.res[layer] = c->GetMeanCaruana()- branch.pred[layer];//c->GetCOG() - branch.pred[layer];
                int chnMax = c->GetNumChannel();
                if (chnMax > 10) chnMax = 10;
		for (int j=0; j<chnMax; j++){
                  NSWChannel *chn = c->GetChannel(j);
                  if (chn != NULL) {
                    branch.amp[layer][j] = chn->GetAmplitude_mV();
                    branch.time[layer][j] = chn->GetPeakingTime();
                  } else {
                    branch.amp[layer][j] = -1;
                    branch.time[layer][j] = -1;
                  }
                }
                for (int j=chnMax; j<10; j++){
                  branch.amp[layer][j] = -1;
                  branch.time[layer][j] = -1;
                }
                branch.gap[layer] = c->GetNumMissing();
		branch.start[layer] = c->GetStartChannel();
                branch.peak[layer] = c->GetPeakChannel();
                branch.nflags[layer] = c->CountNeighbors();
                branch.raw[layer] = c->GetParabolaRaw();
                branch.para[layer] = c->GetParabola();
                branch.cog3[layer] = c->GetCOG3();
                branch.gaus[layer] = c->GetGaussianCentroid();
		branch.meanCaruana[layer] = c->GetMeanCaruana();
	      }
	    }// next layer

	    branch.inter = s->GetIntercept();
            branch.slope = s->GetSlope();
            branch.chi2 = s->GetChi2();
	    branch.invcdf = s->GetInvCDF();
	    branch.cdf = s->GetCDF();
            branch.angle = s->GetAngle();
            branch.nprec = s->GetNumPrecision();
            branch.nclu = s->GetNumCluster();
	    
            hChi2->Fill(s->GetChi2());
	    hChi2divDOF->Fill(s->GetChi2()/s->GetDOF());
	    hAng->Fill(s->GetAngle());
            hAng_Chi2->Fill(s->GetAngle(), s->GetChi2());
            hChi2_nSeg->Fill(s->GetChi2(), segmentList.GetNumSegments());
	    hInvCDFvschi2->Fill(s->GetChi2(), s->GetInvCDF());
	    hCDFvschi2->Fill(s->GetChi2(), s->GetCDF());
	    hInvCDF->Fill(s->GetInvCDF());
	    hCDF->Fill(s->GetCDF());

	    tree->Fill();
            numSeg++;
            totSeg++;
            for (int j=i+1; j<segmentList.GetNumSegments(); j++){
              NSWSegment* s2 = segmentList.GetSegment(j);
              hDist->Fill(s->GetIntercept() - s2->GetIntercept());
	      //printf ("%d: %f %f\n", j, s->GetIntercept() , s2->GetIntercept());   
	    }
	    //} // next segment
	  
	  
	  
	    //############################### Where you select events with only one segment ==//
	    
	    
	    //============================ Michael's code not used ==========================//
	    
	    //double cog[4], para[4];
	    //for (int j=4; j<8; j++){
	    //if(seg->GetCluster(j) == NULL) continue;
	    //  cog[j-4]=seg->GetCluster(j)->GetCOG();
	    //}
	    //if (seg->GetChi2() < 50){ // fill alignment histograms                                                                                                                                      
	    //  double m = (cog[0]-cog[3])/3;
	    //  hAli5->Fill((7-5)*m+cog[3]-cog[1]);
	    //  hAli6->Fill((7-6)*m+cog[3]-cog[2]);
	    // }
	    //hResCog->Fill(cog[1]+ali5-0.5*(cog[0]+cog[2]+ali6));
	    //bool bad = false;
	    //for (int j=4; j<7; j++) {
	    //  NSWCluster* cl = seg->GetCluster(j);
	    //  if (cl->GetCOG3() < -0.99) bad = true;
	    //  cog[j-4]=(cl->GetPeakChannel()+cl->GetCOG3()*5/3.5)*3.2;
	    //  para[j-4]=(cl->GetPeakChannel()+cl->GetParabola())*3.2;
	    //  if ((j % 2) == 1){ // stagger by -0.5*pitch
	    //	cog[j-4] -= 0.5*seg->GetPitch();
	    //	para[j-4] -= 0.5*seg->GetPitch();
	    //  }
	    //}
	    //if (!bad) {
	    //  hResCog3->Fill(cog[1]+ali5-0.5*(cog[0]+cog[2]+ali6));
	    //  hResPara->Fill(para[1]+ali5-0.5*(para[0]+para[2]+ali6));
	    //}
	    
	    //=================================================================================//
	    //                                                                                                                                                                   
	    // Resolution eval with sigma_inclusive*sigma_exclusive                                                                                                                       
	    //  
	    
	    for(int i=min_layer; i<max_layer+1; i++){
	      if(s->GetCluster(i) == NULL) continue;
	      NSWCluster *c1 = s->GetCluster(i);
	      double mean_clu = -1.0;
	      if(clusterType==0)
		mean_clu = c1->GetCOG();
	      else if(clusterType==1)
		mean_clu = c1->GetGaussianCentroid();
	      else if(clusterType==2)
		mean_clu = c1->GetMeanCaruana();
	      else if(clusterType==3)
		mean_clu = c1->GetParabolaMean();

	      if (i==5)
		hResIn[i]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - mean_clu);//-ali5 );
	      else if (i==6)
		hResIn[i]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - mean_clu); //-ali6 );
	      else
		hResIn[i]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - mean_clu);
	      
	      std::vector<double> xEx, yEx, sigEx;
	      double interEx, slopeEx, chi2Ex;
	      double interErrEx, slopeErrEx;
	      xEx.clear();
	      yEx.clear();
	      sigEx.clear();
	      
	      for(int j=min_layer; j<max_layer+1; j++){
		if(i!=j){
		  if(s->GetCluster(j) == NULL) continue;
		  NSWCluster *c2 = s->GetCluster(j);
		  xEx.push_back(c2->GetZPosition());
		  if(clusterType==0)
		    yEx.push_back(c2->GetCOG());
		  else if(clusterType==1)
		    yEx.push_back(c2->GetGaussianCentroid());
		  else if(clusterType==2)
		    yEx.push_back(c2->GetMeanCaruana());
		  else if(clusterType==3)
		    yEx.push_back(c2->GetParabolaMean());
		  sigEx.push_back(0.3);
		  // if ( verbose ) std::cout<<"Exclusive data point    x"<<": "<<c2->GetZPosition()<<"\ty"<<": "<<c2->GetMeanCaruana()<<std::endl;
		}
	      }
	      // if ( verbose ) std::cout<<"In exclusive fit:"<<std::endl;
	      linfit(&xEx[0], &yEx[0], 3, &sigEx[0], 1, &interEx, &slopeEx, &interErrEx, &slopeErrEx, &chi2Ex);
	      if (i==5)
		hResEx[i]->Fill( (interEx+slopeEx*c1->GetZPosition()) - mean_clu);//-ali5 );
	      else if (i==6)
		hResEx[i]->Fill( (interEx+slopeEx*c1->GetZPosition()) - mean_clu);//-ali6 );
	      else
		hResEx[i]->Fill( (interEx+slopeEx*c1->GetZPosition()) - mean_clu);
	    }
	    //} // if 4 clusters
	  
	    // calculate efficiency: 
	    //if (((s->GetChi2()<18) && (s->GetNumClusters()==4)) ||
	    //	((s->GetChi2()<9) && (s->GetNumClusters()==3))) {
	    
	    //double pos_min_beam = 221*3.2 - 1.6;
	    //double pos_max_beam = 252*3.2;
	    
	    std::vector<double> predicted_values(8,0.0);
	    //std::vector<bool> isLayerInBeam(4,false);
	    for (int j=min_layer; j<max_layer+1; j++) {
	      double pred = s->GetPredicted(j);
	      predicted_values[j] = pred;
	      //if(pred>=pos_min_beam && pred<=pos_max_beam){
	      //  isLayerInBeam[j-4] = true;
	      //}
	    }
	    
	    //if(count(isLayerInBeam.begin(), isLayerInBeam.end(), true)>=3){
	    for (int j=min_layer; j<max_layer+1; j++) {
	      hN[j]->Fill(predicted_values[j]);
	      if (s->GetCluster(j) != NULL) hn[j]->Fill(predicted_values[j]);
	    } // next layer 
	    //}// is seg in beam


	    //----------------------------------------------------------------//
	    //      Fill Predicted Location for all 4/4 segments 
	    //      Fill predicted location on missing layer for 3/4 segments //
	    //----------------------------------------------------------------//

	    for(int j=min_layer; j<max_layer+1; j++){
	      if (s->GetNumCluster()==4) {
		h4o4SegPred_allLayers[j]->Fill(predicted_values[j]);
	      }
	      else if ( s->GetNumCluster()==3 ) {
		if (s->GetCluster(j) == NULL) h3o4SegPred_missingLayer[j]->Fill(predicted_values[j]);
	      }
	    }

	    //} // if good chi2 
	    //} // if one segment per event
	  
	  } // end of loop over segments

	  //===========================================================================================//
	  //=============================End of Resolution code =======================================//
          //===========================================================================================// 
	  
	  //delete o_HT;
	}// next sector

	//c_evtDisplay.push_back(c_evtDisplay_perev);
	
	if (eventNumber%100==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	eventNumber++;
	
        if (eventNumber >= 50000) break;
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0); // next event
    fclose(infile);
    //if ( verbose ) std::cout << "number of events with at least one track found: " << num_events_found << std::endl;
  } // next file

  delete o_HT;

  ps->Close();

  //---------------------------------------------//
  // efficiency vs location per layer
  //---------------------------------------------//

  for (int bin=0; bin<1000; bin++){

    std::vector<int>    n_3o4Pred_perLayer, n_4o4Pred_perLayer;
    std::vector<double> efficiency, error;
    n_3o4Pred_perLayer.resize(0);
    n_4o4Pred_perLayer.resize(0);
    efficiency.resize(0);
    error.resize(0);

    for (int layer=min_layer; layer<max_layer+1; layer++) {
      int n4o4Pred = h4o4SegPred_allLayers[layer]->GetBinContent(1+bin);
      int n3o4Pred = h3o4SegPred_missingLayer[layer]->GetBinContent(1+bin);
      n_4o4Pred_perLayer.push_back(n4o4Pred);
      n_3o4Pred_perLayer.push_back(n3o4Pred);
    }
    
    //CalculateEffi(n_totalHits_perLayer, n_totalPred_perLayer, efficiency, error);
    CalculateEffiNew(n_3o4Pred_perLayer, n_4o4Pred_perLayer, efficiency, error);

    for (int layer=0; layer<efficiency.size(); layer++) {
      if (h3o4SegPred_missingLayer[layer]->GetBinContent(1+bin)!=0 ||
	  h4o4SegPred_allLayers[layer]->GetBinContent(1+bin)   !=0 ) {
	hEffi[layer]->SetBinContent(1+bin, efficiency.at(layer));
	hEffi[layer]->SetBinError(1+bin, error.at(layer));
      }
    }
  }

  //--------------------------------------------//
  //  efficiency for the whole layer
  //--------------------------------------------//

  std::vector<int>    n_3o4Pred_perLayer, n_4o4Pred_perLayer;
  std::vector<double> efficiency, error;
  n_3o4Pred_perLayer.resize(0);
  n_4o4Pred_perLayer.resize(0);
  efficiency.resize(0);
  error.resize(0);

  for (int layer=min_layer; layer<max_layer+1; layer++) {
    int n4o4Pred = h4o4SegPred_allLayers[layer]->Integral();
    int n3o4Pred = h3o4SegPred_missingLayer[layer]->Integral();
    n_4o4Pred_perLayer.push_back(n4o4Pred);
    n_3o4Pred_perLayer.push_back(n3o4Pred);
  }

  //CalculateEffi(n_totalHits_perLayer, n_totalPred_perLayer, efficiency, error);
  CalculateEffiNew(n_3o4Pred_perLayer, n_4o4Pred_perLayer, efficiency, error);

  for (int layer=0; layer<efficiency.size(); layer++) {
    hLayerEff->SetBinContent(layer+1, efficiency.at(layer));
    hLayerEff->SetBinError(layer+1, error.at(layer));
  }
    
  //----------------------------//
  //  output to file
  //----------------------------//

  f->cd();
  tree->Write();

  hSeg->Write();
  double total_seg = hSeg->Integral();
  double single_seg = hSeg->GetBinContent(2);
  std::cout<<"Fraction of single segment event: "<< single_seg / total_seg <<std::endl;

  hDist->Write();
  hChi2->Write();
  hChi2divDOF->Write();
  hAng->Write();
  hAng_Chi2->Write();
  hChi2_nSeg->Write();
  hInvCDFvschi2->Write();
  hCDFvschi2->Write();
  hInvCDF->Write();
  hCDF->Write();
  hLayerEff->Write();
  
  hAli5->Fit("gaus", "Q", "", -0.7, 0.7);
  hAli5->Write();
  hAli6->Fit("gaus", "Q", "", -0.8, 0.6);
  hAli6->Write();
  // hResCog->Fit("gaus", "Q", "", -1, 1);                                                                                                                                                  
  hResCog->Write();
  // hResCog3->Fit("gaus", "Q", "", -1, 1);                                                                                                                        
  hResCog3->Write();
  hResPara->Write();
  for (int layer=min_layer; layer<max_layer+1; layer++) {
    hOcc[layer]->Write();
    hTime[layer]->Write();
    hNumClu[layer]->Write();
    hNumClu_BeforeRej[layer]->Write();
  // for (int layer=4; layer<8; layer++) hn[layer]->Write();
  // for (int layer=4; layer<8; layer++) hN[layer]->Write();
    h4o4SegPred_allLayers[layer]->Write();
    h3o4SegPred_missingLayer[layer]->Write();
    hClusCharge[layer]->Write();
    hEffi[layer]->Write();
    hClusCharge_Seg[layer]->Write();
    hClusCharge_Seg_4of4[layer]->Write();
    hClusCentroid_Seg[layer]->Write();
    hClusCentroid_Seg_4of4[layer]->Write();
    hMaxPdoStripCharge_Seg[layer]->Write();
    hMaxPdoStripCharge_Seg_4of4[layer]->Write();
    hNeighFlag0_Clus[layer]->Write();
    hNeighFlag1_Clus[layer]->Write();
    hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Write();
    hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[layer]->Write();
    
    hClusCharge_1stCalib[layer]->Write();
    hClusCharge_Seg_1stCalib[layer]->Write();
    hClusCharge_Seg_4of4_1stCalib[layer]->Write();
    hMaxPdoStripCharge_Seg_1stCalib[layer]->Write();
    hMaxPdoStripCharge_Seg_4of4_1stCalib[layer]->Write();
    hNeighFlag0_Clus_1stCalib[layer]->Write();
    hNeighFlag1_Clus_1stCalib[layer]->Write();
  }

  for (int i=0; i<8; i++) hResIn[i]->Write();
  for (int i=0; i<8; i++) hResEx[i]->Write();

  //===========================================================================================//
  //===========================================================================================//

  hNum_HTcells->Write();
  hSumAmp_HTcells->Write();

  hClusterWidth->Write();
  hMaxPdo->Write();
  hPeakPdo->Write();
  
  hNumClu_Seg->Write();
  double seg3o4 = hNumClu_Seg->GetBinContent(4);
  double seg4o4 = hNumClu_Seg->GetBinContent(5);
  std::cout<<"Fraction of 3o4 segment event: "<< seg3o4 / total_seg <<std::endl;
  std::cout<<"Fraction of 4o4 segment event: "<< seg4o4 / total_seg <<std::endl;

  std::cout<<"Avg per layer eff: "<< 4*seg4o4 / (seg4o4*4+seg3o4) <<std::endl;

  hClusterWidth_Seg->Write();
  hMaxPdo_Seg->Write();
  hPeakPdo_Seg->Write();

  hNumPot->Write();
  hNumMask->Write();
  hNumBkg->Write();
  
  hNumPot_Seg->Write();
  hNumMask_Seg->Write();
  hNumBkg_Seg->Write();
  
  h_fracNumPot_Seg->Write();
  h_fracNumMask_Seg->Write();
  h_fracNumBkg_Seg->Write();
  h_fracNumPot_vs_Ang->Write();
  h_fracNumMask_vs_Ang->Write();
  h_fracNumBkg_vs_Ang->Write();
  h_fracNumPot_vs_Chi2->Write();
  h_fracNumMask_vs_Chi2->Write();
  h_fracNumBkg_vs_Chi2->Write();

  h_numEvwNoHitsLayers_Vs_numLayerswNoHits->Write();
  h_numEvwNoClusLayers_Vs_numLayerswNoClus->Write();
  
  h_ClusWidth_Vs_bkgChns->Write();
  h_Relbcid_Vs_DistMaxPdo->Write();
  h_Relbcid_Vs_DistMaxPdo_1seg3layer->Write();

  //for(int iev=0; iev<c_segments.size(); iev++){
  //  std::string event_dir = "Event"+std::to_string(iev)+"/";
  //  f->mkdir(event_dir.c_str());
  //  f->cd(event_dir.c_str());
  //  for(int iseg=0; iseg<c_segments[iev].size(); iseg++){
  //    c_segments[iev][iseg]->Write();
  //    delete c_segments[iev][iseg];
  //  }
  //}
  
  /*for(int iev=0; iev<c_evtDisplay.size(); iev++){
    std::string event_dir = "Event"+std::to_string(iev)+"/";
    f->mkdir(event_dir.c_str());
    f->cd(event_dir.c_str());
    c_evtDisplay[iev]->Write();
    delete c_evtDisplay[iev];
    }*/

  f->Close();
  //delete seg;
  //delete sg;
  
  f_relbcid_plots->cd();
  for (int layer=4; layer<8; layer++) {
    hRelBcid01_Strips[layer]->Write();
    hRelBcid01_Wires[layer]->Write();
    hRelBcid01_Pads[layer]->Write();
    hRelBcid_Vs_VMM_SFEB[layer]->Write();
    hRelBcid_Vs_VMM_PFEB[layer]->Write();
  }
  f_relbcid_plots->Close();

  delete [] buffer;  
  printf("number of events   = %d\n", eventNumber);
  //printf("number of segments = %d, %5.3f segments/event.", totSeg, totSeg/(double)eventNumber);
  
  //printf("number of rejected events = %d\n", NrejEvnts);
  printf("number of events with no HT cells filled = %d\n", NrejEvnts_noFilledHT);
  printf("number of events with no HT cells with at least 3 layers with at least one valid hit = %d\n", NrejEvnts_noHit1perlayer);
  printf("number of events with no small angle HT cells = %d\n", NrejEvnts_noSmallAngle);
  //printf("number of events with no HT cell with 3 layers having clusters = %d\n", NrejEvnts_noClusterHT);
  //printf("number of events with no HT cell with 3 layers having good clusters = %d\n", NrejEvnts_noGoodClusterHT);

  //printf("another cutflow: \n");
  printf("number of events having (number of layers with no hits >=2): = %d\n",NrejEvnts_noHitsLayers_ge2);
  printf("number of events having (number of layers with hits >=3): = %d\n",NselEvnts_HitsLayers_ge3);
  
  printf("Only for events with (number of layers with hits >=3): \n");
  printf("number of events having (number of layers with no good clusters >=2): = %d\n",NrejEvnts_noClusLayers_ge2);
  printf("number of events having (number of layers with good clusters >=3): = %d\n",NselEvnts_ClusLayers_ge3);
    
  printf("Only for events with (number of layers with good clusters >=3): \n");
  printf("number of events having no good segments: = %d\n", NrejEvnts_noSeg);
  printf("number of events having at least one good segment: = %d\n", NselEvnts_Seg);

  printf ("Encountered %i exceptions\n", errorCount);

}

