// test channelID

#include<stdio.h>
#include "NSWRead/NSWChannelID.h"

int main(){
  NSWChannelID id;
  id.SetDataType(0);      //data type:  0:L1A, 1: config, 2: monitor 
  id.SetTechnology(1);    // (MM = 1, sTGC = 0) 
  id.SetEndPoint(1);      // 0: pad, 1: strip, 2: Trig. Proc, 3: Pad 
  id.SetEta(0);           // 0: eta=+1: endcap A, 1: eta=-1, endcap C
  id.SetRawSector (0);    // sector A01
  id.SetLayer(3);         // layer 4
  id.SetRadius(8);        // pcb4
  id.SetChannelGroup (0); //  ChannelGroup 0 - 7
  id.SetVMMChannel (23);
  id.SetVMM(5);
  printf ("id = ");
  id.Print();
  //id.SetChannelID(0x08D1DB);
  //id.Print();
  printf ("\n");
  char label[40];
  id.GetString(label);
  NSWChannelID id2;
  id2.ParseString(label);
  id2.Print();
  printf ("\n");

  int link[16]={28,80,156,220,284,348,400,476,528,604,668,732,2076,2140,2204,2256};
  id.SetRawSector (12-1);   
  id.SetChannelGroup (0); 
  id.SetVMMChannel (0);
  id.SetVMM(0);
  for (int lay=0; lay<8; lay++){
    for (int r=6; r<=7; r++){
      id.SetRadius(r);
      id.SetLayer(lay);
      id.GetBoardID (label);
      printf ("%4d 0x%3X %s ", link[lay*2+r-6], link[lay*2+r-6], label);
      id.GetString(label);
      printf ("%s\n", label);
    }
  }
  
  /*  
  NSWPacket* p = new NSWPacket(upperID.GetChannelID(), 3);
  NSWChannel* ch[3];
  for (int i=0; i<3; i++){
    ch[i] = new NSWChannel();
    ch[i]->SetChannelID(0, 1, 3, 1, 1, 3, 5, 0, 7, 33+i);
    ch[i]->SetPDO(200+10*i);
    ch[i]->SetTDO(50+10*i);
    ch[i]->SetRelBCID(i);
    p->SetChannel(i, ch[i]);
  }
  p->SetLength(3);
  p->SetBCID (1234, 0);
  p->SetL1ID (0xabcd);
  p->SetHasTDO(true);
  p->SetChecksum(41);
  p->Print();
  p->Test(1);
  printf ("packet size %li, %i bytes\n", sizeof(p), p->GetNumBytes());

  unsigned char buf[512];

  unsigned int * u32 = (unsigned int*)buf;
  *u32=0x01020304;  // stores 04 first in buf[0].
  for (int i=3; i>=0; i--) printf ("%d\t", buf[i]);
  //for (int i=3; i>=0; i--) printf ("%02x\t", buf[i]);
  printf (" = 0x%08x  -- little endian!\n", *u32);
  

  
  int n = p->Store(buf);
  printf ("wrote %d bytes\n", n);
  for (int j=0; j<(n+3)/4; j++) { // loop over words
    for (int i=3; i>=0; i--) printf ("%d\t", buf[i+4*j]);
    for (int i=3; i>=0; i--) printf ("%02X\t", buf[i+4*j]);
    printf ("\n");
  }
  printf ("\n");

  NSWPacket* pp = new NSWPacket(upperID.GetChannelID(), 3);
  pp->Read(buf);
  pp->Print();
  pp->Test();

  delete(p);
  delete(pp);*/
  return 0;
}
