/** @file nswNick.cpp
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <math.h>
#include "TH2.h"
#include "TGraph.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "TImage.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWSegmentList.h"
#include "NSWRead/NSWCalibration.h"

/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32

// Kronecker Delta Function
int Kron_Delta(int i, int j) {
        if ( i == j ) {
                return 1; }
        else  {
                return 0; }
}


/** read  data file, produce root file for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer
  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  FILE *infile;

  std::vector<double> layer1_yresid;
  std::vector<double> layer2_yresid;
  std::vector<double> layer3_yresid;
  std::vector<double> layer4_yresid;
  std::vector<double> layer1_x; // Using x for Benoit's thesis to maintain consistent formatting but NSWRead calls it "z"
  std::vector<double> layer2_x;
  std::vector<double> layer3_x;
  std::vector<double> layer4_x;
  double S_resid = 0;
  double S_zy_L1 = 0;
  double S_y_L1 = 0;
  double S_z_L1 = 0;
  double S_zz_L1 = 0;
  double S_zy_L2 = 0;
  double S_y_L2 = 0;
  double S_z_L2 = 0;
  double S_zz_L2 = 0;
  double S_zy_L3 = 0;
  double S_y_L3 = 0;
  double S_z_L3 = 0;
  double S_zz_L3 = 0;
  double S_zy_L4 = 0;
  double S_y_L4 = 0;
  double S_z_L4 = 0;
  double S_zz_L4 = 0;
  double m_resid[4] = { };
  double b_resid[4] = { };
  float denom_L1, denom_L2, denom_L3, denom_L4;

  if(argc <3){
    printf("Usage: nswNick pdoCalib.txt file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWCalibration cal;
  if (cal.ReadPDO(argv[1]) != 1){
    printf ("Cannot read calibration file.\n");
    return 1;
  }
  //cal.Print();

  NSWEvent event;
  NSWSector* sec;
  NSWClusterList clusterList;
  
  strcpy(fils,argv[2]);
  fils[(strlen(argv[2])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".nick.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }
	  
  int totSeg = 0;
  int numSeg = 0; // per event


  // 01/31/2022 Creating manual loop to look through events 3 times.
  int eventLoop = 0;
  while (eventLoop < 3) {
  ////// start your loop here if you want it to process all events in each pass 
 
  	if (eventLoop ==  1) {
        // Here we have already ran through files once and calculated residuals across all 4 layers
        	denom_L1 = S_resid*S_zz_L1 - pow(S_z_L1,2);
                denom_L2 = S_resid*S_zz_L1 - pow(S_z_L1,2);
                denom_L3 = S_resid*S_zz_L1 - pow(S_z_L1,2);
                denom_L4 = S_resid*S_zz_L1 - pow(S_z_L1,2);
                m_resid[0] = (S_resid*S_zy_L1 - S_z_L1*S_y_L1)/denom_L1;
                b_resid[0] = (S_zz_L1*S_y_L1 - S_z_L1*S_zy_L1)/denom_L1;

                m_resid[1] = (S_resid*S_zy_L2 - S_z_L2*S_y_L2)/denom_L2;
                b_resid[1] = (S_zz_L2*S_y_L2 - S_z_L2*S_zy_L2)/denom_L2;

                m_resid[2] = (S_resid*S_zy_L3 - S_z_L3*S_y_L3)/denom_L3;
                b_resid[2] = (S_zz_L3*S_y_L3 - S_z_L3*S_zy_L3)/denom_L3;

                m_resid[3] = (S_resid*S_zy_L4 - S_z_L4*S_y_L4)/denom_L4;
                b_resid[3] = (S_zz_L4*S_y_L4 - S_z_L4*S_zy_L4)/denom_L4;

        }

  for (int iFile=2; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data



	///////////////////// START NICKS CODE ///////////////////////
	std::cout << "Starting Nicks Code" << std::endl;
	// Initializing these for every event
	double sigma_intrinsic = 1; //  Intrinsic sTGC Spatial Resolution PLACEHOLDER VALUE FOR NOW
	// Sums over all spatial-point coordinates for a multiplet
	double S = 0;
	double S_zy = 0;
	double S_y = 0;
	double S_z = 0;
	double S_zz = 0;
	double z_i = 0;
	double y_i = 0;
	double m = 0; // Angular Resolution Track Fit slope
	double b = 0; // Angular Resolution Track Fit Intercept

	// Need one of these linear fit parameters per layer I have for track-residual fit

	int Space_Points = 0;
	int num_segments = 0;

	//fill hitlist with cluster data
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  cal.Calibrate(sec);
	  clusterList.Clear();
	  clusterList.Fill(sec, 20, 1);

	  NSWSegmentList segmentList;
	  segmentList.Clear();
	  segmentList.Fill4 (&clusterList, 4, 10); //start on layer 4

	  if (segmentList.GetNumSegments() == 1){
	    NSWSegment* seg = segmentList.GetSegment(0);
	    if (seg->GetNumClusters() == 4) {
	      ///// only events with one segment with all 4 layers

		std::cout << "Starting Nicks Code" << std::endl;
        	// Initializing these for every event
        	double sigma_intrinsic = 1; //  Intrinsic sTGC Spatial Resolution PLACEHOLDER VALUE FOR NOW
        	// Sums over all spatial-point coordinates for a multiplet
        	double S = 0;
        	double S_zy = 0;
       		double S_y = 0;
        	double S_z = 0;
        	double S_zz = 0;
        	double z_i = 0;
        	double y_i = 0;
        	double m = 0; // Angular Resolution Track Fit slope
        	double b = 0; // Angular Resolution Track Fit Intercept

        	// Need one of these linear fit parameters per layer I have for track-residual fit

        	int Space_Points = 0;
        	int num_segments = 0;

		// After passing our two preselections, now we loop through all 4 layers
		for (int layer=4; layer<8; layer++){
                //std::cout << "Looping through the layer of each segment" << std::endl;
                //branch.pred[layer] = s->GetPredicted (layer);
                NSWCluster* c = seg->GetCluster(layer);
                //std::cout << "Grabbed a cluster " << std::endl;
                //std::cout << "There are: " << clusterList.GetNumClusters(layer) << " Clusters in this layer " << std::endl;
                //std::cout << "Added to the total number of clusters " << std::endl;
                z_i = c->GetZPosition();
                //std::cout << "Got the z position of the cluster" << std::endl;
                y_i = c->GetCOG();
                //std::cout << "Got the COG of the cluster" << std::endl;
                S += 1./pow(sigma_intrinsic,2);
                S_zy += z_i*y_i/pow(sigma_intrinsic,2);
                S_y += y_i/pow(sigma_intrinsic,2);
                S_z += z_i/pow(sigma_intrinsic,2);
                S_zz += pow(z_i,2)/pow(sigma_intrinsic,2);
                //std::cout << "Exiting Layer loop, with layer = " << layer << std::endl;
        }

	float denominator = S*S_zz - pow(S_z,2);
	m = (S*S_zy - S_z*S_y)/denominator;
	b = (S_zz*S_y - S_z*S_zy)/denominator;

	Space_Points = 4; // Update this once I figure out how many segments I should loop through

	if (eventLoop == 0) { // We just want to calculate these values once, in particular the first time
        	              // So that we can already have our list of residuals and then fit a line to the layer track-residuals
                	      // To extract our matrix of m_resid and b_resid values
		for (int layer=4; layer<8; layer++){
        		NSWCluster *first_c = seg->GetCluster(layer); //Gets the cluster for a given segment
                	z_i = first_c->GetZPosition();
                	y_i = first_c->GetCOG();
                	S_resid += 0.25/pow(sigma_intrinsic,2); // Weighting by 1/4 because we are repeating this operation for each layer
                	if (layer==4) {
                        	layer1_yresid.push_back(y_i - m*z_i - b);
                        	layer1_x.push_back(z_i);
                        	S_zy_L1 += z_i*y_i/pow(sigma_intrinsic,2);
                        	S_y_L1 += y_i/pow(sigma_intrinsic,2);
                        	S_z_L1 += z_i/pow(sigma_intrinsic,2);
                        	S_zz_L1 += pow(z_i,2)/pow(sigma_intrinsic,2);
                	}	
                	if (layer==5) {
                        	layer2_yresid.push_back(y_i - m*z_i - b);
                        	layer2_x.push_back(z_i);
                        	S_zy_L2 += z_i*y_i/pow(sigma_intrinsic,2);
                        	S_y_L2 += y_i/pow(sigma_intrinsic,2);
                        	S_z_L2 += z_i/pow(sigma_intrinsic,2);
                        	S_zz_L2 += pow(z_i,2)/pow(sigma_intrinsic,2);
                	}	
                	if (layer==6) {
                        	layer3_yresid.push_back(y_i - m*z_i - b);
                        	layer3_x.push_back(z_i);
                        	S_zy_L3 += z_i*y_i/pow(sigma_intrinsic,2);
                        	S_y_L3 += y_i/pow(sigma_intrinsic,2);
                        	S_z_L3 += z_i/pow(sigma_intrinsic,2);
                        	S_zz_L3 += pow(z_i,2)/pow(sigma_intrinsic,2);
                	}	
                	if (layer==7) {
                        	layer4_yresid.push_back(y_i - m*z_i - b);
                        	layer4_x.push_back(z_i);
                        	S_zy_L4 += z_i*y_i/pow(sigma_intrinsic,2);
                        	S_y_L4 += y_i/pow(sigma_intrinsic,2);
                        	S_z_L4 += z_i/pow(sigma_intrinsic,2);
                        	S_zz_L4 += pow(z_i,2)/pow(sigma_intrinsic,2);
                	}	
        	} // End of residual calculation loop
		//std::cout << "Leaving residual calculation loop" << std::endl;
	} // End of eventLoop=0 selection


	int num_layers = 4;
	double M[num_layers][num_layers];

	// Initializing Matrix values to zero
	for (int i = 0; i<num_layers;i++) {
	        for (int j = 0; j<num_layers;j++) {
        	        M[i][j] = 0;
        	}
	}

	//std::cout << "Entering for loop to calculate matrix elements method" << std::endl;
	for (int layer_i = 4; layer_i<num_layers+4; layer_i++) { // Looping through all 4 layers of a track

	        NSWCluster *c_i = seg->GetCluster(layer_i);
	        float z_i = c_i->GetZPosition();
	        float y_i = c_i->GetCOG();
 	        for (int layer_j = 4; layer_j < num_layers+4; layer_j++) {
         		NSWCluster *c_j = seg->GetCluster(layer_j);
                	float z_j = c_j->GetZPosition();
                	float y_j = c_j->GetCOG();
                	M[layer_i-4][layer_j-4] = Kron_Delta(layer_i,layer_j) + (z_i*(S_z - Space_Points*z_j) + z_j*S_z - S_zz)/denominator;
        	}
	}

	//Print out M
	//std::cout << "Printing out elements of M" << std::endl;
	for (int i = 0; i < 4; i ++) {
        	for (int j = 0; j <4;j++) {
        	//std::cout << M[i][j] << " ";
        	}
        	//std::cout << std::endl;
	}


	// Now we Calculate the parameters phi and b for each given layer.
	// Now we add a selection to double check that we have already calculated the residuals track-fit parameters m and b
	if (eventLoop > 0) {
        	double M_reduced[4] = {0};
        	double b_reduced[4] = {0};
        	double m_reduced[4] = {0};
        	double phi_star[4] = {0};
        	double b_star[4] = {0};
	
        	for (int i = 0; i < 4; i++) {
                	M_reduced[i] = M[i][3]; //3rd Column is just a placeholder, could be any column I think
                	b_reduced[i] = b_resid[i];
                	m_reduced[i] = m_resid[i];
                	for (int j = 0; j < 4; j++) {
                        	if (i != j) {
                        	M_reduced[i] = M_reduced[i] - M[j][3];
                        	b_reduced[i] = b_reduced[i] - b_resid[j];
                        	m_reduced[i] = m_reduced[i] - m_resid[j];
                        	}
                	}
        	}
        	// Now we calculate the parametrized layer-displacements
        	for (int i = 0; i < sizeof(phi_star)/sizeof(phi_star[0]); i++){
                	phi_star[i] = (1.0/M_reduced[i]) * m_reduced[i];
                	b_star[i] = (1.0/M_reduced[i]) * b_reduced[i];
        	}
		/*
		std::cout<<"reduced thingies" << std::endl;
		std::cout<< M_reduced[0] << std::endl;
		std::cout<< M_reduced[1] << std::endl;
		std::cout<< M_reduced[2] << std::endl;
		std::cout<< M_reduced[3] << std::endl;
		*/
        	//std::cout << "EventLoop is: " << eventLoop <<std::endl;
        	} // End EventLoop > 0 loop
	std::cout<< "Nicks code is done" << std::endl;
  	////// start your loop here if you want it to process this segment in each pass 
  	////// start your loop here if you want it to process this segment in each pass 

	    } // if 4 clusters
	  } // if one segment per event
	} // next sector
       
	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	eventNumber++;
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0); // next event
    fclose(infile);
  } // next file
  ////// end your loop here if you want it to process all events in each pass 
  std::cout << "eventLoop is added: " << eventLoop << std::endl;
  eventLoop++;
  } // End of loop over all events in each pass
  
  //tree->Write();
  //f->Close();
  delete [] buffer;  
  printf("number of events   = %d\n", eventNumber);
  printf("number of segments = %d, %5.3f segments/event.", totSeg, totSeg/(double)eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}




