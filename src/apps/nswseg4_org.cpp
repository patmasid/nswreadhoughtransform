/** @file nswseg4.cpp
This application fills a root tree with segment reconstruction results.
It can be used for monitoring, or for looking at hit reconstruction
results. The tree variables are stored in moni_t.

This version uses only 4 layers, and can run on GIF data from sTGC.

Reads nsw data file and produces monitoring histograms based on segments.
The tree is filled after cluster finding and there is one entry per segment.

@see moni.cpp for monitoring packet data
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include "TH2.h"
//#include" TGraph.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWSegmentList.h"
#include "NSWRead/NSWCalibration.h"

#include <iostream>
#include <sstream>
#include <fstream>

/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32

 //void NSWSegment::CalculateEffi(int, int, double&, double&);

/** Draw a simple event display */
void DrawDisplay (NSWSector* sec, int gifNumber){
  TH1F *hamp[8], *htime[8];
  TH1F *hwire[3][8];
  TH1F *hpad[3][8];
  char name[40], title[80];
  char gifName[80];
  int l1id;
  int imax;
  if (sec->GetTechnology()==1){ // 1 = MM, 0=sTGC */
    imax = 8196;
  } else {
    imax = 400; //1200;
  }
  for (int layer=0; layer<8; layer++){
    sprintf (name, "hamp%d", layer);
    sprintf (title, "PDO layer %d;channel", layer);
    hamp[layer] = new TH1F(name, title, imax, 0, imax);  
    sprintf (name, "htime%d", layer);
    sprintf (title, "Time layer %d;channel", layer);
    htime[layer] = new TH1F(name, title, imax, 0, imax);
  }
  for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(i);
    l1id = pac->GetL1ID();
    for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
      NSWChannel* chn = pac->GetChannel(j);
      if (chn->GetChannelType() == NSWChannel::STRIP){
	int strip = chn->GetDetectorStrip();
	int layer = chn->GetLayer();
	hamp[layer]->Fill(strip, chn->GetPDO());
	htime[layer]->Fill(strip, chn->GetPeakingTime());
      }
    }
  }
  TCanvas* can = new TCanvas ("can", "Event display", 1600, 1200);  
  can->Divide(1, 8);
  //gStyle->SetOptStat(0); 
  for (int layer=0; layer<8; layer++){
    can->cd(layer+1);
    hamp[layer]->GetXaxis()->SetLabelSize(0.1);
    hamp[layer]->GetYaxis()->SetLabelSize(0.1);
    hamp[layer]->GetYaxis()->SetRangeUser(0, 1000);
    hamp[layer]->SetFillColor(2);
    hamp[layer]->Draw("HIST");
    htime[layer]->SetMarkerColor(2);
    htime[layer]->SetMarkerStyle(7);
    htime[layer]->Draw("psame");
  }
  can->cd(0);
  TPaveText t(0.4,0.94,0.6,0.99,"NDC");
  char line[40];
  sprintf (line, "Strips L1ID = %d", l1id);
  t.SetTextSizePixels(8);
  t.AddText(line);
  t.SetFillColor(0);
  t.Draw();

  sprintf (gifName, "ed%04ds.gif", gifNumber);
  can->SaveAs(gifName);

  // wires:
  if (sec->GetTechnology()==0){ //  1 = MM, 0=sTGC
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++){
	sprintf (name, "hwire%d%d", q, layer);
	sprintf (title, "Wire PDO Q%d layer %d;wire", q, layer);
	int range = 20+10*q;
	hwire[q][layer] = new TH1F(name, title, range, 0, range);  
      }
    }
    for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
      NSWPacket *pac =sec->GetPacket(i);
      l1id = pac->GetL1ID();
      for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
	NSWChannel* chn = pac->GetChannel(j);
	if (chn->GetChannelType() == NSWChannel::WIRE){
	  int wire = chn->GetDetectorStrip();
	  int layer = chn->GetLayer();
	  int q = chn->GetRadius();
	  hwire[q][layer]->Fill(wire, chn->GetPDO());
	}
      }
    }
    can->Clear();
    can->Divide(3, 8);
      for (int layer=0; layer<8; layer++){
	for (int q=0; q<3; q++){
	  can->cd(q+3*layer+1);
	  hwire[q][layer]->GetXaxis()->SetLabelSize(0.1);
	  hwire[q][layer]->GetYaxis()->SetLabelSize(0.1);
	  hwire[q][layer]->GetYaxis()->SetRangeUser(0, 1000);
	  hwire[q][layer]->SetFillColor(2);
	  hwire[q][layer]->Draw("HIST");
	}
      }
    can->cd(0);
    TPaveText t(0.4,0.94,0.6,0.99,"NDC");
    char line[40];
    sprintf (line, "Wires L1ID = %d", l1id);
    t.AddText(line);
    t.SetFillColor(0);
    t.Draw();
    sprintf (gifName, "ed%04dw.gif", gifNumber);
    can->SaveAs(gifName);

    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++) delete hwire[q][layer];
    }
    
    // pads:
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++){
	sprintf (name, "hpad%d%d", q, layer);
	sprintf (title, "Pads PDO Q%d layer %d;pad", q, layer);
	int range = 120;
	hpad[q][layer] = new TH1F(name, title, range, 0, range);  
      }
    }
    for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
      NSWPacket *pac =sec->GetPacket(i);
      l1id = pac->GetL1ID();
      for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
	NSWChannel* chn = pac->GetChannel(j);
	if (chn->GetChannelType() == NSWChannel::PAD){
	  int pad = chn->GetDetectorStrip();
	  int layer = chn->GetLayer();
	  int q = chn->GetRadius();
	  hpad[q][layer]->Fill(pad, chn->GetPDO());
	}
      }
    }
    can->Clear();
    can->Divide(3, 8);
      for (int layer=0; layer<8; layer++){
	for (int q=0; q<3; q++){
	  can->cd(q+3*layer+1);
	  hpad[q][layer]->GetXaxis()->SetLabelSize(0.1);
	  hpad[q][layer]->GetYaxis()->SetLabelSize(0.1);
	  hpad[q][layer]->GetYaxis()->SetRangeUser(0, 1000);
	  hpad[q][layer]->SetFillColor(2);
	  hpad[q][layer]->Draw("HIST");
	}
      }
    can->cd(0);
    TPaveText t2(0.4,0.94,0.6,0.99,"NDC");
    //char line[40];
    sprintf (line, "Pads L1ID = %d", l1id);
    t2.AddText(line);
    t2.SetFillColor(0);
    t2.Draw();
    sprintf (gifName, "ed%04dp.gif", gifNumber);
    can->SaveAs(gifName);
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++) delete hpad[q][layer];
    }
  } //if sTGC
    
  delete can;
  for (int layer=0; layer<8; layer++){
    delete  htime[layer];
    delete  hamp[layer];
  }
}

/** calculates the efficiency and the error on the efficiency. 
  @param numer the numerator of the ratio
  @param denom the denominator of the ratio
  @param effic the calculated efficiency
  @param error the calculated error */
void CalculateEffi(int numer, int denom, double &effi, double &error) {
  effi = -1;
  error = 0;

  // Check for divide by zero:
  if (denom <= 0) return;

  effi = numer / (double)denom;

  // Error calculation from arXiv:physics/0701199v1
  double dnum = (double)numer;
  double dden = (double)denom;

  double disc = ((dnum+1)*(dnum+2))/((dden+2)*(dden+3))
    - pow(dnum+1,2)/pow(dden+2,2);
  if (disc >=0) error = sqrt(disc);

  /**********************
  // Error calculation from P. Avery.
  double disc = (1.0/denom * (effic + 1/denom)* (1.0-effic + 1/denom)
		 / (pow(1+2.0/denom,2.0)*(1.0+3.0/denom)));
  if (disc >=0) {
    error = sqrt(disc);

    // For effic = 0 or 1, add to the error the value of the unbiased
    // estimate of Y1/Y2. This will give a better estimate of the conf.
    // interval.
    if (effic==1.0 || effic==0.0) {
      error = (1.0/denom) / (1.0+2.0/denom) + error;
    }
  }
  ************************/
}



/** read  data file, produce root file for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer
  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  FILE *infile;

  if(argc <4){
    printf("Usage: nswseg4 pdoCalib.txt bad_channels.txt file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWCalibration cal;
  if (cal.ReadPDO(argv[1]) != 1){
    printf ("Cannot read calibration file.\n");
    return 1;
  }
  //cal.Print();

  std::vector< std::vector <std::vector<int> >> v_bad_channels_strip;
  v_bad_channels_strip.resize(3);
  for(int iQ=0; iQ<3; iQ++){
    v_bad_channels_strip[iQ].resize(8);
  }

  //Converting the bad channels to vector
  std::string line;
  std::ifstream myfile(argv[2]);
  if ( myfile.is_open() ) {
    while ( std::getline ( myfile,line ) ) {
      std::string data_str =  line;
      std::istringstream iss(line);
      
      int radius = -1;
      int layer = -1;
      std::string det_type = "";
      int det_chnl = -1;
      
      if ( !(iss
	     >> radius 
	     >> layer
	     >> det_type
	     >> det_chnl) ){
	std::cout << "Error in reading bad channels file!" << std::endl;
	return 1;
      }
      
      if(det_type == "strip"){
	v_bad_channels_strip[radius][layer].push_back(det_chnl);
      }

    }
  }

  NSWEvent event;
  NSWSector* sec;
  NSWClusterList clusterList;
  
  clusterList.Set_vecBadChanls(v_bad_channels_strip);
  
  strcpy(fils,argv[3]);
  fils[(strlen(argv[3])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".seg.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }

  /** \struct moni_t
      defines variables in root tree.
      One entry per ROD cluster.
  */
  struct moni_t{
    /** event number, increments over all files. */
    int event;
    /** Level one ID */
    int l1id;
    /** number of precision layer clusters */
    int nprec;
    /** number of clusters */
    int nclu;
    /** number of channels of this cluster */
    int wid[8];
    /** center of gravity*/
    float cog[8];
    /** better cog*/
    float cog2[8];
    /** cluster charge*/
    float charge[8];
    /** z position */
    float z[8];
    /** line fit */
    float inter, slope, chi2, angle;
    /** predicted positon and residual */
    float pred[8], res[8];
    /** median y positon residual */
    float y;
    /** amplitude and time per channel*/
    float amp[8][10], time[8][10];
    /** number of missing strips in the cluster */
    int gap[8];
    /** strip number of the first strip */
    int start[8];
    /** strip number of the peak */
    int peak[8];
    /** number of channels with neighbor flag = 0 */
    int nflags[8];
    /** raw parabola interpolation */
    float raw[8];
    /** corrected parabola interpolation */
    float para[8];
    /** three strip center of gravity */
    float cog3[8];
  };
  moni_t branch;


  TTree *tree = new TTree("tree","Clusters");
  tree->Branch("branch",&branch,"event/I:l1id:nprec:nclu:wid[8]:cog[8]/F:cog2[8]:charge[8]:z[8]:inter:slope:chi2:angle:pred[8]:res[8]:y:amp[8][10]:time[8][10]:gap[8]/I:start[8]:peak[8]:nflags[8]:raw[8]/F:para[8]:cog3[8]");
  TH1F* hSeg = new TH1F ("hSeg", "Segments per event;segments", 30, 0, 30);
  TH1F* hClu = new TH1F ("hClu", "Clusters per event;clusters", 100, 0, 100);
  TH1F* hDist = new TH1F ("hDist", "intercept distance;mm", 200, -400, 400);
  TH1F* hAli5 = new TH1F ("hAli5", "Alignment for layer 5;mm", 1000, -5, 5);
  TH1F* hAli6 = new TH1F ("hAli6", "Alignment for layer 6;mm", 1000, -5, 5);
  TH1F* hResCog = new TH1F ("hResCog", "Full cluster cog residual;Residual in mm", 1000, -3, 3);
  TH1F* hResCog3 = new TH1F ("hResCog3", "Three strip cog residual;Residual in mm", 1000, -3, 3);
  TH1F* hResPara = new TH1F ("hResPara", "Three strip parabola residual;Residual in mm", 1000, -3, 3);
  TH1F* hAngleSeg = new TH1F ("hAngleSeg", "Segment angle", 1000, -90, 90);
  TH1F* hNum[8];

  TH1F * hOcc[8];
  TH2F * hTime[8];
  TH1F * hn[8], *hN[8], *hEffi[8]; // efficiency
  
  char name[80], title[80];
  for (int i=0; i<8; i++) {
    sprintf (name, "hOcc%d", i);
    sprintf (title, "Cluster position of layer %d;Position in mm", i);
    hOcc[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hTime%d", i);
    sprintf (title, "Cluster peaking time of layer %d;Position in mm;time in ns", i);
    hTime[i] = new TH2F (name, title, 1000, 0, 4000, 100, -25, 175);

    sprintf (name, "hn%d", i);
    sprintf (title, "Predicted position of layer %d;Position in mm", i);
    hn[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hN%d", i);
    sprintf (title, "Predicted positon of layer %d;Position in mm", i);
    hN[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hEffi%d", i);
    sprintf (title, "Strip cluster efficiency of layer %d;Position in mm;Efficiency", i);
    hEffi[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hNum%d", i);
    sprintf (title, "Number of clusters in layer %d", i);
    hNum[i] = new TH1F(name, title, 200, 0, 200);
  }

	  
  int totSeg = 0;
  int numSeg = 0; // per event
  int gifNumber = 0;
  
  for (int iFile=3; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data

	//fill hitlist with cluster data
	branch.event = eventNumber;
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  branch.l1id = sec->GetLevel1ID();
	  cal.Calibrate(sec);
	  clusterList.Clear();
	  clusterList.Fill(sec, 20, 1);
	  int nClu = 0;
	  for (int layer=0; layer<8; layer++){
	    nClu += clusterList.GetNumClusters(layer);
	    hNum[layer]->Fill(clusterList.GetNumClusters(layer));
	    for (int i=0; i<clusterList.GetNumClusters(layer); i++){
	      NSWCluster *c = clusterList.GetCluster(layer, i);
	      hOcc[layer]->Fill(c->GetCOG());
	      hTime[layer]->Fill(c->GetCOG(), c->GetPeakingTime());
	    }
	  }
	  hClu->Fill(nClu);
	  if ((gifNumber<20)&&(nClu>2)) {
	    DrawDisplay (sec, gifNumber);
	    gifNumber++;
	  }
	  //if (clusterList.GetNumClusters(4) < 1) continue;
	  //if (clusterList.GetNumClusters(5) < 1) continue;
	  //if (clusterList.GetNumClusters(6) < 1) continue;
	  //if (clusterList.GetNumClusters(7) < 1) continue;
				
	  //if (clusterList.GetNumClusters(4) > 5) continue;
	  //if (clusterList.GetNumClusters(5) > 5) continue;
	  //if (clusterList.GetNumClusters(6) > 5) continue;
	  //if (clusterList.GetNumClusters(7) > 5) continue;

	  //if (nClu > 20) continue;

	  NSWSegmentList segmentList;
	  segmentList.Clear();
	  //printf ("\n\nL1ID = %d\n", event.GetL1ID());
	  segmentList.Fill4 (&clusterList, 4, 10); //start on layer 4
	  for (int i=0; i<segmentList.GetNumSegments(); i++){
	    NSWSegment* s = segmentList.GetSegment(i);
	    branch.y =  s->GetYPosition();
	    for (int layer=0; layer<8; layer++){
	      branch.pred[layer] = s->GetPredicted (layer);
	      NSWCluster* c = s->GetCluster(layer);
	      if (c == NULL){
		branch.cog[layer] = -100;
		branch.cog2[layer] = -100;
		branch.charge[layer] = -100;
		branch.wid[layer] = -1;
		branch.z[layer] = -1;
		branch.res[layer] = -100;
		for (int i=0; i<10; i++){
		  branch.amp[layer][i] = -1;
		  branch.time[layer][i] = -100;
		}
		branch.gap[layer] = -1;
		branch.start[layer] = -1;
		branch.peak[layer] = -1;
		branch.nflags[layer] = -1;
		branch.raw[layer] = -1;
		branch.para[layer] = -1;
		branch.cog3[layer] = -1;
	      } else {
		branch.cog[layer] = c->GetCOG();
		branch.cog2[layer] = c->GetCOG2();
		branch.charge[layer] = c->GetCharge();
		branch.wid[layer] = c->GetNumChannel();
		branch.z[layer] = c->GetZPosition();
		branch.res[layer] = c->GetCOG() - branch.pred[layer];
		int chnMax = c->GetNumChannel();
		if (chnMax > 10) chnMax = 10;
		for (int j=0; j<chnMax; j++){
		  NSWChannel *chn = c->GetChannel(j);
		  if (chn != NULL) {
		    branch.amp[layer][j] = chn->GetAmplitude();
		    branch.time[layer][j] = chn->GetPeakingTime();
		  } else {
		    branch.amp[layer][j] = -1;
		    branch.time[layer][j] = -1;
		  }
		}
		for (int j=chnMax; j<10; j++){
		  branch.amp[layer][j] = -1;
		  branch.time[layer][j] = -1;
		}
		branch.gap[layer] = c->GetNumMissing();
		branch.start[layer] = c->GetStartChannel();
		branch.peak[layer] = c->GetPeakChannel();
		branch.nflags[layer] = c->CountNeighbors();
		branch.raw[layer] = c->GetParabolaRaw();
		branch.para[layer] = c->GetParabola();
		branch.cog3[layer] = c->GetCOG3();
	      }
	    } // next layer
	    
	    branch.inter = s->GetIntercept();
	    branch.slope = s->GetSlope();
	    branch.chi2 = s->GetChi2();
	    branch.angle = s->GetAngle();
	    branch.nprec = s->GetNumPrecision();
	    branch.nclu = s->GetNumCluster();
	    
	    tree->Fill(); 
	    numSeg++;
	    totSeg++;
	    for (int j=i+1; j<segmentList.GetNumSegments(); j++){
	      NSWSegment* s2 = segmentList.GetSegment(j);
	      hDist->Fill(s->GetIntercept() - s2->GetIntercept());
	      //printf ("%d: %f %f\n", j, s->GetIntercept() , s2->GetIntercept());
	    }
	  } // next segment
	  //if (segmentList.GetNumSegments()==8) segmentList.Print();
	  hSeg->Fill(segmentList.GetNumSegments());
	  if (segmentList.GetNumSegments() == 1){
	    NSWSegment* seg = segmentList.GetSegment(0);
	    if (seg->GetNumClusters() == 4) {
	      double cog[4], para[4];
	      for (int j=4; j<8; j++) cog[j-4]=seg->GetCluster(j)->GetCOG();
	      if (seg->GetChi2() < 50){ // fill alignment histograms
		double m = (cog[0]-cog[3])/3;
		hAli5->Fill((7-5)*m+cog[3]-cog[1]);
		hAli6->Fill((7-6)*m+cog[3]-cog[2]);
	      }
	      hResCog->Fill(cog[1]+ali5-0.5*(cog[0]+cog[2]+ali6));
	      bool bad = false;
	      for (int j=4; j<7; j++) {
		NSWCluster* cl = seg->GetCluster(j);
		if (cl->GetCOG3() < -0.99) bad = true; 
		cog[j-4]=(cl->GetPeakChannel()+cl->GetCOG3()*5/3.5)*3.2;
		//cog[j-4]=(cl->GetPeakChannel()+cl->GetCOG3()*1.611)*3.2; // from fit, but bad
		para[j-4]=(cl->GetPeakChannel()+cl->GetParabola())*3.2;
		if ((j % 2) == 1){ // stagger by -0.5*pitch
		  cog[j-4] -= 0.5*seg->GetPitch();
		  para[j-4] -= 0.5*seg->GetPitch();
		}
	      }
	      if (!bad) {
		hResCog3->Fill(cog[1]+ali5-0.5*(cog[0]+cog[2]+ali6));
		hResPara->Fill(para[1]+ali5-0.5*(para[0]+para[2]+ali6));
		hAngleSeg->Fill(seg->GetAngle());
	      }
	    } // if 4 clusters
	    // calculate efficiency:
	    if (((seg->GetChi2()<18) && (seg->GetNumClusters()==4)) ||
		((seg->GetChi2()<9) && (seg->GetNumClusters()==3))) {
	      for (int j=4; j<8; j++) {
		double pred = seg->GetPredicted(j);
		hN[j]->Fill(pred);
		if (seg->GetCluster(j) != NULL) hn[j]->Fill(pred);
	      } // next layer
	    } // if good chi2
	  } // if one segment per event
	} // next sector
       
	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	eventNumber++;
	if (eventNumber >= 30000) break;
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0); // next event
    fclose(infile);
  } // next file

  for (int layer=0; layer<8; layer++) { // efficiency:
    double eff, err;
    for (int bin=0; bin<1000; bin++){
      int N = hN[layer]->GetBinContent(1+bin);
      if (N>10) {
	int n = hn[layer]->GetBinContent(1+bin);
	CalculateEffi(n, N, eff, err);
	hEffi[layer]->SetBinContent(1+bin, eff);
	hEffi[layer]->SetBinError(1+bin, err);
      }
    }
  }
  
  tree->Write();

  hClu->Write();
  hSeg->Write();
  hDist->Write();
  hAli5->Fit("gaus", "Q", "", -0.7, 0.7); 
  hAli5->Write();
  hAli6->Fit("gaus", "Q", "", -0.8, 0.6); 
  hAli6->Write();
  //hResCog->Fit("gaus", "Q", "", -1, 1); 
  hResCog->Write();
  //hResCog3->Fit("gaus", "Q", "", -1, 1); 
  hResCog3->Write();
  hResPara->Write();
  hAngleSeg->Write();
  for (int layer=4; layer<8; layer++) hOcc[layer]->Write();
  for (int layer=4; layer<8; layer++) hTime[layer]->Write();
  // for (int layer=4; layer<8; layer++) hNum[layer]->Write();
  // for (int layer=4; layer<8; layer++) hn[layer]->Write();
  // for (int layer=4; layer<8; layer++) hN[layer]->Write();
  for (int layer=4; layer<8; layer++) hEffi[layer]->Write();
  
  f->Close();
  delete [] buffer;  
  printf("number of events   = %d\n", eventNumber);
  printf("number of segments = %d, %5.3f segments/event.", totSeg, totSeg/(double)eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}

