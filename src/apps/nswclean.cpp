/** @file nswclean.cpp
This application removes events with missingVMM errors and those with 
exceptions. It can be used to combine several runs into one file.
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"NSWRead/NSWEvent.h"
#include <algorithm>


/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32



int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer

  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;
  int duplCount = 0;
  int exceptionCount = 0;
  int outCount = 0;
  FILE *infile, *outfile;

  if (argc < 4){
    printf("Copies events without missingVMM errors and exceptions.\n");
    printf("Usage: nswclean output.data, number of events to compare with, input1.data [input2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWEvent event;

  outfile = fopen(argv[1],"wb");
  if (outfile==NULL) {
    printf("Can't open output file '%s'.\n", argv[1]);
    exit(-1);	
  }

  std::istringstream iss_Nevt(argv[2]);
  int Nevt_comp;
  iss_Nevt >> Nevt_comp;
  std::cout << "Nevt_comp: " << Nevt_comp << std::endl;
    
  for (int iFile=3; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }

    std::vector<std::vector < std::vector <int> >> dupl_evt;

    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	 if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data
	int miss = 0;
	std::vector<std::vector<int> > curr_evt;
	//std::cout << "Event Number: " << eventNumber << std::endl;
	for (int i=0; i<event.GetNumSector(); i++){ // loop over sectors
	  NSWSector* sec = event.GetSector(i);
	  
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac =sec->GetPacket(j);

	    if (pac->GetMissing(true) > 0) miss++; // count missingVMM
	    
	    for (int k=0; k<pac->GetWidth(); k++){
	      std::vector<int> curr_chn_evt;
              NSWChannel* chn = pac->GetChannel(k);
	      curr_chn_evt.push_back(chn->GetChannelType());
	      curr_chn_evt.push_back(chn->GetRadius());
	      curr_chn_evt.push_back(chn->GetLayer());
	      curr_chn_evt.push_back(chn->GetVMM());
	      curr_chn_evt.push_back(chn->GetVMMChannel());
	      curr_chn_evt.push_back(chn->GetPDO());
	      curr_chn_evt.push_back(chn->GetTDO());
	      curr_evt.push_back(curr_chn_evt);
	    }

	  } // next packet
	} // next sector
	
	//dupl_evt.push_back(curr_evt);
	
	if(std::find(dupl_evt.begin(), dupl_evt.end(), curr_evt) == dupl_evt.end() ){
	  dupl_evt.push_back(curr_evt);

	  if(miss == 0){
	    fwrite (buffer, 4, size, outfile); // write event to file
	    outCount++;
	  }
	  else
	    errorCount ++;
	}
	else{ // count bad events
	  
	  std::cout << "dupliicate event!!!" << std::endl;
	  duplCount++;
	  errorCount ++;
	}
	
	if(dupl_evt.size()>=Nevt_comp){
	  dupl_evt.erase(dupl_evt.begin());
	}

	eventNumber++;
	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
      } // try     
      catch (NSWReadException& ex){
	printf ("Exception for event %d\n", eventNumber);
	printf("%s\n", ex.what());
	exceptionCount++;
      }
    } while (size>0); // next event
    fclose(infile);
  } // next file

  fclose(outfile);
  delete [] buffer;  

  printf("\nTotal number of events = %d, events with errors = %d, duplicate events = %d, %f percent duplicate.\n", eventNumber, errorCount, duplCount, 100*duplCount/(float)eventNumber);
  printf ("Encountered %i exceptions, wrote %d clean events\n", exceptionCount, outCount);
}

