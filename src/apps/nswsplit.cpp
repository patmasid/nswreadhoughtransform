/** @file nswclean.cpp
This application removes events with missingVMM errors and those with 
exceptions. It can be used to combine several runs into one file.
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"NSWRead/NSWEvent.h"
#include <algorithm>


/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32



int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer

  char fil[300], fils[300], fil_name[300];
  int eventNumber = 0;
  int errorCount = 0;
  int duplCount = 0;
  int exceptionCount = 0;
  int outCount = 0;
  FILE *infile, *outfile[10000];

  if (argc < 3){
    printf("Copies events without missingVMM errors and exceptions.\n");
    printf("Usage: nswclean output.data name without .data, input1.data [input2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWEvent event;
  strcpy(fil_name,argv[1]);
  
  int current_file = 0;
  
  outfile[0] = fopen(strcat(fil_name,"_0.data"),"wb");
  if (outfile[0]==NULL) {
    printf("Can't open output file '%s'.\n", strcat(argv[1],"_0.data"));
    exit(-1);
  }

  /*std::istringstream iss_Nevt(argv[2]);
  int Nevt_comp;
  iss_Nevt >> Nevt_comp;
  std::cout << "Nevt_comp: " << Nevt_comp << std::endl;*/
  
  int iFile = 2;
  //for (int iFile=3; iFile<argc; iFile++){
  printf ("opening file '%s'.\n\n", argv[iFile]);
  infile = fopen(argv[iFile],"rb");//open data file
  
  if (infile==NULL) { //error if file incorrect or DNE
    printf("Can't open input file '%s'.\n",argv[iFile]);
    exit(-1);	
  }
  
  std::vector<std::vector < std::vector <int> >> dupl_evt;
  
  // start processing events:
  int size; // actual size of the ATLAS event
  do{ //event loop
    try{
      size = event.ReadATLAS(infile, buffer, maxSize);
    }
    catch (NSWReadException& ex){
      if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
      break;
    }
    try{ 
      event.ReadNSW(buffer, size);           // read the data
      fwrite (buffer, 4, size, outfile[current_file]); // write event to file
      
      eventNumber++;
      if ( (eventNumber)%1000==0 ){// && eventNumber<100000){
	printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	fclose(outfile[current_file]);
	current_file++;
	std::string file_name = "_"+std::to_string(current_file)+".data";
	strcpy(fil_name,argv[1]);
	outfile[current_file] = fopen(strcat(fil_name,file_name.c_str()),"wb");
	if (outfile[current_file]==NULL) {
	  printf("Can't open output file '%s'.\n", strcat(argv[1],file_name.c_str()));
	  exit(-1);
	}
      }
      //else if(eventNumber>=100000){
      //  break;
      //}
    } // try     
    catch (NSWReadException& ex){
      printf ("Exception for event %d\n", eventNumber);
      printf("%s\n", ex.what());
      exceptionCount++;
    }
  } while (size>0); // next event
  fclose(infile);
  //} // next file
  
  fclose(outfile[current_file]);
  delete [] buffer;  

  printf("\nTotal number of events = %d\n", eventNumber);
  printf ("Encountered %i exceptions, wrote %d clean events\n", exceptionCount, outCount);
}

