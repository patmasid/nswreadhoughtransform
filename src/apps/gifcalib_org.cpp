/** @file gifcalib.cpp
This application reads 4 data files for pulser amplitudes of 250, 500, 750 
and 1000.
PDO measurements are filled into profile histograms and lines are fitted.
Results are stored in a tree and a txt file for the CalServer.
Used for sTGC GIF data.
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "TH2.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TProfile.h"
#include "NSWRead/NSWEvent.h"



/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer
  char fil[300], fils[300];
  int eventNumber, errorCount =0;
  FILE *infile;
  if(argc != 5){
    printf("Usage: nswcalib file250.data file500.data file750.data file1000.data\n");
    exit(0);
  }
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }
  eventNumber = 0;
  NSWEvent event;
  NSWSector* sec;
  
  /** \struct Calib
      defines variables in root tree.
      One entry per channel
  */
  struct Calib{
    /** elink index */
    int ind;
    /** vmm number */
    int vmm;
    /** channel number */
    int chn;
    /** layer */
    int layer;
    /** electrode type */
    int type;
    /** intercept */
    float inter;
    /** slope */
    float slope;
    /** intercept error*/
    float interErr;
    /** slope Error */
    float slopeErr;
    /** chi2 */
    float chi2;
  };
  Calib data;
  
  TProfile * pdo[64][512]; // pdo vs pulse per link and channel
  TH1F * inter[64]; // intercept
  TH1F * slope[64]; // slope

  FILE* outfile = fopen("pdocal.txt", "w");

  char name[80], title[80], label[40];
  NSWChannelID id; 
  id.SetTechnologyOld(0); // MM = 1, sTGC = 0
  //id.SetSector(sectors[0]); // only one sector for now
  id.SetRawSector(13-1); // only one sector for now
  id.SetEtaOld(13>0?0:1); // 0: eta=+1: endcap A, 1: eta=-1, endcap C
  id.SetDataTypeOld(0); // 0: L1A, 1: config, 2: monitor
  id.SetEndPointOld(1); // 0: pad, 1: strip,  2: Trig. Proc, 3: Pad trig
  for (int layer=4; layer<8; layer++){ // GIF 4 layer
    for (unsigned int i=0; i<8; i++){
      int r = (i>3)? (i-2)/2 : 0;   // radius
      int g = (i>3)? 0 : i%2;       // group
      int e = (i>3)? i%2 : (i/2)%2; // endpoint
      id.SetRadius(r);
      id.SetChannelGroup(g);
      id.SetEndPointOld(e);
      id.SetLayer(layer);
      id.GetBoardID(label);
      //printf ("i = %2d, r = %2d, g = %d, layer = %d, label = '%s'\n", i, r, g, layer, label);
      for (int chn=0; chn<512; chn++){
	sprintf (name, "%s_chn%02d", label, chn);
	sprintf (title, "PDO of %s channel %d;Pulser Amplitude;PDO", label, chn);
	pdo[i+8*layer][chn] = new   TProfile(name, title, 22, 25, 1125);
	pdo[i+8*layer][chn]->SetMarkerStyle(20);
      }
      sprintf (name, "inter%02d", id.GetIndex());
      sprintf (title, "Intercept %s;chn;intercept in PDO", label);
      inter[i+8*layer] = new   TH1F(name, title, 512, 0, 512);
      
      sprintf (name, "slope%02d", id.GetIndex());
      sprintf (title, "Slope %s;chn;slope", label);
      slope[i+8*layer] = new   TH1F(name, title, 512, 0, 512);
    } // next elink 
  } // next layer

  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".calib.root"), "recreate");//output file
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }
  
  TTree *tree = new TTree("tree","Calib");
  tree->Branch("branch",&data, "ind/I:vmm:chn:layer:type:inter/F:slope:interErr:slopeErr:chi2");

  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
    
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);
    }
    const double amp[4] = {250, 500, 750, 1000};
    double pulseAmp = amp[iFile-1];
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
	break;
      }
      try{
	event.ReadNSW(buffer, size); // read the data
	
	//Filling NSW histograms
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac = sec->GetPacket(j);
	    int ind = pac->GetIndex();
	    for (int k=0; k<pac->GetWidth(); k++){
	      NSWChannel *chn = pac->GetChannel(k);
	      int n = 64*chn->GetVMM()+chn->GetVMMChannel();
	      pdo[ind][n]->Fill(pulseAmp, chn->GetPDO());
	    } // next chn
	  } // next packet
	} // next sector
	
	eventNumber++;
	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
      } // try     
      catch (NSWReadException& ex){
	printf ("ReadNSW() encountered an Exception for event %d\n", eventNumber);
	printf("%s\n", ex.what());
	errorCount++;
      }
    } while ((size>0)&&(eventNumber<100000*iFile));// next event
    fclose(infile);
  } // next file
  
  for (int ind=32; ind<64; ind++){ // GIF 4 layer
    data.ind = ind;
    data.layer = ind / 8;
    for (int vmm=0; vmm<8; vmm++){
      data.vmm = vmm;
      for (int chn = 0; chn<64; chn++){
	data.chn = chn;
	int i = ind%8;
	int e = (i>3)? i%2 : (i/2)%2; // endpoint, 0: pad, 1: strip 
	int r = (i>3)? (i-2)/2 : 0;   // radius
	int g = (i>3)? 0 : i%2;       // group
	id.SetRadius(r);
	id.SetChannelGroup(g);
	id.SetEndPointOld(e);
	id.SetLayer(data.layer);
	id.GetBoardID(label);
	id.SetVMMChannel(chn);
	id.SetVMM(vmm);
	data.type = e;
	if ((e==0) &&(vmm==2)) data.type = 2; // wire
	if (pdo[ind][64*vmm+chn]->GetEntries() > 300){
	  
	  pdo[ind][64*vmm+chn]->Fit("pol1", "Q");
	  TF1 *fit = pdo[ind][64*vmm+chn]->GetFunction("pol1");
	  data.inter = fit->GetParameter(0);	
	  data.slope = fit->GetParameter(1);
	  data.interErr = fit->GetParError(0);	
	  data.slopeErr = fit->GetParError(1);	
	  data.chi2 = fit->GetChisquare();
	  tree->Fill();
	  pdo[ind][64*vmm+chn]->Write();
	  inter[ind]->SetBinContent(1+chn+64*vmm, fit->GetParameter(0));
	  inter[ind]->SetBinError  (1+chn+64*vmm, fit->GetParError(0));
	  slope[ind]->SetBinContent(1+chn+64*vmm, fit->GetParameter(1));
	  slope[ind]->SetBinError  (1+chn+64*vmm, fit->GetParError(1));
	  fprintf (outfile, "%6d %6.3f %6.1f\n", id.GetDatabaseID(), data.slope, data.inter);
	}
      }
    }
  }
  for (int ind=32; ind<64; ind++) inter[ind]->Write();
  for (int ind=32; ind<64; ind++) slope[ind]->Write();
  f->Write();
  f->Close();
  fclose(outfile);
  delete [] buffer;  
  printf("\nTotal number of events = %d\n", eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}

