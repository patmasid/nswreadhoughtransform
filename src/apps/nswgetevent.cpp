// reads NSWEvent data and returns one event
// 
#include <stdio.h>
#include <vector>
#include <bits/stdc++.h> 
#include "NSWRead/NSWEvent.h"

#define maxSize 21000000/4  // 4000*32   // for event buffer




int main(int argc, char *argv[]){

  FILE* infile;
  FILE* outfile;
  NSWEvent event;
  int eventCount = 0;
  int size ; // event size
  unsigned int * buffer;
  char fil[300], fils[300];


 if (argc<3) {
    printf ("usage: nswgetevent infile eventnumber\n");
    printf ("reads NSWEvent data and puts an event into a new file.\n");
    return 1;
  }

  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  infile = fopen (argv[1], "rb");
  if (!infile) {
    printf ("Cannot open '%s'\n", argv[1]);
    return 1;
  }

  int target;
  sscanf(argv[2], "%d", &target);
  char ext[40];
  sprintf (ext, ".Event_%d.data", target);
  strcat(fils, ext);

  printf ("Looking for event %d\n\n", target);
  
  do{ //event loop
    try{
      size = event.ReadATLAS(infile, buffer, maxSize);
    }
    catch (NSWReadException& ex){
       if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
      break;
    }
    if (eventCount%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventCount);

    eventCount++;
  } while ((size>0)&&(eventCount-1!=target)); // next event

  fclose (infile);
  if (eventCount-1 == target){
    printf ("Found event %d with %d words.\n", target, size);
    int n = size;
    if (n>8*20) n = 8*20;
    for (int i=0; i<n; i++){
      if (i%8 == 0) printf ("\n");
      printf ("%08X ", buffer[i]);
    }
    printf ("\n");
    
    outfile = fopen (fils, "wb");
    if (!outfile) {
      printf ("Cannot open '%s'\n", fils);
      return 1;
    }
    fwrite (buffer, 4, size, outfile); // write event to file
    fclose (outfile);
  } else {
    printf ("Not found.\n");
  }
    
  delete [] buffer;
}
