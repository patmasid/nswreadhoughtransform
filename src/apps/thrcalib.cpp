/** @file thrcalib.cpp
This application reads 4 data files for thresholds of -30, -10, 10, and 30. 

PDO measurements are filled into  histograms.
Results are stored in a tree and a txt file for the NSWCalibration.
Used for sTGC GIF data.
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "TH2.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TProfile.h"
#include "NSWRead/NSWEvent.h"



/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer
  char fil[300], fils[300];
  int eventNumber, errorCount =0;
  FILE *infile;
  if(argc != 6){
    printf("Usage: nswcalib file-30.data file-10.data file+10.data file+30.data file+50.data\n");
    exit(0);
  }
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }
  eventNumber = 0;
  NSWEvent event;
  NSWSector* sec;
  
  /** \struct Calib
      defines variables in root tree.
      One entry per channel
  */
  struct Calib{
    /** elink index */
    int ind;
    /** vmm number */
    int vmm;
    /** channel number */
    int chn;
    /** layer */
    int layer;
    /** electrode type */
    int type;
    /** baseline */
    float inter, slope;
    float interErr, slopeErr;
    /** turnon */
    float turnon[4];
    /** width */
    float width[4];
    /** turnon error*/
    float turnonErr[4];
    /** width Error */
    float widthErr[4];
    /** chi2 */
    float chi2[4];
  };
  Calib data;
  
  TH1F * pdo[64][512][5]; // pdo vs pulse per link and channel
  TH1F * turnon[64][512]; // sigmoid turnon

  FILE* outfile = fopen("thrcal.txt", "w");

  const double thr[5] = {-30, -10, 10, 30, 50};

  char name[80], title[80], label[40];
  NSWChannelID id; 
  id.SetTechnologyOld(0); // MM = 1, sTGC = 0
  //id.SetSector(sectors[0]); // only one sector for now
  id.SetRawSector(13-1); // only one sector for now
  id.SetEtaOld(13>0?0:1); // 0: eta=+1: endcap A, 1: eta=-1, endcap C
  id.SetDataTypeOld(0); // 0: L1A, 1: config, 2: monitor
  id.SetEndPointOld(1); // 0: pad, 1: strip,  2: Trig. Proc, 3: Pad trig
  for (int layer=4; layer<8; layer++){ // GIF 4 layer
    for (unsigned int i=0; i<8; i++){
      int r = (i>3)? (i-2)/2 : 0;   // radius
      int g = (i>3)? 0 : i%2;       // group
      int e = (i>3)? i%2 : (i/2)%2; // endpoint
      id.SetRadius(r);
      id.SetChannelGroup(g);
      id.SetEndPointOld(e);
      id.SetLayer(layer);
      id.GetBoardID(label);
      //printf ("i = %2d, r = %2d, g = %d, layer = %d, label = '%s'\n", i, r, g, layer, label);
      for (int chn=0; chn<512; chn++){
	for (int k=0; k<5; k++){
	  sprintf (name, "%s_chn%02d_%d", label, chn, k);
	  sprintf (title, "PDO of %s channel %d thr %d;PDO", label, chn, (int)thr[k]);
	  pdo[i+8*layer][chn][k] = new   TH1F(name, title, 200, 0, 200);
	}
	sprintf (name, "turnon_%s_chn%02d", label, chn);
	sprintf (title, "Turnon %s_chn%02d;thr;turnon in PDO", label, chn);
	turnon[i+8*layer][chn] = new   TH1F(name, title,  9, -35, 55);
      }
    } // next elink 
  } // next layer

  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".calib.root"), "recreate");//output file
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }
  
  TTree *tree = new TTree("tree","Calib");
  tree->Branch("branch",&data, "ind/I:vmm:chn:layer:type:inter/F:slope:interErr:slopeErrturnon[5]/F:width[5]:turnonErr[5]:widthErr[5]:chi2[5]");

  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
    
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);
    }
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
	break;
      }
      try{
	event.ReadNSW(buffer, size); // read the data
	
	//Filling NSW histograms
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac = sec->GetPacket(j);
	    int ind = pac->GetIndex();
	    for (int k=0; k<pac->GetWidth(); k++){
	      NSWChannel *chn = pac->GetChannel(k);
	      if (chn->GetNeighbor() == 0) continue; // only non-neighbors
	      int n = 64*chn->GetVMM()+chn->GetVMMChannel();
	      pdo[ind][n][iFile-1]->Fill(chn->GetPDO());
	    } // next chn
	  } // next packet
	} // next sector
	
	eventNumber++;
	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
      } // try     
      catch (NSWReadException& ex){
	printf ("ReadNSW() encountered an Exception for event %d\n", eventNumber);
	printf("%s\n", ex.what());
	errorCount++;
      }
    } while ((size>0)&&(eventNumber<100000*iFile));// next event
    fclose(infile);
  } // next file

  TF1* sig = new TF1("sig", "1/(1+exp(-[1]*(x-[0])))", 20, 200);
  for (int ind=32; ind<64; ind++){ // GIF 4 layer
    data.ind = ind;
    data.layer = ind / 8;
    for (int vmm=0; vmm<8; vmm++){
      data.vmm = vmm;
      for (int chn = 0; chn<64; chn++){
	data.chn = chn;
	int i = ind%8;
	int e = (i>3)? i%2 : (i/2)%2; // endpoint, 0: pad, 1: strip 
	int r = (i>3)? (i-2)/2 : 0;   // radius
	int g = (i>3)? 0 : i%2;       // group
	id.SetRadius(r);
	id.SetChannelGroup(g);
	id.SetEndPointOld(e);
	id.SetLayer(data.layer);
	id.GetBoardID(label);
	id.SetVMMChannel(chn);
	id.SetVMM(vmm);
	data.type = e;
	if ((e==0) &&(vmm==2)) data.type = 2; // wire
	if (pdo[ind][64*vmm+chn][0]->GetEntries() > 300){ // filled enough
	  for (int iFile=1; iFile<5; iFile++){
	    pdo[ind][64*vmm+chn][iFile]->Divide(pdo[ind][64*vmm+chn][0]);
	    sig->SetParameters(50+20*iFile, 0.6);
	    pdo[ind][64*vmm+chn][iFile]->Fit("sig", "Q", "", 40, 200);
	    data.turnon[iFile-1] = sig->GetParameter(0);	
	    data.width[iFile-1] = sig->GetParameter(1);
	    data.turnonErr[iFile-1] = sig->GetParError(0);	
	    data.widthErr[iFile-1] = sig->GetParError(1);	
	    data.chi2[iFile-1] = sig->GetChisquare();
	    pdo[ind][64*vmm+chn][iFile]->Write();
	    turnon[ind][64*vmm+chn]->SetBinContent(1+2*iFile, sig->GetParameter(0));
	    turnon[ind][64*vmm+chn]->SetBinError  (1+2*iFile, sig->GetParError(0));
	  }
	  if (turnon[ind][64*vmm+chn]->GetEntries()<3) continue;
	  turnon[ind][64*vmm+chn]->Fit("pol1", "Q");
	  TF1 *fit = turnon[ind][64*vmm+chn]->GetFunction("pol1");
	  data.inter = fit->GetParameter(0);	
	  data.slope = fit->GetParameter(1);
	  data.interErr = fit->GetParError(0);		
	  data.slopeErr = fit->GetParError(1);	
	  turnon[ind][64*vmm+chn]->Write();
	  tree->Fill();
	  double base =  data.inter-30*data.slope;
	  if ((base>0) && (base<150)) {
	    fprintf (outfile, "%6d %6.3f %6.1f\n", id.GetDatabaseID(), base, 1.0);
	  }
	}
      }
    }
  }
  

 
  
  //for (int ind=32; ind<64; ind++) turnon[ind]->Write();
  //for (int ind=32; ind<64; ind++) width[ind]->Write();
  f->Write();
  f->Close();
  fclose(outfile);
  delete [] buffer;  
  printf("\nTotal number of events = %d\n", eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}

