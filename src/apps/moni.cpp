/** @file moni.cpp
This application fills a root tree with hit reconstruction results.
It can be used for monitoring, or for looking at hit reconstruction
results. The tree variables are stored in moni_t.

Reads nsw data file and produces monitoring histograms based on single
hits. The tree is filled after hitfinding and there is one entry per hit.

@note this should better be done after a track segment has been
found, as many hit parameters depend on track angle.

@see moniROD for monitoring ROD clusters
 
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"TH2.h"
//#include"TGraph.h"
#include"TFile.h"
#include"TF1.h"
#include"TTree.h"
#include"TProfile.h"
#include"NSWRead/NSWEvent.h"



/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


/** read pedestals and data file, produce root file of single hits 
for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer

  char fil[300], fils[300];
  int eventNumber, errorCount = 0;

  FILE *infile;

  if(argc <2){
    printf("Usage: moni file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

                                                        

  eventNumber = 0;

  NSWEvent event;
  NSWSector* sec;

  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".moni.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }

  /** \struct moni_t
      defines variables in root tree.
      One entry per packet.
  */
  struct moni_t{
    /** event number, increments over all files. */
    int event;
    /** global event ID from ATLAS */
    int evt;
    /** Number of sectors per event */
    int nSec;
    /** Number of packets per sector */
    int nPac;
    /** Level one ID */
    int l1id;
    /** bcid */
    int bcid;
    /** number of hits per sector */
    int totHits;
    /** number of hits per packet */
    int wid;
    /** number of hits per packet from trailer */
    int len;
    /** link ID number */
    int linkID;
    /** missing VMMs in hit trailer */
    int missing;
    /** board layer */
    int layer;
    /** board radius */
    int radius;
    /** count of duplicate packet elink IDs */
    int duplicate;
  };
  moni_t branch;


  /** \struct moni_e
      defines variables in root tree.
      One entry per sector (event).
  */
  struct moni_e{
    /** event number, increments over all files. */
    int event;
    /** global event ID from ATLAS */
    int evt;
    /** Level one ID */
    int l1id;
    /** number of packets */
    int npac;
    /** number of duplicate packets */
    int ndup;
    /** number of hits per sector */
    int totHits;
  };
  moni_e edata;
  
  TH1F * bcidH = new TH1F ("bcidH", "BCID per Event", 3500, 0, 3500);
  TH1F * activeH = new TH1F ("activeH", "Active strips per Event", 3500, 0, 3500);
  TH1F * npacH = new TH1F ("npacH", "Packets per Event;packets", 500, 0, 500);
  TH1F * occ[8];
  TH2F * time2d[8], *amp2d[8];
  char name[80], title[80];
  for (int i=0; i<8; i++) {
    sprintf (name, "occ%d", i);
    sprintf (title, "Occupancy of layer %d;channel;Occupancy", i);
    occ[i] = new TH1F (name, title, 8192, 0, 8192);
    sprintf (name, "time2d%d", i);
    sprintf (title, "Drift time of layer %d;strip;time in ns", i);
    time2d[i] = new TH2F (name, title, 8192, 0, 8192, 512, -25, 7*25);
    sprintf (name, "amp2d%d", i);
    sprintf (title, "Amplitude of layer %d;strip;amplitude", i);
    amp2d[i] = new TH2F (name, title, 8192, 0, 8192, 512, 0, 1024);
  }

  TTree *tree = new TTree("tree","Monitoring");
  tree->Branch("branch",&branch,"event/I:evt:nSec:npac:l1id:bcid:totHits:wid:len:linkID:missing:layer:radius:duplicate");

  TTree *etree = new TTree("etree","Events");
  etree->Branch("branch",&edata, "event/I:evt:l1id:npac:ndup:totHits");

  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadATLAS(): %s\n", ex.what());
	}
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data
	branch.nSec = event.GetNumSector();
	if (event.GetNumSector()>0) bcidH->Fill(event.GetSector(0)->GetBCID());

	//fill hitlist with cluster data
	branch.event = eventNumber;
	edata.event = eventNumber;
	branch.evt = event.GetGlobalID();
	edata.evt = event.GetGlobalID();
	branch.totHits = 0; 
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  branch.bcid = sec->GetBCID();
	  branch.nPac = sec->GetNumPacket();
	  activeH->Fill(sec->GetActiveStrips(80));
	  edata.npac = sec->GetNumPacket();
	  edata.ndup = 0;
	  edata.totHits = 0;
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac =sec->GetPacket(j);
	    branch.l1id = pac->GetL1ID();
	    branch.len = pac->GetLength();
	    branch.wid = pac->GetWidth();
	    branch.totHits += pac->GetWidth();
	    edata.totHits  += pac->GetWidth();
	    branch.linkID = pac->GetLinkID();
	    branch.missing = pac->GetMissing();
	    branch.layer = pac->GetLayer();
	    branch.radius = pac->GetRadius();
	    branch.duplicate = 0;
	    if (!pac->IsNull()) edata.l1id = pac->GetL1ID();

	    for (int k=j+1; k<sec->GetNumPacket(); k++){ // loop over packets
	      NSWPacket *p2 =sec->GetPacket(k);
	      if (p2->GetLinkID() == branch.linkID) {
		edata.ndup++;
		branch.duplicate++;
	      }
	    }
	    int layer = pac->GetLayer();
	    for (int k=0; k<pac->GetWidth(); k++){
	      NSWChannel* chn = pac->GetChannel(k);
	      occ[layer]->Fill(chn->GetDetectorStrip());
	      time2d[layer]->Fill(chn->GetDetectorStrip(), chn->GetPeakingTime());
	      amp2d [layer]->Fill(chn->GetDetectorStrip(), chn->GetPDO());
	    } // next channel
	    tree->Fill();
	  } // next packet
	} // next sector
	etree->Fill();

	eventNumber++;
	if (eventNumber%1000==0)  printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0);// next event
    fclose(infile);
  } // next file
  tree->Write();
  etree->Write();
  bcidH->Write();
  activeH->Write();
  npacH->Write();
  for (int i=0; i<8; i++) {
    occ[i]->Write();
    time2d[i]->Write();
    amp2d[i]->Write();
  }
  f->Close();
  delete [] buffer;  
  printf("\nTotal number of events = %d\n", eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}

