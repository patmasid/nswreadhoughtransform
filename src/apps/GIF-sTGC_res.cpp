/** @file nswseg4.cpp
This application fills a root tree with segment reconstruction results.
It can be used for monitoring, or for looking at hit reconstruction
results. The tree variables are stored in moni_t.

This version uses only 4 layers, and can run on GIF data from sTGC.

Reads nsw data file and produces monitoring histograms based on segments.
The tree is filled after cluster finding and there is one entry per segment.

@see moni.cpp for monitoring packet data
 
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include "TH2.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "TLatex.h"
#include "TStyle.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWSegmentList.h"
#include "NSWRead/NSWCalibration.h"

/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


void linfit(double x[], double y[], int ndata, double sig[], int mwt, double *a, double *b, double *siga, double *sigb, double *chi2);


/** calculates the efficiency and the error on the efficiency. 
  @param numer the numerator of the ratio
  @param denom the denominator of the ratio
  @param effic the calculated efficiency
  @param error the calculated error */
void CalculateEffi(int numer, int denom, double &effi, double &error) {
  effi = -1;
  error = 0;

  // Check for divide by zero:
  if (denom <= 0) return;

  effi = numer / (double)denom;

  // Error calculation from arXiv:physics/0701199v1
  double dnum = (double)numer;
  double dden = (double)denom;

  double disc = ((dnum+1)*(dnum+2))/((dden+2)*(dden+3))
    - pow(dnum+1,2)/pow(dden+2,2);
  if (disc >=0) error = sqrt(disc);

  /**********************
  // Error calculation from P. Avery.
  double disc = (1.0/denom * (effic + 1/denom)* (1.0-effic + 1/denom)
		 / (pow(1+2.0/denom,2.0)*(1.0+3.0/denom)));
  if (disc >=0) {
    error = sqrt(disc);

    // For effic = 0 or 1, add to the error the value of the unbiased
    // estimate of Y1/Y2. This will give a better estimate of the conf.
    // interval.
    if (effic==1.0 || effic==0.0) {
      error = (1.0/denom) / (1.0+2.0/denom) + error;
    }
  }
  ************************/
}



/** read  data file, produce root file for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer
  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  FILE *infile;

  if(argc <4){
    printf("Usage: GIF-sTGC_res pdoCalib.txt bad_channels.txt sng(0/1) file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWCalibration cal;
  if (cal.ReadPDO(argv[1]) != 1){
    printf ("Cannot read calibration file.\n");
    return 1;
  }
  // cal.Print();

  //Enable/disable neighbour
  bool sng = true;
  if (argv[3] == 0)
    sng = false;


  std::vector< std::vector <std::vector<int> >> v_bad_channels_strip;
  v_bad_channels_strip.resize(3);
  for(int iQ=0; iQ<3; iQ++){
    v_bad_channels_strip[iQ].resize(8);
  }

  //Converting the bad channels to vector
  std::string line;
  std::ifstream myfile(argv[2]);
  if ( myfile.is_open() ) {
    while ( std::getline ( myfile,line ) ) {
      std::string data_str =  line;
      std::istringstream iss(line);

      int radius = -1;
      int layer = -1;
      std::string det_type = "";
      int det_chnl = -1;

      if ( !(iss
             >> radius
             >> layer
             >> det_type
             >> det_chnl) ){
	std::cout << "Error in reading bad channels file!" << std::endl;
        return 1;
      }

      if(det_type == "strip"){
        v_bad_channels_strip[radius][layer].push_back(det_chnl);
      }

    }
  }



  NSWEvent event;
  NSWSector* sec;
  NSWClusterList clusterList;

  clusterList.Set_vecBadChanls(v_bad_channels_strip);

  strcpy(fils,argv[4]);
  fils[(strlen(argv[4])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".gaus.UpdateRes.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }
  // char mainDir[15] = "PerEventPlots";
  // f->cd();
  // f->mkdir(mainDir);

  // \struct moni_t
  //    defines variables in root tree.
  //    One entry per ROD cluster.
  
  struct moni_t{
    /** event number, increments over all files. */
    int event;
    /** Level one ID */
    int l1id;
    /** number of precision layer clusters */
    int nprec;
    /** number of clusters */
    int nclu;
    /** number of channels of this cluster */
    int wid[8];
    /** center of gravity*/
    float cog[8];
    /** better cog*/
    float cog2[8];
    /** cluster charge*/
    float charge[8];
    /** z position */
    float z[8];
    /** line fit */
    float inter, slope, chi2, angle;
    /** predicted positon and residual */
    float pred[8], res[8];
    /** median y positon residual */
    float y;
    /** amplitude and time per channel*/
    float amp[8][10], time[8][10];
    /** number of missing strips in the cluster */
    int gap[8];
    /** strip number of the first strip */
    int start[8];
    /** strip number of the peak */
    int peak[8];
    /** number of channels with neighbor flag = 0 */
    int nflags[8];
    /** raw parabola interpolation */
    float raw[8];
    /** corrected parabola interpolation */
    float para[8];
    /** three strip center of gravity */
    float cog3[8];
    /** all strip gaussian centroid */
    float gaus[8];
  };
  moni_t branch;

  TH1F *hResIn[4];
  TH1F *hResEx[4];

  TTree *tree = new TTree("tree","Clusters");
  tree->Branch("branch",&branch,"event/I:l1id:nprec:nclu:wid[8]:cog[8]/F:cog2[8]:charge[8]:z[8]:inter:slope:chi2:angle:pred[8]:res[8]:y:amp[8][10]:time[8][10]:gap[8]/I:start[8]:peak[8]:nflags[8]:raw[8]/F:para[8]:cog3[8]:gaus[8]");
  TH1F* hSeg = new TH1F ("hSeg", "Segments per event;segments", 30, 0, 30);
  TH1F* hClu = new TH1F ("hClu", "Clusters per event;clusters", 100, 0, 100);
  TH1F* hDist = new TH1F ("hDist", "intercept distance;mm", 200, -400, 400);
  TH1F* hAli5 = new TH1F ("hAli5", "Alignment for layer 5;mm", 1000, -5, 5);
  TH1F* hAli6 = new TH1F ("hAli6", "Alignment for layer 6;mm", 1000, -5, 5);
  TH1F* hResCog = new TH1F ("hResCog", "Full cluster cog residual;Residual in mm", 1000, -3, 3);
  TH1F* hResCog3 = new TH1F ("hResCog3", "Three strip cog residual;Residual in mm", 1000, -3, 3);
  TH1F* hResPara = new TH1F ("hResPara", "Three strip parabola residual;Residual in mm", 1000, -3, 3);
  TH1F* hChi2 = new TH1F ("hChi2", "Chi2 per segment", 250, 0, 50);
  TH1F* hNum[8];

  TH1F* hOcc[8];
  TH2F* hTime[8];
  TH1F* hn[8], *hN[8], *hEffi[8]; // efficiency

  TH1F* hClusterCutflow = new TH1F("hClusterCutflow", "Cluster Cutflow", 10, -0.5, 9.5);
  TH1F* hEventCutflow   = new TH1F("hEventCutflow",   "Event Cutflow"  , 10, -0.5, 9.5);

  TH2F* hAng_Chi2  = new TH2F("hAng_Chi2", "Track angle vs chi2;Angle;chi2",65, -65, 65, 100, 0, 100);
  TH2F* hChi2_nSeg = new TH2F("hChi2_nSeg","Track chi2 vs segment multi;chi2;#Segment", 50,0,50, 10,0,10);

  char resname[10], restitle[80];
  for (int i=0; i<4; i++){
    sprintf(resname, "hResIn%d", i+1);
    sprintf(restitle, "Layer %d inclusive residual;Residual in mm", i+1);
    hResIn[i] = new TH1F(resname, restitle, 1000, -3, 3);

    sprintf(resname, "hResEx%d", i+1);
    sprintf(restitle, "Layer %d exclusive residual;Residual in mm", i+1);
    hResEx[i] = new TH1F(resname, restitle, 1000, -3, 3);
  }
 
  char name[80], title[80];
  for (int i=0; i<8; i++) {
    sprintf (name, "hOcc%d", i);
    sprintf (title, "Cluster position of layer %d;Position in mm", i);
    hOcc[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hTime%d", i);
    sprintf (title, "Cluster peaking time of layer %d;Position in mm;time in ns", i);
    hTime[i] = new TH2F (name, title, 1000, 0, 4000, 100, -25, 175);

    sprintf (name, "hn%d", i);
    sprintf (title, "Predicted position of layer %d;Position in mm", i);
    hn[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hN%d", i);
    sprintf (title, "Predicted positon of layer %d;Position in mm", i);
    hN[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hEffi%d", i);
    sprintf (title, "Strip cluster efficiency of layer %d;Position in mm;Efficiency", i);
    hEffi[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hNum%d", i);
    sprintf (title, "Number of clusters in layer %d", i);
    hNum[i] = new TH1F(name, title, 200, 0, 200);
  }

	  
  int totSeg = 0;
  int numSeg = 0; // per event
  int gifNumber = 0;
  
  // TGraph *cluster;
  // TGraphErrors *tracks;
  // TCanvas *c_cluster_per_layer;
  // TCanvas *c_tracks_per_event;

  for (int iFile=4; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data

        bool isSingleCluster = true;
  
        // mkdir in TFile for the event 
        // char dirname[30];
        // sprintf(dirname, "%s/Evt_%li", mainDir, eventNumber);
        // f->mkdir(dirname);

	//fill hitlist with cluster data
	branch.event = eventNumber;
        //if(eventNumber != 2) continue;
        //std::cout << "Event Number: " <<eventNumber <<std::endl;
        //std::cout << "******************************************"<<std::endl;
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  branch.l1id = sec->GetLevel1ID();
          cal.Calibrate(sec);
	  clusterList.Clear();
	  clusterList.Fill(sec, 20, 1);

          // *********************************************************************************
          // ************************* Remake cluster List ***********************************
          // *********************************************************************************
          //
          // layer
          for (int layer=0; layer<8; layer++){
            //std::cout<< "In Layer: "<< layer <<std::endl;
            //std::cout<< "First Loop......" <<std::endl;
            // cluster
            for (int i=0; i<clusterList.GetNumClusters(layer); i++){
	      NSWCluster *cl = clusterList.GetCluster(layer, i);
              int num_chns = cl->GetNumChannel();

              // channel
              std::vector<int> nul;
              nul.clear();
              for(int ichn=0; ichn<num_chns; ichn++){
                NSWChannel* channel = cl->GetChannel(ichn);
                //std::cout<< "ichn: " << ichn <<std::endl;
                //if( channel != NULL ) channel->Print();

                // find NULL position
                if(channel==NULL){
                   //std::cout<<"NULL channel number: "<<ichn<<std::endl;
                   if( ichn==0 || ichn == num_chns-1 ) nul.push_back(ichn);
                   else if( cl->GetChannel(ichn+1)->GetNeighbor() == 0 && cl->GetChannel(ichn-1)->GetNeighbor() == 0 )
                     nul.push_back(ichn);
                }
              } //channel

              //std::cout<< "Channel number: " << num_chns << "\tNULL number: " << nul.size() <<std::endl;
	      for(int j=0; j<nul.size(); j++){
                //std::cout<< "Splitting cluster..." <<std::endl;
                if( j== 0)
                  clusterList.SplitCluster(layer, i, nul[j], true);
                else
                  clusterList.SplitCluster(layer, i, nul[j] - nul[j-1] - 1, true);
                if( nul[j] != 0 && nul[j] != num_chns-1 ) i++;
              }
            } //cluster

            //std::cout<< "Second Loop......" <<std::endl;
            // cluster
            for (int i=0; i<clusterList.GetNumClusters(layer); i++){
	      NSWCluster *cl = clusterList.GetCluster(layer, i);
              int num_chns = cl->GetNumChannel();
              if(num_chns < 4) continue;

              // channel
              std::vector<int> valley;
              valley.clear();

              for(int ichn=0; ichn<num_chns; ichn++){
                NSWChannel* channel = cl->GetChannel(ichn);
                //std::cout<< "ichn: " << ichn <<std::endl;
                //if( channel != NULL ) channel->Print();

                if( channel == NULL ) continue;
                if(ichn==0 || ichn==num_chns-1) continue;
                // else{
                //   if( cl->GetChannel(ichn-1)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+1)->GetAmplitude() > channel->GetAmplitude() )
                //     channel->Print();
                // }

                if( cl->GetChannel(ichn-1) == NULL && cl->GetChannel(ichn+1) == NULL ){
                  //std::cout<< "--------1--------"<<std::endl;
                  if( cl->GetChannel(ichn-2)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+2)->GetAmplitude() > channel->GetAmplitude() ) valley.push_back(ichn); 
                }
                else if( cl->GetChannel(ichn-1) == NULL ){
                  //std::cout<< "--------2--------"<<std::endl;
                  if( cl->GetChannel(ichn-2)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+1)->GetAmplitude() > channel->GetAmplitude() ) valley.push_back(ichn); 
                }
                else if( cl->GetChannel(ichn+1) == NULL ){
                  //std::cout<< "--------3--------"<<std::endl;
                  if( cl->GetChannel(ichn-1)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+2)->GetAmplitude() > channel->GetAmplitude() ) valley.push_back(ichn); 
                }
                else if( cl->GetChannel(ichn-1) != NULL && cl->GetChannel(ichn+1) != NULL ){
                  //std::cout<< "--------4--------"<<std::endl;
                  if( cl->GetChannel(ichn-1)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+1)->GetAmplitude() > channel->GetAmplitude() ) valley.push_back(ichn); 
                  // channel->SetBaseline(32);
                  // channel->Print();
                  // std::cout<< channel->GetAmplitude() <<std::endl;
                }
              } //channel

              for(int j=0; j<valley.size(); j++){
                //std::cout<< "Splitting double maxima..." <<std::endl;
                if( j==0 )
                  clusterList.SplitCluster(layer, i, valley[j], false);
                else
                  clusterList.SplitCluster(layer, i, valley[j]-valley[j-1], false);
                i++;
              }
            } //cluster
          } //layer


          //std::cout<<"Before removing bad clusters..."<<std::endl;
          //std::cout << "Check Splitting results..." <<std::endl;
          //for(int layer=0; layer<8; layer++){
          //  std::cout<< "Layer: "<< layer<<std::endl;
          //  for (int i=0; i<clusterList.GetNumClusters(layer); i++){
          //    NSWCluster *cl = clusterList.GetCluster(layer, i);
          //    int num_chns = cl->GetNumChannel();
          //    for(int ichn=0; ichn<num_chns; ichn++){
          //      std::cout<< "ichn: "<< ichn<<std::endl;
          //      NSWChannel* channel = cl->GetChannel(ichn);
          //      if( channel == NULL ) continue;
          //      channel->Print();
          //    }
          //  }
          //}



          // *********************************************************************************
          // ***************************** Cluster Cut ***************************************
          // *********************************************************************************
          //
          // layer
          ///std::cout<<"Before removing bad clusters..."<<std::endl;
	  for (int layer=0; layer<8; layer++){
            //std::cout<< "Layer: "<< layer<<std::endl;

	    for (int i=0; i<clusterList.GetNumClusters(layer); i++){
	      NSWCluster *cl = clusterList.GetCluster(layer, i);

              hClusterCutflow->Fill(0);

              bool isValid = true;
              bool isBadChn = false, isEdgeNbr = true; 
              bool isBeamArea = true, isPeakNbr = false;
              bool isEdgePeak = false, isNegPdo = false;
              int peak_chnl = 0;
              double max_pdo = 0;
              int num_chns = cl->GetNumChannel();
        
              for(int ichn=0; ichn<num_chns; ichn++){   
                //std::cout<<"ichn: " <<ichn<<std::endl;

                //Bad channel
                NSWChannel* channel = cl->GetChannel(ichn);
                int layer = -1;
                int chn_num = -1;
                bool is_chn_bad = true;

                if(channel==NULL){
                  if(ichn==0){
                    layer = cl->GetChannel(ichn+1)->GetLayer();
                    chn_num = cl->GetChannel(ichn+1)->GetDetectorStrip()-1;
                    // std::cout << cl->GetChannel(ichn+1)->GetAmplitude() <<std::endl;
                  }
                  else if(ichn==num_chns-1){
                    layer = cl->GetChannel(ichn-1)->GetLayer();
                    chn_num= cl->GetChannel(ichn-1)->GetDetectorStrip()+1;
                    // std::cout << cl->GetChannel(ichn-1)->GetAmplitude() <<std::endl;
                  }
                  else{
                    if (cl->GetChannel(ichn-1) == NULL){
                      layer = cl->GetChannel(ichn+1)->GetLayer();
                      chn_num= cl->GetChannel(ichn+1)->GetDetectorStrip()-1;
                    }
                    else{
                      layer = cl->GetChannel(ichn-1)->GetLayer();
                      chn_num= cl->GetChannel(ichn-1)->GetDetectorStrip()+1;
                    }
                  }
                }
                else{
                  //channel->Print();
                  layer = channel->GetLayer();
                  chn_num = channel->GetDetectorStrip();

                  if( max_pdo < channel->GetAmplitude() ){
                    max_pdo = channel->GetAmplitude();
                    peak_chnl = ichn;
                  }

                  if( channel->GetAmplitude() < 0 ) {isNegPdo = true;break;}
                  // std::cout << channel->GetAmplitude() <<std::endl;
                }
                isBadChn = clusterList.isChnBad(layer, chn_num);
                if(isBadChn) break;
        
                if(channel==NULL) continue;
                if(sng){
                  //std::cout << "sng: " << sng << std::endl;
                  if( ichn==0 || ichn==(num_chns-1) ){
                    //edge channels are not neighbours
                    if(channel->GetNeighbor()==1){
                      if( ichn == 0 && (cl->GetSplit() == 0 || cl->GetSplit() == 2) ){
                        //isEdgeNbr = false;
                        isEdgeNbr = true;
                        break;
                      }
                      else if( ichn == num_chns-1 && (cl->GetSplit() == 0 || cl->GetSplit() == 1) ){
                        //isEdgeNbr = false;
                        isEdgeNbr = true;
                        break;
                      }
                      //std::cout<<"Edge not neighbour!"<<std::endl;
                      //isValid=(isValid && false);
                      //return isValid;
                    }
                    //channel outside beam area
                    if(chn_num<140 || chn_num>330){
                      isBeamArea = false;
                      //std::cout<<"Outside beam area!"<<std::endl;
                      //isValid=(isValid && false);
                      break;
                      //return isValid;
                    }
                  }
                  //middle ones are neighbours 
                  else if(channel->GetNeighbor()==0){
                    isPeakNbr = true;
                      //std::cout<<"Peak is neighbour!"<<std::endl;
                    //isValid=(isValid && false);
                  }
                  //return isValid;
                }
              }//chnl

              if( peak_chnl == 0 || peak_chnl == num_chns-1 ) isEdgePeak = true;

              isValid = !isBadChn && isEdgeNbr && isBeamArea && !isPeakNbr && !isEdgePeak && !isNegPdo;
              if(!isValid) {
                //std::cout<<"Cluster removed!"<<std::endl;
                clusterList.RemoveBadCluster(layer, i);
                i--;
              }

              if(isBadChn) continue;
              hClusterCutflow->Fill(1);

              if (!isEdgeNbr) continue;
              hClusterCutflow->Fill(2);

              if (!isBeamArea) continue;
              hClusterCutflow->Fill(3);

              if (isPeakNbr) continue;
              hClusterCutflow->Fill(4);

              if (isEdgePeak) continue;
              hClusterCutflow->Fill(5);

              if (isNegPdo) continue;
              hClusterCutflow->Fill(6);

            }//cluster
            // std::cout<<"Cluster Size: " <<clusterList.GetNumClusters(layer)<<std::endl;
          }//layer


          //std::cout<<"After removing bad clusters..."<<std::endl;
          //std::cout << "Check Splitting results..." <<std::endl;
          //for(int layer=0; layer<8; layer++){
          //  std::cout<< "Layer: "<< layer<<std::endl;
          //  for (int i=0; i<clusterList.GetNumClusters(layer); i++){
          //    NSWCluster *cl = clusterList.GetCluster(layer, i);
          //    int num_chns = cl->GetNumChannel();
          //    for(int ichn=0; ichn<num_chns; ichn++){
          //      std::cout<< "ichn: "<< ichn<<std::endl;
          //      NSWChannel* channel = cl->GetChannel(ichn);
          //      if( channel == NULL ) continue;
          //      channel->Print();
          //    }
          //  }
          //}

	  int nClu = 0;
	  for (int layer=0; layer<8; layer++){
            if (clusterList.GetNumClusters(layer) > 1) isSingleCluster = false;
	    nClu += clusterList.GetNumClusters(layer);
	    hNum[layer]->Fill(clusterList.GetNumClusters(layer));

            // draw cluster plots per layer
            // std::vector<float> xpos, pdo;
            // std::vector<TF1*> func;
            // xpos.clear();
            // pdo.clear();
            // func.clear();

	    for (int i=0; i<clusterList.GetNumClusters(layer); i++){
	      NSWCluster *c = clusterList.GetCluster(layer, i);

              // channel loop inside cluster
              // for(unsigned int chn_i=0; chn_i<c->GetNumChannel(); chn_i++){
              //   NSWChannel *chn = c->GetChannel(chn_i);
              //   if(chn == NULL) continue;
              //   xpos.push_back(chn->GetStripPosition());
              //   pdo.push_back(chn->GetAmplitude());
              // }

              // func.push_back(c->GetGaussianFunction());

	      hOcc[layer]->Fill(c->GetGaussianCentroid());
	      hTime[layer]->Fill(c->GetGaussianCentroid(), c->GetPeakingTime());
	    }

            // f->cd(dirname);

            // if(clusterList.GetNumClusters(layer) != 0){
            //    char canvasName[40], graphName[40];
            //    sprintf(canvasName, "c_Evt_%li_Layer%i_Cluster", eventNumber, layer);
            //    sprintf(graphName, "Evt_%li_Layer%i_Cluster", eventNumber, layer);

            //    c_cluster_per_layer = new TCanvas(canvasName, canvasName, 1000, 600);
            //    c_cluster_per_layer->cd();

            //    double max_pdo = *max_element(pdo.begin(), pdo.end());

            //    cluster = new TGraph(xpos.size(), &xpos[0], &pdo[0]);
            //    cluster->SetName(graphName);
            //    cluster->SetTitle();
            //    cluster->SetMaximum(max_pdo*1.2);
            //    cluster->SetMinimum(0);
            //    cluster->GetXaxis()->SetTitle("x [mm]");
            //    cluster->GetYaxis()->SetTitle("pdo");
            //    cluster->SetMarkerColor(4);
            //    cluster->SetMarkerSize(1);
            //    cluster->SetMarkerStyle(21);
            //    cluster->Draw("AP");

            //    for (unsigned int j=0; j<func.size(); j++){
            //      func[j]->DrawF1(xpos[0]-2, xpos[xpos.size()-1]+2, "SAME");
            //    }

            //    c_cluster_per_layer->Update();
            //    c_cluster_per_layer->Write();
            // }

	  } //layer

          // f->cd();

	  hClu->Fill(nClu);
	  //if ((gifNumber<20)&&(nClu>2)) {
          //  DrawDisplay (sec, gifNumber);
          //  gifNumber++;
          //}
	  // if ((gifNumber<20)&&(nClu>2)) {
	  //   DrawDisplay (sec, gifNumber);
	  //   gifNumber++;
	  // }
	  //if (clusterList.GetNumClusters(0) < 1) continue;
	  //if (clusterList.GetNumClusters(1) < 1) continue;
	  //if (clusterList.GetNumClusters(2) < 1) continue;
	  //if (clusterList.GetNumClusters(3) < 1) continue;
	  //if (clusterList.GetNumClusters(4) < 1) continue;
	  //if (clusterList.GetNumClusters(5) < 1) continue;
	  //if (clusterList.GetNumClusters(6) < 1) continue;
	  //if (clusterList.GetNumClusters(7) < 1) continue;
				
	  //if (clusterList.GetNumClusters(0) > 5) continue;
	  //if (clusterList.GetNumClusters(1) > 5) continue;
	  //if (clusterList.GetNumClusters(2) > 5) continue;
	  //if (clusterList.GetNumClusters(3) > 5) continue;
	  //if (clusterList.GetNumClusters(4) > 5) continue;
	  //if (clusterList.GetNumClusters(5) > 5) continue;
	  //if (clusterList.GetNumClusters(6) > 5) continue;
	  //if (clusterList.GetNumClusters(7) > 5) continue;

	  //if (nClu > 20) continue;

	  NSWSegmentList segmentList;
	  segmentList.Clear();
	  //printf ("\n\nL1ID = %d\n", event.GetL1ID());
	  segmentList.Fill4 (&clusterList, 4, 10, 1);

	  hSeg->Fill(segmentList.GetNumSegments());

          // std::vector<float> strip_pos, strip_err, layer_pos;
          // std::vector<float> lin_inter, lin_slope, lin_chi2;
          // strip_pos.clear();
          // strip_err.clear();
          // layer_pos.clear();
          // lin_inter.clear();
          // lin_slope.clear();
          // lin_chi2.clear();

          // std::vector<TF1*> lin_func;
          // lin_func.clear();

	  for (int i=0; i<segmentList.GetNumSegments(); i++){
	    NSWSegment* s = segmentList.GetSegment(i);
	    branch.y =  s->GetYPosition();
	    for (int layer=0; layer<8; layer++){
	      branch.pred[layer] = s->GetPredicted (layer);
	      NSWCluster* c = s->GetCluster(layer);
	      if (c == NULL){
		branch.cog[layer] = -100;
		branch.cog2[layer] = -100;
		branch.charge[layer] = -100;
		branch.wid[layer] = -1;
		branch.z[layer] = -1;
		branch.res[layer] = -100;
		for (int i=0; i<10; i++){
		  branch.amp[layer][i] = -1;
		  branch.time[layer][i] = -100;
		}
		branch.gap[layer] = -1;
		branch.start[layer] = -1;
		branch.peak[layer] = -1;
		branch.nflags[layer] = -1;
		branch.raw[layer] = -1;
		branch.para[layer] = -1;
		branch.cog3[layer] = -1;
                branch.gaus[layer] = -1;
	      } else {
		branch.cog[layer] = c->GetCOG();
		branch.cog2[layer] = c->GetCOG2();
		branch.charge[layer] = c->GetCharge();
		branch.wid[layer] = c->GetNumChannel();
		branch.z[layer] = c->GetZPosition();
		branch.res[layer] = c->GetCOG() - branch.pred[layer];
		int chnMax = c->GetNumChannel();
		if (chnMax > 10) chnMax = 10;
		for (int j=0; j<chnMax; j++){
		  NSWChannel *chn = c->GetChannel(j);
		  if (chn != NULL) {
		    branch.amp[layer][j] = chn->GetAmplitude();
		    branch.time[layer][j] = chn->GetPeakingTime();
		  } else {
		    branch.amp[layer][j] = -1;
		    branch.time[layer][j] = -1;
		  }
		}
		for (int j=chnMax; j<10; j++){
		  branch.amp[layer][j] = -1;
		  branch.time[layer][j] = -1;
		}
		branch.gap[layer] = c->GetNumMissing();
		branch.start[layer] = c->GetStartChannel();
		branch.peak[layer] = c->GetPeakChannel();
		branch.nflags[layer] = c->CountNeighbors();
		branch.raw[layer] = c->GetParabolaRaw();
		branch.para[layer] = c->GetParabola();
		branch.cog3[layer] = c->GetCOG3();
                branch.gaus[layer] = c->GetGaussianCentroid();

		// strip_pos.push_back(c->GetGaussianCentroid());
                // strip_err.push_back(c->GetGaussianSigma());
		// layer_pos.push_back(c->GetZPosition());
	      }
	    } // next layer
	    
	    branch.inter = s->GetIntercept();
	    branch.slope = s->GetSlope();
	    branch.chi2 = s->GetChi2();
	    branch.angle = s->GetAngle();
	    branch.nprec = s->GetNumPrecision();
	    branch.nclu = s->GetNumCluster();
 
            hChi2->Fill(s->GetChi2());
            hAng_Chi2->Fill(s->GetAngle(), s->GetChi2());
            hChi2_nSeg->Fill(s->GetChi2(), segmentList.GetNumSegments());

	    // TF1 *flin = new TF1("flin", "[0]+[1]*x");
            // flin->SetParameter(0, s->GetIntercept());
            // flin->SetParameter(1, s->GetSlope());
            // flin->SetChisquare(s->GetChi2());
            // flin->SetParName(0, "inter");
            // flin->SetParName(1, "slope");
            // lin_func.push_back(flin);
            // lin_inter.push_back(s->GetIntercept());
            // lin_slope.push_back(s->GetSlope());
            // lin_chi2.push_back(s->GetChi2());

	    tree->Fill(); 
	    numSeg++;
	    totSeg++;
	    for (int j=i+1; j<segmentList.GetNumSegments(); j++){
	      NSWSegment* s2 = segmentList.GetSegment(j);
	      hDist->Fill(s->GetIntercept() - s2->GetIntercept());
	      //printf ("%d: %f %f\n", j, s->GetIntercept() , s2->GetIntercept());
	    }
	  } // next segment

          //char dirname[30];
          //sprintf(dirname, "%s/Evt_%li", mainDir, eventNumber);
          //f->cd(dirname);

          //if(strip_pos.size() != 0){
          //  double min=148, max=185;

          //  char canvasName[40], graphName[40];
          //  sprintf(canvasName, "c_Evt_%li_allTracks", eventNumber);
          //  sprintf(graphName, "Evt_%li_allTracks", eventNumber);
          //  c_tracks_per_event = new TCanvas(canvasName, canvasName, 800, 600);
          //  c_tracks_per_event->cd();

          //  tracks = new TGraphErrors(strip_pos.size(), &layer_pos[0], &strip_pos[0], 0, &strip_err[0]);
          //  tracks->SetName(graphName);
          //  tracks->SetTitle();
          //  tracks->GetXaxis()->SetLimits(min, max);
          //  // tracks->SetMaximum(500);
          //  // tracks->SetMinimum(0);
          //  tracks->GetXaxis()->SetTitle("x [mm]");
          //  tracks->GetYaxis()->SetTitle("y [mm]");
          //  tracks->SetMarkerColor(4);
          //  tracks->SetMarkerSize(1);
          //  tracks->SetMarkerStyle(21);
          //  tracks->Draw("AP");

          //  int min_chi2_ptr = max_element(lin_chi2.begin(), lin_chi2.end()) - lin_chi2.begin();

          //  for(unsigned int fun_i=0; fun_i<lin_func.size(); fun_i++){
          //    lin_func[fun_i]->DrawF1(min, max, "SAME");
          //  }

          //  std::string tex_slope = "slope = "+std::to_string(lin_slope[min_chi2_ptr]);
          //  std::string tex_inter = "intercept = "+std::to_string(lin_inter[min_chi2_ptr]);
          //  std::string tex_chi2  = "chi2 = "+std::to_string(lin_chi2[min_chi2_ptr]);

          //  TLatex *fit_display = new TLatex();
          //  fit_display->SetNDC();
          //  fit_display->SetTextFont(42);
          //  fit_display->SetTextSize(8);
          //  fit_display->DrawLatex(0.2, 0.80, tex_slope.c_str());
          //  fit_display->DrawLatex(0.2, 0.74, tex_inter.c_str());
          //  fit_display->DrawLatex(0.2, 0.68, tex_chi2.c_str());

          //  c_tracks_per_event->Update();
          //  c_tracks_per_event->Write();
          //}

          //for(unsigned int fun_i=0; fun_i<lin_func.size(); fun_i++) delete lin_func[fun_i];

          //f->cd();

          hEventCutflow->Fill(0);

	  //if (segmentList.GetNumSegments()==8) segmentList.Print();
	  if (segmentList.GetNumSegments() == 1){
            hEventCutflow->Fill(1);

	    NSWSegment* seg = segmentList.GetSegment(0);
	    if (seg->GetNumClusters() == 4) {
              hEventCutflow->Fill(2);

	      double cog[4], para[4];
	      for (int j=4; j<8; j++){ 
                cog[j-4]=seg->GetCluster(j)->GetCOG();
              }
	      if (seg->GetChi2() < 50){ // fill alignment histograms
		double m = (cog[0]-cog[3])/3;
		hAli5->Fill((7-5)*m+cog[3]-cog[1]);
		hAli6->Fill((7-6)*m+cog[3]-cog[2]);
	      }
	      hResCog->Fill(cog[1]+ali5-0.5*(cog[0]+cog[2]+ali6));
	      bool bad = false;
	      for (int j=4; j<7; j++) {
		NSWCluster* cl = seg->GetCluster(j);
		if (cl->GetCOG3() < -0.99) bad = true; 
		cog[j-4]=(cl->GetPeakChannel()+cl->GetCOG3()*5/3.5)*3.2;
		//cog[j-4]=(cl->GetPeakChannel()+cl->GetCOG3()*1.611)*3.2; // from fit, but bad
		para[j-4]=(cl->GetPeakChannel()+cl->GetParabola())*3.2;
		if ((j % 2) == 1){ // stagger by -0.5*pitch
		  cog[j-4] -= 0.5*seg->GetPitch();
		  para[j-4] -= 0.5*seg->GetPitch();
		}
	      }
	      if (!bad) {
		hResCog3->Fill(cog[1]+ali5-0.5*(cog[0]+cog[2]+ali6));
		hResPara->Fill(para[1]+ali5-0.5*(para[0]+para[2]+ali6));
	      }

              //
              // Resolution eval with sigma_inclusive*sigma_exclusive
              //
              //bool isAbnormal = false;
              int candidate = 0, exclude = 0;
              for(int i=4; i<8; i++){
                NSWCluster *cl = seg->GetCluster(i);
                if( cl->GetPeakNSWChannel()->GetRelBCID() >=3 && cl->GetPeakNSWChannel()->GetRelBCID() <=5 ) candidate += 1;
                else if( cl->GetPeakNSWChannel()->GetRelBCID() < 2 || cl->GetPeakNSWChannel()->GetRelBCID() == 7) exclude += 1;
              }
              if (candidate < 3) continue;
              if (exclude > 0) continue;

              hEventCutflow->Fill(3);

              for(int i=4; i<8; i++){
                NSWCluster *c1 = seg->GetCluster(i);
                //double abnormal_residual = branch.inter+branch.slope*c1->GetZPosition() - c1->GetGaussianCentroid();
                //if ( i == 4 && abnormal_residual <= -0.018 && abnormal_residual >= -0.024 ) isAbnormal = true;
                //if ( isAbnormal ) {
                //  std::cout<<"Layer: " <<i<<std::endl;
                //  for( int ichn=0; ichn< c1->GetNumChannel(); ichn++){
                //    NSWChannel *channel = c1->GetChannel(ichn);
                //    if ( channel != NULL ) channel->Print();
                //  }
                //  continue;
                //}

                if (i==5)
                  hResIn[i-4]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - c1->GetGaussianCentroid()-ali5 );
                else if (i==6)
                  hResIn[i-4]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - c1->GetGaussianCentroid()-ali6 );
                else
                  hResIn[i-4]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - c1->GetGaussianCentroid() );

                std::vector<double> xEx, yEx, sigEx;
                double interEx, slopeEx, chi2Ex;
                double interErrEx, slopeErrEx;
                xEx.clear();
                yEx.clear();
                sigEx.clear();

                for(int j=4; j<8; j++){
                  if(i!=j){
                    NSWCluster *c2 = seg->GetCluster(j);
                    xEx.push_back(c2->GetZPosition());
                    yEx.push_back(c2->GetGaussianCentroid());
                    sigEx.push_back(c2->GetGaussianSigma());
                    // std::cout<<"Exclusive data point    x"<<": "<<c2->GetZPosition()<<"\ty"<<": "<<c2->GetGaussianCentroid()<<std::endl;
                  }
                } 
                // std::cout<<"In exclusive fit:"<<std::endl;
                linfit(&xEx[0], &yEx[0], 3, &sigEx[0], 1, &interEx, &slopeEx, &interErrEx, &slopeErrEx, &chi2Ex);
                if (i==5)
                  hResEx[i-4]->Fill( (interEx+slopeEx*c1->GetZPosition()) - c1->GetGaussianCentroid()-ali5 );
                else if (i==6)
                  hResEx[i-4]->Fill( (interEx+slopeEx*c1->GetZPosition()) - c1->GetGaussianCentroid()-ali6 );
                else
                  hResEx[i-4]->Fill( (interEx+slopeEx*c1->GetZPosition()) - c1->GetGaussianCentroid() );

                // if (eventNumber < 100){
                //   std::cout<<"layer "<<i<<":"<<std::endl;
                //   std::cout<<"  intersept in "<<branch.inter<<std::endl;
                //   std::cout<<"  slope in     "<<branch.slope<<std::endl;
                //   std::cout<<"  intersept ex "<<interEx<<std::endl;
                //   std::cout<<"  slope ex     "<<slopeEx<<std::endl;
                //   std::cout<<"  z position "<<c1->GetZPosition()<<std::endl;
                //   std::cout<<"  hit pos    "<<c1->GetGaussianCentroid()<<std::endl;
                //   std::cout<<"  res in     "<<branch.inter+branch.slope*c1->GetZPosition() - c1->GetGaussianCentroid()<<std::endl;
                //   std::cout<<"  res ex     "<<interEx+slopeEx*c1->GetZPosition() - c1->GetGaussianCentroid()<<std::endl;
                // }
              }
              //std::cout<< "Chi2: "<<branch.chi2<<std::endl;
	    } // if 4 clusters

	    // calculate efficiency:
	    if (((seg->GetChi2()<18) && (seg->GetNumClusters()==4)) ||
		((seg->GetChi2()<9) && (seg->GetNumClusters()==3))) {
	      for (int j=4; j<8; j++) {
		double pred = seg->GetPredicted(j);
		hN[j]->Fill(pred);
		if (seg->GetCluster(j) != NULL) hn[j]->Fill(pred);
	      } // next layer
	    } // if good chi2
	  } // if one segment per event
	} // next sector

	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	eventNumber++;

        if (eventNumber >= 200000) break;
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0); // next event
    fclose(infile);
  } // next file

  for (int layer=0; layer<8; layer++) { // efficiency:
    double eff, err;
    for (int bin=0; bin<1000; bin++){
      int N = hN[layer]->GetBinContent(1+bin);
      if (N>10) {
	int n = hn[layer]->GetBinContent(1+bin);
	CalculateEffi(n, N, eff, err);
	hEffi[layer]->SetBinContent(1+bin, eff);
	hEffi[layer]->SetBinError(1+bin, err);
      }
    }
  }
  
  tree->Write();

  hClu->Write();
  double total_clu = hClu->Integral();
  double single_clu = hClu->GetBinContent(4)+hClu->GetBinContent(5);
  std::cout<<"Fraction of single cluster event: "<< single_clu / total_clu <<std::endl;

  hSeg->Write();
  double total_seg = hSeg->Integral();
  double single_seg = hSeg->GetBinContent(2);
  std::cout<<"Fraction of single segment event: "<< single_seg / total_seg <<std::endl;

  hDist->Write();
  hChi2->Write();
  hAng_Chi2->Write();
  hChi2_nSeg->Write();
  hClusterCutflow->Write();
  hEventCutflow->Write();
  hAli5->Fit("gaus", "Q", "", -0.7, 0.7); 
  hAli5->Write();
  hAli6->Fit("gaus", "Q", "", -0.8, 0.6); 
  hAli6->Write();
  // hResCog->Fit("gaus", "Q", "", -1, 1); 
  hResCog->Write();
  // hResCog3->Fit("gaus", "Q", "", -1, 1); 
  hResCog3->Write();
  hResPara->Write();
  for (int layer=4; layer<8; layer++) hOcc[layer]->Write();
  for (int layer=4; layer<8; layer++) hTime[layer]->Write();
  for (int layer=4; layer<8; layer++) hNum[layer]->Write();
  // for (int layer=4; layer<8; layer++) hn[layer]->Write();
  // for (int layer=4; layer<8; layer++) hN[layer]->Write();
  for (int layer=4; layer<8; layer++) hEffi[layer]->Write();

  for (int i=0; i<4; i++) hResIn[i]->Write();
  for (int i=0; i<4; i++) hResEx[i]->Write();


  printf("**************************************************\n");
  printf("*               Cluster Cutflow                  *\n");
  printf("**************************************************\n");
  std::vector<std::string> clusterCuts = {"Total clusters       ",
                                          "Bad channel          ",
                                          "Edge is neighbor     ",
                                          "In beam area         ",
                                          "Peak is not neighbor ",
                                          "Edge is not peak     ",
                                          "No negative amplitude"};
  for(int i_bin = 0; i_bin<clusterCuts.size(); i_bin++)
    printf (" %s \t %i \n",clusterCuts.at(i_bin).c_str(),(int)hClusterCutflow->GetBinContent(i_bin+1));
  printf("**************************************************\n");

  printf("**************************************************\n");
  printf("*                Event Cutflow                   *\n");
  printf("**************************************************\n");
  std::vector<std::string> eventCuts = {"Total events",
                                        "One segment ",
                                        "4/4         ",
                                        "relBCID cut "};
  for(int i_bin = 0; i_bin<eventCuts.size(); i_bin++)
    printf (" %s \t %i \n",eventCuts.at(i_bin).c_str(),(int)hEventCutflow->GetBinContent(i_bin+1));
  printf("**************************************************\n");

  // TCanvas *c_align5 = new TCanvas("c_align5", "c_align5", 800, 600);
  // c_align5->cd();
  // gStyle->SetOptFit(111);
  // gStyle->SetOptStat(false);
  // hAli5->Draw();  
  // c_align5->Update();
  // c_align5->Write();

  // TCanvas *c_align6 = new TCanvas("c_align6", "c_align6", 800, 600);
  // c_align6->cd();
  // gStyle->SetOptFit(111);
  // gStyle->SetOptStat(false);
  // hAli6->Draw();
  // c_align6->Update();
  // c_align6->Write();

  f->Close();
  delete [] buffer;  
  printf("number of events   = %d\n", eventNumber);
  printf("number of segments = %d, %5.3f segments/event.", totSeg, totSeg/(double)eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}

