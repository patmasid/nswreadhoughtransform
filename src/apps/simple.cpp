/** @file simple.cpp
This application fills a root tree with vmm hit data.
It can be used for monitoring or for analysis.
Translated from ProcessorROOTSimple of the NSWDataProcessor package.
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"TFile.h"
#include"TF1.h"
#include"TTree.h"
#include"TProfile.h"
#include"NSWRead/NSWEvent.h"



/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


/** read pedestals and data file, produce root file of single hits 
for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer

  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;
  int dummyCount = 0;

  FILE *infile;

  if(argc <2){
    printf("Usage: simple file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

    
  // header info
  uint32_t m_guid;
  uint32_t m_run_number;
  uint32_t m_lumiblock;
  uint32_t m_sourceId;
  int m_triggerCalibrationKey;

  // packet info:
  std::vector<uint32_t> m_linkId;
  std::vector<uint32_t> m_fragmentSize;
  std::vector<uint32_t> m_level1Id;
  std::vector<uint32_t> m_bcid;
  std::vector<uint32_t> m_orbit;
  std::vector<uint32_t> m_timeset;
  std::vector<uint32_t> m_checksum;
  std::vector<uint32_t> m_nhits;
  std::vector<uint32_t> m_level0Id;
  std::vector<uint32_t> m_missing_data_flags;
  std::vector<uint32_t> m_to_flag;
  std::vector<uint32_t> m_error_flag;
  std::vector<uint32_t> m_alive;
  
  // hit info:
  std::vector<uint32_t> m_vmmid;
  std::vector<uint32_t> m_channel;
  std::vector<uint32_t> m_pdo;
  std::vector<uint32_t> m_tdo;
  std::vector<uint32_t> m_relbcid;
  std::vector<uint32_t> m_nflag;
  std::vector<uint32_t> m_parity;
  
  std::vector<uint32_t> m_layer;    // from linkID
  std::vector<uint32_t> m_radius;
  std::vector<int> m_strip;         // negative values are not on the chamber

  // added later:
  std::vector<uint32_t> m_type;     // electrode type
  std::vector<int> m_sector;
  std::vector<int> m_technology;
  std::vector<uint32_t> m_dateTime;
  
  // packet tree:
  uint32_t p_guid;
  uint32_t p_run_number;
  std::vector<uint32_t> p_level1id;
  uint32_t p_lumiblock;

  std::vector<uint32_t> p_linkId;
  std::vector<uint32_t> p_fragmentSize;
  std::vector<uint32_t> p_level1Id;
  std::vector<uint32_t> p_missingVMM;
  
  std::vector<uint32_t> p_layer;    // from linkID
  std::vector<uint32_t> p_radius;
  std::vector<uint32_t> p_group;
  std::vector<uint32_t> p_index;
  std::vector<std::string> p_label;
  std::vector<uint32_t> p_checksum;
  std::vector<uint32_t> p_type;     // electrode type
  std::vector<int> p_sector;
  std::vector<int> p_technology;
  std::vector<uint32_t> p_dateTime;

  NSWEvent event;
 
  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".simple.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }


  TTree* m_output_tree = new TTree("nsw", "nsw");
  
  //  m_output_tree->Branch("partial_file", &m_partial_file);
  
  m_output_tree->Branch("guid", &m_guid);
  m_output_tree->Branch("run_number", &m_run_number);
  m_output_tree->Branch("lumiblock", &m_lumiblock);
  m_output_tree->Branch("sourceId", &m_sourceId);
  m_output_tree->Branch("triggerCalibrationKey", &m_triggerCalibrationKey);
  
  m_output_tree->Branch("linkId", &m_linkId);
  m_output_tree->Branch("fragmentSize", &m_fragmentSize);
  m_output_tree->Branch("level1Id", &m_level1Id);
  m_output_tree->Branch("bcid", &m_bcid);
  m_output_tree->Branch("orbit", &m_orbit);
  m_output_tree->Branch("timeset", &m_timeset);
  m_output_tree->Branch("checksum", &m_checksum);
  m_output_tree->Branch("nhits", &m_nhits);
  m_output_tree->Branch("level0Id", &m_level0Id);
  m_output_tree->Branch("missing_data_flags", &m_missing_data_flags);
  m_output_tree->Branch("to_flag", &m_to_flag);
  m_output_tree->Branch("error_flag", &m_error_flag);
  m_output_tree->Branch("alive", &m_alive);
  
  m_output_tree->Branch("vmmid", &m_vmmid);
  m_output_tree->Branch("channel", &m_channel);
  m_output_tree->Branch("pdo", &m_pdo);
  m_output_tree->Branch("tdo", &m_tdo);
  m_output_tree->Branch("relbcid", &m_relbcid);
  m_output_tree->Branch("nflag", &m_nflag);
  m_output_tree->Branch("parity", &m_parity);

  m_output_tree->Branch("layer", &m_layer);
  m_output_tree->Branch("radius", &m_radius);
  m_output_tree->Branch("strip", &m_strip);
  
  m_output_tree->Branch("type", &m_type);
  m_output_tree->Branch("sector", &m_sector);
  m_output_tree->Branch("technology", &m_technology);
  m_output_tree->Branch("dateTime", &m_dateTime);
  


  TTree* pac_tree = new TTree("pac", "pac");
  
  pac_tree->Branch("guid", &p_guid);
  pac_tree->Branch("run_number", &p_run_number);
  pac_tree->Branch("l1id", &p_level1id);
  pac_tree->Branch("lumiblock", &p_lumiblock);
  
  pac_tree->Branch("linkId", &p_linkId);
  pac_tree->Branch("fragmentSize", &p_fragmentSize);
  pac_tree->Branch("level1Id", &p_level1Id);
  pac_tree->Branch("missingVMM", &p_missingVMM);
  pac_tree->Branch("layer", &p_layer);
  pac_tree->Branch("radius", &p_radius);
  pac_tree->Branch("group", &p_group);
  pac_tree->Branch("index", &p_index);
  pac_tree->Branch("label", &p_label);
  pac_tree->Branch("checksum", &p_checksum);
  
  pac_tree->Branch("type", &p_type);
  pac_tree->Branch("sector", &p_sector);
  pac_tree->Branch("technology", &p_technology);
  pac_tree->Branch("dateTime", &p_dateTime);

  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	 if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data

	m_guid = 0;
	m_run_number = 0;
	m_lumiblock = 0;
	m_sourceId = 0;
	m_triggerCalibrationKey = -1;

	m_linkId.clear();
	m_fragmentSize.clear();
	m_level1Id.clear();
	m_bcid.clear();
	m_orbit.clear();
	m_timeset.clear();
	m_checksum.clear();
	m_nhits.clear();
	m_level0Id.clear();
	m_missing_data_flags.clear();
	m_to_flag.clear();
	m_error_flag.clear();
	m_vmmid.clear();
	m_channel.clear();
	m_pdo.clear();
	m_tdo.clear();
	m_relbcid.clear();
	m_nflag.clear();
	m_parity.clear();
	m_alive.clear();
	m_strip.clear();
	m_type.clear();

	m_layer.clear();
	m_radius.clear();
	m_type.clear();
	m_sector.clear();
	m_technology.clear();
	m_dateTime.clear();
	
	p_guid = 0;
	p_run_number = 0;
	p_level1id.clear();
	p_lumiblock = 0;
	
	p_linkId.clear();
	p_fragmentSize.clear();
	p_level1Id.clear();
	p_missingVMM.clear();

	p_layer.clear();
	p_radius.clear();
	p_group.clear();
	p_index.clear();
	p_label.clear();
	p_checksum.clear();
	p_type.clear();
	p_sector.clear();
	p_technology.clear();
	p_dateTime.clear();
	


	unsigned int gid = event.GetGlobalID();
	unsigned int lumi_block = event.GetLumiBlock();
	unsigned int dateTime = event.GetDateTime();
	
	for (int i=0; i<event.GetNumSector(); i++){ // loop over sectors
	  NSWSector* sec = event.GetSector(i);
	  if (sec->IsDummy()) {
	    dummyCount++;
	    // continue;
	  }
	  unsigned int source_id = sec->GetSourceID();
	  unsigned int run_no = sec->GetRunNumber();
	  int triggerCalibrationKey = sec->GetTriggerCalibrationKey();

	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac =sec->GetPacket(j);
	    // if (pac->GetEndPoint() != 1) continue; // only strips
	    unsigned int link_id = pac->GetLinkID();
	    unsigned int frag_size = pac->GetLinkBytes();
	    unsigned int l1id = pac->GetL1ID();
	    unsigned int bcid = pac->GetBCID();
	    unsigned int orbit = pac->GetOrbit();
	    unsigned int to = pac->GetTimeout();
	    unsigned int checksum = pac->GetChecksum();
	    unsigned int n_hits = pac->GetLength();
	    unsigned int l0id = pac->GetL0ID();
	    unsigned int missing_data = pac->GetMissing();
	    unsigned int to_flag = pac->GetTimeout();
	    unsigned int e_flag = pac->GetExtended();

	    unsigned int layer = pac->GetLayer();
	    unsigned int radius = pac->GetRadius();
	    unsigned int group = pac->GetChannelGroup();
	    unsigned int index = pac->GetIndex();

	    int sector = pac->GetSector();
	    int technology = pac->GetTechnology();
	    int etype = pac->GetChannelType();
     
	    p_guid = gid;
	    p_run_number = run_no;
	    p_level1id.push_back(l1id);
	    p_lumiblock = lumi_block;
	    
	    p_linkId.push_back(link_id);
	    p_fragmentSize.push_back(frag_size); // packet size in bytes
	    p_level1Id.push_back(l1id);
	    p_missingVMM.push_back(missing_data);
	    
	    p_layer.push_back(layer);
	    p_radius.push_back(radius);
	    p_group.push_back(group);
	    p_index.push_back(index);
	    char boardID[40];
	    pac->GetBoardID(boardID);
	    std::string str(boardID);
	    p_label.push_back(str);

	    p_checksum.push_back(pac->TestChecksum());
	    
	    p_type.push_back(etype);
	    p_sector.push_back(sector);
	    p_technology.push_back(technology);
	    p_dateTime.push_back(dateTime);
	
	    m_alive.push_back(link_id);
	    m_guid = gid;
	    m_run_number = run_no;
	    m_sector.push_back(sector);
	    m_technology.push_back(technology);
	    m_dateTime.push_back(dateTime);
	    for (int k=0; k<pac->GetWidth(); k++){ // loop over channels
	      NSWChannel* chn = pac->GetChannel(k);
	      m_lumiblock = lumi_block;
	      m_sourceId = source_id;
	      m_triggerCalibrationKey = triggerCalibrationKey;

	      m_linkId.push_back(link_id);
	      m_fragmentSize.push_back(frag_size); // packet size in bytes
	      m_level1Id.push_back(l1id);
	      m_bcid.push_back(bcid);
	      m_orbit.push_back(orbit);
	      m_timeset.push_back(to);
	      m_checksum.push_back(checksum); // pac->TestChecksum()
	      m_nhits.push_back(n_hits);
	      m_level0Id.push_back(l0id);
	      m_missing_data_flags.push_back(missing_data);
	      m_to_flag.push_back(to_flag);
	      m_error_flag.push_back(e_flag);
    
	      m_vmmid.push_back(chn->GetVMM());
	      m_channel.push_back(chn->GetVMMChannel());
	      m_pdo.push_back(chn->GetPDO());
	      m_tdo.push_back(chn->GetTDO());
	      m_relbcid.push_back(chn->GetRelBCID());
	      m_nflag.push_back(chn->GetNeighbor());
	      //m_parity.push_back(chn->GetParity()); // read parity
	      m_parity.push_back(chn->CalculateParity(bcid)); // test parity

	      m_layer.push_back(layer);
	      m_radius.push_back(radius);
	      m_strip.push_back(chn->GetDetectorStrip());
	      m_type.push_back(chn->GetChannelType()); // per channel
	    } // next channel
	  } // next packet
	} // next sector
	if (m_linkId.size()>0) m_output_tree->Fill(); // once per event
	if (p_linkId.size()>0) pac_tree->Fill();      // once per packet

	eventNumber++;
	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
      } // try     
      catch (NSWReadException& ex){
	printf ("ReadNSW() encountered an Exception for event %d\n", eventNumber);
	printf("%s\n", ex.what());
	errorCount++;
      }
      //    } while ((size>0)&&(eventNumber<1000)); // next event
    } while (size>0); // next event
    fclose(infile);
  } // next file

  m_output_tree->Write();
  pac_tree->Write();
  f->Close();
  delete [] buffer;  

  printf("\nTotal number of events = %d with %d dummy fragments\n", eventNumber, dummyCount);
  printf ("Encountered %i exceptions\n", errorCount);
}

