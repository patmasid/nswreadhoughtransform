/** @file simNSW.cpp simulates data for the full micromegas sector.

The goal is to smulate realistic data with all relevant effects.
The rate input parameter determines the total occupancy.
The occupancy will depend  on the X srip number, with more hits
at small radii.

Output data includes ROS headers and should be readable by Athena.
Sparsification takes place, not all channels are written out.

Instead of reading an actual pedestal file for the ped and noise values, 
a simulation provides these values. 
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>
#include "NSWLink.h"



/** generates random numbers [1..2^32-1] quickly 
   see Numerical Recipes in C */

#define  RAND    (quickRand=1664525L*quickRand+1013904223L)
#define  DRAND  ((quickRand=1664525L*quickRand+1013904223L)/(double)(UINT_MAX+1.
0))


#define  SEED(x) quickRand=x;
unsigned int quickRand;



/** main program.
@param filename The name of the output bytestream data file.
@param numEvents The number of events to simulate.
@param rate The hit rate in Hz/cm^2.
*/
int main(int argc, char *argv[]){
  unsigned int *eventBuffer;
  NSWChannel[8][8192] chn;

  
  if (argc != 4){
    printf ("usage: simNSW filename numEvents rate.\n");
    printf ("Simulates 1 NSW chamber.\n");
    return 1;
  }
  outfile = fopen(argv[1], "wb");
  if (outfile==NULL) {
    printf ("Can't open output file '%s'.\n", argv[1]);
    return (1);
  }
  SEED (11); // constant seed for reproducable data.

  eventBuffer = new unsigned int [100000]; // big enough???
  
  for (int layer=0; layer<8; layer++){ // initialize channel constants
    for (int i=0; i<8192; i++){
      chn[layer][i].SetPedestal(33);
      chn[layer][i].SetTMin(55);
      chn[layer][i].SetTMax(133);
    }
  }
  /** main loop over events */
  for (int eventNumber = 0; eventNumber < maxEvents; eventNumber++){
    eventBuffer[1] = 0xdd1234dd;   // ROB: 10 words
    eventBuffer[3] = 10;           // ROB header size ????????? fragment size!
    eventBuffer[4] = 0x03000000;   // ROB format version
    eventBuffer[5] = 0;            // source ID
    eventBuffer[6] = 3;            // num status
    eventBuffer[7] = 0;            // status is ok
    eventBuffer[8] = 0;            // status is ok
    eventBuffer[9] = 0;            // status is ok
    eventBuffer[10] = 0;            // NumberOfSpecificHeaderWords

    eventBuffer[11] = 0xee1234ee;   // ROD header marker
    eventBuffer[12] = 9;            // size of header
    eventBuffer[13] = 0x03000100;   // format version 3.0, ROD 1.0
    eventBuffer[14] = 0x006b0001;   // source ID, sector 2
    eventBuffer[15] = 123;    // runNumber
    eventBuffer[16] = eventNumber;  // level 1 ID
    eventBuffer[17] = 0xabc;        // bunch crossing
    eventBuffer[18] = 0;            // trigger type
    eventBuffer[19] = 0;            // detector event type: define it!

    // write status:
    eventBuffer[30] = 0;            // global status
    eventBuffer[31] = 0;            // missing data
    eventBuffer[32] = 0;            // local status and 'presence'
    eventBuffer[33] = 0;            // orbit ID
    
    for (int layer=0; layer<8; layer++){ // create hits
      int ncl = (RAND>>16) % 8;          // up to 8 clusters / layer
      for (int icl=0; icl<ncl; icl++){  
	int wid = (RAND>>16) % 12   ;    // cluster width
	if (wid==0) wid = 1;             // minimum
	int start = (RAND>>16) % 8192;   // first channel
	int stop = start + wid;
	if (stop > 8191) stop = 8191;
	for (int i=start+1; i<stop; i++){      // loop over channels
	  chn[layer][i].SetPDO (100+((RAND>>16) % 300));
	  chn[layer][i].SetTime (120.0/(stop-start)+10);
	}
	chn[layer][start].SetPDO(20);     // neighbor with crosstalk
	chn[layer][start].SetNeighbor(true);
	chn[layer][stop] .SetPDO(20);     // neighbor
	chn[layer][stop] .SetNeighbor(true);
      }
    }
	
    int numPackets = 0;
    NSWPacket * p[1000]; 
    for (int layer=0; layer<8; layer++){ // make packets
      int threshold = 25; // in pdo counts
      for (int board=0; board<16; board++){ // loop over FFME8
	int chan = 0;
	while ((chan<512)&&(chn[layer][chan]->GetPDO()<=threshold)){
	  chan++;
	}
	if (chn[layer][chan]->GetPDO()>threshold) { //start packet
	  int wid = 1;
	  int start = chan;
	  if (start>0) start = start -1; //take left neighbor 
	  while ((chan<512)&&(chn[layer][chan]->GetPDO()>threshold)){
	    chan++;
	  }
	  int stop = chan;
	  if (stop>511) stop = 511; // don't add neighbor outside range
	  p[numPackets] = new NSWPacket(chn[layer][start]->GetUpperID(), stop-start);
	  for (int i=start; i<=stop; i++){
	    p[numPackets]->AddChannel (chn[layer][i]);
	  }
	  
	  
    }
    // write NSW MM data table
    eventBuffer[34] = (1<<16) | (p->GetNumBytes()+3)/4;    // MM hit data words total
    eventBuffer[35] = (5<<16) | 0;           // MM trigger input
    eventBuffer[36] = (6<<16) | 0;           // MM trigger output
    

  }
  delete [] eventBuffer;
    
