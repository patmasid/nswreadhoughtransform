// test the reading of NSWEvent class objects
// 
#include <stdio.h>


#include "NSWRead/NSWEvent.h"

#define maxSize 21000000/4 // 4000*32   // for event buffer

int main(int argc, char *argv[]){

  FILE* infile;
  NSWEvent event;
  int eventCount = 0;
  int errorCount = 0;
  int size ; // event size
  //unsigned int buffer[maxSize];
  unsigned int * buffer;
  
  if (argc==1) {
    printf ("usage: testNSWEvent infile\n");
    printf ("dumps NSW data to screen.\n");
    return 1;
  }

  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  infile = fopen (argv[1], "rb");
  if (!infile) {
    printf ("Cannot open '%s'\n", argv[1]);
    return 1;
  }



  do{ //event loop
    try{
      size = event.ReadATLAS(infile, buffer, maxSize);
    }
    catch (NSWReadException& ex){
      printf ("NSWRead: %s\n", ex.what());
      break;
    }

    try {
      event.ReadNSW(buffer, size);
      event.Print();
    } 
    catch (NSWReadException& ex){
      printf ("ReadNSWException: ");
      printf("%s\n", ex.what());
      errorCount++;
    }
    eventCount++;
  } while (size>0); // next event

  printf ("Read %i events\n", eventCount);
  printf ("Encountered %i exceptions\n", errorCount);
  fclose (infile);
  delete [] buffer;
}
