// reads NSWEvent data and returns one event
// 
#include <stdio.h>
#include <vector>
#include <bits/stdc++.h> 
#include "NSWRead/NSWEvent.h"

#define maxSize 21000000/4  // 4000*32   // for event buffer




int main(int argc, char *argv[]){

  FILE* infile;
  FILE* outfile;
  NSWEvent event;
  int eventCount = 0;
  int size ; // event size
  unsigned int * buffer;
  char fil[300], fils[300];


 if (argc<3) {
    printf ("usage: nswgetL1ID infile L1ID\n");
    printf ("reads NSWEvent data and puts an event into a new file.\n");
    printf ("L1ID can be decimal or 0x hex number\n");
    return 1;
  }

  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  infile = fopen (argv[1], "rb");
  if (!infile) {
    printf ("Cannot open '%s'\n", argv[1]);
    return 1;
  }

  int target;
  if ((argv[2][0]=='0')&&((argv[2][1]=='x')||(argv[2][1]=='X'))) { // hex
    sscanf (&argv[2][2], "%x", &target);
  } else { // dec
    sscanf (argv[2], "%d", &target);
  }
      
  printf ("Searching for L1ID 0x%08X = %d...\n\n", target, target);
  unsigned int l1id = 0;
  char ext[40];
  sprintf (ext, ".L1ID_%08X.data", target);
  strcat(fils, ext);
  
  do{ //event loop
    try{
      size = event.ReadATLAS(infile, buffer, maxSize);
    }
    catch (NSWReadException& ex){
       if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
      break;
    }
    try{ 
      event.ReadNSW(buffer, size);           // read the data
      l1id = event.GetL1ID();
    } // try     
    catch (NSWReadException& ex){
      if (ex.GetSeverity() > SEV_INFO) {
	printf ("ReadNSWException: ");
	printf("%s\n", ex.what());
      }
    }
    if (eventCount%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d, L1ID 0x%08X\u001b[0m\n", eventCount, l1id);

    eventCount++;
  } while ((size>0)&&(l1id!=target)); // next event

  fclose (infile);
  if (l1id == target){
    printf ("Found event %d with %d words.\n", eventCount-1, size);
    /***************************
    int n = size;
    if (n>8*20) n = 8*20;
    for (int i=0; i<n; i++){
      if (i%8 == 0) printf ("\n");
      printf ("%08X ", buffer[i]);
    }
    printf ("\n");
    *************************/
    event.Print();
    
    outfile = fopen (fils, "wb");
    if (!outfile) {
      printf ("Cannot open '%s'\n", fils);
      return 1;
    }
    fwrite (buffer, 4, size, outfile); // write event to file
    fclose (outfile);
  } else {
    printf ("Not found.\n");
  }
    
  delete [] buffer;
}
