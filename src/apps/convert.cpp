// convert binary swROD input to ATLAS fragments

#include<stdio.h>
#include "NSWRead/NSWPacket.h"

int main(int argc, char *argv[]) {
  FILE  *infile, *outfile;
  if (argc<3) {
    printf ("usage: convert infile outfile\n");
    return(1);
  }
    
  infile = fopen(argv[1], "r");
  if (infile==NULL) {
    printf ("Can't open input file '%s'.\n", argv[1]);
    return (1);
  }
  
  outfile = fopen(argv[2], "wb");
  if (outfile==NULL) {
    printf ("Can't open output file '%s'.\n", argv[2]);
    return (1);
  }

  NSWChannelID upperID; 
  upperID.SetChannelID (0,  1, 3, 1, 1, 3, 5, 0, 0, 0);


  unsigned int linkword[2];
  unsigned char data[1000]; // vmm data input format
  unsigned int out[1000];   // output in ATLAS format
  while (2==fread(linkword, 4, 2, infile)){
    //printf ("linkwords %08X %08X\n", linkword[0], linkword[1]);
    int numRead = fread(data, 1, linkword[0]-8, infile);
    //printf ("read %d of %d bytes.\n", numRead, linkword[0]-8);
    int wid = (linkword[0]-8)/4; // assumes hits with TDC
    if (wid<0) wid = 0; // Null header, no channel hits
    NSWPacket* p = new NSWPacket(upperID.GetChannelID(), wid);
    p->SetLinkID(linkword[1]);
    p->SetLinkBytes(linkword[0]);
    p->ReadBytes(data);
    p->Print();
    //p->Test(1);
    if (p->GetWidth()==0)continue;

    int runNumber = 123;
    out[0]  = 0xcc1234cc;   // ROS: 11 words
    out[1]  = 0;            // ROS fragment size, fill later
    out[2]  = 11;           // ROS header size
    out[3]  = 0x03000000;   // ROS format version
    out[4]  = 0x006e0001;   // source ID, sector 2 (zero based)
    out[5]  = 1;            // num status
    out[6]  = 0;            // status is ok
    out[7]  = 3;            // NumberOfSpecificHeaderWords
    out[8]  = runNumber;    // run number
    out[9]  = 0;            // ExtendedLvl1Id
    out[10] = p->GetBCID(); // bunch crossing

    out[11] = 0xdd1234dd;   // ROB: 10 words
    out[12] = 0;            // ROB fragment size, fill later
    out[13] = 10;           // ROB header size
    out[14] = 0x03000000;   // ROB format version
    out[15] = 0x006e0001;   // source ID, sector 2 (zero based)
    out[16] = 3;            // num status
    out[17] = 0;            // status is ok
    out[18] = 0;            // status is ok
    out[19] = 0;            // status is ok
    out[20] = 0;            // NumberOfSpecificHeaderWords

    out[21] = 0xee1234ee;   // ROD header marker
    out[22] = 9;            // size of header
    out[23] = 0x03000100;   // format version 3.0, ROD 1.0
    out[24] = 0x006e0001;   // source ID, sector 2 (zero based)
    out[25] = runNumber;    // runNumber
    out[26] = p->GetL1ID(); // level 1 ID
    out[27] = p->GetBCID(); // bunch crossing
    out[28] = 0;            // trigger type
    out[29] = 0;            // detector event type: not defined yet

    out[30] = 0;            // global status
    out[31] = 0;            // missing data
    out[32] = 0;            // local status and 'presence'
    out[33] = p->GetOrbit();  // orbit ID
    
    out[34] = (1<<16) | 2+(p->GetNumBytes()+3)/4;    // MM hit data words
    out[35] = (5<<16) | 0;           // MM trigger input
    out[36] = (6<<16) | 0;           // MM trigger output
    
    int written = p->Write(&(out[37])); 

    out[37+written] = 4;           // Trailer: number of status words
    out[38+written] = 3+4+written; // number of data words: table, status, pac.
    out[39+written] = 0;           // status position before data

    int total = 30; // headers
    total += written;
    //total += (p->GetNumBytes()+3)/4;
    total += 4+1+6; // for status and trailer

    out[1] = total; // ROS fragment size
    out[12] = total-11; // ROB fragment size

    fwrite(out, 4, 40+written, outfile);
    delete p;
  }
  fclose(infile);
  fclose(outfile);
  return 0;
}
