/** @file stgcGnam.cpp
This application fills a root tree with GNAM monitoring histograms.
It can be used for monitoring, or for testing Gnam modifications.

Reads nsw data file and produces monitoring histograms for sTGC.

@see mmGnam for monitoring micromegas
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "TH2.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TProfile.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"


/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32

int thissector = 12; // only one sector for now

/** read pedestals and data file, produce root file of single hits 
for monitoring. */
int main(int argc, char *argv[]){
unsigned int * buffer; // for ATLAS event buffer
char fil[300], fils[300];
int eventNumber, errorCount =0 ; int lumiBlock = 0;
FILE *infile;
if(argc <2){
  printf("Usage: gnam file1.data [file2.data]...\n");
  exit(0);
}
buffer = new unsigned int [maxSize];
if (buffer == NULL) {
  printf ("Cannot allocate event buffer.\n");
  return 1;
}
eventNumber = 0;
NSWEvent event;
NSWSector* sec;
NSWClusterList clusterList;
strcpy(fils,argv[1]);
fils[(strlen(argv[1])-5)] = 0;
strcpy(fil,fils);
TFile *f = new TFile(strcat(fils,".stgcGnam.root"), "recreate");//output file
if (!f) {
  printf ("Cannot open root file!\n");
  return -1;
}
//NSW Histograms
TH1F * procTimeHist;
TH1F * BCIDDist, *level1IDDist;
TH1F * relDist, *pdoDist, *tdoDist; //sum from all channels
TH1F * numSectorDist, *numPacketDist, *packetWidthDist;
TH1F * evtPerLBDist;
TH1F * occDist[64]; // occupancy per link
TH2F * ampDist[64]; // amplitude 2d
TH2F * timeDist[64]; // time 2d
TH1F * widthDist[64]; // number of channels
TH2F * yieldEventDist;
TH1F * yieldDist; // percentage of recorded packets
TH1F * occLinkDist;
TH2F * occLinkChnDist; // occupancy per link
TH1F * pdoDist_chn[3]; // per channel type
TH1F * tdoDist_chn[3]; // per channel type
TH1F * occDist_chn[8][3];
TH2F * timeDist_chn[8][3];
TH2F * pulserDist;
TH1F * lengthErrorDist; 
TH1F * missingDist;
TH2F * missingVMMDist, * missingeLinksperEvent, *missingVMMDistperEvent;
TH1F * clusterOccDist[8], *clusterWidthDist[8], *numClusterDist;

BCIDDist = new   TH1F("/SHIFT/Event/BCID", "BCID Distribution", 1000, 0.0,  4000.0);
level1IDDist = new   TH1F("/SHIFT/Event/Level1ID", "Level1 ID Distribution", 1000, 0.0, 10000.0);
relDist = new   TH1F("/SHIFT/Event/relBCIDDist", "RelBCID Distribution", 8, 0, 8);
pdoDist = new   TH1F("/SHIFT/Event/pdoDist", "PDO Distribution;PDO", 256, 0.0, 1024.0);
tdoDist = new   TH1F("/SHIFT/Event/tdoDist", "TDO Distribution;TDO", 256, 0.0, 256.0);
  
pdoDist_chn[0] = new TH1F("/SHIFT/Event/pdoDist_Pads", "PDO Distribution for Pads;PDO_Pads", 256, 0.0, 1024.0);
pdoDist_chn[1] = new TH1F("/SHIFT/Event/pdoDist_Strips", "PDO Distribution for Strips;PDO_Strips", 256, 0.0, 1024.0);
pdoDist_chn[2] = new TH1F("/SHIFT/Event/pdoDist_Wires", "PDO Distribution for Wires;PDO_Wires", 256, 0.0, 1024.0);
  
tdoDist_chn[0] = new TH1F("/SHIFT/Event/tdoDist_Pads", "TDO Distribution for Pads;TDO_Pads", 256, 0.0, 256.0);
tdoDist_chn[1] = new TH1F("/SHIFT/Event/tdoDist_Strips", "TDO Distribution for Strips;TDO_Strips", 256, 0.0, 256.0);
tdoDist_chn[2] = new   TH1F("/SHIFT/Event/tdoDist_Wires", "TDO Distribution for Wires;TDO_Wires", 256, 0.0, 256.0);
  
numSectorDist = new   TH1F("/SHIFT/Event/numSectorDist", "numSector Distribution", 40, 0.0, 40.0);
numPacketDist = new   TH1F("/SHIFT/Event/numPacketDist", "numPacket Distribution", 100, 0.0, 100.0);
packetWidthDist = new   TH1F("/SHIFT/Event/packetWidthDist", "Packet width Distribution;channels", 513, 0.0, 513.0);
evtPerLBDist = new   TH1F("/SHIFT/Event/evtPerLBDist", "Events per LB;LB", 100, 0.0, 1000.0);
occLinkDist = new   TH1F("/SHIFT/Event/occLinkDist", "eLink packet occupancy;elink number", 64, 0.0, 64.0);
occLinkChnDist = new   TH2F("/SHIFT/Event/occLinkChnDist", "eLink channel occupancy;elink number;packet width", 64, 0.0, 64.0, 200, 0, 200);
yieldEventDist = new   TH2F("/SHIFT/Event/yieldEventDist", "eLink yield vs L1ID", 4000, 0.0, 4000.0, 64, 0.0, 64.0);
yieldDist = new   TH1F("/SHIFT/Event/yieldDist", "eLink yield;elink index", 64, 0.0, 64.0);
pulserDist = new   TH2F("/SHIFT/pulser/pulserDist", "Occupancy from pulser;PCB;layer", 64, 0.0, 64.0, 8, 0, 8);
lengthErrorDist = new   TH1F("/SHIFT/Errors/lengthErrorDist", "ROC trailer length errors vs eLink index;elink index", 64, 0.0, 64.0);
missingDist = new   TH1F("/SHIFT/Errors/missingDist", "missing VMM in ROC trailer vs eLink index;elink index", 64, 0.0, 64.0);
missingeLinksperEvent = new TH2F("/SHIFT/Errors/missingeLinksperEvent", "missing elinks per event; Event No.; elink index", 20000, 0.0, 20000.0, 64, 0.0, 64.0);
missingVMMDistperEvent = new TH2F("/SHIFT/Errors/missingVMMDistperEvent", "missing VMMs per event; Event No.; missing VMM", 20000, 0.0, 20000.0, 64, 0.0, 64.0);
missingVMMDist = new   TH2F("/SHIFT/Errors/missingVMMDist", "missing VMM in ROC trailer vs eLink index;elink index", 64, 0.0, 64.0, 256, 0.0, 256.0);
numClusterDist = new   TH1F("/SHIFT/clusters/numClustersDist", "Number of clusters per sector", 100, 0.0, 100.0);

char name[80], title[80], label[40];
NSWChannelID id; 
id.SetTechnologyOld(0); // MM = 1, sTGC = 0
//id.SetSector(sectors[0]); // only one sector for now
id.SetRawSector(thissector-1); // only one sector for now
id.SetEtaOld(thissector>0?0:1); // 0: eta=+1: endcap A, 1: eta=-1, endcap C
id.SetDataTypeOld(0); // 0: L1A, 1: config, 2: monitor
id.SetEndPointOld(1); // 0: pad, 1: strip,  2: Trig. Proc, 3: Pad trig
for (int layer=0; layer<8; layer++){
  for (unsigned int i=0; i<8; i++){
    int r = (i>3)? (i-2)/2 : 0;   // radius
    int g = (i>3)? 0 : i%2;       // group
    int e = (i>3)? i%2 : (i/2)%2; // endpoint
    id.SetRadius(r);
    id.SetChannelGroup(g);
    id.SetEndPointOld(e);
    id.SetLayer(layer);
    id.GetBoardID(label);
    //printf ("i = %2d, r = %2d, g = %d, layer = %d, label = '%s'\n", i, r, g, layer, label);      
    sprintf (name, "/SHIFT/eLinks/occ_%s", label);
    sprintf (title, "Occupancy of eLink %s;chn", label);
    occDist[i+8*layer] = new   TH1F(name, title, 512, 0.0, 512.0);
      
    sprintf (name, "/SHIFT/eLinks/time_%s", label);
    sprintf (title, "Time of eLink %s;chn;ns", label);
    timeDist[i+8*layer] = new   TH2F(name, title, 512, 0.0, 512.0, 250, -25.0, 225.0);

    sprintf (name, "/SHIFT/eLinks/amp_%s", label);
    sprintf (title, "Amplitude of eLink %s;chn;ADC", label);
    ampDist[i+8*layer] = new   TH2F(name, title, 512, 0.0, 512.0, 256, 0.0, 1024.0);

    sprintf (name, "/SHIFT/eLinks/width_%s", label);
    sprintf (title, "Number of channels for eLink %s;chn", label);
    widthDist[i+8*layer] = new   TH1F(name, title, 513, 0.0, 513.0);
  } // next elink 

  sprintf (name, "/SHIFT/layers/layer_%d/occ_Pads", layer);
  sprintf (title, "Occupancy of Pads for layer  %d;chn", layer);
  occDist_chn[layer][0]= new TH1F(name, title, 512, 0.0, 512.0);
  sprintf (name, "/SHIFT/layers/layer_%d/occ_Strips", layer);
  sprintf (title, "Occupancy of Strips for layer  %d;chn", layer);
  occDist_chn[layer][1] = new TH1F(name, title, 1127, 0.0, 1127.0);
  sprintf (name, "/SHIFT/layers/layer_%d/occ_Wires", layer);
  sprintf (title, "Occupancy of Wires for layer  %d;chn", layer);
  occDist_chn[layer][2] = new TH1F(name, title, 100, 0.0, 100.0);
   
  sprintf (name, "/SHIFT/layers/layer_%d/time_Pads", layer);
  sprintf (title, "Peak Time Dist. of Pads for layer  %d;chn;ns", layer);
  timeDist_chn[layer][0] = new   TH2F(name, title, 512, 0.0, 512.0, 125, 0.0, 125.0);
  sprintf (name, "/SHIFT/layers/layer_%d/time_Strips", layer);
  sprintf (title, "Peak Time Dist. of Strips for layer  %d;chn;ns", layer);
  timeDist_chn[layer][1] = new   TH2F(name, title, 600, 0.0, 600.0, 125, 0.0, 125.0);
  sprintf (name, "/SHIFT/layers/layer_%d/time_Wires", layer);
  sprintf (title, "Peak Time Dist. of Wires for layer  %d;chn;ns", layer);
  timeDist_chn[layer][2] = new   TH2F(name, title, 100, 0.0, 100.0, 125, 0.0, 125.0);
    
  sprintf (name, "/SHIFT/clusters/clusterOcc_%d", layer);
  sprintf (title, "Cluster position in layer %d;strip", layer);
  clusterOccDist[layer] = new   TH1F(name, title, 8192, 0.0, 8192.0);
  sprintf (name, "/SHIFT/clusters/clusterWidth_%d", layer);
  sprintf (title, "Cluster width in layer %d;strips", layer);
  clusterWidthDist[layer] = new   TH1F(name, title, 200, 0.0, 200.0);
} // next layer

for (int iFile=1; iFile<argc; iFile++){
  printf ("opening file '%s'.\n\n", argv[iFile]);
  infile = fopen(argv[iFile],"rb");//open data file

  if (infile==NULL) { //error if file incorrect or DNE
    printf("Can't open input file '%s'.\n",argv[iFile]);
    exit(-1);
  }

  // start processing events:
  int size; // actual size of the ATLAS event
  do{ //event loop
    try{
      size = event.ReadATLAS(infile, buffer, maxSize);
    }
    catch (NSWReadException& ex){
      if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
      break;
    }
    try{
    event.ReadNSW(buffer, size); // read the data

    //Filling NSW histograms
    BCIDDist->Fill(event.GetBCID());
    level1IDDist->Fill (event.GetL1ID());
    evtPerLBDist->Fill (event.GetLumiBlock());
	  //evtHitNumDist->Fill (event.GetEvtHitNum());
    numSectorDist->Fill (event.GetNumSector()); 

    for (int i=0; i<event.GetNumSector(); i++){
      NSWSector* sec = event.GetSector(i);
      if (sec == NULL){
        printf ("ERROR: sector %d is null!\n", i);
        continue;
      }
      numPacketDist->Fill (sec->GetNumPacket());
      for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
        NSWPacket *pac = sec->GetPacket(j);
        if (pac == NULL){
          printf ("ERROR: packet %d of sector %d is null!\n", j, i);
          continue;
        }
        /*if (pac->GetMissing() != 0){
         printf ("RED Flag: elink %s has missing VMM errors \n", label); // elink with missing VMM
         continue;
        }*/ 
        packetWidthDist->Fill (pac->GetWidth());
        int r = pac->GetRadius();
        int g = pac->GetChannelGroup();
	      int ind = pac->GetIndex();
	      occLinkDist->Fill(ind+0.1);
	      occLinkChnDist->Fill(ind+0.1, pac->GetWidth());
	      widthDist[ind]->Fill(pac->GetWidth());
	      yieldEventDist->Fill(event.GetL1ID(), ind+0.1);
	      yieldDist->Fill(ind+0.1);
	      
        if (pac->GetWidth() != pac->GetLength()){ // bad ROC trailer
          lengthErrorDist->Fill(ind);
        }

        if (pac->GetMissing() != 0){ // missing VMM
          missingDist->Fill(ind);
	        missingVMMDist->Fill(ind, pac->GetMissing());
          missingeLinksperEvent->Fill(eventNumber, ind);
          missingVMMDistperEvent->Fill(eventNumber, pac->GetMissing());
        }

        for (int k=0; k<pac->GetWidth(); k++){
          NSWChannel *chn = pac->GetChannel(k);
          if (chn == NULL){
            printf ("ERROR: channel %d of packet %d is null!\n", k, pac->GetLinkID());
            continue;
          }
          pdoDist->Fill (chn->GetPDO());
	        tdoDist->Fill (chn->GetTDO());
	        relDist->Fill (chn->GetRelBCID());
	        occDist[ind]->Fill(chn->GetVMM()*64+chn->GetVMMChannel());
	        timeDist[ind]->Fill(chn->GetVMM()*64+chn->GetVMMChannel(), chn->GetPeakingTime());
	        ampDist[ind]->Fill(chn->GetVMM()*64+chn->GetVMMChannel(), chn->GetAmplitude());
	        int layer = pac->GetLayer();
	        if ((layer < 0) || (layer > 7)){
            printf ("ERROR: layer %d is out of range!\n", layer);
	      	  continue;
	        }
          // channel type PAD=0, STRIP=1, WIRE=2
          // QS1 strips: 1 .. 406, QS2: 1 .. 365, QS3: 1 .. 307, 1078 in total
          // QL1 strips: 1 .. 408, QL2: 1 .. 366, QL3: 1 .. 353, 1127 in total
          if (chn->GetChannelType() == 0) { 
            pdoDist_chn[0]->Fill(chn->GetPDO());
            tdoDist_chn[0]->Fill(chn->GetTDO());
            occDist_chn[layer][0]->Fill(chn->GetDetectorStrip());
            timeDist_chn[layer][0]->Fill(chn->GetDetectorStrip(), chn->GetPeakingTime());
          }
          if (chn->GetChannelType() == 1) {
            pdoDist_chn[1]->Fill(chn->GetPDO());
            tdoDist_chn[1]->Fill(chn->GetTDO());
            occDist_chn[layer][1]->Fill(chn->GetDetectorStrip());
            timeDist_chn[layer][1]->Fill(chn->GetDetectorStrip(), chn->GetPeakingTime());
          }
          if (chn->GetChannelType() == 2) {
            pdoDist_chn[2]->Fill(chn->GetPDO());
            tdoDist_chn[2]->Fill(chn->GetTDO());
            occDist_chn[layer][2]->Fill(chn->GetDetectorStrip());
            timeDist_chn[layer][2]->Fill(chn->GetDetectorStrip(), chn->GetPeakingTime());
          }
          pulserDist->Fill(chn->GetVMM()/4+chn->GetRadius()*2+0.1, layer+0.1);
        } // next chn
	    } // next packet

      clusterList.Clear();
	    clusterList.Fill(sec);

      int totClusters = 0;
      for (int layer =0; layer<8; layer++) {
        totClusters += clusterList.GetNumClusters(layer);
        for (int i=0; i<clusterList.GetNumClusters(layer); i++){
          NSWCluster*  clu = clusterList.GetCluster(layer, i);
          clusterOccDist[layer]->Fill(clu->GetCOG());
          clusterWidthDist[layer]->Fill(clu->GetWidth());
        }
        numClusterDist->Fill(totClusters);
      }
    } // next sector

    eventNumber++;
    if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
  } // try     
  catch (NSWReadException& ex){
    printf ("ReadNSW() encountered an Exception for event %d\n", eventNumber);
	  printf("%s\n", ex.what());
	  errorCount++;
  }
  }
  while (size>0);// next event
  fclose(infile);
  } // next file

  f->Write();
  f->Close();
  delete [] buffer;  
  printf("\nTotal number of events = %d\n", eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}

