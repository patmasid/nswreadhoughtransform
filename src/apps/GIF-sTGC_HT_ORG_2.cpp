/** @file nswseg4.cpp
This application fills a root tree with segment reconstruction results.
It can be used for monitoring, or for looking at hit reconstruction
results. The tree variables are stored in moni_t.

This version uses only 4 layers, and can run on GIF data from sTGC.

Reads nsw data file and produces monitoring histograms based on segments.
The tree is filled after cluster finding and there is one entry per segment.

@see moni.cpp for monitoring packet data
 
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <exception>
#include "TH2.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "TLatex.h"
#include "TStyle.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWSegmentList.h"
#include "NSWRead/NSWCalibration.h"
#include "NSWRead/NSWHoughTx.h"

/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


void linfit(double x[], double y[], int ndata, double sig[], int mwt, double *a, double *b, double *siga, double *sigb, double *chi2);

/** Draw a simple event display */
void DrawDisplay (NSWSector* sec, int gifNumber){
  TH1F *hamp[8], *htime[8];
  TH1F *hwire[3][8];
  TH1F *hpad[3][8];
  char name[40], title[80];
  char gifName[80];
  int l1id;
  int imax;
  if (sec->GetTechnology()==1){ // 1 = MM, 0=sTGC */
    imax = 8196;
  } else {
    imax = 400; //1200;
  }
  for (int layer=0; layer<8; layer++){
    sprintf (name, "hamp%d", layer);
    sprintf (title, "PDO layer %d;channel", layer);
    hamp[layer] = new TH1F(name, title, imax, 0, imax);  
    sprintf (name, "htime%d", layer);
    sprintf (title, "Time layer %d;channel", layer);
    htime[layer] = new TH1F(name, title, imax, 0, imax);
  }
  for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(i);
    l1id = pac->GetL1ID();
    for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
      NSWChannel* chn = pac->GetChannel(j);
      if (chn->GetChannelType() == NSWChannel::STRIP){
	int strip = chn->GetDetectorStrip();
	int layer = chn->GetLayer();
	hamp[layer]->Fill(strip, chn->GetPDO());
	htime[layer]->Fill(strip, chn->GetPeakingTime());
      }
    }
  }
  TCanvas* can = new TCanvas ("can", "Event display", 1600, 1200);  
  can->Divide(1, 8);
  //gStyle->SetOptStat(0); 
  for (int layer=0; layer<8; layer++){
    can->cd(layer+1);
    hamp[layer]->GetXaxis()->SetLabelSize(0.1);
    hamp[layer]->GetYaxis()->SetLabelSize(0.1);
    hamp[layer]->GetYaxis()->SetRangeUser(0, 1000);
    hamp[layer]->SetFillColor(2);
    hamp[layer]->Draw("HIST");
    htime[layer]->SetMarkerColor(2);
    htime[layer]->SetMarkerStyle(7);
    htime[layer]->Draw("psame");
  }
  can->cd(0);
  TPaveText t(0.4,0.94,0.6,0.99,"NDC");
  char line[40];
  sprintf (line, "Strips L1ID = %d", l1id);
  t.SetTextSizePixels(8);
  t.AddText(line);
  t.SetFillColor(0);
  t.Draw();

  sprintf (gifName, "ed%04ds.gif", gifNumber);
  can->SaveAs(gifName);

  // wires:
  if (sec->GetTechnology()==0){ //  1 = MM, 0=sTGC
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++){
	sprintf (name, "hwire%d%d", q, layer);
	sprintf (title, "Wire PDO Q%d layer %d;wire", q, layer);
	int range = 20+10*q;
	hwire[q][layer] = new TH1F(name, title, range, 0, range);  
      }
    }
    for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
      NSWPacket *pac =sec->GetPacket(i);
      l1id = pac->GetL1ID();
      for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
	NSWChannel* chn = pac->GetChannel(j);
	if (chn->GetChannelType() == NSWChannel::WIRE){
	  int wire = chn->GetDetectorStrip();
	  int layer = chn->GetLayer();
	  int q = chn->GetRadius();
	  hwire[q][layer]->Fill(wire, chn->GetPDO());
	}
      }
    }
    can->Clear();
    can->Divide(3, 8);
      for (int layer=0; layer<8; layer++){
	for (int q=0; q<3; q++){
	  can->cd(q+3*layer+1);
	  hwire[q][layer]->GetXaxis()->SetLabelSize(0.1);
	  hwire[q][layer]->GetYaxis()->SetLabelSize(0.1);
	  hwire[q][layer]->GetYaxis()->SetRangeUser(0, 1000);
	  hwire[q][layer]->SetFillColor(2);
	  hwire[q][layer]->Draw("HIST");
	}
      }
    can->cd(0);
    TPaveText t(0.4,0.94,0.6,0.99,"NDC");
    char line[40];
    sprintf (line, "Wires L1ID = %d", l1id);
    t.AddText(line);
    t.SetFillColor(0);
    t.Draw();
    sprintf (gifName, "ed%04dw.gif", gifNumber);
    can->SaveAs(gifName);

    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++) delete hwire[q][layer];
    }
    
    // pads:
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++){
	sprintf (name, "hpad%d%d", q, layer);
	sprintf (title, "Pads PDO Q%d layer %d;pad", q, layer);
	int range = 120;
	hpad[q][layer] = new TH1F(name, title, range, 0, range);  
      }
    }
    for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
      NSWPacket *pac =sec->GetPacket(i);
      l1id = pac->GetL1ID();
      for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
	NSWChannel* chn = pac->GetChannel(j);
	if (chn->GetChannelType() == NSWChannel::PAD){
	  int pad = chn->GetDetectorStrip();
	  int layer = chn->GetLayer();
	  int q = chn->GetRadius();
	  hpad[q][layer]->Fill(pad, chn->GetPDO());
	}
      }
    }
    can->Clear();
    can->Divide(3, 8);
      for (int layer=0; layer<8; layer++){
	for (int q=0; q<3; q++){
	  can->cd(q+3*layer+1);
	  hpad[q][layer]->GetXaxis()->SetLabelSize(0.1);
	  hpad[q][layer]->GetYaxis()->SetLabelSize(0.1);
	  hpad[q][layer]->GetYaxis()->SetRangeUser(0, 1000);
	  hpad[q][layer]->SetFillColor(2);
	  hpad[q][layer]->Draw("HIST");
	}
      }
    can->cd(0);
    TPaveText t2(0.4,0.94,0.6,0.99,"NDC");
    //char line[40];
    sprintf (line, "Pads L1ID = %d", l1id);
    t2.AddText(line);
    t2.SetFillColor(0);
    t2.Draw();
    sprintf (gifName, "ed%04dp.gif", gifNumber);
    can->SaveAs(gifName);
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++) delete hpad[q][layer];
    }
  } //if sTGC
    
  delete can;
  for (int layer=0; layer<8; layer++){
    delete  htime[layer];
    delete  hamp[layer];
  }
}

/** calculates the efficiency and the error on the efficiency. 
  @param numer the numerator of the ratio
  @param denom the denominator of the ratio
  @param effic the calculated efficiency
  @param error the calculated error */
void CalculateEffi(int numer, int denom, double &effi, double &error) {
  effi = -1;
  error = 0;

  // Check for divide by zero:
  if (denom <= 0) return;

  effi = numer / (double)denom;

  // Error calculation from arXiv:physics/0701199v1
  double dnum = (double)numer;
  double dden = (double)denom;

  double disc = ((dnum+1)*(dnum+2))/((dden+2)*(dden+3))
    - pow(dnum+1,2)/pow(dden+2,2);
  if (disc >=0) error = sqrt(disc);

  /**********************
  // Error calculation from P. Avery.
  double disc = (1.0/denom * (effic + 1/denom)* (1.0-effic + 1/denom)
		 / (pow(1+2.0/denom,2.0)*(1.0+3.0/denom)));
  if (disc >=0) {
    error = sqrt(disc);

    // For effic = 0 or 1, add to the error the value of the unbiased
    // estimate of Y1/Y2. This will give a better estimate of the conf.
    // interval.
    if (effic==1.0 || effic==0.0) {
      error = (1.0/denom) / (1.0+2.0/denom) + error;
    }
  }
  ************************/
}



/** read  data file, produce root file for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer
  char fil[300], fils[300];
  int eventNumber = 0;
  int NrejEvnts = 0;
  int NrejEvnts_noFilledHT = 0;
  int NrejEvnts_noHit1perlayer = 0;
  int NrejEvnts_noSmallAngle = 0;
  int NrejEvnts_noClusterHT = 0;
  int NrejEvnts_noGoodClusterHT = 0;

  int NrejEvnts_noHitsLayers_ge2 = 0;
  int NselEvnts_HitsLayers_ge3 = 0;

  int NrejEvnts_noClusLayers_ge2 = 0;
  int NselEvnts_ClusLayers_ge3 = 0;

  int NrejEvnts_noSeg = 0;
  int NselEvnts_Seg = 0;

  int errorCount = 0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  FILE *infile;

  if(argc <6){
    printf("Usage: GIF-sTGC_HT pdoCalib.txt bad_channels.txt num_strips_percell sng(0/1) file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  NSWCalibration cal;
  if (cal.ReadPDO(argv[1]) != 1){
    printf ("Cannot read calibration file.\n");
    return 1;
  }
  //cal.Print();

  std::vector< std::vector <std::vector<int> >> v_bad_channels_strip;
  v_bad_channels_strip.resize(3);
  for(int iQ=0; iQ<3; iQ++){
    v_bad_channels_strip[iQ].resize(8);
  }

  //Converting the bad channels to vector
  std::string line;
  std::ifstream myfile(argv[2]);
  if ( myfile.is_open() ) {
    while ( std::getline ( myfile,line ) ) {
      std::string data_str =  line;
      std::istringstream iss(line);

      int radius = -1;
      int layer = -1;
      std::string det_type = "";
      int det_chnl = -1;

      if ( !(iss
             >> radius
             >> layer
             >> det_type
             >> det_chnl) ){
	std::cout << "Error in reading bad channels file!" << std::endl;
        return 1;
      }

      if(det_type == "strip"){
        v_bad_channels_strip[radius][layer].push_back(det_chnl);
      }

    }
  }

  std::istringstream iss_Nstrips(argv[3]);
  int Nstrips_perHTcell;
  iss_Nstrips >> Nstrips_perHTcell;
  std::cout << "Nstrips_perHTcell: " << Nstrips_perHTcell << std::endl;

  std::istringstream iss_sng(argv[4]);
  bool sng;
  iss_sng >> sng;
  std::cout << "sng: " << sng << std::endl;

  NSWEvent event;
  NSWSector* sec;
  NSWClusterList clusterList;
  //NSWSegment* seg, * sg;

  clusterList.Set_vecBadChanls(v_bad_channels_strip);

  strcpy(fils,argv[5]);
  fils[(strlen(argv[5])-5)] = 0;
  strcpy(fil,fils);
  std::string root_name = ".HT_Nstrips"+std::to_string(Nstrips_perHTcell)+"_sng"+std::to_string(sng)+"_res_eff.root";//"_caruana.root";
  TFile *f = new TFile(strcat(fils,root_name.c_str()), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }
  // char mainDir[15] = "PerEventPlots";
  f->cd();
  f->mkdir("segs1_3LayerClus");
  f->mkdir("segs1_4LayerClus_3LayerSeg");
  f->mkdir("segs0_3or4LayerHits");

  /** \struct moni_t
      defines variables in root tree.
      One entry per ROD cluster.
  */
  struct moni_t{
    /** event number, increments over all files. */
    int event;
    /** Level one ID */
    int l1id;
    /** number of precision layer clusters */
    int nprec;
    /** number of clusters */
    int nclu;
    /** number of channels of this cluster */
    int wid[8];
    /** center of gravity*/
    float cog[8];
    /** better cog*/
    float cog2[8];
    /** cluster charge*/
    float charge[8];
    /** z position */
    float z[8];
    /** line fit */
    float inter, slope, chi2, angle, invcdf, cdf;
    /** predicted positon and residual */
    float pred[8], res[8];
    /** median y positon residual */
    float y;
    /** amplitude and time per channel*/
    float amp[8][10], time[8][10];
    /** number of missing strips in the cluster */
    int gap[8];
    /** strip number of the first strip */
    int start[8];
    /** strip number of the peak */
    int peak[8];
    /** number of channels with neighbor flag = 0 */
    int nflags[8];
    /** raw parabola interpolation */
    float raw[8];
    /** corrected parabola interpolation */
    float para[8];
    /** three strip center of gravity */
    float cog3[8];
    /** all strip gaussian centroid */
    float gaus[8];
    /** all strip caruana mean */
    float meanCaruana[8];
  };
  moni_t branch;

  TH1F *hResIn[4];
  TH1F *hResEx[4];

  TTree *tree = new TTree("tree","Clusters");
  tree->Branch("branch",&branch,"event/I:l1id:nprec:nclu:wid[8]:cog[8]/F:cog2[8]:charge[8]:z[8]:inter:slope:chi2:angle:pred[8]:res[8]:y:amp[8][10]:time[8][10]:gap[8]/I:start[8]:peak[8]:nflags[8]:raw[8]/F:para[8]:cog3[8]:gaus[8]:meanCaruana[8]");
  TH1F* hSeg = new TH1F ("hSeg", "Segments per event;segments", 30, 0, 30);
  
  TH1F* hDist = new TH1F ("hDist", "intercept distance;mm", 200, -400, 400);
  TH1F* hAli5 = new TH1F ("hAli5", "Alignment for layer 5;mm", 1000, -5, 5);
  TH1F* hAli6 = new TH1F ("hAli6", "Alignment for layer 6;mm", 1000, -5, 5);
  TH1F* hResCog = new TH1F ("hResCog", "Full cluster cog residual;Residual in mm", 1000, -3, 3);
  TH1F* hResCog3 = new TH1F ("hResCog3", "Three strip cog residual;Residual in mm", 1000, -3, 3);
  TH1F* hResPara = new TH1F ("hResPara", "Three strip parabola residual;Residual in mm", 1000, -3, 3);
  TH1F* hChi2 = new TH1F ("hChi2", "Chi2 per segment", 500, 0, 2000);
  TH1F* hChi2divDOF = new TH1F ("hChi2divDOF", "Chi2 divided by DOF per segment", 500, 0, 2000);
  TH1F* hAng = new TH1F("hAng", "Angle per segment", 90, -90, 90);
  TH1F* hInvCDF = new TH1F("hInvCDF","Inv-CDF for the obtained Chi2",50,0,1);
  TH1F* hCDF = new TH1F("hCDF","CDF for the obtained chi2",50,0,1);
  TH1F* hLayerEff = new TH1F("hLayerEff", "Layer efficiency", 10, 0, 10);

  std::string hNum_HTcells_name, hSumAmp_HTcells_name;
  hNum_HTcells_name = "hNum_HTcells_"+std::to_string(Nstrips_perHTcell);
  hSumAmp_HTcells_name = "hSumAmp_HTcells"+std::to_string(Nstrips_perHTcell);

  TH1F* hNum_HTcells = new TH1F(hNum_HTcells_name.c_str(),"number of valid HT cells per event",11,-0.5,10.5);
  TH1F* hSumAmp_HTcells = new TH1F(hSumAmp_HTcells_name.c_str(),"total amplitude of HT cells selected more than one per event",40,0,4000);
  TH1F* hClusterWidth = new TH1F("ClusterWidth","ClusterWidth",20,0,20);
  TH1F* hMaxPdo = new TH1F("MaxPdo","MaxPdo",1024,-0.5,1023.5);
  TH1F* hPeakPdo = new TH1F("PeakPdo","PeakPdo",1024,-0.5,1023.5);
  TH1F* hNumPot = new TH1F("NumPot","Number of potential signal channels per cluster", 20,0,20);
  TH1F* hNumMask = new TH1F("NumMask","Number of masked signal channels per cluster", 20,0,20);
  TH1F* hNumBkg = new TH1F("NumBkg","Number of background channels per cluster", 20,0,20);

  TH1F* hClusterWidth_Seg = new TH1F("ClusterWidth_Seg","ClusterWidth_Seg",20,0,20);
  TH1F* hMaxPdo_Seg = new TH1F("MaxPdo_Seg","MaxPdo_Seg",1024,-0.5,1023.5);
  TH1F* hPeakPdo_Seg = new TH1F("PeakPdo_Seg","PeakPdo_Seg",1024,-0.5,1023.5);
  TH1F* hNumPot_Seg = new TH1F("NumPot_Seg","Number of potential signal channels per cluster in a reco segment",20,0,20);
  TH1F* hNumMask_Seg = new TH1F("NumMask_Seg","Number of masked signal channels per cluster in a reco segment", 20,0,20);
  TH1F* hNumBkg_Seg = new TH1F("NumBkg_Seg","Number of background channels per cluster in a reco segment", 20,0,20);

  TH1F* h_fracNumPot_Seg = new TH1F("fracNumPot_Seg","Number of potential signal channels per cluster", 50,0,1);
  TH1F* h_fracNumMask_Seg = new TH1F("fracNumMask_Seg","Number of masked signal channels per cluster", 50,0,1);
  TH1F* h_fracNumBkg_Seg = new TH1F("fracNumBkg_Seg","Number of background channels per cluster", 50,0,1);
  TH2F* h_fracNumPot_vs_Ang = new TH2F("fracNumPot_vs_Ang","# of potential signal chans per clu in a reco segment vs angle", 90,-90,90,50,0,1);
  TH2F* h_fracNumMask_vs_Ang = new TH2F("fracNumMask_vs_Ang","# of masked signal chans per clu in a reco segment vs angle", 90,-90,90,50,0,1);
  TH2F* h_fracNumBkg_vs_Ang = new TH2F("fracNumBkg_vs_Ang","# of background chans per clu in a reco segment vs angle", 90,-90,90,50,0,1);
  TH2F* h_fracNumPot_vs_Chi2 = new TH2F("fracNumPot_vs_Chi2","# of potential signal chans per clu in a reco segment vs chi2", 500,0,2000,50,0,1);
  TH2F* h_fracNumMask_vs_Chi2 = new TH2F("fracNumMask_vs_Chi2","# of masked signal chans per clu in a reco segment vs chi2", 500,0,2000,50,0,1);
  TH2F* h_fracNumBkg_vs_Chi2 = new TH2F("fracNumBkg_vs_Chi2","# of background chans per clu in a reco segment vs chi2", 500,0,2000,50,0,1);

  TH1F* h_numEvwNoHitsLayers_Vs_numLayerswNoHits = new TH1F("numEvwNoHitsLayers_Vs_numLayerswNoHits","numEvwNoHitsLayers_Vs_numLayerswNoHits",5,-0.5,4.5);
  TH1F* h_numEvwNoClusLayers_Vs_numLayerswNoClus = new TH1F("numEvwNoClusLayers_Vs_numLayerswNoClus","numEvwNoClusLayers_Vs_numLayerswNoClus",5,-0.5,4.5);

  TH2F* h_ClusWidth_Vs_bkgChns = new TH2F("ClusWidth_Vs_bkgChns","ClusWidth_Vs_bkgChns",20,0,20,20,0,20);
  
  TH2F* h_Relbcid_Vs_DistMaxPdo_1seg3layer = new TH2F("Relbcid_Vs_DistMaxPdo_1seg2layer","Relbcid_Vs_DistMaxPdo_1seg3layer",8,0,8,8,0,8);
  TH2F* h_Relbcid_Vs_DistMaxPdo = new TH2F("Relbcid_Vs_DistMaxPdo","Relbcid_Vs_DistMaxPdo",8,0,8,8,0,8);

  //================= per event plots after segment reconstruction =================//
  
  std::vector< std::vector<TCanvas*> > c_segments;
  
  std::vector<TCanvas*> c_evtDisplay;
  
  //std::vector< std::vector<TH2F*> > h_Pdo_Vs_PosVsLayer;
  //std::vector< std::vector<TGraph*> > g_Pos_Vs_Layer_allHits;
  
  //=============================================================================//

  TH1F* hNumClu[8];
  TH1F* hNumClu_Seg = new TH1F("hNumClu_Seg", "Number of layers with cluster", 20, 0, 20);
  TH1F * hOcc[8];
  TH2F * hTime[8];
  TH1F * hn[8], *hN[8], *hEffi[8]; // efficiency
  TH1F* hClusCharge[8]; 
  TH1F* hClusCharge_Seg_4of4[8];
  TH1F* hClusCharge_Seg[8];
  TH1F* hClusCentroid_Seg[8];
  TH1F* hClusCentroid_Seg_4of4[8];
  TH1F* hMaxPdoStripCharge_Seg[8];
  TH1F* hMaxPdoStripCharge_Seg_4of4[8];
  TH1F* hNeighFlag0_Clus[8];
  TH1F* hNeighFlag1_Clus[8];

  TH2F* hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[8];
  TH2F* hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[8];

  TH2F* hAng_Chi2  = new TH2F("hAng_Chi2", "Chi2 vs Track angle;Angle;chi2",65, -65, 65, 100, 0, 100);
  TH2F* hChi2_nSeg = new TH2F("hChi2_nSeg","segment multi vs Track chi2;chi2;#Segment", 50,0,50, 10,0,10);
  TH2F* hInvCDFvschi2 = new TH2F("hInvCDFvschi2","inv-cdf vs chi2",500,0,2000,50,0,1);
  TH2F* hCDFvschi2 = new TH2F("hCDFvschi2","cdf vs chi2",500,0,2000,50,0,1);

  char resname[10], restitle[80];
  for (int i=0; i<4; i++){
    sprintf(resname, "hResIn%d", i+1);
    sprintf(restitle, "Layer %d inclusive residual;Residual in mm", i+1);
    hResIn[i] = new TH1F(resname, restitle, 1000, -3, 3);

    sprintf(resname, "hResEx%d", i+1);
    sprintf(restitle, "Layer %d exclusive residual;Residual in mm", i+1);
    hResEx[i] = new TH1F(resname, restitle, 1000, -3, 3);
    
  }
 
  char name[80], title[80];
  for (int i=0; i<8; i++) {
    sprintf (name, "hOcc%d", i);
    sprintf (title, "Cluster position of layer %d;Position in mm", i);
    hOcc[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hTime%d", i);
    sprintf (title, "Cluster peaking time of layer %d;Position in mm;time in ns", i);
    hTime[i] = new TH2F (name, title, 1000, 0, 4000, 100, -25, 175);

    sprintf (name, "hn%d", i);
    sprintf (title, "Predicted position of layer %d;Position in mm", i);
    hn[i] = new TH1F (name, title, 1000, 0, 4000);
    
    sprintf (name, "hN%d", i);
    sprintf (title, "Predicted positon of layer %d;Position in mm", i);
    hN[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hEffi%d", i);
    sprintf (title, "Strip cluster efficiency of layer %d;Position in mm;Efficiency", i);
    hEffi[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hNumClu%d", i);
    sprintf (title, "Number of clusters in layer %d", i);
    hNumClu[i] = new TH1F(name, title, 20, 0, 20);
    
    sprintf (name, "hClusCharge%d", i);
    sprintf (title, "sum of all channel charges (pdo - baseline) in layer %d", i);
    hClusCharge[i]= new TH1F(name, title,200,0,2000);

    sprintf (name, "hClusCharge_Seg%d", i);
    sprintf (title, "sum of all channel charges (pdo - baseline) for segmemnts in layer %d", i);
    hClusCharge_Seg[i]= new TH1F(name, title,200,0,2000);

    sprintf (name, "hClusCharge_Seg_4of4_%d", i);
    sprintf (title, "sum of all channel charges (pdo - baseline) for segmemnts 4 out of 4 in layer %d", i);
    hClusCharge_Seg_4of4[i]= new TH1F(name, title,200,0,2000);

    sprintf (name, "hClusCentroid_Seg%d", i);
    sprintf (title, "cluster centroid position for layer %d", i);
    hClusCentroid_Seg[i]=new TH1F(name, title, 135 ,0,1350);

    sprintf (name, "hClusCentroid_Seg_4of4_%d", i);
    sprintf (title, "cluster centroid position for 4 out of 4 in layer %d", i);
    hClusCentroid_Seg_4of4[i]=new TH1F(name, title, 135 ,0,1350);

    sprintf (name, "hMaxPdoStripCharge_Seg%d", i);
    sprintf (title, "max pdo strip charge on layer %d", i);
    hMaxPdoStripCharge_Seg[i] = new TH1F(name, title, 1000, 0, 1000);

    sprintf (name, "hMaxPdoStripCharge_Seg_4of4_%d", i);
    sprintf (title, "max pdo strip charge 4 out of 4 on layer %d", i);
    hMaxPdoStripCharge_Seg_4of4[i] =new TH1F(name, title, 1000, 0, 1000);
    
    sprintf (name, "hNeighFlag0_Clus%d", i);
    sprintf (title, "cluster channels with nigh flag 0 on layer %d", i);
    hNeighFlag0_Clus[i] = new TH1F(name, title, 1000, 0, 1000);

    sprintf (name, "hNeighFlag1_Clus%d", i);
    sprintf (title, "cluster channels with nigh flag 0 on layer %d", i);
    hNeighFlag1_Clus[i] = new TH1F(name, title, 1000, 0, 1000);

    sprintf (name, "hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib%d", i);
    sprintf (title, "Max pdo vs 1st neighbour pdo on layer %d", i);
    hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[i] = new TH2F(name, title, 100, 0, 1000, 100, 0, 1000);

    sprintf (name, "hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib%d", i);
    sprintf (title, "Max pdo vs 2nd neighbour pdo on layer %d",i);
    hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[i] =new TH2F(name, title, 100, 0, 1000, 100, 0, 1000);

  }
	  
  int totSeg = 0;
  int numSeg = 0; // per event
  int gifNumber = 0;
  
  for (int iFile=5; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    int num_events_found = 0;
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data

        bool isSingleCluster = true;
  
	//fill hitlist with cluster data
	branch.event = eventNumber;
	
	//std::string c_display_name = "c_display_Ev"+std::to_string(eventNumber);
	//TCanvas* c_evtDisplay_perev = new TCanvas(c_display_name.c_str(),c_display_name.c_str(),800,600);
	bool isEvtRej_noHitsLayers = false, isEvtRej_noClusLayers = false;

	for (int i=0; i<event.GetNumSector(); i++){
	  
	  NSWSector* sec = event.GetSector(i);
	  cal.Calibrate(sec); 

	  /*std::vector<float> z_chn, x_chn; 
	  z_chn.clear();
	  x_chn.clear();

	  std::vector<bool> isLayerWithHits(4,false);

	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac =sec->GetPacket(j);
	    if (pac->GetEndPoint() != 1) continue; // only strips 
	    for (int k=0; k<pac->GetWidth(); k++){
	      NSWChannel* chn = pac->GetChannel(k);
	      if(chn->GetAmplitude_mV()==0) continue;
	      int layer= chn->GetLayer();
	      isLayerWithHits[layer-4] = true;
	      //z_chn.push_back(chn->GetZPosition());
	      //x_chn.push_back(chn->GetStripPosition());
	    }// next k  
	  }// next j
		    
	  int num_noHitsLayers = count(isLayerWithHits.begin(), isLayerWithHits.end(), false);
	  h_numEvwNoHitsLayers_Vs_numLayerswNoHits->Fill(num_noHitsLayers);
	  if(num_noHitsLayers >= 2){
	    NrejEvnts_noHitsLayers_ge2++;
	    isEvtRej_noHitsLayers = true;
	  }
	  else
	    NselEvnts_HitsLayers_ge3++;

	  if(z_chn.size()!=0){
	    c_evtDisplay_perev->cd();
	    TGraph* g_display_chn = new TGraph(z_chn.size(), &z_chn[0], &x_chn[0]);
	    std::string g_display_chn_name = "g_display_chn_Ev"+std::to_string(eventNumber);
	    g_display_chn->SetName(g_display_chn_name.c_str());
	    g_display_chn->GetXaxis()->SetTitle("z [mm]");
	    g_display_chn->GetYaxis()->SetTitle("x [mm]");
	    g_display_chn->SetMarkerColor(kGreen+1);  
	    g_display_chn->SetMarkerSize(1);
	    g_display_chn->SetMarkerStyle(21);
	    g_display_chn->Draw("AP");
	  }*/

	  int clusterType = 1;
	  
	  NSWHoughTx* o_HT = new NSWHoughTx(Nstrips_perHTcell);
	  o_HT->Set_vecBadChanls(v_bad_channels_strip);
	  o_HT->Set_sng(sng);
	  o_HT->define_HT_var();
	  o_HT->select_hits(sec);
	  o_HT->HoughTransform();
	  std::vector< std::pair< std::pair<int,int>,double > > sumAmpl_HTcells = o_HT->Fill_HT(40,3,clusterType);
	  if(o_HT->reject == true){
	    NrejEvnts++;
	  }
	  if(o_HT->reject_noFilledHT == true){
            NrejEvnts_noFilledHT++;
          }
	  if(o_HT->reject_noHit1perlayer == true){
	    NrejEvnts_noHit1perlayer++;
	  }
	  if(o_HT->reject_noSmallAngle == true){
            NrejEvnts_noSmallAngle++;
          }
	  if(o_HT->reject_noClusterHT == true){
            NrejEvnts_noClusterHT++;
          }
	  /*if(o_HT->reject_noGoodClusterHT == true){
            NrejEvnts_noGoodClusterHT++;
	    }*/

	  //=============================================================================//
	  
	  std::vector<std::vector< std::vector <NSWCluster*>>> multiple_clusters = o_HT->GetMultiClusters();
	  int num_HTcells = multiple_clusters.size();
	  //std::cout << "Number of tracks from HT in event " << eventNumber << ": " << num_HTcells << std::endl;
	  
	  hNum_HTcells->Fill(num_HTcells);
          for(int HTcell = 0; HTcell<sumAmpl_HTcells.size(); HTcell++){
            hSumAmp_HTcells->Fill(sumAmpl_HTcells[HTcell].second);
          }

	  //===========================================================================================//
	  //============================= Resolution code taken from GIF-sTGC_res =====================//
	  //===========================================================================================// 
	  std::vector<NSWClusterList> clusterList(num_HTcells);
	  std::vector<NSWClusterList> clusterList_badclusters(num_HTcells);
	  std::vector<NSWClusterList> clusterList_CombHT(num_HTcells);
	  //clusterList.Clear();
	  NSWSegmentList segList;
	  segList.Clear();
	  NSWSegmentList segmentList;
	  segmentList.Clear();

	  std::vector<float> z_clu, x_clu;
          z_clu.clear();
          x_clu.clear();

	  std::vector<bool> isLayerWithClus(4,false);
		  
	  for(int iHT=0; iHT<num_HTcells; iHT++){ 
	    clusterList.at(iHT).Clear();
	    clusterList_badclusters.at(iHT).Clear();
	    clusterList.at(iHT).SetClusterList(multiple_clusters.at(iHT));
	    //========================== Double Maxima split=============================//
	    for (int layer=0; layer<8; layer++){
	      int numClu_Before_dblmax = clusterList.at(iHT).GetNumClusters(layer);
	      // cluster   
	      for (int i=0; i<clusterList.at(iHT).GetNumClusters(layer); i++){
		NSWCluster *cl = clusterList.at(iHT).GetCluster(layer, i);
		int num_chns = cl->GetNumChannel();
		
		// channel
		std::vector<int> nul;
		nul.clear();
		for(int ichn=0; ichn<num_chns; ichn++){
		  NSWChannel* channel = cl->GetChannel(ichn);
		  
		  // find NULL position
		  if(channel==NULL){
		    if( ichn==0 || ichn == num_chns-1 ) nul.push_back(ichn);
		    else if( cl->GetChannel(ichn+1)->GetNeighbor() == 0 && cl->GetChannel(ichn-1)->GetNeighbor() == 0 )
		      nul.push_back(ichn);
		  }
		} //channel
		
		for(int j=0; j<nul.size(); j++){
		  if( j== 0){
		    clusterList.at(iHT).SplitCluster(layer, i, nul[j], true);
		  }
		  else{
		    clusterList.at(iHT).SplitCluster(layer, i, nul[j] - nul[j-1] - 1, true);
		  }
		  if( nul[j] != 0 && nul[j] != num_chns-1 ) i++;
		}
	      } //cluster 
	      // cluster                                                                                                            
	      int numClu_After1_dblmax = clusterList.at(iHT).GetNumClusters(layer);
	      for (int i=0; i<clusterList.at(iHT).GetNumClusters(layer); i++){
		NSWCluster *cl = clusterList.at(iHT).GetCluster(layer, i);
		int num_chns = cl->GetNumChannel();
		if(num_chns < 4) continue;
		
		// channel                                                                                                          
		std::vector<int> valley;
		valley.clear();
		
		for(int ichn=0; ichn<num_chns; ichn++){
		  NSWChannel* channel = cl->GetChannel(ichn);
		  
		  if( channel == NULL ) continue;
		  if(ichn==0 || ichn==num_chns-1) continue;
		  
		  if( cl->GetChannel(ichn-1) == NULL && cl->GetChannel(ichn+1) == NULL ){
		    if( cl->GetChannel(ichn-2)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+2)->GetAmplitude() > channel->GetAmplitude() ) valley.push_back(ichn);
		  }
		  else if( cl->GetChannel(ichn-1) == NULL ){
		    if( cl->GetChannel(ichn-2)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+1)->GetAmplitude() > channel->GetAmplitude() ) valley.push_back(ichn);
		  }
		  else if( cl->GetChannel(ichn+1) == NULL ){
		    if( cl->GetChannel(ichn-1)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+2)->GetAmplitude() > channel->GetAmplitude() ) valley.push_back(ichn);
		  }
		  else if( cl->GetChannel(ichn-1) != NULL && cl->GetChannel(ichn+1) != NULL ){
		    if( cl->GetChannel(ichn-1)->GetAmplitude() > channel->GetAmplitude() && cl->GetChannel(ichn+1)->GetAmplitude() > channel->GetAmplitude() ) valley.push_back(ichn);
		  }
		} //channel
		
		for(int j=0; j<valley.size(); j++){
		  if( j==0 ){
		    clusterList.at(iHT).SplitCluster(layer, i, valley[j], false);
		  }
		  else{
		    clusterList.at(iHT).SplitCluster(layer, i, valley[j]-valley[j-1], false);
		  }
		  i++;
		}
	      } //cluster 
	      //int numClu_After2_dblmax = clusterList.at(iHT).GetNumClusters(layer);
	      //if(numClu_Before_dblmax != numClu_After1_dblmax || numClu_After1_dblmax != numClu_After2_dblmax || numClu_Before_dblmax != numClu_After2_dblmax){
	      //  std::cout << "layer " << layer << " Before: " << numClu_Before_dblmax << " After-1: " << numClu_After1_dblmax << " After-2: " << numClu_After2_dblmax << std::endl;
	      //}
	      
	      if(clusterList.at(iHT).GetNumClusters(layer)==0) continue;
	    
	      for (int icl=0; icl<clusterList.at(iHT).GetNumClusters(layer); icl++){
		
		std::string reason = "";
		NSWCluster *cl = clusterList.at(iHT).GetCluster(layer, icl);
		
		h_ClusWidth_Vs_bkgChns->Fill(cl->GetNumBkg(), cl->GetWidth());
		/*for(int ichn=0; ichn<cl->GetNumChannel(); ichn++){
		  NSWChannel* channel = cl->GetChannel(ichn);
		  if(channel == NULL) continue;
		  h_Relbcid_Vs_DistMaxPdo->Fill(fabs(ichn-cl->Getimax()),channel->GetRelBCID());
		}*/
		
		if(!(o_HT->isGoodCluster(cl,reason,clusterType))){
		  /*if(reason == "no_beam_area"){
		    std::vector<float> z_bad_clu, x_bad_clu;
                    z_bad_clu.clear();
                    x_bad_clu.clear();

                    z_bad_clu.push_back(cl->GetZPosition());
                    if(clusterType==1){
                      x_bad_clu.push_back(cl->GetGaussianCentroid());
                    }
                    else if(clusterType==2){
                      x_bad_clu.push_back(cl->GetMeanCaruana());
                    }

                    c_evtDisplay_perev->cd();
                    TGraph* g_display_bad_clu_area = new TGraph(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_area_Ev"+std::to_string(eventNumber);
                    g_display_bad_clu_area->SetName(g_display_bad_clu_name.c_str());
                    g_display_bad_clu_area->GetXaxis()->SetTitle("z [mm]");
                    g_display_bad_clu_area->GetYaxis()->SetTitle("x [mm]");
                    g_display_bad_clu_area->SetMarkerColor(kBlack);
                    g_display_bad_clu_area->SetMarkerSize(1.5);
                    g_display_bad_clu_area->SetMarkerStyle(45);
                    g_display_bad_clu_area->Draw("P SAME");
		  }
		  if(reason == "cluster_wid_lt3"){
		    std::vector<float> z_bad_clu, x_bad_clu;
		    z_bad_clu.clear();
		    x_bad_clu.clear();
		    
		    z_bad_clu.push_back(cl->GetZPosition());
		    if(clusterType==1){
		      x_bad_clu.push_back(cl->GetGaussianCentroid());
		    }
		    else if(clusterType==2){
		      x_bad_clu.push_back(cl->GetMeanCaruana());
		    }

		    c_evtDisplay_perev->cd();
		    TGraph* g_display_bad_clu_wid = new TGraph(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_wid_Ev"+std::to_string(eventNumber);
		    g_display_bad_clu_wid->SetName(g_display_bad_clu_name.c_str());
		    g_display_bad_clu_wid->GetXaxis()->SetTitle("z [mm]");
		    g_display_bad_clu_wid->GetYaxis()->SetTitle("x [mm]");
		    g_display_bad_clu_wid->SetMarkerColor(kBlack);
		    g_display_bad_clu_wid->SetMarkerSize(1.5);
		    g_display_bad_clu_wid->SetMarkerStyle(29);
		    g_display_bad_clu_wid->Draw("P SAME");
		    
		  }
		  else if(reason == "recopos_not_middle"){
		    std::vector<float> z_bad_clu, x_bad_clu;
                    z_bad_clu.clear();
                    x_bad_clu.clear();

                    z_bad_clu.push_back(cl->GetZPosition());
                    if(clusterType==1){
                      x_bad_clu.push_back(cl->GetGaussianCentroid());
                    }
                    else if(clusterType==2){
                      x_bad_clu.push_back(cl->GetMeanCaruana());
                    }

                    c_evtDisplay_perev->cd();
                    TGraph* g_display_bad_clu_pos = new TGraph(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_pos_Ev"+std::to_string(eventNumber);
                    g_display_bad_clu_pos->SetName(g_display_bad_clu_name.c_str());
                    g_display_bad_clu_pos->GetXaxis()->SetTitle("z [mm]");
                    g_display_bad_clu_pos->GetYaxis()->SetTitle("x [mm]");
                    g_display_bad_clu_pos->SetMarkerColor(kBlack);
                    g_display_bad_clu_pos->SetMarkerSize(1.5);
                    g_display_bad_clu_pos->SetMarkerStyle(49);
                    g_display_bad_clu_pos->Draw("P SAME");
                  }
		  else if(reason == "chn_bad"){
		    std::vector<float> z_bad_clu, x_bad_clu;
                    z_bad_clu.clear();
                    x_bad_clu.clear();

                    z_bad_clu.push_back(cl->GetZPosition());
                    if(clusterType==1){
                      x_bad_clu.push_back(cl->GetGaussianCentroid());
                    }
                    else if(clusterType==2){
                      x_bad_clu.push_back(cl->GetMeanCaruana());
                    }

                    c_evtDisplay_perev->cd();
                    TGraph* g_display_bad_clu_chn = new TGraph(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_chn_Ev"+std::to_string(eventNumber);
                    g_display_bad_clu_chn->SetName(g_display_bad_clu_name.c_str());
                    g_display_bad_clu_chn->GetXaxis()->SetTitle("z [mm]");
                    g_display_bad_clu_chn->GetYaxis()->SetTitle("x [mm]");
                    g_display_bad_clu_chn->SetMarkerColor(kBlack);
                    g_display_bad_clu_chn->SetMarkerSize(1.5);
                    g_display_bad_clu_chn->SetMarkerStyle(34);
                    g_display_bad_clu_chn->Draw("P SAME");
                  }
		  else if(reason == "middle_no_neighbour"){
		    std::vector<float> z_bad_clu, x_bad_clu;
                    z_bad_clu.clear();
                    x_bad_clu.clear();

                    z_bad_clu.push_back(cl->GetZPosition());
                    if(clusterType==1){
                      x_bad_clu.push_back(cl->GetGaussianCentroid());
                    }
                    else if(clusterType==2){
                      x_bad_clu.push_back(cl->GetMeanCaruana());
                    }

                    c_evtDisplay_perev->cd();
                    TGraph* g_display_bad_clu_neigh = new TGraph(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_neigh_Ev"+std::to_string(eventNumber);
                    g_display_bad_clu_neigh->SetName(g_display_bad_clu_name.c_str());
                    g_display_bad_clu_neigh->GetXaxis()->SetTitle("z [mm]");
                    g_display_bad_clu_neigh->GetYaxis()->SetTitle("x [mm]");
                    g_display_bad_clu_neigh->SetMarkerColor(kBlack);
                    g_display_bad_clu_neigh->SetMarkerSize(1.5);
                    g_display_bad_clu_neigh->SetMarkerStyle(47);
                    g_display_bad_clu_neigh->Draw("P SAME");
                  }
		  else if(reason == "bkg_relbcid_chn"){
		    std::vector<float> z_bad_clu, x_bad_clu;
                    z_bad_clu.clear();
                    x_bad_clu.clear();

                    z_bad_clu.push_back(cl->GetZPosition());
                    if(clusterType==1){
                      x_bad_clu.push_back(cl->GetGaussianCentroid());
                    }
                    else if(clusterType==2){
                      x_bad_clu.push_back(cl->GetMeanCaruana());
                    }

                    c_evtDisplay_perev->cd();
                    TGraph* g_display_bad_clu_bkg = new TGraph(z_bad_clu.size(), &z_bad_clu[0], &x_bad_clu[0]);
		    std::string g_display_bad_clu_name = "g_display_bad_clu_bkg_Ev"+std::to_string(eventNumber);
                    g_display_bad_clu_bkg->SetName(g_display_bad_clu_name.c_str());
                    g_display_bad_clu_bkg->GetXaxis()->SetTitle("z [mm]");
                    g_display_bad_clu_bkg->GetYaxis()->SetTitle("x [mm]");
                    g_display_bad_clu_bkg->SetMarkerColor(kBlack);
                    g_display_bad_clu_bkg->SetMarkerSize(1.5);
                    g_display_bad_clu_bkg->SetMarkerStyle(39);
                    g_display_bad_clu_bkg->Draw("P SAME");
		  }*/
		  
		  clusterList_badclusters.at(iHT).AddCluster(layer, cl);
		  clusterList.at(iHT).RemoveBadCluster(layer, icl);
		  icl--;
		}
	      }
	      
	      if(clusterList.at(iHT).GetNumClusters(layer)!=0)
                isLayerWithClus[layer-4] = true;
	      
	    } //layer 
	    
	    //=======================Double maxima split end===============================//
	    
	    //============= PRINTOUTS =================//
	    /*
	    for(int il=0; il<8; il++){
	      //std::cout << "layer: " << il << " number of clusters: " << multiple_clusters[iHT][il].size() << std::endl;
	      for(int icl=0; icl<multiple_clusters[iHT][il].size(); icl++){
	    	NSWCluster* c = multiple_clusters[iHT][il][icl];
	    	if(c == NULL) continue;
	    	//std::cout << "Cluster no. " << icl << " centroid: " << c->GetMeanCaruana() << " num_pot: " << c->GetNumPotential() << " num_mask: " << c->GetNumMasked() << " num_bkg: " << c->GetNumBkg() << std::endl;
	      }
	    }*/
	    //=========================================//
	    //std::cout << "eventNumber: " << eventNumber << " HT cell: " << iHT << std::endl;
	  }
	  
	  // Combining different HT cells for common clusters
	  int HT_num = 0;  
	  int combHT_num = 0;
	  bool new_list = true;
	  bool isCommon_Clu = false;
	  
	  if(num_HTcells>0){
	    do{
	      if(new_list){
		if(HT_num+1 < num_HTcells){
		  isCommon_Clu = clusterList.at(HT_num).isCommonCluster(clusterList.at(HT_num+1));
		  
		  if(isCommon_Clu){
		    clusterList_CombHT.at(combHT_num).Clear();
		    clusterList.at(HT_num).CombineClustersLists(clusterList.at(HT_num+1),clusterList_CombHT.at(combHT_num));
		    new_list = false;
		    HT_num += 2;
		  }
		  else{
		    clusterList_CombHT.at(combHT_num).Clear();
		    
		    for(int layer=0; layer<8; layer++){
		      for(int icl=0; icl<clusterList.at(HT_num).GetNumClusters(layer); icl++){
			NSWCluster* clui = new NSWCluster();
			*clui = *(clusterList.at(HT_num).GetCluster(layer, icl));
			if(clui == NULL) continue;
			clusterList_CombHT.at(combHT_num).AddCluster(layer,clui);
		      }
		    }
		    
		    new_list =true;
		    combHT_num++;
		    HT_num++;
		  }
		}
		else{
		  clusterList_CombHT.at(combHT_num).Clear(); 
		  
		  for(int layer=0; layer<8; layer++){
		    for(int icl=0; icl<clusterList.at(HT_num).GetNumClusters(layer); icl++){
		      NSWCluster* clui = new NSWCluster();
		      *clui = *(clusterList.at(HT_num).GetCluster(layer, icl));
		      if(clui == NULL) continue;
		      clusterList_CombHT.at(combHT_num).AddCluster(layer,clui);
		    }
		  }
		  new_list =true;
		  combHT_num++;
		  HT_num++;
		}
	      }	
	      else{
		isCommon_Clu = clusterList.at(HT_num).isCommonCluster(clusterList_CombHT.at(combHT_num));
		if(isCommon_Clu){
		  
		  clusterList.at(HT_num).CombineClustersLists(clusterList_CombHT.at(combHT_num));
		  new_list = false;
		  HT_num++;
		}
		else{
		  new_list =true;
		  combHT_num++;
		}
	      }
	    }
	    while(HT_num<num_HTcells);
	    
	    for(int icell=0; icell<clusterList_CombHT.size(); icell++){
	      segmentList.multiCluster_Fill4(&clusterList_CombHT.at(icell), 4, 10, clusterType, 0.3);
	      for (int layer=0; layer<8; layer++){
		for (int icl=0; icl<clusterList_CombHT.at(icell).GetNumClusters(layer); icl++){
		  
		  NSWCluster *cl = clusterList_CombHT.at(icell).GetCluster(layer, icl);
		  int num_chns = cl->GetNumChannel();
		  if(cl->GetWidth()<3)
		    std::cout << "Cluster number: " << icl << " num_chns: " << cl->GetWidth() << " gaussian centroid: " << cl->GetGaussianCentroid() << " caruana mean: " << cl->GetMeanCaruana() << std::endl;
		  
		  hClusterWidth->Fill(cl->GetWidth());
		  
		  hMaxPdo->Fill(cl->GetMaxPDO());
		  TF1* f_clu = cl->GetGaussianFunction();
		  hPeakPdo->Fill(f_clu->GetMaximum( cl->GetChannel(0)->GetStripPosition(), cl->GetChannel(num_chns-1)->GetStripPosition() ));
		  hNumPot->Fill(cl->GetNumPotential());
		  hNumMask->Fill(cl->GetNumMasked());
		  hNumBkg->Fill(cl->GetNumBkg());
		  
		  hClusCharge[layer]->Fill(cl->GetTotalAmplitude());
		  
		  for(int ichn=0; ichn<num_chns; ichn++){
		    NSWChannel* channel = cl->GetChannel(ichn);
		    if(channel == NULL) continue;
		    if(channel->GetNeighbor()==0){
		      hNeighFlag0_Clus[layer]->Fill(channel->GetAmplitude_mV());
		    }
		    else if(channel->GetNeighbor()==1){
		      hNeighFlag1_Clus[layer]->Fill(channel->GetAmplitude_mV());
		    }
		  }
		  
		  /*z_clu.push_back(cl->GetZPosition());
		    if(clusterType==1){
		    x_clu.push_back(cl->GetGaussianCentroid()); 
		    } 
		    else if(clusterType==2){ 
		    x_clu.push_back(cl->GetMeanCaruana()); 
		    }*/
		  
		}
		
		hNumClu[layer]->Fill(clusterList_CombHT.at(icell).GetNumClusters(layer));
		
	      }
	    }
	  }

	  /*int num_noClusLayers = count(isLayerWithClus.begin(), isLayerWithClus.end(), false);
	  
	  if(!isEvtRej_noHitsLayers){
	    h_numEvwNoClusLayers_Vs_numLayerswNoClus->Fill(num_noClusLayers);
	    
	    if(num_noClusLayers >= 2){
	      NrejEvnts_noClusLayers_ge2++;
	      isEvtRej_noClusLayers = true;
	    }
	    else
	      NselEvnts_ClusLayers_ge3++;
	  } */
	
	  /*if(z_clu.size()!=0){
	    c_evtDisplay_perev->cd();
	    TGraph* g_display_clu = new TGraph(z_clu.size(), &z_clu[0], &x_clu[0]);
	    std::string g_display_clu_name = "g_display_clu_Ev"+std::to_string(eventNumber);
	    g_display_clu->SetName(g_display_clu_name.c_str());
	    g_display_clu->GetXaxis()->SetTitle("z [mm]");
	    g_display_clu->GetYaxis()->SetTitle("x [mm]");
	    g_display_clu->SetMarkerColor(kBlue);
	    g_display_clu->SetMarkerSize(1.5);
	    g_display_clu->SetMarkerStyle(22);
	    g_display_clu->Draw("P SAME");
	  }*/

	  if(segmentList.GetNumSegments()==0){
	    /*if(!isEvtRej_noHitsLayers && !isEvtRej_noClusLayers){
		NrejEvnts_noSeg++;
		//std::cout << "num of layers with no clusters: " << num_noClusLayers << std::endl;
	    }*/
	    hSeg->Fill(segmentList.GetNumSegments());
	    delete o_HT;
	    continue;
	  }
	  //std::cout << "num of segments: " << segmentList.GetNumSegments() << std::endl;
	  //hSeg->Fill(segmentList.GetNumSegments());
	  //std::cout << "Number of tracks from HT in event after segment reconstruction: " << eventNumber << ": " << segmentList.GetNumSegments() << std::endl;
	  
	  std::vector<TCanvas*> c_seg_vec;
	  std::vector<TCanvas*> c_display_vec;

	  //std::cout << "//================= Event ==================//" << std::endl;
		  
	  for(int i=0; i<segmentList.GetNumSegments(); i++){
	    //std::cout << "//================= Segment number: " << i << " ==================//" << std::endl;
	    NSWSegment* seg = segmentList.GetSegment(i);
	    //std::cout << "//========== chi square: " << seg->GetChi2() << std::endl;

	    double pos_min_beam = 223*3.2 - 1.6;
            double pos_max_beam = 230*3.2;

	    std::vector<double> predicted_values(8,0.0);
	    std::vector<bool> isLayerInBeam(4,false);
            for (int j=4; j<8; j++) {
              double pred = seg->GetPredicted(j);
              predicted_values[j] = pred;
	      if(pred>=pos_min_beam && pred<=pos_max_beam){
                isLayerInBeam[j-4] = true;
              }
            }

	    //if( fabs(seg->GetAngle()) >= 20){// || seg->GetChi2() > 40){
	    if(count(isLayerInBeam.begin(), isLayerInBeam.end(), true)<3 || fabs(seg->GetAngle()) >= 20 || (seg->GetChi2()/seg->GetDOF()) > 40){
	      segmentList.RemoveSegment(i);
	      i--;
	    }
	    //}
	
	    /*else{
	      //======================================================================================================================//
	      //======================================= printouts and plots and graphs per event =====================================//
	      //======================================================================================================================//
	      //int chi2 = seg->GetChi2();
	      std::string seg_name = "Ev"+std::to_string(eventNumber)+"_"+"Seg"+std::to_string(i);
	      std::vector<float> z, x;
	      NSWCluster* cl;
	      TF1* f_seg;
	      x.clear();
	      z.clear();
	      double zmin, zmax;

	      for (int layer=4; layer<8; layer++){

		std::string gName = "Ev"+std::to_string(eventNumber)+"_"+"Seg"+std::to_string(i)+"_"+"Layer"+std::to_string(layer);
		//std::cout << "layer: " << layer << std::endl;
		NSWCluster* c = seg->GetCluster(layer); 
		if(c == NULL) continue;
		cl = c;
		z.push_back(c->GetZPosition());
		if(clusterType==1){
		  x.push_back(c->GetGaussianCentroid());
		}
		else if(clusterType==2){
		  x.push_back(c->GetMeanCaruana()); 
		}

		TCanvas* cCluster = new TCanvas(gName.c_str(),gName.c_str(),800,600); 

		std::vector<float> xpos, pdo, zpos; 
		TF1* f_clu;
		xpos.clear();
		pdo.clear();
		zpos.clear();

		int num_chns = c->GetNumChannel();
		for(int ichn=0; ichn<num_chns; ichn++){
		  NSWChannel* chn = c->GetChannel(ichn);
		  if(chn == NULL) continue;
		  //std::cout << "Channel num: " << chn->GetDetectorStrip() << ", RelBCID: " << chn->GetRelBCID() << std::endl;
		  xpos.push_back(chn->GetStripPosition());
		  pdo.push_back(chn->GetAmplitude_mV());
		  zpos.push_back(c->GetZPosition());
		}
		
		f_clu = c->GetGaussianFunction();
		double max_pdo = *max_element(pdo.begin(), pdo.end())*1.5;
		
		cCluster->cd();
		TGraph* gCluster = new TGraph(xpos.size(), &xpos[0], &pdo[0]);
		gCluster->SetName(gName.c_str());
		gCluster->SetTitle();
		gCluster->SetMaximum(max_pdo); 
		gCluster->SetMinimum(0);
		gCluster->GetXaxis()->SetTitle("x [mm]");
		gCluster->GetYaxis()->SetTitle("pdo"); 
		gCluster->SetMarkerColor(4); 
		gCluster->SetMarkerSize(1);
		gCluster->SetMarkerStyle(21); 
		gCluster->Draw("AP");
		
		f_clu->DrawF1(xpos[0]-2, xpos[xpos.size()-1]+2, "SAME");
		
		c_seg_vec.push_back(cCluster);

	      }
	      
	      zmin = cl->GetZPosition(4,true,0) - 10;
	      zmax = cl->GetZPosition(7,true,0) + 10;

	      f_seg = new TF1("f_seg","[0]+[1]*x",zmin,zmax);
	      f_seg->SetParameter(0,seg->GetIntercept());
	      f_seg->SetParameter(1,seg->GetSlope());
	      
	      TCanvas* cSegment = new TCanvas(seg_name.c_str(),seg_name.c_str(),800,600);
	      cSegment->cd();
	      TGraph* gSegment = new TGraph(z.size(), &z[0], &x[0]);
	      gSegment->SetName(seg_name.c_str());
	      gSegment->SetTitle();
	      gSegment->GetXaxis()->SetTitle("z [mm]");
	      gSegment->GetYaxis()->SetTitle("x [mm]");
	      gSegment->SetMarkerColor(kRed);
	      gSegment->SetMarkerSize(2);
	      gSegment->SetMarkerStyle(20);
	      gSegment->Draw("AP");
	      
	      f_seg->DrawF1(zmin, zmax, "SAME");
	      
	      c_seg_vec.push_back(cSegment);
	      
	      if(z.size()!=0){
		c_evtDisplay_perev->cd();
		gSegment->Draw("P SAME");
		f_seg->DrawF1(zmin, zmax, "SAME");
	      }

	      //======================================================================================================================// 
	      //======================================================================================================================// 
	      //======================================================================================================================// 
	  
	    }*/
	  
	    //c_segments.push_back(c_seg_vec);
	    //std::cout << "//===============================================//" << std::endl;
	    
	    //std::cout << "Number of tracks from HT in event after segment reconstruction: " << eventNumber << ": " << segmentList.GetNumSegments() << std::endl;
	  }
	  
	  hSeg->Fill(segmentList.GetNumSegments());

	  //############### Debugging #####################
	  /*if(!isEvtRej_noHitsLayers && !isEvtRej_noClusLayers){
	    if(segmentList.GetNumSegments()==0)
	      NrejEvnts_noSeg++;
	    else
	      NselEvnts_Seg++;
	    
	    //Check various situations . 
	    // 1. With 1 segment events
	    if(segmentList.GetNumSegments()==1){
	      if(num_noClusLayers==1){
		//f->cd("segs1_3LayerClus");
		//c_evtDisplay_perev->Write();
		
		for(int iHT=0; iHT<num_HTcells; iHT++){
		  for(int layer=4; layer<8; layer++){
		    for (int icl=0; icl<clusterList.at(iHT).GetNumClusters(layer); icl++){
		      NSWCluster *cl = clusterList.at(iHT).GetCluster(layer, icl);
		      for(int ichn=0; ichn<cl->GetNumChannel(); ichn++){
			NSWChannel* channel = cl->GetChannel(ichn);
			if(channel == NULL) continue;
			h_Relbcid_Vs_DistMaxPdo_1seg3layer->Fill(fabs(ichn-cl->Getimax()),channel->GetRelBCID());
			//std::cout << "Channel relbcid: " << channel->GetRelBCID() << std::endl;
		      }
		    }
		    if(clusterList_badclusters.at(iHT).GetNumClusters(layer)!=0){
		      for (int icl=0; icl<clusterList_badclusters.at(iHT).GetNumClusters(layer); icl++){
			NSWCluster *cl = clusterList_badclusters.at(iHT).GetCluster(layer, icl);
			for(int ichn=0; ichn<cl->GetNumChannel(); ichn++){
			  NSWChannel* channel = cl->GetChannel(ichn);
			  if(channel == NULL) continue;
			  h_Relbcid_Vs_DistMaxPdo_1seg3layer->Fill(fabs(ichn-cl->Getimax()),channel->GetRelBCID());
			  //std::cout << "Channel relbcid: " << channel->GetRelBCID() << std::endl;
			}
		      }
                    }
		  }
		}
	      }
	      //else if(num_noClusLayers==0 && segmentList.GetSegment(0)->GetNumClusters()==3){
		//f->cd("segs1_4LayerClus_3LayerSeg");
		//c_evtDisplay_perev->Write();
	      //}
	    }

	    //2. With 0 segments
	    //if(segmentList.GetNumSegments()==0){
	    //  if(num_noHitsLayers==0 || num_noHitsLayers==1){
		//f->cd("segs0_3or4LayerHits");
		//c_evtDisplay_perev->Write();
	    //  }
	    //}

	  }*/
	  //############### End of Debugging #####################     


	  //if(segmentList.GetNumSegments()>=3)
	  //  std::cout << ">=3 segments: event number: " << eventNumber << std::endl;

	  //====================== printouts ======================//

	  /*if(segmentList.GetNumSegments()==0) continue;
	  for (int i=0; i<segmentList.GetNumSegments(); i++){ 
	    NSWSegment* s = segmentList.GetSegment(i);
	    if(fabs(s->GetAngle()) < 70) continue;
	    std::cout << "Segment " << i  << " angle is large" << std::endl;
	    for (int layer=0; layer<8; layer++){
	      NSWCluster* c = s->GetCluster(layer);  
	      if(c == NULL) continue;
	      std::cout << "layer: " << layer << " cluster channels: " << c->GetNumChannel() << " first channel position: " << c->GetChannel(0)->GetStripPosition() << " cluster centroid: " << c->GetMeanCaruana() <<" last channel position: " << c->GetChannel(c->GetNumChannel()-1)->GetStripPosition() << std::endl;
	    }
	    }*/
	  //=======================================================//
	  
	  std::vector<TF1*> lin_func;
          lin_func.clear();
	  
	  //std::cout << "Number of segments: " << segmentList.GetNumSegments() << std::endl;
	  
	  
	  if(segmentList.GetNumSegments()==0){
	    delete o_HT;
	    continue;
	  }
	  
	  num_events_found++;
	  for (int i=0; i<segmentList.GetNumSegments(); i++){
            NSWSegment* s = segmentList.GetSegment(i);

	    //============== for debugging ===============//
	    //if(s->GetNumCluster()<=3) continue;
	    //============================================//

            branch.y =  s->GetYPosition();
	    hNumClu_Seg->Fill(s->GetNumCluster());
            for (int layer=0; layer<8; layer++){
	      //std::cout << "GIF-sTGC_HT: layer - " << layer << std::endl;

              branch.pred[layer] = s->GetPredicted (layer);
              NSWCluster* c = s->GetCluster(layer);
	      
              if (c == NULL){
                branch.cog[layer] = -100;
                branch.cog2[layer] = -100;
                branch.charge[layer] = -100;
                branch.wid[layer] = -1;
                branch.z[layer] = -1;
                branch.res[layer] = -100;
                for (int i=0; i<10; i++){
                  branch.amp[layer][i] = -1;
                  branch.time[layer][i] = -100;
                }
                branch.gap[layer] = -1;
                branch.start[layer] = -1;
                branch.peak[layer] = -1;
                branch.nflags[layer] = -1;
                branch.raw[layer] = -1;
                branch.para[layer] = -1;
                branch.cog3[layer] = -1;
                branch.gaus[layer] = -1;
		branch.meanCaruana[layer] = -1;
              } else {
		int num_chns = c->GetNumChannel();

		hClusterWidth_Seg->Fill(c->GetWidth());
		hMaxPdo_Seg->Fill(c->GetMaxPDO());
		TF1* f_clu = c->GetGaussianFunction();
		hPeakPdo_Seg->Fill(f_clu->GetMaximum( c->GetChannel(0)->GetStripPosition(), c->GetChannel(num_chns-1)->GetStripPosition() ));
		hNumPot_Seg->Fill(c->GetNumPotential());
		hNumMask_Seg->Fill(c->GetNumMasked());
		hNumBkg_Seg->Fill(c->GetNumBkg());

		hClusCharge_Seg[layer]->Fill(c->GetTotalAmplitude());
		hClusCentroid_Seg[layer]->Fill(c->GetGaussianCentroid());
		hMaxPdoStripCharge_Seg[layer]->Fill(c->GetChannel(c->Getimax())->GetAmplitude_mV());
		
		int neigh_1 = c->Getimax()-1;
		int neigh_2 = c->Getimax()+1;
		
		bool isNeigh1Null = (c->GetChannel(neigh_1)==NULL);
		bool isNeigh2Null = (c->GetChannel(neigh_2)==NULL);
		
		if(!isNeigh1Null && isNeigh2Null){
		  hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_1)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		}
		else if(isNeigh1Null && !isNeigh2Null){
		  hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_2)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		}
		else if(isNeigh1Null && isNeigh2Null){

		}
		else{
		  if(c->GetChannel(neigh_1)->GetAmplitude_mV() >= c->GetChannel(neigh_2)->GetAmplitude_mV()){
		    hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_1)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		    hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_2)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		  }
		  else{
		    hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_2)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		    hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[layer]->Fill(c->GetChannel(neigh_1)->GetAmplitude_mV(), c->GetChannel(c->Getimax())->GetAmplitude_mV());
		  }
		}

		if(s->GetNumCluster()==4){
		  hClusCharge_Seg_4of4[layer]->Fill(c->GetTotalAmplitude());
		  hClusCentroid_Seg_4of4[layer]->Fill(c->GetGaussianCentroid());
		  hMaxPdoStripCharge_Seg_4of4[layer]->Fill(c->GetChannel(c->Getimax())->GetAmplitude_mV());
		}

		int sum_AllRelBC = c->GetNumPotential()+c->GetNumMasked()+c->GetNumBkg();
		double frac_Pot = c->GetNumPotential()*1.0/sum_AllRelBC;
		double frac_Mask = c->GetNumMasked()*1.0/sum_AllRelBC;
		double frac_Bkg = c->GetNumBkg()*1.0/sum_AllRelBC;
		
		h_fracNumPot_Seg->Fill(frac_Pot);
		h_fracNumMask_Seg->Fill(frac_Mask);
		h_fracNumBkg_Seg->Fill(frac_Bkg);
		h_fracNumPot_vs_Ang->Fill(s->GetAngle(), frac_Pot);
		h_fracNumMask_vs_Ang->Fill(s->GetAngle(), frac_Mask);
		h_fracNumBkg_vs_Ang->Fill(s->GetAngle(), frac_Bkg);
		h_fracNumPot_vs_Chi2->Fill(s->GetChi2(), frac_Pot);
		h_fracNumMask_vs_Chi2->Fill(s->GetChi2(), frac_Mask);
		h_fracNumBkg_vs_Chi2->Fill(s->GetChi2(), frac_Bkg);

		//std::cout << "cluster width: " << c->GetWidth() << std::endl;
                branch.cog[layer] = c->GetCOG();
                branch.cog2[layer] = c->GetCOG2();
                branch.charge[layer] = c->GetCharge();
                branch.wid[layer] = c->GetNumChannel();
                branch.z[layer] = c->GetZPosition();
                branch.res[layer] = c->GetCOG() - branch.pred[layer];
                int chnMax = c->GetNumChannel();
                if (chnMax > 10) chnMax = 10;
		for (int j=0; j<chnMax; j++){
                  NSWChannel *chn = c->GetChannel(j);
                  if (chn != NULL) {
                    branch.amp[layer][j] = chn->GetAmplitude_mV();
                    branch.time[layer][j] = chn->GetPeakingTime();
                  } else {
                    branch.amp[layer][j] = -1;
                    branch.time[layer][j] = -1;
                  }
                }
                for (int j=chnMax; j<10; j++){
                  branch.amp[layer][j] = -1;
                  branch.time[layer][j] = -1;
                }
                branch.gap[layer] = c->GetNumMissing();
		branch.start[layer] = c->GetStartChannel();
                branch.peak[layer] = c->GetPeakChannel();
                branch.nflags[layer] = c->CountNeighbors();
                branch.raw[layer] = c->GetParabolaRaw();
                branch.para[layer] = c->GetParabola();
                branch.cog3[layer] = c->GetCOG3();
                branch.gaus[layer] = c->GetGaussianCentroid();
		branch.meanCaruana[layer] = c->GetMeanCaruana();
	      }
	    }// next layer

	    branch.inter = s->GetIntercept();
            branch.slope = s->GetSlope();
            branch.chi2 = s->GetChi2();
	    branch.invcdf = s->GetInvCDF();
	    branch.cdf = s->GetCDF();
            branch.angle = s->GetAngle();
            branch.nprec = s->GetNumPrecision();
            branch.nclu = s->GetNumCluster();
	    
            hChi2->Fill(s->GetChi2());
	    hChi2divDOF->Fill(s->GetChi2()/s->GetDOF());
	    hAng->Fill(s->GetAngle());
            hAng_Chi2->Fill(s->GetAngle(), s->GetChi2());
            hChi2_nSeg->Fill(s->GetChi2(), segmentList.GetNumSegments());
	    hInvCDFvschi2->Fill(s->GetChi2(), s->GetInvCDF());
	    hCDFvschi2->Fill(s->GetChi2(), s->GetCDF());
	    hInvCDF->Fill(s->GetInvCDF());
	    hCDF->Fill(s->GetCDF());

	    tree->Fill();
            numSeg++;
            totSeg++;
            for (int j=i+1; j<segmentList.GetNumSegments(); j++){
              NSWSegment* s2 = segmentList.GetSegment(j);
              hDist->Fill(s->GetIntercept() - s2->GetIntercept());
	      //printf ("%d: %f %f\n", j, s->GetIntercept() , s2->GetIntercept());   
	    }
	    //} // next segment
	  
	  
	  
	    //############################### Where you select events with only one segment ==//
	    
	    
	    //============================ Michael's code not used ==========================//
	    
	    //double cog[4], para[4];
	    //for (int j=4; j<8; j++){
	    //if(seg->GetCluster(j) == NULL) continue;
	    //  cog[j-4]=seg->GetCluster(j)->GetCOG();
	    //}
	    //if (seg->GetChi2() < 50){ // fill alignment histograms                                                                                                                                      
	    //  double m = (cog[0]-cog[3])/3;
	    //  hAli5->Fill((7-5)*m+cog[3]-cog[1]);
	    //  hAli6->Fill((7-6)*m+cog[3]-cog[2]);
	    // }
	    //hResCog->Fill(cog[1]+ali5-0.5*(cog[0]+cog[2]+ali6));
	    //bool bad = false;
	    //for (int j=4; j<7; j++) {
	    //  NSWCluster* cl = seg->GetCluster(j);
	    //  if (cl->GetCOG3() < -0.99) bad = true;
	    //  cog[j-4]=(cl->GetPeakChannel()+cl->GetCOG3()*5/3.5)*3.2;
	    //  para[j-4]=(cl->GetPeakChannel()+cl->GetParabola())*3.2;
	    //  if ((j % 2) == 1){ // stagger by -0.5*pitch
	    //	cog[j-4] -= 0.5*seg->GetPitch();
	    //	para[j-4] -= 0.5*seg->GetPitch();
	    //  }
	    //}
	    //if (!bad) {
	    //  hResCog3->Fill(cog[1]+ali5-0.5*(cog[0]+cog[2]+ali6));
	    //  hResPara->Fill(para[1]+ali5-0.5*(para[0]+para[2]+ali6));
	    //}
	    
	    //=================================================================================//
	    //                                                                                                                                                                   
	    // Resolution eval with sigma_inclusive*sigma_exclusive                                                                                                                       
	    //  
	    
	    for(int i=4; i<8; i++){
	      if(s->GetCluster(i) == NULL) continue;
	      NSWCluster *c1 = s->GetCluster(i);
	      double mean_clu = -1.0;
	      if(clusterType==1)
		mean_clu = c1->GetGaussianCentroid();
	      else if(clusterType==2)
		mean_clu = c1->GetMeanCaruana();
		
	      if (i==5)
		hResIn[i-4]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - mean_clu);//-ali5 );
	      else if (i==6)
		hResIn[i-4]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - mean_clu); //-ali6 );
	      else
		hResIn[i-4]->Fill( (branch.inter+branch.slope*c1->GetZPosition()) - mean_clu);
	      
	      std::vector<double> xEx, yEx, sigEx;
	      double interEx, slopeEx, chi2Ex;
	      double interErrEx, slopeErrEx;
	      xEx.clear();
	      yEx.clear();
	      sigEx.clear();
	      
	      for(int j=4; j<8; j++){
		if(i!=j){
		  if(s->GetCluster(j) == NULL) continue;
		  NSWCluster *c2 = s->GetCluster(j);
		  xEx.push_back(c2->GetZPosition());
		  if(clusterType==1)
		    yEx.push_back(c2->GetGaussianCentroid());
		  else if(clusterType==2)
		    yEx.push_back(c2->GetMeanCaruana());
		  sigEx.push_back(0.3);
		  // std::cout<<"Exclusive data point    x"<<": "<<c2->GetZPosition()<<"\ty"<<": "<<c2->GetMeanCaruana()<<std::endl;
		}
	      }
	      // std::cout<<"In exclusive fit:"<<std::endl;
	      linfit(&xEx[0], &yEx[0], 3, &sigEx[0], 1, &interEx, &slopeEx, &interErrEx, &slopeErrEx, &chi2Ex);
	      if (i==5)
		hResEx[i-4]->Fill( (interEx+slopeEx*c1->GetZPosition()) - mean_clu);//-ali5 );
	      else if (i==6)
		hResEx[i-4]->Fill( (interEx+slopeEx*c1->GetZPosition()) - mean_clu);//-ali6 );
	      else
		hResEx[i-4]->Fill( (interEx+slopeEx*c1->GetZPosition()) - mean_clu);
	    }
	    //} // if 4 clusters
	  
	    // calculate efficiency: 
	    //if (((s->GetChi2()<18) && (s->GetNumClusters()==4)) ||
	    //	((s->GetChi2()<9) && (s->GetNumClusters()==3))) {
	    
	    //double pos_min_beam = 223*3.2 - 1.6;
	    //double pos_max_beam = 230*3.2;
	    
	    std::vector<double> predicted_values(8,0.0);
	    //std::vector<bool> isLayerInBeam(4,false);
	    for (int j=4; j<8; j++) {
	      double pred = s->GetPredicted(j);
	      predicted_values[j] = pred;
	      //if(pred>=pos_min_beam && pred<=pos_max_beam){
	      //  isLayerInBeam[j-4] = true;
	      //}
	    }
	    
	    //if(count(isLayerInBeam.begin(), isLayerInBeam.end(), true)>=3){
	    for (int j=4; j<8; j++) {
	      hN[j]->Fill(predicted_values[j]);
	      if (s->GetCluster(j) != NULL) hn[j]->Fill(predicted_values[j]);
	    } // next layer 
	    //}// is seg in beam
	    
	    //} // if good chi2 
	    //} // if one segment per event
	  
	  }
	  //===========================================================================================//
	  //=============================End of Resolution code =======================================//
          //===========================================================================================// 
	  
	  delete o_HT;
	}// next sector

	//c_evtDisplay.push_back(c_evtDisplay_perev);
	
	if (eventNumber%100==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	eventNumber++;
	
        if (eventNumber >= 100000) break;
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0); // next event
    fclose(infile);
    //std::cout << "number of events with at least one track found: " << num_events_found << std::endl;
  } // next file

  for (int layer=0; layer<8; layer++) { // efficiency:                                                                                                                                                    
    double eff, err;
    for (int bin=0; bin<1000; bin++){
      int N = hN[layer]->GetBinContent(1+bin);
      if (N!=0) {
        int n = hn[layer]->GetBinContent(1+bin);
        CalculateEffi(n, N, eff, err);
        hEffi[layer]->SetBinContent(1+bin, eff);
        hEffi[layer]->SetBinError(1+bin, err);
      }
    }
    
    // layer efficiency                       
    double effLayer, errLayer;
    long NLayer = hN[layer]->Integral();
    long nLayer = hn[layer]->Integral();
    CalculateEffi(nLayer, NLayer, effLayer, errLayer);

    hLayerEff->SetBinContent(layer+1, effLayer);
    hLayerEff->SetBinError(layer+1, errLayer);

  }
  
  f->cd();
  tree->Write();

  hSeg->Write();
  double total_seg = hSeg->Integral();
  double single_seg = hSeg->GetBinContent(2);
  std::cout<<"Fraction of single segment event: "<< single_seg / total_seg <<std::endl;

  hDist->Write();
  hChi2->Write();
  hChi2divDOF->Write();
  hAng->Write();
  hAng_Chi2->Write();
  hChi2_nSeg->Write();
  hInvCDFvschi2->Write();
  hCDFvschi2->Write();
  hInvCDF->Write();
  hCDF->Write();
  hLayerEff->Write();
  
  hAli5->Fit("gaus", "Q", "", -0.7, 0.7);
  hAli5->Write();
  hAli6->Fit("gaus", "Q", "", -0.8, 0.6);
  hAli6->Write();
  // hResCog->Fit("gaus", "Q", "", -1, 1);                                                                                                                                                  
  hResCog->Write();
  // hResCog3->Fit("gaus", "Q", "", -1, 1);                                                                                                                        
  hResCog3->Write();
  hResPara->Write();
  for (int layer=4; layer<8; layer++) {
    hOcc[layer]->Write();
    hTime[layer]->Write();
    hNumClu[layer]->Write();
  // for (int layer=4; layer<8; layer++) hn[layer]->Write();
  // for (int layer=4; layer<8; layer++) hN[layer]->Write();
    hClusCharge[layer]->Write();
    hEffi[layer]->Write();
    hClusCharge_Seg[layer]->Write();
    hClusCharge_Seg_4of4[layer]->Write();
    hClusCentroid_Seg[layer]->Write();
    hClusCentroid_Seg_4of4[layer]->Write();
    hMaxPdoStripCharge_Seg[layer]->Write();
    hMaxPdoStripCharge_Seg_4of4[layer]->Write();
    hNeighFlag0_Clus[layer]->Write();
    hNeighFlag1_Clus[layer]->Write();
    hMaxPdo_Vs_1stNeighPdo_Seg_4of4_2Calib[layer]->Write();
    hMaxPdo_Vs_2ndNeighPdo_Seg_4of4_2Calib[layer]->Write();
  }

  for (int i=0; i<4; i++) hResIn[i]->Write();
  for (int i=0; i<4; i++) hResEx[i]->Write();

  //===========================================================================================//
  //===========================================================================================//

  hNum_HTcells->Write();
  hSumAmp_HTcells->Write();

  hClusterWidth->Write();
  hMaxPdo->Write();
  hPeakPdo->Write();
  
  hNumClu_Seg->Write();
  hClusterWidth_Seg->Write();
  hMaxPdo_Seg->Write();
  hPeakPdo_Seg->Write();

  hNumPot->Write();
  hNumMask->Write();
  hNumBkg->Write();
  
  hNumPot_Seg->Write();
  hNumMask_Seg->Write();
  hNumBkg_Seg->Write();
  
  h_fracNumPot_Seg->Write();
  h_fracNumMask_Seg->Write();
  h_fracNumBkg_Seg->Write();
  h_fracNumPot_vs_Ang->Write();
  h_fracNumMask_vs_Ang->Write();
  h_fracNumBkg_vs_Ang->Write();
  h_fracNumPot_vs_Chi2->Write();
  h_fracNumMask_vs_Chi2->Write();
  h_fracNumBkg_vs_Chi2->Write();

  h_numEvwNoHitsLayers_Vs_numLayerswNoHits->Write();
  h_numEvwNoClusLayers_Vs_numLayerswNoClus->Write();
  
  h_ClusWidth_Vs_bkgChns->Write();
  h_Relbcid_Vs_DistMaxPdo->Write();
  h_Relbcid_Vs_DistMaxPdo_1seg3layer->Write();

  //for(int iev=0; iev<c_segments.size(); iev++){
  //  std::string event_dir = "Event"+std::to_string(iev)+"/";
  //  f->mkdir(event_dir.c_str());
  //  f->cd(event_dir.c_str());
  //  for(int iseg=0; iseg<c_segments[iev].size(); iseg++){
  //    c_segments[iev][iseg]->Write();
  //    delete c_segments[iev][iseg];
  //  }
  //}
  
  /*for(int iev=0; iev<c_evtDisplay.size(); iev++){
    std::string event_dir = "Event"+std::to_string(iev)+"/";
    f->mkdir(event_dir.c_str());
    f->cd(event_dir.c_str());
    c_evtDisplay[iev]->Write();
    delete c_evtDisplay[iev];
    }*/

  f->Close();
  //delete seg;
  //delete sg;
  delete [] buffer;  
  printf("number of events   = %d\n", eventNumber);
  //printf("number of segments = %d, %5.3f segments/event.", totSeg, totSeg/(double)eventNumber);
  
  //printf("number of rejected events = %d\n", NrejEvnts);
  printf("number of events with no HT cells filled = %d\n", NrejEvnts_noFilledHT);
  printf("number of events with no HT cells with at least 3 layers with at least one valid hit = %d\n", NrejEvnts_noHit1perlayer);
  printf("number of events with no small angle HT cells = %d\n", NrejEvnts_noSmallAngle);
  //printf("number of events with no HT cell with 3 layers having clusters = %d\n", NrejEvnts_noClusterHT);
  //printf("number of events with no HT cell with 3 layers having good clusters = %d\n", NrejEvnts_noGoodClusterHT);

  //printf("another cutflow: \n");
  printf("number of events having (number of layers with no hits >=2): = %d\n",NrejEvnts_noHitsLayers_ge2);
  printf("number of events having (number of layers with hits >=3): = %d\n",NselEvnts_HitsLayers_ge3);
  
  printf("Only for events with (number of layers with hits >=3): \n");
  printf("number of events having (number of layers with no good clusters >=2): = %d\n",NrejEvnts_noClusLayers_ge2);
  printf("number of events having (number of layers with good clusters >=3): = %d\n",NselEvnts_ClusLayers_ge3);
    
  printf("Only for events with (number of layers with good clusters >=3): \n");
  printf("number of events having no good segments: = %d\n", NrejEvnts_noSeg);
  printf("number of events having at least one good segment: = %d\n", NselEvnts_Seg);

  printf ("Encountered %i exceptions\n", errorCount);

}

