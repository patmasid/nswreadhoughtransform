/** @file gifcalib.cpp
This application reads 4 data files for pulser amplitudes of 250, 500, 750 
and 1000.
PDO measurements are filled into profile histograms and lines are fitted.
Results are stored in a tree and a txt file for the CalServer.
Used for sTGC GIF data.
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "TH2.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TProfile.h"
#include "NSWRead/NSWEvent.h"
#include "TCanvas.h"


/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer
  char fil[300], fils[300];
  int eventNumber, errorCount =0;
  FILE *infile;
  if(argc != 5){
    printf("Usage: nswcalib file250.data file500.data file750.data file1000.data\n");
    exit(0);
  }
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }
  eventNumber = 0;
  NSWEvent event;
  NSWSector* sec;
  
  /** \struct Calib
      defines variables in root tree.
      One entry per channel
  */
  struct Calib{
    /** elink index */
    int ind;
    /** vmm number */
    int vmm;
    /** channel number */
    int chn;
    /** layer */
    int layer;
    /** electrode type */
    int type;
    /** intercept */
    float inter;
    /** slope */
    float slope;
    /** intercept error*/
    float interErr;
    /** slope Error */
    float slopeErr;
    /** chi2 */
    float chi2;
  };
  Calib data;
  
  TCanvas *calib_fit[64][512];
  TProfile * pdo[64][512]; // pdo vs pulse per link and channel
  std::vector<int> pdo_vec[64][512][4];
  TH1F * inter[64]; // intercept
  TH1F * slope[64]; // slope
  TH1F * hbaseline[8];
  TH1F * hslope[8];

  FILE* outfile = fopen("pdocal_50ns_test.txt", "w");

  char name[80], title[80], label[40];
  NSWChannelID id[64][512];

  for (int layer=4; layer<8; layer++){ // GIF 4 layer
    for (unsigned int i=0; i<8; i++){
      int r = (i>3)? (i-2)/2 : 0;   // radius
      int g = (i>3)? 0 : i%2;       // group
      int e = (i>3)? i%2 : (i/2)%2; // endpoint
 
      //printf ("i = %2d, r = %2d, g = %d, layer = %d, label = '%s'\n", i, r, g, layer, label);
      for (int chn=0; chn<512; chn++){
	//id[i+8*layer][chn].SetTechnologyOld(0); // MM = 1, sTGC = 0
	////id[i+8*layer][chn].SetSector(sectors[0]); // only one sector for now 
	//id[i+8*layer][chn].SetRawSector(13-1); // only one sector for now 
	//id[i+8*layer][chn].SetEtaOld(13>0?0:1); // 0: eta=+1: endcap A, 1: eta=-1, endcap C 
	//id[i+8*layer][chn].SetDataTypeOld(0); // 0: L1A, 1: config, 2: monitor
	//id[i+8*layer][chn].SetRadius(r);
	//id[i+8*layer][chn].SetChannelGroup(g);
	//id[i+8*layer][chn].SetEndPointOld(e);
	//id[i+8*layer][chn].SetLayer(layer);
	id[i+8*layer][chn].GetBoardID(label);
	int vmm_num = chn/64;
	int channel = chn%64;
	//id[i+8*layer][chn].SetVMMChannel(channel);
        //id[i+8*layer][chn].SetVMM(vmm_num);
	//if(id[i+8*layer][chn].GetLayer()==7 && id[i+8*layer][chn].GetEndPoint()==1){
	//std::cout << "BEFORE ALL For databaseID: " << "GetIndex(): " << i+8*layer << " CH databaseID: " << id[i+8*layer][chn].GetDatabaseID() << " CH GetRadius(): " << id[i+8*layer][chn].GetRadius() << " CH GetVMM(): " << id[i+8*layer][chn].GetVMM() << " CH GetVMMChannel(): " << id[i+8*layer][chn].GetVMMChannel() << " CH GetEta(): " << id[i+8*layer][chn].GetEta() << " CH GetRawSector(): " << id[i+8*layer][chn].GetRawSector() << " CH GetLayer(): " << id[i+8*layer][chn].GetLayer() << " CH GetTechnology(): " << id[i+8*layer][chn].GetTechnology() << " CH GetEndPoint(): " << id[i+8*layer][chn].GetEndPoint() << std::endl;
	//}
	
	sprintf (name, "%s_ind%02d_chn%02d", label, i+8*layer, chn);
	sprintf (title, "PDO of %s channel %d;Pulser Amplitude;PDO", label, chn);
	pdo[i+8*layer][chn] = new   TProfile(name, title, 22, 25, 1125);
	pdo[i+8*layer][chn]->SetMarkerStyle(20);
      }
      sprintf (name, "inter%s_ind%02d", label, i+8*layer);
      sprintf (title, "Intercept %s;chn;intercept in PDO", label);
      inter[i+8*layer] = new   TH1F(name, title, 512, 0, 512);
      
      sprintf (name, "slope%s_ind%02d", label, i+8*layer);
      sprintf (title, "Slope %s;chn;slope", label);
      slope[i+8*layer] = new   TH1F(name, title, 512, 0, 512);
    } // next elink 
  } // next layer

  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".calib.root"), "recreate");//output file
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }
  f->cd();
  f->mkdir("Layer0");
  f->mkdir("Layer1");
  f->mkdir("Layer2");
  f->mkdir("Layer3");
  f->mkdir("Layer4");
  f->mkdir("Layer5");
  f->mkdir("Layer6");
  f->mkdir("Layer7");
  
  TTree *tree = new TTree("tree","Calib");
  tree->Branch("branch",&data, "ind/I:vmm:chn:layer:type:inter/F:slope:interErr:slopeErr:chi2");
  
  std::cout << "#######################################" << std::endl;
  std::cout << "Writing histograms: " << std::endl;
  std::cout << "#######################################" << std::endl;
  
  std::vector<int> dbid_list;

  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
    
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);
    }
    const double amp[4] = {250, 500, 750, 1000};
    double pulseAmp = amp[iFile-1];
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("ReadATLAS(): %s\n", ex.what());
	break;
      }
      try{
	event.ReadNSW(buffer, size); // read the data
	
	//Filling NSW histograms
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac = sec->GetPacket(j);
	    int ind = pac->GetIndex();
	    //std::cout << "pac->GetIndex(): " << ind << std::endl;
	    for (int k=0; k<pac->GetWidth(); k++){
	      NSWChannel *chn = pac->GetChannel(k);
	      //std::cout << "BEFORE: " << "CH databaseID: " << chn->GetDatabaseID() << " CH Layer: " << chn->GetLayer() << " CH type: " << chn->GetEndPoint() << " CH VMM: " << chn->GetVMM() << " CH channel: " << chn->GetVMMChannel() << " CH Channel Physical: " << chn->GetDetectorStrip() << std::endl;
	      //if (pac->GetLinkID() == 45953) pac->SetLinkID(0xB380); // _B -> _A 
	      //if (pac->GetLinkID() == 45955) pac->SetLinkID(45953);  // _D -> _B 
	      //std::cout << "AFTER: " << "CH databaseID: " << chn->GetDatabaseID() << " CH Layer: " << chn->GetLayer() << " CH type: " << chn->GetEndPoint() << " CH VMM: " << chn->GetVMM() << " CH channel: " << chn->GetVMMChannel() << " CH Channel Physical: " << chn->GetDetectorStrip() << std::endl;
	      int n = 64*chn->GetVMM()+chn->GetVMMChannel();
	      pdo[ind][n]->Fill(pulseAmp, chn->GetPDO());
	      pdo_vec[ind][n][iFile-1].push_back(chn->GetPDO());
	      
	      //id[ind][n].SetTechnologyOld(chn->GetTechnology()); // MM = 1, sTGC = 0 
	      //id[ind][n].SetSector(sectors[0]); // only one sector for now 
	      //id[ind][n].SetRawSector(chn->GetRawSector()); // only one sector for now 
	      //id[ind][n].SetEtaOld(chn->GetEta()); // 0: eta=+1: endcap A, 1: eta=-1, endcap C 
	      //id[ind][n].SetDataTypeOld(0); // 0: L1A, 1: config, 2: monitor 
	      //id[ind][n].SetEndPointOld(1); // 0: pad, 1: strip,  2: Trig. Proc, 3: Pad trig 
	      //id[ind][n].SetRadius(chn->GetRadius());
	      //id[ind][n].SetChannelGroup(g);
	      //id[ind][n].SetEndPointOld(e);
	      //id[ind][n].SetLayer(layer);
	      id[ind][n].GetBoardID(label);
	      id[ind][n].SetChannelID(pac->GetLinkID(),chn->GetVMM(),chn->GetVMMChannel());
	      
	      // writing databaseID map
	      /*if(std::find(dbid_list.begin(), dbid_list.end(), chn->GetDatabaseID())==dbid_list.end()){
		//fprintf (outfile, "%d %6d %6d %6d %6d %6d %6d %6d %6d %6d\n", chn->GetTechnology(), chn->GetRawSector(), chn->GetEta(), chn->GetRadius(), chn->GetLayer(), chn->GetEndPoint(), chn->GetDetectorStrip(), chn->GetConfigID(), chn->GetVMMChannel(), chn->GetDatabaseID()); 
		fprintf (outfile, "%d %6d %6d %6d %6d %6d %6d %6d %6d %6d\n", chn->GetTechnology(), chn->GetRawSector(), chn->GetEta(), chn->GetRadius(), chn->GetLayer(), chn->GetEndPoint(), chn->GetDetectorStrip(), chn->GetConfigID(), chn->GetVMMChannel(), chn->GetDatabaseID());
		dbid_list.push_back(chn->GetDatabaseID());
	      }
	      else
	      continue;*/
	      // writing databaseID map   ///

	      //std::cout << "CH Number: " << n << " CH Layer: " << chn->GetLayer() << " CH Index: " << ind << " CH ind/8: " << ind/8 << std::endl;

	      //if(chn->GetLayer()==7 && chn->GetEndPoint()==1){
		
		//std::cout << "CH Number: " << n << " CH Layer: " << chn->GetLayer() << " CH databaseID: " << chn->GetDatabaseID() << " CH type: " << chn->GetEndPoint() << " CH VMM: " << chn->GetVMM() << " CH channel: " << chn->GetVMMChannel() << " CH Channel Physical: " << chn->GetDetectorStrip() << " CH Pdo: " << chn->GetPDO() << std::endl;
	      //if(chn->GetEndPoint()==1){
	      //  std::cout << "BEFORE For databaseID: " << "GetIndex(): " << pac->GetIndex() << " CH databaseID: " << chn->GetDatabaseID() << " CH GetRadius(): " << chn->GetRadius() << " CH GetVMM(): " << chn->GetVMM() << " CH GetVMMChannel(): " << chn->GetVMMChannel() << " CH GetEta(): " << chn->GetEta() << " CH GetRawSector(): " << chn->GetRawSector() << " CH GetLayer(): " << chn->GetLayer() << " CH GetTechnology(): " << chn->GetTechnology() << " CH GetEndPoint(): " << chn->GetEndPoint() << std::endl;
	      //}
		//std::cout << "Writing Histograms: " << " Index: " << pac->GetIndex() << " GetRadius(): " << chn->GetRadius() << std::endl;
	      //}
	    } // next chn
	  } // next packet
	} // next sector
	
	eventNumber++;
	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	//if (eventNumber >= 10000) break;
      } // try     
      catch (NSWReadException& ex){
	printf ("ReadNSW() encountered an Exception for event %d\n", eventNumber);
	printf("%s\n", ex.what());
	errorCount++;
      }
    } while ((size>0)&&(eventNumber<100000*iFile));// next event
    fclose(infile);
  } // next file
  
  std::cout << "#######################################" << std::endl;
  std::cout << "Writing pdocal file: " << std::endl;
  std::cout << "#######################################" << std::endl;
  
  for(int layer=0; layer<8; layer++){
    std::string baseline_name ="hBaseline_layer"+std::to_string(layer);
    hbaseline[layer] = new TH1F(baseline_name.c_str(),baseline_name.c_str(),50,0,500);

    std::string slope_name ="hSlope_layer"+std::to_string(layer);
    hslope[layer] = new TH1F(slope_name.c_str(),slope_name.c_str(),50,0,500);
  }

  for (int ind=32; ind<64; ind++){ // GIF 4 layer
    data.ind = ind;
    data.layer = ind / 8;
    for (int vmm=0; vmm<8; vmm++){
      data.vmm = vmm;
      for (int chn = 0; chn<64; chn++){
	data.chn = chn;
	
	//Not Used
	//int i = ind%8;
	//int e = (i>3)? i%2 : (i/2)%2; // endpoint, 0: pad, 1: strip 
	//int r = (i>3)? (i-2)/2 : 0;   // radius
	//int g = (i>3)? 0 : i%2;       // group

	//id.SetRadius(r);
	//id.SetChannelGroup(g);
	//id.SetEndPointOld(e);
	//id.SetLayer(data.layer);
	//id.GetBoardID(label);
	//id.SetVMMChannel(chn);
	//id.SetVMM(vmm);
	
	data.type = id[ind][64*vmm+chn].GetEndPoint();
	if ((data.type==0) &&(vmm==2)) data.type = 2; // wire
	//if(id[ind][64*vmm+chn].GetLayer()==7 && id[ind][64*vmm+chn].GetEndPoint()==1){
	  //std::cout << "ind: " << ind << " CH Number: " << 64*vmm+chn << " CH databaseID: " << id[ind][64*vmm+chn].GetDatabaseID() << " CH Layer: " << id[ind][64*vmm+chn].GetLayer() << " CH type: " << id[ind][64*vmm+chn].GetEndPoint() << " CH VMM: " << id[ind][64*vmm+chn].GetVMM() << " CH channel: " << id[ind][64*vmm+chn].GetVMMChannel() << " CH Channel Physical: " << id[ind][64*vmm+chn].GetDetectorStrip() << " CH pdo entries: " << pdo[ind][64*vmm+chn]->GetEntries() << " CH amp 250: " << pdo_vec[ind][64*vmm+chn][0].size() << " CH amp 500: " << pdo_vec[ind][64*vmm+chn][1].size() << " CH amp 750: " << pdo_vec[ind][64*vmm+chn][2].size() << " CH amp 1000: " << pdo_vec[ind][64*vmm+chn][3].size() << std::endl;
	  //std::cout << "AFTER For databaseID: " << "GetIndex(): " << ind << " CH databaseID: " << id[ind][64*vmm+chn].GetDatabaseID() << " CH GetRadius(): " << id[ind][64*vmm+chn].GetRadius() << " CH GetVMM(): " << id[ind][64*vmm+chn].GetVMM() << " CH GetVMMChannel(): " << id[ind][64*vmm+chn].GetVMMChannel() << " CH GetEta(): " << id[ind][64*vmm+chn].GetEta() << " CH GetRawSector(): " << id[ind][64*vmm+chn].GetRawSector() << " CH GetLayer(): " << id[ind][64*vmm+chn].GetLayer() << " CH GetTechnology(): " << id[ind][64*vmm+chn].GetTechnology() << " CH GetEndPoint(): " << id[ind][64*vmm+chn].GetEndPoint() << std::endl;
	  //std::cout << "Writing pdocal file: " << "Index: " << ind << " GetRadius(): " << id[ind][64*vmm+chn].GetRadius() << std::endl;
	//}
	if (pdo[ind][64*vmm+chn]->GetEntries() > 0){
	  
	  pdo[ind][64*vmm+chn]->Fit("pol1", "Q");
	  TF1 *fit = pdo[ind][64*vmm+chn]->GetFunction("pol1");
	  data.inter = fit->GetParameter(0);	
	  data.slope = fit->GetParameter(1);
	  data.interErr = fit->GetParError(0);	
	  data.slopeErr = fit->GetParError(1);	
	  data.chi2 = fit->GetChisquare();
	  tree->Fill();
	  pdo[ind][64*vmm+chn]->Write();
	  inter[ind]->SetBinContent(1+chn+64*vmm, fit->GetParameter(0));
	  inter[ind]->SetBinError  (1+chn+64*vmm, fit->GetParError(0));
	  slope[ind]->SetBinContent(1+chn+64*vmm, fit->GetParameter(1));
	  slope[ind]->SetBinError  (1+chn+64*vmm, fit->GetParError(1));
	  fprintf (outfile, "%6d %6.3f %6.1f\n", id[ind][64*vmm+chn].GetDatabaseID(), data.slope, data.inter);
	  //fprintf (outfile, "%6d %6d %6d %6d %6d %6d %6d\n",id[ind][64*vmm+chn].GetRawSector(), id[ind][64*vmm+chn].GetEta(), id[ind][64*vmm+chn].GetRadius(), id[ind][64*vmm+chn].GetLayer(), id[ind][64*vmm+chn].GetEndPoint(), id[ind][64*vmm+chn].GetDetectorStrip(), id[ind][64*vmm+chn].GetDatabaseID());

	  int channel_no = 64*vmm+chn;
	  std::string c_name = "c_calib_fit_index"+std::to_string(ind)+"_chn"+std::to_string(channel_no)+"_dbid"+std::to_string(id[ind][64*vmm+chn].GetDatabaseID());
	  calib_fit[ind][64*vmm+chn] = new TCanvas(c_name.c_str(),c_name.c_str(),800,600);

	  if(id[ind][64*vmm+chn].GetEndPoint()==1 && id[ind][64*vmm+chn].GetDetectorStrip()>=0 && id[ind][64*vmm+chn].GetDetectorStrip()<408){
	    if(data.slope<0)
	      std::cout << "The strip " << id[ind][64*vmm+chn].GetDetectorStrip() << " with databaseid " << id[ind][64*vmm+chn].GetDatabaseID() << " has negative slope!!" << std::endl;
	    calib_fit[ind][64*vmm+chn]->cd();
	    pdo[ind][64*vmm+chn]->Draw();
	    fit->DrawF1(240,1400,"SAME");
	    std::string dir_name = "Layer"+std::to_string(data.layer);
	    f->cd(dir_name.c_str());
	    calib_fit[ind][64*vmm+chn]->Write();
	    hbaseline[data.layer]->Fill(data.inter);
	    hslope[data.layer]->Fill(data.slope);
	  }
	  
	  delete calib_fit[ind][64*vmm+chn];

	  //std::cout << " CH databaseID: " << id[ind][64*vmm+chn].GetDatabaseID() << " CH Layer: " << id[ind][64*vmm+chn].GetLayer() << " CH type: " << id[ind][64*vmm+chn].GetEndPoint() << " CH VMM: " << id[ind][64*vmm+chn].GetVMM() << " CH channel: " << id[ind][64*vmm+chn].GetVMMChannel() << " CH Channel Physical: " << id[ind][64*vmm+chn].GetDetectorStrip() << std::endl;
	  //std::cout << "For databaseID: " << " CH databaseID: " << id[ind][64*vmm+chn].GetDatabaseID() << " CH GetRadius(): " << id[ind][64*vmm+chn].GetRadius() << " CH GetVMM(): " << id[ind][64*vmm+chn].GetVMM() << " CH GetVMMChannel(): " << id[ind][64*vmm+chn].GetVMMChannel() << " CH GetEta(): " << id[ind][64*vmm+chn].GetEta() << " CH GetRawSector(): " << id[ind][64*vmm+chn].GetRawSector() << " CH GetLayer(): " << id[ind][64*vmm+chn].GetLayer() << " CH GetTechnology(): " << id[ind][64*vmm+chn].GetTechnology() << " CH GetEndPoint(): " << id[ind][64*vmm+chn].GetEndPoint() << std::endl;
	  
	  //if(id[ind][64*vmm+chn].GetLayer()==7 && id[ind][64*vmm+chn].GetEndPoint()==1){
	  //if(data.type == 2){
	  //std::cout << "AFTER For databaseID Wire: " << "GetIndex(): " << ind << " CH databaseID: " << id[ind][64*vmm+chn].GetDatabaseID() << " CH GetRadius(): " << id[ind][64*vmm+chn].GetRadius() << " CH GetVMM(): " << id[ind][64*vmm+chn].GetVMM() << " CH GetVMMChannel(): " << id[ind][64*vmm+chn].GetVMMChannel() << " CH GetEta(): " << id[ind][64*vmm+chn].GetEta() << " CH GetRawSector(): " << id[ind][64*vmm+chn].GetRawSector() << " CH GetLayer(): " << id[ind][64*vmm+chn].GetLayer() << " CH GetTechnology(): " << id[ind][64*vmm+chn].GetTechnology() << " CH GetEndPoint(): " << id[ind][64*vmm+chn].GetEndPoint() << std::endl;
	  //}
	  //if(data.type == 0){
	  //if(id[ind][64*vmm+chn].GetEndPoint()==1){
	  //  std::cout << "AFTER For databaseID : " << "GetIndex(): " << ind << " CH databaseID: " << id[ind][64*vmm+chn].GetDatabaseID() << " CH GetRadius(): " << id[ind][64*vmm+chn].GetRadius() <<" CH GetVMM(): " << id[ind][64*vmm+chn].GetVMM() << " CH GetVMMChannel(): " << id[ind][64*vmm+chn].GetVMMChannel() << " CH GetEta(): " << id[ind][64*vmm+chn].GetEta() << " CH GetRawSector(): " << id[ind][64*vmm+chn].GetRawSector() << " CH GetLayer(): " << id[ind][64*vmm+chn].GetLayer() << " CH GetTechnology(): " << id[ind][64*vmm+chn].GetTechnology() << " CH GetEndPoint(): " << id[ind][64*vmm+chn].GetEndPoint() << std::endl;
	  //}
	  
	}
      }
    }
  }
  for (int ind=32; ind<64; ind++) inter[ind]->Write();
  for (int ind=32; ind<64; ind++) slope[ind]->Write();
  for (int layer=0; layer<8; layer++){
    hbaseline[layer]->Write();
    hslope[layer]->Write();
    delete hbaseline[layer];
    delete hslope[layer];
  }
  f->Close();
  fclose(outfile);
  delete [] buffer;  
  //printf("\nTotal number of events = %d\n", eventNumber);
  //printf ("Encountered %i exceptions\n", errorCount);
  
  /*TLatex t4;
  t4.SetTextAlign(22);
  t4.SetTextColor(kBlack);
  t4.SetTextFont(43);
  t4.SetTextSize(40);
  std::string eq4 = "y = "+std::to_string(slope_L4)+"x + "+std::to_string(intercept_L4);
  t4.DrawLatex(t_x,t_y,eq4.c_str());*/

}

