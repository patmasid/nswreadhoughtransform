/** @file nswseg.cpp
This application fills a root tree with segment reconstruction results.
It can be used for monitoring, or for looking at hit reconstruction
results. The tree variables are stored in moni_t.

Reads nsw data file and produces monitoring histograms based on segments.
The tree is filled after cluster finding and there is one entry per segment.

@see moni.cpp for monitoring packet data
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include "TH2.h"
//#include" TGraph.h"
#include "TFile.h"
#include "TF1.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "NSWRead/NSWEvent.h"
#include "NSWRead/NSWClusterList.h"
#include "NSWRead/NSWSegmentList.h"

/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32

/** Draw a simple event display */
void DrawDisplay (NSWSector* sec, int gifNumber){
  TH1F *hamp[8], *htime[8];
  TH1F *hwire[3][8];
  TH1F *hpad[3][8];
  char name[40], title[80];
  char gifName[80];
  int l1id;
  int imax;
  if (sec->GetTechnology()==1){ // 1 = MM, 0=sTGC */
    imax = 8196;
  } else {
    imax = 1200;
  }
  for (int layer=0; layer<8; layer++){
    sprintf (name, "hamp%d", layer);
    sprintf (title, "PDO layer %d;channel", layer);
    hamp[layer] = new TH1F(name, title, imax, 0, imax);  
    sprintf (name, "htime%d", layer);
    sprintf (title, "Time layer %d;channel", layer);
    htime[layer] = new TH1F(name, title, imax, 0, imax);
  }
  for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(i);
    l1id = pac->GetL1ID();
    for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
      NSWChannel* chn = pac->GetChannel(j);
      if (chn->GetChannelType() == NSWChannel::STRIP){
	int strip = chn->GetDetectorStrip();
	int layer = chn->GetLayer();
	hamp[layer]->Fill(strip, chn->GetPDO());
	htime[layer]->Fill(strip, chn->GetPeakingTime());
      }
    }
  }
  TCanvas* can = new TCanvas ("can", "Event display", 1600, 1200);  
  can->Divide(1, 8);
  //gStyle->SetOptStat(0); 
  for (int layer=0; layer<8; layer++){
    can->cd(layer+1);
    hamp[layer]->GetXaxis()->SetLabelSize(0.1);
    hamp[layer]->GetYaxis()->SetLabelSize(0.1);
    hamp[layer]->GetYaxis()->SetRangeUser(0, 1000);
    hamp[layer]->SetFillColor(2);
    hamp[layer]->Draw("HIST");
    htime[layer]->SetMarkerColor(2);
    htime[layer]->SetMarkerStyle(7);
    htime[layer]->Draw("psame");
  }
  can->cd(0);
  TPaveText t(0.4,0.94,0.6,0.99,"NDC");
  char line[40];
  sprintf (line, "Strips L1ID = %d", l1id);
  t.SetTextSizePixels(8);
  t.AddText(line);
  t.SetFillColor(0);
  t.Draw();

  sprintf (gifName, "ed%04ds.gif", gifNumber);
  can->SaveAs(gifName);

  // wires:
  if (sec->GetTechnology()==0){ //  1 = MM, 0=sTGC
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++){
	sprintf (name, "hwire%d%d", q, layer);
	sprintf (title, "Wire PDO Q%d layer %d;wire", q, layer);
	int range = 20+10*q;
	hwire[q][layer] = new TH1F(name, title, range, 0, range);  
      }
    }
    for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
      NSWPacket *pac =sec->GetPacket(i);
      l1id = pac->GetL1ID();
      for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
	NSWChannel* chn = pac->GetChannel(j);
	if (chn->GetChannelType() == NSWChannel::WIRE){
	  int wire = chn->GetDetectorStrip();
	  int layer = chn->GetLayer();
	  int q = chn->GetRadius();
	  hwire[q][layer]->Fill(wire, chn->GetPDO());
	}
      }
    }
    can->Clear();
    can->Divide(3, 8);
      for (int layer=0; layer<8; layer++){
	for (int q=0; q<3; q++){
	  can->cd(q+3*layer+1);
	  hwire[q][layer]->GetXaxis()->SetLabelSize(0.1);
	  hwire[q][layer]->GetYaxis()->SetLabelSize(0.1);
	  hwire[q][layer]->GetYaxis()->SetRangeUser(0, 1000);
	  hwire[q][layer]->SetFillColor(2);
	  hwire[q][layer]->Draw("HIST");
	}
      }
    can->cd(0);
    TPaveText t(0.4,0.94,0.6,0.99,"NDC");
    char line[40];
    sprintf (line, "Wires L1ID = %d", l1id);
    t.AddText(line);
    t.SetFillColor(0);
    t.Draw();
    sprintf (gifName, "ed%04dw.gif", gifNumber);
    can->SaveAs(gifName);

    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++) delete hwire[q][layer];
    }
    
    // pads:
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++){
	sprintf (name, "hpad%d%d", q, layer);
	sprintf (title, "Pads PDO Q%d layer %d;pad", q, layer);
	int range = 120;
	hpad[q][layer] = new TH1F(name, title, range, 0, range);  
      }
    }
    for (int i=0; i<sec->GetNumPacket(); i++){ // loop over packets
      NSWPacket *pac =sec->GetPacket(i);
      l1id = pac->GetL1ID();
      for (int j=0; j<pac->GetWidth(); j++){ // loop over channels
	NSWChannel* chn = pac->GetChannel(j);
	if (chn->GetChannelType() == NSWChannel::PAD){
	  int pad = chn->GetDetectorStrip();
	  int layer = chn->GetLayer();
	  int q = chn->GetRadius();
	  hpad[q][layer]->Fill(pad, chn->GetPDO());
	}
      }
    }
    can->Clear();
    can->Divide(3, 8);
      for (int layer=0; layer<8; layer++){
	for (int q=0; q<3; q++){
	  can->cd(q+3*layer+1);
	  hpad[q][layer]->GetXaxis()->SetLabelSize(0.1);
	  hpad[q][layer]->GetYaxis()->SetLabelSize(0.1);
	  hpad[q][layer]->GetYaxis()->SetRangeUser(0, 1000);
	  hpad[q][layer]->SetFillColor(2);
	  hpad[q][layer]->Draw("HIST");
	}
      }
    can->cd(0);
    TPaveText t2(0.4,0.94,0.6,0.99,"NDC");
    //char line[40];
    sprintf (line, "Pads L1ID = %d", l1id);
    t2.AddText(line);
    t2.SetFillColor(0);
    t2.Draw();
    sprintf (gifName, "ed%04dp.gif", gifNumber);
    can->SaveAs(gifName);
    for (int layer=0; layer<8; layer++){
      for (int q=0; q<3; q++) delete hpad[q][layer];
    }
  } //if sTGC
    
  delete can;
  for (int layer=0; layer<8; layer++){
    delete  htime[layer];
    delete  hamp[layer];
  }
}
  
/** read  data file, produce root file for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer


  char fil[300], fils[300];
  int eventNumber = 0;
  int errorCount = 0;

  FILE *infile;

  if(argc <2){
    printf("Usage: seg file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }


  NSWEvent event;
  NSWSector* sec;
  NSWClusterList clusterList;
  
  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".seg.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }

  /** \struct moni_t
      defines variables in root tree.
      One entry per ROD cluster.
  */
  struct moni_t{
    /** event number, increments over all files. */
    int event;
    /** Level one ID */
    int l1id;
    /** number of precision layer clusters */
    int nprec;
    /** number of clusters */
    int nclu;
    /** number of channels of this cluster */
    int wid[8];
    /** cluster parameters:*/
    float cog[8], amp[8];
    /** better cog*/
    float cog2[8];
    /** cluster charge*/
    float charge[8];
    /** z position */
    float z[8];
    /** line fit */
    float inter, slope, chi2, angle;
    /** predicted positon and residual */
    float pred[8], res[8];
    /** median y positon residual */
    float y;
    /** PDO and TDO per channel*/
    float adc[8][10], time[8][10];
    int gap[8];
  };
  moni_t branch;


  TTree *tree = new TTree("tree","Clusters");
  tree->Branch("branch",&branch,"event/I:l1id:nprec:nclu:wid[8]:cog[8]/F:amp[8]:cog2[8]:charge[8]:z[8]:inter:slope:chi2:angle:pred[8]:res[8]:y:adc[8][10]:time[8][10]:gap[8]/I");
  TH1F* hSeg = new TH1F ("hSeg", "Segments per event;segments", 100, 0, 100);
  TH1F* hClu = new TH1F ("hClu", "Clusters per event;clusters", 100, 0, 100);
  TH1F* hDist = new TH1F ("hDist", "intercept distance;strips", 200, -100, 100);
  TH1F* hNum[8];

  TH1F * hOcc[8];
  char name[80], title[80];
  for (int i=0; i<8; i++) {
    sprintf (name, "hOcc%d", i);
    sprintf (title, "Cluster position of layer %d;Position in mm", i);
    hOcc[i] = new TH1F (name, title, 1000, 0, 4000);

    sprintf (name, "hNum%d", i);
    sprintf (title, "Number of clusters in layer %d", i);
    hNum[i] = new TH1F(name, title, 200, 0, 200);
  }

	  
  int totSeg = 0;
  int numSeg = 0; // per event
  int gifNumber = 0;
  
  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data

	//fill hitlist with cluster data
	branch.event = eventNumber;
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  branch.l1id = sec->GetLevel1ID();
	  clusterList.Clear();
	  clusterList.Fill(sec, 20, 2);
	  int nClu = 0;
	  for (int layer=0; layer<8; layer++){
	    nClu += clusterList.GetNumClusters(layer);
	    hNum[layer]->Fill(clusterList.GetNumClusters(layer));
	    for (int i=0; i<clusterList.GetNumClusters(layer); i++){
	      NSWCluster *c = clusterList.GetCluster(layer, i);
	      hOcc[layer]->Fill(c->GetCOG());
	    }
	  }
	  hClu->Fill(nClu);
	  if ((gifNumber<100)&&(nClu>3)) {
	    DrawDisplay (sec, gifNumber);
	    gifNumber++;
	  }
	  //if (clusterList.GetNumClusters(0) < 1) continue;
	  //if (clusterList.GetNumClusters(1) < 1) continue;
	  //if (clusterList.GetNumClusters(2) < 1) continue;
	  //if (clusterList.GetNumClusters(3) < 1) continue;
	  //if (clusterList.GetNumClusters(4) < 1) continue;
	  //if (clusterList.GetNumClusters(5) < 1) continue;
	  //if (clusterList.GetNumClusters(6) < 1) continue;
	  //if (clusterList.GetNumClusters(7) < 1) continue;
				
	  if (clusterList.GetNumClusters(0) > 5) continue;
	  if (clusterList.GetNumClusters(1) > 5) continue;
	  if (clusterList.GetNumClusters(2) > 5) continue;
	  if (clusterList.GetNumClusters(3) > 5) continue;
	  if (clusterList.GetNumClusters(4) > 5) continue;
	  if (clusterList.GetNumClusters(5) > 5) continue;
	  if (clusterList.GetNumClusters(6) > 5) continue;
	  if (clusterList.GetNumClusters(7) > 5) continue;

	  if (nClu > 20) continue;

	  NSWSegmentList segmentList;
	  segmentList.Clear();
	  segmentList.Fill (&clusterList, 10);
	  for (int i=0; i<segmentList.GetNumSegments(); i++){
	    NSWSegment* s = segmentList.GetSegment(i);
	    branch.y =  s->GetYPosition();
	    for (int layer=0; layer<8; layer++){
	      NSWCluster* c = s->GetCluster(layer);
	      if (c == NULL){
		branch.cog[layer] = -100;
		branch.cog2[layer] = -100;
		branch.charge[layer] = -100;
		branch.wid[layer] = -1;
		branch.amp[layer] = -100;
		branch.z[layer] = -1;
		branch.pred[layer] = -100;
		branch.res[layer] = -100;
		for (int i=0; i<10; i++){
		  branch.adc[layer][i] = -1;
		  branch.time[layer][i] = -1;
		}
		branch.gap[layer] = -1;
	      } else {
		branch.cog[layer] = c->GetCOG();
		branch.cog2[layer] = c->GetCOG2();
		branch.charge[layer] = c->GetCharge();
		branch.wid[layer] = c->GetNumChannel();
		branch.amp[layer] = c->GetAverageAmplitude();
		branch.z[layer] = c->GetZPosition();
		branch.pred[layer] = s->GetIntercept() + s->GetSlope() * c->GetZPosition();
		branch.res[layer] = c->GetCOG() - branch.pred[layer];
		int chnMax = c->GetNumChannel();
		if (chnMax > 10) chnMax = 10;
		for (int j=0; j<chnMax; j++){
		  NSWChannel *chn = c->GetChannel(j);
		  if (chn != NULL) {
		    branch.adc[layer][j] = chn->GetAmplitude();
		    branch.time[layer][j] = chn->GetPeakingTime();
		  } else {
		    branch.adc[layer][j] = -1;
		    branch.time[layer][j] = -1;
		  }
		}
		for (int j=chnMax; j<10; j++){
		  branch.adc[layer][j] = -1;
		  branch.time[layer][j] = -1;
		}
		branch.gap[layer] = c->GetNumMissing();
	      }
	    } // next layer
	    
	    branch.inter = s->GetIntercept();
	    branch.slope = s->GetSlope();
	    branch.chi2 = s->GetChi2();
	    double pitch = 0.45;
	    if (s->GetIntercept()>5120) pitch = 0.5;
	    branch.angle = s->GetAngle();
	    branch.nprec = s->GetNumPrecision();
	    branch.nclu = s->GetNumCluster();
	    
	    tree->Fill(); 
	    numSeg++;
	    totSeg++;
	    for (int j=i+1; j<segmentList.GetNumSegments(); j++){
	      NSWSegment* s2 = segmentList.GetSegment(j);
	      hDist->Fill(s->GetIntercept() - s2->GetIntercept());
	      //printf ("%d: %f %f\n", j, s->GetIntercept() , s2->GetIntercept());
	    }
	  } // next segment
	  //if (segmentList.GetNumSegments()==8) segmentList.Print();
	  hSeg->Fill(segmentList.GetNumSegments());
	  
	} // next sector

	if (eventNumber%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
	eventNumber++;
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0); // next event
    fclose(infile);
  } // next file
  tree->Write();

  hClu->Write();
  hSeg->Write();
  hDist->Write();
  for (int layer=0; layer<8; layer++){
    hOcc[layer]->Write();
    hNum[layer]->Write();
  }
  f->Close();
  delete [] buffer;  
  printf("number of events   = %d\n", eventNumber);
  printf("number of segments = %d, %5.3f segments/event.", totSeg, totSeg/(double)eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}

