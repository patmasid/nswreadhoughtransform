/** @file nswnoise.cpp
This application converts a nsw raw data file to a wav sound file.
It can be used for noise testing.
 
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"NSWRead/NSWEvent.h"
// install sndfile-dev:
#include<sndfile.h>



/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


/** read pedestals and data file, produce root file of single hits 
for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer

  char fil[300], fils[300];
  int eventNumber, errorCount = 0;
  int chnCount = 0;
  FILE *infile;

  if(argc <3){
    printf("Usage: nsw2wav samplingRate file1.data [file2.data]...\n");
    exit(0);
  }
  int sampleRate;
  sscanf(argv[1], "%d", &sampleRate);
  printf ("sample rate %d Hz\n", sampleRate);
                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  // maximum 1 million samples, 1 event per sample
#define	SAMPLE_COUNT 1000000       

  SNDFILE	*wavfile ;
  SF_INFO	sfinfo ;

  short int * wavbuf = new  short int [SAMPLE_COUNT];
  if (wavbuf == NULL){
    printf ("Cannot allocate wav buffer.\n");
    exit(0) ;
  } 
  
  

  eventNumber = 0;

  NSWEvent event;
  NSWSector* sec;

  strcpy(fils,argv[2]);
  fils[(strlen(argv[2])-5)] = 0;
  strcpy(fil,fils);



  for (int iFile=2; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadATLAS(): %s\n", ex.what());
	}
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data
	int totADC = 0;
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac =sec->GetPacket(j);
	    for (int k=0; k<pac->GetWidth(); k++){
	      NSWChannel* chn = pac->GetChannel(k);
	      totADC += chn->GetPDO();
	    } // next channel
	  } // next packet
	} // next sector
	wavbuf[eventNumber] = totADC;
	eventNumber++;
	if (eventNumber%1000==0)  printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while ((size>0) && (eventNumber<SAMPLE_COUNT));// next event
    fclose(infile);
  } // next file
  
  delete [] buffer;  
  printf("\nTotal number of events = %d, duration = %5.1f seconds.\n", eventNumber, (float)eventNumber/SAMPLE_COUNT);
  printf ("Encountered %i exceptions\n", errorCount);

  // write wav file:
  memset (&sfinfo, 0, sizeof (sfinfo)) ;
  sfinfo.samplerate	= sampleRate;
  sfinfo.frames		= eventNumber;
  sfinfo.channels		= 1;
  sfinfo.format		= (SF_FORMAT_WAV | SF_FORMAT_PCM_16);

  
  if (! (wavfile = sf_open (strcat(fils,".wav"), SFM_WRITE, &sfinfo))){
    printf ("Error : Not able to open output file '%s'.\n", fils);
    delete [] wavbuf;
    exit(0);
  }

  int n = sf_write_short (wavfile, wavbuf, eventNumber);
  if (n!= eventNumber){
    puts (sf_strerror (wavfile)) ;
  }
  sf_close (wavfile);
  delete [] wavbuf;
  exit(1);
}


