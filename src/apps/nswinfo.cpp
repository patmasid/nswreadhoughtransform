/** \file nswinfo.cpp
This program reads NSWEvent data and prints a summary
*/

#include <stdio.h>
#include <vector>
#include <bits/stdc++.h> 
#include "NSWRead/NSWEvent.h"

#define maxSize 21000000/4  // 4000*32   // for event buffer

std::vector<unsigned int> ids;

typedef struct Statistic{ // statistics for packets
  /** link ID number */
  int linkID;
  /** count packets */
  int count;
  /** Number of channels */
  int wid;
  /** Number of null packets */
  int nulls;
  /** number of packets with missing VMMs */
  int missing;
  /** layer and radius on the chamber */
  int layer, radius;
  /** checksum errors */
  int checksum;
  /** parity errors */
  int parity;
  /** BCID mismatch */
  int bcid;
  /** L1ID mismatch */
  int l1id;
  /** trailer length does not match packet size */
  int length;
  /** invalid packet size */
  int size;
} Stat;


// Compares two IDs for sorting according to staring layer, radius, and group. 
bool CompareID(Stat* s1, Stat* s2) { 
  NSWChannelID id1(s1->linkID);
  NSWChannelID id2(s2->linkID);
  int tech1 = id1.GetTechnology();
  int tech2 = id2.GetTechnology();
  if (tech1 != tech2) return (tech1 < tech2);
  return (id1.GetIndex() < id2.GetIndex()); 
} 

int FindDuplicateIDs(int event){
  char boardID[20];
  int dup = 0;
  NSWChannelID id;
  if (ids.size() == 0) return 0;
  /* search for duplicate link IDs in the sorted vector: */
  std::sort(ids.begin(), ids.end());

  for (unsigned int i=0; i<ids.size()-1; i++){ 
    if (ids[i] == ids[i+1]) {
      dup++;
      id.SetChannelID(ids[i]);
      id.GetBoardID(boardID);
      printf ("               Duplicate link ID in event %d: %6i=%17s\n", event, ids[i], boardID);
    }
  }
  return dup;
}


int main(int argc, char *argv[]){

  FILE* infile;
  NSWEvent event;
  int eventCount = 0;
  int errorCount = 0;
  int duplicateCount = 0;
  int checksumCount = 0;
  int dummyCount = 0;
  int size ; // event size
  unsigned int oldL1ID = 0xFFFFFFFF;
  unsigned int * buffer;
  unsigned int l1idErrors = 0;
  std::vector<Stat*> stat;


 if (argc==1) {
    printf ("usage: nswinfo infile\n");
    printf ("reads NSWEvent data and prints a summary.\n");
    return 1;
  }

  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

  infile = fopen (argv[1], "rb");
  if (!infile) {
    printf ("Cannot open '%s'\n", argv[1]);
    return 1;
  }

  int packetCount = 0;
  printf ("\n");
  
  do{ //event loop
    try{
      size = event.ReadATLAS(infile, buffer, maxSize);
    }
    catch (NSWReadException& ex){
       if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
      break;
    }

    try {
      event.ReadNSW(buffer, size);
      int eventBad = 0;
      //printf("Event %5d with %d fragments:\n", eventCount, event.GetNumSector());
      int eventBCID = event.GetBCID();
      int eventL1ID = event.GetL1ID();
      for (int i=0; i<event.GetNumSector(); i++){
	NSWSector* sec = event.GetSector(i);
	//printf ("L1ID %X: Fragment %X with %d packets.\n", event.GetL1ID(), sec->GetSourceID(), sec->GetNumPacket());
	ids.clear(); // count and store elink IDs
	if (sec->IsDummy()) dummyCount++;
	for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	  NSWPacket *pac =sec->GetPacket(j);
	  int linkID = pac->GetLinkID();
	  packetCount++;
	  ids.push_back(linkID);
	  int index = -1;
	  for (unsigned int k=0; k<stat.size(); k++) {
	    if (linkID == stat[k]->linkID) {
	      index = k;
	      break;
	    }
	  }
	  if (index == -1) { // new stat
	    Stat* s = new Stat;
	    s->linkID = linkID;
	    s->count = 1;
	    s->wid = pac->GetWidth();
	    s->nulls = pac->IsNull();
	    s->missing = pac->GetMissing(true)>0?1:0;
	    s->layer = pac->GetLayer();
	    s->radius = pac->GetRadius();
	    s->checksum = 0;
	    s->bcid = !pac->CheckBCID(eventBCID);
	    s->l1id = !pac->CheckL1ID(eventL1ID);
	    s->length = !pac->CheckLength();
	    s->size = !pac->CheckSize();
	    if (pac->TestChecksum()!=0) {
	      s->checksum++;
	      checksumCount ++;
	    }
	    s->parity = pac->GetParityErrors();
	    stat.push_back(s);
	  } else { // add to old stat
	    stat[index]->count++;
	    stat[index]->wid += pac->GetWidth();
	    stat[index]->nulls += pac->IsNull();
	    stat[index]->missing += (pac->GetMissing(true)>0?1:0);

	    stat[index]->bcid += !pac->CheckBCID(eventBCID);
	    stat[index]->l1id += !pac->CheckL1ID(eventL1ID);
	    stat[index]->length += !pac->CheckLength();
	    stat[index]->size += !pac->CheckSize();
	    if (pac->TestChecksum()!=0) {
	      stat[index]->checksum++;
	      checksumCount ++;
	    }
	    stat[index]->parity += pac->GetParityErrors();
	    if (stat[index]->count > eventCount+1) eventBad++;
	  }
	} // next packet
	int n = FindDuplicateIDs(eventCount);
	duplicateCount += n;
      } // next sector
      unsigned int l1id = event.GetL1ID();
      /***********************************
      if (((l1id & 0xFFFFFF) != ((oldL1ID+1) & 0xFFFFFF)) &&
	  ((l1id >> 24) != ((oldL1ID+0x01000000) >> 24))) {
	printf ("               Bad L1ID sequence for event %6d: L1ID = 0x%08X, previous L1ID = 0x%08X.\n", eventCount, l1id, oldL1ID);
	l1idErrors ++;
      }
      ***********************************/
      oldL1ID = l1id;
      if (eventCount%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventCount);
    }
    catch (NSWReadException& ex){
      if (ex.GetSeverity() > SEV_INFO) {
	printf ("ReadNSWException: ");
	printf("%s\n", ex.what());
	errorCount++;
      }
    }
    eventCount++;
  } while (size>0); // next event

  fclose (infile);
  delete [] buffer;
  printf ("Read %i events with %i packets, %10.6f packets/event and %d dummy events\n", eventCount, packetCount, packetCount/(double)eventCount, dummyCount);
  printf ("Encountered %d exceptions, %d checksum errors, %d L1ID sequence errors and %d duplicate packet IDs.\n", errorCount, checksumCount, l1idErrors, duplicateCount);

  std::sort (stat.begin(), stat.end(), CompareID);
  printf ("        LinkID                  L  R     lost   width  nullPackets %%  checksum parity mis.VMM L1ID    BCID   len size\n");
  for (unsigned int i=0; i<stat.size(); i++){
    char boardID[20];
    NSWChannelID id;
    id.SetChannelID(stat[i]->linkID);
    id.GetBoardID(boardID);
    int nonNull = stat[i]->count-stat[i]->nulls;
    double aveChn;
    if (nonNull >0){
      aveChn =  stat[i]->wid/(double)nonNull;
    } else {
      aveChn = 0;
    }
    printf ("%3d: %08X=%17s %i %2i %8i %6.2f %8i %5.0f     %6i", i+1, stat[i]->linkID, boardID, stat[i]->layer, stat[i]->radius, eventCount-stat[i]->count, aveChn, stat[i]->nulls, 100*stat[i]->nulls/(float)stat[i]->count, stat[i]->checksum);
    printf (" %6i %6i    %4i %6i %4i %4i\n", stat[i]->parity, stat[i]->missing, stat[i]->l1id, stat[i]->bcid, stat[i]->length, stat[i]->size);
  }
  for (unsigned int i=0; i<stat.size(); i++) delete  stat[i];
  stat.clear();
}
