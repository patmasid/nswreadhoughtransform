/** @file nswnoise.cpp
This application fills a root tree with nsw noise related quantities.
It can be used for monitoring.
The tree variables are stored in moni_t.


 
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"TH2.h"
//#include"TGraph.h"
#include"TFile.h"
#include"TF1.h"
#include"TTree.h"
#include"TProfile.h"
#include"NSWRead/NSWEvent.h"



/** Buffer size for an entire ATLAS event. */
#define maxSize 21000000/4 // 4000*32   // for event buffer

/** allocate space for all 32 chambers */
#define maxChamber 32


/** read pedestals and data file, produce root file of single hits 
for monitoring. */
int main(int argc, char *argv[]){
  unsigned int * buffer; // for ATLAS event buffer

  char fil[300], fils[300];
  int eventNumber, errorCount = 0;
  int chnCount = 0;
  FILE *infile;

  if(argc <2){
    printf("Usage: nswnoise file1.data [file2.data]...\n");
    exit(0);
  }

                        
  buffer = new unsigned int [maxSize];
  if (buffer == NULL) {
    printf ("Cannot allocate event buffer.\n");
    return 1;
  }

                                                        

  eventNumber = 0;

  NSWEvent event;
  NSWSector* sec;

  strcpy(fils,argv[1]);
  fils[(strlen(argv[1])-5)] = 0;
  strcpy(fil,fils);
  TFile *f = new TFile(strcat(fils,".noise.root"), "recreate");//make output file naming convention 
  if (!f) {
    printf ("Cannot open root file!\n");
    return -1;
  }

  /** \struct data_p
      defines variables in root tree.
      One entry per packet.
  */
  struct data_p{
    /** event number, increments over all files. */
    int event;
    /** global event ID from ATLAS */
    int evt;
    /** Level one ID */
    int l1id;
    /** bcid */
    int bcid;
    /** number of channels per packet */
    int wid;
    /** number of channels per packet from trailer */
    int len;
    /** link ID number */
    int linkID;
    /** missing VMMs in hit trailer */
    int missing;
    /** board layer */
    int layer;
    /** board radius */
    int radius;
    /** packet length in bytes */
    int bytes;
    /** elink index */
    int index;
  };
  data_p branch;


  /** \struct data_e
      defines variables in root tree.
      One entry per sector (event).
  */
  struct data_e{
    /** event number, increments over all files. */
    int event;
    /** global event ID from ATLAS */
    int evt;
    /** Level one ID */
    int l1id;
    /** number of packets */
    int npac;
    /** number of channels per event */
    int totchn;
    /** number of channels per layer */
    int numchn[8];
    /** total number of bytes in packets */
    int totbytes;
    /** number of active channels per event, ADC>100 */
    int active;
    /** sum of all ADC values per event */
    float totadc;
  };
  data_e edata;

  /** \struct data_c
      defines variables in root tree.
      One entry per channel (event) from histogram of pdo values.
  */
  struct data_c{
    /** layer number. */
    int layer;
    /** channel number */
    int chn;
    /** Occupancy = Entries / events */
    float occ;
    /** mean */
    float mean;
    /** rms */
    float rms;
    /** skewness */
    float skew;
  };
  data_c cdata;

  TTree *tree = new TTree("tree","Monitoring");
  tree->Branch("branch",&branch,"event/I:evt:l1id:bcid:wid:len:linkID:missing:layer:radius:bytes:index");

  TTree *etree = new TTree("etree","Events");
  etree->Branch("branch",&edata, "event/I:evt:l1id:npac:totchn:numchn[8]:totbytes:active:totadc/F");
  
  TTree *ctree = new TTree("ctree","Channels");
  ctree->Branch("branch",&cdata, "layer/I:chn:occ/F:mean:rms:skew");
  
  TH1F * bcidH = new TH1F ("bcidH", "BCID per Event", 3500, 0, 3500);
  TH1F * totchnH = new TH1F ("totchnH", "Total channels per event;channels", 3500, 0, 3500);
  TH1F * npacH = new TH1F ("npacH", "Packets per event;packets", 200, 0, 200);
  TH1F * pdoH = new TH1F ("pdoH", "PDO;ADC count", 1024, 0, 1024);
  TH1F * loccH = new TH1F ("loccH", "log(occupancy);log(occ)", 200, -20, 0);
  TH1F * occ[8];
  TH2F * time2d[8], *amp2d[8], *cor2d[8];
  TH1F * pdo[8][8192];
  char name[80], title[80];
  for (int layer=0; layer<8; layer++) {
    sprintf (name, "occ%d", layer);
    sprintf (title, "Occupancy of layer %d;channel;Occupancy", layer);
    occ[layer] = new TH1F (name, title, 8192, 0, 8192);
    sprintf (name, "time2d%d", layer);
    sprintf (title, "Drift time of layer %d;strip;time in ns", layer);
    time2d[layer] = new TH2F (name, title, 8192, 0, 8192, 512, -25, 7*25);
    sprintf (name, "amp2d%d", layer);
    sprintf (title, "Amplitude of layer %d;strip;amplitude", layer);
    amp2d[layer] = new TH2F (name, title, 8192, 0, 8192, 512, 0, 1024);
    for (int i=0; i<8192; i++){
      sprintf (name, "pdol%dchn%04d", layer, i);
      sprintf (title, "PDO of layer %d chn %d;PDO", layer, i);
      pdo[layer][i] = new TH1F (name, title, 1024, 0, 1024);
    }
  }


  for (int iFile=1; iFile<argc; iFile++){
    printf ("opening file '%s'.\n\n", argv[iFile]);
    infile = fopen(argv[iFile],"rb");//open data file
  
    if (infile==NULL) { //error if file incorrect or DNE
      printf("Can't open input file '%s'.\n",argv[iFile]);
      exit(-1);	
    }
 
    
    // start processing events:
    int size; // actual size of the ATLAS event
    do{ //event loop
      try{
	size = event.ReadATLAS(infile, buffer, maxSize);
      }
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadATLAS(): %s\n", ex.what());
	}
	break;
      }
      try{ 
 	event.ReadNSW(buffer, size);           // read the data
	if (event.GetNumSector()>0) bcidH->Fill(event.GetSector(0)->GetBCID());

	//fill hitlist with cluster data
	branch.event = eventNumber;
	edata.event = eventNumber;
	branch.evt = event.GetGlobalID();
	edata.evt = event.GetGlobalID();
	edata.totchn = 0;
	edata.totadc = 0;
	edata.totbytes = 0;
	edata.active = 0;
	for (int i=0; i<event.GetNumSector(); i++){
	  NSWSector* sec = event.GetSector(i);
	  branch.bcid = sec->GetBCID();
	  if (sec->GetActiveStrips(0)>3500) continue;
	  totchnH->Fill(sec->GetActiveStrips(0));
	  edata.npac = sec->GetNumPacket();
	  edata.l1id = event.GetL1ID();
	  edata.active +=  sec->GetActiveStrips(100);
	  for (int layer=0; layer<8; layer++) edata.numchn[layer] = 0;
	  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
	    NSWPacket *pac =sec->GetPacket(j);
	    branch.l1id = pac->GetL1ID();
	    branch.len = pac->GetLength();
	    branch.wid = pac->GetWidth();
	    edata.totchn  += pac->GetWidth();
	    branch.linkID = pac->GetLinkID();
	    branch.missing = pac->GetMissing();
	    branch.layer = pac->GetLayer();
	    branch.radius = pac->GetRadius();
	    branch.index = pac->GetIndex(); // 0..159
	    branch.bytes = pac->GetLinkBytes();
	    edata.totbytes += pac->GetLinkBytes();
	    int layer = pac->GetLayer();
	    edata.numchn[layer] += pac->GetWidth();
	    for (int k=0; k<pac->GetWidth(); k++){
	      NSWChannel* chn = pac->GetChannel(k);
	      occ[layer]->Fill(chn->GetDetectorStrip());
	      time2d[layer]->Fill(chn->GetDetectorStrip(), chn->GetPeakingTime());
	      amp2d [layer]->Fill(chn->GetDetectorStrip(), chn->GetPDO());
	      pdoH->Fill(chn->GetPDO());
	      edata.totadc += chn->GetPDO();
	      int s = chn->GetDetectorStrip();
	      if (s>=0) pdo[layer][s]->Fill(chn->GetPDO());
	      chnCount++;
	    } // next channel
	    tree->Fill();
	  } // next packet
	} // next sector
	etree->Fill();

	eventNumber++;
	if (eventNumber%1000==0)  printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventNumber);
      } // try     
      catch (NSWReadException& ex){
	if (ex.GetSeverity() > SEV_INFO) {
	  printf ("ReadNSWException: ");
	  printf("%s\n", ex.what());
	  errorCount++;
	}
      }
    } while (size>0);// next event
    fclose(infile);
  } // next file
  
  for (int layer=0; layer<8; layer++) { // compute occupancy per channel
    for (int i=0; i<8192; i++){
      double o = occ[layer]->GetBinContent(1+i);
      if (o>1) loccH->Fill(log(o/eventNumber));
    }
  }
  tree->Write();
  etree->Write();
  bcidH->Write();
  totchnH->Write();
  pdoH->Write();
  npacH->Write();
  loccH->Write();
  for (int layer=0; layer<8; layer++) {
    occ[layer]->Write();
    time2d[layer]->Write();
    amp2d[layer]->Write();
  }
  for (int layer=0; layer<8; layer++) {
    cdata.layer = layer;
    for (int i=0; i<8192; i++) {
      cdata.chn  = i;
      cdata.occ  = pdo[layer][i]->GetEntries()/eventNumber;
      cdata.mean = pdo[layer][i]->GetMean();
      cdata.rms  = pdo[layer][i]->GetRMS();
      cdata.skew = pdo[layer][i]->GetSkewness();
      ctree->Fill();
      pdo[layer][i]->GetXaxis()->SetRangeUser (0,200);
      pdo[layer][i]->Write();
    }
  }
  ctree->Write();
  f->Close();
  delete [] buffer;  
  printf("\nTotal number of events = %d with %f chn/evt\n", eventNumber, chnCount/(double)eventNumber);
  printf ("Encountered %i exceptions\n", errorCount);
}

