// reads two NSWEvent data and prints differences
// 
#include <stdio.h>
#include "NSWRead/NSWEvent.h"

#define maxSize 21000000/4  // 4000*32   // for event buffer



int main(int argc, char *argv[]){

  FILE* infile1, *infile2;
  NSWEvent event1, event2;
  int size1, size2 ; // event size
  unsigned int * buffer1, *buffer2;
  unsigned int eventCount = 0;
  unsigned int errorCount = 0;
  unsigned int packetCount = 0;


 if (argc==1) {
    printf ("usage: nswdiff infile1 infile2\n");
    printf ("reads two NSWEvent data and prints differences.\n");
    return 1;
  }

  buffer1 = new unsigned int [maxSize];
  if (buffer1 == NULL) {
    printf ("Cannot allocate event buffer1.\n");
    return 1;
  }

  buffer2 = new unsigned int [maxSize];
  if (buffer2 == NULL) {
    printf ("Cannot allocate event buffer2.\n");
    return 1;
  }

  infile1 = fopen (argv[1], "rb");
  if (!infile1) {
    printf ("Cannot open '%s'\n", argv[1]);
    return 1;
  }

  infile2 = fopen (argv[2], "rb");
  if (!infile2) {
    printf ("Cannot open '%s'\n", argv[2]);
    return 1;
  }

  int packetCount1 = 0;
  int packetCount2 = 0;
  printf ("\n");
  
  do{ //event loop
    try{
      size1 = event1.ReadATLAS(infile1, buffer1, maxSize);
    }
    catch (NSWReadException& ex){
       if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
      break;
    }

    try{
      size2 = event2.ReadATLAS(infile2, buffer2, maxSize);
    }
    catch (NSWReadException& ex){
       if (ex.GetSeverity() > SEV_INFO) printf ("NSWRead: %s\n", ex.what());
      break;
    }

    try {
      event1.ReadNSW(buffer1, size1);
      event2.ReadNSW(buffer2, size2);
      //if (size1 != size2) printf ("Size1 = %d differs from size2 = %d\n", size1, size2);

      bool match = false;
      if (event1.GetNumSector() == 0) continue; // should loop over sectors
      if (event2.GetNumSector() == 0) continue; // should loop over sectors 
      NSWSector* sec1 = event1.GetSector(0);
      NSWSector* sec2 = event2.GetSector(0);
      for (int j1=0; j1<sec1->GetNumPacket(); j1++){ // loop over packets
	NSWPacket *pac1 =sec1->GetPacket(j1);
	int linkID1 = pac1->GetLinkID();
	packetCount++;
	for (int j2=0; j2<sec2->GetNumPacket(); j2++){ // loop over packets
	  NSWPacket *pac2 =sec2->GetPacket(j2);
	  int linkID2 = pac2->GetLinkID();
	  if (linkID1 == linkID2) {// found it
	    if (pac1->GetLinkBytes() != pac2->GetLinkBytes()){
	      printf ("Packet %8X: Bytes1 = %d differs from bytes2 = %d\n", linkID1, pac1->GetLinkBytes(), pac2->GetLinkBytes());
	    } 
	    for (int i1=0; i1<pac1->GetWidth(); i1++){
	      NSWChannel* c1 = pac1->GetChannel(i1);
	      NSWChannel* c2 = pac2->GetChannel(i1);
	      if (c1->GetPDO() != c2->GetPDO()) printf ("Ch %3d: PDO1 = %4d, PDO2 = %4d\n",  i1, c1->GetPDO(), c2->GetPDO());
	      if (c1->GetTDO() != c2->GetTDO()) printf ("Ch %3d: TDO1 = %4d, TDO2 = %4d\n",  i1, c1->GetTDO(), c2->GetTDO());
	    }
	    if (pac1->Gethecksum() != pac2->Gethecksum()) printf ("Packet %8X: checksum1 = %d differs from checksum2 = %d\n", pac1->Gethecksum(), pac2->Gethecksum());
	    match = true;
	    break; 
	  }
	} // next j2
	if (!match) {
	  printf ("No matching packet for linkID %X found.\n", linkID1);
	}
      } // next j1
      eventCount++;
      if (eventCount%1000==0) printf("\u001b[32;1m\u001b[1AEvent = %d\u001b[0m\n", eventCount);
    }
    catch (NSWReadException& ex){
      if (ex.GetSeverity() > SEV_INFO) {
	printf ("ReadNSWException: ");
	printf("%s\n", ex.what());
	errorCount++;
      }
    }
    eventCount++;
  } while (size1>0); // next event

  fclose (infile1);
  delete [] buffer1;
  fclose (infile2);
  delete [] buffer2;
  printf ("Read %i events with %i packets, %10.6f packets/event\n", eventCount, packetCount, packetCount/(double)eventCount);
  printf ("Encountered %i exceptions.\n", errorCount);

}
