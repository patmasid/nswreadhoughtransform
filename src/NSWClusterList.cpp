//General reading class for atlas nsw data files 
#include <stdio.h>

#include "NSWRead/NSWClusterList.h"


NSWClusterList::NSWClusterList(){
} 


NSWClusterList::~NSWClusterList(){
  //std::cout << "~NSWClusterList: " << std::endl;
  for (int layer = 0; layer<8; layer++){
    //std::cout << "layer: " << layer << " cluster[layer].size(): " << cluster[layer].size() << std::endl;
    //for (unsigned int i=0; i<cluster[layer].size(); i++) delete cluster[layer][i];
    cluster[layer].clear();
  }
}

void NSWClusterList::SetClusterList(std::vector<std::vector<NSWCluster*>> list){
  
  for (int layer = 0; layer<8; layer++){
    cluster[layer].clear();
  }

  for( int layer=0; layer<list.size(); layer++){
    for( int icl=0; icl<list.at(layer).size(); icl++ ) {
      cluster[layer].push_back(list.at(layer).at(icl));
    }
  }

}

void NSWClusterList::Clear(){
  for (int layer = 0; layer<8; layer++){
    if(cluster[layer].size()!=0) {
      for (unsigned int i=0; i<cluster[layer].size(); i++) {
	cluster[layer].clear();
      }
    }
  }
} 

bool NSWClusterList::isChnBad(int layer, int chnl_num){
  if(v_bad_chnls[0][layer].size() != 0){
    if(std::find(v_bad_chnls[0][layer].begin(), v_bad_chnls[0][layer].end(), chnl_num) != v_bad_chnls[0][layer].end())
      return true;
    else
      return false;
  }
  else
    return false;
}

void NSWClusterList::Fill (NSWSector* sec, double minAmp, int minWid){
  double minPDO = minAmp+40;
  NSWChannel* det [8][8192]; // detector [layer][channel]
  if (sec == NULL) return;
  for (int layer=0; layer<8; layer++){
    for (int i=0; i<8192; i++) det[layer][i] = NULL;
  }

  //std::vector<bool> bad_layers;
  //bad_layers.resize(8);
  //for(int iL=0; iL<8; iL++){
  //  bad_layers[iL] = false;
  //}

  // store channels:
  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(j);
    if (pac->GetEndPoint() != 1) continue; // only strips
    for (int k=0; k<pac->GetWidth(); k++){
      NSWChannel* chn = pac->GetChannel(k);
      // if (chn->GetAmplitude_mV() < thr) continue;
      int layer = chn->GetLayer();
      int detchn = chn->GetDetectorStrip();
      if ((detchn>=0) && (detchn<8192)) {
	det[layer][detchn]=chn; // store it here
	//if(isChnBad(layer, detchn) && !(bad_layers[layer])){
	//bad_layers[layer] = true;
	//}
      } 
    } // next k
  } // next j

  //for(int iL=0; iL<8; iL++){
  //if(bad_layers[iL]){
  //  for(int chn = 0; chn<8192; chn++){
  //	det[iL][chn]=NULL;
  //  }
  //}   
  //}

  // Make clusters:
  NSWCluster* cl = NULL;

  for (int layer=0; layer<8; layer++){
    int i=0; // count channels 
    do{
      if (det[layer][i] != NULL) { // there is a channel
        if (cl == NULL) { // start of new cluster
          cl = new NSWCluster(det[layer][i]); // first channel 
        } else { // continuation 
          cl->AddChannel(det[layer][i]);
        }
        i++;
      } else {  // no channel
        if (cl != NULL) { // in a cluster
          if ((i+1<8192) && (det[layer][i+1] != NULL)){ // only one missing
            cl->AddChannel(NULL);
            cl->AddChannel(det[layer][i+1]);
            i ++;
          } else { // cluster ended
	    if ((cl->GetWidth()>=minWid)&&(cl->GetMaxPDO()>=minPDO)) {
	      cl->FitGaussian();
	      cl->FitCaruana();
	      cluster[layer].push_back(cl);
	    } else {
	      delete cl;
	    }
	    cl = NULL;
	    i += 2;
          } // 1 missing
          i++;
        } // in a cluster 
        i++;
      } // no channel
    } while (i<8192);
    if (cl != NULL) { // last cluster in this layer
      if ((cl->GetWidth()>=minWid)&&(cl->GetMaxPDO()>=minPDO)) {
        cl->FitGaussian();
	cl->FitCaruana();
        cluster[layer].push_back(cl);
      } else {
        delete cl;
      }
      cl = NULL;
    }
    // printf ("layer %d: %li clusters.\n", layer, cluster[layer].size());
  }
  

  /*for (int layer=0; layer<8; layer++){
    int i=0; // count channels
    do{
      if (det[layer][i] != NULL) { // there is a channel
	if (cl == NULL) { // start of new cluster
	  cl = new NSWCluster(det[layer][i]); // first channel
	} else { // continuation
	  cl->AddChannel(det[layer][i]);
	}
	i++;
      } else {  // no channel
	if (cl != NULL) { // in a cluster
	  if ((i+1<8192) && (det[layer][i+1] != NULL)){ // only one missing
	    cl->AddChannel(NULL);
	    cl->AddChannel(det[layer][i+1]);
	    i ++;
	  } else {
	    if ((i+2<8192) && (det[layer][i+2] != NULL)){ // two missing
	      cl->AddChannel(NULL);
	      cl->AddChannel(NULL);
	      cl->AddChannel(det[layer][i+2]);
	      i += 2;
	    } else { // cluster ended
	      if ((cl->GetWidth()>=minWid)&&(cl->GetMaxPDO()>=minPDO)) {
		cl->FitGaussian();
		cl->FitCaruana();
		cluster[layer].push_back(cl); 
	      } else {
		delete cl;
	      }
	      cl = NULL;
	      i += 2;
	    } // if 2 missing
	  } // if 1 missing
	  i++;
	} // in a cluster
	i++;
      } // no channel
    } while (i<8192);
    if (cl != NULL) { // last cluster in this layer
      if ((cl->GetWidth()>=minWid)&&(cl->GetMaxPDO()>=minPDO)) {
	cl->FitGaussian();
	cl->FitCaruana();
	cluster[layer].push_back(cl);
      } else {
	delete cl;
      }
      cl = NULL;
    }
    // printf ("layer %d: %li clusters.\n", layer, cluster[layer].size());
  } // next layer*/
}

void NSWClusterList::RemoveBadCluster(int layer, int i){

  //auto it = std::find(cluster[layer].begin(), cluster[layer].end(), cluster[layer][i]);
  cluster[layer].erase(cluster[layer].begin()+i);
}

void NSWClusterList::AddCluster(int layer, NSWCluster* cl){
  cluster[layer].push_back(cl);
  //std::cout << "pointer cl: " << &cl << std::endl;
}

void NSWClusterList::SplitCluster(int layer, int i, int chnl, bool isNULL){

  int num_chns = cluster[layer][i]->GetNumChannel();
  NSWCluster *cl1=NULL;
  NSWCluster *cl2=NULL;

  if( isNULL ){
    if( chnl == 0 ){
      cl1 = new NSWCluster(cluster[layer][i]->GetChannel(chnl+1));
      for(int i_chn=chnl+2; i_chn<num_chns; i_chn++){
        cl1->AddChannel(cluster[layer][i]->GetChannel(i_chn));
      }
      cl1->FitGaussian();
      cl1->FitCaruana();
      cluster[layer][i] = cl1;
    }
    else if( chnl == num_chns-1 ){
      cl1 = new NSWCluster(cluster[layer][i]->GetChannel(0));
      for(int i_chn=1; i_chn<num_chns-1; i_chn++){
        cl1->AddChannel(cluster[layer][i]->GetChannel(i_chn));
      }
      cl1->FitGaussian();
      cl1->FitCaruana();
      cluster[layer][i] = cl1;
    }
    else{
      cl1 = new NSWCluster(cluster[layer][i]->GetChannel(0));
      cl2 = new NSWCluster(cluster[layer][i]->GetChannel(chnl+1));
      for(int i_chn=1; i_chn<num_chns; i_chn++){
        if( i_chn< chnl )
          cl1->AddChannel(cluster[layer][i]->GetChannel(i_chn));
	else if( i_chn> chnl+1 )
          cl2->AddChannel(cluster[layer][i]->GetChannel(i_chn));
      }
      cl1->FitGaussian();
      cl1->FitCaruana();
      cl2->FitGaussian();
      cl2->FitCaruana();
      cluster[layer][i] = cl1;
      cluster[layer].insert(cluster[layer].begin()+i+1, cl2);

    }
  } // NULL     
  else{
    cl1 = new NSWCluster(cluster[layer][i]->GetChannel(0));
    cl2 = new NSWCluster(cluster[layer][i]->GetChannel(chnl));
    for(int i_chn=1; i_chn<num_chns; i_chn++){
      if( i_chn<= chnl )
	cl1->AddChannel(cluster[layer][i]->GetChannel(i_chn));
      if( i_chn > chnl )
	cl2->AddChannel(cluster[layer][i]->GetChannel(i_chn));
    }
    cl1->FitGaussian();
    cl1->FitCaruana();
    cl1->SetSplit(2);
    cl2->FitGaussian();
    cl2->FitCaruana();
    cl2->SetSplit(1);
    cluster[layer][i] = cl1;
    cluster[layer].insert(cluster[layer].begin()+i+1, cl2);

  } // not NULL 
}

void NSWClusterList::GetVectorClusters(std::vector< std::vector< NSWCluster* > > &multi_cluster_vec){
  
  multi_cluster_vec.resize(8);

  for(int layer=0; layer<8; layer++){
    //if(GetNumClusters(layer) == 0) multi_cluster_vec.at(layer).push_back(NULL);
    //else{
    for(int icl=0; icl<GetNumClusters(layer); icl++){
      
      NSWCluster* clui = GetCluster(layer, icl);
      multi_cluster_vec.at(layer).push_back(clui);
    }
    //}
  }

}

bool NSWClusterList::isCommonCluster(NSWClusterList &list){
  
  bool isCommon = false;
  
  //std::cout << "blahhhhh" << std::endl;

  for(int layer=0; layer<8; layer++){
    
    //std::cout << "Layer: " << layer << " Number of clusters: " << GetNumClusters(layer) << std::endl;

    for(int icl=0; icl<GetNumClusters(layer); icl++){
      NSWCluster* clui = GetCluster(layer, icl);
      for(int jcl=0; jcl<list.GetNumClusters(layer); jcl++){
	NSWCluster* cluj = list.GetCluster(layer, jcl);
	//std::cout << "blahhhhh" << std::endl;
	if(clui == NULL || cluj == NULL) continue;
	if(clui->isSameCluster(cluj)){
	  isCommon = true;
	  return isCommon;
	}
	else{
	  continue;
	}
	
      }
    }
    
  }
  
  return isCommon;
}

void NSWClusterList::CombineClustersLists(NSWClusterList &list, NSWClusterList &list_comb){
  
  /*std::vector< std::vector< NSWCluster* > > multi_cluster_vec;
  multi_cluster_vec.resize(8);

  for(int layer=0; layer<8; layer++){
    for(int icl=0; icl<GetNumClusters(layer); icl++){

      NSWCluster* clu = GetCluster(layer, icl);
      multi_cluster_vec.at(layer).push_back(clu);
      
    }
  }*/
  
  for(int layer=0; layer<8; layer++){
    
    for(int icl=0; icl<GetNumClusters(layer); icl++){
      
      NSWCluster* clui = new NSWCluster();
      *clui = *(GetCluster(layer, icl));
      if(clui == NULL) continue;
      if(list_comb.GetNumClusters(layer) == 0){
	list_comb.AddCluster(layer,clui);
      }
      else{
	bool found = false;
	for(int jcl=0; jcl<list_comb.GetNumClusters(layer); jcl++){
	  NSWCluster* cluj = new NSWCluster();
	  *cluj = *(list_comb.GetCluster(layer, jcl));
	  if(clui->isSameCluster(cluj)){
	    found = true;
	    break;
	  }
	}
	if(!found){
	  list_comb.AddCluster(layer,clui);
	}
      }
      
    }
    
    for(int icl=0; icl<list.GetNumClusters(layer); icl++){
      
      NSWCluster* clui = new NSWCluster();
      *clui = *(list.GetCluster(layer, icl));
      if(clui == NULL) continue;
      if(list_comb.GetNumClusters(layer) == 0){
        list_comb.AddCluster(layer,clui);
      }
      else{
        bool found = false;
        for(int jcl=0; jcl<list_comb.GetNumClusters(layer); jcl++){
          NSWCluster* cluj = new NSWCluster();
          *cluj= *(list_comb.GetCluster(layer, jcl));
          if(clui->isSameCluster(cluj)){
            found = true;
            break;
          }
        }
        if(!found){
          list_comb.AddCluster(layer,clui);
        }
      }
      
    }
    
  }
  
  /*list_comb.Clear();

  for(int layer=0; layer<8; layer++){
    for(int iicl=0; iicl<multi_cluster_vec.at(layer).size(); iicl++){
      NSWCluster* clu = new NSWCluster();
      *clu = *(multi_cluster_vec.at(layer).at(iicl));
      list_comb.AddCluster(layer,clu);
    }
  }*/
  
}

void NSWClusterList::CombineClustersLists(NSWClusterList &list){
  
  std::vector< std::vector< NSWCluster* > > multi_cluster_vec;
  multi_cluster_vec.resize(8);

  for(int layer=0; layer<8; layer++){
    for(int icl=0; icl<GetNumClusters(layer); icl++){

      NSWCluster* clu = new NSWCluster();
      *clu = *(GetCluster(layer, icl));
      multi_cluster_vec.at(layer).push_back(clu);
      
    }
  }

  for(int layer=0; layer<8; layer++){
    
    for(int icl=0; icl<GetNumClusters(layer); icl++){
      
      NSWCluster* clui = new NSWCluster();
      *clui = *(GetCluster(layer, icl));
      if(clui == NULL) continue;
      if(multi_cluster_vec.at(layer).size() == 0){
	multi_cluster_vec.at(layer).push_back(clui);
      }
      else{
	bool found = false;
	for(int jcl=0; jcl<multi_cluster_vec.at(layer).size(); jcl++){
	  NSWCluster* cluj = multi_cluster_vec.at(layer).at(jcl);
	  if(clui->isSameCluster(cluj)){
	    found = true;
	    break;
	  }
	}
	if(!found){
	  multi_cluster_vec.at(layer).push_back(clui);
	}
      }
      
    }
    
    for(int icl=0; icl<list.GetNumClusters(layer); icl++){
      
      NSWCluster* clui = new NSWCluster();
      *clui = *(list.GetCluster(layer, icl));
      if(clui == NULL) continue;
      if(multi_cluster_vec.at(layer).size() == 0){
	multi_cluster_vec.at(layer).push_back(clui);
      }
      else{
        bool found = false;
        for(int jcl=0; jcl<multi_cluster_vec.at(layer).size(); jcl++){
          NSWCluster* cluj = multi_cluster_vec.at(layer).at(jcl);
          if(clui->isSameCluster(cluj)){
            found = true;
            break;
          }
        }
	if(!found){
          multi_cluster_vec.at(layer).push_back(clui);
        }
      }
      
    }
    
  }
  
  list.Clear();

  for(int layer=0; layer<8; layer++){
    for(int iicl=0; iicl<multi_cluster_vec.at(layer).size(); iicl++){
      NSWCluster* clu = multi_cluster_vec.at(layer).at(iicl);
      list.AddCluster(layer,clu);
    }
  }
  
}


void NSWClusterList::Print () {
  // print some information
  printf ("ClusterList: ");
  for (int layer=0; layer<8; layer++){
    printf ("%3li, ", cluster[layer].size());
  }
  printf ("\n");
  /****************
  for (int layer=0; layer<8; layer++){
    printf ("layer %d:\n", layer);
    for (int i=0; i<cluster[layer].size(); i++) cluster[layer][i]->Print();
  }
  *****************/
}

