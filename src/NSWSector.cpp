//General reading class for atlas nsw data files 
#include<stdio.h>

#include "NSWRead/NSWSector.h"
#include "NSWRead/NSWReadException.h"


extern int DEBUG;

NSWSector::NSWSector(){
  sector = 0; // no sector number yet
  for (int i=0; i<4; i++) atlasStatus[i] = 0;
  robSize = 0;  
  rosStatus = 0;
  numChannels = 0;
  isDummy = false;
} 

NSWSector::~NSWSector(){
  for (unsigned int i=0; i<packet.size(); i++) delete  packet[i];
  packet.clear();
}


int NSWSector::Read(unsigned int *source){
  // returns number of words read if reading went ok, -1 otherwise
  unsigned int nData = 0; // count words in *source

  // read the atlasHeader(s)
  unsigned int *h;
  robSize = 0;  
  if (source[0] != 0xDD1234DD) { // ROBin header
    printf ("Error: Expected ROB header, got %08X.\n", source[nData]);
    THROW_EXCEPTION ("Cannot find ROB header.", SEV_WARN);
    return -1;
  }
  h = (unsigned int*) &robHeader;

  for (int i = 0; i<3; i++) *h++ = source[i];
  robSize = robHeader.fragmentSize;
  if (DEBUG) printf ("NSWSector::Read robFragmentSize = %i, headerSize = %i\n", robSize, robHeader.headerSize);

  for (unsigned int i = 3; i<robHeader.headerSize; i++) *h++ = source[i];
  //if (robHeader.status[0]) printf ("ROB status = %08X\n", robHeader.status[0]);
  nData += robHeader.headerSize; 

  if (source[nData] != 0xEE1234EE) { // ROD header
    printf ("Error: Expected ROD header, got %08X.\n", source[nData]);
    THROW_EXCEPTION ("Cannot find ROD header.", SEV_WARN);
    return -1;
  }
  h = (unsigned int*) &atlasHeader;
  for (int i = 0; i<9; i++) *h++ = source[nData+i];
  nData += 9;
  if ((atlasHeader.version & 0xffff) < 0x0100) {
    if (atlasHeader.eventType != 0) {
      printf ("Error: ROD minor version in header, got %08X, eventtype = %X.\n", atlasHeader.version & 0xffff, atlasHeader.eventType);
      THROW_EXCEPTION ("Old ROD data version.", SEV_WARN);
      return -1;
    } else {
      isDummy = true; // not written by swROD
    }
  }
  SetSector(atlasHeader.sourceID);
  
  // read status:
  int numStatus = 4;  // TODO: get this from ROD trailer?
  if ((atlasHeader.version & 0xffff) >= 0x0102) { // triggerCalibrationKey
    if ((atlasHeader.eventType & 1) == 1){
      numStatus = 5;
    }
  }
  if (isDummy) numStatus = 0;

  if (robSize<numStatus+nData){ //TODO: throw exception
    printf ("NSWSector::Read() Error: cannot read %d StatusElements\n", numStatus);
    THROW_EXCEPTION ("Cannot find ROD status.", SEV_WARN);
    return -1;
  }

  for (int i=0; i<numStatus; i++) atlasStatus[i] = source[i+nData];
  nData += numStatus; 
  //printf ("StatusElements are 0x%08X  0x%08X 0x%08X 0x%08X\n", atlasStatus[0], atlasStatus[1], atlasStatus[2], atlasStatus[3]);
  //printf ("Missing data = %04X, local status = %04X, presence = %04X\n", atlasStatus[1]&0xffff, atlasStatus[2]>>16, atlasStatus[2]&0xffff);
  
  // Now read packets:
  int pac = 0;
  while (nData<robSize-3){ // 3 words in ROD trailer
    // printf ("Reading packet %d: read %d words out of %d\n", pac, nData, robSize);
    NSWPacket* p = new NSWPacket(&(source[nData]));
    nData += p->GetWordLength();
    packet.push_back(p);
    pac++;
    numChannels += p->GetWidth();
  }
  //printf ("Read all packets: robSize = %d, nData = %d\n", robSize, nData);
  if (robSize<nData+3){
    printf ("Cannot read ATLAS trailer: robSize = %d, nData = %d\n", robSize, nData);
    THROW_EXCEPTION ("Cannot find ATLAS trailer.", SEV_WARN);
    return -1;
  }
  unsigned int *a = (unsigned int*) &atlasTrailer;
  for (int i=0; i<3; i++) *a++ = source[nData+i]; 
  nData +=3;

  //printf ("ATLAS trailer: numStatus = %08X, data elements = %08X, statusPos. = %08X ", atlasTrailer.numStatus, atlasTrailer.numData, atlasTrailer.StatusPosition);

  if (atlasTrailer.numData != nData-19-numStatus-robHeader.numStatus){ 
    printf ("ERROR: Bad Trailer: Read %d instead of %d data words, robSize = %d\n", nData, atlasTrailer.numData, robSize); 
    THROW_EXCEPTION ("Trailer size mismatch.", SEV_INFO);
  }
  return nData; // successful reading 
}//end read


int NSWSector::ReadGnam (const unsigned int *source, unsigned long int size){
  // returns number of words read if reading went ok, -1 otherwise
  int nData = 0; // count words in *source

  robHeader.status[0] = *(source-1); // HACK for ROB status in Gnam
  // read the atlasHeader of the ROD, there is no ROB header for Gnam
  unsigned int *h;
  robSize = size;
  if (DEBUG) printf ("NSWSector::Read robSize = %i\n", robSize);
 
  if (source[0] != 0xEE1234EE) { // ROD header
    printf ("Error: Expected ROD header, got %08X.\n", source[0]);
    THROW_EXCEPTION ("Cannot find ROD header.", SEV_WARN);
    return -1;
  }
  h = (unsigned int*) &atlasHeader;
  for (int i = 0; i<9; i++) *h++ = source[nData+i];
  nData += 9;
  if ((atlasHeader.version & 0xffff) < 0x0100) {
    printf ("Error: ROD minor version in header, got %08X.\n", atlasHeader.version & 0xffff);
    THROW_EXCEPTION ("Old ROD data version.", SEV_WARN);
    return -1;
  }
  SetSector(atlasHeader.sourceID);
  
  //printf ("L1ID in header, got %08X.\n",  atlasHeader.level1ID);
  // read status:
  int numStatus = 4;  // TODO: get this from ROB header?
  if (robSize<numStatus+nData){ //TODO: throw exception
    printf ("NSWSector::Read() Error: cannot read %d StatusElements\n", numStatus);
    THROW_EXCEPTION ("Cannot find ROD status.", SEV_WARN);
    return -1;
  }

  for (int i=0; i<numStatus; i++) atlasStatus[i] = source[i+nData];
  nData += numStatus; 
  //printf ("StatusElements are 0x%08X  0x%08X 0x%08X 0x%08X\n", atlasStatus[0], atlasStatus[1], atlasStatus[2], atlasStatus[3]);
  //printf ("Missing data = %04X, local status = %04X, presence = %04X\n", atlasStatus[1]&0xffff, atlasStatus[2]>>16, atlasStatus[2]&0xffff);
  
  // Now read packets:
  int pac = 0;
  while (nData<robSize-3){ // 3 words in ROD trailer
    // printf ("Reading packet %d: read %d words out of %d\n", pac, nData, robSize);
    NSWPacket* p = new NSWPacket((unsigned int*)&(source[nData]));
    nData += p->GetWordLength();
    packet.push_back(p);
    pac++;
  }
  //printf ("Read all packets: robSize = %d, nData = %d\n", robSize, nData);
  if (robSize<nData+3){
    printf ("Cannot read ATLAS trailer: robSize = %d, nData = %d\n", robSize, nData);
    THROW_EXCEPTION ("Cannot find ATLAS trailer.", SEV_WARN);
    return -1;
  }
  unsigned int *a = (unsigned int*) &atlasTrailer;
  for (int i=0; i<3; i++) *a++ = source[nData+i]; 
  nData +=3;

  //printf ("ATLAS trailer: numStatus = %08X, data elements = %08X, statusPos. = %08X \n", atlasTrailer.numStatus, atlasTrailer.numData, atlasTrailer.StatusPosition);
  if (atlasTrailer.numData != nData-16){
    printf ("ERROR: Bad Trailer: Read %d instead of %d data words, robSize = %d\n", nData, atlasTrailer.numData, robSize); 
    THROW_EXCEPTION ("Trailer size mismatch.", SEV_INFO);
  }
  return nData; // successful reading 
}//end ReadGnam


NSWPacket* NSWSector::GetPacket(int i){
  // return a pointer to a packet:
  return packet[i];
}



int NSWSector::GetSector(){
  return sector;
}

void NSWSector::SetSector (unsigned int sourceID){
  int det = sourceID>>16; // subdetector ID, 6B for MM A side, 6C for C side
  sector = (sourceID & 0xf) + 1;
  technology = (sourceID<0x6D); //  1 = MM, 0=sTGC

  if (det==0x6c) sector = -sector; // negative eta MM
  if (det==0x6e) sector = -sector; // negative eta sTGC
  //printf ("sourceID = %X, sector = %d\n", sourceID, sector);
}

int NSWSector::GetActiveStrips(int amp){
  int activeStrips = 0;

  for (int j=0; j<GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac = GetPacket(j);
    for (int k=0; k<pac->GetWidth(); k++){
      NSWChannel* chn = pac->GetChannel(k);
      if (chn->GetPDO() > amp) activeStrips++;
    }
  }
  return activeStrips;
}



int NSWSector::Store( unsigned int * buf){
  /************************************
   ************************************/
  return 0;
}

void NSWSector::Simulate(unsigned channelID,  int moduleID){
  /**********************************
  int layer;
  commonHeader = moduleID<<24; // type=0, size unknown

  // for Y layer clusters:
  numCluster[4] = 4; // one per layer in SPU 4
  cluster[4] = new NSWCluster* [numCluster[4]];

  // for X layer clusters:
  for (layer=0; layer<4; layer++){ 
    numCluster[layer] = 1; // one per layer in SPU [layer]
    // allocate X cluster array:
    cluster[layer] = new NSWCluster* [numCluster[layer]];
    cluster[layer][0] = new NSWCluster(channelID|(layer<<9), 192, numSample);
    cluster[4][layer] = new NSWCluster(channelID|(layer<<9)|0x100, 48, numSample);
  } 
  // sort the y data into layers:
  for (layer=0; layer<4; layer++) {
    numYCluster[layer] =0;
    clusterY[layer] = new NSWCluster* [numCluster[4]]; // could be smaller.
  }
  for (int i=0; i<numCluster[4]; i++){
    layer = cluster[4][i]->GetLayer();
    clusterY[layer][numYCluster[layer]] = cluster[4][i]; 
    numYCluster[layer]++;
  }
  ***************************************/
}

//return the total number of strips in this chamber:
int NSWSector::GetNumStrips(){
  return 0; // TODO FIX THIS
}  

void NSWSector::Print () {
  // print some information
  printf ("Sector %d:\n", GetSector());
  printf ("Header = 0x%08X, Run number = %i.\n", atlasHeader.marker, atlasHeader.runNumber);
  printf ("Source ID = 0x%08X, L1ID =  0x%08X, BCID = 0x%08X\n", atlasHeader.sourceID, atlasHeader.level1ID, atlasHeader.BCID);
  if (isDummy) {
    printf ("Empty dummy event without packets\n");
  } else {
    printf ("StatusElements are ");
    for (unsigned int i=0; i<atlasTrailer.numStatus; i++){
      printf ("0x%08X  ", atlasStatus[i]);
    }
    printf ("\n");
    //printf ("Missing data = %04X, local status = %04X, presence = %04X\n", atlasStatus[1]&0xffff, atlasStatus[2]>>16, atlasStatus[2]&0xffff);
    printf ("Number of packets: %li\n", packet.size());
    
    for (unsigned int i=0; i<packet.size(); i++) packet[i]->Print();
  } // isDummy
  printf ("ATLAS trailer: numStatus = %08X, data elements = %08X, statusPos. = %08X\n\n", atlasTrailer.numStatus, atlasTrailer.numData, atlasTrailer.StatusPosition);
  /*************
  if (atlasTrailer.StatusPosition ==0) {
    printf ("data follow status\n");
  } else {
    printf ("status follows data\n");
  }
  **************/
}
