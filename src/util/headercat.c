// ATLAS header data dump to screen. Lists L1ID, BCID, sourceID, etc.
// 
#include <stdio.h>


#define MAX_SIZE 25000
#define UINT     unsigned int

int Skip2Header (FILE* infile, unsigned int * h);


typedef struct AtlasHeader{
  UINT marker;
  UINT size;
  UINT version;
  UINT sourceID;
  UINT runNumber;
  UINT level1ID;
  UINT BCID;
  UINT triggerType;
  UINT eventType;
} TATLASHeader;


typedef struct AtlasTrailer {
  UINT numStatus;
  UINT numData;
  UINT StatusPosition;
}TAtlasTrailer;




int main(int argc, char *argv[]){

  FILE* infile;



  unsigned int buffer[MAX_SIZE];
  int eventCount;

 if (argc==1) {
    printf ("usage: headercat infile\n");
    printf ("dumps atlas headers to screen.\n");
    return (1);
  }

  infile = fopen (argv[1], "rb");
  if (!infile) {
    printf ("Cannot open 'argv[1]'\n");
    return 1;
  }


  eventCount = 0;
  printf ("Marker:       Fsize    Hsize   version  sourceID runNum  Level1ID    BCID  \n" );
  while (Skip2Header(infile, buffer) ==7) {
    if (buffer[0] == 0xee1234ee) {
      printf ("0x%08X: %8s %8X %8X %8X %8X %8X %8X\n", buffer[0], "---", buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6]);
    } else {
      printf ("0x%08X: %8X %8X %8X %8X %8X %8X %8X\n", buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7]);
    }
    eventCount++;
  }// next event
  //printf ("Read %i events\n", eventCount);
  fclose (infile);
}


int Skip2Header (FILE* infile, unsigned int * h){
  // skips forward until it reads the CSC ATLAS header
  int n, skipped = 0;

  do{ // find next ATLAS header
    n=fread(h, 4, 1, infile);
    skipped++;
    //printf ("Skipping %x\n", h[0]);
  } while ((n==1)&&(((h[0]& 0xFFFF00)!=0x123400)||((h[0]>>24)!=(h[0]&0xFF))||((h[0]&0xF)!=((h[0]>>4)&0xF))));
  n=fread(&(h[1]), 4, 7, infile); // read rest of header
  //printf ("h[3] = %x\n", h[3]);
  return n;
}
