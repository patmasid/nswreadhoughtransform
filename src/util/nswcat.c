/** @file nswcat.c
  NSW data dump to screen. Uses  atlas format

  This is not the real reading code: no C++, no classes, no storage of 
  the event for later analysis. Instead, this is old code for debugging.
  The same function is now availble in testNSWEvent, but this program
  sometimes shows problems better.
  */ 

#include <stdio.h>

int Skip2NSW (FILE* infile, unsigned int * h);


typedef struct AtlasHeader{
  unsigned int marker;
  unsigned int size;
  unsigned int version;
  unsigned int sourceID;
  unsigned int runNumber;
  unsigned int level1ID;
  unsigned int BCID;
  unsigned int triggerType;
  unsigned int eventType;
} TATLASHeader;


typedef struct AtlasTrailer {
  unsigned int numStatus;
  unsigned int numData;
  unsigned int StatusPosition;
}TAtlasTrailer;



int main(int argc, char *argv[]){

  FILE* infile;
  TATLASHeader atlasHeader;
  unsigned int statusElement[5];
  TAtlasTrailer atlasTrailer;
  
  int eventCount, n, nData;
  
  unsigned int format;
  unsigned int pacHead[2]; // packet header
  unsigned int data[555];  // room for longest packet
  int numStatus = 4;       // should not be hard coded.
  int robSize = 0;

  if (argc==1) {
    printf ("usage: nswcat infile\n");
    printf ("dumps nsw data to screen.\n");
    return (1);
  }
  
  infile = fopen (argv[1], "rb");
  if (!infile) {
    printf ("Cannot open '%s'\n", argv[1]);
    return 1;
  }
  

  eventCount = 0;
  while ((robSize=Skip2NSW(infile, (unsigned int*)&atlasHeader))>0) {
    fread (&(atlasHeader.runNumber), 4, 9-4, infile); // finish reading the atlasHeader
    printf ("\nROD fragment %i: Header = 0x%08X, Run number = %i.\n", eventCount, atlasHeader.marker, atlasHeader.runNumber);
    printf ("Source ID = 0x%08X, L1ID =  0x%08X, BCID = 0x%08X\n", atlasHeader.sourceID, atlasHeader.level1ID, atlasHeader.BCID);
    
    format = atlasHeader.version & 0xffff;
    printf ("Format in ROD header: 0x%08X, eventType = 0x%08X.\n", atlasHeader.version, atlasHeader.eventType);
    if (format < 0x0100) {
      printf ("Format too old to read.\n");
      fclose(infile);
      return 1;
    }
    if (format >= 0x0102) { // triggerCalibrationKey
      if ((atlasHeader.eventType & 1) == 1){
	numStatus = 5;
      }
    }

    nData = 0;
    n=fread(statusElement, 4, numStatus, infile); 
    if (n!=numStatus){
      printf ("Cannot read %d StatusElements\n", numStatus);
      fclose (infile);
      return 1;
    }
    nData += n; 
    printf ("StatusElements are ");
    for (int i=0; i<numStatus; i++){
      printf ("0x%08X  ", statusElement[i]);
    }
    printf ("\n");
    //printf ("Missing data = %04X, local status = %04X, presence = %04X\n", statusElement[1]&0xffff, statusElement[2]>>16, statusElement[2]&0xffff);


    int pac = 0;
    // Now read packets:
    while (nData<robSize-20){
      printf ("Packet %d: read %d words out of %d\n", pac, nData, robSize);
      n=fread(pacHead, 4, 2, infile); 
      if (n!=2){
	printf ("Cannot read packet header\n");
	fclose (infile);
	return 1;
      }
      nData += n;
      unsigned int linkSize = pacHead[0] & 0xffff;
      unsigned int linkID   = pacHead[1];
      unsigned int wordSize = (linkSize+3)/4;

      printf ("header[0]=%08X, header[1] = %08X\n", pacHead[0], pacHead[1]);
      printf ("Packet header of link %d with %d bytes: ", linkID, linkSize);
      if (wordSize>600) {
	fread(data, 4, 15, infile);
	printf ("\nREAD ERROR! header[0]=%08X, header[1] = %08X\n", pacHead[0], pacHead[1]);
	printf ("Next 15 words:\n");
	for (int i=0; i<15; i++) printf ("%2d: %08X\n", i, data[i]);
	fclose (infile);
	return 1;
      }
      if (linkID>0xffff){ // new channel ID
	int layer = (linkID >> 12) &3;
	int multi = (linkID >> 15) &1;
	int phi = (linkID >>16)&7;
	int eta = (linkID >>12)&1;
	int size = (linkID >> 16)&1;
	int sector = 2*phi + 2-size;
	if (eta == 0) sector = -sector;
	printf ("%08X: %c%02dM%dL%d\n", linkID, eta==0?'C':'A', sector<0?-sector:sector, multi, layer);
      } else {
	printf ("old link ID %d\n", linkID);
      }
      n=fread(data, 4, wordSize-2, infile); 
      if (n!=wordSize-2){
	printf ("Cannot read packet body, got only %d words.\n", n);
	fclose (infile);
	return 1;
      }
      nData += n;
      unsigned int headerType = data[0]>>30;
      unsigned int l1;
      //printf ("data[0] = %08X, headerType = %d\n", data[0], headerType);
      switch (headerType){
      case 0: // MM full header
      case 2: // sTGC full header
	l1 = data[0]&0xffff;
	printf ("Full hit header: BCID = %d, L1ID = %d = 0x%04X\n", (data[0]>>16)&0xfff, l1, l1);
	for (int i=1; i<n-1; i++) {
	  printf ("%3d: %08X chn %3d PDO %4d TDO %3d REL %d\n", i, data[i], (data[i]>>18)&0x1ff, (data[i]>>8)&0x3ff, data[i]&0xff, (data[i]>>27)&7);  
	}
	printf ("hit trailer: %08X checksum = %d, length = %d is ", data[n-1], data[n-1]&0xff, (data[n-1]>>8)&0x3ff);
	if (((data[n-1]>>8)&0x3ff) == n-2) {
	  printf ("correct.\n"); 
	} else {
	  printf ("WRONG.\n");
	}
	break;
      case 1: // null event header
	l1 = (data[0]>>16)&0xff;
	printf ("Null event header: L1ID = %d = 0x%2X\n", l1, l1);
	break;
      case 3: // illegal
	printf ("Error: Illegal header type %d\n", headerType);
      }
      pac++;
    } // next packet
    //printf ("%d packets, read %d words out of %d\n", pac, nData, robSize);

     
    n=fread(&atlasTrailer, 4, 3, infile);
    if (n!=3){
      printf ("Cannot read ATLAS trailer\n");
      fclose (infile);
      return 1;
    }
    printf ("ATLAS trailer: numStatus = %08X, data elements = %08X, statusPos. = %08X ", atlasTrailer.numStatus, atlasTrailer.numData, atlasTrailer.StatusPosition);
    if (atlasTrailer.StatusPosition ==0) {
      printf ("data follow status\n");
    } else {
      printf ("status follows data\n");
    }
    if (atlasTrailer.numData != nData-numStatus){
      printf ("ERROR: Bad Trailer: Read %d instead of %d data words", nData, atlasTrailer.numData);
    } 

    eventCount++;
    printf ("\n");
  }// next event
  printf ("Read %i events\n", eventCount);
  fclose (infile);
}


int Skip2NSW (FILE* infile, unsigned int * h){
  // skips forward until it reads the NSW ATLAS header
  // return the length of the ROD fragment
  int n, skipped = 0;
  int subdetID;
  int robLength = 0; // ROD length + 10
  do{
    do{ // find next ROD header
      n=fread(h, 4, 1, infile);
      skipped++;
      //printf ("Skipping %x\n", h[0]);
      if (h[0] == 0xdd1234dd) { // ROB header
	n=fread (&robLength, 4, 1, infile); // read next word
	skipped++;
      }
    } while ((n==1)&&(h[0] != 0xee1234ee));
    n=fread(&(h[1]), 4, 3, infile); // read detector type
    //printf ("h[3] = %x\n", h[3]);
    subdetID = (h[3]>>16)&0xff;
  } while ((n==3)&&(subdetID<0x6b)&&(subdetID>0x6f));
  printf ("Skipped %i words, subdetectorID = %X, ROB length = %d\n", skipped-1, subdetID, robLength);
  return robLength; // ROD fragment length
}
