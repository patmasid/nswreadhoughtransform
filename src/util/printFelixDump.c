/** @file printFelixDump.c
  NSW data dump to screen. Reads FELIX dump format

  This is not the real reading code: no C++, no classes, no storage of 
  the event for later analysis. Instead, this is code for debugging.
  */ 

#include <stdio.h>
//#include <stdlib.h>
#include <string.h>


#define MAX_SIZE 25000


int main (int argc, char *argv[]) {

  FILE* infile;
  int packetCount;  
  unsigned char data[2000]; // room for longest packet
  char line[4000];
  
  
  if (argc==1) {
    printf ("usage: printFelixDump infile\n");
    printf ("or:   |printFelixDump for stdin\n");
    printf ("reads FELIX dump and prints nsw data to screen.\n\n");
    infile = stdin;  
  } else {
    infile = fopen (argv[1], "r");
    if (!infile) {
      printf ("Cannot open 'argv[1]'\n");
      return 1;
    }
  }

  packetCount = 0;
  while (fgets (line, 4000, infile)) {
    if (line[0]=='>') continue; // skip >>> message from ...
    int len = strlen(line);
    if (len<12) continue;  // skip empty lines
    //printf ("len = %3d: %s", len, line);
    int j = 0;   // count bytes
    int i = 0;   // count char
    do {
      int k = j;
      if (j>=8) k = 3-(j%4)+4*(j/4); // byte-reverse
      sscanf (&line[i], "%hhx", &data[k]);
//printf ("%2d %2d %c%c_ %x\n", i, j, line[i], line[i+1], data[k]);
      i = i+3;
      j++;
    } while (i < len-1);
    packetCount++;
    unsigned int * p = (unsigned int*) data;
    if (p[0]%4 == 2) p[p[0]/4] &= 0xffff0000; // set rest of word to zero
    printf ("Link header: %d bytes from link %d\n", p[0], p[1]);
    if (p[0] == 10) { // null event
      printf ("NULL Event  0x%04X: rocid = %d, L1ID = %d\n", p[2]>>16, (p[2]>>24)&0x3f, (p[2]>>16)&0xff);
    } else {
      printf ("Hit header   0x%08X: Orbit = %d, BCID = %4d, L1ID = %d\n", p[2], (p[2]>>30)&3, (p[2]>>16)&0xfff, p[2]&0xffff);
      for (i=0; i<p[0]/4-4; i++){ // p[0]/4-4 is negative for null events !!!
	printf ("Hit %6d   0x%08X: chn %3d PDO %4d TDO %3d REL %d\n", i, p[3+i], (p[3+i]>>18)&0x1ff, (p[3+i]>>8)&0x3ff, p[3+i]&0xff, (p[3+i]>>27)&7);
      }
      printf ("Hit trailer: 0x%08X checksum = %d, length = %d", p[p[0]/4-1], p[p[0]/4-1]&0xff, (p[p[0]/4-1]>>8)&0x3ff);
      if (((p[p[0]/4-1]>>8)&0x3ff) == p[0]/4-4) {
	printf (" is correct.\n");
      } else {
	printf (" is WRONG!\n");
      }
    }
  } // next packet
  
  printf ("Read %i packets\n", packetCount);
  fclose (infile);
}

