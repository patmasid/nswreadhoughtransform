/** @file padcat.c
  NSW pad trigger data dump to screen. Uses  uncompressed atlas format

  This is not the real reading code: no C++, no classes, no storage of 
  the event for later analysis. Instead, this is code for debugging.
  */ 

#include <stdio.h>

int Skip2NSW (FILE* infile, unsigned int * h);


typedef struct AtlasHeader{
  unsigned int marker;
  unsigned int size;
  unsigned int version;
  unsigned int sourceID;
  unsigned int runNumber;
  unsigned int level1ID;
  unsigned int BCID;
  unsigned int triggerType;
  unsigned int eventType;
} TATLASHeader;


typedef struct AtlasTrailer {
  unsigned int numStatus;
  unsigned int numData;
  unsigned int StatusPosition;
}TAtlasTrailer;



int main(int argc, char *argv[]){

  FILE* infile;
  TATLASHeader atlasHeader;
  unsigned int statusElement[5];
  TAtlasTrailer atlasTrailer;
  
  int eventCount, n, nData;
  
  unsigned int format;
  unsigned int pacHead[2];  // packet header
  unsigned int data[1000];  // room for longest packet
  int numStatus = 4;        // should not be hard coded.
  int robSize = 0;

  if (argc==1) {
    printf ("usage: padcat infile\n");
    printf ("dumps nsw pad trigger data to screen.\n");
    return (1);
  }
  
  infile = fopen (argv[1], "rb");
  if (!infile) {
    printf ("Cannot open '%s'\n", argv[1]);
    return 1;
  }
  

  eventCount = 0;
  while ((robSize=Skip2NSW(infile, (unsigned int*)&atlasHeader))>0) {
    fread (&(atlasHeader.runNumber), 4, 9-4, infile); // finish reading the atlasHeader
    printf ("\nROD fragment %i: Header = 0x%08X, Run number = %i, ROBsize = %d.\n", eventCount, atlasHeader.marker, atlasHeader.runNumber, robSize);
    printf ("Source ID = 0x%08X, L1ID =  0x%08X, BCID = 0x%08X\n", atlasHeader.sourceID, atlasHeader.level1ID, atlasHeader.BCID);
    
    format = atlasHeader.version & 0xffff;
    printf ("Format in ROD header: 0x%08X, eventType = 0x%08X.\n", atlasHeader.version, atlasHeader.eventType);
    if (format < 0x0100) {
      printf ("Format too old to read.\n");
      fclose(infile);
      return 1;
    }
    /*************
    if (format >= 0x0102) { // triggerCalibrationKey
      if ((atlasHeader.eventType & 1) == 1){
	numStatus = 5;
      }
    }***************/

    nData = 0;
    n=fread(statusElement, 4, numStatus, infile); 
    if (n!=numStatus){
      printf ("Cannot read %d StatusElements\n", numStatus);
      fclose (infile);
      return 1;
    }
    nData += n; 
    printf ("StatusElements are ");
    for (int i=0; i<numStatus; i++){
      printf ("0x%08X  ", statusElement[i]);
    }
    printf ("\n");
    //printf ("Missing data = %04X, local status = %04X, presence = %04X\n", statusElement[1]&0xffff, statusElement[2]>>16, statusElement[2]&0xffff);


    int pac = 0;
    // Now read packets:
    while (nData<robSize-20){
      printf ("Packet %d: read %d words out of %d\n", pac, nData, robSize);
      n=fread(pacHead, 4, 2, infile); 
      if (n!=2){
	printf ("Cannot read packet header\n");
	fclose (infile);
	return 1;
      }
      nData += n;
      unsigned int linkSize = pacHead[0] & 0xffff;
      unsigned int linkID   = pacHead[1];
      unsigned int wordSize = (linkSize+3)/4;

      printf ("header[0]=%08X, header[1] = %08X\n", pacHead[0], pacHead[1]);
      printf ("Packet header of link %d with %d bytes:\n", linkID, linkSize);
      if (wordSize>600) {
	fread(data, 4, 15, infile);
	printf ("\nREAD ERROR! header[0]=%08X, header[1] = %08X\n", pacHead[0], pacHead[1]);
	printf ("Next 15 words:\n");
	for (int i=0; i<15; i++) printf ("%2d: %08X\n", i, data[i]);
	fclose (infile);
	return 1;
      }
      n=fread(data, 4, wordSize-2, infile); 
      if (n!=wordSize-2){
	printf ("Cannot read packet body, got only %d words.\n", n);
	fclose (infile);
	return 1;
      }
      nData += n;
      unsigned short * d = (unsigned short*) data;
      for (int i=0; i<9; i++) printf ("%2d: %04X\n", i, d[i]);
      printf ("fragID=%d, secID=%d, EC=%d, flags=%d\n", d[0]>>12, (d[0]>>8)&0xf, (d[0]>>7)&1, d[0]&0x7f);
      printf ("BCID=%d, orbit=%d, spare=%d\n", d[1]>>4, (d[1]>>2)&3, d[1]&3);
      printf ("L1ID = %d = 0x%X\n", d[2]<<16|d[3], d[2]<<16|d[3]);
      int multiplicity = d[4] & 0xfff;
      printf ("BC_flags=%d, multiplicity=%d\n", d[4]>>12, multiplicity);
      int mapsize = 224; // in bytes, for large sector
      if ((atlasHeader.sourceID&0xf)%2==1) mapsize = 144; // small sector
      printf ("payload %d words, %d bytes of bitmaps, expect %d bytes\n", n, n*4-10-2, multiplicity*mapsize);/*
      for (int i=0; i<multiplicity; i++){
	for (int j=0; j<mapsize/2; j++){
	  printf ("%04x%04x ", d[5+i*mapsize+j*2], d[5+i*mapsize+j*2+1]);
	}
	printf ("\n");
	}*/
      pac++;
    } // next packet
    //printf ("%d packets, read %d words out of %d\n", pac, nData, robSize);

     
    n=fread(&atlasTrailer, 4, 3, infile);
    if (n!=3){
      printf ("Cannot read ATLAS trailer\n");
      fclose (infile);
      return 1;
    }
    printf ("ATLAS trailer: numStatus = %08X, data elements = %08X, statusPos. = %08X ", atlasTrailer.numStatus, atlasTrailer.numData, atlasTrailer.StatusPosition);
    if (atlasTrailer.StatusPosition ==0) {
      printf ("data follow status\n");
    } else {
      printf ("status follows data\n");
    }
    if (atlasTrailer.numData != nData-numStatus){
      printf ("ERROR: Bad Trailer: Read %d instead of %d data words", nData, atlasTrailer.numData);
    } 

    eventCount++;
    printf ("\n");
  }// next event
  printf ("Read %i events\n", eventCount);
  fclose (infile);
}


int Skip2NSW (FILE* infile, unsigned int * h){
  // skips forward until it reads the NSW ATLAS header
  // return the length of the ROD fragment
  int n, skipped = 0;
  int subdetID;
  int robLength = 0; // ROD length + 10
  do{
    do{ // find next ROD header
      n=fread(h, 4, 1, infile);
      skipped++;
      //printf ("Skipping %x\n", h[0]);
      if (h[0] == 0xdd1234dd) { // ROB header
	n=fread (&robLength, 4, 1, infile); // read next word
	skipped++;
      }
    } while ((n==1)&&(h[0] != 0xee1234ee));
    n=fread(&(h[1]), 4, 3, infile); // read detector type
    //printf ("h[3] = %x\n", h[3]);
    subdetID = (h[3]>>16)&0xff;
  } while ((n==3)&&(subdetID<0x6b)&&(subdetID>0x6f));
  printf ("Skipped %i words, subdetectorID = %X, ROB length = %d\n", skipped-1, subdetID, robLength);
  return robLength; // ROD fragment length
}
