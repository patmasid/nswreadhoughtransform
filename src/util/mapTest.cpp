// test program to read the sTGC mapping and to check each entry
// read the sTGC mapping file https://gitlab.cern.ch/McGill-sTGC/NSW_sTGC_Channel_Mapping/blob/master/mapping.txt?expanded=true&viewer=simple

#include <iostream>
#include "NSWRead/NSWChannelID.h"
// ROOT include(s): #include "TTree.h"

int main( int argc, char* argv[] ) {
  NSWChannelID id;

  std::ifstream infile;   
  std::string mapFile = "mapping.txt";
    
  // Open Mapping file
  infile.open (mapFile);
  if( infile.fail() ) {   // Checks to see if file opened
    std::cout << "Error loading map file 'mapping.txt'" << std::endl;
    exit(-1);
  }
 
 
  // Chamber strings options
  std::string code, chamber, chamberLong, channelType;

  // Chamber integer options
  int radius, layer, vmmid, vmmchn, chna, chnc;

  // Char
  char Large_or_Small, Pivot_or_Confirm, endcap_A_or_C;

  // Variables to compare channels
  int start;
  int chn;
  int chn_c;
  int n = 0;      // count lines
  int errors = 0; // count errors
  std::string line;
  while (std::getline(infile, line)) {
    std::istringstream iss(line);
    int it0, it1, it2, it3, it4, it5, it6, it7, it8, it9;
    std::string st0, st1, st2, st3, st4, st5, rad;
    if (!(iss >> st0 >> it0 >> it1 >> st1 >> it2 >> it3 >> it4 >> it5 >> it6 >> st2 >> st3 >> it7 >> st4 >> st5 >> it8 >> it9)) { break; }

      //std::cout <<  " -------------------------------------  "<<std::endl;
    //std::cout  <<" READ "<<  st0 <<" "<< it0 <<" "<<it1 <<" "<< st1 <<" "<< it2  <<" "<< it3 << " "<<it4 <<" "<< it5 <<" "<< it6 << " "<<st2 << " "<<st3 <<" "<< it7 <<" "<< st4 <<" "<< st5 << " "<<it8 <<" "<< it9<< std::endl;

    code = st0;
    chamberLong = code[1];
    rad = code[2];
    chamber = code[3];
    radius = std::stoi( rad ) - 1;
    layer = it1 - 1;
    channelType = st1; // Appears as S, W and P
    vmmid = it2 - 1;
    vmmchn = it3;
    chna = it8;
    chnc = it9;
    unsigned endpointType; //Endpoint type 0: pad, 1: strip,

    if (channelType[0] == 's') {
      endpointType = 1;
    } else { // pads and wires are on the same fiber
      endpointType = 0;
    }
    unsigned eta = 0;     // 0: endcap A, 1: endcap C

  // change from layer 0-3 to layer like data 0-7
  //
    unsigned sector0 = 0;
    unsigned layer0 = 0;
    if (chamberLong[0] == 'S') { // small chamber
      sector0 = 5; // 0-based sector A06 small
      if (chamber[0]=='C') { // confirm
	layer0 = layer;
      } else { // pivot
	layer0 = layer+4;
      }
    } else { // large chamber
      sector0 = 4; // 0-based sector A05 large
      if (chamber[0]=='C') { // confirm
	layer0 = layer+4;
      } else { // pivot
	layer0 = layer;
      }
    } 


//       if( chamberLong == "L" ) Large_or_Small = 'L';
 //        if( chamberLong == "S" ) Large_or_Small = 'S';
  //         if( chamber == "P" ) Pivot_or_Confirm = 'P';
   //          if( chamber == "C" ) Pivot_or_Confirm = 'C';


/*
 * does the same as above keep it for reference
      int layer_0to7_data ;
      layer_0to7_data = layer; // 0-3

      if( chamberLong == "S" ) {
        if ( chamber == "P" ) layer_0to7_data= layer+4 ;
      } else if ( chamberLong == "L" ) {
        if ( chamber == "C" ) layer_0to7_data= layer+4 ;
      }

      layer0=layer_0to7_data;  // check
      layer=layer_0to7_data;  // check
*/

  
 
    unsigned group = 0;   // board output
    id.SetChannelID (0, 0, endpointType, eta, sector0, layer0, radius, group);
    unsigned upperID = id.GetChannelID();

    id.SetChannelID(upperID, vmmid, vmmchn); // add the channel to elink
    
		
    // Test of the channelType
    if ((id.GetChannelType() == NSWChannelID::WIRE) && (channelType[0] != 'w')) {
      printf ("WIRE: %c ", channelType[0]);
      id.Print();
    }
    if ((id.GetChannelType() == NSWChannelID::STRIP) && (channelType[0] != 's')) {
      printf ("STRIP: %c ", channelType[0]);
      id.Print();
    }
    if ((id.GetChannelType() == NSWChannelID::PAD) && (channelType[0] != 'p')) {
      printf ("PAD: %c ", channelType[0]);
      id.Print();
    }
      
 
    int channelfound=id.GetDetectorStrip();


	// ---------------- print ---------------------
		//print all mapped channels
  	// std::cout << " channelType[0]= "<< channelType[0] << " channelfound = "<< channelfound << " chna = "<< chna <<" diff= "<<(channelfound-chna)<<std::endl;

		// print more if there is an unmapped channel
    //  if (chna != channelfound && (channelType[0] == 's')  ) {
    //  if (chna != channelfound && (channelType[0] == 'p')  ) {
    //  if (chna != channelfound && (channelType[0] == 'w')  ) {
    if (chna != channelfound  ) {
    std::cout << "--- if DIFFERENT After d.GetDetectorStrip() if chn is not mapped " << std::endl;
    std::cout << " channelType[0] = "<< channelType[0] <<std::endl;
    std::cout << " channelfound = "<< channelfound <<std::endl;
    std::cout << " chna = "<< chna <<std::endl;
      std::cout <<" "<<  st0 <<" "<< it0 <<" "<<it1 <<" "<< st1 <<" "<< it2  <<" "<< it3 << " "<<it4 <<" "<< it5 <<" "<< it6 << " "<<st2 << " "<<st3 <<" "<< it7 <<" "<< st4 <<" "<< st5 << " "<<it8 <<" "<< it9<< std::endl;
    std::cout << "--- end if DIFFERENT After d.GetDetectorStrip() if chn is not mapped " << std::endl;
      id.Print();
      errors++;
    }

      //std::cout <<  " end of the event -------------------------------------  "<<std::endl;

    n++;
  } // while
  
  // Close Mapping file 
  infile.close();
  infile.clear();

  std::cout << "--- End of event loop " <<std::endl;
  printf ("Read %d lines, found %d errors.\n", n, errors);
}
