// read elinkID or MMFE8ID and convert

#include <stdio.h>
#include "NSWRead/NSWChannelID.h"

int main(int argc, char *argv[]){
  NSWChannelID id;
  int n, ret = 0;
  unsigned int number;

  if (argc == 1) {
    printf ("read elinkID or boardName sector and convert.\n");
    printf ("usage: nswelink elinkID\n");
    printf ("or   : nswelink BoardID\n");
    printf ("or   : nswelink 0xHEXNUMBER\n");
    printf ("or   : nswelink decimalNumber\n");
    return (1);
  }

  if ((argv[1][1]=='x')||(argv[1][1]=='X')){ // hex number
    n = sscanf(&(argv[1][2]), "%x", &number);
  } else { // dec number
    n = sscanf(argv[1], "%u", &number);
  }
  if (n == 1){ // got a number
    //printf ("n = %d, number = %d = %08X\n", n, number, number);
    id.SetChannelID (number);
    ret = 0; // id is ok
  } else { // got a string
    if ((argv[1][0] == 'N') || (argv[1][0] == 'n')) {
      ret = id.ParseString (argv[1]);
    } else { // parse boardID
      ret = id.ParseBoardID (argv[1]);
    }
  }

  if (ret == 0) { // success
    char string[40];
    char boardID[20];
    id.GetString  (string); // official string representation
    id.GetBoardID (boardID);
    printf ("%d: %s %s\n", id.GetChannelID(), boardID, string);
  } else {
    printf ("Parse error\n");
  }
  return 0;
}
