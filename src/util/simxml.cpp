// writes an xml file of elinks for the MM data generator
// links are ordered by elink index 0..159

#include <stdio.h>
#include "NSWRead/NSWChannelID.h"

int main(int argc, char *argv[]){

  FILE* outfile;
  if (argc==1) {
    printf ("usage: simxml outfile\n");
    printf ("writes an xml file of elinks for the MM data generator.\n");
    return (1);
  }
  
  outfile = fopen (argv[1], "w");
  if (!outfile) {
    printf ("Cannot open 'argv[1]'\n");
    return 1;
  }
  NSWChannelID id;
  char label[80];
  char name[120];
  
  id.SetTechnology(1); // MM = 1, sTGC = 0
  id.SetRawSector(13); // 0-based, is A14, small sector for now
  id.SetEta(0); // 0: eta=+1: endcap A, 1: eta=-1, endcap C
  id.SetDataType(0); // 0: L1A, 1: config, 2: monitor
  id.SetEndPoint(1); // 0: pad, 1: strip,  2: Trig. Proc, 3: Pad trig

  for (int layer=0; layer<8; layer++){
    for (unsigned int i=0; i<20; i++){
      int index = i + 20*layer;         // link index
      int r = (i<12)? i : 12+ (i-12)/2; // radius
      int g = (i<12)? 0 : i%2;          // group
      id.SetRadius(r);
      id.SetChannelGroup(g);
      id.SetLayer(layer);
      id.GetBoardID(label);
      id.GetString(name);
      //printf ("i = %2d, r = %2d, g = %d, layer = %d, label = '%s'\n", i, r, g, layer, label);
      fprintf (outfile, "<obj class=\"SwRodInputLink\" id=\"%s\">\n", label);
      fprintf (outfile, " <attr name=\"FelixId\" type=\"u64\" val=\"%d\"/>\n", index);
      fprintf (outfile, " <attr name=\"DetectorId\" type=\"u64\" val=\"%d\"/>\n", id.GetChannelID());
      fprintf (outfile, " <attr name=\"DetectorName\" type=\"string\" val=\"%s\"/>\n", name);

      fprintf (outfile,  "</obj>\n\n");
    }
  }
  fclose (outfile);
}
