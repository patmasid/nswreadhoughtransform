//General reading class for atlas csc data files 
#include<stdio.h>
//#include<math.h>

#include "NSWRead/NSWReadException.h"
#include "NSWRead/NSWPacket.h"


// Read from a buffer
NSWPacket::NSWPacket(unsigned int * source){
  linkBytes = source[0] & 0xffff;
  linkID = source[1];
  //printf ("setting packet ID %08X\n", linkID);
  SetChannelID (linkID); // either old or new link ID
  //channel.clear();
  ROCID = 0;
  L1ID = 0;
  BCID = 0;  // BCID in lower 12 bit, orbit in bit 12 and 13
  hasTDO = true; // assume MM
  extended = false; 
  missing = 0; // missing VMM in trailer
  L0ID = 0;
  length = 0; // number of channels in trailer
  checksum = 0;
  timeout = 0;  
  runningSum = 0; // checksum
  width = 0; // for null packets

  // read header:
  headerType = source[2]>>30;
  //printf ("headerType = %d\n",  headerType);
  unsigned char* p = (unsigned char*) source;

  switch (headerType){
  case 0: // MM full header
  case 2: // sTGC full header
    L1ID = source[2] & 0xffff;
    BCID = (source[2]>>16) & 0xfff;
    hasTDO = (source[2]>>31) > 0;
    //printf ("Full hit header: BCID = %d, L1ID = %d = 0x%04X\n", BCID, L1ID, L1ID);
    width = (linkBytes+3)/4 - 4; // round up to next word, only for MM
    //printf ("NSWPacket: %d channels wide\n", width);
    //channel = new NSWChannel* [width]; // allocate pointer array
 
    for(int i=0; i<width; i++){        // allocate NSWChannels
      NSWChannel* c = new NSWChannel(linkID, source[3+i]); 
      c->SetPacket(this);
      channel.push_back(c);
    }

    checksum = source[3+width]      & 0xff;
    length =  (source[3+width]>>8)  & 0x3ff;
    L0ID =    (source[3+width]>>18) & 0xf;
    missing = (source[3+width]>>22) & 0xff;
    timeout = (source[3+width]>>30) & 1;
    extended = (source[3+width]>>31) > 0;
    /***************************************/
    //    if (missing>0) printf ("hit trailer: %08X missing = %02X, checksum = %d, length = %d is ", source[3+width], missing, checksum, length);
    /***********************
    if (length == width) { // trailer vs link header
      printf ("correct.\n"); 
    } else {
      printf ("WRONG.\n");
    }****************************************/
    for (int i=8; i<linkBytes; i++) runningSum += p[i]; // checksum
    runningSum = (runningSum & 0xFF)+(runningSum>>8); // add carry bits
    runningSum = (runningSum & 0xFF)+(runningSum>>8); // do it twice
    /********************************
    for (int i=8; i<linkBytes; i++){
      runningSum += p[i]; // checksum
      if (runningSum>0xFF) {
	runningSum = (runningSum+1) & 0xFF;
      }
    }
    ****************************/
    runningSum = (~runningSum) & 0xFF;
    //if (runningSum != 0) printf ("checksum = %X\n", runningSum);
    break;
  case 1: // null event header
    L1ID =  (source[2]>>16) & 0xff;
    ROCID = (source[2]>>24) & 0x3f;
    //printf ("Null event header: L1ID = %d = 0x%2X\n", L1ID, L1ID);
    break;
  case 3: // illegal
    printf ("Error: Illegal header type %d\n", headerType);
    THROW_EXCEPTION ("Illegal hit header type = 3.", SEV_WARN);
  } // switch
  //Test(1);
  //if (GetParityErrors()>0) Print();
} 

NSWPacket::~NSWPacket(){
  if (headerType == 1) return; // NULL packet
  if (headerType == 3) return; // ILLEGAL packet

  for (unsigned int i=0; i<channel.size(); i++) delete  channel[i];
  channel.clear();

}

void NSWPacket::DeleteChannels(){
  if (headerType == 1) return; // NULL packet
  if (headerType == 3) return; // ILLEGAL packet

  for (unsigned int i=0; i<channel.size(); i++) delete  channel[i];
  channel.clear();
}


NSWChannel* NSWPacket::GetChannel (int i){
  // returns channel i, i>=0 and i<width:
  if ((i>=0) && (i<width)){ 
    return channel[i];
  } else {
    printf ("NSWPacket::GetChannel(): Invalid channel number %i.\n", i);
    Print();
    return NULL; //invalid channel number
  }
}

void NSWPacket::SetChannel (int i, NSWChannel* ch){
  if ((i>=0) && (i<width)){ 
    channel[i] = ch;
  } else { //invalid channel number
    printf ("NSWPacket::SetChannel(): Invalid channel number %i.\n", i);
    Print();
  }
}

int NSWPacket::GetNumBytes() {
  if (width==0) {
    return 2; // Null event
  } else {
    if (hasTDO) {
      return 8+4*width;
    } else {
      return 8+3*width;
    }
  }
}


int NSWPacket::Test(int verbose){
  int ret = 0;
  // felix error?
  // checksum
  // length in trailer
  if (length != width){
    ret |= (1<<3);
    if (verbose) printf ("NSWPacket::Test: L1ID=%4X, trailer length = %d, but width = %d.\n", L1ID, length, width);
  }
  // parity is 1: parity error
  for (int i=0; i<width; i++){
    if (channel[i]->CalculateParity(BCID)!=0) {
      ret |= (1<<4);
      if (verbose) {
	printf ("NSWPacket::Test: L1ID=%4X, Parity error, ", L1ID);
	NSWChannelID id(channel[i]->GetChannelID());
	id.Print();
      }
    }
  }
  // FELIX timeout
  // BCID illegal
  if ((BCID & 0xfff) > 3564) { // 3564 bunches max.
    ret |= (1<<5);
    if (verbose) printf ("NSWPacket::Test: L1ID=%4X, Illegal BCID: %d\n", L1ID, BCID & 0xfff);
  }
  // different orbit/bcid L1ID
  /*********************************************
  unsigned short ob0 = channel[0]->GetBCID();
  unsigned short l10 = channel[0]->GetL1ID();
  for (int i=1; i<width; i++){
    if (channel[i]->GetBCID()!=ob0) {
      ret |= (1<<6);
      if (verbose) printf ("NSWPacket::Test: channel %d BCID mismatch.\n", i);
    }
    if (channel[i]->GetL1ID()!=l10) {
      ret |= (1<<6);
      if (verbose) printf ("NSWPacket::Test: channel %d L1ID mismatch.\n", i);
    }
  }
  **********************************************/
  //TO=1, i.e. a one or more VMMs are being ignored due to time-out
  if (missing > 0) {
    ret |= (1<<7);
    if (verbose) printf ("NSWPacket::Test: L1ID=%4X, Time-out occurred, missing = 0x%02X.\n", L1ID, missing);
  }
  // vmm channel number out of order
  return ret;
}

int NSWPacket::GetParityErrors(){
  int count =0;
  for (int i=0; i<width; i++){
    if (channel[i]->CalculateParity(BCID)!=0) count++;
  }
  return count;
}


int NSWPacket::GetMissing(bool mask){
  if (mask & (GetTechnology() == 0) & (GetRadius() > 0) & (GetEndPoint() == 1)){
    return missing & (~12); // removes the two bits with value 12 for sFEB6
  } else {
    return missing;
  }
}


  
void NSWPacket::Print () {
  // print some information
  if (headerType == 1) {
    printf ("Null packet: ROCID = %i, L1ID = 0x%04X on link %d\n", ROCID, L1ID, linkID);
  } else {
    printf ("Packet with %d bytes on linkID %d, timeout = %d\n", linkBytes, linkID, timeout);
    printf ("orbit: %i, BCID = %4d, L1ID = 0x%08X, %s TDO on link %d\n",
	    GetOrbit(), BCID, L1ID, hasTDO?"Has":"NO", linkID);
    for (int i=0; i<width; i++) {
      printf ("%4i: ", i);
      channel[i]->Print();
    }
    printf ("missing %d, L0ID %x,  width: %i, Checksum = %02X\n",
	    missing, L0ID, width, TestChecksum());
  }
}
