/** file NSWReadException.cpp
    defines exceptions for the NSW data reading code
*/

#include "NSWRead/NSWReadException.h"
#include <stdio.h>

NSWReadException::NSWReadException(const std::string &file, unsigned int line, const std::string &message, int severity){
  this->file = file;
  this->line = line;
  this->message = message;
  this->severity = severity;
  sprintf (txt, "%s\nin line %d of file '%s'", message.c_str(), line, file.c_str());
}

NSWReadException::NSWReadException(const std::string &message){
  this->file = "";
  this->line = 0;
  this->severity = 0;
  this->message = message;
  sprintf (txt, "%s", message.c_str());
}

NSWReadException::~NSWReadException() throw() {
}

const char * NSWReadException::what() const throw() {
  return txt;
}
