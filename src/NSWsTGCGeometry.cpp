#include <stdio.h>

#include "NSWRead/NSWPadWires.h"



NSWPadWires::NSWPadWires(){
}


NSWPadWires::~NSWPadWires(){
}

// pads L1: 96 56 52, 204 in total
// pads L2: 96 56 52, 204 in total
// pads L3: 96 56 56, 208 in total
// pads L4: 96 56 56, 208 in total



void NSWsTGCGeometry::ReadData(NSWSector* sec){

  //----------------------------------//
  //  Clear all existing data first 
  //----------------------------------//

  Clear();

  if (sec == NULL) return;

  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(j);

    //--------------------------------------//
    //                  pFEBs
    //--------------------------------------//

    if (pac->GetEndPoint() == TypePFEB) { // keep only pFEB type==0

      //----------------------------------------------//
      //                Store Pads
      //----------------------------------------------//
      
      for (int k=0; k<pac->GetWidth(); k++){

	NSWChannel* chn = pac->GetChannel(k);
	
	int layer = chn->GetLayer();
	int detchn = chn->GetDetectorStrip();

	//------------------------------//
	//            Pads
	//------------------------------//

	if (chn->GetChannelType() == TypePad ) { // pad type == 0
	
	  pad_hits.at(layer).push_back(chn); // store it here
	  Fill_PadTower_SingleHit( chn );  // fill associated pad tower
	  Fill_WireTower_SingleHit( chn ); // fill associated wire tower
	} // if pads
	//-------------------------------//
	//            Wires
	//-------------------------------//
	else if ( chn->GetChannelType() == TypeWire ) { // wire type == 2
	  
	  wire_hits.at(layer).push_back(chn); // store it here
          Fill_PadTower_SingleHit( chn );  // fill associated pad tower
	  Fill_WireTower_SingleHit( chn ); // fill associated wire tower
	}
	else {
	  std::cout << "WTF: Non Pad/Wire Channel detected for pFEB" << std::endl;
	}

      } // loop over packet width
    } // if pFEB
    //-----------------------------------------//
    //                 sFEB
    //-----------------------------------------//
    else if (pac->GetEndPoint() == TypeSFEB ) { // only sFEB
      for (int k=0; k<pac->GetWidth(); k++){

	NSWChannel* chn = pac->GetChannel(k);

	int layer = chn->GetLayer();
	int detchn = chn->GetDetectorStrip();

	if ( chn->GetChannelType() == TypeStrip ) {
	  if ((detchn>=0) && (detchn<maxStripNum)) {
	    strip_channels[layer][detchn]=chn; // make a map of all strip hits
	  }
	}
	else {
	  std::cout << "WTF: Non Strip Channel detected for sFEB" << std::endl;
        }

      } // loop over packet width
    } // if type == sFEB
    else {
      std::cout << "WTF: Non pFEB/sFEB board type detected for sTGC" << std::endl;
    }

  } // next j packet

  FindStripClusters();

}

//----------------------------------------//
//   Add single hits to pads towers
//---------------------------------------//
void NSWsTGCGeometry::Fill_PadTower_SingleHit( NSWChannel* chn ) {
  
  std::vector<int> AssociatedPadTowerIndex = GetAssociatedPadTowers( chn );
  
  for ( int itower=0; itower< AssociatedPadTowerIndex.size(); itower++ ) {
    pad_towers.at( AssociatedPadTowerIndex.at( itower ) )->AddHit( chn );
  }

}

//----------------------------------------//
//   Add single hits to wire towers
//---------------------------------------//
void NSWsTGCGeometry::Fill_WireTower_SingleHit( NSWChannel* chn ) {

  std::vector<int> AssociatedWireTowerIndex = GetAssociatedWireTowers( chn );

  for ( int itower=0; itower< AssociatedWireTowerIndex.size(); itower++ ) {
    wire_towers.at( AssociatedWireTowerIndex.at( itower ) )->AddHit( chn );
  }

}

//----------------------------------------//
//   Add strip cluster to pad towers
//---------------------------------------//
void NSWsTGCGeometry::Fill_PadTower_StripCluster(int layer, NSWCluster* cluster, int clusterType ) {

  double mean_cluster = -1.0;

  if( clusterType==TypeClCoG ) 
    mean_cluster = cl->GetCOG();
  else if(clusterType==TypeClGauss)
    mean_cluster = cl->GetGaussianCentroid();
  else if(clusterType==TypeClCaruana)
    mean_cluster = cl->GetMeanCaruana();
  else if(clusterType==TypeClParabola)
    mean_cluster = cl->GetParabolaMean();
  else {
    std::cout << "WTF: cluster type not defined NSWsTGCGeometry::Fill_PadTower_StripCluster" << std::endl;
    return;
  }

  //-----------------------------------------//
  //   binomial search for pad tower row
  //-----------------------------------------//

  if ( Map_PadTowerRow_Position.at(layer).size() == 0 || 
       Map_PadTowerRow_TowerIndex.size() != Map_PadTowerRow_Position.at(layer).size() ) {
    std::cout << "WTF: No map of pad tower position " << std::endl;
    return;
  }

  int lower_bound = 0;
  int upper_bound = Map_PadTowerRow_Position.at(layer).size();
  int index = (lower_bound+upper_bound)/2;

  while ( true ) {

    std::pair<float, float> tower_pos = Map_PadTowerRow_Position.at(layer).at(index);

    //---------------------------------------------------------------------------------//
    //   if cluster mean inside tower row, associate cluster to all towers in row
    //---------------------------------------------------------------------------------//

    if ( mean_cluster => tower_pos.first && mean_cluster <= tower_pos.second ) {
      std::vector<int> tower_indexes =  Map_PadTowerRow_TowerIndex.at(index);
      for( int itower=0; itower<tower_indexes.size(); itower++ ) {
	pad_towers.at(tower_indexes.at(itower)).addStripCluster(layer, cl, clusterType);
      }
      break;
    }
    //-------------------------------------------------------------------------------//
    //  if cluster lower than lower edge of index row, it should be in between (lower bound) - (index-1)
    //-------------------------------------------------------------------------------//
    else if ( mean_cluster < tower_pos.first ) {
      upper_bound = index-1; // new upper bound
      index = (lower_bound + upper_bound)/2; 
    }
    //-------------------------------------------------------------------------------//
    //  if cluster higher than upper edge of index row, it should be in between (index+1) - (upper bound)
    //-------------------------------------------------------------------------------//
    else if ( mean_cluster > tower_pos.second ) {
      lower_bound = index+1; // new lower bound
      index = (lower_bound + upper_bound)/2;
    }

    if ( index < 0 || index > tower_indexes.size() ) {
      std::cout << "WTF: binomial search for tower row failed" << std::endl;
    }

  }
    
  return;

}

//----------------------------------------------------------//
//    Reconstruct Clusters
//----------------------------------------------------------//
void NSWsTGCGeometry::FindClusters(int clusterType){
  
  //----------------------------------------------------------//
  //  Reconstruct all clusters from all hits on all layers
  //----------------------------------------------------------//

  std::vector< std::vector <NSWCluster*>> Clusters_PerLayer_tmp;

  Clusters_PerLayer_tmp.resize(NLayers);
  for (int layer=0; layer<Clusters_PerLayer_tmp.size(); layer++){
    Clusters_PerLayer_tmp.at(layer).resize(0);
  }

  Clusters_PerLayer.resize(NLayers);
  for (int layer=0; layer<Clusters_PerLayer.size(); layer++){
    Clusters_PerLayer.at(layer).resize(0);
  }


  //----------------------------------------------------------//
  //          Loop layer and reconstruct clusters
  //----------------------------------------------------------//

  NSWCluster* cl = NULL;

  for (int layer=4; layer<NLayers; layer++){

    int min_chn = 0;
    int max_chn = NChannelsInLayer;

    int chn = min_chn; // count channels

    do{

      //std::cout << "channel in clusterization: " << chn << std::endl;

      //------------------------------------------------//
      //       If current channel is hit
      //------------------------------------------------//

      if (strip_channels[layer][chn] != NULL){// && strip_channels[layer][chn]->GetAmplitude()>0) { // there is a channel

        //-----------------------------------------------//
        //        If neighbor channel is hit
        //-----------------------------------------------//

        if(strip_channels[layer][chn+1] != NULL){// && strip_channels[layer][chn]->GetAmplitude()>0){

          //------------------------------------------------------//
          //  if current channel is neighbor on edge hit
          //  but next channel is true over threshold hit
          //------------------------------------------------------//

          if(strip_channels[layer][chn]->GetNeighbor()==0 && strip_channels[layer][chn+1]->GetNeighbor()==1){

	    //-------------------------------------------------//
            //  If no new clusters exist, begin new cluster
            //  place both current + next channel into cluster
            //-------------------------------------------------//

            if (cl == NULL) { // start of new cluster
              cl = new NSWCluster(strip_channels[layer][chn]); // first channel
              cl->AddChannel(strip_channels[layer][chn+1]);
              chn+=2; // processed 2 channels
            }
            //--------------------------------------------------//
            //   If cluster already exist, finish old cluster with neighbor off edge ch. (current ch.)
            //   start new cluster with next ch. into cluster
            //--------------------------------------------------//
            else { // continuation cluster ended
              cl->AddChannel(strip_channels[layer][chn]);

              //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
              cl->FitGaussian();
              cl->FitCaruana();
              cl->SaveParabolaMean();            
	      //std::cout << "1: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: "
	      // << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
              Clusters_PerLayer_tmp[layer].push_back(cl);

              // begin next cluster
              cl = NULL;
              cl = new NSWCluster(strip_channels[layer][chn]);
              cl->AddChannel(strip_channels[layer][chn+1]);

              // processed 2 channels
              chn+=2;
            }
          }
          //------------------------------------------------------------------//
          //   if both current and next cluster are neighbor on edge hits
          //------------------------------------------------------------------//
          else if(strip_channels[layer][chn]->GetNeighbor()==0 && strip_channels[layer][chn+1]->GetNeighbor()==0){ // divide the cluster

            //------------------------------------------------------------------------//
            //   If no cluster exists.  Ignore current ch. hit
            //   add next ch. hit, also edge hit neighbor on, as first hit in cluster
            //------------------------------------------------------------------------//
            if (cl == NULL) { // start of new cluster
              cl = new NSWCluster(strip_channels[layer][chn+1]); // first channel
              chn+=2; // processed 2 channels
            }
            //----------------------------------------------------------------------//
            //  If cluster already exist, finish current cluster with the current ch.
            //  which is it's last edge hit.  start new cluster with new edge hit (next ch.)
            //----------------------------------------------------------------------//
            else{
	      // finish current cluster with added edge hit (current chn)
              cl->AddChannel(strip_channels[layer][chn]);

              cl->FitGaussian();
              cl->FitCaruana();
              cl->SaveParabolaMean();
              //std::cout << "2: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: "
	      // << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
              Clusters_PerLayer_tmp[layer].push_back(cl);

              //  Begin new cluster with edge hit (chn+1)
              cl = NULL;
              cl = new NSWCluster(strip_channels[layer][chn+1]);
              chn+=2;
            }
          }

	  //---------------------------------------------------------------------//
          //     if both current and next ch. hits are true over threshold hits
          //     or if current ch. hit is over threshold and next ch. hit is edge neighbor hit
          //     Only process current over thr. hit. check next + 2nd next ch. next iteration of loop
          //---------------------------------------------------------------------//
          else{

            if (cl == NULL) { // start of new cluster
              cl = new NSWCluster(strip_channels[layer][chn]); // first channel
              chn+=1; // process one channel
            }
            else{ // or add current ch. to current cluster
              cl->AddChannel(strip_channels[layer][chn]);
              chn+=1; // process one channel
            }

          }
        } // end of if(strip_channels[layer][chn+1] != NULL)
        //----------------------------------------------------------//
        //  if current ch. is hit but next detector ch. is not hit
        //----------------------------------------------------------//
        else{
          //-------------------------------------//
          //  if current ch. is an edge hit
          //-------------------------------------//
          if(strip_channels[layer][chn]->GetNeighbor()==0){
            // if its the only channel in a cluster then ignore
            if (cl == NULL) {
              chn+=1; // skip channel hit
            }
            // if a cluster already exists. add to the back of the cluster
            // output cluster
            else{
              cl->AddChannel(strip_channels[layer][chn]);
              //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
              cl->FitGaussian();
              cl->FitCaruana();
              cl->SaveParabolaMean();
	      //std::cout << "3: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth()
              //<< " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
              Clusters_PerLayer_tmp[layer].push_back(cl);

              cl = NULL;
              chn+=1;
            }
          }
          //------------------------------------------//
          //  if current channel is a over thr. hit
          //------------------------------------------//
          else{
            //  if it's a new cluster, start cluster
            if (cl == NULL) { // start of new cluster
              cl = new NSWCluster(strip_channels[layer][chn]); // first channel
              chn+=1;
            }
            // if cluster exists, add the channel
            else{
              cl->AddChannel(strip_channels[layer][chn]);
              chn+=1;
            }
          }
        } // end of if current ch. is hit but next detector ch. is not hit
      } // end of if current channel is hit
      //---------------------------------------------//
      //  if current channel is not hit
      //---------------------------------------------//
      else{  // no channel
        //--------------------------------------------//
        //        if a cluster already exists
        //--------------------------------------------//
        if (cl != NULL) { // in a cluster
          //------------------------------------------//
          //  if we are not at the end of the layer
          //  and the next channel is a hit
          //------------------------------------------//
          if ((chn+1<=max_chn) && (strip_channels[layer][chn+1] != NULL)){// && strip_channels[layer][chn+1]->GetAmplitude()>0)){ // only one missing
            //--------------------------------------------//
            // if previous ch. is a true over thr. hit
            // and next ch. is a true over thr. hit
            // add a null hit into the middle of the cluster
            // assume this is a missing channel in the middle of a cluster
            //------------------------------------------------//
            if(strip_channels[layer][chn-1]->GetNeighbor()==1 && strip_channels[layer][chn+1]->GetNeighbor()==1){
              cl->AddChannel(NULL);
              cl->AddChannel(strip_channels[layer][chn+1]);
              chn +=2;
            }
            //-----------------------------------------------//
            // if previous hit was not a true over thr. hit
            // output the cluster as null hit is the end of cluster
            //-----------------------------------------------//
            else{ // cluster ended

              //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
              cl->FitGaussian();
              cl->FitCaruana();
              cl->SaveParabolaMean();
	      //std::cout << "4: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq()
              //<< " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
              Clusters_PerLayer_tmp[layer].push_back(cl);

              cl = NULL;
              chn +=1;

            }
          } // end of ((chn+1<=max_chn) && (strip_channels[layer][chn+1] != NULL))
          //-------------------------------------------------//
          // if next hit is also null just like current hit
          // and cluster exists then output cluster
          //-------------------------------------------------//
          else { // cluster ended
            //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
            cl->FitGaussian();
            cl->FitCaruana();
            cl->SaveParabolaMean();
            //std::cout << "5: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: "
            //<< cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
            Clusters_PerLayer_tmp[layer].push_back(cl);

            cl = NULL;
            chn +=2; //skip current+next null channels
          } // if 2 missing
        } // end of if cluster exists
        //----------------------------------------------------------//
        // if current ch. has no hit && no current cluster exists
        // don't do anything and move on
        //----------------------------------------------------------//
        else{
          chn++;
        }
      } // no channel
    } while (chn<=max_chn); // process every hit

    //----------------------------------------------------------//
    //  if we are at then end of the channel list
    // and we have 1 final cluster. output cluster
    //----------------------------------------------------------//

    if (cl != NULL) { // last cluster in this layer
      //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
      cl->FitGaussian();
      cl->FitCaruana();
      cl->SaveParabolaMean();
      //std::cout << "6: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() 
      // << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
      Clusters_PerLayer_tmp[layer].push_back(cl);

      cl = NULL;
    }
  } // end of loop over all layers

  delete cl;

  //==================================================================================================//
  //=============== End of Clustering taken from Michael's code in NSWClusterList ====================//
  //==================================================================================================//

  //--------------------------------------------------------------------------------------------------//
  //                                     Split Clusters
  //--------------------------------------------------------------------------------------------------//

  clusterList.Clear();
  clusterList.SetClusterList(Clusters_PerLayer_tmp);

  clusterList_badclusters.Clear();

  for (int layer=0; layer<NLayers; layer++){

    //---------------------------------------------------------//
    // find the location of any ch. = null = no hit
    // split cluster on nulls if, null is at start or end of cluster
    // or null is flanked by two neighbor off edge channels
    //---------------------------------------------------------//

    for (int i=0; i<clusterList.GetNumClusters(layer); i++){

      NSWCluster *cl = clusterList.GetCluster(layer, i);
      int num_chns = cl->GetNumChannel();

      std::vector<int> nul;
      nul.clear();

      for(int ichn=0; ichn<num_chns; ichn++){

        NSWChannel* channel = cl->GetChannel(ichn);
        // find NULL position
        if(channel==NULL){
          if( ichn==0 || ichn == num_chns-1 ) nul.push_back(ichn);
          else if( cl->GetChannel(ichn+1)->GetNeighbor() == 0 &&
                   cl->GetChannel(ichn-1)->GetNeighbor() == 0 )
            nul.push_back(ichn);
        }

      } //channel

      for(int j=0; j<nul.size(); j++){

        // split at all null points
        if( j == 0 ){
          clusterList.SplitCluster(layer, i, nul[j], true);
        }
        else{
          clusterList.SplitCluster(layer, i, nul[j] - nul[j-1] - 1, true);
        }
        if( nul[j] != 0 && nul[j] != num_chns-1 ) i++;
      }

    } // end of loop over cluster list

    //---------------------------------------------------------//
    // find the location of any valleys
    // valley-1 PDO > valley PDO < valley+1 PDO
    // split cluster on valleys
    //---------------------------------------------------------//

    float min_valley_PDO_diff = 5.0;

    for (int i=0; i<clusterList.GetNumClusters(layer); i++){

      NSWCluster *cl = clusterList.GetCluster(layer, i);
      int num_chns = cl->GetNumChannel();
      if(num_chns < 6) continue; // don't split clusters smaller than 6

      std::vector<int> valley;
      valley.clear();

      for(int ichn=0; ichn<num_chns; ichn++){

        NSWChannel* channel = cl->GetChannel(ichn);

        if( channel == NULL ) continue;

	if(ichn<=1 || ichn>=num_chns-2) continue; // do not break off single channels

        // find closest 2 hits and check to see if they are both greater than valley
        // by at least min_valley_PDO_diff amount of PDO

        if( cl->GetChannel(ichn-1) == NULL && cl->GetChannel(ichn+1) == NULL ){
          if ( ichn-2 < 0 || ichn+2 >= num_chns ) continue; // should not happen as end of cluster if null was removed before
          if( (cl->GetChannel(ichn-2)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff &&
              (cl->GetChannel(ichn+2)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff ) valley.push_back(ichn);
        }
        else if( cl->GetChannel(ichn-1) == NULL ){
          if ( ichn-2 < 0 ) continue; // should not happen as end of cluster if null was removed before
          if( (cl->GetChannel(ichn-2)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff &&
              (cl->GetChannel(ichn+1)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff ) valley.push_back(ichn);
        }
        else if( cl->GetChannel(ichn+1) == NULL ){
          if ( ichn+2 >= num_chns ) continue; // should not happen as end of cluster if null was removed before
          if( (cl->GetChannel(ichn-1)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff &&
              (cl->GetChannel(ichn+2)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff ) valley.push_back(ichn);
        }
        else if( cl->GetChannel(ichn-1) != NULL && cl->GetChannel(ichn+1) != NULL ){
          if( (cl->GetChannel(ichn-1)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff &&
              (cl->GetChannel(ichn+1)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff ) valley.push_back(ichn);
        }
      }// end of finding valley loop

      //-------------------------------------------//
      //         Split at every valley
      //-------------------------------------------//

      for(int j=0; j<valley.size(); j++){
        if( j==0 ){
          clusterList.SplitCluster(layer, i, valley[j], false);
        }
        else{
          clusterList.SplitCluster(layer, i, valley[j]-valley[j-1], false);
        }
        i++;
      }
    } // end of split at all valleys loop

    // if there are no more clusters then continue
    if(clusterList.GetNumClusters(layer)==0) continue;

    for (int icl=0; icl<clusterList.GetNumClusters(layer); icl++){

      std::string reason = "";
      NSWCluster *cl = clusterList.GetCluster(layer, icl);

      if( !isGoodCluster(cl,reason,clusterType) ) {
        clusterList_badclusters.AddCluster(layer, cl);
        clusterList.RemoveBadCluster(layer, icl);
        icl--;
      }

    }

    //--------------------------------------------------------------------//
    //               Final list of clusters are filled into Clusters_PerLayer
    //--------------------------------------------------------------------//

    for (int icl=0; icl<clusterList.GetNumClusters(layer); icl++){
      NSWCluster *cl = clusterList.GetCluster(layer, icl);

      cl->FitGaussian();
      cl->FitCaruana();

      Clusters_PerLayer.at(layer).push_back(cl);


    }

  } // end of loop over layers

  return;

}

void NSWsTGCGeometry::FindMaxROITower() {
}
    NSWClusterList* pt_clusterList = new NSWClusterList();
    pt_clusterList->SetClusterList(cluster);

    // clusterList.push_back(pt_clusterList);
    //printf("For pad tower %i:\n", i_pt);
    bool same_clusterList = false;
    for( int i_list=0; i_list<clusterList.size(); i_list++ ){
      //printf("Comparing to %i clusterList.\n", i_list);
      NSWClusterList* cl_list = clusterList.at(i_list);

      int same_layer=0;
      int max_layer=4;
      for( int i_layer=4; i_layer<8; i_layer++ ){
        int same_cluster=0;
        if( cl_list->GetNumClusters(i_layer) != pt_clusterList->GetNumClusters(i_layer) ) continue;
        else{
          if( cl_list->GetNumClusters(i_layer) == 0 ) max_layer--;

          for( int i_clu=0; i_clu<cl_list->GetNumClusters(i_layer); i_clu++ ){
            NSWCluster* cl = cl_list->GetCluster(i_layer, i_clu);
            bool same_channel = true;
            if( cl->GetNumChannel() != pt_clusterList->GetCluster(i_layer, i_clu)->GetNumChannel() ) break;
            else{
              for( int i_chn=0; i_chn<cl->GetNumChannel(); i_chn++ ){
                if( cl->GetChannel(i_chn)->GetDetectorStrip() != pt_clusterList->GetCluster(i_layer, i_clu)->GetChannel(i_chn)->GetDetectorStrip() ) {
                  same_channel = false;
                  break;
                }
              }
            }
            
            if( same_channel ) same_cluster++;
            else break;
          }

        }
        
        if( same_cluster != 0 && same_cluster == cl_list->GetNumClusters(i_layer) ) same_layer++;
        //printf("same layer: %i, same cluster: %i\n", same_layer, same_cluster);
      }

      if( same_layer == max_layer ){
        same_clusterList = true;
        break;
      }
    }
    
    if( !same_clusterList )
      clusterList.push_back(pt_clusterList);
    else
      pt_clusterList = NULL;


  } // next pad tower

}


// wires L1: 32, 48 ,57, 137 in total
// wires L2: 32, 49, 58, 139 in total
// wires L3: 32, 49, 58, 139 in total
// wires L4: 32, 48 ,57, 137 in total
/* void NSWPadWires::Fill(NSWSector* sec){

  if (sec == NULL) return;
  for (int layer=0; layer<8; layer++){
    for (int i=0; i<140; i++) wires[layer][i] = NULL;
  }


  // store channels:
  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(j);
    if (pac->GetEndPoint() != 0) { // keep only pFEB
      continue;
    }

    for (int k=0; k<pac->GetWidth(); k++){
      NSWChannel* chn = pac->GetChannel(k);

      if (chn->GetChannelType() != 2) continue; // only wires

      int layer = chn->GetLayer();
      int detchn = chn->GetDetectorStrip();
      if ((detchn>=0) && (detchn<140)) {
	      wires[layer][detchn]=chn; // store it here
      } 
    } // next k
  } // next j

  NSWCluster* cl = NULL;

  for (int layer=0; layer<8; layer++){
    int i=0; // count channels 
    do{
      if (wires[layer][i] != NULL) { // there is a channel
        if (cl == NULL) { // start of new cluster
          cl = new NSWCluster(wires[layer][i]); // first channel 
        } else { // continuation 
          cl->AddChannel(wires[layer][i]);
        }
        i++;
      } else {  // no channel
        if (cl != NULL) { // in a cluster
	        cluster[layer].push_back(cl);
	      }
	      cl = NULL;
  	    i++;
      }
    } while (i<140);
    if (cl != NULL) { // last cluster in this layer
      cluster[layer].push_back(cl);
      cl = NULL;
    }
    // printf ("layer %d: %li clusters.\n", layer, cluster[layer].size());
  }
}
*/

void NSWPadWires::Print(){

  for (int layer=0; layer<8; layer++){
    for (int i=0; i<140; i++){
      if (wires[layer][i] == NULL) continue;

      int phys_chn = wires[layer][i]->GetDetectorStrip();
      int vmmid =  wires[layer][i]->GetConfigID();
      int vmmchn = wires[layer][i]->GetVMMChannel();

      printf("layer = %i, type = %i, phys_chn = %i, vmmid = %i, vmmchn = %i, rocid = %i, position = %4.2f\n", wires[layer][i]->GetLayer(), wires[layer][i]->GetChannelType(), phys_chn, vmmid, vmmchn, wires[layer][i]->GetVMMID(), wires[layer][i]->GetWireCenterPosition());
    }
  }
}


