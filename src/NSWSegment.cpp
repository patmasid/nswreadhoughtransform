
#include <stdio.h>

#include "NSWRead/NSWSegment.h"

void linfit(double x[], double y[], int ndata, double sig[], int mwt, double *a, double *b, double *siga, double *sigb, double *chi2);

NSWSegment::NSWSegment(){
  Clear(); 
} 


NSWSegment::~NSWSegment(){
}


void NSWSegment::Clear(){
  for (int layer = 0; layer<8; layer++)  cluster[layer] = NULL;
  numClusters = 0;
  inter = -1;
  slope = -1;
  chi2 = -1;
  isLarge = 0;
  tech = 0;
} 


void NSWSegment::PutCluster (int layer, NSWCluster* cl){
  cluster[layer] = cl;
  numClusters++;
  if (cl != NULL){
    isLarge = cl->IsLarge();
    tech = cl->GetTechnology();
  }
}

void NSWSegment::RemoveCluster(int layer){
  cluster[layer] = NULL;
  numClusters--;
}

void NSWSegment::FitPrecision(double sigma){
  double y[4], x[4], sig[4], aerror, berror;
  int n=0;
  if (cluster[0] != NULL) {
    y[n] = cluster[0]->GetCOG();
    x[n] = cluster[0]->GetZPosition();
    n++;
  }
  if (cluster[1] != NULL) {
    y[n] = cluster[1]->GetCOG();
    x[n] = cluster[1]->GetZPosition();
    n++;
  }
  if (cluster[6] != NULL) {
    y[n] = cluster[6]->GetCOG();
    x[n] = cluster[6]->GetZPosition();
    n++;
  }
  if (cluster[7] != NULL) {
    y[n] = cluster[7]->GetCOG();
    x[n] = cluster[7]->GetZPosition();
    n++;
  }
  if (n<2) return;
  for (int i=0; i<n; i++) sig[i] = sigma;
  //for (int i=0; i<n; i++) printf ("Fit%d: %8.2f %8.2f", i, x[i], y[i]); 
  linfit (x, y, n, sig, 1, &inter, &slope, &aerror, &berror, &chi2);
  //printf ("  %8.2f %8.1f\n", inter, chi2);
}

void NSWSegment::FitPrecisionGaus(int start, double sigma){
  double y[4], x[4], sig[4], aerror, berror;
  int n=0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  sigma_4layer.clear();
  sigma_4layer.resize(4);
  if (cluster[0+start] != NULL) {
    y[n] = cluster[0+start]->GetGaussianCentroid();
    x[n] = cluster[0+start]->GetZPosition();
    //sig[n] = cluster[0+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[0+start]->GetWidth()<=2) {
	sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[0+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[1+start] != NULL) {
    y[n] = cluster[1+start]->GetGaussianCentroid(); //+ali5; // with alignment
    x[n] = cluster[1+start]->GetZPosition();
    //sig[n] = cluster[1+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[1+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[1+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[2+start] != NULL) {
    y[n] = cluster[2+start]->GetGaussianCentroid(); //+ali6; // with alignment
    x[n] = cluster[2+start]->GetZPosition();
    //sig[n] = cluster[2+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[2+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[2+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[3+start] != NULL) {
    y[n] = cluster[3+start]->GetGaussianCentroid();
    x[n] = cluster[3+start]->GetZPosition();
    //sig[n] = cluster[3+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[3+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[3+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (n<2) return;
  //if (sigma>=0) for (int i=0; i<n; i++) sig[i] = sigma;
  //for (int i=0; i<n; i++) printf ("Fit%d: %8.2f %8.2f  ", i, x[i], y[i]); 
  linfit (x, y, n, sig, 1, &inter, &slope, &aerror, &berror, &chi2);
  //printf ("  %8.2f %8.1f\n", inter, chi2);
}

void NSWSegment::FitPrecisionCaruana(int start, double sigma){
  double y[4], x[4], sig[4], aerror, berror;
  int n=0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  sigma_4layer.clear();
  sigma_4layer.resize(4);
  if (cluster[0+start] != NULL) {
    y[n] = cluster[0+start]->GetMeanCaruana();
    x[n] = cluster[0+start]->GetZPosition();
    //sig[n] = cluster[0+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[0+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[0+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[1+start] != NULL) {
    y[n] = cluster[1+start]->GetMeanCaruana(); //+ali5; // with alignment
    x[n] = cluster[1+start]->GetZPosition();
    //sig[n] = cluster[1+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[1+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[1+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[2+start] != NULL) {
    y[n] = cluster[2+start]->GetMeanCaruana(); //+ali6; // with alignment 
    x[n] = cluster[2+start]->GetZPosition();
    //sig[n] = cluster[2+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[2+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[2+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[3+start] != NULL) {
    y[n] = cluster[3+start]->GetMeanCaruana();
    x[n] = cluster[3+start]->GetZPosition();
    //sig[n] = cluster[3+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[3+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[3+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (n<2) return;
  if (sigma>=0) for (int i=0; i<n; i++) sig[i] = sigma;
  linfit (x, y, n, sig, 1, &inter, &slope, &aerror, &berror, &chi2);
}

void NSWSegment::FitPrecisionCOG(int start, double sigma){
  double y[4], x[4], sig[4], aerror, berror;
  int n=0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  sigma_4layer.clear();
  sigma_4layer.resize(4);

  if (cluster[0+start] != NULL) {
    y[n] = cluster[0+start]->GetCOG();
    x[n] = cluster[0+start]->GetZPosition();
    if (sigma>=0) {
      if(cluster[0+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[0+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[1+start] != NULL) {
    y[n] = cluster[1+start]->GetCOG()+ali5; // with alignment
    x[n] = cluster[1+start]->GetZPosition();
    if (sigma>=0) {
      if(cluster[1+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[1+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
   n++;
  }
  if (cluster[2+start] != NULL) {
    y[n] = cluster[2+start]->GetCOG()+ali6; // with alignment
    x[n] = cluster[2+start]->GetZPosition();
    if (sigma>=0) {
      if(cluster[2+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[2+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[3+start] != NULL) {
    y[n] = cluster[3+start]->GetCOG();
    x[n] = cluster[3+start]->GetZPosition();
    if (sigma>=0) {
      if(cluster[3+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[3+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (n<2) return;
  for (int i=0; i<n; i++) sig[i] = sigma;
  //for (int i=0; i<n; i++) printf ("Fit%d: %8.2f %8.2f  ", i, x[i], y[i]); 
  linfit (x, y, n, sig, 1, &inter, &slope, &aerror, &berror, &chi2);
  //printf ("  %8.2f %8.1f\n", inter, chi2);
}

void NSWSegment::FitPrecisionParabola(int start, double sigma){
  double y[4], x[4], sig[4], aerror, berror;
  int n=0;
  double ali5 = -0.03564; //alignment layer 5 in mm
  double ali6 = -0.07446;  //alignment layer 6 in mm
  sigma_4layer.clear();
  sigma_4layer.resize(4);
  if (cluster[0+start] != NULL) {
    y[n] = cluster[0+start]->GetParabolaMean();
    x[n] = cluster[0+start]->GetZPosition();
    //sig[n] = cluster[0+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[0+start]->GetWidth()<=2) {
	sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[0+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[1+start] != NULL) {
    y[n] = cluster[1+start]->GetParabolaMean(); //+ali5; // with alignment
    x[n] = cluster[1+start]->GetZPosition();
    //sig[n] = cluster[1+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[1+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[1+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[2+start] != NULL) {
    y[n] = cluster[2+start]->GetParabolaMean(); //+ali6; // with alignment
    x[n] = cluster[2+start]->GetZPosition();
    //sig[n] = cluster[2+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[2+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[2+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (cluster[3+start] != NULL) {
    y[n] = cluster[3+start]->GetParabolaMean();
    x[n] = cluster[3+start]->GetZPosition();
    //sig[n] = cluster[3+start]->GetGaussianSigma();
    if (sigma>=0) {
      if(cluster[3+start]->GetWidth()<=2) {
        sig[n] = 3.2; // most likely very off if only 2 hit cluster
        sigma_4layer[n] = 3.2;
      }
      else if(cluster[3+start]->GetWidth()<6){
        sig[n] = sigma;
        sigma_4layer[n] = sigma;
      }
      else{
        sig[n] = sigma*2;
        sigma_4layer[n] = sigma*2;
      }
    }
    n++;
  }
  if (n<2) return;
  //if (sigma>=0) for (int i=0; i<n; i++) sig[i] = sigma;
  //for (int i=0; i<n; i++) printf ("Fit%d: %8.2f %8.2f  ", i, x[i], y[i]); 
  linfit (x, y, n, sig, 1, &inter, &slope, &aerror, &berror, &chi2);
  //printf ("  %8.2f %8.1f\n", inter, chi2);
}

int NSWSegment::GetNumPrecision() {
  int n=0;
  if (cluster[0] != NULL) n++;
  if (cluster[1] != NULL) n++;
  if (cluster[6] != NULL) n++;
  if (cluster[7] != NULL) n++;
  if (cluster[2] != NULL){
    if (cluster[2]->GetTechnology() == 0) n++; //(MM = 1, sTGC = 0)
  }
  if (cluster[3] != NULL){
    if (cluster[3]->GetTechnology() == 0) n++; //(MM = 1, sTGC = 0)
  }
  if (cluster[4] != NULL){
    if (cluster[4]->GetTechnology() == 0) n++; //(MM = 1, sTGC = 0)
  }
  if (cluster[5] != NULL){
    if (cluster[5]->GetTechnology() == 0) n++; //(MM = 1, sTGC = 0)
  }
  return n;
}

int NSWSegment::GetNumCluster() {
  int n=0;
  for (int i=0; i<8; i++){
    if (cluster[i] != NULL) n++;
  }
  return n;
}

double NSWSegment::GetInvCDF(){
  double dof = GetDOF();
  double inv_cdf = ROOT::Math::chisquared_cdf_c(GetChi2(),dof);
  return inv_cdf;
}

double NSWSegment::GetCDF(){
  double dof = GetDOF(); 
  double cdf = ROOT::Math::chisquared_cdf(GetChi2(),dof);
  return cdf;
}

double NSWSegment::GetPitch() {
  for (int i=0; i<8; i++){
    if (cluster[i] != NULL) return cluster[i]->GetPitch();
  }
  return -1; // should not happen
}


double NSWSegment::GetPredicted (int layer){
  return inter + slope * NSWCluster::GetZPosition(layer, isLarge, tech);
}

void NSWSegment::Compare(NSWSegment* seg, std::vector<int> &common_clusters){
  for(int layer=4; layer<8; layer++){
    NSWCluster* clu1 = GetCluster(layer);
    NSWCluster* clu2 = seg->GetCluster(layer);
    if(clu1 == NULL || clu2 == NULL) continue;
    if(clu1->isSameCluster(clu2)){
      common_clusters.push_back(layer);
    }
  }
}

void NSWSegment::Print () {
  // print some information
  printf ("%s segment: inter = %f, angle = %f, chi2 = %f\n", (tech==0)?"sTGC":"MM", inter, GetAngle(), chi2);
  for (int layer=0; layer<8; layer++){
    NSWCluster *c = cluster[layer];
    if (c != NULL){
      printf ("Layer %d: radius = %d, numChn = %2d, cog = %6.1f\n", layer, c->GetRadius(), c->GetNumChannel(), c->GetCOG());
    } else {
      printf ("Layer %d: Missing.\n", layer);
    }
      
  }
  printf ("\n");
}

