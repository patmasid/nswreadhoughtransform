#include <stdio.h>

#include "NSWRead/NSWPadWires.h"



NSWPadWires::NSWPadWires(){
}


NSWPadWires::~NSWPadWires(){
}

// pads L1: 96 56 52, 204 in total
// pads L2: 96 56 52, 204 in total
// pads L3: 96 56 56, 208 in total
// pads L4: 96 56 56, 208 in total
void NSWPadWires::FindPadTower(NSWSector* sec){

  padTower_list.clear();

  if (sec == NULL) return;
  for (int layer=0; layer<8; layer++){
    for (int i=0; i<210; i++) pads[layer][i] = NULL;
  }

  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(j);

    //if (pac->GetEndPoint() == 1){ // sFEB
    //  for (int k=0; k<pac->GetWidth(); k++){
    //    NSWChannel* chn = pac->GetChannel(k);
    //    printf("layer %d phys_chn %d vmmid %d vmmchn %d pos %4.2f\n", chn->GetLayer(), chn->GetDetectorStrip(), chn->GetConfigID(), chn->GetVMMChannel(), chn->GetStripPosition());
    //  }
    //}

    if (pac->GetEndPoint() != 0) { // keep only pFEB
      continue;
    }

    //----------------------------------------------//
    //                Store Pads
    //----------------------------------------------//

    for (int k=0; k<pac->GetWidth(); k++){
      NSWChannel* chn = pac->GetChannel(k);

      if (chn->GetChannelType() != 0) continue; // only pads

      // if (chn->GetChannelType() == 0){
      //   std::vector<float> pad_pos = chn->GetPadPosition();
      //   printf("layer %d phys_chn %d vmmid %d vmmchn %d\n", chn->GetLayer(), chn->GetDetectorStrip(), chn->GetConfigID(), chn->GetVMMChannel());
      //   for(int i=0; i<pad_pos.size(); i++){
      //     printf("  %4.2f", pad_pos.at(i));
      //   }
      //   std::cout<<std::endl;
      // }

      int layer = chn->GetLayer();
      int detchn = chn->GetDetectorStrip();
      if ((detchn>=0) && (detchn<210)) {
        pads[layer][detchn]=chn; // store it here
      }
    } //next k
  } // next j


  //----------------------------------------//
  //         put into pad towers
  //----------------------------------------//

  for(int i_pt=0; i_pt<padTower_QL1C.size(); i_pt++){

    std::vector<NSWChannel*> padTower;
    padTower.clear();

    int num_matchPads=0;

    for(int j_layer=0; j_layer<4; j_layer++){

      int pad_chn = padTower_QL1C[i_pt][j_layer];

      if( pads[j_layer+4][pad_chn] != NULL ) num_matchPads++;
      padTower.push_back( pads[j_layer+4][pad_chn] );
    }

    if(num_matchPads >= 3)
      padTower_list.push_back(padTower);
  }

}

void NSWPadWires::MatchPadTower(NSWSector* sec){

  if (sec == NULL) return;
  for (int layer=0; layer<8; layer++){
    for (int i=0; i<140; i++) wires[layer][i] = NULL;
  }

  // store channels
  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(j);
    if (pac->GetEndPoint() != 0) { // keep only pFEB
      continue;
    }

    for (int k=0; k<pac->GetWidth(); k++){
      NSWChannel* chn = pac->GetChannel(k);

      if (chn->GetChannelType() != 2) continue; // only wires

      int layer = chn->GetLayer();
      int detchn = chn->GetDetectorStrip();
      if ((detchn>=0) && (detchn<140)) {
	      wires[layer][detchn]=chn; // store it here
      } 
    } // next k
  } // next j


  for(int i_pt=0; i_pt<GetNumPadTower(); i_pt++){
    std::vector<NSWCluster*> tmp;
    std::vector<std::vector<NSWCluster*>> cluster(8, tmp);
    std::vector<NSWChannel*> pad_tower = GetPadTower(i_pt);


    NSWCluster* cl = NULL;

    for (int layer=4; layer<8; layer++){
      std::vector<float> pad_pos;
      if(pad_tower[layer-4] != NULL)
        pad_pos = pad_tower[layer-4]->GetPadPosition();
      else
        continue;

      double y_max = *max_element(pad_pos.begin()+4, pad_pos.end());
      double y_min = *min_element(pad_pos.begin()+4, pad_pos.end());

      std::vector<int> first_chns;

      for (int chn_num=0; chn_num<140; chn_num++){
        NSWChannel* chn = wires[layer][chn_num];

        if ( chn != NULL && cl != NULL ){
          cl->AddChannel(chn);
          continue;
        }
        else if ( cl != NULL ){
          cluster[layer].push_back(cl);
          cl = NULL;
          continue;
        }

        if ( chn != NULL){
          // int phys_chn = chn->GetDetectorStrip();
          // int vmmid    = chn->GetConfigID();
          // int vmmchn   = chn->GetVMMChannel();
          // printf("layer = %i, type = %i, phys_chn = %i, vmmid = %i, vmmchn = %i, position = %4.2f %4.2f %4.2f\n", chn->GetLayer(), chn->GetChannelType(), phys_chn, vmmid, vmmchn, chn->GetWireUpperEdge(), chn->GetWireCenterPosition(), chn->GetWireLowerEdge());
          if((chn->GetWireUpperEdge() < y_max && chn->GetWireUpperEdge() > y_min) || 
            (chn->GetWireLowerEdge() < y_max && chn->GetWireLowerEdge() > y_min)) {
              
            if (cl == NULL){
              first_chns.push_back(chn_num);
              cl = new NSWCluster(chn);
            }
            else{
              //cl->AddChannel(chn);
              printf("Warning: Expection found in NSWPadWires::MatchPadTower!\n");
            }
          }
        } 
      } // next chn
      if (cl != NULL) { // last cluster in this layer
        cluster[layer].push_back(cl);
        cl = NULL;
      }

      if (first_chns.size() != cluster[layer].size())
        printf("Warning: Expection found in NSWPadWires::MatchPadTower!\n");

      // backward search for consecutive wire channels
      for (int num_clu=0; num_clu<cluster[layer].size(); num_clu++){
        for (int num_chn=first_chns[num_clu]-1; num_chn>=0; num_chn--){
          if (wires[layer][num_chn] != NULL)  cluster[layer][num_clu]->AddChannel(wires[layer][num_chn]);
          else break;
        }
      }

    } // next layer

    NSWClusterList* pt_clusterList = new NSWClusterList();
    pt_clusterList->SetClusterList(cluster);

    // clusterList.push_back(pt_clusterList);
    //printf("For pad tower %i:\n", i_pt);
    bool same_clusterList = false;
    for( int i_list=0; i_list<clusterList.size(); i_list++ ){
      //printf("Comparing to %i clusterList.\n", i_list);
      NSWClusterList* cl_list = clusterList.at(i_list);

      int same_layer=0;
      int max_layer=4;
      for( int i_layer=4; i_layer<8; i_layer++ ){
        int same_cluster=0;
        if( cl_list->GetNumClusters(i_layer) != pt_clusterList->GetNumClusters(i_layer) ) continue;
        else{
          if( cl_list->GetNumClusters(i_layer) == 0 ) max_layer--;

          for( int i_clu=0; i_clu<cl_list->GetNumClusters(i_layer); i_clu++ ){
            NSWCluster* cl = cl_list->GetCluster(i_layer, i_clu);
            bool same_channel = true;
            if( cl->GetNumChannel() != pt_clusterList->GetCluster(i_layer, i_clu)->GetNumChannel() ) break;
            else{
              for( int i_chn=0; i_chn<cl->GetNumChannel(); i_chn++ ){
                if( cl->GetChannel(i_chn)->GetDetectorStrip() != pt_clusterList->GetCluster(i_layer, i_clu)->GetChannel(i_chn)->GetDetectorStrip() ) {
                  same_channel = false;
                  break;
                }
              }
            }
            
            if( same_channel ) same_cluster++;
            else break;
          }

        }
        
        if( same_cluster != 0 && same_cluster == cl_list->GetNumClusters(i_layer) ) same_layer++;
        //printf("same layer: %i, same cluster: %i\n", same_layer, same_cluster);
      }

      if( same_layer == max_layer ){
        same_clusterList = true;
        break;
      }
    }
    
    if( !same_clusterList )
      clusterList.push_back(pt_clusterList);
    else
      pt_clusterList = NULL;


  } // next pad tower

}


// wires L1: 32, 48 ,57, 137 in total
// wires L2: 32, 49, 58, 139 in total
// wires L3: 32, 49, 58, 139 in total
// wires L4: 32, 48 ,57, 137 in total
/* void NSWPadWires::Fill(NSWSector* sec){

  if (sec == NULL) return;
  for (int layer=0; layer<8; layer++){
    for (int i=0; i<140; i++) wires[layer][i] = NULL;
  }


  // store channels:
  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(j);
    if (pac->GetEndPoint() != 0) { // keep only pFEB
      continue;
    }

    for (int k=0; k<pac->GetWidth(); k++){
      NSWChannel* chn = pac->GetChannel(k);

      if (chn->GetChannelType() != 2) continue; // only wires

      int layer = chn->GetLayer();
      int detchn = chn->GetDetectorStrip();
      if ((detchn>=0) && (detchn<140)) {
	      wires[layer][detchn]=chn; // store it here
      } 
    } // next k
  } // next j

  NSWCluster* cl = NULL;

  for (int layer=0; layer<8; layer++){
    int i=0; // count channels 
    do{
      if (wires[layer][i] != NULL) { // there is a channel
        if (cl == NULL) { // start of new cluster
          cl = new NSWCluster(wires[layer][i]); // first channel 
        } else { // continuation 
          cl->AddChannel(wires[layer][i]);
        }
        i++;
      } else {  // no channel
        if (cl != NULL) { // in a cluster
	        cluster[layer].push_back(cl);
	      }
	      cl = NULL;
  	    i++;
      }
    } while (i<140);
    if (cl != NULL) { // last cluster in this layer
      cluster[layer].push_back(cl);
      cl = NULL;
    }
    // printf ("layer %d: %li clusters.\n", layer, cluster[layer].size());
  }
}
*/

void NSWPadWires::Print(){

  for (int layer=0; layer<8; layer++){
    for (int i=0; i<140; i++){
      if (wires[layer][i] == NULL) continue;

      int phys_chn = wires[layer][i]->GetDetectorStrip();
      int vmmid =  wires[layer][i]->GetConfigID();
      int vmmchn = wires[layer][i]->GetVMMChannel();

      printf("layer = %i, type = %i, phys_chn = %i, vmmid = %i, vmmchn = %i, rocid = %i, position = %4.2f\n", wires[layer][i]->GetLayer(), wires[layer][i]->GetChannelType(), phys_chn, vmmid, vmmchn, wires[layer][i]->GetVMMID(), wires[layer][i]->GetWireCenterPosition());
    }
  }
}


