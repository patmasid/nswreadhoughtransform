/** \file linfit.c
copied from numerical recipes and modified.
*/

#include <math.h>
//#include <stdio.h>



/** Straight line fit from Numerical Recipes.

   Given a set of points x[0..ndata-1], y[0..ndata-1] with standard deviations
   sig[0..ndata-1], fit them to a straight line y = a+bx by minimizing chi-squared.
   Returned are a,b and their respective probable uncertainties siga and sigb,
   the chi-square chi2. If mwt = 0 on input, then the
   standard deviations are assumed to be unavailable: 
   the normalization of chi2 is to unit standard deviation on all points.
   
   @note This was copied from Numerical Recipes and modified:
   The arrays are 0-based and the goodness-of-fit is not computed.
   All float variables are now double. 
   
   @param x the 0-based array of x values.
   @param y the 0-based array of y values.
   @param ndata the number of elements in x, y, and sig.
   @param sig the 0-based array of y errors
   @param mwt if set to 0, then the errors in sig are ignored.
   @param a the intercept of the line.
   @param b the slope of the line.
   @param siga the error on the intercept.
   @param sigb the error on the slope
   @param chi2 the chi-squared of the fit.
   
*/

void linfit(double x[], double y[], int ndata, double sig[], int mwt, double *a, double *b, double *siga, double *sigb, double *chi2){
  int i;
  double wt, t, sxoss, sx = 0.0, sy = 0.0, st2 = 0.0, ss, sigdat;
  double diff;

  *b = 0.0;
  if (mwt) {
    ss = 0.0;
    for (i = 0; i<ndata; i++) {
      wt = 1.0/(sig[i]*sig[i]);
      ss +=  wt;
      sx +=  x[i]*wt;
      sy +=  y[i]*wt;
    }
  } else {
    for (i = 0; i<ndata; i++) {
      sx +=  x[i];
      sy +=  y[i];
    }
    ss = ndata;
  }
  sxoss = sx/ss;

  //for (i = 0; i<ndata; i++) printf ("%i: %f %f %f\n", i, x[i],y[i],sig[i]);

  if (mwt) {
    for (i=0; i<ndata; i++) {
      t = (x[i]-sxoss)/sig[i];
      st2 +=  t*t;
      *b +=  t*y[i]/sig[i];
    }
  } else {
    for (i=0;i<ndata; i++) {
      t = x[i]-sxoss;
      st2 +=  t*t;
      *b +=  t*y[i];
    }
  }
  *b /=  st2;
  *a = (sy-sx*(*b))/ss;
  *siga = sqrt((1.0 + sx*sx/(ss*st2))/ss);
  *sigb = sqrt(1.0/st2);
  // calculate chi2:
  *chi2 = 0.0;
  if (mwt  ==  0) { // have no errors
    for (i=0; i<ndata; i++){
      diff = y[i] - (*a)-(*b)*x[i];
      *chi2 +=  diff * diff;
    }
    sigdat = sqrt((*chi2)/(ndata-2));
    *siga *=  sigdat;
    *sigb *=  sigdat;
  } else { // use sig[] as errors
    for (i = 0; i<ndata; i++){
      diff = (y[i]-(*a)-(*b)*x[i])/sig[i];
      *chi2 +=  diff * diff;
    }
  }
}


