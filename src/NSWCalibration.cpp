// Stores the result of a calibration and calibrates channels
#include<stdio.h>
#include<stdlib.h>

#include"NSWRead/NSWCalibration.h"


NSWCalibration::NSWCalibration(){
  cal = new TCalRecord[3*1048576];
  for (int i=0; i<3*1048576; i++){
    SetIntercept (i, 32.0);
    //SetIntercept (i, 49.0);
    SetSlope (i, 0.3); 
    //SetSlope (i, 1.09);
    cal[i].tmin =  72; // average tdo calibration
    cal[i].tmax = 160;
    cal[i].status = 0;
  }
} 

NSWCalibration::NSWCalibration(char *calmeth){
  SetCalibMethod(calib_meth=calmeth);
  cal = new TCalRecord[3*1048576];
  for (int i=0; i<3*1048576; i++){
    if(calib_meth=="th"){
      SetIntercept (i, 49.0); 
      SetSlope (i, 1.09);
    }
    else if(calib_meth=="tp"){
      SetIntercept (i, 32.0);
      SetSlope (i, 0.3);
    }
    //std::cout << "cal[databaseID].slope: " << cal[i].slope << " cal[databaseID].inter: " << cal[i].inter << std::endl;
    cal[i].tmin =  72; // average tdo calibration
    cal[i].tmax = 160;
    cal[i].status = 0;
  }
}

NSWCalibration::~NSWCalibration(){
  delete [] cal;
}



int NSWCalibration::ReadPDO (char *filename){
  //read all PDO calibration constants from pdocal.txt
  char line[200]; // read from file
  unsigned int id; // channelID
  float inter, slope;
  FILE* infile = fopen(filename, "r");//open data file

  if (infile==NULL) { //error if file incorrect or DNE
    printf("Can't open input file '%s'.\n", filename);
    return 0; // false
  }
  
  while (fgets (line, 199, infile) != NULL){
    //printf ("%s\n", line);
    int n = sscanf (line, "%d %f %f", &id,  &slope, &inter);
    if (n != 3) {
      printf ("Cannot read from '%s'\n", line);
      fclose (infile);
      return 0;
    }
    SetIntercept (id, inter);
    SetSlope (id, slope);
    cal[id].status = 1; // calibrated
  } // while
  fclose (infile);
  return 1;
}



void NSWCalibration::Calibrate (NSWSector* sec){
  //stores pedestals in strips
  for (int i=0; i<sec->GetNumPacket(); i++){
    NSWPacket* pac = sec->GetPacket(i);
    for (int j=0; j<pac->GetWidth(); j++){
      NSWChannel* chn = pac->GetChannel(j);
      unsigned int db = chn->GetDatabaseID();
      //std::cout << "Calibration: GetDatabaseID - " << chn->GetDatabaseID() << " GetIntercept(db): " << GetIntercept(db) << " GetSlope(db): " << GetSlope(db) << std::endl;
      chn->SetBaseline(GetIntercept(db));
      chn->SetGain(GetSlope(db));
      chn->SetTmin(cal[db].tmin);
      chn->SetTmax(cal[db].tmax);
      chn->SetStatus(cal[db].status);
    }
  }
}



void NSWCalibration::Print () {
  // print some information
  for (int i=0; i<3*1048576; i++){
    if (cal[i].status==0) continue;
    printf("%6d  %8.2f %8.4f %3i %3i %3i\n", 
	   i, GetIntercept(i), GetSlope(i),
	   cal[i].tmin, cal[i].tmax, cal[i].status);
  }
}



