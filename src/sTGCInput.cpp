//---------------------------------------------------------------------------
// sTGC mapping code by Michael Schernau and 
// Micha Haacke, Sonia Kabana, Sebastian Olivares, Pablo Ulloa
//
//---------------------------------------------------------------------------
// System include(s):
#include <iostream>
#include <iomanip>
#include <fstream>

// Local include(s):
#include "NSWRead/sTGCInput.h"
 
sTGCInput::sTGCInput()
{

  /* 
  // for mapping method 1 (code early 2021)
  
  //std::cout << "--- Initialize" << std::endl;

  radius = 0;
  layer = 0;
  vmmid = 0;
  vmmchn = 0;
  chna = 0;
  chnc = 0;
  start = 0;
  chn = 0;
  chn_c = 0;
  
  j1 = 0;
  j2 = 0;
  j3 = 0;
  j4 = 0;
  j5 = 0;
  j6 = 0;
  j7 = 0;
  j8 = 0;
  j9 = 0;
  j10 = 0;
  j11 = 0;
  j12 = 0;
  j13 = 0;
  j14 = 0;
  j15 = 0;
  j16 = 0;
  j17 = 0;
  j18 = 0;
  j19 = 0;
  j20 = 0;
  j21 = 0;
  j22 = 0;
  j23 = 0;
  j24 = 0;
  j25 = 0;
  j26 = 0;
  j27 = 0;
  j28 = 0;
  j29 = 0;
  j30 = 0;
  j31 = 0;
  
  k1 = 0;
  k2 = 0;
  k3 = 0;
  k4 = 0;
  k5 = 0;
  k6 = 0;
  k7 = 0;
  k8 = 0;
  k9 = 0;
  k10 = 0;
  k11 = 0;
  k12 = 0;
  k13 = 0;
  k14 = 0;
  k15 = 0;
  k16 = 0;
  k17 = 0;
  k18 = 0;
  k19 = 0;
  k20 = 0;
  k21 = 0;
  k22 = 0;
  k23 = 0;
  k24 = 0;
  k25 = 0;
  k26 = 0;
  k27 = 0;
  k28 = 0;
  k29 = 0;
  k30 = 0;
  k31 = 0;

  l1 = 0;
  l2 = 0;
  l3 = 0;
  l4 = 0;
  l5 = 0;
  l6 = 0;
  l7 = 0;
  l8 = 0;
  l9 = 0;
  l10 = 0;
  l11 = 0;
  l12 = 0;
  l13 = 0;

  m1 = 0;
  m2 = 0;

  n1 = 0;
  n2 = 0;


  // Initialize the offsets for chna mapping of pad layer 0 
  int i=0,j=0;
  
  for(j=0; j<dimvimmid; j++) {
  for(i=0; i<dimvmmchn; i++) {
    offset_pad_S_P_layer0_radius0[j][i]=0; 
    offset_pad_S_C_layer0_radius0[j][i]=0;

    offset_pad_L_P_layer0_radius0[j][i]=0; 
    offset_pad_L_C_layer0_radius0[j][i]=0;

    offset_pad_S_P_layer0_radius1[j][i]=0; 
    offset_pad_S_C_layer0_radius1[j][i]=0;

    offset_pad_L_P_layer0_radius1[j][i]=0; 
    offset_pad_L_C_layer0_radius1[j][i]=0;
    
    offset_pad_S_P_layer0_radius2[j][i]=0; 
    offset_pad_S_C_layer0_radius2[j][i]=0;

    offset_pad_L_P_layer0_radius2[j][i]=0; 
    offset_pad_L_C_layer0_radius2[j][i]=0;
    }
    }
  
  // set the offset values 
  set_offsets_chna_pad_layer0();
 

  // Initialize the offsets for chnc mapping of pad layer 0 
  for(j=0; j<dimvimmid; j++) {
  for(i=0; i<dimvmmchn; i++) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]=0;
      offset_chnc_pad_S_C_layer0_radius0[j][i]=0;

      offset_chnc_pad_S_P_layer0_radius1[j][i]=0;
      offset_chnc_pad_S_C_layer0_radius1[j][i]=0;

      offset_chnc_pad_S_P_layer0_radius2[j][i]=0;
      offset_chnc_pad_S_C_layer0_radius2[j][i]=0;

      offset_chnc_pad_L_P_layer0_radius0[j][i]=0;
      offset_chnc_pad_L_C_layer0_radius0[j][i]=0;

      offset_chnc_pad_L_P_layer0_radius1[j][i]=0;
      offset_chnc_pad_L_C_layer0_radius1[j][i]=0;

      offset_chnc_pad_L_P_layer0_radius2[j][i]=0;
      offset_chnc_pad_L_C_layer0_radius2[j][i]=0;
      }
      }
      
      // set the offset values 
  set_offsets_chnc_pad_layer0();
 

  // ----  initialize and set values for offsets chnc pad layer 3 Small Confirm
  // // Small
  for(j=0; j<dimvimmid; j++) {
  for(i=0; i<dimvmmchn; i++) {
  offset_chnc_pad_S_C_layer3_radius0[j][i]=0;
  offset_chnc_pad_S_C_layer2_radius0[j][i]=0;
  offset_chnc_pad_S_C_layer3_radius1[j][i]=0;
  offset_chnc_pad_S_C_layer2_radius1[j][i]=0;
  offset_chnc_pad_S_C_layer3_radius2[j][i]=0;
  offset_chnc_pad_S_C_layer2_radius2[j][i]=0;
}
}
  // set the offset values 
  set_offsets_chnc_pad_layer3_Small_Confirm();
  set_offsets_chnc_pad_layer2_Small_Confirm();

  // ---- end of initialize and set values for offsets chnc pad layer 3 Small Confirm

  // Large
  for(j=0; j<dimvimmid; j++) {
  for(i=0; i<dimvmmchn; i++) {
  offset_chnc_pad_L_P_layer3_radius0[j][i]=0;
  offset_chnc_pad_L_P_layer3_radius1[j][i]=0;
  offset_chnc_pad_L_P_layer3_radius2[j][i]=0;
  offset_chnc_pad_L_P_layer2_radius0[j][i]=0;
  offset_chnc_pad_L_P_layer2_radius1[j][i]=0;
  offset_chnc_pad_L_P_layer2_radius2[j][i]=0;
  offset_chnc_pad_L_C_layer3_radius0[j][i]=0;
  offset_chnc_pad_L_C_layer3_radius1[j][i]=0;
  offset_chnc_pad_L_C_layer3_radius2[j][i]=0;
  offset_chnc_pad_L_C_layer2_radius0[j][i]=0;
  offset_chnc_pad_L_C_layer2_radius1[j][i]=0;
  offset_chnc_pad_L_C_layer2_radius2[j][i]=0;
}
}
  // set the offset values 
  set_offsets_chnc_pad_layer3_Large_Pivot();
  set_offsets_chnc_pad_layer2_Large_Pivot();
  set_offsets_chnc_pad_layer3_Large_Confirm();
  set_offsets_chnc_pad_layer2_Large_Confirm();

  // ---- end of initialize and set values for offsets chnc pad layer 3 Small Confirm
    
  // end of mapping method 1 (part of it) 
  */
}

sTGCInput::~sTGCInput()
{
  
  // std::cout << "--- Destruction" << std::endl;
  
}


// ------------- function map_strip
//
unsigned int sTGCInput::map_strip(char endcap_A_or_C, char Large_or_Small, char Pivot_or_Confirm, int radius, int layer, int vmmid, int vmmchn) {


  //method 3

  int AC,SL;
  if ( endcap_A_or_C == 'A' ) AC=0;
  if ( endcap_A_or_C == 'C' ) AC=1;

  if( Large_or_Small ==  'S' )  SL=0;
  if( Large_or_Small ==  'L' )  SL=1;

  int layer0to7;
  layer0to7 = layer;


  /* 
  // method 1
  
  channelType= "strip";

  //  std::cout<<"  map_strip reads: "<<" endcap_A_or_C=  "<<endcap_A_or_C<<" Large_or_Small= "<<Large_or_Small<<" Pivot_or_Confirm= "<<Pivot_or_Confirm<<" radius = "<<radius<<" layer= "<<layer<<" vmmid=  "<<vmmid<<" vmmchn= "<<vmmchn<<std::endl;

  if( Large_or_Small ==  'L' )  chamberLong = 'L';
  if( Large_or_Small ==  'S' )  chamberLong = 'S';

  if( Pivot_or_Confirm == 'P' )  chamber = 'P';
  if( Pivot_or_Confirm == 'C' )  chamber = 'C';


// change to 0-3 layers 

   if( chamberLong == 'S' ) {
        if ( layer <= 3 ) chamber = 'C';
        if ( layer >= 4 ) chamber = 'P';
      } else if ( chamberLong == 'L' ) {
        if ( layer <= 3 ) chamber = 'P';
        if ( layer >= 4 ) chamber = 'C';
      }

        int layermap;
        if ( layer < 4 ) layermap = layer;
        else layermap = layer - 4;

        layer = layermap;
        
       
	//      std::cout<<"  map_strip AFTER layer change "<<" endcap_A_or_C=  "<<endcap_A_or_C<<" Large_or_Small= "<<Large_or_Small<<" Pivot_or_Confirm= "<<Pivot_or_Confirm<<" radius = "<<radius<<" layer= "<<layer<<" vmmid=  "<<vmmid<<" vmmchn= "<<vmmchn<<std::endl;
       

	// ------------------- Mapping code -----------------------------------

    
	// Michael Schernau code  
	chn = -1; 
	start = 406; 
	if ( radius == 1 ) start = 512;
	if ( chamberLong == 'L') start = start + 2;
	if ( (layer == 0) || (layer == 3) ) {
  chn = start - vmmid*64 - vmmchn;
  if ( chn < 71 ) chn = chn + 32;
  if ( chamberLong == 'L' ) {
  if ( chn < 9 ) chn = chn + 32;
}
  else {
  if ( chn < 7 ) chn = chn + 32;
}
}
	else {
  chn = 23 - 64 + vmmid*64 + vmmchn;
  if ( chn > 70 ) chn = chn - 32;
  if ( chn > 102 ) chn = chn - 32;
  if ( chamberLong == 'L' ) chn = chn + 2;
}

	      
	// below is code from Micha Haacke, Sonia Kabana, Sebastian Olivares, Pablo Ulloa
    
	// ****************************************************************
	// Wheel - A
	// ****************************************************************



	// ----------------------------------------------------------------
	// ---- Strips
	// ----------------------------------------------------------------

	if ( channelType == "strip" ) {
            
  if ( chamberLong == 'S' ) {
  
  if ( radius == 1 ) {
    
  // Sebastian: layer-0
  if ( layer == 0 ) {
  if ( (vmmid == 6 && vmmchn > 57) || vmmid == 7 ) chn = chn - 32;
}

  // Sonia: layer-1-2-3
  if ( layer == 1 || layer == 2 ) {

  if ( vmmid == 2 ) {
  if ( vmmchn >= 19 && vmmchn <= 47 ) {
  chn = chn - 73;
}
  if ( vmmchn >= 48 && vmmchn <= 63 ) {
  chn = chn - 41;
}
}
  if ( vmmid >= 3 && vmmid <= 7 ) {
  chn = chn - 41;
}
} 

  if ( layer == 3 ) {
  if ( vmmid == 6 ) {
  if ( vmmchn >= 58 && vmmchn <= 63 ) {
  chn = chn - 32;
}
}
  if ( vmmid == 7 ) {
  chn = chn - 32;
}
}    
       
}
  
  if ( radius == 2 ) {
    
  // Sebastian: layer-0
  if ( layer == 0 ) {
  if ( (vmmid == 3 && vmmchn > 12) || vmmid == 4 || (vmmid == 5 && vmmchn < 16) ) chn = chn + 106;
  if ( (vmmid == 5 && vmmchn > 15) || (vmmid == 6 && vmmchn < 48) ) chn = chn + 74;
  if ( (vmmid == 6 && vmmchn > 47) || vmmid == 7 ) chn = chn + 42;
}

  // Sonia: layer-1-2-3
  if ( layer == 1 ) {
  if ( vmmid >= 3 && vmmid <= 7 ) {
  chn = chn - 99;
}
}
  if (layer == 2) {
  if (vmmid == 3 ) {
  if (vmmchn >= 13 && vmmchn <= 63 ) {
  chn = chn - 99;
}
}
  if ( vmmid >= 4 && vmmid <= 7 ) {
  chn = chn - 99;
}
}
  if ( layer == 3 ) {
  if ( vmmid == 3 ) {
  if ( vmmchn >= 13 && vmmchn <= 63 ) {
  chn = chn + 106;
}
} 
  if ( vmmid == 4 ) {
  chn = chn + 106;
}
  if ( vmmid == 5 ) {
  if ( vmmchn >= 0 && vmmchn <= 15 ) {
  chn = chn + 106;
}
  if ( vmmchn >= 16 && vmmchn <= 63 )
    {
  chn = chn + 74;
}
} 
  if ( vmmid == 6 ) {
  if ( vmmchn >= 0 && vmmchn <= 47 ) {
  chn = chn + 74;
}
  if ( vmmchn >= 48 && vmmchn <= 63 ) {
  chn = chn + 42;
}
}
  if ( vmmid == 7 ) {
  chn = chn + 42;
}  
} 

}
         
} // SHORT

  if ( chamberLong == 'L' ) {

  if ( radius == 1 ) {

  // Sebastian: layer-0
  if ( layer == 0 ) {
  if ( (vmmid == 2 && vmmchn > 17) || vmmid == 3 || vmmid == 4 || vmmid == 5 || (vmmid == 6 && vmmchn < 60) ) chn = chn - 2;
  if ( (vmmid == 6 && vmmchn > 59) || vmmid == 7 ) chn = chn - 34;
}

  // Sonia: layer-1-2-3
  if ( layer == 1 || layer == 2 ) {
  if ( vmmid == 2 ) {
  if ( vmmchn >= 18 && vmmchn <= 47 ) {
  chn = chn - 74;
} 
  if ( vmmchn >= 48 && vmmchn <= 63 ) {
  chn = chn - 42;
}
} 
  if ( vmmid >= 3 && vmmid <= 7 ) {
  chn = chn - 42;
}
} 
  if ( layer == 3 ) {
  if ( vmmid == 2 ) {
  if ( vmmchn >= 18 && vmmchn <= 63 ) {
  chn = chn - 2;
}
}
  if ( vmmid == 3 ) {
  chn = chn - 2;
}
  if ( vmmid >= 4 && vmmid <= 5 ) {
  chn = chn - 2;
}
  if ( vmmid == 6 ) {
  if ( vmmchn >= 0 && vmmchn <= 59 ) {
  chn = chn - 2;
}
  if ( vmmchn >= 60 && vmmchn <= 63 ) {
  chn = chn - 34;
}
}
  if ( vmmid == 7 ) {
  chn = chn - 34;
}    
}
    
}

  if ( radius == 2 ) {

  // Sebastian: layer-0
  if ( layer == 0 ) {
  if ( (vmmid == 2 && vmmchn > 30) || vmmid == 3 || vmmid == 4 || (vmmid == 5 && vmmchn < 18) ) chn = chn + 104;
  if ( (vmmid == 5 && vmmchn > 17) || (vmmid == 6 && vmmchn < 48) ) chn = chn + 72;
  if ( (vmmid == 6 && vmmchn > 47) || vmmid == 7 ) chn = chn + 40;
}

  // Sonia: layer-1-2-3
  if ( layer == 1 or layer == 2 ) {
  if ( vmmid == 2 ) {
  if ( vmmchn >= 31 && vmmchn <= 47 ) {
  chn = chn - 87;
}
  if ( vmmchn >= 48 && vmmchn <= 63 ) {
  chn = chn - 55;
}
}
  if ( vmmid >= 3 && vmmid <= 7 ) {
  chn = chn - 55;
}
}
  if ( layer == 3 ) {
  if ( vmmid == 2 ) {
  if ( vmmchn >= 31 && vmmchn <= 63 ) {
  chn = chn + 104;
}
}
  if ( vmmid >= 3 && vmmid <= 4 ) {
  chn = chn + 104;
}
  if ( vmmid == 5 ) {
  if ( vmmchn >= 0 && vmmchn <= 17 ) {
  chn = chn + 104;
}
  if ( vmmchn >= 18 && vmmchn <= 63 ) {
  chn = chn + 72;
}
}
  if ( vmmid == 6 ) {
  if ( vmmchn >= 0 && vmmchn <= 47 ) {
  chn = chn + 72;
}
  if ( vmmchn >= 48 && vmmchn <= 63 ) {
  chn = chn + 40;
}
}
  if ( vmmid == 7 ) {
  chn = chn + 40;
}
}  
    
}

} // LONG
      
} // STRIP


	// ------------ end strip mapping ---------------


	chn_c = chn;

	//----------- return mapped channel A or C 
	int channel; channel=-2; 
	// -2 means that channel was not set by this program
	// -1 means that channel does not exist in mapping.txt
	if ( endcap_A_or_C == 'A' ) channel = chn;
	if ( endcap_A_or_C == 'C' ) channel = chn_c;
	//std::cout<<" before return of map_strip channekl= "<<channel<<std::endl;

	return(channel);
	// end method 1
 */


  // Sonia Kabana
  // method 3
  return MappedChn_strip_init[AC][SL][layer0to7][radius][vmmid][vmmchn];
 
 
} // end of map_strip(




// ------------- function map_pad
//
unsigned int sTGCInput::map_pad(char endcap_A_or_C, char Large_or_Small, char Pivot_or_Confirm, int radius, int layer, int vmmid, int vmmchn) {


  int layer0to7;
  layer0to7=layer;

  int AC,SL;
  if ( endcap_A_or_C == 'A' ) AC=0;
  if ( endcap_A_or_C == 'C' ) AC=1;

  if( Large_or_Small ==  'S' )  SL=0;
  if( Large_or_Small ==  'L' )  SL=1;


  /* 
 //method 1
  channelType = "pad";
  if( Large_or_Small ==  'L' )  chamberLong = 'L';
  if( Large_or_Small ==  'S' )  chamberLong = 'S';

  if( Pivot_or_Confirm == 'P' )  chamber = 'P';
  if( Pivot_or_Confirm == 'C' )  chamber = 'C';

  //std::cout<<"  map_pad 2 "<<" endcap_A_or_C=  "<<endcap_A_or_C<<" Large_or_Small= "<<Large_or_Small<<" Pivot_or_Confirm= "<<Pivot_or_Confirm<<" radius = "<<radius<<" layer= "<<layer<<" vmmid=  "<<vmmid<<" vmmchn= "<<vmmchn<<std::endl;


// change to 0-3 layers 

   if( chamberLong == 'S' ) {
        if ( layer <= 3 ) chamber = 'C';
        if ( layer >= 4 ) chamber = 'P';
      } else if ( chamberLong == 'L' ) {
        if ( layer <= 3 ) chamber = 'P';
        if ( layer >= 4 ) chamber = 'C';
      }

        int layermap;
        if ( layer < 4 ) layermap = layer;
        else layermap = layer - 4;

        //layer = layermap;
        layer = layermap;
        
       
	//  std::cout<<"  map_pad AFTER layer change "<<" endcap_A_or_C=  "<<endcap_A_or_C<<"chamber= "<<chamber<<" Large_or_Small= "<<Large_or_Small<<" Pivot_or_Confirm= "<<Pivot_or_Confirm<<" radius = "<<radius<<" layer= "<<layer<<" vmmid=  "<<vmmid<<" vmmchn= "<<vmmchn<<std::endl;


// ------------------- Mapping code -----------------------------------

    
    // Michael Schernau code  
    chn = -1; 
    start = 406; 
    if ( radius == 1 ) start = 512;
    if ( chamberLong == 'L') start = start + 2;
    if ( (layer == 0) || (layer == 3) ) {
      chn = start - vmmid*64 - vmmchn;
      if ( chn < 71 ) chn = chn + 32;
      if ( chamberLong == 'L' ) {
      if ( chn < 9 ) chn = chn + 32;
      }
      else {
      if ( chn < 7 ) chn = chn + 32;
      }
    }
    else {
      chn = 23 - 64 + vmmid*64 + vmmchn;
      if ( chn > 70 ) chn = chn - 32;
      if ( chn > 102 ) chn = chn - 32;
      if ( chamberLong == 'L' ) chn = chn + 2;
    }

          
    
    // below is code from Micha Haacke, Sonia Kabana, Sebastian Olivares, Pablo Ulloa
    
    // ****************************************************************
    // Wheel - A
    // ****************************************************************
    
    
   
    // ----------------------------------------------------------------
    // ---- Pads
    // ----------------------------------------------------------------
   



//------- Here starts chna mapping of pad layer 0, Sonia

int i,j;


  if ( channelType == "pad" ) {

    if (  chamberLong =='S' ) {
      if ( radius == 0 ) {
        if ( layer == 0 ) {
	
          if ( chamber == 'P' ) {
	  
            if ( vmmid == 1 ) {
	    j=vmmid;
              if ( vmmchn >= 36 && vmmchn <=63 ) {
	      i=vmmchn;
              chn = chn + offset_pad_S_P_layer0_radius0[j][i];
	      } 
	      }

            if ( vmmid == 2 ) {
	    j=vmmid;
	    if ( vmmchn >= 0 && vmmchn <= 39 ) {
  i=vmmchn;
  chn = chn + offset_pad_S_P_layer0_radius0[j][i];
} 
}

} // end if ( chamber == 'P' ) { 

	  if ( chamber == 'C' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 36 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn + offset_pad_S_C_layer0_radius0[j][i];
} 
} 
  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 39 ) {
  i=vmmchn;
  chn = chn + offset_pad_S_C_layer0_radius0[j][i];
} 
} 

} // if ( chamber == 'C' )
	  
} // if ( layer == 0 )
} // if ( radius == 0 )
      
      
      
      if ( radius == 1 ) {
  if ( layer == 0 ) {

  if ( chamber == 'P' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 49 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn + offset_pad_S_P_layer0_radius1[j][i];
} 
} 

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 14 ) {
  i=vmmchn;
  chn = chn + offset_pad_S_P_layer0_radius1[j][i];
} 
} 
  
} // if ( chamber == 'P' ) 

  
  if ( chamber == 'C' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 40 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn + offset_pad_S_C_layer0_radius1[j][i] ;
} 
} 
  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 23 ) {
  i=vmmchn;
  chn = chn  + offset_pad_S_C_layer0_radius1[j][i];
} 
} 
  
}  // if ( chamber == 'C' ) 

} // if ( layer == 0 )
} // if ( radius == 1 )

      

      if ( radius == 2 ) {
  if ( layer == 0 ) {
  
  if ( chamber == 'P' ) {

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 9 && vmmchn <= 32 ) {
  i=vmmchn;
  chn = chn  + offset_pad_S_P_layer0_radius2[j][i];
} 
} 

} // if ( chamber == 'P' )  


  if ( chamber == 'C'  ) {
  
  if ( vmmid == 2 ) {  
  j=vmmid;
  if ( vmmchn >= 2 && vmmchn <= 40 ) {
  i=vmmchn;
  chn = chn  + offset_pad_S_C_layer0_radius2[j][i];
} 
} 

} // if ( chamber == 'C'  )

  
} // if ( layer == 0 )
} // if ( radius == 2 )
      
} // if ( code[1]=='S' )   if (  chamberLong  SMALL


    
    if (  chamberLong =='L' ) {
  
  if ( radius == 0 ) {
  if ( layer == 0 ) {
                        
  if ( chamber == 'P' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 2 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_P_layer0_radius0[j][i];
} 
} 

  if ( vmmid == 2 ) { 
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 39 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_P_layer0_radius0[j][i];
} 
} 

} // if ( chamber == 'P' )


  if ( chamber == 'C' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 8 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_C_layer0_radius0[j][i];
} 
} 

  if ( vmmid == 2 ) { 
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 39 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_C_layer0_radius0[j][i];
} 
} 

  
} // if ( chamber == 'C' ) 
      
} // if ( layer == 0 )
} // if ( radius == 0 )


  if ( radius == 1 ) {
  if ( layer == 0 ) {

  if ( chamber == 'P' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 38 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_P_layer0_radius1[j][i];
} 
} 

  if ( vmmid == 2 ) { 
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 29 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_P_layer0_radius1[j][i];
} 
} 

} // if ( chamber == 'P' )


  if ( chamber == 'C' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 38 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_C_layer0_radius1[j][i];
} 
} 

  if ( vmmid == 2 ) {  
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 29 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_C_layer0_radius1[j][i];
} 
} 

}  // if ( chamber == 'C' )
       
} // if ( layer == 0 )
} // if ( radius == 1 )


  if ( radius == 2 ) {
  if ( layer == 0 ) {

  if ( chamber == 'P' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 26 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_P_layer0_radius2[j][i];
} 
}

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 21 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_P_layer0_radius2[j][i];
} 
}

} // if ( chamber == 'P' )


  if ( chamber == 'C' ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 26 && vmmchn <= 63 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_C_layer0_radius2[j][i];
} 
} 

  if(  vmmid == 2 ) {  
  j=vmmid;
  if(  vmmchn >= 0 && vmmchn <= 13 ) {
  i=vmmchn;
  chn = chn  + offset_pad_L_C_layer0_radius2[j][i];
} 
} 
  
}// if ( chamber == 'C' )
} // if ( layer == 0 )
} // if ( radius == 2 )
        
} // if ( code[1]=='L' )

} // if ( channelType == "pad" ) {
   

  //------- Here ends chna mapping of pad layer 0 


  // ----------------------------------------------------------------
  // ---- chna ---- Pad Layer(1,2,3) ---- Pablo ----
  // ----------------------------------------------------------------
  if (channelType == "pad"){
  if (chamberLong == 'S'){
  if (radius == 0){
  if (layer == 1){
  if (vmmid==1 && 24<=vmmchn && vmmchn<=47) chn = 112 - chn + 2*((chn+1)%4);
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=27) ) chn =  80 - chn + 2*((chn+1)%4);
}
  if (layer == 2){
  if (chamber == 'P'){
  if (vmmid==1 && 24<=vmmchn && vmmchn<=47) chn = 98 - chn;
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=10) ) chn = 66 - chn;
}
  if (chamber == 'C'){
  if (vmmid==1 && 24<=vmmchn && vmmchn<=47) chn = 119 - chn;
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=31) ) chn =  87 - chn;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  if ( (vmmid==1 && 53<=vmmchn && vmmchn<=61) || (vmmid==2 &&  0<=vmmchn && vmmchn<=39) ) chn = 288 - chn + 2*((chn+1)%3);
  if (vmmid==1 && 62<=vmmchn && vmmchn<=63) chn = chn - 268;
}
  if (chamber == 'C'){
  if ( (vmmid==1 && 32<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=39) ) chn = 308 - chn + 2*((chn+1)%4);
}
}
}
  if (radius == 1){
  if (layer == 1){
  if (chamber == 'P'){
  if ( (vmmid==1 && 49<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=14) ) chn = 69 - chn + 2*(chn%2);
}
  if (chamber == 'C'){
  if (vmmid==1 && 40<=vmmchn && vmmchn<=47) chn = 109 - chn + 2*(chn%3);
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=23) ) chn =  77 - chn + 2*((chn+2)%3);
}
}
  if (layer == 2){
  if (vmmid==1 && 42<=vmmchn && vmmchn<=47) chn = 110 - chn;
  if ( (vmmid==1 && 49<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=22) ) chn =  78 - chn;
}
  if (layer == 3){
  if ( (vmmid==1 && 41<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=21) ) chn = 406 - chn + 2*(chn%3);
}
}
  if (radius == 2){
  if (layer == 1){
  if (chamber == 'P'){
  if (vmmid==2 &&  9<=vmmchn && vmmchn<=32) chn = 87 - chn + 2*(chn%2);
}
  if (chamber == 'C'){
  if (vmmid==1 &&  1<=vmmchn && vmmchn<=39) chn = 61 - chn + 2*(chn%3);
}
}
  if (layer == 2){
  if (chamber == 'P'){
  if (vmmid==2 &&  2<=vmmchn && vmmchn<=40) chn =  96 - chn;
}
  if (chamber == 'C'){
  if (vmmid==1 &&  0<=vmmchn && vmmchn<=41) chn =  65 - chn;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  if (vmmid==2 &&  1<=vmmchn && vmmchn<=39) chn =  276 - chn + 2*((chn+1)%3);
}
  if (chamber == 'C'){
  if (vmmid==2 &&  0<=vmmchn && vmmchn<=41) chn =  277 - chn + 2*(chn%3);
}
}
}
}
  if (chamberLong == 'L'){
  if (radius == 0){
  if (layer == 1){
  if (chamber == 'P'){
  if (vmmid==1 &&  8<=vmmchn && vmmchn<=47) chn = 130 - chn + 2*((chn+3)%6);
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=45) ) chn =  98 - chn + 2*((chn+5)%6);
}
  if (chamber == 'C'){
  if (vmmid==1 &&  8<=vmmchn && vmmchn<=47) chn = 124 - chn + 2*((chn+3)%6);
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=39) ) chn =  92 - chn + 2*((chn+5)%6);
}
}
  if (layer == 2){
  if (chamber == 'P'){
  if (vmmid==1 &&  8<=vmmchn && vmmchn<=47) chn = 145 - chn;
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=47) ) chn = 113 - chn;
  if (vmmid==2 && 48<=vmmchn && vmmchn<=55) chn =  81 - chn;
}
  if (chamber == 'C'){
  if (vmmid==1 && 13<=vmmchn && vmmchn<=47) chn = 134 - chn;
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=44) ) chn = 102 - chn;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  if (vmmid==1 &&  0<=vmmchn && vmmchn<=5 ) chn = chn - 331;
  if ( (vmmid==1 &&  6<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=39) ) chn = 347 - chn + 2*((chn+4)%7);
  if (vmmid==2 && vmmchn==40)               chn = chn - 226;
  if (vmmid==2 && 41<=vmmchn && vmmchn<=47) chn = 240 - chn;
}
  if (chamber == 'C'){
  if ( (vmmid==1 &&  3<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=34) ) chn = 337 - chn + 2*(chn%6);
}
}
}
  if (radius == 1){
  if (layer == 1){
  if (vmmid==1 && 34<=vmmchn && vmmchn<=47) chn = 112 - chn + 2*((chn+1)%4);
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=25) ) chn =  80 - chn + 2*((chn+1)%4);
}
  if (layer == 2){
  if (chamber == 'P'){
  if (vmmid==1 && 24<=vmmchn && vmmchn<=47) chn = 124 - chn;
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=34) ) chn =  92 - chn;
}
  if (chamber == 'C'){
  if (vmmid==1 && 34<=vmmchn && vmmchn<=47) chn = 115 - chn;
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=25) ) chn =  83 - chn;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  if ( (vmmid==1 && 29<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=39) ) chn = 418 - chn + 2*((chn+3)%5);
}
  if (chamber == 'C'){
  if ( (vmmid==1 && 38<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=29) ) chn = 410 - chn + 2*((chn+3)%4);
}
}
}
  if (radius == 2){
  if (layer == 1){
  if (chamber == 'P'){
  if (vmmid==1 && 44<=vmmchn && vmmchn<=47) chn = chn - 12;
}
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=39) ) chn =  94 - chn + 2*((chn+3)%4);
}
  if (layer == 2){
  if (chamber == 'P'){
  if (vmmid==1 && 34<=vmmchn && vmmchn<=47) chn = 129 - chn;
}
  if ( (vmmid==1 && 48<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=39) ) chn =  97 - chn;
}
  if (layer == 3){
  if (chamber == 'P'){
  if ( (vmmid==1 && 26<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=31) ) chn = 315 - chn + 2*((chn+1)%5);
}
  if (chamber == 'C'){
  if ( (vmmid==1 && 26<=vmmchn && vmmchn<=63) || (vmmid==2 &&  0<=vmmchn && vmmchn<=17) ) chn = 316 - chn + 2*((chn+1)%4);
}
}
}
}
}
  //--------------------
  //end_Pad_Layer(1,2,3)
  //--------------------





  // ****************************************************************
  // Wheel - C
  // ****************************************************************
    
  chn_c = chn; // this line must be after all chna mapping and before all chnc mapping
  
  

  // ----------------------------------------------------------------
  // ---- Pads
  // ----------------------------------------------------------------
    
  //------- Sonia: Here starts chnc mapping of pad layer 0 


  if ( channelType == "pad" ) {

  if ( chamberLong =='S' ) {
  
  if ( layer == 0 ) {
  
  if ( radius == 0 ) {
  
  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 36 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_S_P_layer0_radius0[j][i];
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_S_C_layer0_radius0[j][i];
} 
} // if ( vmmid == 1

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 39 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_S_P_layer0_radius0[j][i];
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_S_C_layer0_radius0[j][i];
} 
} // if ( vmmid == 2

} // if ( radius == 0 ) {


  if ( radius == 1 ) {

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 49 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_S_P_layer0_radius1[j][i];
} 
} // if ( vmmid == 

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 14 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_S_P_layer0_radius1[j][i];
} 
} // if ( vmmid == 2


  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 40 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_S_C_layer0_radius1[j][i];
} 
} // if ( vmmid == 1

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 23 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_S_C_layer0_radius1[j][i];
} 
} // if ( vmmid == 2
  
} // if ( radius == 1
  
        
  if ( radius == 2 ) {
  
  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 9 && vmmchn <= 32 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_S_P_layer0_radius2[j][i];
} 
} // if ( vmmid == 2

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 2 && vmmchn <= 40 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_S_C_layer0_radius2[j][i];
} 
} // if ( vmmid == 2

} // if ( radius == 2
  
} // if ( layer == 0 ) {
  
} // if ( code[1]=='S' ) {


  if ( chamberLong =='L' ) {
  
  if ( layer == 0 ) {

  if ( radius == 0 ) { 

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 2 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_L_P_layer0_radius0[j][i];
} 
} // if ( vmmid == 1

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 39 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_L_P_layer0_radius0[j][i];
} 
} // if ( vmmid == 1


  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 8 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_L_C_layer0_radius0[j][i];
} 
} // if ( vmmid == 1


  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 39 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_L_C_layer0_radius0[j][i];
} 
} // if ( vmmid == 2
  
} // if ( radius == 0


  if ( radius == 1 ) { 

  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 38 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_L_P_layer0_radius1[j][i];
} 
} // if ( vmmid == 1


  if ( vmmid == 2) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 1 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_L_P_layer0_radius1[j][i];
} 
} // if ( vmmid == 2

  if ( vmmid == 2) {
  j=vmmid;
  if ( vmmchn >= 2 && vmmchn <= 29 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_L_P_layer0_radius1[j][i];
} 
} // if ( vmmid == 1

  
  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 38 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_L_C_layer0_radius1[j][i];
} 
} // if ( vmmid == 1

  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 29 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_L_C_layer0_radius1[j][i];
} 
} // if ( vmmid == 1
  
} // if ( radius == 1

  
  if ( radius == 2) {
  
  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 26 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_L_P_layer0_radius2[j][i];
} 
} // if ( vmmid == 1


  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 21 ) {
  i=vmmchn;
  if ( chamber == 'P' ) chn_c = chn_c  + offset_chnc_pad_L_P_layer0_radius2[j][i];
} 
} // if ( vmmid == 2


  if ( vmmid == 1 ) {
  j=vmmid;
  if ( vmmchn >= 26 && vmmchn <= 63 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_L_C_layer0_radius2[j][i];
} 
} // if ( vmmid == 1


  if ( vmmid == 2 ) {
  j=vmmid;
  if ( vmmchn >= 0 && vmmchn <= 13 ) {
  i=vmmchn;
  if ( chamber == 'C' ) chn_c = chn_c  + offset_chnc_pad_L_C_layer0_radius2[j][i];
} 
} // if ( vmmid == 2

} // if ( radius == 2
  
} // if ( layer == 0 ) {
  
} // if ( chamberLong =='L' ) {

} // if ( channelType == "pad" ) {


  //------- Sonia: Here ends chnc mapping of pad layer 0 








  //------- Pablo: Here starts chnc mapping of pad layer 1 

  if ( channelType == "pad" ) {
  if (layer == 1 ) {
  if ( chamberLong =='S' ) {
  if ( radius == 0 ) {
  if ( ( vmmid==1 && 24<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=27 ) )
    chn_c = chn_c  + 3 - 2*((chn_c+3)%4);
}
  if ( radius == 1 ) {
  if ( chamber=='P')
    if ( ( vmmid==1 && 49<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=14 ) )
      chn_c = chn_c - 1 + 2*(chn_c%2);
  if ( chamber=='C')
    if ( ( vmmid==1 && 40<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=23 ) )
      chn_c = chn_c + 2 - 2*((chn_c+2)%3);
}
  if ( radius == 2 ) {
  if ( chamber=='P' )
    if ( vmmid==2 &&  9<=vmmchn && vmmchn<=32 )
      chn_c = chn_c - 1 + 2*(chn_c%2);
  if ( chamber=='C' )
    if ( vmmid==1 &&  1<=vmmchn && vmmchn<=39 )
      chn_c = chn_c + 2 - 2*((chn_c+2)%3);
}
}//end if ( chamberLong =='S' )
  if ( chamberLong =='L' ){
  if ( radius == 0 ) {
  if ( chamber=='P')
    if ( ( vmmid==1 &&  8<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=45 ) )
      chn_c = chn_c  + 5 - 2*((chn_c+5)%6);
  if ( chamber=='C')
    if ( ( vmmid==1 &&  8<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=39 ) )
      chn_c = chn_c  + 5 - 2*((chn_c+5)%6);
}
  if ( radius == 1 ) {
  if ( ( vmmid==1 && 34<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=25 ) )
    chn_c = chn_c  + 3 - 2*((chn_c+3)%4);
}
  if ( radius == 2 ) {
  if ( chamber=='P' )
    if ( ( vmmid==1 && 44<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=39 ) )
      chn_c = chn_c  + 3 - 2*((chn_c+3)%4);
  if ( chamber=='C')
    if ( ( vmmid==1 && 52<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=39 ) )
      chn_c = chn_c  + 3 - 2*((chn_c+3)%4);
}
}//end if ( chamberLong =='L' ) 
}//end if ( layer == 1 ) {
}//end if ( channelType == "pad" ) {

  //------- Here ends chnc mapping of pad layer 1 

  //------- Pablo: Here starts chnc mapping of pad layer 2,3 Small Pivot
  if ( channelType == "pad" ) {
  if ( chamberLong =='S' ) {
  if (layer == 2 ) {
  if ( radius == 0 ) {
  if ( chamber=='P' )
    if ( ( vmmid==1 && 24<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  1<=vmmchn && vmmchn<=10 ) )
      chn_c = chn_c + 2 - 2*((chn_c+2)%3);
}//end if ( radius == 0 ) {
  if ( radius == 1 ) {
  if ( chamber=='P' )
    if ( ( vmmid==1 && 42<=vmmchn && vmmchn<=63 ) || ( vmmid==2 &&  1<=vmmchn && vmmchn<=22 ) )
      chn_c = chn_c + 2 - 2*((chn_c+2)%3);
}//end if ( radius == 1 ) {
  if ( radius == 2 ) {
  if ( chamber=='P' )
    if ( vmmid==2 &&  2<=vmmchn && vmmchn<=40 )
      chn_c = chn_c + 2 - 2*((chn_c+2)%3);
}//end if ( radius == 2 ) {
}//end if ( layer == 2 ) {
  if (layer == 3 ) {
  if ( radius == 0 ) {
  if ( chamber=='P' )
    if ( ( vmmid==1 && 53<=vmmchn && vmmchn<=62 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=39 ) )
      chn_c = chn_c + 2 - 2*((chn_c+2)%3);
}//end if ( radius == 0 ) {
  if ( radius == 1 ) {
  if ( chamber=='P' )
    if ( ( vmmid==1 && 41<=vmmchn && vmmchn<=62 ) || ( vmmid==2 &&  0<=vmmchn && vmmchn<=21 ) )
      chn_c = chn_c + 2 - 2*((chn_c+2)%3);
}//end if ( radius == 1 ) {
  if ( radius == 2 ) {
  if ( chamber=='P' )
    if ( vmmid==2 &&  1<=vmmchn && vmmchn<=39 )
      chn_c = chn_c + 2 - 2*((chn_c+2)%3);
}//end if ( radius == 2 ) {
}//end if ( layer == 3 ) {
}//end if ( chamberLong =='S' )
}//end if ( channelType == "pad" ) {

  //------- Here ends chnc mapping of pad layer 2,3 Small Pivot -- Pablo




  //  Sonia correction of chnc code : SMALL CONFIRM pad layer 2,3
  //


  if ( endcap_A_or_C == 'C' ) {
    
  if ( channelType == "pad" ) {
    
  if ( chamberLong =='S' ) {

  if ( chamber == 'C' ) {
    
  // ---------- radius 0 

  if ( radius == 0 ) {

  // ---- layer 3

  if ( layer == 3 ) {
  if ( (vmmid == 1 && vmmchn > 31) || (vmmid == 2 && vmmchn < 40) ) {

  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_S_C_layer3_radius0[j][i];

} // vmmid
} // layer 3

  // ---- layer 2

  if ( layer == 2 ) {
  if ( (vmmid == 1 and vmmchn > 23) or (vmmid == 2 and vmmchn < 32) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_S_C_layer2_radius0[j][i];
}
} // layer 2 


  // ---- layer 2


} // radius 0


  // ---------- radius 1

  j= vmmid;
  i= vmmchn;

  if ( radius == 1 ) {

  if ( layer == 3 ) {

  if ( vmmid == 1 and vmmchn == 41 ) chn_c = chn_c + offset_chnc_pad_S_C_layer3_radius1[j][i] ;

  if ( vmmid == 1 and (vmmchn > 42 and vmmchn < 63) ) chn_c = chn_c + offset_chnc_pad_S_C_layer3_radius1[j][i];
        
  if ( vmmid == 2 and vmmchn == 21 ) chn_c = chn_c + offset_chnc_pad_S_C_layer3_radius1[j][i];
  if ( vmmid == 2 and vmmchn < 20 ) chn_c = chn_c + offset_chnc_pad_S_C_layer3_radius1[j][i];

} // if layer 3



  if ( layer == 2 ) {
  if ( vmmid == 1 and vmmchn == 42 ) chn_c = chn_c + offset_chnc_pad_S_C_layer2_radius1[j][i];
  if ( vmmid == 1 and (vmmchn > 43 and vmmchn < 64) ) chn_c = chn_c + offset_chnc_pad_S_C_layer2_radius1[j][i];
  if ( vmmid == 2 and vmmchn == 22 ) chn_c = chn_c + offset_chnc_pad_S_C_layer2_radius1[j][i];
  if ( vmmid == 2 and (vmmchn > 0 and vmmchn < 21) ) chn_c = chn_c + offset_chnc_pad_S_C_layer2_radius1[j][i];
} // if layer 2

} // radius 1


  // ---------- radius 2


  if ( radius == 2 ) {
  if ( layer == 3 and (vmmid == 2 and vmmchn == 0) ) chn_c = chn_c + offset_chnc_pad_S_C_layer3_radius2[j][i];
  if ( layer == 3 and (vmmid == 2 and vmmchn == 41) ) chn_c = chn_c + offset_chnc_pad_S_C_layer3_radius2[j][i];
  if ( layer == 3 and (vmmid == 2 and (vmmchn > 1 and vmmchn < 40)) )  chn_c = chn_c + offset_chnc_pad_S_C_layer3_radius2[j][i];

  // layer 2 radius 2
  if ( layer == 2 and (vmmid == 1 and vmmchn == 0) ) chn_c = chn_c + offset_chnc_pad_S_C_layer2_radius2[j][i];
  if ( layer == 2 and (vmmid == 1 and vmmchn == 41) ) chn_c = chn_c + offset_chnc_pad_S_C_layer2_radius2[j][i];
  if ( layer == 2 and (vmmid == 1 and (vmmchn > 1 and vmmchn < 40)) ) chn_c = chn_c + offset_chnc_pad_S_C_layer2_radius2[j][i];
      
} // if radius 2



  //----------------------------
} // CONFIRM
  
} // if ( code[1]=='S' ) {

} // if ( channelType == "pad" ) {
} // if (  endcap_A_or_C == 'C' 


  // end of Sonia;s part of correction of chnc code : SMALL CONFIRM pad layer 2,3








  // LARGE 
  
  if ( endcap_A_or_C == 'C' ) {
    
  if ( channelType == "pad" ) {
      
  if ( chamberLong =='L' ) {
  
  if ( chamber == 'P' ) {

  if ( radius == 0 ) {
      
  if ( layer == 3 ) {
  if ( (vmmid == 1 && vmmchn == 63) || (vmmid == 2 && vmmchn == 46) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius0[j][i];
}
  if ( vmmid == 2 && (vmmchn == 35 || vmmchn == 45) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius0[j][i];
}
  if ( vmmid == 2 && (vmmchn == 37 || vmmchn == 43) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius0[j][i];
}
  if ( vmmid == 2 && (vmmchn == 38 || vmmchn == 42) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius0[j][i];
}
  if ( vmmid == 2 && (vmmchn == 39 || vmmchn == 41) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius0[j][i];
}
  if ( vmmid == 2 && (vmmchn == 40 || vmmchn == 47) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius0[j][i];
}
  if ( vmmid == 1 && vmmchn < 63 ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius0[j][i];
}     
  if ( vmmid == 2 && vmmchn < 35 ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius0[j][i];
}
} // LAYER 3

  if ( layer == 2 ) {      
  if ( ( vmmid == 1 && (vmmchn > 7 && vmmchn < 64) ) || ( vmmid == 2 && vmmchn < 56 ) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer2_radius0[j][i];
}      
} // LAYER 2
      
} // RADIUS 0

  if ( radius == 1 ) {
      
  if ( layer == 3 ) {
  if ( (vmmid == 1 && (vmmchn > 28 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 40) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius1[j][i];
}      
} // LAYER 3

  if ( layer == 2 ) {
  if ( (vmmid == 1 && (vmmchn > 23 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 35) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer2_radius1[j][i];
}
} // LAYER 2
      
} // RADIUS 1

  if ( radius == 2 ) {

  if ( layer == 3 ) {
  if ( (vmmid == 1 && (vmmchn > 25 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 32) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer3_radius2[j][i];
}  
} // LAYER 3

  if ( layer == 2 ) {
  if ( (vmmid == 1 && (vmmchn > 33 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 40) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_P_layer2_radius2[j][i];
}  
} // LAYER 2

} // RADIUS 2
    
} // PIVOT

  if ( chamber == 'C' ) {

  if ( radius == 0 ) {

  if ( layer == 3 ) {
  if ( (vmmid == 1 && (vmmchn > 2 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 35) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_C_layer3_radius0[j][i];
}
} // LAYER 3

  // CHECK THIS ONE
  if ( layer == 2 ) {
  if ( (vmmid == 1 && (vmmchn > 12 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 45) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_C_layer2_radius0[j][i];
}
} // LAYER 2
      
} // RADIUS 0

  if ( radius == 1 ) {

  if ( layer == 3 ) {
  if ( (vmmid == 1 && (vmmchn > 37 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 30) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_C_layer3_radius1[j][i];
}
} // LAYER 3

  if ( layer == 2 ) {
  if ( (vmmid == 1 && (vmmchn > 33 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 26) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_C_layer2_radius1[j][i];
}
} // LAYER 2     

} // RADIUS 1

  if ( radius == 2 ) {

  if ( layer == 3 ) {
  if ( (vmmid == 1 && (vmmchn > 25 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 18) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_C_layer3_radius2[j][i];
}
} // LAYER 3

  if ( layer == 2 ) {
  if ( (vmmid == 1 && (vmmchn > 47 && vmmchn < 64)) || (vmmid == 2 && vmmchn < 40) ) {
  j= vmmid;
  i= vmmchn;
  chn_c = chn_c + offset_chnc_pad_L_C_layer2_radius2[j][i];
}
} // LAYER 2

} // RADIUS 2
    
} // CONFIRM
  
} // LONG
      
} // PAD
    
} // NSW-C
     
  // end of Sonia;s part of correction of chnc code : SMALL CONFIRM pad layer 2,3  

  //----------- return mapped channel A or C 
  int channel; channel=-2; 
  // -2 means that channel was not set by this program
  // -1 means that channel does not exist in mapping.txt
  if ( endcap_A_or_C == 'A' ) channel = chn;
  if ( endcap_A_or_C == 'C' ) channel = chn_c;
  return(channel);

  // end of method 1
*/

  // Sonia Kabana
  // method 3
  return MappedChn_pad_init[AC][SL][layer0to7][radius][vmmid][vmmchn];
 
} // end of map_pad(






// ------------- function map_wire
//
unsigned int sTGCInput::map_wire(char endcap_A_or_C, char Large_or_Small, char Pivot_or_Confirm, int radius, int layer, int vmmid, int vmmchn) {

  int AC,SL;
  if ( endcap_A_or_C == 'A' ) AC=0;
  if ( endcap_A_or_C == 'C' ) AC=1;

  if( Large_or_Small ==  'S' )  SL=0;
  if( Large_or_Small ==  'L' )  SL=1;


  int layer0to7;
  layer0to7 = layer;


  /*
   // method 1
  channelType = "wire";



  if( Large_or_Small ==  'L' )  chamberLong = 'L';
  if( Large_or_Small ==  'S' )  chamberLong = 'S';

  if( Pivot_or_Confirm == 'P' )  chamber = 'P';
  if( Pivot_or_Confirm == 'C' )  chamber = 'C';


// change from 0-7 layers to 0-3 layers 

   if( chamberLong == 'S' ) {
        if ( layer <= 3 ) chamber = 'C';
        if ( layer >= 4 ) chamber = 'P';
      } else if ( chamberLong == 'L' ) {
        if ( layer <= 3 ) chamber = 'P';
        if ( layer >= 4 ) chamber = 'C';
      }

        int layermap;
        if ( layer < 4 ) layermap = layer;
        else layermap = layer - 4;

        layer = layermap;
        
       


// ------------------- Mapping code -----------------------------------

    
    // Michael Schernau code  
    chn = -1; 
    start = 406; 
    if ( radius == 1 ) start = 512;
    if ( chamberLong == 'L') start = start + 2;
    if ( (layer == 0) || (layer == 3) ) {
      chn = start - vmmid*64 - vmmchn;
      if ( chn < 71 ) chn = chn + 32;
      if ( chamberLong == 'L' ) {
      if ( chn < 9 ) chn = chn + 32;
      }
      else {
      if ( chn < 7 ) chn = chn + 32;
      }
    }
    else {
      chn = 23 - 64 + vmmid*64 + vmmchn;
      if ( chn > 70 ) chn = chn - 32;
      if ( chn > 102 ) chn = chn - 32;
      if ( chamberLong == 'L' ) chn = chn + 2;
    }

          
    
    // ****************************************************************
    // Wheel - A
    // ****************************************************************
    
    

    // ----------------------------------------------------------------
    // ---- Wires
    // ----------------------------------------------------------------

    // Micha Haacke
    
    if (channelType == "wire"){
      if (chamberLong == 'S'){
        if (radius == 0){
          if (layer == 0){
            if (chamber == 'P'){
              chn = chn - 348;
            }
            if (chamber == 'C'){
              chn = -chn + 368;
            }
          }
          if (layer == 1){
            if (chamber == 'P'){
              chn = -chn + 17;
            }
            if (chamber == 'C'){
              chn = chn + 3;
            }
          }
          if (layer == 2){
            if (chamber == 'P'){
              chn = -chn + 17;
            }
            if (chamber == 'C'){
              chn = chn + 4;
            }
          }
          if (layer == 3){
              if (chamber == 'P'){
                chn = chn - 348;
              }
              if (chamber == 'C'){
                chn = -chn + 368;
              }
          }
        }
        if (radius == 1){
          if (layer == 0){
	  if (chamber == 'P'){
              chn = chn -454;
            }
            if (chamber == 'C'){
              chn = -chn + 484;
            }
          }
          if (layer == 1){
            if (chamber == 'P'){
              chn = -chn +17;
}
            if (chamber == 'C'){
  chn = chn + 13;
}
}
          if (layer == 2){
  if (chamber == 'P'){
  chn = -chn + 17;
}
  if (chamber == 'C'){
  chn = chn + 14;
}
}
          if (layer == 3){
  if (chamber == 'P'){
  chn = chn - 454;
}
  if (chamber == 'C'){
  chn = -chn + 484;
}
}
}
        if (radius == 2){
  if (layer == 0){
  if (chamber == 'P'){
  chn = chn - 348;
}
  if (chamber == 'C'){
  chn = -chn + 387;
}
}
  if (layer == 1){
  if (chamber == 'P'){
  chn = -chn +17;
}
  if (chamber == 'C'){
  chn = chn +22;
}
}
  if (layer == 2){
  if (chamber == 'P'){
  chn = -chn +17;
}
  if (chamber == 'C'){
  chn = chn +22;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn = chn - 348;
}
  if (chamber == 'C'){
  chn = -chn +386;
}
}
}
}
      if (chamberLong == 'L'){
  if (radius == 0){
  if (layer == 0){
  if (chamber == 'P'){
  chn = chn - 350;
}
  if (chamber == 'C'){
  chn = - chn + 383;
}
}
  if (layer == 1 || layer == 2){
  if (chamber == 'P'){
  chn = -chn +19;
}
  if (chamber == 'C'){
  chn = chn +14;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn = chn - 350;
}
  if (chamber == 'C'){
  chn = -chn +383;
}
}
}
  if (radius == 1){
  if (layer == 0){
  if (chamber == 'P'){
  chn =  chn - 456;
}
  if (chamber == 'C'){
  chn = -chn + 505;
}
}
  if (layer == 1 || layer == 2){
  if (chamber == 'P'){
  chn = - chn + 19;
}
  if (chamber == 'C'){
  chn = chn + 31;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn = chn - 456;
}
  if (chamber == 'C'){
  chn = -chn + 505;
}
}
}
  if (radius == 2){
  if (layer == 0){
  if (chamber == 'P'){
  chn = chn - 350 ;
}
  if (chamber == 'C'){
  chn = -chn + 408 ;
}
}
  if (layer == 1 || layer == 2){
  if (chamber == 'P'){
  chn = -chn +19;
}
  if (chamber == 'C'){
  chn = chn +40;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn = chn - 350;
}
  if (chamber == 'C'){
  chn = -chn +408;
}
}
}   
}
}
    // ****************************************************************
    // Wheel - C
    // ****************************************************************
    
    chn_c = chn; // this line must be after all chna mapping and before all chnc mapping
    
   

    // ----------------------------------------------------------------
    // ---- Wires
    // ----------------------------------------------------------------

    if (channelType == "wire"){
  if (chamberLong == 'S'){
  if (radius == 0){
  if (layer == 0){
  if (chamber == 'P'){
  chn_c = -chn_c + 20;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 20;
}
}
  if (layer == 1){
  if (chamber == 'P'){
  chn_c = -chn_c + 21;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 20;
}
}
  if (layer == 2){
  if (chamber == 'P'){
  chn_c = -chn_c + 20;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 21;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn_c = -chn_c + 20;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 20;
}
}
}
  if (radius == 1){
  if (layer == 0){
  if (chamber == 'P'){
  chn_c = -chn_c +30;
}
  if (chamber == 'C'){
  chn_c = -chn_c +30;
}
}
  if (layer == 1){
  if (chamber == 'P'){
  chn_c = -chn_c +31;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 30;
}
}
  if (layer == 2){
  if (chamber == 'P'){
  chn_c = -chn_c +30;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 31;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn_c = -chn_c +30;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 30;
}
}
}
  if (radius == 2){
  if (layer == 0){
  if (chamber == 'P'){
  chn_c = -chn_c + 38;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 39;
}
}
  if (layer == 1){
  if (chamber == 'P'){
  chn_c = -chn_c +39;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 39;
}
}
  if (layer == 2){
  if (chamber == 'P'){
  chn_c = -chn_c +39;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 39;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn_c = -chn_c +39;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 38;
}
}
}
}
  if (chamberLong == 'L'){  
  if (radius == 0){
  if (layer == 0){
  if (chamber == 'P'){
  chn_c = -chn_c + 33;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 33;
}
}
  if (layer == 1 || layer == 2){
  if (chamber == 'P'){
  chn_c = -chn_c + 33;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 33;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn_c = -chn_c + 33;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 33;
}
}
}
  if (radius == 1){
  if (layer == 0){
  if (chamber == 'P'){
  chn_c = -chn_c +49;
}
  if (chamber == 'C'){
  chn_c = -chn_c +49 ;
}
}
  if (layer == 1 ){
  if (chamber == 'P'){
  chn_c = -chn_c + 50;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 50;
}
}

  if (layer == 2){
  if (chamber == 'P'){
  chn_c = -chn_c + 50;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 50;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn_c = -chn_c + 49;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 49;
}
}
}
  if (radius == 2){
  if (layer == 0){
  if (chamber == 'P'){
  chn_c = -chn_c + 58;
}
  if (chamber == 'C'){
  chn_c = - chn_c + 58;
}
}
  if (layer == 1){
  if (chamber == 'P'){
  chn_c = -chn_c +59;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 59;
}
}
  if (layer == 2){
  if (chamber == 'P'){
  chn_c = -chn_c +59;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 59 ;
}
}
  if (layer == 3){
  if (chamber == 'P'){
  chn_c = -chn_c +58;
}
  if (chamber == 'C'){
  chn_c = -chn_c + 58;
}
}
}
}
}

    

    //----------- return mapped channel A or C 
    int channel; channel=-2; 
    // -2 means that channel was not set by this program
    // -1 means that channel does not exist in mapping.txt
    if ( endcap_A_or_C == 'A' ) channel = chn;
    if ( endcap_A_or_C == 'C' ) channel = chn_c;
    return(channel);

    // end method 1
*/


  // Sonia Kabana
  // method 3
    return MappedChn_wire_init[AC][SL][layer0to7][radius][vmmid][vmmchn];
  
} // end of map_wire

void sTGCInput::set_offsets_chna_pad_layer0()
{


  //  std::cout << "--- set_offsets_chna_pad_layer0 " <<std::endl;




  //------ Create the offsets for pad layer 0 chna mapping

  // j is the vmmid
  // i s  the vmmchn
  int i,j;

  par=0;
  j=1; 
  for (i=36; i<64; i++) {
    offset_pad_S_P_layer0_radius0[j][i]= - 305 + par;
    offset_pad_S_C_layer0_radius0[j][i]= - 305 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=0; i<40; i++) {
    offset_pad_S_P_layer0_radius0[j][i]= - 249 + par;
    offset_pad_S_C_layer0_radius0[j][i]= - 249 + par;
    par +=2;
  }

  par=0;
  j=1; 
  for (i=49; i<64; i++) {
    offset_pad_S_P_layer0_radius1[j][i]= - 398 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=0; i<15; i++) {
    offset_pad_S_P_layer0_radius1[j][i]= - 368 + par;
    par +=2;
  }
    
  par=0;
  j=1; 
  for (i=40; i<64; i++) {
    offset_pad_S_C_layer0_radius1[j][i]= - 407 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=0; i<24; i++) {
    offset_pad_S_C_layer0_radius1[j][i]= - 359 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=9; i<33; i++) {
    offset_pad_S_P_layer0_radius2[j][i]= - 268 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=2; i<41; i++) {
    offset_pad_S_C_layer0_radius2[j][i]= - 275 + par;
    par +=2;
  }

  par=0;
  j=1; 
  for (i=2; i<64; i++) {
    offset_pad_L_P_layer0_radius0[j][i]= - 341 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=0; i<40; i++) {
    offset_pad_L_P_layer0_radius0[j][i]= - 217 + par;
    par +=2;
  }

  par=0;
  j=1; 
  for (i=8; i<64; i++) {
    offset_pad_L_C_layer0_radius0[j][i]= - 335 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=0; i<40; i++) {
    offset_pad_L_C_layer0_radius0[j][i]= - 223 + par;
    par +=2;
  }

  par=0;
  j=1; 
  for (i=38; i<64; i++) {
    offset_pad_L_P_layer0_radius1[j][i]= - 411 + par;
    offset_pad_L_C_layer0_radius1[j][i]= - 411 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=0; i<30; i++) {
    offset_pad_L_P_layer0_radius1[j][i]= - 359 + par;
    offset_pad_L_C_layer0_radius1[j][i]= - 359 + par;
    par +=2;
  }

  par=0;
  j=1; 
  for (i=26; i<64; i++) {
    offset_pad_L_P_layer0_radius2[j][i]=  - 317 + par;
    offset_pad_L_C_layer0_radius2[j][i]=  - 317 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=0; i<22; i++) {
    offset_pad_L_P_layer0_radius2[j][i]=  - 241 + par;
    par +=2;
  }

  par=0;
  j=2; 
  for (i=0; i<14; i++) {
    offset_pad_L_C_layer0_radius2[j][i]=  - 241 + par;
    par +=2;
  }

  // ---------------- end set offsets for chna pad layer 0 -------------------
}





void sTGCInput::set_offsets_chnc_pad_layer3_Small_Confirm()
{
  //std::cout << "--- set_offsets_chnc_pad_layer3_Small_Confirm " <<std::endl;

  //------ Create the offsets for mapping
  // Sonia

  // j is the vmmid
  // i s  the vmmchn
  int i,j;

  //-------- radius 0

  j=1;
  for (i=32; i<64; i++) {
    j5 += 1;

    if ( j5 % 2 == 0 && j5 % 4 == 0 ) offset_chnc_pad_S_C_layer3_radius0[j][i]= +3;
    else if ( j5 % 2 == 0 && j5 % 4 != 0 ) offset_chnc_pad_S_C_layer3_radius0[j][i]= -1;
    else {
      k5 += 1;
      if ( k5 % 2 != 0 ) offset_chnc_pad_S_C_layer3_radius0[j][i]=- 3;
      else offset_chnc_pad_S_C_layer3_radius0[j][i]= + 1;
    }  // if 

  } // for


  j=2;
  for (i=0; i<40; i++) {
    j5 += 1;

    if ( j5 % 2 == 0 && j5 % 4 == 0 ) offset_chnc_pad_S_C_layer3_radius0[j][i]= +3;
    else if ( j5 % 2 == 0 && j5 % 4 != 0 ) offset_chnc_pad_S_C_layer3_radius0[j][i]= -1;
    else {
      k5 += 1;
      if ( k5 % 2 != 0 ) offset_chnc_pad_S_C_layer3_radius0[j][i]=- 3;
      else offset_chnc_pad_S_C_layer3_radius0[j][i]= + 1;
    }  // if 

  } // for


  //-------- radius 1

  j=1;
  i=41;
  offset_chnc_pad_S_C_layer3_radius1[j][i] = -2;

  j=1;
  for (i=43; i<63; i++) {
    j11 += 1;

    if ( j11 % 3 != 0 ) {

      k11 += 1;
      if ( k11 % 2 != 0 ) offset_chnc_pad_S_C_layer3_radius1[j][i] = +2;

      else offset_chnc_pad_S_C_layer3_radius1[j][i] = -2;

    }
  } // for (i=43; i<63; i

  j=2;
  i=21;
  offset_chnc_pad_S_C_layer3_radius1[j][i] = +2;

  j=2;
  for (i=0; i<20; i++) {
    j12 += 1;
    if ( j12 % 3 != 0 ) {
      k12 += 1;
      if ( k12 % 2 != 0 ) offset_chnc_pad_S_C_layer3_radius1[j][i] = +2;
      else offset_chnc_pad_S_C_layer3_radius1[j][i] = - 2; 
    }
  }


  // layer 3 radius 2
  j=2;
  i=0;
  offset_chnc_pad_S_C_layer3_radius2[j][i] = - 2;
  //      if ( layer == 3 and (vmmid == 2 and vmmchn == 0) ) chn_c = chn_c - 2;


  j=2;
  i=41;
  offset_chnc_pad_S_C_layer3_radius2[j][i] = + 2;
  //if ( layer == 3 and (vmmid == 2 and vmmchn == 41) ) chn_c = chn_c + 2;

  j=2;
  for (i=2; i<40; i++) {
    //trrr
    //if ( layer == 3 and (vmmid == 2 and (vmmchn > 1 and vmmchn < 40)) ) {
    j16 += 1;
    if ( j16 % 3 != 0 ) {
      k16 += 1;
      if ( k16 % 2 != 0 ) offset_chnc_pad_S_C_layer3_radius2[j][i] =  + 2;
      else offset_chnc_pad_S_C_layer3_radius2[j][i] = - 2;
    }
  }

  
  // ---------------- end set offsets for et_offsets_chnc_pad_layer3_Small_Confirm-------------------
}






void sTGCInput::set_offsets_chnc_pad_layer2_Small_Confirm()
{
  // std::cout << "--- set_offsets_chnc_pad_layer2_Small_Confirm " <<std::endl;

  //------ Create the offsets for mapping
  // Sonia

  // j is the vmmid
  // i s  the vmmchn
  int i,j;


  // radius 0
  //-------- layer 2

  j= 1;

  for (i=24; i<64; i++) {
    j6 += 1; 
    if ( j6 % 2 == 0 and j6 % 4 == 0 ) offset_chnc_pad_S_C_layer2_radius0[j][i]= + 3;
    else if ( j6 % 2 == 0 and j6 % 4 != 0 ) offset_chnc_pad_S_C_layer2_radius0[j][i]= - 1;
    else {
      k6 += 1;
      if ( k6 % 2 != 0 ) offset_chnc_pad_S_C_layer2_radius0[j][i]= - 3;
      else offset_chnc_pad_S_C_layer2_radius0[j][i]=  + 1;
    } 
  } // for

  
  j= 2;
  for (i=0; i<32; i++) {

    j6 += 1; 
    if ( j6 % 2 == 0 and j6 % 4 == 0 ) offset_chnc_pad_S_C_layer2_radius0[j][i]= + 3;
    else if ( j6 % 2 == 0 and j6 % 4 != 0 ) offset_chnc_pad_S_C_layer2_radius0[j][i]= - 1;
    else {
      k6 += 1;
      if ( k6 % 2 != 0 ) offset_chnc_pad_S_C_layer2_radius0[j][i]= - 3;
      else offset_chnc_pad_S_C_layer2_radius0[j][i]=  + 1;
    } 
  } // for



  //----------  radius 1
  j=1;
  i=42;
  offset_chnc_pad_S_C_layer2_radius1[j][i] = - 2;

  j=1;
  for (i=44; i<64; i++) {
    j13 += 1;
    if ( j13 % 3 != 0 ) {
      k13 +=1;
      if ( k13 % 2 != 0 ) offset_chnc_pad_S_C_layer2_radius1[j][i] =  + 2;
      else offset_chnc_pad_S_C_layer2_radius1[j][i] = - 2;
    }
  }

  j=2;
  i=22;
  offset_chnc_pad_S_C_layer2_radius1[j][i] = + 2;


  j=2;
  for (i=1; i<21; i++) {
    j14 += 1;
    if ( j14 % 3 != 0 ) {
      k14 += 1;
      if ( k14 % 2 != 0 ) offset_chnc_pad_S_C_layer2_radius1[j][i] = + 2;
      else offset_chnc_pad_S_C_layer2_radius1[j][i] = - 2;
    }
  }

  

  // layer 2 radius 2

  j=1;
  i=0;
  offset_chnc_pad_S_C_layer2_radius2[j][i] = - 2;
  //if ( layer == 2 and (vmmid == 1 and vmmchn == 0) ) chn_c = chn_c - 2;
      
  j=1;
  i=41;
  offset_chnc_pad_S_C_layer2_radius2[j][i] = + 2;
  //if ( layer == 2 and (vmmid == 1 and vmmchn == 41) ) chn_c = chn_c + 2;

  j=1;
  for (i=2; i<40; i++) {
    // if ( layer == 2 and (vmmid == 1 and (vmmchn > 1 and vmmchn < 40)) ) {
    j17 += 1;
    if ( j17 % 3 != 0 ) {
      k17 += 1;
      if ( k17 % 2 != 0 ) offset_chnc_pad_S_C_layer2_radius2[j][i] = + 2;
      else offset_chnc_pad_S_C_layer2_radius2[j][i] = - 2;
    }
  }


  // ---------------- end set offsets for et_offsets_chnc_pad_layer2_Small_Confirm-------------------
}





void sTGCInput::set_offsets_chnc_pad_layer3_Large_Pivot()
{
  // std::cout << "--- set_offsets_chnc_pad_layer3_Large_Pivot " <<std::endl;

  int i,j;
  j=1;
  i = 63;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = -4; 
  j=2;
  i = 46;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = -4;
  j=2;
  i = 35;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = -2;
  i = 45;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = -2;
  i = 37;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = +2;
  i = 43;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = +2;
  i = 38;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = +4;
  i = 42;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = +4;
  i = 39;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = +6;
  i = 41;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = +6;
  i = 40;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = -6;
  i = 47;
  offset_chnc_pad_L_P_layer3_radius0[j][i] = -6;
  
  j=1;
  for ( i = 0; i < 63; i++) {
    j19 += 1;
    if ( j19 % 7 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = - 6;
    else {
      k19 += 1;
      if ( k19 % 3 == 0 && k19 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = + 6;
      else if ( k19 % 3 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = 0;
      else {
	l1 += 1;
	if ( (l1+1) % 4 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = + 2;
	else if ( l1 % 4 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = + 4;
	else if ( l1 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = - 2;
	else offset_chnc_pad_L_P_layer3_radius0[j][i] = - 4;
      }
    }      
  }
  
  j=2;
  for ( i = 0; i < 35; i++) {
    j20 += 1;
    if ( j20 % 7 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = -4;
    else {
      k20 += 1;
      if ( k20 % 3 == 0 && k20 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = - 6;
      else if ( k20 % 3 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] =  +2;
      else {
        l2 += 1;
        if ( (l2+1) % 4 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] =  + 4;
        else if ( l2 % 4 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] =  + 6;
        else if ( l2 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius0[j][i] = 0;
        else offset_chnc_pad_L_P_layer3_radius0[j][i] =   -2;
      }
    }
  }

  j=1;
  for ( i = 29; i < 64; i++) {
    j22 += 1;
    if ( j22 % 5 == 0 ) offset_chnc_pad_L_P_layer3_radius1[j][i] = + 4;
    else {
      k22 += 1;
      if ( k22 % 4 == 0 ) offset_chnc_pad_L_P_layer3_radius1[j][i] = + 2;
      else if ( k22 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius1[j][i] = - 2;
      else {
	l4 += 1;
	if ( l4 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius1[j][i] = chn_c;
	else offset_chnc_pad_L_P_layer3_radius1[j][i] = - 4;
      }
    }
  }

  j=2;
  for ( i = 0; i < 40; i++) {
    j22 += 1;
    if ( j22 % 5 == 0 ) offset_chnc_pad_L_P_layer3_radius1[j][i] = + 4;
    else {
      k22 += 1;
      if ( k22 % 4 == 0 ) offset_chnc_pad_L_P_layer3_radius1[j][i] = + 2;
      else if ( k22 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius1[j][i] = - 2;
      else {
	l4 += 1;
	if ( l4 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius1[j][i] = chn_c;
	else offset_chnc_pad_L_P_layer3_radius1[j][i] = - 4;
      }
    }
  }

  j=1;
  for ( i = 26; i < 64; i++) {
    j24 += 1;
    if ( j24 % 5 == 0 ) offset_chnc_pad_L_P_layer3_radius2[j][i] = + 4;
    else {
      k24 += 1;
      if ( k24 % 4 == 0 ) offset_chnc_pad_L_P_layer3_radius2[j][i] = + 2;
      else if ( k24 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius2[j][i] = - 2;
      else {
	l6 += 1;
	if ( l6 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius2[j][i] = + 0;
	else offset_chnc_pad_L_P_layer3_radius2[j][i] = - 4;
      }
    }
  }

  j=2;
  for ( i = 0; i < 32; i++) {
    j24 += 1;
    if ( j24 % 5 == 0 ) offset_chnc_pad_L_P_layer3_radius2[j][i] = + 4;
    else {
      k24 += 1;
      if ( k24 % 4 == 0 ) offset_chnc_pad_L_P_layer3_radius2[j][i] = + 2;
      else if ( k24 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius2[j][i] = - 2;
      else {
	l6 += 1;
	if ( l6 % 2 == 0 ) offset_chnc_pad_L_P_layer3_radius2[j][i] = + 0;
	else offset_chnc_pad_L_P_layer3_radius2[j][i] = - 4;
      }
    }
  }

} 

void sTGCInput::set_offsets_chnc_pad_layer2_Large_Pivot()
{

  int i,j;
  
  j=1;
  for ( i = 8; i < 64; i++) {
    j21 += 1;
    if ( j21 % 7 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = + 6;
    else {
      k21 += 1;
      if ( k21 % 3 == 0 && k21 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = + 4;
      else if ( k21 % 3 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = - 2;
      else {
	l3 += 1;
	if ( (l3+1) % 4 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = + 0;
	else if ( l3 % 4 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = + 2;
	else if ( l3 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = - 4;
	else offset_chnc_pad_L_P_layer2_radius0[j][i] = - 6;
      }
    }
  }

  j=2;
  for ( i = 0; i < 56; i++) {
    j21 += 1;
    if ( j21 % 7 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = + 6;
    else {
      k21 += 1;
      if ( k21 % 3 == 0 && k21 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = + 4;
      else if ( k21 % 3 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = - 2;
      else {
	l3 += 1;
	if ( (l3+1) % 4 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = + 0;
	else if ( l3 % 4 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = + 2;
	else if ( l3 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius0[j][i] = - 4;
	else offset_chnc_pad_L_P_layer2_radius0[j][i] = - 6;
      }
    }
  }


  j=1;
  for ( i = 24; i < 64; i++) {
    j23 += 1;
    if ( j23 % 5 == 0 ) offset_chnc_pad_L_P_layer2_radius1[j][i] = + 4;
    else {
      k23 += 1;
      if ( k23 % 4 == 0 ) offset_chnc_pad_L_P_layer2_radius1[j][i] = + 2;
      else if ( k23 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius1[j][i] = - 2;
      else {
	l5 += 1;
	if ( l5 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius1[j][i] = + 0;
	else offset_chnc_pad_L_P_layer2_radius1[j][i] = - 4;
      }
    }
  }
  
  j=2;
  for ( i = 0; i < 35; i++) {
    j23 += 1;
    if ( j23 % 5 == 0 ) offset_chnc_pad_L_P_layer2_radius1[j][i] = + 4;
    else {
      k23 += 1;
      if ( k23 % 4 == 0 ) offset_chnc_pad_L_P_layer2_radius1[j][i] = + 2;
      else if ( k23 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius1[j][i] = - 2;
      else {
	l5 += 1;
	if ( l5 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius1[j][i] = + 0;
	else offset_chnc_pad_L_P_layer2_radius1[j][i] = - 4;
      }
    }
  }

  j=1;
  for ( i = 34; i < 64; i++) {
    j25 += 1;
    if ( j25 % 5 == 0 ) offset_chnc_pad_L_P_layer2_radius2[j][i] = + 4;
    else {
      k25 += 1;
      if ( k25 % 4 == 0 ) offset_chnc_pad_L_P_layer2_radius2[j][i] = + 2;
      else if ( k25 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius2[j][i] = - 2;
      else {
	l7 += 1;
	if ( l7 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius2[j][i] = + 0;
	else offset_chnc_pad_L_P_layer2_radius2[j][i] = - 4;
      }
    }
  }

  j=2;
  for ( i = 0; i < 40; i++) {
    j25 += 1;
    if ( j25 % 5 == 0 ) offset_chnc_pad_L_P_layer2_radius2[j][i] = + 4;
    else {
      k25 += 1;
      if ( k25 % 4 == 0 ) offset_chnc_pad_L_P_layer2_radius2[j][i] = + 2;
      else if ( k25 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius2[j][i] = - 2;
      else {
	l7 += 1;
	if ( l7 % 2 == 0 ) offset_chnc_pad_L_P_layer2_radius2[j][i] = + 0;
	else offset_chnc_pad_L_P_layer2_radius2[j][i] = - 4;
      }
    }
  }
  
}

void sTGCInput::set_offsets_chnc_pad_layer3_Large_Confirm()
{

  int i,j;
  
  j=1;
  for ( i = 3; i < 64; i++) {
    j26 += 1;
    if ( j26 % 6 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = + 5;
    else {
      k26 += 1;
      if ( k26 % 5 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = + 3;
      else {
	l8 += 1;
	if ( l8 % 4 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = + 1;
	else {
	  m1 += 1;
	  if ( m1 % 3 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = - 1;
	  else {
	    n1 += 1;
	    if ( n1 % 2 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = - 3;
	    else offset_chnc_pad_L_C_layer3_radius0[j][i] = - 5;
	  }
	}
      }
    }
  }
  
  j=2;
  for ( i = 0; i < 35; i++) {
    j26 += 1;
    if ( j26 % 6 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = + 5;
    else {
      k26 += 1;
      if ( k26 % 5 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = + 3;
      else {
	l8 += 1;
	if ( l8 % 4 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = + 1;
	else {
	  m1 += 1;
	  if ( m1 % 3 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = - 1;
	  else {
	    n1 += 1;
	    if ( n1 % 2 == 0 ) offset_chnc_pad_L_C_layer3_radius0[j][i] = - 3;
	    else offset_chnc_pad_L_C_layer3_radius0[j][i] = - 5;
	  }
	}
      }
    }
  }

  j=1;
  for ( i = 38; i < 64; i++) {
    j28 += 1;
    if ( j28 % 4 == 0 ) offset_chnc_pad_L_C_layer3_radius1[j][i] = + 3;
    else {
      k28 += 1;
      if ( k28 % 3 == 0 ) offset_chnc_pad_L_C_layer3_radius1[j][i] = + 1;
      else {
	l10 += 1;
	if ( l10 % 2 == 0 ) offset_chnc_pad_L_C_layer3_radius1[j][i] = - 1;
	else offset_chnc_pad_L_C_layer3_radius1[j][i] = - 3;
      }
    }   
  }

  j=2;
  for ( i = 0; i < 30; i++) {
    j28 += 1;
    if ( j28 % 4 == 0 ) offset_chnc_pad_L_C_layer3_radius1[j][i] = + 3;
    else {
      k28 += 1;
      if ( k28 % 3 == 0 ) offset_chnc_pad_L_C_layer3_radius1[j][i] = + 1;
      else {
	l10 += 1;
	if ( l10 % 2 == 0 ) offset_chnc_pad_L_C_layer3_radius1[j][i] = - 1;
	else offset_chnc_pad_L_C_layer3_radius1[j][i] = - 3;
      }
    }   
  }

  j=1;
  for ( i = 26; i < 64; i++) {
    j30 += 1;
    if ( j30 % 4 == 0 ) offset_chnc_pad_L_C_layer3_radius2[j][i] = + 3;
    else {
      k30 += 1;
      if ( k30 % 3 == 0 ) offset_chnc_pad_L_C_layer3_radius2[j][i] = + 1;
      else {
	l12 += 1;
	if ( l12 % 2 == 0 ) offset_chnc_pad_L_C_layer3_radius2[j][i] = - 1;
	else offset_chnc_pad_L_C_layer3_radius2[j][i] = - 3;
      }
    }     
  }

  j=2;
  for ( i = 0; i < 18; i++) {
    j30 += 1;
    if ( j30 % 4 == 0 ) offset_chnc_pad_L_C_layer3_radius2[j][i] = + 3;
    else {
      k30 += 1;
      if ( k30 % 3 == 0 ) offset_chnc_pad_L_C_layer3_radius2[j][i] = + 1;
      else {
	l12 += 1;
	if ( l12 % 2 == 0 ) offset_chnc_pad_L_C_layer3_radius2[j][i] = - 1;
	else offset_chnc_pad_L_C_layer3_radius2[j][i] = - 3;
      }
    }     
  }
  
}


void sTGCInput::set_offsets_chnc_pad_layer2_Large_Confirm()
{
  
  int i,j;

  j=1;
  for ( i = 13; i < 64; i++) {
    j27 += 1;
    if ( j27 % 6 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = + 5;
    else {
      k27 += 1;
      if ( k27 % 5 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = + 3;
      else {
	l9 += 1;
	if ( l9 % 4 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = + 1;
	else {
	  m2 += 1;
	  if ( m2 % 3 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = - 1;
	  else {
	    n2 += 1;
	    if ( n2 % 2 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = - 3;
	    else offset_chnc_pad_L_C_layer2_radius0[j][i] = - 5;
	  }
	}
      }
    }
  }

  j=2;
  for ( i = 0; i < 45; i++) {
    j27 += 1;
    if ( j27 % 6 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = + 5;
    else {
      k27 += 1;
      if ( k27 % 5 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = + 3;
      else {
	l9 += 1;
	if ( l9 % 4 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = + 1;
	else {
	  m2 += 1;
	  if ( m2 % 3 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = - 1;
	  else {
	    n2 += 1;
	    if ( n2 % 2 == 0 ) offset_chnc_pad_L_C_layer2_radius0[j][i] = - 3;
	    else offset_chnc_pad_L_C_layer2_radius0[j][i] = - 5;
	  }
	}
      }
    }
  }

  j=1;
  for ( i = 34; i < 64; i++) {
    j29 += 1;
    if ( j29 % 4 == 0 ) offset_chnc_pad_L_C_layer2_radius1[j][i] = + 3;
    else {
      k29 += 1;
      if ( k29 % 3 == 0 ) offset_chnc_pad_L_C_layer2_radius1[j][i] = + 1;
      else {
	l11 += 1;
	if ( l11 % 2 == 0 ) offset_chnc_pad_L_C_layer2_radius1[j][i] = - 1;
	else offset_chnc_pad_L_C_layer2_radius1[j][i] = - 3;
      }
    } 
  }

  j=2;
  for ( i = 0; i < 26; i++) {
    j29 += 1;
    if ( j29 % 4 == 0 ) offset_chnc_pad_L_C_layer2_radius1[j][i] = + 3;
    else {
      k29 += 1;
      if ( k29 % 3 == 0 ) offset_chnc_pad_L_C_layer2_radius1[j][i] = + 1;
      else {
	l11 += 1;
	if ( l11 % 2 == 0 ) offset_chnc_pad_L_C_layer2_radius1[j][i] = - 1;
	else offset_chnc_pad_L_C_layer2_radius1[j][i] = - 3;
      }
    } 
  }

  j=1;
  for ( i = 48; i < 64; i++) {
    j31 += 1;
    if ( j31 % 4 == 0 ) offset_chnc_pad_L_C_layer2_radius2[j][i] = + 3;
    else {
      k31 += 1;
      if ( k31 % 3 == 0 ) offset_chnc_pad_L_C_layer2_radius2[j][i] = + 1;
      else {
	l13 += 1;
	if ( l13 % 2 == 0 ) offset_chnc_pad_L_C_layer2_radius2[j][i] = - 1;
	else offset_chnc_pad_L_C_layer2_radius2[j][i] = - 3;
      }
    }     
  }

  j=2;
  for ( i = 0; i < 40; i++) {
    j31 += 1;
    if ( j31 % 4 == 0 ) offset_chnc_pad_L_C_layer2_radius2[j][i] = + 3;
    else {
      k31 += 1;
      if ( k31 % 3 == 0 ) offset_chnc_pad_L_C_layer2_radius2[j][i] = + 1;
      else {
	l13 += 1;
	if ( l13 % 2 == 0 ) offset_chnc_pad_L_C_layer2_radius2[j][i] = - 1;
	else offset_chnc_pad_L_C_layer2_radius2[j][i] = - 3;
      }
    }     
  }
  
}


void sTGCInput::set_offsets_chnc_pad_layer0()
{


  // std::cout << "--- set_offsets_chnc_pad_layer0 " <<std::endl;



  //------ Create the offsets for pad layer 0 chnc mapping
  // Sonia

  // j is the vmmid
  // i s  the vmmchn
  int i,j;


  par=1.;
  j=1; 
  for (i=36; i<64; i++) {
    if( par == 1 ) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]= +3;
      offset_chnc_pad_S_C_layer0_radius0[j][i]= +3;
    }
    if( par == 2 ) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]= +1;
      offset_chnc_pad_S_C_layer0_radius0[j][i]= +1;
    }
    if( par == 3 ) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]= -1;
      offset_chnc_pad_S_C_layer0_radius0[j][i]= -1;
    }
    if( par == 4 ) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]= -3;
      offset_chnc_pad_S_C_layer0_radius0[j][i]= -3;
    }
    par +=1;
    if( par == 5 ) par = 1;
  }

  par=1;
  j=2; 
  for (i=0; i<40; i++) {
    if( par == 1 ) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]= +3;
      offset_chnc_pad_S_C_layer0_radius0[j][i]= +3;
    }
    if( par == 2 ) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]= +1;
      offset_chnc_pad_S_C_layer0_radius0[j][i]= +1;
    }
    if( par == 3 ) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]= -1;
      offset_chnc_pad_S_C_layer0_radius0[j][i]= -1;
    }
    if( par == 4 ) {
      offset_chnc_pad_S_P_layer0_radius0[j][i]= -3;
      offset_chnc_pad_S_C_layer0_radius0[j][i]= -3;
    }
    par +=1;
    if( par == 5 ) par=1;
  }

  par=1;
  j=1; 
  for (i=49; i<64; i++) {
    if( par == 1 ) offset_chnc_pad_S_P_layer0_radius1[j][i]= +1;
    if( par == 2 ) offset_chnc_pad_S_P_layer0_radius1[j][i]= -1;
    par +=1;
    if( par == 3 ) par=1;
  }

  par = 1;
  j=2; 
  for (i= 0; i<= 14; i++) {
    if( par  == 1 ) offset_chnc_pad_S_P_layer0_radius1[j][i]= -1;
    if( par  == 2 ) offset_chnc_pad_S_P_layer0_radius1[j][i]= +1;
    par  +=1;
    if( par  == 3 ) par =1;
  }

  par = 1;
  j=1; 
  for (i= 40; i< 64; i++) {
    if( par  == 1 ) offset_chnc_pad_S_C_layer0_radius1[j][i]= +2;
    if( par  == 2 ) offset_chnc_pad_S_C_layer0_radius1[j][i]= 0;
    if( par  == 3 ) offset_chnc_pad_S_C_layer0_radius1[j][i]= -2;
    par  +=1;
    if( par  == 4 ) par =1;
  }

  par =1;
  j=2; 
  for (i= 0; i< 24; i++) {
    if( par  == 1 ) offset_chnc_pad_S_C_layer0_radius1[j][i]= +2;
    if( par  == 2 ) offset_chnc_pad_S_C_layer0_radius1[j][i]= 0;
    if( par  == 3 ) offset_chnc_pad_S_C_layer0_radius1[j][i]= -2;
    par  +=1;
    if( par  == 4 ) par =1;
  }

  par =1;
  j=2; 
  for (i= 9; i< 33; i++) {
    if( par  == 1 ) offset_chnc_pad_S_P_layer0_radius2[j][i]= +1;
    if( par  == 2 ) offset_chnc_pad_S_P_layer0_radius2[j][i]= -1;
    par  +=1;
    if( par  == 3 ) par =1;
  }

  par =1;
  j=2; 
  for (i= 2; i< 41; i++) {
    if( par  == 1 ) offset_chnc_pad_S_C_layer0_radius2[j][i]= +2;
    if( par  == 2 ) offset_chnc_pad_S_C_layer0_radius2[j][i]= 0;
    if( par  == 3 ) offset_chnc_pad_S_C_layer0_radius2[j][i]= -2;
    par  +=1;
    if( par  == 4 ) par =1;
  }

  par =1;
  j=1;
  for (i= 2; i< 62; i++) {
    if( par  == 1 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= +5;
    if( par  == 2 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= +3;
    if( par  == 3 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= +1;
    if( par  == 4 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= -1;
    if( par  == 5 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= -3;
    if( par  == 6 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= -5;
    par  +=1;
    if( par  == 7 ) par =1;
  }

  j=1;
  i = 62 ; 
  offset_chnc_pad_L_P_layer0_radius0[j][i]= +5;
  i = 63 ;
  offset_chnc_pad_L_P_layer0_radius0[j][i]= +3;

  j=2; 
  i = 0 ;
  offset_chnc_pad_L_P_layer0_radius0[j][i]= + 1;
  i = 1 ; 
  offset_chnc_pad_L_P_layer0_radius0[j][i]= - 1;
  i = 2 ; 
  offset_chnc_pad_L_P_layer0_radius0[j][i]= - 3;
  i = 3 ; 
  offset_chnc_pad_L_P_layer0_radius0[j][i]= - 5;

  par=1;
  j=2; 
  for (i= 4; i< 40; i++) {
    if( par == 1 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= +5;
    if( par == 2 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= +3;
    if( par == 3 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= +1;
    if( par == 4 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= -1;
    if( par == 5 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= -3;
    if( par == 6 ) offset_chnc_pad_L_P_layer0_radius0[j][i]= -5;
    par +=1;
    if( par == 7 ) par=1;
  }

  par=1;
  j=1; 
  for (i= 8; i< 62; i++) {
    if( par == 1 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= +5;
    if( par == 2 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= +3;
    if( par == 3 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= +1;
    if( par == 4 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= -1;
    if( par == 5 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= -3;
    if( par == 6 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= -5;
    par +=1;
    if( par == 7 ) par=1;
  }


  j=1; 
  i = 62 ; 
  offset_chnc_pad_L_C_layer0_radius0[j][i]= + 5;

  i = 63 ; 
  offset_chnc_pad_L_C_layer0_radius0[j][i]= + 3;

  j=2; 

  i = 0 ; 
  offset_chnc_pad_L_C_layer0_radius0[j][i]= + 1;

  i = 1 ;
  offset_chnc_pad_L_C_layer0_radius0[j][i]= - 1;

  i = 2 ; 
  offset_chnc_pad_L_C_layer0_radius0[j][i]= - 3;

  i = 3 ; 
  offset_chnc_pad_L_C_layer0_radius0[j][i]= - 5;

  par=1;
  j=2; 
  for (i= 4; i< 40; i++) {
    if( par == 1 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= +5;
    if( par == 2 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= +3;
    if( par == 3 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= +1;
    if( par == 4 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= -1;
    if( par == 5 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= -3;
    if( par == 6 ) offset_chnc_pad_L_C_layer0_radius0[j][i]= -5;
    par +=1;
    if( par == 7 ) par=1;
  }

  par=1;
  j=1; 
  for (i= 38; i< 62; i++) {
    if( par == 1 ) offset_chnc_pad_L_P_layer0_radius1[j][i]= +3;
    if( par == 2 ) offset_chnc_pad_L_P_layer0_radius1[j][i]= +1;
    if( par == 3 ) offset_chnc_pad_L_P_layer0_radius1[j][i]= -1;
    if( par == 4 ) offset_chnc_pad_L_P_layer0_radius1[j][i]= -3;
    par +=1;
    if( par == 5 ) par=1;
  }


  j=1; 
  i = 62 ; 
  offset_chnc_pad_L_P_layer0_radius1[j][i]= + 3;
  i = 63 ; 
  offset_chnc_pad_L_P_layer0_radius1[j][i]= + 1;

  j=2; 
  i = 0 ; 
  offset_chnc_pad_L_P_layer0_radius1[j][i]= - 1;
  i = 1 ; 
  offset_chnc_pad_L_P_layer0_radius1[j][i]= - 3;


  par=1;
  j=2; 
  for (i= 2; i< 30; i++) {
    if( par == 1 ) offset_chnc_pad_L_P_layer0_radius1[j][i]= +3;
    if( par == 2 ) offset_chnc_pad_L_P_layer0_radius1[j][i]= +1;
    if( par == 3 ) offset_chnc_pad_L_P_layer0_radius1[j][i]= -1;
    if( par == 4 ) offset_chnc_pad_L_P_layer0_radius1[j][i]= -3;
    par +=1;
    if( par == 5 ) par=1;
  }


  par=1;
  j=1; 
  for (i= 38; i< 62; i++) {
    if( par == 1 ) offset_chnc_pad_L_C_layer0_radius1[j][i]= +3;
    if( par == 2 ) offset_chnc_pad_L_C_layer0_radius1[j][i]= +1;
    if( par == 3 ) offset_chnc_pad_L_C_layer0_radius1[j][i]= -1;
    if( par == 4 ) offset_chnc_pad_L_C_layer0_radius1[j][i]= -3;
    par +=1;
    if( par == 5 ) par=1;
  }

  j=1; 

  i = 62 ; 
  offset_chnc_pad_L_C_layer0_radius1[j][i]= + 3;

  i = 63 ; 
  offset_chnc_pad_L_C_layer0_radius1[j][i]= + 1;

  j=2; 
  i = 0 ; 
  offset_chnc_pad_L_C_layer0_radius1[j][i]= - 1;

  i = 1 ; 
  offset_chnc_pad_L_C_layer0_radius1[j][i]= - 3;


  par=1;
  j=2; 
  for (i= 2; i< 30; i++) {
    if( par == 1 ) offset_chnc_pad_L_C_layer0_radius1[j][i]= +3;
    if( par == 2 ) offset_chnc_pad_L_C_layer0_radius1[j][i]= +1;
    if( par == 3 ) offset_chnc_pad_L_C_layer0_radius1[j][i]= -1;
    if( par == 4 ) offset_chnc_pad_L_C_layer0_radius1[j][i]= -3;
    par +=1;
    if( par == 5 ) par=1;
  }

  par=1;
  j=1; 
  for (i= 26; i< 62; i++) {
    if( par == 1 ) offset_chnc_pad_L_P_layer0_radius2[j][i]= +3;
    if( par == 2 ) offset_chnc_pad_L_P_layer0_radius2[j][i]= +1;
    if( par == 3 ) offset_chnc_pad_L_P_layer0_radius2[j][i]= -1;
    if( par == 4 ) offset_chnc_pad_L_P_layer0_radius2[j][i]= -3;
    par +=1;
    if( par == 5 ) par=1;
  }


  j=1; 

  i = 62 ; 
  offset_chnc_pad_L_P_layer0_radius2[j][i]= + 3;

  i = 63 ; 
  offset_chnc_pad_L_P_layer0_radius2[j][i]= + 1;


  j=2; 

  i = 0 ; 
  offset_chnc_pad_L_P_layer0_radius2[j][i]= - 1;

  i = 1 ; 
  offset_chnc_pad_L_P_layer0_radius2[j][i]= - 3;


  par=1;
  j=2; 
  for (i= 2; i< 22; i++) {
    if( par == 1 ) offset_chnc_pad_L_P_layer0_radius2[j][i]= +3;
    if( par == 2 ) offset_chnc_pad_L_P_layer0_radius2[j][i]= +1;
    if( par == 3 ) offset_chnc_pad_L_P_layer0_radius2[j][i]= -1;
    if( par == 4 ) offset_chnc_pad_L_P_layer0_radius2[j][i]= -3;
    par +=1;
    if( par == 5 ) par=1;
  }

  par=1;
  j=1; 
  for (i= 26; i< 62; i++) {
    if( par == 1 ) offset_chnc_pad_L_C_layer0_radius2[j][i]= +3;
    if( par == 2 ) offset_chnc_pad_L_C_layer0_radius2[j][i]= +1;
    if( par == 3 ) offset_chnc_pad_L_C_layer0_radius2[j][i]= -1;
    if( par == 4 ) offset_chnc_pad_L_C_layer0_radius2[j][i]= -3;
    par +=1;
    if( par == 5 ) par=1;
  }

  j=1; 

  i = 62 ; 
  offset_chnc_pad_L_C_layer0_radius2[j][i]= + 3;

  i = 63 ; 
  offset_chnc_pad_L_C_layer0_radius2[j][i]= + 1;

  j=2; 

  i = 0 ; 
  offset_chnc_pad_L_C_layer0_radius2[j][i]= - 1;

  i = 1 ; 
  offset_chnc_pad_L_C_layer0_radius2[j][i]= - 3;


  par=1;
  j=2; 
  for (i= 2; i< 14; i++) {
    if( par == 1 ) offset_chnc_pad_L_C_layer0_radius2[j][i]= +3;
    if( par == 2 ) offset_chnc_pad_L_C_layer0_radius2[j][i]= +1;
    if( par == 3 ) offset_chnc_pad_L_C_layer0_radius2[j][i]= -1;
    if( par == 4 ) offset_chnc_pad_L_C_layer0_radius2[j][i]= -3;
    par +=1;
    if( par == 5 ) par=1;
  }


  // ---------------- end set offsets for chnc pad layer 0 -------------------

}




void sTGCInput::execute()
{

  std::ifstream infile;   

  mapFileName = "standard_test_mapping_randomized3times_triple_lines";
  //mapFileName = "standard_test_mapping_randomized3times";
  //mapFileName = "mapping";
  std::stringstream ss;
  std::string path = "../util/TextInput/";
  ss << path << mapFileName << ".txt";
  std::string mapFile = ss.str();
    
  // Open Mapping file
  infile.open(mapFile);
  
  // Checks to see if file opened
  if( infile.fail() ) {   
    std::cout << "Error loading map file" << std::endl; 
  }
 
 
  std::string line;
  while (std::getline(infile, line)) {
    std::istringstream iss(line);
    int it0, it1, it2, it3, it4, it5, it6, it7, it8, it9;
    std::string st0, st1, st2, st3, st4, st5, rad;
    if (!(iss >> st0 >> it0 >> it1 >> st1 >> it2 >> it3 >> it4 >> it5 >> it6 >> st2 >> st3 >> it7 >> st4 >> st5 >> it8 >> it9)) { break; }

    //std::cout <<  st0 <<" "<< it0 <<" "<<it1 <<" "<< st1 <<" "<< it2  <<" "<< it3 << " "<<it4 <<" "<< it5 <<" "<< it6 << " "<<st2 << " "<<st3 <<" "<< it7 <<" "<< st4 <<" "<< st5 << " "<<it8 <<" "<< it9<< std::endl;
    

    code = st0;
    chamberLong = code[1];
    rad = code[2];
    chamber = code[3];
    radius = std::stoi( rad ) - 1;
    layer = it1 - 1;
    channelType = st1;
    vmmid = it2 - 1;
    vmmchn = it3;
    chna = it8;
    chnc = it9;

    

    // Check if everything is well defined
    // std::cout << "Chamber: " << code << ", Chamber longitude: " << chamberLong << ", Chamber type: " << chamber << std::endl;
    // std::cout << "Radius: " << radius << ", Layer: " << layer << ", Channel type: " << channelType << std::endl;
    // std::cout << "VMM ID: " << vmmid << ", VMM chn: " << vmmchn  << std::endl;
    // std::cout << "channel A: " << chna << ", channel c: " << chnc  << std::endl;
    // std::cout << "-------------------------- " << std::endl;


    // -----------------------------------------------------------------
if( chamberLong == 'L' ) Large_or_Small = 'L';
if( chamberLong == 'S' ) Large_or_Small = 'S';
if( chamber == 'P' ) Pivot_or_Confirm = 'P';
if( chamber == 'C' ) Pivot_or_Confirm = 'C';
    // -----------------------------------------------------------------
    
      if( channelType == "strip" ) {

// call function that performs the Mapping to get the A channel
endcap_A_or_C = 'A';
chn = map_strip(endcap_A_or_C,  Large_or_Small,  Pivot_or_Confirm, radius,  layer,  vmmid,  vmmchn);

// call function that performs the Mapping to get the C channel
//endcap_A_or_C = 'C';
//chn_c = map_strip(endcap_A_or_C,  Large_or_Small,  Pivot_or_Confirm, radius,  layer,  vmmid,  vmmchn);
//no need to call map_strip again for C mapping,  since chn_c = chn for strips:
chn_c = chn;

}

    // -----------------------------------------------------------------
    
      if( channelType == "pad" ) {

// call function that performs the Mapping to get the A channel
endcap_A_or_C = 'A';
chn = map_pad(endcap_A_or_C,  Large_or_Small,  Pivot_or_Confirm, radius,  layer,  vmmid,  vmmchn);


// call function that performs the Mapping to get the C channel
endcap_A_or_C = 'C';
chn_c = map_pad(endcap_A_or_C,  Large_or_Small,  Pivot_or_Confirm, radius,  layer,  vmmid,  vmmchn);

}

    // -----------------------------------------------------------------
    
      if( channelType == "wire" ) {

	// call function that performs the Mapping to get the A channel
	endcap_A_or_C = 'A';
	chn = map_wire(endcap_A_or_C,  Large_or_Small,  Pivot_or_Confirm, radius,  layer,  vmmid,  vmmchn);

	// call function that performs the Mapping to get the C channel
	endcap_A_or_C = 'C';
	chn_c = map_wire(endcap_A_or_C,  Large_or_Small,  Pivot_or_Confirm, radius,  layer,  vmmid,  vmmchn);

      }


      // ****************************************************************


      // Check for differences for chn
      //if ( chna != chn && ( ( channelType == "strip" ) || channelType == "pad" || ( channelType == wire) ) {
      //if ( chna != chn && (  channelType == "wire"  ) )
      //if (  channelType == "strip"  || channelType == "pad"  )
      //if ( chna != chn && (  channelType == "strip"  || channelType == "pad" ) )
      //if ( chna != chn &&  channelType == "strip"   )
      //if ( chna != chn && ( ( channelType == "wire" ) ) ) {
      //if ( chna != chn && ( ( channelType == "pad" ) ) ) {
      if ( chna != chn  ) {
	std::cout << "--------- printout after map chn ----------------- " << std::endl;
	std::cout << "Chamber: " << code << ", Chamber longitude: " << chamberLong << ", Chamber type: " << chamber << std::endl;
	std::cout << "Radius: " << radius << ", Layer: " << layer << ", Channel type: " << channelType << std::endl;
	std::cout << "VMM ID: " << vmmid << ", VMM chn: " << vmmchn  << std::endl;
	std::cout << "channel A chna: " << chna << ", channel chn: " << chn  <<  ", channel diff: " << chn-chna  << std::endl;
	//std::cout << "channel C chnc: " << chnc << ", channel chn_c: " << chn_c  <<  ", channel diff: " << chn_c-chnc << std::endl;
	std::cout << "-------------------------- " << std::endl;
      }



      if ( chnc != chn_c  ) {
	std::cout << "--------- printout after map chn_c----------------- " << std::endl;
	std::cout << "Chamber: " << code << ", Chamber longitude: " << chamberLong << ", Chamber type: " << chamber << std::endl;
	std::cout << "Radius: " << radius << ", Layer: " << layer << ", Channel type: " << channelType << std::endl;
	std::cout << "VMM ID: " << vmmid << ", VMM chn: " << vmmchn  << std::endl;
	//std::cout << "channel A chna: " << chna << ", channel chn: " << chn  <<  ", channel diff: " << chn-chna  << std::endl;
	std::cout << "channel C chnc: " << chnc << ", channel chn_c: " << chn_c  <<  ", channel diff: " << chn_c-chnc << std::endl;
	std::cout << "-------------------------- " << std::endl;
      }
      
      // ---------------------------------------------
       
  }
  
  // Close Mapping file 
  infile.close();
  infile.clear();

  std::cout << "--- End of event loop " <<std::endl;   
}
