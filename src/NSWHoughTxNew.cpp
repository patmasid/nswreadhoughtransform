#include "NSWRead/NSWHoughTxNew.h"

NSWHoughTxNew::NSWHoughTxNew(double num_strips_percell, int firstStripN, int lastStripN, int firstLayerN, int lastLayerN){
  // true road width include 50% more on both sides for total overlap of roads
  //    so half the Nstrip_perCell to compensate
  
  verbose = false;

  bool setrange = SetStripRange( num_strips_percell, firstStripN, lastStripN, firstLayerN, lastLayerN );
  if ( !setrange ) std::cout << "WTF: set strip range failed: Do not continue until first setting SetStripRangeCorrectly " << std::endl;

}

bool NSWHoughTxNew::SetStripRange( double num_strips_percell, int firstStripN, int lastStripN, int firstLayerN, int lastLayerN ) {

  if ( firstLayerN == lastLayerN ||
       firstLayerN < 0 ||
       lastLayerN  >=8 ) {
    std::cout << "WTF: first Layer N and last Layer N set wrong. Aborting" 
              << firstLayerN << "-" << lastLayerN << std::endl;
    return false;
  }

  if ( firstLayerN < lastLayerN ) {
    firstLayerNum = firstLayerN;
    lastLayerNum  = lastLayerN;
  }
  else {
    firstLayerNum = lastLayerN;
    lastLayerNum  = firstLayerN;
  }

  NLayers = lastLayerNum - firstLayerNum + 1;

  if ( firstStripN == lastStripN ) {
    lastStripN++;
    std::cout << "Warning: number of strips must be over 1, adding 1 to last strip " 
	      << firstStripN << "-" << lastStripN << std::endl;
    return false;
  }
  
  if ( firstStripN < lastStripN ) {
    firstStripNum = firstStripN;
    lastStripNum  = lastStripN;
  }
  else {
    firstStripNum = lastStripN;
    lastStripNum  = firstStripN;
  }

  // n strip = diff+1
  NChannelsInLayer = lastStripNum - firstStripNum + 1;

  Nstrips_percell = num_strips_percell/2.0; 
  Ncells = ((int) NChannelsInLayer/Nstrips_percell);
  
  xL4_bins_arr.resize(Ncells+1);
  xL7_bins_arr.resize(Ncells+1);

  firstStripPos = NSWChannelID::GetStripPosition(firstStripNum);
  lastStripPos  = NSWChannelID::GetStripPosition(lastStripNum);
  stripWidth    = (lastStripPos-firstStripPos)/(lastStripNum-firstStripNum);
  
  return true;
}

NSWHoughTxNew::~NSWHoughTxNew(){
}

void NSWHoughTxNew::Clear(){

  // large vector matrix of NCell x NCell x NLayer all empty of clusters
  HTCell_Clusters.resize(Ncells);
  HTCell_NLayer_WClusters.resize(Ncells);

  for ( int icell=0; icell<HTCell_Clusters.size(); icell++ ){
    HTCell_Clusters.at(icell).resize(Ncells);
    HTCell_NLayer_WClusters.at(icell).resize(Ncells);
    for ( int jcell=0; jcell< HTCell_Clusters.at(icell).size(); jcell++ ) { 
      HTCell_Clusters.at(icell).at(jcell).resize(lastLayerNum+1); // make array large enough to have all layers
      HTCell_NLayer_WClusters.at(icell).at(jcell) = 0; // no clusters in any XY cell;
      for ( int ilayer=0; ilayer<HTCell_Clusters.at(icell).at(jcell).size(); ilayer++ ) {
	HTCell_Clusters.at(icell).at(jcell).at(ilayer).resize(0);
      }
    }
  }

  // no accepted cells
  acceptedHTCell_index.resize(0);
  acceptedHTCell_clusters.resize(0);

}

bool NSWHoughTxNew::isChnBad(int layer, int chnl_num){
  if(v_bad_chnls[0][layer].size() != 0){
    if(std::find(v_bad_chnls[0][layer].begin(), v_bad_chnls[0][layer].end(), chnl_num) != v_bad_chnls[0][layer].end())
      return true;
    else
      return false;
  }
  else
    return false;
}

void NSWHoughTxNew::SetFirstStripPosition( double pos ) {
  firstStripPos = pos;
}

void NSWHoughTxNew::define_HT_var(){

  if ( verbose ) {
    std::cout << "strip range pos (mm) " << firstStripPos << " " << lastStripPos << " " << stripWidth << std::endl;
    std::cout << "strip range num      " << firstStripNum << " " << lastStripNum << " " << Nstrips_percell << " " << Ncells << std::endl;
  }

  double rem = NChannelsInLayer/Nstrips_percell - floor(NChannelsInLayer/Nstrips_percell);
  rem = rem*Nstrips_percell;

  double halfStripOffset = stripWidth/2.0;

  for(int iedge=0; iedge<Ncells+1; iedge++){
    if(iedge == 0){
      xL4_bins_arr[iedge] = iedge*stripWidth+firstStripPos - halfStripOffset; // start from edge of 0th strip not center
      xL7_bins_arr[iedge] = iedge*stripWidth+firstStripPos - halfStripOffset;
    }
    else if(iedge == 1){  
      xL4_bins_arr[iedge] = iedge*stripWidth*Nstrips_percell+firstStripPos - halfStripOffset; // start from edge of 0th strip not center
      xL7_bins_arr[iedge] = iedge*stripWidth*Nstrips_percell+firstStripPos - halfStripOffset;
      if ( firstLayerNum%2 == 1 ) xL4_bins_arr[iedge] -= halfStripOffset;
      if ( lastLayerNum%2  == 1 ) xL7_bins_arr[iedge] -= halfStripOffset;
    }
    else if(iedge == Ncells){
      xL4_bins_arr[iedge] = xL4_bins_arr[iedge-1]+(stripWidth*rem);
      xL7_bins_arr[iedge] = xL7_bins_arr[iedge-1]+(stripWidth*rem);
    }
    else{
      xL4_bins_arr[iedge] = xL4_bins_arr[iedge-1]+(stripWidth*Nstrips_percell);
      xL7_bins_arr[iedge] = xL7_bins_arr[iedge-1]+(stripWidth*Nstrips_percell);
    }
  }

  if ( verbose ) std::cout << "end edge: " << xL4_bins_arr[Ncells] << " " << xL7_bins_arr[Ncells] << std::endl;

}

int NSWHoughTxNew::OverlappingCell( int layer, double pos, bool lowest ) {
  
  if ( layer != firstLayerNum && layer != lastLayerNum ) {
    std::cout << "WTF: Error only query on layer " << firstLayerNum << "/" << lastLayerNum << std::endl;
  }

  //-----------------------------------------//
  // calculate the inverse of define_HT_var  //
  //-----------------------------------------//

  double cell_float = (pos - firstStripPos + stripWidth/2.0);
  if (      layer == firstLayerNum && firstLayerNum%2 == 1 ) cell_float += stripWidth/2.0;
  else if ( layer == lastLayerNum  && lastLayerNum%2  == 1 ) cell_float += stripWidth/2.0;
  cell_float = cell_float / stripWidth / Nstrips_percell;

  int cell = (int) cell_float;

  double cell_half = cell_float - floor(cell_float);

  if ( verbose ) std::cout << "cell info " << cell_float << " " << pos << std::endl;

  //----------------------------------------------------//
  //  Due to the overlapping nature of roads
  //  if you are in the bottom half of a ith cell
  //  you are overlapping with the road of ith-1 cell 
  //----------------------------------------------------//

  //if ( firstStripPos < lastStripPos ) {
    if      ( cell_half<0.5 && lowest )  cell -= 1;
    else if ( cell_half>0.5 && !lowest ) cell += 1;
    //}
    //else { // higher is lower
  //   if      ( cell_half>0.5 && lowest )  cell -= 1;
  //  else if ( cell_half<0.5 && !lowest ) cell += 1;
  //}

  if ( cell < 0 ) cell = 0;
  else if ( cell >= Ncells ) cell = Ncells-1;

  if ( verbose ) std::cout << "cluster pos (mm) " << pos << " " << firstStripPos << " " << Nstrips_percell << " " << cell << std::endl;

  return cell;

}

double NSWHoughTxNew::GetMinimumSlopeDiffTarget(int iFirstL, int iLastL, double clusterPos, double clusterZ, int clusterLayer) {

  double cellLowEdgeFirstL = xL4_bins_arr[iFirstL] - Nstrips_percell*stripWidth/2.0;
  double cellLowEdgeLastL  = xL7_bins_arr[iLastL]  - Nstrips_percell*stripWidth/2.0;

  double cellHighEdgeFirstL = xL4_bins_arr[iFirstL+1] + Nstrips_percell*stripWidth/2.0;
  double cellHighEdgeLastL  = xL7_bins_arr[iLastL+1]  + Nstrips_percell*stripWidth/2.0;

  double ZLayerFirst = NSWCluster::GetZPosition(firstLayerNum, true, 0);
  double ZLayerLast  = NSWCluster::GetZPosition(lastLayerNum,  true, 0);

  double slope = 0.0;

  if ( clusterLayer == firstLayerNum ) { // return minimum slope from cluster to last layer cell
    double slopeLowEdge  = ( cellLowEdgeLastL  - clusterPos ) / ( ZLayerLast - clusterZ );
    double slopeHighEdge = ( cellHighEdgeLastL - clusterPos ) / ( ZLayerLast - clusterZ );
    
    if ( fabs( target_slope - slopeLowEdge ) < 
	 fabs( target_slope - slopeHighEdge) ) slope = slopeLowEdge;
    else slope = slopeHighEdge;
  }
  else if ( clusterLayer == lastLayerNum ) { // return minimum slope of the cluster to first layer cell
    double slopeLowEdge  = ( cellLowEdgeFirstL  - clusterPos ) / ( ZLayerFirst - clusterZ );
    double slopeHighEdge = ( cellHighEdgeFirstL - clusterPos ) / ( ZLayerFirst - clusterZ );

    if ( fabs( target_slope - slopeLowEdge ) <
         fabs( target_slope - slopeHighEdge) ) slope = slopeLowEdge;
    else slope = slopeHighEdge;
  }
  else { // if a middle layer return the minimum slope of the road itself
    // an X shaped crossing lines from high to low or low to high edges between first and last layer 
    //    must be the highest and lowest slopes available in the road
    double slopeLowEdge  = ( cellLowEdgeLastL  - cellHighEdgeFirstL ) / ( ZLayerLast - ZLayerFirst );
    double slopeHighEdge = ( cellHighEdgeLastL - cellLowEdgeFirstL  ) / ( ZLayerLast - ZLayerFirst );

    if ( fabs( target_slope - slopeLowEdge ) <
         fabs( target_slope - slopeHighEdge) ) slope = slopeLowEdge;
    else slope = slopeHighEdge;
  }

  return slope;

}

double NSWHoughTxNew::GetPredictedEdge(int iFirstL, int iLastL, double clusterZ, bool low){

  double cellEdgeFirstL;
  double cellEdgeLastL;

  if ( low ) {
    cellEdgeFirstL = xL4_bins_arr[iFirstL] - Nstrips_percell*stripWidth/2.0;
    cellEdgeLastL  = xL7_bins_arr[iLastL]  - Nstrips_percell*stripWidth/2.0;
  }
  else {
    cellEdgeFirstL = xL4_bins_arr[iFirstL+1] + Nstrips_percell*stripWidth/2.0;
    cellEdgeLastL  = xL7_bins_arr[iLastL+1]  + Nstrips_percell*stripWidth/2.0;
  }

  double ZLayerFirst = NSWCluster::GetZPosition(firstLayerNum, true, 0);
  double ZLayerLast  = NSWCluster::GetZPosition(lastLayerNum,  true, 0);

  double predictedEdge  = (cellEdgeLastL  - cellEdgeFirstL ) / (ZLayerLast - ZLayerFirst) * (clusterZ - ZLayerFirst) + cellEdgeFirstL;

  return predictedEdge;

}

void NSWHoughTxNew::SetTargetAngle ( double target_angle, double angle_range ) {
  if ( stripWidth > 0 ) {
    target_seg_angle = target_angle;
  }
  else { // if last strip pos < first strip pos then angle should also be flipped
    target_seg_angle = -target_angle;
  }

  seg_angle_range  = angle_range;

  if ( stripWidth > 0 ) {
    low_target_ang  = (target_seg_angle-seg_angle_range)/180.0*TMath::Pi();
    high_target_ang = (target_seg_angle+seg_angle_range)/180.0*TMath::Pi();
  }
  else { // if last strip pos < first strip pos then positive angle is actually decreasing strip count
    low_target_ang  = (target_seg_angle+seg_angle_range)/180.0*TMath::Pi();
    high_target_ang = (target_seg_angle-seg_angle_range)/180.0*TMath::Pi();
  }

  target_slope      = TMath::Tan(target_seg_angle);
  low_target_slope  = TMath::Tan(low_target_ang);
  high_target_slope = TMath::Tan(high_target_ang);

  return;
}

void NSWHoughTxNew::SetAcceptNClusterSegment ( int ncluster ) {
  AcceptSegment_NCluster = ncluster;
  return;
}

//------------------------------------------------------//
//   Fill HT Cells that correspond to clusters
//------------------------------------------------------//

void NSWHoughTxNew::FillHTCell(NSWCluster * cl, int cl_layer, int clusterType){

  //--------------------//
  //  Get cluster info
  //--------------------//

  double clusterPosition=0;
  if ( clusterType == 0 ) {
    clusterPosition = cl->GetCOG();
  }
  else if ( clusterType == 1 ) {
    clusterPosition = cl->GetGaussianCentroid();
  }
  else if ( clusterType == 2 ) {
    clusterPosition = cl->GetMeanCaruana();
  }
  else if( clusterType == 3 ){
    clusterPosition = cl->GetParabolaMean();
  }
  else {
    std::cout << "WTF: Cluster type not recognnized" << std::endl;
    return;
  }

  double clusterZ = cl->GetZPosition();
  
  double ZLayerFirst = NSWCluster::GetZPosition(firstLayerNum, true, 0);
  double ZLayerLast  = NSWCluster::GetZPosition(lastLayerNum,  true, 0);

  //--------------------------------------------//
  //  More efficient to loop over closer layer
  //  ex: if cell layers are 4 and 7
  //  layer 4+5 are closer to layer 4 cells
  //  layer 6+7 are closer to layer 7 cells
  //--------------------------------------------//

  // if cluster layer is closer to first layer
  // xlayer will be first looped over than y layer
  int xLayer     = firstLayerNum;
  double xLayerZ = ZLayerFirst;
  int yLayer     = lastLayerNum;
  double yLayerZ = ZLayerLast;

  // if cluster layer is closer to last layer than first layer
  if ( abs(cl_layer-firstLayerNum) > abs(lastLayerNum-cl_layer) ) {
    xLayer  = lastLayerNum;
    xLayerZ = ZLayerLast;
    yLayer  = firstLayerNum;
    yLayerZ = ZLayerFirst;
  }

  double predictedClusterXLayerLocation_LowEdge  = (xLayerZ-clusterZ) * low_target_slope  + clusterPosition;
  double predictedClusterXLayerLocation_HighEdge = (xLayerZ-clusterZ) * high_target_slope + clusterPosition;
 
  // ensure correct order in layer y.  order can swap depending on slopes

  if ( firstStripPos < lastStripPos ) {
    if ( predictedClusterXLayerLocation_LowEdge > 
	 predictedClusterXLayerLocation_HighEdge ) {
      double tmp = predictedClusterXLayerLocation_LowEdge;
      predictedClusterXLayerLocation_LowEdge = predictedClusterXLayerLocation_HighEdge;
      predictedClusterXLayerLocation_HighEdge = tmp;
    }
  }
  else {
    if ( predictedClusterXLayerLocation_LowEdge < 
	 predictedClusterXLayerLocation_HighEdge ) {
      double tmp = predictedClusterXLayerLocation_LowEdge;
      predictedClusterXLayerLocation_LowEdge = predictedClusterXLayerLocation_HighEdge;
      predictedClusterXLayerLocation_HighEdge = tmp;
    }
  }


  //------------------------------------------------------------------------------------------//
  //    Get the Cell index of the lowest and highest Hough transform overlapping L4 cell
  //------------------------------------------------------------------------------------------//

  int lowestXCellIndex  = OverlappingCell( xLayer, predictedClusterXLayerLocation_LowEdge,  true  );
  int HighestXCellIndex = OverlappingCell( xLayer, predictedClusterXLayerLocation_HighEdge, false );

  if ( lowestXCellIndex > HighestXCellIndex ) {
    double tmp = HighestXCellIndex;
    HighestXCellIndex = lowestXCellIndex;
    lowestXCellIndex = tmp;
  }

  //----------------------------------------------------//
  //    Loop over possible X cells to find Y cells
  //----------------------------------------------------//

  for ( int ix=lowestXCellIndex; ix <= HighestXCellIndex; ix++ ) {

    //---------------------------------------------------------------//
    // edge of the road is half a cell width wider than the cell 
    //    to ensure 100% overlap of roads
    //---------------------------------------------------------------//

    double cellLowEdgeX  = -Nstrips_percell*stripWidth/2.0;
    double cellHighEdgeX = Nstrips_percell*stripWidth/2.0;

    // Edge of ith cell is [ith] - [ith+1] if you are in the same layer as the cells are
    // this avoids dividing by 0 in calculations
    if ( xLayer == firstLayerNum ) {
      cellLowEdgeX  += xL4_bins_arr[ix];
      cellHighEdgeX += xL4_bins_arr[ix+1];
    }
    else {
      cellLowEdgeX  += xL7_bins_arr[ix];
      cellHighEdgeX += xL7_bins_arr[ix+1];
    }

    if ( verbose ) std::cout << "xedge " << cellLowEdgeX << " " << cellHighEdgeX << " " << ix << " " << xL4_bins_arr[ix-1] << " " << xL4_bins_arr[ix] << std::endl;

    //-------------------------------------------------------------//
    //   Slopes used to find overlapping Y cells
    //   Just use target segment angle slopes for layer 4/7
    //   Calculate slope for layer 5/6 as the line from low/high edge 
    //      of bin through the target
    //-------------------------------------------------------------//

    double lowSlope  = low_target_slope;
    double highSlope = high_target_slope;

    if ( verbose ) std::cout << "target slope " << lowSlope << " " << highSlope << std::endl;

    if ( cl_layer != firstLayerNum && cl_layer != lastLayerNum ) {
      lowSlope  = (cellLowEdgeX  - clusterPosition) / (xLayerZ - clusterZ);
      highSlope = (cellHighEdgeX - clusterPosition) / (xLayerZ - clusterZ);
    }

    if ( verbose ) std::cout << "target slope " << lowSlope << " " << highSlope << " cl_layer " << cl_layer << std::endl;

    //----------------------------------------------------------------------------------------------//
    //   project what the high and low edges correspond to in layer Y
    //----------------------------------------------------------------------------------------------//

    double predictedYLowEdge;
    double predictedYHighEdge;

    // if it's the edge layer use the slope line as guides. // work here
    if ( cl_layer == firstLayerNum && cl_layer == lastLayerNum ) {
      predictedYLowEdge  = (yLayerZ-clusterZ) * low_target_slope  + clusterPosition;
      predictedYHighEdge = (yLayerZ-clusterZ) * high_target_slope + clusterPosition;
    }
    else { // if one of the middle layers
      predictedYLowEdge  = cellLowEdgeX  + lowSlope  * ( yLayerZ - xLayerZ );
      predictedYHighEdge = cellHighEdgeX + highSlope * ( yLayerZ - xLayerZ );
    }
    // ensure correct order in layer y.  order can swap depending on slopes

    if ( firstStripPos < lastStripPos ) {
      if ( predictedYLowEdge > predictedYHighEdge ) {
	double tmp = predictedYLowEdge;
	predictedYLowEdge = predictedYHighEdge;
	predictedYHighEdge = tmp;
      }
    }
    else {
      if ( predictedYLowEdge < predictedYHighEdge ) {
        double tmp = predictedYLowEdge;
        predictedYLowEdge = predictedYHighEdge;
        predictedYHighEdge = tmp;
      }
    }

    //-----------------------------------------------------------------------------//
    //   Get Highest and Lowest overlapping bin for Y
    //-----------------------------------------------------------------------------//

    int lowestYCellIndex  = OverlappingCell( yLayer, predictedYLowEdge,  true  );
    int HighestYCellIndex = OverlappingCell( yLayer, predictedYHighEdge, false );

    if ( lowestYCellIndex > HighestYCellIndex ) {
      double tmp = HighestYCellIndex;
      HighestYCellIndex = lowestYCellIndex;
      lowestYCellIndex = tmp;
    }

    //-----------------------------------------------------------------------//
    //   Place cluster in HT Cell of every overlapping XY cell
    //-----------------------------------------------------------------------//

    if ( verbose ) std::cout << lowestXCellIndex << " " << HighestXCellIndex << " "
	      << lowestYCellIndex << " " << HighestYCellIndex << " " << Ncells << std::endl;

    for(int iy=lowestYCellIndex; iy<=HighestYCellIndex; iy++){
      if ( xLayer == firstLayerNum ) {
	if ( HTCell_Clusters.at(ix).at(iy).at(cl_layer).size() == 0 ) {
	  HTCell_NLayer_WClusters.at(ix).at(iy) += 1;
	  if ( HTCell_NLayer_WClusters.at(ix).at(iy) == AcceptSegment_NCluster ) {
	    acceptedHTCell_index.push_back(std::make_pair(ix,iy));
	  }
	}
	HTCell_Clusters.at(ix).at(iy).at(cl_layer).push_back(cl);

	if ( verbose ) {

	  double predictedLowEdge  = GetPredictedEdge(ix, iy, clusterZ, true);
	  double predictedHighEdge = GetPredictedEdge(ix, iy, clusterZ, false);

	  if ( predictedLowEdge > predictedHighEdge ) {
	    double tmp = predictedLowEdge;
	    predictedLowEdge  = predictedHighEdge;
	    predictedHighEdge = tmp;
	  }

	  if ( (clusterPosition < predictedLowEdge || 
		clusterPosition > predictedHighEdge) &&
	       !(fabs( clusterPosition - predictedLowEdge)  < 0.01 ||
		 fabs( clusterPosition - predictedHighEdge) < 0.01 ) )  {
	    std::cout << "WTF: Cluster outside of road!!!" << std::endl;
	  }

	  if ( ix==lowestXCellIndex && iy==lowestYCellIndex ) {
	    
	    if ( cl_layer != firstLayerNum  && cl_layer != lastLayerNum ) {
	      
	      if ( ix > 0 ) {
		
		double predictedLowEdge  = GetPredictedEdge(ix-1, iy, clusterZ, true);
		double predictedHighEdge = GetPredictedEdge(ix-1, iy, clusterZ, false);
		
		if ( predictedLowEdge > predictedHighEdge ) {
		  double tmp = predictedLowEdge;
		  predictedLowEdge  = predictedHighEdge;
		  predictedHighEdge = tmp;
		}
		
		if ( clusterPosition > predictedLowEdge &&
		     clusterPosition < predictedHighEdge &&
		     fabs( clusterPosition - predictedLowEdge)  > 0.01 &&
		     fabs( clusterPosition - predictedHighEdge) > 0.01 ) {
		  std::cout << "WTF: Cluster within road outside of lower edge " << ix-1 << "," << iy << " "
			    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
		}
	      }
	      if ( iy > 0 ) {
		predictedLowEdge  = GetPredictedEdge(ix, iy-1, clusterZ, true);
		predictedHighEdge = GetPredictedEdge(ix, iy-1, clusterZ, false);
		
		if ( predictedLowEdge > predictedHighEdge ) {
		  double tmp = predictedLowEdge;
		  predictedLowEdge  = predictedHighEdge;
		  predictedHighEdge = tmp;
		}
		
		if ( clusterPosition > predictedLowEdge &&
		     clusterPosition < predictedHighEdge &&
		     fabs( clusterPosition - predictedLowEdge)  > 0.01 &&
		     fabs( clusterPosition - predictedHighEdge) > 0.01 ) {
		  std::cout << "WTF: Cluster within road outside of lower edge " << ix << "," << iy-1 << " "
			    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
		}
	      }

	    }
	    else if ( cl_layer == firstLayerNum ) { // cluster is the first layer
              if ( iy > 0 ) {
                double slope = GetMinimumSlopeDiffTarget(ix, iy-1, clusterPosition, clusterZ, cl_layer);

                if ( slope > low_target_slope && slope < high_target_slope ) {
		  std::cout << "WTF: Cluster within road with correct angle but outside of lower edge " << ix << "," << iy-1 << " "
                            << low_target_slope << " " << slope << " " << high_target_slope << std::endl;
                }
              }
            }
            else { // cluster is in the last layer
              if ( ix > 0 ) {
                double slope = GetMinimumSlopeDiffTarget(ix-1, iy, clusterPosition, clusterZ, cl_layer);

                if ( slope > low_target_slope && slope < high_target_slope ) {
		  std::cout << "WTF: Cluster within road with correct angle but outside of lower edge " << ix-1 << "," << iy << " "
                            << low_target_slope << " " << slope << " " << high_target_slope << std::endl;
                }
              }
            }

	  } // end of lowest ix,iy
	  else if ( ix==HighestXCellIndex && iy==HighestYCellIndex ) {

	    if ( cl_layer != firstLayerNum  && cl_layer != lastLayerNum ) {
	      
	      if ( ix < Ncells ) {
		double predictedLowEdge  = GetPredictedEdge(ix+1, iy, clusterZ, true);
		double predictedHighEdge = GetPredictedEdge(ix+1, iy, clusterZ, false);
		
		if ( predictedLowEdge > predictedHighEdge ) {
		  double tmp = predictedLowEdge;
		  predictedLowEdge  = predictedHighEdge;
		  predictedHighEdge = tmp;
		}
		
		if ( clusterPosition > predictedLowEdge &&
		     clusterPosition < predictedHighEdge &&
		     fabs( clusterPosition - predictedLowEdge)  > 0.01 &&
		     fabs( clusterPosition - predictedHighEdge) > 0.01 ) {
		  std::cout << "WTF: Cluster within road outside of higher edge " << ix+1 << "," << iy << " "
			    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
		}
	      }
	      if ( iy < Ncells ) {
		predictedLowEdge  = GetPredictedEdge(ix, iy+1, clusterZ, true);
		predictedHighEdge = GetPredictedEdge(ix, iy+1, clusterZ, false);
		
		if ( predictedLowEdge > predictedHighEdge ) {
		  double tmp = predictedLowEdge;
		  predictedLowEdge  = predictedHighEdge;
		  predictedHighEdge = tmp;
		}
		
		if ( clusterPosition > predictedLowEdge &&
		     clusterPosition < predictedHighEdge &&
		     fabs( clusterPosition - predictedLowEdge)  > 0.01 &&
		     fabs( clusterPosition - predictedHighEdge) > 0.01 ) {
		  std::cout << "WTF: Cluster within road outside of higher edge " << ix << "," << iy+1 << " "
			    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
		}
	      }
	    } // not first layer && last layer
	    else if ( cl_layer == firstLayerNum ) { // cluster is the first layer
              if ( iy < Ncells ) {
                double slope = GetMinimumSlopeDiffTarget(ix, iy+1, clusterPosition, clusterZ, cl_layer);

                if ( slope > low_target_slope && slope < high_target_slope ) {
		  std::cout << "WTF: Cluster within road with correct angle but outside of higher edge " << ix << "," << iy+1 << " "
                            << low_target_slope << " " << slope << " " << high_target_slope << std::endl;
                }
              }
            }
            else { // cluster is in the last layer
              if ( ix < Ncells ) {
                double slope = GetMinimumSlopeDiffTarget(ix+1, iy, clusterPosition, clusterZ, cl_layer);

                if ( slope > low_target_slope && slope < high_target_slope ) {
		  std::cout << "WTF: Cluster within road with correct angle but outside of higher edge " << ix+1 << "," << iy << " "
                            << low_target_slope << " " << slope << " " << high_target_slope << std::endl;
                }
              }
            }

	  } // end of highest ix,iy

	  std::cout << "Road (" << ix << "," << iy <<  ") " << cl_layer << " " 
		    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
	}

      }
      else {
        if ( HTCell_Clusters.at(iy).at(ix).at(cl_layer).size() == 0 ) {
          HTCell_NLayer_WClusters.at(iy).at(ix) += 1;
          if ( HTCell_NLayer_WClusters.at(iy).at(ix) == AcceptSegment_NCluster ) {
            acceptedHTCell_index.push_back(std::make_pair(iy,ix));
	  }
	}
	HTCell_Clusters.at(iy).at(ix).at(cl_layer).push_back(cl);

        if ( verbose ) {

          double predictedLowEdge  = GetPredictedEdge(iy, ix, clusterZ, true);
          double predictedHighEdge = GetPredictedEdge(iy, ix, clusterZ, false);

          if ( predictedLowEdge > predictedHighEdge ) {
            double tmp = predictedLowEdge;
            predictedLowEdge  = predictedHighEdge;
            predictedHighEdge = tmp;
          }

          if ( (clusterPosition < predictedLowEdge ||
		clusterPosition > predictedHighEdge) &&
               !(fabs( clusterPosition - predictedLowEdge)  < 0.01 ||
                 fabs( clusterPosition - predictedHighEdge) < 0.01 ) ) {
	    std::cout << "WTF: Cluster outside of road!!!" << std::endl;
          }

	  if ( ix==lowestXCellIndex && iy==lowestYCellIndex ) {

	    if ( cl_layer != firstLayerNum  && cl_layer != lastLayerNum ) {
	      
	      if ( iy > 0 ) {
		
		double predictedLowEdge  = GetPredictedEdge(iy-1, ix, clusterZ, true);
		double predictedHighEdge = GetPredictedEdge(iy-1, ix, clusterZ, false);
		
		if ( predictedLowEdge > predictedHighEdge ) {
		  double tmp = predictedLowEdge;
		  predictedLowEdge  = predictedHighEdge;
		  predictedHighEdge = tmp;
		}
		
		if ( clusterPosition > predictedLowEdge &&
		     clusterPosition < predictedHighEdge &&
		     fabs( clusterPosition - predictedLowEdge)  > 0.01 &&
		     fabs( clusterPosition - predictedHighEdge) > 0.01 ) {
		  std::cout << "WTF: Cluster within road outside of lower edge " << iy-1 << "," << ix << " "
			    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
		}
	      }
	      if ( ix > 0 ) {
		
		predictedLowEdge  = GetPredictedEdge(iy, ix-1, clusterZ, true);
		predictedHighEdge = GetPredictedEdge(iy, ix-1, clusterZ, false);
		
		if ( predictedLowEdge > predictedHighEdge ) {
		  double tmp = predictedLowEdge;
		  predictedLowEdge  = predictedHighEdge;
		  predictedHighEdge = tmp;
		}
		
		if ( clusterPosition > predictedLowEdge &&
		     clusterPosition < predictedHighEdge &&
		     fabs( clusterPosition - predictedLowEdge)  > 0.01 &&
		     fabs( clusterPosition - predictedHighEdge) > 0.01 ) {
		  std::cout << "WTF: Cluster within road outside of lower edge " << iy << "," << ix-1 << " "
			    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
		}
	      }
	    } // end of if not first or last layer
	    else if ( cl_layer == firstLayerNum ) { // cluster is the first layer
              if ( ix > 0 ) {
                double slope = GetMinimumSlopeDiffTarget(iy, ix-1, clusterPosition, clusterZ, cl_layer);

                if ( slope > low_target_slope && slope < high_target_slope ) {
		  std::cout << "WTF: Cluster within road with correct angle but outside of lower edge " << iy << "," << ix-1 << " "
                            << low_target_slope << " " << slope << " " << high_target_slope << std::endl;
                }
              }
            }
            else { // cluster is in the last layer
              if ( iy > 0 ) {
                double slope = GetMinimumSlopeDiffTarget(iy-1, ix, clusterPosition, clusterZ, cl_layer);

                if ( slope > low_target_slope && slope < high_target_slope ) {
		  std::cout << "WTF: Cluster within road with correct angle but outside of lower edge " << iy-1 << "," << ix << " "
                            << low_target_slope << " " << slope << " " << high_target_slope << std::endl;
                }
              }
            }

          } // end of lowest ix,iy
          else if ( ix==HighestXCellIndex && iy==HighestYCellIndex ) {
	    
	    if ( cl_layer != firstLayerNum  && cl_layer != lastLayerNum ) {

	      if ( iy < Ncells ) {
		double predictedLowEdge  = GetPredictedEdge(iy+1, ix, clusterZ, true);
		double predictedHighEdge = GetPredictedEdge(iy+1, ix, clusterZ, false);
		
		if ( predictedLowEdge > predictedHighEdge ) {
		  double tmp = predictedLowEdge;
		  predictedLowEdge  = predictedHighEdge;
		  predictedHighEdge = tmp;
		}
		
		if ( clusterPosition > predictedLowEdge &&
		     clusterPosition < predictedHighEdge &&
		     fabs( clusterPosition - predictedLowEdge)  > 0.01 &&
		     fabs( clusterPosition - predictedHighEdge) > 0.01 ) {
		  std::cout << "WTF: Cluster within road outside of higher edge " << iy+1 << "," << ix << " "
			    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
		}
	      }
	      if ( ix < Ncells ) {
		predictedLowEdge  = GetPredictedEdge(iy, ix+1, clusterZ, true);
		predictedHighEdge = GetPredictedEdge(iy, ix+1, clusterZ, false);
		
		if ( predictedLowEdge > predictedHighEdge ) {
		  double tmp = predictedLowEdge;
		  predictedLowEdge  = predictedHighEdge;
		  predictedHighEdge = tmp;
		}
		
		if ( clusterPosition > predictedLowEdge &&
		     clusterPosition < predictedHighEdge &&
		     fabs( clusterPosition - predictedLowEdge)  > 0.01 &&
		     fabs( clusterPosition - predictedHighEdge) > 0.01 ) {
		  std::cout << "WTF: Cluster within road outside of higher edge " << iy << "," << ix+1 << " "
			    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
		}
	      }

	    } // cluster position is not first layer and not last layer
	    else if ( cl_layer == firstLayerNum ) { // cluster is the first layer
	      if ( ix < Ncells ) {
		double slope = GetMinimumSlopeDiffTarget(iy, ix+1, clusterPosition, clusterZ, cl_layer);
		
		if ( slope > low_target_slope && slope < high_target_slope ) {
		  std::cout << "WTF: Cluster within road with correct angle but outside of higher edge " << iy << "," << ix+1 << " "
                            << low_target_slope << " " << slope << " " << high_target_slope << std::endl;
		}
	      }
	    }
	    else { // cluster is in the last layer
	      if ( iy < Ncells ) {
		double slope = GetMinimumSlopeDiffTarget(iy+1, ix, clusterPosition, clusterZ, cl_layer);

                if ( slope > low_target_slope && slope < high_target_slope ) {
		  std::cout << "WTF: Cluster within road with correct angle but outside of higher edge " << iy+1 << "," << ix << " "
                            << low_target_slope << " " << slope << " " << high_target_slope << std::endl;
                }
	      } 
	    }

	  } // end if if highest ix,iy

	  std::cout << "Road (" << iy << "," << ix <<  ") " << cl_layer << " "
                    << predictedLowEdge << " " << clusterPosition << " " << predictedHighEdge << std::endl;
        }

      }
    }
  }

  if ( verbose ) std::cout << "finished filling HT cell" << std::endl;

  return;
  
}

void NSWHoughTxNew::ClearStripHitMap() {

  ch_det.resize(lastLayerNum+1);

  for (int layer=0; layer<=lastLayerNum; layer++){
    ch_det.at(layer).resize( NChannelsInLayer );
    for (int i=0; i<NChannelsInLayer; i++) {
      ch_det[layer][i] = NULL;
    }
  }

  return;
  
}

void NSWHoughTxNew::select_hits(NSWSector* sec){

  ClearStripHitMap();

  std::vector<bool> bad_layers;
  bad_layers.resize(lastLayerNum+1);
  for(int iL=0; iL<=lastLayerNum; iL++){
    bad_layers[iL] = false;
  }

  // store channels:
  if ( verbose ) std::cout << "sec->GetNumPacket(): " << sec->GetNumPacket() << std::endl;
  for (int j=0; j<sec->GetNumPacket(); j++){ // loop over packets
    NSWPacket *pac =sec->GetPacket(j);
    if ( verbose ) std::cout << "Before pac->GetWidth(): " << pac->GetWidth() << std::endl;
    if ( verbose ){
      if(pac->GetTechnology() == 1) std::cout << "MM pac->GetWidth(): " << pac->GetWidth() << std::endl;
    }
    if(pac->GetTechnology() != 0) continue; // only sTGC
    if (pac->GetEndPoint() != 1) continue; // only strips
    if ( verbose ) std::cout << "sTGC pac->GetWidth(): " << pac->GetWidth() << std::endl;
    for (int k=0; k<pac->GetWidth(); k++){
      NSWChannel* chn = pac->GetChannel(k);
      //if ( verbose ) std::cout << " CH Quad-1: " << chn->GetRadius() << " CH LAYER: " << chn->GetLayer() << " CH VMM: " << chn->GetVMM() << " VMM-CH: " << chn->GetVMMChannel() 
      //			       << " CH NUM: " << chn->GetDetectorStrip() << " CH POS " << chn->GetStripPosition() << " CH rel_bc " << chn->GetRelBCID() 
      //			       << " CH PDO: " << chn->GetPDO() << " CH AMPLITUDE: " << chn->GetAmplitude() << " CH AMPLITUDE mV: " << chn->GetAmplitude_mV() << " CH BASELINE: " << chn->GetBaseline()
      //			       << " CH NEIGH: " << int(chn->GetNeighbor()) << std::endl;
      
      // if (chn->GetAmplitude_mV() < thr) continue; 
      int layer  = chn->GetLayer();
      int detchn = chn->GetDetectorStrip();
      
      if(verbose) std::cout << "after assigning layer and detchn:" << std::endl;
      
      if(verbose && detchn==-1) std::cout << "If detchn==-1: CH database ID:" << chn->GetDatabaseID() << " CH Quad-1: " << chn->GetRadius() << " CH LAYER: " << chn->GetLayer() << " CH VMM: " << chn->GetVMM() << " VMM-CH: " << chn->GetVMMChannel()
					  << " CH NUM: " << chn->GetDetectorStrip() << " CH POS " << chn->GetStripPosition() << " CH rel_bc " << chn->GetRelBCID()
					  << " CH PDO: " << chn->GetPDO() << " CH AMPLITUDE: " << chn->GetAmplitude() << " CH AMPLITUDE mV: " << chn->GetAmplitude_mV() << " CH BASELINE: " << chn->GetBaseline()
					  << " CH NEIGH: " << int(chn->GetNeighbor()) << std::endl;

      if(detchn==-1) continue;
      
      // firstStrip is index 0; last accepted strip should be ch_det.at(layer).size()-1
      int chnIndex = detchn - firstStripNum; 
      
      if ( layer >= firstLayerNum && layer <= lastLayerNum ) {
	if ((chnIndex>=0) && (chnIndex<ch_det.at(layer).size())) {
	  ch_det[layer][chnIndex]=chn; // store it here 
	}
      }

      if(verbose) std::cout << "End of select_hits" << std::endl;

    } // next k
  } // next j

  if(verbose) std::cout << "End of select_hits function" << std::endl;

}

void NSWHoughTxNew::SetStripHitMap( std::vector< std::vector <NSWChannel*>> stripHitMap ) {
  ch_det = stripHitMap;
}

bool NSWHoughTxNew::isGoodCluster(NSWCluster* cl, std::string &reason, int clusterType, double pos_min_beam, double pos_max_beam){

  bool isValid = true;
  int num_chns = cl->GetNumChannel();

  int layer = -1;

  //double pos_min_beam = 160*3.2 - 1.6;
  //double pos_max_beam = 279*3.2;
  double mean_cluster = -1.0;

  if(clusterType==0)
    mean_cluster = cl->GetCOG();
  else if(clusterType==1)
    mean_cluster = cl->GetGaussianCentroid();
  else if(clusterType==2)
    mean_cluster = cl->GetMeanCaruana();
  else if(clusterType==3)
    mean_cluster = cl->GetParabolaMean();

  /*if(mean_cluster<pos_min_beam || mean_cluster>pos_max_beam){
    isValid=(isValid && false);
    reason = "no_beam_area";
    return false;
  }*/

  if(cl->GetWidth()<min_chan_perclu){
    isValid = (isValid && false);
    reason = "cluster_wid_lt2";
    return false;
  }

  double pos_min = -10000, pos_max = 10000;

  pos_min = cl->GetChannel(0)->GetStripPosition();
  pos_max = cl->GetChannel(num_chns-1)->GetStripPosition();

  if(clusterType==0)
    mean_cluster = cl->GetCOG();
  else if(clusterType==1)
    mean_cluster = cl->GetGaussianCentroid();
  else if(clusterType==2)
    mean_cluster = cl->GetMeanCaruana();
  else if(clusterType==3)
    mean_cluster = cl->GetParabolaMean();
  
  //std::cout << "GetStripPosition(imax): " << GetStripPosition(cl->Getimax()) << std::endl;

  //std::cout << "clusterType: " << clusterType << std::endl;
  //std::cout << "mean_cluster GetParabolaMean: " << mean_cluster << std::endl;

  /* //centroid can be outside
  if(mean_cluster<=pos_min || mean_cluster>=pos_max){
    isValid=(isValid && false);
    reason = "recopos_not_middle";
    //if ( verbose ) std::cout << "centroid not in beam area- layer: " << layer << " centroid: " << mean_cluster << std::endl;
    return false;
  }
  */

  for(int ichn=0; ichn<num_chns; ichn++){
    NSWChannel* channel = cl->GetChannel(ichn);
    int chn_num = -1;
    bool is_chn_bad = false;
    
    if(fabs(ichn-cl->Getimax()) > 1) continue;
    
    if(channel==NULL){
      if(ichn==0){
	layer = cl->GetChannel(ichn+1)->GetLayer();
	chn_num = cl->GetChannel(ichn+1)->GetDetectorStrip()-1;
      }
      else if(ichn==num_chns-1){
	layer = cl->GetChannel(ichn-1)->GetLayer();
	chn_num= cl->GetChannel(ichn-1)->GetDetectorStrip()+1;
      }
      else{
	layer = cl->GetChannel(ichn+1)->GetLayer();
	chn_num= cl->GetChannel(ichn+1)->GetDetectorStrip()-1;
      }
    }
    else{
      layer = channel->GetLayer();
      chn_num = channel->GetDetectorStrip();
    }

    is_chn_bad = isChnBad(layer, chn_num);

    /*if(is_chn_bad){
      isValid = (isValid && false);
      reason = "chn_bad";
      return false;
    }*/

    if(channel==NULL) continue;
    
    /*if(sng){

      if( ichn==0 || ichn==(num_chns-1) ){ //edge channels are not neighbours
	
      }
      else if(channel->GetNeighbor()==0){ //middle ones are neighbours 
	isValid=(isValid && false);
	reason = "middle_no_neighbour";
	return false;
      }
    }*/
    
    if(fabs(ichn-cl->Getimax()) > 0) continue;

    //========== relbcid 0 1 7 in central strips events are 0 ===========//
    //if ( verbose ) std::cout << "MaxPdo Value: " << cl->GetMaxPDO() << " Pdo of imax: " << cl->GetChannel(cl->Getimax())->GetPDO() << std::endl;
    if( channel->GetRelBCID() == 0 || channel->GetRelBCID() == 1 || channel->GetRelBCID() == 7 ){
      //if ( verbose ) std::cout<< "fabs(ichn-cl->Getimax()): "<< fabs(ichn-cl->Getimax()) << " Cluster width: " << cl->GetWidth() << std::endl;
      isValid=(isValid && false);
      reason = "bkg_relbcid_chn";
      return false;
    }
    //else if(fabs(ichn-cl->Getimax()) > 1 && (channel->GetRelBCID() == 0 || channel->GetRelBCID() == 1 || channel->GetRelBCID() == 7)){
      //if ( verbose ) std::cout << "fabs(ichn-cl->Getimax()): " << fabs(ichn-cl->Getimax()) << " Cluster width: " << cl->GetWidth() << std::endl;
    //}

    //========== relbcid 0 1 7 events are 0 ===========// 
    /*if(channel->GetRelBCID() == 0 || channel->GetRelBCID() == 1 || channel->GetRelBCID() == 7){
      isValid=(isValid && false);
      reason = "bkg_relbcid_chn";
      return false;       
    }*/

  }

  return isValid;

}


void NSWHoughTxNew::SetClusters(NSWClusterList clusterList_tmp, NSWClusterList clusterList_badclusters_tmp) {

  clusterList = clusterList_tmp;
  clusterList_badclusters = clusterList_badclusters_tmp;
  
  //--------------------------------------------------------------------//
  //    Final list of clusters are filled into Clusters_PerLayer
  //--------------------------------------------------------------------//

  Clusters_PerLayer.resize(lastLayerNum+1);
  for (int layer=0; layer<Clusters_PerLayer.size(); layer++){
    Clusters_PerLayer.at(layer).resize(0);
  }

  for ( int layer=0; layer<Clusters_PerLayer.size(); layer++){
    for (int icl=0; icl<clusterList.GetNumClusters(layer); icl++){
      NSWCluster *cl = clusterList.GetCluster(layer, icl);
      
      cl->FitGaussian();
      cl->FitCaruana();
    
      Clusters_PerLayer.at(layer).push_back(cl);
    }
  }

  return;

}

void NSWHoughTxNew::FindClusters(int clusterType){

  //----------------------------------------------------------//
  //  Reconstruct all clusters from all hits on all layers
  //----------------------------------------------------------//
  if(verbose) std::cout << "Reconstruct all clusters from all hits on all layers start" << std::endl;
  std::vector< std::vector <NSWCluster*>> Clusters_PerLayer_tmp;

  Clusters_PerLayer_tmp.resize(lastLayerNum+1);
  for (int layer=0; layer<Clusters_PerLayer_tmp.size(); layer++){
    Clusters_PerLayer_tmp.at(layer).resize(0);
  }

  Clusters_PerLayer.resize(lastLayerNum+1);
  for (int layer=0; layer<Clusters_PerLayer.size(); layer++){
    Clusters_PerLayer.at(layer).resize(0);
  }


  //----------------------------------------------------------//
  //          Loop layer and reconstruct clusters
  //----------------------------------------------------------//

  NSWCluster* cl = NULL;

  for (int layer=firstLayerNum; layer<=lastLayerNum; layer++){

    int min_chn = 0;
    int max_chn = ch_det.at(layer).size();

    int chn = min_chn; // count channels

    //--------------------------------------------------//
    //             Loop over all channels
    //--------------------------------------------------//

    do{

      if ( chn+1 >= max_chn ) break;

      //if ( verbose ) std::cout << "channel in clusterization: " << chn << std::endl;

      //------------------------------------------------//
      //       If current channel is hit
      //------------------------------------------------//

      if (ch_det[layer][chn] != NULL){// && ch_det[layer][chn]->GetAmplitude()>0) { // there is a channel

	//-----------------------------------------------//
	//        If neighbor channel is hit
	//-----------------------------------------------//

	if(ch_det[layer][chn+1] != NULL){// && ch_det[layer][chn]->GetAmplitude()>0){

	  //------------------------------------------------------//
	  //  if current channel is neighbor on edge hit 
	  //  but next channel is true over threshold hit
	  //------------------------------------------------------//

	  if(ch_det[layer][chn]->GetNeighbor()==0 && ch_det[layer][chn+1]->GetNeighbor()==1){ 

	    //-------------------------------------------------//
	    //  If no new clusters exist, begin new cluster
	    //  place both current + next channel into cluster
	    //-------------------------------------------------//

	    if (cl == NULL) { // start of new cluster
	      cl = new NSWCluster(ch_det[layer][chn]); // first channel
	      cl->AddChannel(ch_det[layer][chn+1]);
	      chn+=2; // processed 2 channels
	    }
	    //--------------------------------------------------//
	    //   If cluster already exist, finish old cluster with neighbor off edge ch. (current ch.)
	    //   start new cluster with next ch. into cluster
	    //--------------------------------------------------//
	    else { // continuation cluster ended
	      cl->AddChannel(ch_det[layer][chn]);

	      //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
	      cl->FitGaussian();
	      cl->FitCaruana();
	      cl->SaveParabolaMean();
	      //if ( verbose ) std::cout << "1: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
	      Clusters_PerLayer_tmp[layer].push_back(cl);

	      // begin next cluster
	      cl = NULL;
	      cl = new NSWCluster(ch_det[layer][chn]);
	      cl->AddChannel(ch_det[layer][chn+1]);

	      // processed 2 channels
	      chn+=2;
	    }
	  }
	  //------------------------------------------------------------------//
	  //   if both current and next cluster are neighbor on edge hits
	  //------------------------------------------------------------------//
	  else if(ch_det[layer][chn]->GetNeighbor()==0 && ch_det[layer][chn+1]->GetNeighbor()==0){ // divide the cluster

	    //------------------------------------------------------------------------//
	    //   If no cluster exists.  Ignore current ch. hit
	    //   add next ch. hit, also edge hit neighbor on, as first hit in cluster
	    //------------------------------------------------------------------------//
	    if (cl == NULL) { // start of new cluster
	      cl = new NSWCluster(ch_det[layer][chn+1]); // first channel
	      chn+=2; // processed 2 channels
	    }
	    //----------------------------------------------------------------------//
	    //  If cluster already exist, finish current cluster with the current ch.
	    //  which is it's last edge hit.  start new cluster with new edge hit (next ch.)
	    //----------------------------------------------------------------------//
	    else{

	      // finish current cluster with added edge hit (current chn)
	      cl->AddChannel(ch_det[layer][chn]);

	      cl->FitGaussian();
	      cl->FitCaruana();
	      cl->SaveParabolaMean();
	      //if ( verbose ) std::cout << "2: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
	      Clusters_PerLayer_tmp[layer].push_back(cl);

	      //  Begin new cluster with edge hit (chn+1)
	      cl = NULL;
	      cl = new NSWCluster(ch_det[layer][chn+1]);
	      chn+=2;
	    }
	  }
	  //---------------------------------------------------------------------//
	  //     if both current and next ch. hits are true over threshold hits
	  //     or if current ch. hit is over threshold and next ch. hit is edge neighbor hit
	  //     Only process current over thr. hit. check next + 2nd next ch. next iteration of loop
	  //---------------------------------------------------------------------//
	  else{
	    
	    if (cl == NULL) { // start of new cluster
	      cl = new NSWCluster(ch_det[layer][chn]); // first channel
	      chn+=1; // process one channel
	    }
	    else{ // or add current ch. to current cluster
	      cl->AddChannel(ch_det[layer][chn]);
	      chn+=1; // process one channel
	    }

	  }
	} // end of if(ch_det[layer][chn+1] != NULL)
	//----------------------------------------------------------//
	//  if current ch. is hit but next detector ch. is not hit
	//----------------------------------------------------------//
	else{
	  //-------------------------------------//
	  //  if current ch. is an edge hit
	  //-------------------------------------//
	  if(ch_det[layer][chn]->GetNeighbor()==0){
	    // if its the only channel in a cluster then ignore
	    if (cl == NULL) {
	      chn+=1; // skip channel hit
	    }
	    // if a cluster already exists. add to the back of the cluster 
	    // output cluster
	    else{
	      cl->AddChannel(ch_det[layer][chn]);
	      //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
	      cl->FitGaussian();
	      cl->FitCaruana();
	      cl->SaveParabolaMean();
	      //if ( verbose ) std::cout << "3: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() 
	      //<< " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
	      Clusters_PerLayer_tmp[layer].push_back(cl);

	      cl = NULL;
	      chn+=1;
	    }
	  }
	  //------------------------------------------//
	  //  if current channel is a over thr. hit 
	  //------------------------------------------//
	  else{
	    //  if it's a new cluster, start cluster
	    if (cl == NULL) { // start of new cluster
	      cl = new NSWCluster(ch_det[layer][chn]); // first channel
	      chn+=1;
	    }
	    // if cluster exists, add the channel
	    else{
	      cl->AddChannel(ch_det[layer][chn]);
	      chn+=1;
	    }
	  }
	} // end of if current ch. is hit but next detector ch. is not hit
      } // end of if current channel is hit 
      //---------------------------------------------//
      //  if current channel is not hit
      //---------------------------------------------//
      else{  // no channel
	//--------------------------------------------//
	//        if a cluster already exists
	//--------------------------------------------//
	if (cl != NULL) { // in a cluster
	  //------------------------------------------//
	  //  if we are not at the end of the layer
	  //  and the next channel is a hit
	  //------------------------------------------//
	  if ((chn+1<=max_chn) && (ch_det[layer][chn+1] != NULL)){// && ch_det[layer][chn+1]->GetAmplitude()>0)){ // only one missing
	    //--------------------------------------------//
	    // if previous ch. is a true over thr. hit
	    // and next ch. is a true over thr. hit
	    // add a null hit into the middle of the cluster
	    // assume this is a missing channel in the middle of a cluster
	    //------------------------------------------------//
	    if(ch_det[layer][chn-1]->GetNeighbor()==1 && ch_det[layer][chn+1]->GetNeighbor()==1){
	      cl->AddChannel(NULL);
	      cl->AddChannel(ch_det[layer][chn+1]);
	      chn +=2;
	    }
	    //-----------------------------------------------//
	    // if previous hit was not a true over thr. hit
	    // output the cluster as null hit is the end of cluster
	    //-----------------------------------------------//
	    else{ // cluster ended

              //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
	      cl->FitGaussian();
	      cl->FitCaruana();
	      cl->SaveParabolaMean();
	      //if ( verbose ) std::cout << "4: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() 
	      //<< " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
	      Clusters_PerLayer_tmp[layer].push_back(cl);

	      cl = NULL;
	      chn +=1;

	    }
	  } // end of ((chn+1<=max_chn) && (ch_det[layer][chn+1] != NULL))
	  //-------------------------------------------------//
	  // if next hit is also null just like current hit
	  // and cluster exists then output cluster
	  //-------------------------------------------------//
	  else { // cluster ended
	    //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
	    cl->FitGaussian();
	    cl->FitCaruana();
	    cl->SaveParabolaMean();
	    //if ( verbose ) std::cout << "5: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " 
	    //<< cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
	    Clusters_PerLayer_tmp[layer].push_back(cl);

	    cl = NULL;
	    chn +=2; //skip current+next null channels
	  } // if 2 missing
	} // end of if cluster exists
	//----------------------------------------------------------//
	// if current ch. has no hit && no current cluster exists
	// don't do anything and move on
	//----------------------------------------------------------//
	else{
	  chn++;
	}
      } // no channel
    } while (chn<max_chn); // process every hit

    //----------------------------------------------------------//
    //  if we are at then end of the channel list
    // and we have 1 final cluster. output cluster
    //----------------------------------------------------------//
    
    if (cl != NULL) { // last cluster in this layer
      //if ((cl->GetWidth()>=3)){//&&(cl->GetMaxPDO()>=40)) {
      cl->FitGaussian();
      cl->FitCaruana();
      cl->SaveParabolaMean();
      //if ( verbose ) std::cout << "6: gaussian centroid: " << cl->GetGaussianCentroid() << " chi squared: " << cl->GetGaussianChisq() << " cluster width: " << cl->GetWidth() << " num of channels in the cluster: " << cl->GetNumChannel() << std::endl;
      Clusters_PerLayer_tmp[layer].push_back(cl);

      cl = NULL;
    }
  } // end of loop over all layers

  delete cl;

  //==================================================================================================//
  //=============== End of Clustering taken from Michael's code in NSWClusterList ====================//
  //==================================================================================================//

  //--------------------------------------------------------------------------------------------------//
  //                                     Split Clusters 
  //--------------------------------------------------------------------------------------------------//

  clusterList.Clear();
  clusterList.SetClusterList(Clusters_PerLayer_tmp);
    
  clusterList_badclusters.Clear();

  for (int layer=firstLayerNum; layer<=lastLayerNum; layer++){

    //---------------------------------------------------------//
    // find the location of any ch. = null = no hit
    // split cluster on nulls if, null is at start or end of cluster
    // or null is flanked by two neighbor off edge channels
    //---------------------------------------------------------//

    for (int i=0; i<clusterList.GetNumClusters(layer); i++){

      NSWCluster *cl = clusterList.GetCluster(layer, i);
      int num_chns = cl->GetNumChannel();

      std::vector<int> nul;
      nul.clear();

      for(int ichn=0; ichn<num_chns; ichn++){

	NSWChannel* channel = cl->GetChannel(ichn);
	// find NULL position
	if(channel==NULL){
	  if( ichn==0 || ichn == num_chns-1 ) nul.push_back(ichn);
	  else if( cl->GetChannel(ichn+1)->GetNeighbor() == 0 && 
		   cl->GetChannel(ichn-1)->GetNeighbor() == 0 )
	    nul.push_back(ichn);
	}

      } //channel

      for(int j=0; j<nul.size(); j++){

	// split at all null points
	if( j == 0 ){
	  clusterList.SplitCluster(layer, i, nul[j], true);
	}
	else{
	  clusterList.SplitCluster(layer, i, nul[j] - nul[j-1] - 1, true);
	}
	if( nul[j] != 0 && nul[j] != num_chns-1 ) i++;
      }

    } // end of loop over cluster list

    //---------------------------------------------------------//
    // find the location of any valleys
    // valley-1 PDO > valley PDO < valley+1 PDO
    // split cluster on valleys 
    //---------------------------------------------------------//

    float min_valley_PDO_diff = 5.0;

    for (int i=0; i<clusterList.GetNumClusters(layer); i++){

      NSWCluster *cl = clusterList.GetCluster(layer, i);
      int num_chns = cl->GetNumChannel();
      if(num_chns < 6) continue; // don't split clusters smaller than 6

      std::vector<int> valley;
      valley.clear();

      for(int ichn=0; ichn<num_chns; ichn++){

	NSWChannel* channel = cl->GetChannel(ichn);

	if( channel == NULL ) continue;
	if(ichn<=1 || ichn>=num_chns-2) continue; // do not break off single channels

	// find closest 2 hits and check to see if they are both greater than valley
	// by at least min_valley_PDO_diff amount of PDO

	if( cl->GetChannel(ichn-1) == NULL && cl->GetChannel(ichn+1) == NULL ){
	  if ( ichn-2 < 0 || ichn+2 >= num_chns ) continue; // should not happen as end of cluster if null was removed before 
	  if( (cl->GetChannel(ichn-2)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff && 
	      (cl->GetChannel(ichn+2)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff ) valley.push_back(ichn);
	}
	else if( cl->GetChannel(ichn-1) == NULL ){
	  if ( ichn-2 < 0 ) continue; // should not happen as end of cluster if null was removed before
	  if( (cl->GetChannel(ichn-2)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff && 
	      (cl->GetChannel(ichn+1)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff ) valley.push_back(ichn);
	}
	else if( cl->GetChannel(ichn+1) == NULL ){
	  if ( ichn+2 >= num_chns ) continue; // should not happen as end of cluster if null was removed before
	  if( (cl->GetChannel(ichn-1)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff && 
	      (cl->GetChannel(ichn+2)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff ) valley.push_back(ichn);
	}
	else if( cl->GetChannel(ichn-1) != NULL && cl->GetChannel(ichn+1) != NULL ){
	  if( (cl->GetChannel(ichn-1)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff && 
	      (cl->GetChannel(ichn+1)->GetAmplitude() - channel->GetAmplitude())>=min_valley_PDO_diff ) valley.push_back(ichn);
	}
      }// end of finding valley loop

      //-------------------------------------------//
      //         Split at every valley
      //-------------------------------------------//

      for(int j=0; j<valley.size(); j++){
	if( j==0 ){
	  clusterList.SplitCluster(layer, i, valley[j], false);
	}
	else{
	  clusterList.SplitCluster(layer, i, valley[j]-valley[j-1], false);
	}
	i++;
      }

    } // end of split at all valleys loop
  
    // if there are no more clusters then continue
    if(clusterList.GetNumClusters(layer)==0) continue;

    for (int icl=0; icl<clusterList.GetNumClusters(layer); icl++){

      std::string reason = "";
      NSWCluster *cl = clusterList.GetCluster(layer, icl);

      if( !isGoodCluster(cl,reason,clusterType, 
			 NSWChannelID::GetStripPosition(0)-stripWidth/2.0, 
			 NSWChannelID::GetStripPosition(NChannelsInLayer)+stripWidth/2.0 ) ) {
	clusterList_badclusters.AddCluster(layer, cl);
	clusterList.RemoveBadCluster(layer, icl);
	icl--;
      }

    }

    //--------------------------------------------------------------------//
    //    Final list of clusters are filled into Clusters_PerLayer
    //--------------------------------------------------------------------//

    for (int icl=0; icl<clusterList.GetNumClusters(layer); icl++){
      NSWCluster *cl = clusterList.GetCluster(layer, icl);
      
      cl->FitGaussian();
      cl->FitCaruana();
      
      Clusters_PerLayer.at(layer).push_back(cl);
    
      
    }

  } // end of loop over layers
  if(verbose) std::cout << "End of FindClusters" << std::endl;
  return;

}

void NSWHoughTxNew::Fill_HT( int clusterType, int BeamStartStrip, int BeamEndStrip){
  
  //-------------------------------------------------//
  //     Run Hough Transform only near the beam
  //-------------------------------------------------//

  int beamWidth  = BeamStartStrip-BeamEndStrip;

  //Only run over intercept near beam location
  //widening by 5 allows 25 track on edges
  int startCell = (BeamStartStrip - 5)/Nstrips_percell;
  int endCell   = (BeamEndStrip + 5)/Nstrips_percell+1; 

  if ( endCell > Ncells ) endCell = Ncells;
  if ( startCell < 1 )    startCell = 1;

  int lowlimitBeam = BeamStartStrip - 5;
  int highlimitBeam = BeamEndStrip  + 5;

  if ( lowlimitBeam < 0 ) lowlimitBeam = 0;
  if ( highlimitBeam > NChannelsInLayer - 1 ) highlimitBeam = NChannelsInLayer - 1;

  double startBeamPos = NSWChannelID::GetStripPosition( lowlimitBeam );
  double endBeamPos   = NSWChannelID::GetStripPosition( highlimitBeam );

  if ( startBeamPos > endBeamPos ) {
    double tmp = endBeamPos;
    endBeamPos = startBeamPos;
    startBeamPos = tmp;
  }

  //---------------------------------------------//
  //   Clear the HTCell_Clusters
  //---------------------------------------------//

  Clear();
  
  //-----------------------------------------------------------//
  //              Loop over all clusters
  //-----------------------------------------------------------//

  for( int iL=firstLayerNum; iL<=lastLayerNum; iL++ ) {
    for ( int ic=0; ic<Clusters_PerLayer.at(iL).size(); ic++ ) {

      NSWCluster * cl = Clusters_PerLayer.at(iL).at(ic);

      double clusterPosition=0;
      if ( clusterType == 0 ) {
	clusterPosition = cl->GetCOG();
      }
      else if ( clusterType == 1 ) {
	clusterPosition = cl->GetGaussianCentroid();
      }
      else if ( clusterType == 2 ) {
	clusterPosition = cl->GetMeanCaruana();
      }
      else if( clusterType == 3 ) {
	clusterPosition = cl->GetParabolaMean();
      }
      else {
	std::cout << "WTF: Cluster type not recognnized" << std::endl;
	return;
      }

      if ( clusterPosition > startBeamPos && clusterPosition < endBeamPos ) {
	FillHTCell( cl, iL, clusterType );
      }

    } // end of loop over Clusters_PerLayer
  } // end of loop over layers

  if ( verbose ) std::cout << "filling acceptedHTCell " << acceptedHTCell_index.size() << std::endl;

  for ( int icell =0; icell<acceptedHTCell_index.size(); icell++ ) {
    int ixL4 = acceptedHTCell_index.at(icell).first;
    int ixL7 = acceptedHTCell_index.at(icell).second;
    if ( verbose ) std::cout << ixL4 << " " << ixL7 << " " << HTCell_Clusters.at(ixL4).at(ixL7).size() << std::endl;
    acceptedHTCell_clusters.push_back(HTCell_Clusters.at(ixL4).at(ixL7));
  }

  if ( verbose ) std::cout << "filled acceptedHTCell " << acceptedHTCell_clusters.size() << std::endl;

  return;

}
