//General reading class for atlas csc data files 
#include<stdio.h>
//#include <time.h>
#include"NSWRead/NSWEvent.h"
/** the DEBUG flag enables output for data format debugging:
    set it to 1 to enable debugging output, 0 to disable.
*/

int DEBUG = 0; // debug level 0, 1, or 2

/**
   Constructor of the NSWEvent class.
This creates an empty event.
*/

NSWEvent::NSWEvent(){
  lumiBlock = 0;
  BCID = 0;
  L1ID = 0;
} 

/** destructor of the event class
    deletes all allocated objects.
*/
NSWEvent::~NSWEvent(){
  Clear();
}

/** delete any previous data */
void NSWEvent::Clear(){
  for (unsigned int i=0; i<sector.size(); i++) delete  sector[i];
  sector.clear();
  lumiBlock = 0;
  BCID = 0;
  L1ID = 0;
  pAA1234AA = NULL; // keep pointer to full atlas event header
}




/** read one entire ATLAS event into an (external) buffer.
    This should be followed by a call to ReadNSW() to fill the data members 
    of this class.
    For data from the eventbuilder, this includes the full event header.
    For data from the crate, this includes the ROS header.
    If the event is already in a buffer, skip this function and go to ReadNSW.

    @param infile is a FILE pointer to the raw data file
    @param buf The calling program should allocate the buffer.
    @param maxSize is the size of the buffer in words
    @return Returns the number of words read, or 0 if there was an error.
    This size can be from the ATLAS event builder 
    (in which case there can be several ROD fragments in this buffer), 
    or from the ROS (which means that there is only one ROD fragment)
    @see ReadNSW()
*/
int NSWEvent::ReadATLAS (FILE* infile, unsigned int * buf, int maxSize){

  unsigned int * p = buf;
  int num = 0; // number of words read
  unsigned int header, size;

  //first delete old sectors in case this event is recycled:
  Clear();

  do { // search for event or ROS header
    if (fread (p, 4, 1, infile) != 1){
      if (feof(infile)) { // EOF has been reached
	THROW_EXCEPTION ("EOF has been reached.", SEV_INFO);
      } else {
	THROW_EXCEPTION ("Cannot read ATLAS event from file.", SEV_FATAL);
      }
      return 0; // read error
    }
    header = *p;
    if (DEBUG>2) printf ("ReadATLAS: header = %08x\n", header);
    //don't increment p until the right header has been found
    num++;
    if (num>maxSize){
      if (feof(infile)) { // EOF has been reached
	THROW_EXCEPTION ("Unexpected EOF has been reached.", SEV_WARN);
      } else {
	THROW_EXCEPTION ("Cannot find ATLAS or ROD header.", SEV_WARN);
      }
      return 0; // buffer space exceeded
    }
  } while ((header != 0xaa1234aa));// && (header != 0xcc1234cc) && (header != 0xdd1234dd) );
  pAA1234AA = p; // keep pointer to full atlas event header
  // aa1234aa is a full atlas event header
  // cc1234cc is the ROS header
  // dd1234dd is the ROBin header
  p++;
  // read the size:
  if (fread (p, 4, 1, infile) != 1){
    if (feof(infile)) { // EOF has been reached
      THROW_EXCEPTION ("Unexpected EOF has been reached.", SEV_INFO);
    } else {
      THROW_EXCEPTION ("Cannot read the size of the ATLAS event.", SEV_FATAL);
    }
    return 0; // read error
  }
  size = *p;
  p++;
  num++;
  if (num>maxSize) {
    THROW_EXCEPTION ("Buffer space for ATLAS event exceeded.", SEV_WARN);
    return 0; // buffer space exceeded
  }
  if (fread (p, 4, size-2, infile) != size-2) { // rest of header
    if (feof(infile)) { // EOF has been reached
      THROW_EXCEPTION ("Unexpected EOF has been reached.", SEV_INFO);
    } else {
      THROW_EXCEPTION ("Cannot read ATLAS event from file.", SEV_ERROR);
    }
    return 0; // read error
  }
  p += size;
  num += size-2;
  if (num>maxSize) {
    THROW_EXCEPTION ("Buffer space for ATLAS event exceeded.", SEV_WARN);
    return 0; // buffer space exceeded
  }

  if (header == 0xaa1234aa) { // global ATLAS event header has date/time and event number
      numStatus = pAA1234AA[5];
      globalID = pAA1234AA[numStatus+9]; // event number
      dateTime = pAA1234AA[numStatus+7];
      runNumber = pAA1234AA[numStatus + 12];
      lumiBlock = pAA1234AA[numStatus + 13];
      nano = pAA1234AA[numStatus+8];
      L1ID = pAA1234AA[numStatus + 14];
      BCID = pAA1234AA[numStatus + 15];
      //printf("ReadATLAS: eventNumber = %d, dateTime = %08X, nano=%d L1ID=%08X, BCID=%08X\n", globalID, dateTime,  nano, L1ID, BCID);
    }
  // rosStatus = p + ??? ;
  if (DEBUG) printf ("ReadATLAS: read %i words in total and %i for the event.\n", num, size);
  return size;
}

/** overwrite the global ID of the event in memory. This is used to fix the 
    ATLAS headers written by swROD.*/
void NSWEvent::FixGlobalID (int id){
  globalID = id;
  pAA1234AA[numStatus+9] = id;
}

/**
   Fills the internal data members of this class with event data.
   @param source a buffer with an atlas event.
   source has been filled by ReadATLAS() or the monitoring system.
   @param size the number of words in the source buffer. 
   This is not the allocated size, but the actual number of words as returned by ReadAtlas().
   @return  true of reading went ok, false otherwise.
   The return value should be used to validate the event: do not
   process the event if the return value is negative.
   @note Some events have no NSW ROD fragments. In this case, reading is
   succesful and TRUE is returned, but the GetNumSectors() of the
   event will be zero, indicating an empty event.
   @see ReatATLAS()
*/
int NSWEvent::ReadNSW (unsigned int * source, int size){
  int sourceID;
  unsigned int * p = &source[-1];     // read pointer
  unsigned int * end = source + size; // end  pointer
  //  unsigned int * start;               //start of ROD data pointer
  // int lengthROB = 0; // !!! should be one for each ROD fragment !!!



  do {  // read the new data from the source buffer:
    do{ // loop over RODs, NSW or other
      do{ // find next  header marker
	p++;
	if (p>end)  { // end of buffer, maybe other det. are following NSW
	  //THROW_EXCEPTION ("Cannot find a ROD header in this event.");
	  return (GetNumSector() > 0); // got at least one NSW ROD.
	} 
	if (DEBUG>1) printf ("ReadNSW: Reading %8x\n", *p);
      } while (*p != 0xDD1234DD);
      if (DEBUG) printf ("ReadNSW: Found ROD header %8x\n", *p);
      //start = p-1; // start of ROD data

      sourceID = (p[4]>>16)&0xff;
      if (sourceID == 0)sourceID = (p[4]>>24)&0xff; // FIXES BAD OKS VALUE!
      if (DEBUG) printf ("sourceID = 0x%2x\n", sourceID);

    } while ((sourceID<0x6B) || (sourceID>0x6E)); // NSW endcaps
    
    // got a NSW ROBin header.   
    if (DEBUG) printf ("Reading sectors:\n");

    NSWSector *s = new NSWSector();
    int ret = s->Read(p);
    if (ret<0) {
      THROW_EXCEPTION ("Cannot read sector.", SEV_ERROR);
      return ret;
    } else {
      sector.push_back(s);
    }
    p+= ret-1;
   

    if (DEBUG) printf ("NSWEvent::ReadNSW() %li words left.\n", end-p);

  } while (p<end-10); // next ROD, skip the 1 or 2 ROS trailer words at the end
  return 1; // successful reading

}//end readNSW


/**
   Fills the internal data members of this class with event data.
   @param rods a vector of pointers to the rod fragments.
   @param sizes a vecotr of the number of words in the rods. 
   This is not the allocated size, but the actual number of words as returned by ReadAtlas().
   @return  true of reading went ok, false otherwise.
   The return value should be used to validate the event: do not
   process the event if the return value is negative.
   @note Some events have no NSW ROD fragments. In this case, reading is
   succesful and TRUE is returned, but the GetNumSectors() of the
   event will be zero, indicating an empty event.
   @see ReatATLAS()
*/
int NSWEvent::ReadGnam (std::vector<const uint32_t *> rods,
                        std::vector<unsigned long int> sizes){
  //printf ("ReadGnam: %d fragments.\n", rods.size());
  Clear(); // first delete old sectors in case this event is recycled.

  //  for (unsigned int i=0; i<rods.size(); i++){ // loop over ROD fragments
  for (unsigned int i=0; i<1; i++){ // loop over ROD fragments
    //printf ("ReadGnam: fragment %d: size = %d.\n", i, sizes[i]);
    //for (int j=0; j<sizes[i]; j++){
    //  printf ("%3d: %08X\n", j, rods[i][j]);
    //}
    NSWSector *s = new NSWSector();
    int ret = s->ReadGnam (rods[i], sizes[i]);
    if (ret<0) {
      THROW_EXCEPTION ("Cannot read sector.", SEV_ERROR);
      return ret;
    } else {
      sector.push_back(s);
    }
  }
  return 1; // successful reading
}//end ReadGnam


int NSWEvent::Store( unsigned int * buf){
  /*  unsigned int * start = buf;
  if (numChamber<1) return 0; // write nothing

  // ROD header
  *buf++ = atlasHeader[0].marker;
  *buf++ = atlasHeader[0].size;
  *buf++ = atlasHeader[0].version;
  *buf++ = atlasHeader[0].sourceID;
  *buf++ = atlasHeader[0].runNumber;
  *buf++ = atlasHeader[0].level1ID;
  *buf++ = atlasHeader[0].BCID;
  *buf++ = atlasHeader[0].triggerType;
  *buf++ = atlasHeader[0].eventType;

  int length = sector[0]->Store(buf);
  buf += length;


  *buf++ = 0; // status word
  // ROD trailer:
  *buf++ = 1;                  // there is one status element
  *buf++ = length0 + length1;  // total number of data words
  *buf++ = 1;                  // the status element is located after the data
  */
  return 0;//buf-start;
}

void NSWEvent::Simulate(TAtlasHeader h, unsigned int channelID[2]){
  /**
  int i;
  // first delete old chambers in case this event is recycled:
  for(i=0; i<numSector; i++) delete sector[i];
  numSector = 0; // empty event
  atlasHeader[0] = h;

  for (i = 0; i<1; i++){
    sector[numsector] = new NSWSector();
    sector[numSector]->SetSector (atlasHeader[numChamber].sourceID, 0x300);
    sector[numSector]->Simulate(channelID[i], numSample, i==0?5:11);
    numChamber ++;
  }
  */
}

/**
   Print some information about the event by looping over all chambers
   and traversing the hierachy down to the NSWStrip() level. 
   This can be used for debugging or for converting events to ASCII.
*/
void NSWEvent::Print ()  const {
  // print some information
  printf ("NSWEvent::Print Event %d, LumiBlock %d, L1ID 0x%X, BCID %d, %li fragments\n", globalID, lumiBlock, L1ID, BCID, sector.size());

  if (sector.size() > 80) {
    printf ("BAD EVENT: %li sectors\n\n", sector.size());
  } else { 
    for (unsigned int i=0; i<sector.size(); i++) sector[i]->Print();
  }
}

