//General reading class for atlas nsw data files 
#include <stdio.h>
//#include <stdlib.h>
#include <math.h>
#include <algorithm>

#include "NSWRead/NSWSegmentList.h"


NSWSegmentList::NSWSegmentList(){
} 


NSWSegmentList::~NSWSegmentList(){
  for (int i=0; i<GetNumSegments(); i++) delete segment[i];
  segment.clear();
}


void NSWSegmentList::Clear(){
  for (int i=0; i<GetNumSegments(); i++) delete segment[i];
  segment.clear();
} 


void NSWSegmentList::AddSegment(NSWSegment* seg){
  segment.push_back(seg);
}

void NSWSegmentList::RemoveSegment(int index){
  segment.erase(segment.begin()+index);
}

void NSWSegmentList::Fill (NSWClusterList* clusterList, double chiCut){
  const double dMax = 36; // 36mm cut for pairs of hits
  //declaring vector of pairs 
  std::vector <std::pair <NSWCluster*, NSWCluster*> > upperPair, lowerPair;
  std::vector <NSWCluster*> upperSingle, lowerSingle;
  double p0, p1, p6, p7; // cog positions
  upperPair.clear();
  lowerPair.clear();
  upperSingle.clear();
  lowerSingle.clear();
  // layer 0 and 1:
  for (int i0 = 0; i0 < clusterList->GetNumClusters(0); i0++){
    NSWCluster *c0 = clusterList->GetCluster(0, i0);
    p0 = c0->GetCOG();
    if (p0 < 0) continue;
    for (int i1 = 0; i1 < clusterList->GetNumClusters(1); i1++){
      NSWCluster *c1 = clusterList->GetCluster(1, i1);
      p1 = c1->GetCOG();
      if (p1 < 0) continue;
      if (fabs(p0-p1)<dMax){
	upperPair.push_back (std::make_pair(c0, c1)); 
      }
    }
  }
  // layer 0 single
  for (int i0 = 0; i0 < clusterList->GetNumClusters(0); i0++){
    NSWCluster *c0 = clusterList->GetCluster(0, i0);
    p0 = c0->GetCOG();
    if (p0 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<upperPair.size(); i++){
      if (upperPair[i].first == c0) {
	found = true;
	break;
      }
    }
    if (!found) upperSingle.push_back(c0);
  }
  // layer 1 single
  for (int i1 = 0; i1 < clusterList->GetNumClusters(1); i1++){
    NSWCluster *c1 = clusterList->GetCluster(1, i1);
    p1 = c1->GetCOG();
    if (p1 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<upperPair.size(); i++){
      if (upperPair[i].second == c1) {
	found = true;
	break;
      }
    }
    if (!found) upperSingle.push_back(c1);
  }
  //printf("upperPair %ld, upperSingle %ld, 0: %d, 1: %d\n", upperPair.size(), upperSingle.size(), clusterList->GetNumClusters(0), clusterList->GetNumClusters(1));
  
  // lower precision layers 6 and 7:
  for (int i6 = 0; i6 < clusterList->GetNumClusters(6); i6++){
    NSWCluster *c6 = clusterList->GetCluster(6, i6);
    p6 = c6->GetCOG();
    if (p6 < 0) continue;
    for (int i7 = 0; i7 < clusterList->GetNumClusters(7); i7++){
      NSWCluster *c7 = clusterList->GetCluster(7, i7);
      p7 = c7->GetCOG();
      if (p7 < 0) continue;
      if(fabs(p6-p7)<dMax) {
	lowerPair.push_back (std::make_pair(c6, c7)); 
      }
    }
  }
  // layer  6 single:
  for (int i6 = 0; i6 < clusterList->GetNumClusters(6); i6++){
    NSWCluster *c6 = clusterList->GetCluster(6, i6);
    p6 = c6->GetCOG();
    if (p6 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<lowerPair.size(); i++){
      if (lowerPair[i].first == c6) {
	found = true;
	break;
      }
    }
    if (!found) lowerSingle.push_back(c6);
  }
  // layer 7 single:
  for (int i7 = 0; i7 < clusterList->GetNumClusters(7); i7++){
    NSWCluster *c7 = clusterList->GetCluster(7, i7);
    p7 = c7->GetCOG();
    if (p7 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<lowerPair.size(); i++){
      if (lowerPair[i].second == c7) {
	found = true;
	break;
      }
    }
    if (!found) lowerSingle.push_back(c7);
  }
  //printf("lowerPair %ld, lowerSingle %ld, 6: %d, 7: %d\n", lowerPair.size(), lowerSingle.size(), clusterList->GetNumClusters(6), clusterList->GetNumClusters(7));
  //printf ("p-p %ld, p-s %ld, s-p %ld, s-s %ld\n", upperPair.size()*lowerPair.size(), upperPair.size()*lowerSingle.size(), upperSingle.size()*lowerPair.size(), upperSingle.size()*lowerSingle.size());

  NSWSegment* s;
  // upper pair - lower pair:
  for (unsigned int i=0; i<upperPair.size(); i++){
    for (unsigned int j=0; j<lowerPair.size(); j++){
      s = new NSWSegment; 
      s->PutCluster(0, upperPair[i].first);
      s->PutCluster(1, upperPair[i].second);
      s->PutCluster(6, lowerPair[j].first);
      s->PutCluster(7, lowerPair[j].second);
      s->FitPrecision(0.6);
      if (s->GetChi2() < chiCut * chiCut) {
	segment.push_back(s);
      } else {
	delete s;
      }
    }
  }
  
  // upper pair - lower single:
  for (unsigned int i=0; i<upperPair.size(); i++){
    for (unsigned int j=0; j<lowerSingle.size(); j++){
      s = new NSWSegment; 
      s->PutCluster(0, upperPair[i].first);
      s->PutCluster(1, upperPair[i].second);
      int layer = lowerSingle[j]->GetLayer();
      s->PutCluster(layer, lowerSingle[j]);
      s->FitPrecision(0.6);
      if (s->GetChi2() < chiCut) {
	segment.push_back(s);
      } else {
	delete s;
      }	
    }
  }
  
  // upper single - lower pair:
  for (unsigned int i=0; i<upperSingle.size(); i++){
    for (unsigned int j=0; j<lowerPair.size(); j++){
      s = new NSWSegment; 
      int layer = upperSingle[i]->GetLayer();
      s->PutCluster(layer, upperSingle[i]);
      s->PutCluster(6, lowerPair[j].first);
      s->PutCluster(7, lowerPair[j].second);
      s->FitPrecision(0.6);
      if (s->GetChi2() < chiCut) {
	segment.push_back(s);
      } else {
	delete s;
      }	
    }
  }

  // upper single - lower single
  for (unsigned int i=0; i<upperSingle.size(); i++){
    for (unsigned int j=0; j<lowerSingle.size(); j++){
      s = new NSWSegment; 
      int layer = upperSingle[i]->GetLayer();
      s->PutCluster(layer, upperSingle[i]);
      layer = lowerSingle[j]->GetLayer();
      s->PutCluster(layer, lowerSingle[j]);
      s->FitPrecision(0.6);
      segment.push_back(s); // no chi2 cut possible for 2 points
    }
  }
	  
  for (unsigned int i=0; i<segment.size(); i++){ // find ypos
    s = segment[i];
    std::vector <double> ypos;
    ypos.clear();
    for (int layer=2; layer<6; layer++){ // add stereo clusters
      for (int j=0; j<clusterList->GetNumClusters(layer); j++){
	NSWCluster *c = clusterList->GetCluster(layer, j);
	double pred = s->GetIntercept() + s->GetSlope() * c->GetZPosition();
	if (fabs(c->GetCOG()-pred)<dMax) {
	  s->PutCluster(layer, c);
	  double res = c->GetCOG() - pred;
	  if (layer==3) res = -res;
	  if (layer==5) res = -res;
	  double fiducial = 10+s->GetIntercept()/8000*40; // edge
	  if (fabs(res)<fiducial)  ypos.push_back(res);
	}
      }
    } // next layer

    std::sort(ypos.begin(), ypos.end()); // get median
    int size = ypos.size();
    double y = -100;// median y
    if (size > 0){
      if (size%2 == 0) {
	y = 0.5*(ypos[size/2 - 1] + ypos[size/2]);
      } else {
	y =  ypos[size/2];
      }
    }
    s->SetYPosition (y);    
  } // next segment  
  	  
  for (unsigned int i=0; i<segment.size(); i++){ // cleanup
    s = segment[i];
    if ((s->GetNumCluster() - s->GetNumPrecision() < 2) || // y clusters
	(s->GetNumCluster() < 5) ||
	(fabs(s->GetAngle()) > 65)) {
      delete s;
      segment.erase(segment.begin()+i);
      i--;
    }
  }
  /*
  for (int i=0; i<segment.size(); i++){ // cleanup segments sharing clusters
    si = segment[i];
    for (int j=i+1; j<segment.size(); j++){
      NSWSegment* sj = segment[j];
      int common = 0; // count common clusters
      for (int layer=0; layer<8; layer++){
	if (si->GetCluster(layer) == sj->GetCluster(layer)) common++;
      }
      //printf ("%d %d: %d, %f %f\n", i, j, common, s->GetChi2(), s2->GetChi2());
      if ((common>3) &&
	  (si->GetChi2() < sj->GetChi2()) &&
	  (si->GetNumPrecision() == sj->GetNumPrecision())){
	//printf ("remove j = %d from %d segments\n", j, segment.size());
        delete sj;
	segment.erase(segment.begin()+j); // remove j
	j--;
      } else {
	//printf ("remove i = %d from %d segments\n", i, segment.size());
        delete si;
	segment.erase(segment.begin()+i); // remove i
	i--;
	j = segment.size();//break;
      }	
    } // next j
  } // next i
  */
  //printf ("List has %d segments\n",  GetNumSegments());
}










void NSWSegmentList::Fill4 (NSWClusterList* clusterList, int start, double chiCut, int clusterType, double sig){
  const double dMax = 12;   // 22mm cut for pairs of hits
  const double sigma = 0.3; // error estimate of the hits, 0.25 for 0 angle
  //declaring vector of pairs 
  std::vector <std::pair <NSWCluster*, NSWCluster*> > upperPair, lowerPair;
  std::vector <NSWCluster*> upperSingle, lowerSingle;
  double p0, p1, p6, p7; // cog positions
  
  upperPair.clear();
  lowerPair.clear();
  upperSingle.clear();
  lowerSingle.clear();
  
  // layer 0 and 1:
  for (int i0 = 0; i0 < clusterList->GetNumClusters(0+start); i0++){
    NSWCluster *c0 = clusterList->GetCluster(0+start, i0);
    if(clusterType==0)
      p0 = c0->GetCOG();
    else if(clusterType==1)
      p0 = c0->GetGaussianCentroid();
    else if(clusterType==2)
      p0 = c0->GetMeanCaruana();
    else if(clusterType==3)
      p0 = c0->GetParabolaMean();

    if (p0 < 0) continue;
    for (int i1 = 0; i1 < clusterList->GetNumClusters(1+start); i1++){
      NSWCluster *c1 = clusterList->GetCluster(1+start, i1);
      if(clusterType==0)
        p1 = c1->GetCOG();
      else if(clusterType==1)
        p1 = c1->GetGaussianCentroid();
      else if(clusterType==2)
	p1 = c1->GetMeanCaruana();
      else if(clusterType==3)
	p1 = c1->GetParabolaMean();

      if (p1 < 0) continue;
      if(fabs(p0-p1)<dMax) {
	upperPair.push_back (std::make_pair(c0, c1)); 
      }
    }
  }

  // layer 0 single
  for (int i0 = 0; i0 < clusterList->GetNumClusters(0+start); i0++){
    NSWCluster *c0 = clusterList->GetCluster(0+start, i0);
    if(clusterType==0)
      p0 = c0->GetCOG();
    else if(clusterType==1)
      p0 = c0->GetGaussianCentroid();      
    else if(clusterType==2)
      p0 = c0->GetMeanCaruana();
    else if(clusterType==3)
      p0 = c0->GetParabolaMean();

    if (p0 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<upperPair.size(); i++){
      if (upperPair[i].first == c0) {
	found = true;
	break;
      }
    }
    if (!found) upperSingle.push_back(c0);
  }

  // layer 1 single
  for (int i1 = 0; i1 < clusterList->GetNumClusters(1+start); i1++){
    NSWCluster *c1 = clusterList->GetCluster(1+start, i1);
    if(clusterType==0)
      p1 = c1->GetCOG();
    else if(clusterType==1)
      p1 = c1->GetGaussianCentroid();
    else if(clusterType==2)
      p1 = c1->GetMeanCaruana();
    else if(clusterType==3)
      p1 = c1->GetParabolaMean();

    if (p1 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<upperPair.size(); i++){
      if (upperPair[i].second == c1) {
	found = true;
	break;
      }
    }
    if (!found) upperSingle.push_back(c1);
  }


  //printf("upperPair %ld, upperSingle %ld, 0: %d, 1: %d\n", upperPair.size(), upperSingle.size(), clusterList->GetNumClusters(0), clusterList->GetNumClusters(1));
  
  // lower precision layers 2 and 3:

  for (int i6 = 0; i6 < clusterList->GetNumClusters(2+start); i6++){
    NSWCluster *c6 = clusterList->GetCluster(2+start, i6);
    if(clusterType==0)
      p6 = c6->GetCOG();
    else if (clusterType==1)
      p6 = c6->GetGaussianCentroid();		
    else if (clusterType==2)
      p6 = c6->GetMeanCaruana();
    else if (clusterType==3)
      p6 = c6->GetParabolaMean();

    if (p6 < 0) continue;
    for (int i7 = 0; i7 < clusterList->GetNumClusters(3+start); i7++){
      NSWCluster *c7 = clusterList->GetCluster(3+start, i7);
      if(clusterType==0)
        p7 = c7->GetCOG();
      else if(clusterType==1)
	p7 = c7->GetGaussianCentroid();	
      else if(clusterType==2)
	p7 = c7->GetMeanCaruana();
      else if(clusterType==3)
	p7 = c7->GetParabolaMean();

      if (p7 < 0) continue;
      if(fabs(p6-p7)<dMax) {
	lowerPair.push_back (std::make_pair(c6, c7)); 
      }
    }
  }
  
  // layer  6 single:
  for (int i6 = 0; i6 < clusterList->GetNumClusters(2+start); i6++){
    NSWCluster *c6 = clusterList->GetCluster(2+start, i6);
    if(clusterType==0)
      p6 = c6->GetCOG();
    else if (clusterType==1)
      p6 = c6->GetGaussianCentroid();
    else if (clusterType==2)
      p6 = c6->GetMeanCaruana();
    else if (clusterType==3)
      p6 = c6->GetParabolaMean();

    if (p6 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<lowerPair.size(); i++){
      if (lowerPair[i].first == c6) {
	found = true;
	break;
      }
    }
    if (!found) lowerSingle.push_back(c6);
  }
  
  // layer 7 single:

  for (int i7 = 0; i7 < clusterList->GetNumClusters(3+start); i7++){
    NSWCluster *c7 = clusterList->GetCluster(3+start, i7);
    if(clusterType==0) 
      p7 = c7->GetCOG();
    else if (clusterType==1)
      p7 = c7->GetGaussianCentroid();
    else if (clusterType==2)
      p7 = c7->GetMeanCaruana();
    else if (clusterType==3)
      p7 = c7->GetParabolaMean();

    if (p7 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<lowerPair.size(); i++){
      if (lowerPair[i].second == c7) {
	found = true;
	break;
      }
    }
    if (!found) lowerSingle.push_back(c7);
  }

  //printf("lowerPair %ld, lowerSingle %ld, 6: %d, 7: %d\n", lowerPair.size(), lowerSingle.size(), clusterList->GetNumClusters(2), clusterList->GetNumClusters(3));
  //printf ("p-p %ld, p-s %ld, s-p %ld, s-s %ld\n", upperPair.size()*lowerPair.size(), upperPair.size()*lowerSingle.size(), upperSingle.size()*lowerPair.size(), upperSingle.size()*lowerSingle.size());

  NSWSegment* s;
  // upper pair - lower pair: 4hits / 4layers
  for (unsigned int i=0; i<upperPair.size(); i++){
    for (unsigned int j=0; j<lowerPair.size(); j++){
      s = new NSWSegment; 
      s->PutCluster(0+start, upperPair[i].first);
      s->PutCluster(1+start, upperPair[i].second);
      s->PutCluster(2+start, lowerPair[j].first);
      s->PutCluster(3+start, lowerPair[j].second);
      
      if(clusterType == 0)
        s->FitPrecisionCOG(start, sigma);
      else if(clusterType == 1)
        s->FitPrecisionGaus(start, sigma);
      else if(clusterType == 2)
        s->FitPrecisionCaruana(start, sigma);
      else if(clusterType == 3)
	s->FitPrecisionParabola(start, sigma);

      if (s->GetChi2() < chiCut * chiCut) {
	segment.push_back(s);
      } else {
	delete s;
      }
    }
  }
  
  // upper pair - lower single: 3hits / 4layers

  for (unsigned int i=0; i<upperPair.size(); i++){
    for (unsigned int j=0; j<lowerSingle.size(); j++){
      s = new NSWSegment; 
      s->PutCluster(0+start, upperPair[i].first);
      s->PutCluster(1+start, upperPair[i].second);
      int layer = lowerSingle[j]->GetLayer();

      s->PutCluster(layer, lowerSingle[j]);

      if(clusterType == 0)
        s->FitPrecisionCOG(start, sigma);
      else if(clusterType == 1)
        s->FitPrecisionGaus(start, sigma);
      else if(clusterType == 2)
        s->FitPrecisionCaruana(start, sigma);
      else if(clusterType == 3)
	s->FitPrecisionParabola(start, sigma);

      if (s->GetChi2() < chiCut) {
	segment.push_back(s);
      } else {
	delete s;
      }
    }
  }

  // upper single - lower pair:
  for (unsigned int i=0; i<upperSingle.size(); i++){
    for (unsigned int j=0; j<lowerPair.size(); j++){
      s = new NSWSegment; 
      int layer = upperSingle[i]->GetLayer();
      s->PutCluster(layer, upperSingle[i]);
      s->PutCluster(2+start, lowerPair[j].first);
      s->PutCluster(3+start, lowerPair[j].second);

      if(clusterType == 0)
        s->FitPrecisionCOG(start, sigma);
      else if(clusterType == 1)
        s->FitPrecisionGaus(start, sigma);
      else if(clusterType == 2)
        s->FitPrecisionCaruana(start, sigma);
      else if(clusterType == 3)
	s->FitPrecisionParabola(start, sigma);

      if (s->GetChi2() < chiCut) {
	segment.push_back(s);
      } else {
	delete s;
      }
    }
  }
  
  /*for (unsigned int i=0; i<segment.size(); i++){ // cleanup
    s = segment[i];
    if (fabs(s->GetAngle()) >= 65) {
      delete s;
      segment.erase(segment.begin()+i);
      i--;
    }
    }*/
  //==========================================================================//

  /*
  for (int i=0; i<segment.size(); i++){ // cleanup segments sharing clusters
    si = segment[i];
    for (int j=i+1; j<segment.size(); j++){
      NSWSegment* sj = segment[j];
      int common = 0; // count common clusters
      for (int layer=0; layer<8; layer++){
	if (si->GetCluster(layer) == sj->GetCluster(layer)) common++;
      }
      //printf ("%d %d: %d, %f %f\n", i, j, common, s->GetChi2(), s2->GetChi2());
      if ((common>3) &&
	  (si->GetChi2() < sj->GetChi2()) &&
	  (si->GetNumPrecision() == sj->GetNumPrecision())){
	//printf ("remove j = %d from %d segments\n", j, segment.size());
        delete sj;
	segment.erase(segment.begin()+j); // remove j
	j--;
      } else {
	//printf ("remove i = %d from %d segments\n", i, segment.size());
        delete si;
	segment.erase(segment.begin()+i); // remove i
	i--;
	j = segment.size();//break;
      }	
    } // next j
  } // next i
  */
  //printf ("Fill4: List has %d segments\n",  GetNumSegments());
}






void NSWSegmentList::multiCluster_Fill4 (NSWClusterList* clusterList, int start, double chiCut, int clusterType, double sig){
  const double dMax = 12;   // 22mm cut for pairs of hits
  const double sigma = sig; // error estimate of the hits, 0.25 for 0 angle
  //declaring vector of pairs 
  std::vector <std::pair <NSWCluster*, NSWCluster*> > upperPair, lowerPair;
  std::vector <NSWCluster*> upperSingle, lowerSingle;
  double p0, p1, p6, p7; // cog positions
  
  //std::cout << "clusterType: " << clusterType << std::endl;

  upperPair.clear();
  lowerPair.clear();
  upperSingle.clear();
  lowerSingle.clear();
  //std::cout << "Layer " << 0+start << " num of clusters: " << clusterList->GetNumClusters(0+start) << std::endl;
  //std::cout << "Layer " << 1+start << " num of clusters: " << clusterList->GetNumClusters(1+start) << std::endl;
  
  //std::cout << "upper pair: " << std::endl;
  
  // layer 0 and 1:
  for (int i0 = 0; i0 < clusterList->GetNumClusters(0+start); i0++){
    NSWCluster *c0 = clusterList->GetCluster(0+start, i0);
    if(clusterType==0)
      p0 = c0->GetCOG();
    else if(clusterType==1)
      p0 = c0->GetGaussianCentroid();
    else if(clusterType==2)
      p0 = c0->GetMeanCaruana();

    if (p0 < 0) continue;
    for (int i1 = 0; i1 < clusterList->GetNumClusters(1+start); i1++){
      NSWCluster *c1 = clusterList->GetCluster(1+start, i1);
      if(clusterType==0)
        p1 = c1->GetCOG();
      else if(clusterType==1)
        p1 = c1->GetGaussianCentroid();
      else if(clusterType==2)
	p1 = c1->GetMeanCaruana();

      if (p1 < 0) continue;
      //if(fabs(p0-p1)<dMax) {
      upperPair.push_back (std::make_pair(c0, c1)); 
      //}
    }
  }

  //std::cout << "upper pair size: " << upperPair.size() << std::endl;

  //std::cout << "upper single layer 4: " << std::endl;
  // layer 0 single
  for (int i0 = 0; i0 < clusterList->GetNumClusters(0+start); i0++){
    NSWCluster *c0 = clusterList->GetCluster(0+start, i0);
    if(clusterType==0)
      p0 = c0->GetCOG();
    else if(clusterType==1)
      p0 = c0->GetGaussianCentroid();      
    else if(clusterType==2)
      p0 = c0->GetMeanCaruana();

    if (p0 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<upperPair.size(); i++){
      if (upperPair[i].first == c0) {
	found = true;
	break;
      }
    }
    if (!found) upperSingle.push_back(c0);
  }

  //std::cout << "upper single layer 5: " << std::endl;
  // layer 1 single
  for (int i1 = 0; i1 < clusterList->GetNumClusters(1+start); i1++){
    NSWCluster *c1 = clusterList->GetCluster(1+start, i1);
    if(clusterType==0)
      p1 = c1->GetCOG();
    else if(clusterType==1)
      p1 = c1->GetGaussianCentroid();
    else if(clusterType==2)
      p1 = c1->GetMeanCaruana();

    if (p1 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<upperPair.size(); i++){
      if (upperPair[i].second == c1) {
	found = true;
	break;
      }
    }
    if (!found) upperSingle.push_back(c1);
  }

  //std::cout << "upper single size: " << upperSingle.size() << std::endl;

  //printf("upperPair %ld, upperSingle %ld, 0: %d, 1: %d\n", upperPair.size(), upperSingle.size(), clusterList->GetNumClusters(0), clusterList->GetNumClusters(1));
  
  // lower precision layers 2 and 3:

  //std::cout << "Layer " << 2+start << " num of clusters: " << clusterList->GetNumClusters(2+start) << std::endl;
  //std::cout << "Layer " << 3+start << " num of clusters: " << clusterList->GetNumClusters(3+start) << std::endl;

  //std::cout << "lower pair: " << std::endl;
  for (int i6 = 0; i6 < clusterList->GetNumClusters(2+start); i6++){
    NSWCluster *c6 = clusterList->GetCluster(2+start, i6);
    if(clusterType==0)
      p6 = c6->GetCOG();
    else if (clusterType==1)
      p6 = c6->GetGaussianCentroid();		
    else if(clusterType==2)
      p6 = c6->GetMeanCaruana();
      
    if (p6 < 0) continue;
    for (int i7 = 0; i7 < clusterList->GetNumClusters(3+start); i7++){
      NSWCluster *c7 = clusterList->GetCluster(3+start, i7);
      if(clusterType==0)
        p7 = c7->GetCOG();
      else if(clusterType==1)
	p7 = c7->GetGaussianCentroid();	
      else if(clusterType==2)
	p7 = c7->GetMeanCaruana();

      if (p7 < 0) continue;
      //if(fabs(p6-p7)<dMax) {
      lowerPair.push_back (std::make_pair(c6, c7)); 
      //}
    }
  }
  
  //std::cout << "lower pair size: " << lowerPair.size() << std::endl;

  // layer  6 single:
  //std::cout << "lower single layer 6: " << std::endl;
  for (int i6 = 0; i6 < clusterList->GetNumClusters(2+start); i6++){
    NSWCluster *c6 = clusterList->GetCluster(2+start, i6);
    if(clusterType==0)
      p6 = c6->GetCOG();
    else if (clusterType==1)
      p6 = c6->GetGaussianCentroid();
    else if(clusterType==2)
      p6 = c6->GetMeanCaruana();

    if (p6 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<lowerPair.size(); i++){
      if (lowerPair[i].first == c6) {
	found = true;
	break;
      }
    }
    if (!found) lowerSingle.push_back(c6);
  }
  
  // layer 7 single:
  //std::cout << "lower single layer 7: " << std::endl;
  for (int i7 = 0; i7 < clusterList->GetNumClusters(3+start); i7++){
    NSWCluster *c7 = clusterList->GetCluster(3+start, i7);
    if(clusterType==0) 
      p7 = c7->GetCOG();
    else if (clusterType==1)
      p7 = c7->GetGaussianCentroid();
    else if(clusterType==2)
      p7 = c7->GetMeanCaruana();

    if (p7 < 0) continue;
    bool found = false;
    for (unsigned int i=0; i<lowerPair.size(); i++){
      if (lowerPair[i].second == c7) {
	found = true;
	break;
      }
    }
    if (!found) lowerSingle.push_back(c7);
  }

  //std::cout << "lower single size: " << lowerSingle.size() << std::endl;

  //printf("lowerPair %ld, lowerSingle %ld, 6: %d, 7: %d\n", lowerPair.size(), lowerSingle.size(), clusterList->GetNumClusters(2), clusterList->GetNumClusters(3));
  //printf ("p-p %ld, p-s %ld, s-p %ld, s-s %ld\n", upperPair.size()*lowerPair.size(), upperPair.size()*lowerSingle.size(), upperSingle.size()*lowerPair.size(), upperSingle.size()*lowerSingle.size());

  //================================================== saving all the segments possible in a vector (not the final vector) =======================//

  std::vector<NSWSegment*> seglist_perHT;

  NSWSegment* s;
  // upper pair - lower pair: 4hits / 4layers
  //std::cout << "//======== upper pair lower pair segments: " << std::endl;
  for (unsigned int i=0; i<upperPair.size(); i++){
    for (unsigned int j=0; j<lowerPair.size(); j++){
      int num_clu_2chn = 0;
      if(upperPair[i].first->GetNumChannel()==2) num_clu_2chn++; 
      if(upperPair[i].second->GetNumChannel()==2) num_clu_2chn++;
      if(lowerPair[i].first->GetNumChannel()==2) num_clu_2chn++;
      if(lowerPair[i].second->GetNumChannel()==2) num_clu_2chn++;
      
      if(num_clu_2chn >= 2) continue;

      s = new NSWSegment; 
      s->PutCluster(0+start, upperPair[i].first);
      s->PutCluster(1+start, upperPair[i].second);
      s->PutCluster(2+start, lowerPair[j].first);
      s->PutCluster(3+start, lowerPair[j].second);
      
      if(clusterType == 0)
        s->FitPrecisionCOG(start, sigma);
      else if(clusterType == 1)
        s->FitPrecisionGaus(start, sigma);
      else if(clusterType == 2)
	s->FitPrecisionCaruana(start, sigma);
      else if(clusterType == 3)
	s->FitPrecisionParabola(start, sigma);

      //std::cout << "segment angle: " << s->GetAngle() << std::endl;
      ////seglist_perHT.push_back(s); 
      segment.push_back(s);
      
      /*std::cout << "For seg: " ;
      for(int iL=4; iL<8; iL++){
	NSWCluster* cl = s->GetCluster(iL);
	if(cl == NULL) continue;
	std::cout << "Cluster Layer " << iL << ": " << cl->GetGaussianCentroid() << " " ;
      }
      std::cout << " " << std::endl;*/
      
    }
  }
  //std::cout << "//=====================================" << std::endl;
  
  // upper pair - lower single: 3hits / 4layers
  //std::cout << "//======== upper pair lower single segments: " <<std::endl;
  for (unsigned int i=0; i<upperPair.size(); i++){
    for (unsigned int j=0; j<lowerSingle.size(); j++){
      
      int num_clu_2chn = 0;
      if(upperPair[i].first->GetNumChannel()==2) num_clu_2chn++;
      if(upperPair[i].second->GetNumChannel()==2) num_clu_2chn++;
      if(lowerSingle[i]->GetNumChannel()==2) num_clu_2chn++;

      if(num_clu_2chn>=1) continue;

      s = new NSWSegment; 
      s->PutCluster(0+start, upperPair[i].first);
      s->PutCluster(1+start, upperPair[i].second);
      int layer = lowerSingle[j]->GetLayer();
      //std::cout << "lower single layer: " << layer << " cluster centroid: " << lowerSingle[j]->GetGaussianCentroid() << std::endl;
      s->PutCluster(layer, lowerSingle[j]);

      if(clusterType == 0)
        s->FitPrecisionCOG(start, sigma);
      else if(clusterType == 1)
        s->FitPrecisionGaus(start, sigma);
      else if(clusterType == 2)
	s->FitPrecisionCaruana(start, sigma);
      else if(clusterType == 3)
	s->FitPrecisionParabola(start, sigma);

      //std::cout << "segment angle: " << s->GetAngle() << std::endl;
      ////seglist_perHT.push_back(s); 
      segment.push_back(s);

      /*std::cout << "For seg: " ;
      for(int iL=4; iL<8; iL++){
        NSWCluster* cl = s->GetCluster(iL);
        if(cl == NULL) continue;
	std::cout << "Cluster Layer " << iL << ": " << cl->GetGaussianCentroid() << " " ;
      }
      std::cout << " " << std::endl;*/

    }
  }
  //std::cout << "//=====================================" << std::endl;

  // upper single - lower pair:
  //std::cout << "//======== upper single lower pair segments: " <<std::endl;
  for (unsigned int i=0; i<upperSingle.size(); i++){
    for (unsigned int j=0; j<lowerPair.size(); j++){
      int num_clu_2chn = 0;
      if(upperSingle[i]->GetNumChannel()==2) num_clu_2chn++;
      if(lowerPair[i].first->GetNumChannel()==2) num_clu_2chn++;
      if(lowerPair[i].second->GetNumChannel()==2) num_clu_2chn++;
      
      if(num_clu_2chn>=1) continue;
      
      s = new NSWSegment; 
      int layer = upperSingle[i]->GetLayer();
      //std::cout << "upper single layer: " << layer << " cluster centroid: " << upperSingle[j]->GetGaussianCentroid() << std::endl;
      s->PutCluster(layer, upperSingle[i]);
      s->PutCluster(2+start, lowerPair[j].first);
      s->PutCluster(3+start, lowerPair[j].second);

      if(clusterType == 0)
        s->FitPrecisionCOG(start, sigma);
      else if(clusterType == 1)
        s->FitPrecisionGaus(start, sigma);
      else if(clusterType == 2)
	s->FitPrecisionCaruana(start, sigma);
      else if(clusterType == 3)
	s->FitPrecisionParabola(start, sigma);

      //std::cout << "segment angle: " << s->GetAngle() << std::endl;
      ////seglist_perHT.push_back(s); 
      segment.push_back(s);

      /*std::cout << "For seg: " ;
      for(int iL=4; iL<8; iL++){
        NSWCluster* cl = s->GetCluster(iL);
        if(cl == NULL) continue;
	std::cout << "Cluster Layer " << iL << ": " << cl->GetGaussianCentroid() << " " ;
      }
      std::cout << " " << std::endl;*/

    }
  }

  //===================== Printouts ==========================//
  
  //std::cout << "in SegmentList class - Segment size " << seglist_perHT.size() << std::endl;

  //===================== Removing tracks which do not show good fit ====================//

  ////for (int ii=0; ii<seglist_perHT.size(); ii++){
  //std::cout << "Seg Comparisons: " << segment.size() << std::endl;
  
  //std::cout << "Segment size: " << segment.size() << std::endl;

  for (int ii=0; ii<segment.size(); ii++){   

    ////NSWSegment* s_ii = seglist_perHT[ii];
    ////if(seglist_perHT[ii] == NULL) continue;
    
    NSWSegment* s_ii = segment[ii];

    //===================== Printouts ==========================//
    //std::cout << "in SegmentList class - Segment " << ii << ": chi2 " << s_ii->GetChi2() << std::endl;
    //==========================================================//
    
    ////for (int jj=ii+1; jj<seglist_perHT.size(); jj++){
    for (int jj=ii+1; jj<segment.size(); jj++){
      ////NSWSegment* s_jj = seglist_perHT[jj];
      ////if(seglist_perHT[jj] == NULL) continue;
      
      ////NSWCluster* clu = NULL;
      ////int common_clu = 0;
      
      ////for (int layer=4; layer<8; layer++){
      ////  if (s_ii->GetCluster(layer) == s_jj->GetCluster(layer)){
      ////    common_clu++;
      ////    clu = s_ii->GetCluster(layer);
      ////  }
      ////}

      NSWSegment* s_jj = segment[jj];
      
      std::vector<int> common_layers;
      s_ii->Compare(s_jj, common_layers);
      int common_clu = common_layers.size();

      //std::cout << "common clusters: " << common_clu << std::endl;

      // Always take the 4 cluster segment if 3 are shared.
      if(common_clu >= 3){
	if ( s_ii->GetNumClusters() == s_jj->GetNumClusters() ) {
	  if(s_ii->GetChi2() > s_jj->GetChi2()){
	    RemoveSegment(ii);
	    ii--;
	    jj = GetNumSegments();
	  }
	  else {
	    RemoveSegment(jj);
	    jj--;
	  }
	}
	else if ( s_ii->GetNumClusters()>s_jj->GetNumClusters() ) {
	  RemoveSegment(jj);
	  jj--;
	}
	else {
	  RemoveSegment(ii);
	  ii--;
	  jj = GetNumSegments();
	}
      }
      else if(common_clu == 2){ // more than one common clusters
	////if(s_ii->GetChi2() > s_jj->GetChi2()){
	////  seglist_perHT[ii] = NULL;
	////}
	////else{
	////  seglist_perHT[jj] = NULL;
	////}
	//std::cout << "More Common Clusters: " << std::endl;
	//############### Debugging #####################
	//std::cout << common_clu <<" Clusters commons: " << std::endl;
	//std::cout << "For s_ii: " ;
	//for(int iL=4; iL<8; iL++){
	//  NSWCluster* cl = s_ii->GetCluster(iL);
	//  if(cl == NULL) continue;
	//  std::cout << "Cluster Layer " << iL << ": " << cl->GetGaussianCentroid() << " " ;
	//}
	//std::cout << " " << std::endl;
	//std::cout << "For s_jj: " ;
	//for(int iL=4; iL<8; iL++){
	//  NSWCluster* cl = s_jj->GetCluster(iL);
	//  if(cl == NULL) continue;
	//  std::cout << "Cluster Layer " << iL << ": " << cl->GetGaussianCentroid() << " " ;
	//}
	//std::cout << " " << std::endl;
	//############### End of Debugging ##################### 

	/*std::cout << "For first seg " << ii << ": " ;
	for(int iL=4; iL<8; iL++){
	  NSWCluster* cl = s_ii->GetCluster(iL);
	  if(cl == NULL) continue;
	  std::cout << "Cluster Layer " << iL << ": " << cl->GetGaussianCentroid() << " " ;
	}
	std::cout << " " << std::endl;

	std::cout << "For second seg " << jj << ": " ;
	for(int iL=4; iL<8; iL++){
	  NSWCluster* clus = s_jj->GetCluster(iL);
	  if(clus == NULL) continue;
	  std::cout << "Cluster Layer " << iL << ": " << clus->GetGaussianCentroid() << " " ;
	}
	std::cout << " " << std::endl;*/
	  
	if(s_ii->GetChi2() < 20 && s_jj->GetChi2() < 20 ) {
	  
	  if ( s_ii->GetNumClusters()==s_jj->GetNumClusters() ) {
	    if(s_ii->GetChi2() > s_jj->GetChi2()){
	      RemoveSegment(ii);
	      ii--;
	      jj = GetNumSegments();
	    }
	    else {
	      RemoveSegment(jj);
	      jj--;
	    }
	  }
	  else if ( s_ii->GetNumClusters()>s_jj->GetNumClusters() ) {
	    RemoveSegment(jj);
	    jj--;
	  }
	  else {
	    RemoveSegment(ii);
	    ii--;
	    jj = GetNumSegments();
	  }
	}
	else if(s_ii->GetChi2() > s_jj->GetChi2()){
	  RemoveSegment(ii);
	  ii--;
	  jj = GetNumSegments();
	}
	else if(s_ii->GetChi2() == s_jj->GetChi2() && s_ii->GetNumClusters()==s_jj->GetNumClusters() && common_clu==s_ii->GetNumClusters()){
	  //std::cout << "Double counting: " << std::endl;
	  RemoveSegment(jj);
	  jj--;
	}
	else if(s_ii->GetChi2() < s_jj->GetChi2()){
	  RemoveSegment(jj);
          jj--;
	}
      }
      else if(common_clu == 1){ // only one common cluster
	//std::cout << "Common Clusters: " << std::endl;
	//std::cout << common_clu <<" Clusters commons: " << std::endl;
	NSWCluster* clu = s_ii->GetCluster(common_layers[0]);

	if(s_ii->GetChi2() < 20 && s_jj->GetChi2() < 20 ) {
          
	  if ( s_ii->GetNumClusters()==s_jj->GetNumClusters() ) {
            if(s_ii->GetChi2() > s_jj->GetChi2()){
	      if(s_ii->GetNumClusters() <=3){
		////seglist_perHT[ii] = NULL;
		RemoveSegment(ii);
		ii--;
		jj = GetNumSegments();
	      }
	      else if(s_ii->GetNumClusters() == 4){
		////int il = clu->GetLayer();
		int high_chi2 = s_ii->GetChi2();

		int il = common_layers[0];
		s_ii->RemoveCluster(il);
		if(clusterType == 0)
		  s_ii->FitPrecisionCOG(start, sigma);
		else if(clusterType == 1)
		  s_ii->FitPrecisionGaus(start, sigma);
		else if(clusterType == 2)
		  s_ii->FitPrecisionCaruana(start, sigma);
		else if(clusterType == 3)
		  s_ii->FitPrecisionParabola(start, sigma);

		if(s_ii->GetChi2() >= high_chi2){
		  s_ii->PutCluster(il,clu);
		  if(clusterType == 0)
		    s_ii->FitPrecisionCOG(start, sigma);
		  else if(clusterType == 1)
		    s_ii->FitPrecisionGaus(start, sigma);
		  else if(clusterType == 2)
		    s_ii->FitPrecisionCaruana(start, sigma);
		  else if(clusterType == 3)
		    s_ii->FitPrecisionParabola(start, sigma);
		}

	      }
	    }
            else {

	      if(s_jj->GetNumClusters() <=3){
		////seglist_perHT[ii] = NULL;
		RemoveSegment(jj);
		jj--;
	      }
	      else if(s_jj->GetNumClusters() == 4){
		////int il = clu->GetLayer();
		int high_chi2 = s_jj->GetChi2();

		int il = common_layers[0];
		s_jj->RemoveCluster(il);
		if(clusterType == 0)
		  s_jj->FitPrecisionCOG(start, sigma);
		else if(clusterType == 1)
		  s_jj->FitPrecisionGaus(start, sigma);
		else if(clusterType == 2)
		  s_jj->FitPrecisionCaruana(start, sigma);
		else if(clusterType == 3)
		  s_jj->FitPrecisionParabola(start, sigma);
		  
		if(s_jj->GetChi2() >= high_chi2){
		  s_jj->PutCluster(il,clu);
		  if(clusterType == 0)
		    s_jj->FitPrecisionCOG(start, sigma);
		  else if(clusterType == 1)
		    s_jj->FitPrecisionGaus(start, sigma);
		  else if(clusterType == 2)
		    s_jj->FitPrecisionCaruana(start, sigma);
		  else if(clusterType == 3)
		    s_jj->FitPrecisionParabola(start, sigma);
		}

	      }
	    } // end of else jj > ii chi2
          } // end of if cluster # equal
          else if ( s_ii->GetNumClusters()>s_jj->GetNumClusters() ) {
            RemoveSegment(jj);
            jj--;
          }
          else {
            RemoveSegment(ii);
            ii--;
            jj = GetNumSegments();
          }
        } // end of if both chi2 are low
	else if(s_ii->GetChi2() > s_jj->GetChi2()){
	  int high_chi2 = s_ii->GetChi2();
	  if(s_ii->GetNumClusters() <=3){
	    ////seglist_perHT[ii] = NULL;
	    RemoveSegment(ii);
	    ii--;
	    jj = GetNumSegments();
	  }
	  else if(s_ii->GetNumClusters() == 4){
	    ////int il = clu->GetLayer();
	    int il = common_layers[0];
	    s_ii->RemoveCluster(il);
	    if(clusterType == 0)
	      s_ii->FitPrecisionCOG(start, sigma);
	    else if(clusterType == 1)
	      s_ii->FitPrecisionGaus(start, sigma);
	    else if(clusterType == 2)
	      s_ii->FitPrecisionCaruana(start, sigma);
	    else if(clusterType == 3)
	      s_ii->FitPrecisionParabola(start, sigma);
	    
	    if(s_ii->GetChi2() >= high_chi2){
	      s_ii->PutCluster(il,clu);
	      if(clusterType == 0)
		s_ii->FitPrecisionCOG(start, sigma);
	      else if(clusterType == 1)
		s_ii->FitPrecisionGaus(start, sigma);
	      else if(clusterType == 2)
		s_ii->FitPrecisionCaruana(start, sigma);
	      else if(clusterType == 3)
		s_ii->FitPrecisionParabola(start, sigma);
	    }
	  
	  }
	}
	else if(s_ii->GetChi2() < s_jj->GetChi2()){
          int high_chi2= s_jj->GetChi2();
          if(s_jj->GetNumClusters() <=3){
	    ////seglist_perHT[jj] = NULL;
	    if(s_jj->GetNumClusters() <=3){
	      RemoveSegment(jj);
	      jj--;
	    }
          }
          else if(s_jj->GetNumClusters() == 4){
            int il = clu->GetLayer();
            s_jj->RemoveCluster(il);
            if(clusterType == 0)
              s_jj->FitPrecisionCOG(start, sigma);
            else if(clusterType == 1)
              s_jj->FitPrecisionGaus(start, sigma);
	    else if(clusterType == 2)
	      s_jj->FitPrecisionCaruana(start, sigma);
	    else if(clusterType == 3)
	      s_jj->FitPrecisionParabola(start, sigma);

            if(s_jj->GetChi2() >= high_chi2){
	      s_jj->PutCluster(il,clu);
	      if(clusterType == 0)
                s_jj->FitPrecisionCOG(start, sigma);
              else if(clusterType == 1)
                s_jj->FitPrecisionGaus(start, sigma);
	      else if(clusterType == 2)
		s_jj->FitPrecisionCaruana(start, sigma);
	      else if(clusterType == 3)
		s_jj->FitPrecisionParabola(start, sigma);
            }
	    

          }
	}
      }
      else{ // no common clusters
	
      }

    }

  }
  
  //std::cout << "After - Segment size: "<< segment.size() << std::endl;

  /*for(int iseg=0; iseg<seglist_perHT.size(); iseg++){
    if(seglist_perHT[iseg] == NULL) continue;
    NSWSegment* seg = seglist_perHT[iseg];
    if(seg->GetChi2()){
      
    }
    segment.push_back(seglist_perHT[iseg]);
    }*/

}

void NSWSegmentList::Print () {
  // print some information
  printf ("SegmentList with %d entries:\n", GetNumSegments());
  for (int i=0; i<GetNumSegments(); i++) segment[i]->Print();
}

